package com.mii.constant;

public class Scheduler {

	public static final String OneTimeTask 		= "One Time Task";
	public static final String RepeatInterval	= "Repeat Interval";
	public static final String RepeatTime		= "Repeat Time";
	
}
