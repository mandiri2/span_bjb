package com.mii.constant;

public class StepRecon {
	public static final String rtgsRequest		= "RTGSRequest";
	public static final String rtgsResponse		= "RTGSResponse";
	public static final String rtgsGetNoSakti	= "RTGSGetNoSakti";
	public static final String fileAlreadyInLocalPath	= "fileAlreadyInLocalPath";

	public static final String failedInitiateDataRTGSRequest	= "Initiate Data RTGS Failed";
	public static final String successInitiateDataRTGSRequest	= "Initiate Data RTGS Success";
	public static final String successRTGSRequest	= "Success, Finish RTGS Request!";
	public static final String failedRTGSRequest	= "Failed, Finish RTGS Request!";
	public static final String successRTGSResponse	= "Success, Finish RTGS Response!";
	public static final String failedRTGSResponse	= "Failed, Finish RTGS Response!";
	public static final String sftpBPSXNotConnected	= "SFTP BPSX Not Connected";
	public static final String sftpBPOXNotConnected	= "SFTP BPOX Not Connected";
	public static final String fileBPSXNotFound	= "File BPSX Not Found";
	public static final String fileReconAvalilabelInLocal		= "File Recon Avalilabel in Local";//
	public static final String fileReconNotAvalilabelInLocal	= "File Recon Not Avalilabel in Local";//
	public static final String fileBPOXNotFound	= "File BPOX Not Found";
	public static final String failedDownloadBPSX	= "Failed Download BPSX from EQ";
	public static final String failedDownloadBPOX	= "Failed Download BPOX from EQ";
	public static final String successDownloadBPOX	= "Success Download BPOX from EQ";
	public static final String failedReadBPSX	= "Failed Read BPSX";
	public static final String succesReadBPSX	= "Success Read BPSX";
	public static final String failedReadBPOX	= "Failed Read BPOX";
	public static final String successReconciliateFile	= "Success Reconciliate File";//
	public static final String failedReconciliateFile	= "Failed Reconciliate File";//
	public static final String rejectResponseBPP= "Reject Response BPP";
	public static final String successResponseBPP= "Success Response BPP";
	public static final String failedGenerateRTGSRequestFile	= "Failed Generate RTGS Request File";
	public static final String successGenerateRTGSRequestFile	= "Success Generate RTGS Request File";
	public static final String sftpRTGSNotConnected	= "SFTP BPSX Not Connected";
	public static final String failedPutRTGSRequest	= "Failed Put RTGS Request";
	public static final String succesPutRTGSRequest	= "Success Put RTGS Request";
	public static final String failedMoveFile	= "Failed Move File";
	public static final String successMoveFile	= "Failed Move File";
}
