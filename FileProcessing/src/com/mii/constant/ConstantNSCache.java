package com.mii.constant;

public class ConstantNSCache {
	public static final String localPathBS			= "BS_LocalPath";

	public static final String hostBPP 				= "RTGS_BPP_Host"; 
	public static final String usernameBPP			= "RTGS_BPP_Username"; 
	public static final String portBPP				= "RTGS_BPP_Port"; 
	public static final String privateKeyPathBPP	= "RTGS_BPP_ProvateKeyPath"; 
	public static final String workingPathBPP		= "RTGS_BPP_WorkingPath";
	public static final String requestPathBPP		= "RTGS_BPP_RequestPath";
	public static final String responsePathBPP		= "RTGS_BPP_ResponsePath";
	public static final String workingPathBPO		= "RTGS_BPO_WorkingPath";
	public static final String fileNameBPPResponse	= "RTGS_BPP_FileNameResponse";
	public static final String fileNameBPPRequest	= "RTGS_BPP_FileNameRequest";
	public static final String backupRequestDirBPP	= "RTGS_BPP_BackupRequestDir";
	public static final String backupResponseDirBPP	= "RTGS_BPP_BackupResponseDir";
	public static final String knownHostKey			= "KNOWN_HOST_KEY_PATH_FILE";
	
	public static final String hostBPSX				= "RTGS_BPSX_Host"; 
	public static final String usernameBPSX			= "RTGS_BPSX_UserName"; 
	public static final String portBPSX				= "RTGS_BPSX_Port"; 
	public static final String privateKeyPathBPSX	= "RTGS_BPSX_PrivateKeyPath"; 
	public static final String remotePathBPSX		= "RTGS_BPSX_RemotePath";
	public static final String workingPathBPSX		= "RTGS_BPSX_WorkingPath"; 
	public static final String backupDirBPSX		= "RTGS_BPSX_BackupDir";
	public static final String filePatternBPSX		= "RTGS_BPSX_FilePattern";
	
	public static final String destinationAccountNumber	= "RTGS_TRX_DestAcctNo";
	public static final String destinationAccountName	= "RTGS_TRX_DestAcctName";
	public static final String sourceAccountName	= "RTGS_TRX_SourceAcctNo";
	public static final String bankCode				= "RTGS_TRX_BankCode";
	public static final String description1			= "RTGS_TRX_Desc1";
	public static final String description2			= "RTGS_TRX_Desc2";
	public static final String description3			= "RTGS_TRX_Desc3";
	public static final String description4			= "RTGS_TRX_Desc4";
	public static final String userID				= "RTGS_TRX_UserID";
	public static final String reserved1			= "RTGS_TRX_Reserved1";
	public static final String reserved2			= "RTGS_TRX_Reserved2";
	public static final String reserved3			= "RTGS_TRX_Reserved3";
	public static final String reserved4			= "RTGS_TRX_Reserved4";
	
	public static final String fileNameReconFile	= "ReconFile_TRX_FileName";
	public static final String directoryReconFile	= "ReconFile_Directory_Working";
	public static final String backupReconFile		= "ReconFile_Directory_Backup";
	
	public static final String wsAddressGetNoSakti	= "WS_Addr_GetNoSakti";
	public static final String wsUsernameGetNoSakti = "WS_UserName_GetNoSakti";
	public static final String wsPasswordGetNoSakti = "WS_Password_GetNoSakti";
	
	

}
