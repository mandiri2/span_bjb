package com.mii.constant;

public class State {

	public static final String Success 	= "Success";
	public static final String Failed 	= "Failed";
	public static final String Error 	= "Error";
	public static final String NotFound= "Not Found";
	public static final String Started	= "Started";
	public static final String Shutdown	= "Shutdown";
	public static final String Standby	= "Standby";
	
}
