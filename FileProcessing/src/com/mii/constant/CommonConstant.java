package com.mii.constant;

import java.util.UUID;

import com.mii.util.CommonDate;

public class CommonConstant {

	public static final String contextFile		= "config/fileProcessingMPNG2.xml";
//	public static final String defaultLog4jFile	= "/Users/admin/Desktop/FileProcessingPath/Log4j/Config/log4j.properties";
	public static final String defaultLog4jFile	= "log4j.properties";
	public static final String textFileFormat	= ".txt";
	public static String transactionID	= UUID.randomUUID().toString();
	public static final String lineSeparator	= "line.separator";
	public static final String kresSeparator	= "SGT#";
	public static final String atSeparator		= "SGT@";
	public static final String stripSeparator	= "SGT-";
	public static final String percentSeparator	= "SGT%";
	public static final String andSeparator		= "SGT&";
	public static final String starSeparator	= "SGT*";
	public static final String regexTitikKoma	= ";";
	public static final String emptyString		= "";
	
//	public static final String BPSXFileReferenceNo			= "BPSXReferenceNo";
	public static final int BPSXContentReferenceNoStartIdx	= 10;
	public static final int BPSXContentReferenceNoEndIdx	= 26;
	public static final int BPSXContentReferenceContentLineLength	= 57;
	public static final int BPSXContentHeaderLength	= 46;
	public static final int BPSXContentDetailLength	= 57;
	public static final int BPSXContentFooterLength	= 22;
	
	public static final String BPPPaymentTye	= "R99";
	public static final String BPPFilenameUnikID= CommonDate.getCurrentDateString("yyyyMMddHHmmss");
	public static final String BPPUnikIDParam	= "BPPUnikID";
	public static final String BPPSchedulerNameGetFileResponse		= "SchedulerGetResponseRTGSFile";
	public static final String BPPSchedulerServiceGetFileResponse	= "RTGSResponseHandler";
	public static final String BPPSchedulerIntervalGetFileResponse	= "30";
	public static final String BPPSchedulerEndTimeGetFileResponse	= "08:59:00";
	public static final String BPPTransactionIDDataParam			= "BPPTransactionIDData";
	public static final String BPPResultReject	= "F";
	public static final String BPPReferenceNoParam	= "ReferenceNo";
	
	public static final String ReconFileRegex		= ";";
	
	public static final String WSDefaultAddressGetNoSakti 	= "http://APPMDWDEV03.dev.corp.btpn.co.id:5555/ws/com.btpn.mpn.ws.GetNoSakti/com_btpn_mpn_ws_GetNoSakti_Port";
	public static final String WSDefaultUsernameGetNoSakti 	= "Administrator";
	public static final String WSDefaultPasswordGetNoSakti 	= "manage";
	
	public static final String PelimpahanTrxCode = "999";
	public static final String PelimpahanTrxCodeEq = "059";
	public static final String CreditViaTellerTrxCode = "101";
	public static final String CreditViaTellerTrxCodeEq = "557";
	public static final String CreditViaATMTrxCode = "202";
	public static final String CreditViaATMTrxCodeEq = "558";
	public static final String DebitViaATMTrxCodeEq = "058";
	public static final String DebitViaATMTrxCode = "702";
	public static final String DebitViaTellerTrxCodeEq = "057";
	public static final String DebitViaTellerTrxCode = "601";
	
	//FAST
	public static final String FAST_FOUND						= "Valid/benar";
	public static final String FAST_SALAH_NAMA					= "Salah Nama";
	public static final String FAST_SALAH_NOMOR					= "Salah Nomor";
	public static final String FAST_NOT_FOUND					= "Tidak ditemukan";
	public static final String FAST_REK_DORMANT					= "Rekening Tutup/tidak aktif";
}
