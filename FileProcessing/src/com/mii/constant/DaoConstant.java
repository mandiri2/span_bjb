package com.mii.constant;

public class DaoConstant {
	public static final String ProviderDao = "providerDao";
	public static final String SpanDataValidationDao = "spanDataValidationDao";
	public static final String SpanRecreateRejectedFile = "spanRecreateFileRejectedDao";
	public static final String SpanDocumentNoDetailListDao = "spanDocumentNoDetailListDao";
	public static final String SpanHostDataDetailsDao = "spanHostDataDetailsDao";
	public static final String SpanHostResponseCodeDao = "spanHostResponseCodeDao";
	public static final String SpanRecreateFileRejectedDao = "spanRecreateFileRejectedDao";
	public static final String SpanVoidDataTrxDao = "spanVoidDataTrxDao";
	public static final String SpanHostResponseDao = "spanHostResponseDao";
	public static final String SpanHostDataFailedDao = "spanHostDataFailedDao";
	public static final String SpanUserLogDao = "spanUserLogDao";
	public static final String SpanLogTrxUserDao = "spanLogTrxUserDao";
	public static final String SystemParameterDAO = "sysParamDAO";
	public static final String DataMT940DAO	= "dataListDAO";
	public static final String SpanAccountBalanceLogDao = "spanAccountBalanceLogDao";
	public static final String SpanDataValidationSP2DNoDao = "spanDataValidationSP2DNoDao";
	public static final String SpanDataOriginalXMLDao = "spanDataOriginalXMLDao";
	public static final String SpanGetFilesDao = "spanGetFilesDao";
	public static final String SpanPenihilanLogDao = "spanPenihilanLogDao";
}
