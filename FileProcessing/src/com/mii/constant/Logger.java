package com.mii.constant;

public class Logger {

	public static final String initEvent	= "Init";
	public static final String startEvent	= "Start";
	public static final String finishEvent	= "Finish";
	public static final String errorEvent	= "Error";
	
	public static final String processRTGSRequest		= "RTGS Request";
	public static final String processRTGSResponse		= "RTGS Response";
	public static final String processGetNoSakti		= "Get No Sakti";
	public static final String processReconFile			= "Recon File";
	
	public static final String detailProcessCheckDataBPS	= "checkDataBPSX";
	public static final String detailProcessCheckBPSXFile	= "checkBPSXFile";
	public static final String detailProcessCheckBPOXFile	= "checkBPOXFile";
	public static final String detailProcessreadBPSXFile	= "readBPSXFile";
	public static final String detailProcessreadBPOXFile	= "readBPOXFile";
	public static final String detailProcessextractBPOXFile	= "extractBPOXFile";
	public static final String detailProcessgenerateRTGSFileRequest	= "generateRTGSFileRequest";
	public static final String detailProcessputFileRTGSRequest	= "putFileRTGSRequest";
	public static final String detailProcessgetFileRTGSRsponse	= "getFileRTGSResponse";
	public static final String detailProcessmoveFile	= "moveFile";
	public static final String detailProcessremoveAllOfHashMap	= "removeAllOfHashMap";
	
}
