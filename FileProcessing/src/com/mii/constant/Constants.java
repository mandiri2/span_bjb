package com.mii.constant;


public final class Constants {
	
	//UIM
	public static final String UIM_HOST 						= "UIM_HOST";
	public static final String UIM_PORT 						= "UIM_PORT";
	public static final String UIM_ID_APLIKASI					= "UIM_ID_APLIKASI";
	
	//GAPURA
	public static final String SPAN_LOCAL_IP					= "SPAN_LOCAL_IP";
	public static final String GAPURA_CID						= "GAPURA_CID";
	public static final String GAPURA_IP						= "GAPURA_IP";
	public static final String GAPURA_PORT						= "GAPURA_PORT";
	public static final String GSS_IP							= "GSS_IP";
	public static final String GSS_PORT							= "GSS_PORT";
	
	//FAST
	public static final String FAST_FOUND						= "Valid/benar";
	public static final String FAST_SALAH_NAMA					= "Salah Nama";
	public static final String FAST_SALAH_NOMOR					= "Salah Nomor";
	public static final String FAST_NOT_FOUND					= "Tidak ditemukan";
	public static final String FAST_REK_DORMANT					= "Rekening Tutup/tidak aktif";
	
	public static final String APPROVAL_STATUS_PENDING_CREATE 	= "Pending - Create";
	public static final String APPROVAL_STATUS_PENDING_EDIT 	= "Pending - Edit";
	public static final String APPROVAL_STATUS_PENDING_DELETE 	= "Pending - Delete";
	public static final String APPROVAL_STATUS_APPROVED_CREATE	= "Approved - Create";
	public static final String APPROVAL_STATUS_APPROVED_EDIT	= "Approved - Edit";
	public static final String APPROVAL_STATUS_REJECTED_CREATE 	= "Rejected - Create";
	public static final String APPROVAL_STATUS_REJECTED_EDIT 	= "Rejected - Edit";
	public static final String APPROVAL_STATUS_REJECTED_DELETE 	= "Rejected - Delete";
	
	public static final String APPROVAL_ACTION_CREATE			= "Create";
	public static final String APPROVAL_ACTION_EDIT				= "Edit";
	public static final String APPROVAL_ACTION_DELETE			= "Delete";
	
	public static final String APPROVAL_PARAMETER_USER			= "User";
	public static final String APPROVAL_PARAMETER_ROLE			= "Role";
	public static final String APPROVAL_PARAMETER_SCHEDULER		= "Scheduler";
	public static final String APPROVAL_PARAMETER_SPARAM		= "System Parameter";
	public static final String APPROVAL_PARAMETER_BRANCH		= "Branch";
	public static final String APPROVAL_PARAMETER_HOLIDAY		= "Holiday";
	
	public final static String SCHEDULER_RUN_ONCE 				= "One Time Task";
	public final static String SCHEDULER_REPEATING				= "Repeat Interval";
	public final static String SCHEDULER_COMPLEX_REPEATING 		= "Repeat Time";

	public final static String SCHEDULER_STATUS_SHUTDOWN		= "Shutdown";
	public final static String SCHEDULER_STATUS_STARTED 		= "Started";
	public final static String SCHEDULER_STATUS_STANDBY 		= "Standby";
	
	public final static String PAYMENT_RESPONSE_CODE_SUCCESS	= "00";
	public final static String PAYMENT_RESPONSE_MESSAGE_SUCCESS	= "Success";
	public final static String PAYMENT_RESPONSE_CODE_REJECT		= "B5";
	public final static String PAYMENT_RESPONSE_MESSAGE_REJECT 	= "Kode billing sudah dibayar";
	public final static String PAYMENT_RESPONSE_CODE_TIMEOUT	= "TO";
	public final static String PAYMENT_RESPONSE_CODE_TIMEOUT_MPN= "90";
	public final static String PAYMENT_RESPONSE_CODE_M3			= "M3";
	public final static String PAYMENT_RESPONSE_MESSAGE_TIMEOUT	= "Timeout";
	public final static String PAYMENT_RESPONSE_MESSAGE_TIMEOUT_MDW	= "No response from middleware";
	public final static String PAYMENT_REKENING_GL				= "REKENING_TITIPAN_TRANSAKSI";
	public final static String PAYMENT_KODE_BANK_PERSEPSI		= "KODE_BANK_PERSEPSI";
	public final static String PAYMENT_CHANNEL_TYPE				= "PAYMENT_CHANNEL_TYPE";
	public final static String PAYMENT_CURRENCY_IDR				= "IDR";
	public final static String PAYMENT_ROLE_ID_SUPERVISOR		= "PAYMENT_ROLE_ID_SUPERVISOR";
	public final static String PAYMENT_REPORT_DIR				= "PAYMENT_REPORT_DIR";
	public final static String PAYMENT_REPORT_DB_URL			= "PAYMENT_REPORT_DB_URL";
	public final static String PAYMENT_REPORT_DB_USERNAME		= "PAYMENT_REPORT_DB_USERNAME";
	public final static String PAYMENT_REPORT_DB_PASSWORD		= "PAYMENT_REPORT_DB_PASSWORD";
	
	public final static String TRANSACTION_EVENT_PAYMENT		= "PAYMENT";	
	public final static String TRANSACTION_EVENT_INQUIRY		= "INQUIRY";	
	public final static String TRANSACTION_EVENT_REINQUIRY		= "REINQUIRY";
	
	public final static String TRANSACTION_FEE_AMOUNT			= "TRANSACTION_FEE_AMOUNT";

	public final static String PAYMENT_REQUEST_TYPE				= "PAYMENT_REQUEST_TYPE";
	public final static String INQUIRY_REQUEST_TYPE				= "INQUIRY_REQUEST_TYPE";

	public final static String REPORT_BPN_DJP_INQUIRY			= "BPN_DJP_Inquiry";
	public final static String REPORT_BPN_DJA_INQUIRY			= "BPN_DJA_Inquiry";
	public final static String REPORT_BPN_DJBC_INQUIRY			= "BPN_DJBC_Inquiry";
	
	public final static String REPORT_BPN_DJP					= "BPN_DJP";
	public final static String REPORT_BPN_DJA					= "BPN_DJA";
	public final static String REPORT_BPN_DJBC					= "BPN_DJBC";
	
	public final static String REPORT_BPN_DJP_SEMENTARA			= "BPN_DJP_Sementara";
	public final static String REPORT_BPN_DJA_SEMENTARA			= "BPN_DJA_Sementara";
	public final static String REPORT_BPN_DJBC_SEMENTARA		= "BPN_DJBC_Sementara";
	
	public final static String BPS_TARGET_DIRECTORY				= "BPS Target Directory";
	
	public final static String APPROVAL_STATUS_CODE_SUCCESS	    = "0";
	public final static String APPROVAL_STATUS_CODE_FAILED	    = "1";
	
	public final static String DEFAULT_ROLE_ID_UPLOAD_USER		= "DEFAULT_ROLE_ID_UPLOAD_USER";

	public final static String WEB_SERVICE_IP				    = "WEB_SERVICE_IP";
	public final static String WEB_SERVICE_PORT				    = "WEB_SERVICE_PORT";
	public final static String WEB_SERVICE_USERNAME			    = "WEB_SERVICE_USERNAME";
	public final static String WEB_SERVICE_PASSWORD			    = "WEB_SERVICE_PASSWORD";
	
	public final static String WEB_SERVICE_INTERNAL_IP			= "WEB_SERVICE_INTERNAL_IP";
	public final static String WEB_SERVICE_INTERNAL_PORT		= "WEB_SERVICE_INTERNAL_PORT";
	
	public final static String LDAP_ADDRESS_URL					= "LDAP_ADDRESS_URL";
	public final static String LDAP_SEARCH_BASE					= "LDAP_SEARCH_BASE";
	public final static String LDAP_SEARCH_ATTRIBUTE			= "LDAP_SEARCH_ATTRIBUTE";
	public final static String LDAP_DOMAIN						= "LDAP_DOMAIN";
	
	public final static String KODE_CABANG_KANTOR_PUSAT			= "KANTOR_PUSAT";
	
	public final static String ROLE_ID_TELLER					= "ROLE_ID_TELLER";
	public final static String ROLE_ID_TELLER_SUPERVISOR		= "ROLE_ID_TELLER_SUPERVISOR";
	public final static String ROLE_ID_DEPKEU					= "ROLE_ID_DEPKEU";
	public final static String ROLE_ID_BO						= "ROLE_ID_BO";
	
	public final static int ADD_MINUTES_SCHEDULER				= 3;
	
	public final static String PARAM_DEBITACCNO					= "DEBIT_ACCOUNT_NO_";
	public final static String GAJI 							= "GAJI";
	public final static String GAJIR 							= "GAJIR";
	public final static String ACK								= "ACK";
	public final static String BS								= "BS";
	public final static String SORBOR							= "SORBOR";
	
	public final static String EMAILWAIT						= "EMAILWAIT";
	public final static String EMAILAPPROVED					= "EMAILAPPROVED";
	public static final String ERROR = "Error";
	public final static String FISTLOGINYES						= "Y";
	public final static String FISTLOGINNO						= "N";
	public final static String FISTLOGINCHANGE					= "C";
	
	public final static String VOIDFLAG							= "0";
	public final static String VOIDDOCNO_INVOKE_TYPE_PARAM		= "VOIDDOCNO_INVOKE_TYPE_PARAM";
	
	public final static int MAXAPPROVELATEFILE					= 100;
	
	public static final String SORBOREXT = ".SBR";
	
	//BS
	public static final String KEYSTORE_FILE_PATH_BS = "KEYSTORE_FILE_PATH_BS";
	public static final String PRIVATE_KEY_USER_BS = "PRIVATE_KEY_USER_BS";
	public static final String LIMIT_ROW_BANK_STATEMEMT	= "LIMIT_ROW_BANK_STATEMEMT";
	public static final String XML_BANK_STATEMENT_PATH = "XML_BANK_STATEMENT_PATH";
	public static final String JAR_BANK_STATEMENT_PATH = "JAR_BANK_STATEMENT_PATH";
	public static final String KEYSTORE_PASS_BS = "KEYSTORE_PASS_BS";
	public static final String PRIVATE_KEY_PASS_BS = "PRIVATE_KEY_PASS_BS";
	public static final String KODE_BANK_12_DIGIT = "KODE_BANK_12_DIGIT";
//	public static final String START_DATE_TH_BS = "START_DATE_TH_BS";
//	public static final String START_DATE_TH_BS_PATTERN = "START_DATE_TH_BS_PATTERN";
	public static final String DYNAMIC_BS_DATE = "DYNAMIC_BS_DATE";
	
	public static final String DEBIT_CODE = "D";
	public static final String CREDIT_CODE = "C";
	
	public static final String DEFAULT_TRANSACTION_CODE_CREDIT = "519";
	public static final String DEFAULT_TRANSACTION_CODE_DROPPING = "896";
	public static final String DEFAULT_TRANSACTION_CODE_DEBIT = "019";
	public static final String DEFAULT_TRANSACTION_CODE_PENIHILAN = "017";
	
	public static final String LINE_AREA_ROW = "LINE_AREA_ROW";
	public static final String TOTAL_AMOUNT_CREDIT = "TOTAL_AMOUNT_CREDIT";
	public static final String TOTAL_ROW_PER_BS = "TOTAL_ROW_PER_BS";
	
	public static final String SPAN_REKENING_PENAMPUNG_RTGS = "SPAN_REKENING_PENAMPUNG_RTGS";
	public static final String SPAN_NAMA_REKENING_PENAMPUNG_RTGS = "SPAN_NAMA_REKENING_PENAMPUNG_RTGS";
	public static final String SPAN_CUT_OFF_TIME = "SPAN_CUT_OFF_TIME";
	public static final String SALAH_NO_REKENING = "95";
	public static final String SALAH_NAMA = "99";
	public static final String GAPURA_TIMEOUT = "97";
	public static final String RC_VOID = "78";
	public static final String MSG_VOID = "VOID";
	
	//EXTRACT SP2D
	public static String SPAN_LIMIT_RTGS = "SPAN_LIMIT_RTGS";
	public static String SPAN_LIMIT_SKN = "SPAN_LIMIT_SKN";
	
	public static String JAR_BANK_STATEMENT_PATH_BACKUP = "JAR_BANK_STATEMENT_PATH_BACKUP";
	public static String SPAN_DEPKEU_SFTP_PUT_BS_REMOTE_DIR = "SPAN_DEPKEU_SFTP_PUT_BS_REMOTE_DIR";
	
	
	//GET_SPAN_SUMMARIES
	public final static String DATESTART_PART1					= "and b.create_date>=to_date('";
	public final static String DATESTART_PART2					= " 00:00:00', 'yyyyMMdd HH24:MI:SS')";
	public final static String DATEEND_PART1					= "and b.create_date<=to_date('";
	public final static String DATEEND_PART2					= " 23:59:59', 'yyyyMMdd HH24:MI:SS')";
	
	// ISO FIELD PARAM
	public final static String MERCHANT_TYPE					= "ISO_MERCHANT_TYPE";
	public final static String PROCESSING_CODE					= "ISO_PROCESSING_CODE";
	public final static String PROCESSING_CODE_RETRY			= "ISO_PROCESSING_CODE_RETRY";
	public final static String POS_ENTRY_MODE 					= "ISO_POS_ENTRY_MODE";
	public final static String ACQUIRING_CODE 					= "ISO_ACQUIRING_IDENTIFICATION_CODE";
	public final static String FORWARDING_CODE 					= "ISO_FORWARDING_IDENTIFICATION_CODE";
	public final static String ACCEPTOR_IDENTIFICATION			= "ISO_ACCEPTOR_IDENTIFICATION";
	public final static String ACCEPTOR_CODE 					= "ISO_ACCEPTOR_CODE";
	public final static String ACCEPTOR_NAME 					= "ISO_ACCEPTOR_NAME";
	public final static String NARASI1 							= "ISO_NARASI1";
	public final static String NARASI2 							= "ISO_NARASI2";
	public final static String NARASI3 							= "ISO_NARASI3";
	public final static String CURRENCY_CODE					= "ISO_CURRENCY_CODE";
	public final static String SOURCE_ACC_NUMBER				= "ISO_SOURCE_ACC_NUMBER";
	public final static String ISO_IP_SERVER					= "ISO_IP_SERVER";
	public final static String ISO_PORT_SERVER					= "ISO_PORT_SERVER";
	public final static String ISO_ECHO_INTERVAL				= "ISO_ECHO_INTERVAL";
	public final static String ISO_CONNECT_INTERVAL				= "ISO_CONNECT_INTERVAL";
	public final static String ISO_TIMEOUT_LIMIT				= "ISO_TIMEOUT_LIMIT";
	public static final String ISO_PRC_LIMIT 					= "ISO_PRC_LIMIT";
	public static final String ISO_IF400_SUCCESS_CODE 			= "00";
	public static final String ISO_LOGGING			 			= "ISO_LOGGING";
	
	//SCHEDULLER RUN FLAG
	public static final String SCH_RUN_FLAG_SPANDATAVALIDATION 			= "SCH_RUN_FLAG_SPANDATAVALIDATION";
	public static final String SCH_RUN_FLAG_EXECUTETRANSACTION 			= "SCH_RUN_FLAG_EXECUTETRANSACTION";
	public static final String SCH_RUN_FLAG_RESPONSETRANSACTION 		= "SCH_RUN_FLAG_RESPONSETRANSACTION";
	public static final String SCH_RUN_FLAG_REPORTTRANSACTION 			= "SCH_RUN_FLAG_REPORTTRANSACTION";
	public static final String SCH_RUN_FLAG_CREATEBANKSTATEMENT 		= "SCH_RUN_FLAG_CREATEBANKSTATEMENT";
	public static final String SCH_RUN_FLAG_SENDBANKSTATEMENT 			= "SCH_RUN_FLAG_SENDBANKSTATEMENT";
	public static final String SCH_RUN_FLAG_PENIHILAN		 			= "SCH_RUN_FLAG_PENIHILAN";
	public static final String SCH_RUN_FLAG_AUTO_RTBP		 			= "SCH_RUN_FLAG_AUTO_RTBP";
	public static final String SCH_RUN_FLAG_ACK_LISTER		 			= "SCH_RUN_FLAG_ACK_LISTER";
	public static final String SCH_RUN_FLAG_AUTO_OB_SORBOR				= "SCH_RUN_FLAG_AUTO_OB_SORBOR";
	public static final String SCH_RUN_FLAG_GET_REKENING_KORAN_DATA		= "SCH_RUN_FLAG_GET_REKENING_KORAN_DATA";
	public static final String SCH_START_FLAG_SPANDATAVALIDATION		= "SCH_START_FLAG_SPANDATAVALIDATION";
	public static final String SCH_START_FLAG_EXECUTETRANSACTION 		= "SCH_START_FLAG_EXECUTETRANSACTION";
	public static final String SCH_START_FLAG_RESPONSETRANSACTION 		= "SCH_START_FLAG_RESPONSETRANSACTION";
	public static final String SCH_START_FLAG_REPORTTRANSACTION 		= "SCH_START_FLAG_REPORTTRANSACTION";
	public static final String SCH_START_FLAG_SENDBANKSTATEMENT 		= "SCH_START_FLAG_SENDBANKSTATEMENT";
	public static final String SCH_START_FLAG_AUTO_RTBP 				= "SCH_START_FLAG_AUTO_RTBP";
	public static final String SCH_START_FLAG_ACK_LISTER 				= "SCH_START_FLAG_ACK_LISTER";
	public static final String SCH_START_FLAG_AUTO_OB_SORBOR			= "SCH_START_FLAG_AUTO_OB_SORBOR";
	public static final String SCH_START_FLAG_GET_REKENING_KORAN_DATA	= "SCH_START_FLAG_GET_REKENING_KORAN_DATA";
	public static final String SCH_END_FLAG_SPANDATAVALIDATION			= "SCH_END_FLAG_SPANDATAVALIDATION";
	public static final String SCH_END_FLAG_EXECUTETRANSACTION 			= "SCH_END_FLAG_EXECUTETRANSACTION";
	public static final String SCH_END_FLAG_RESPONSETRANSACTION 		= "SCH_END_FLAG_RESPONSETRANSACTION";
	public static final String SCH_END_FLAG_REPORTTRANSACTION 			= "SCH_END_FLAG_REPORTTRANSACTION";
	public static final String SCH_END_FLAG_SENDBANKSTATEMENT 			= "SCH_END_FLAG_SENDBANKSTATEMENT";
	public static final String SCH_END_FLAG_PENIHILAN		 			= "SCH_END_FLAG_PENIHILAN";
	public static final String SCH_END_FLAG_AUTO_RTBP		 			= "SCH_END_FLAG_AUTO_RTBP";
	public static final String SCH_END_FLAG_ACK_LISTER		 			= "SCH_END_FLAG_ACK_LISTER";
	public static final String SCH_END_FLAG_AUTO_OB_SORBOR				= "SCH_END_FLAG_AUTO_OB_SORBOR";
	public static final String SCH_END_FLAG_GET_REKENING_KORAN_DATA		= "SCH_END_FLAG_GET_REKENING_KORAN_DATA";
	public static final String SPAN_CREATE_FILE_REJECT 					= "SPAN_CREATE_FILE_REJECT";
	
	//SCHEDULLER BS FLAGGER
	public static final String BS_HLP_RUN_FLAG 					= "BS_HLP_RUN_FLAG";
	public static final String BS_HLP_RUN_START 				= "BS_HLP_RUN_START";
	public static final String BS_HLP_RUN_END	 				= "BS_HLP_RUN_END";
	public static final String BS_HLP_HOUR_FORMAT				= "HHmmss";
	public static final String DEBIT_ACCOUNT_NO_GAJIR_NAME = "DEBIT_ACCOUNT_NO_GAJIR_NAME";
	//SCHEDULLER ACK VOID FLAGGER
	public static final String ACK_VOID_HLP_RUN_FLAG 				= "ACK_VOID_HLP_RUN_FLAG";
	public static final String ACK_VOID_HLP_RUN_START 				= "ACK_VOID_HLP_RUN_START";
	public static final String ACK_VOID_HLP_RUN_END	 				= "ACK_VOID_HLP_RUN_END";
	public static final String ACK_VOID_HLP_HOUR_FORMAT				= "HHmmss";
	
	//scheduller resultToBeProcess
	public static final String RETUR = "RETUR";
	public static final String RETRY = "RETRY";
	public static final String SEND_ACK = "SEND_ACK";
	
	public static final String GAPURA_CONNECTION_TIMEOUT = "GAPURA_TIMEOUT. Timeout when connecting to GAPURA, Please Try Again.";
	public static final String GAPURA_RC_SUCCESS = "0000";
	public static final String GAPURA_RC_NOT_FOUND = "0004";
	
	//FTP
	public static final String SPAN_CREATE_SORBOR_PATH = "FTP_SORBOR_FILE"; // /opt/test/ftp/sorbor/
	public static final String SPAN_DEPKEU_SFTP_IPADDRESS = "SPAN_DEPKEU_SFTP_IPADDRESS";
	public static final String SPAN_DEPKEU_SFTP_USERNAME = "SPAN_DEPKEU_SFTP_USERNAME";
	public static final String SPAN_DEPKEU_SFTP_PASSWORD = "SPAN_DEPKEU_SFTP_PASSWORD";
	public static final String SPAN_DEPKEU_SFTP_PUT_SORBOR_REMOTE_DIR = "SPAN_DEPKEU_SFTP_PUT_SORBOR_REMOTE_DIR";
	public static final String JAR_BACKUP_SORBOR = "JAR_BACKUP_SORBOR";
	
	//BANK_ATTRIBUTE
	public static final String SPAN_BANK_NAME = "BANK_NAME";
	public static final String SPAN_BANK_CODE = "BANK_CODE";
	
	//SCHEDULLER SORBOR
	public static final String LIMIT_GENERATE_SORBOR = "LIMIT_GENERATE_SORBOR";
	
	//Scheduler getTHEQ
	public static final String LIMIT_GET_TH			= "LIMIT_GET_TH";
	
	//START END INDEX FOR NAME VALIDATION
	public static final int START_INDEX_NAME_VALIDATION 	= 0;
	public static final int END_INDEX_NAME_VALIDATION 		= 35;
}
