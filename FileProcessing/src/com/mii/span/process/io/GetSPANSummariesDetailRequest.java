package com.mii.span.process.io;

public class GetSPANSummariesDetailRequest {
	private String filename;
	private String clickOn;
	private String searchBy;
	private String params;
	private String legStatus;
	
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getClickOn() {
		return clickOn;
	}
	public void setClickOn(String clickOn) {
		this.clickOn = clickOn;
	}
	public String getSearchBy() {
		return searchBy;
	}
	public void setSearchBy(String searchBy) {
		this.searchBy = searchBy;
	}
	public String getParams() {
		return params;
	}
	public void setParams(String params) {
		this.params = params;
	}
	public String getLegStatus() {
		return legStatus;
	}
	public void setLegStatus(String legStatus) {
		this.legStatus = legStatus;
	}
}
