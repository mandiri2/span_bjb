package com.mii.span.process.io.helper;

import java.io.Serializable;

public class DetailsOutputList implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String errorCode;
	private String errorMessage;
	private String creditAcctNo;
	private String creditAcctName;
	private String benefBankName;
	private String spanType;
	private String debitAccount;
	private String docDate;
	private String debitAccountType;
	private String debitAmount;
	private String remittanceNo;
	private String trxDetailID;
	private String docNumber;
	
	private boolean selected;
	
	public DetailsOutputList(String errorCode, String errorMessage,
			String creditAcctNo, String creditAcctName, String benefBankName,
			String spanType, String debitAccount, String docDate,
			String debitAccountType, String debitAmount, String remittanceNo,
			String trxDetailID, String docNumber) {
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.creditAcctNo = creditAcctNo;
		this.creditAcctName = creditAcctName;
		this.benefBankName = benefBankName;
		this.spanType = spanType;
		this.debitAccount = debitAccount;
		this.docDate = docDate;
		this.debitAccountType = debitAccountType;
		this.debitAmount = debitAmount;
		this.remittanceNo = remittanceNo;
		this.trxDetailID = trxDetailID;
		this.docNumber = docNumber;
	}
	public DetailsOutputList() {
		// TODO Auto-generated constructor stub
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public String getCreditAcctNo() {
		return creditAcctNo;
	}
	public void setCreditAcctNo(String creditAcctNo) {
		this.creditAcctNo = creditAcctNo;
	}
	public String getCreditAcctName() {
		return creditAcctName;
	}
	public void setCreditAcctName(String creditAcctName) {
		this.creditAcctName = creditAcctName;
	}
	public String getBenefBankName() {
		return benefBankName;
	}
	public void setBenefBankName(String benefBankName) {
		this.benefBankName = benefBankName;
	}
	public String getSpanType() {
		return spanType;
	}
	public void setSpanType(String spanType) {
		this.spanType = spanType;
	}
	public String getDebitAccount() {
		return debitAccount;
	}
	public void setDebitAccount(String debitAccount) {
		this.debitAccount = debitAccount;
	}
	public String getDocDate() {
		return docDate;
	}
	public void setDocDate(String docDate) {
		this.docDate = docDate;
	}
	public String getDebitAccountType() {
		return debitAccountType;
	}
	public void setDebitAccountType(String debitAccountType) {
		this.debitAccountType = debitAccountType;
	}
	public String getDebitAmount() {
		return debitAmount;
	}
	public void setDebitAmount(String debitAmount) {
		this.debitAmount = debitAmount;
	}
	public String getRemittanceNo() {
		return remittanceNo;
	}
	public void setRemittanceNo(String remittanceNo) {
		this.remittanceNo = remittanceNo;
	}
	public String getTrxDetailID() {
		return trxDetailID;
	}
	public void setTrxDetailID(String trxDetailID) {
		this.trxDetailID = trxDetailID;
	}
	public String getDocNumber() {
		return docNumber;
	}
	public void setDocNumber(String docNumber) {
		this.docNumber = docNumber;
	}
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
}
