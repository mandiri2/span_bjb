package com.mii.span.process.io.helper;

public class ReturCodes {

	private String returCode;
	private String errorMessage;

	public ReturCodes(String returCode, String errorMessage) {
		this.returCode = returCode;
		this.errorMessage = errorMessage;
	}

	public String getReturCode() {
		return returCode;
	}

	public void setReturCode(String returCode) {
		this.returCode = returCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

}
