package com.mii.span.process.io.helper;

import java.io.Serializable;

public class DownloadErrorResponse implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String filename,
		batchid,
		trxDetailId,
		amount,
		creditAccNo,
		creditAccName,
		errorCode,
		errorMessage,
		paymentMethod,
		debitAccount,
		debitAccountType,
		benBankCode,
		docNumber,
		benBank,
		description;

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getBatchid() {
		return batchid;
	}

	public void setBatchid(String batchid) {
		this.batchid = batchid;
	}

	public String getTrxDetailId() {
		return trxDetailId;
	}

	public void setTrxDetailId(String trxDetailId) {
		this.trxDetailId = trxDetailId;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getCreditAccNo() {
		return creditAccNo;
	}

	public void setCreditAccNo(String creditAccNo) {
		this.creditAccNo = creditAccNo;
	}

	public String getCreditAccName() {
		return creditAccName;
	}

	public void setCreditAccName(String creditAccName) {
		this.creditAccName = creditAccName;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getDebitAccount() {
		return debitAccount;
	}

	public void setDebitAccount(String debitAccount) {
		this.debitAccount = debitAccount;
	}

	public String getDebitAccountType() {
		return debitAccountType;
	}

	public void setDebitAccountType(String debitAccountType) {
		this.debitAccountType = debitAccountType;
	}

	public String getBenBankCode() {
		return benBankCode;
	}

	public void setBenBankCode(String benBankCode) {
		this.benBankCode = benBankCode;
	}

	public String getDocNumber() {
		return docNumber;
	}

	public void setDocNumber(String docNumber) {
		this.docNumber = docNumber;
	}

	public String getBenBank() {
		return benBank;
	}

	public void setBenBank(String benBank) {
		this.benBank = benBank;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
