package com.mii.span.process.io;

import java.util.List;

import com.mii.span.process.io.helper.Files;

public class ForceReturInput {
	private List<Files> files;
	private String legStatus;
	private String providerCode;

	public List<Files> getFiles() {
		return files;
	}

	public void setFiles(List<Files> files) {
		this.files = files;
	}

	public String getLegStatus() {
		return legStatus;
	}

	public void setLegStatus(String legStatus) {
		this.legStatus = legStatus;
	}

	public String getProviderCode() {
		return providerCode;
	}

	public void setProviderCode(String providerCode) {
		this.providerCode = providerCode;
	}

}
