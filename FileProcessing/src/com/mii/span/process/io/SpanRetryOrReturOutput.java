package com.mii.span.process.io;

public class SpanRetryOrReturOutput {
	private String oStatus;
	private String oErrorMessage;
	
	public SpanRetryOrReturOutput(String oStatus, String oErrorMessage) {
		super();
		this.oStatus = oStatus;
		this.oErrorMessage = oErrorMessage;
	}
	
	public SpanRetryOrReturOutput() {
	}

	public String getoStatus() {
		return oStatus;
	}
	public void setoStatus(String oStatus) {
		this.oStatus = oStatus;
	}
	public String getoErrorMessage() {
		return oErrorMessage;
	}
	public void setoErrorMessage(String oErrorMessage) {
		this.oErrorMessage = oErrorMessage;
	}
	
}
