package com.mii.span.process.io;

import java.util.List;

import com.mii.span.process.io.helper.Files;

public class ToBeProcessInput {
	private List<Files> files;
	private String typeProcess;
	private String returType;
	private String accountRetur;
	private String legStatus;
	private String exeType;
	private String providerCode;

	public List<Files> getFiles() {
		return files;
	}

	public void setFiles(List<Files> files) {
		this.files = files;
	}

	public String getTypeProcess() {
		return typeProcess;
	}

	public void setTypeProcess(String typeProcess) {
		this.typeProcess = typeProcess;
	}

	public String getReturType() {
		return returType;
	}

	public void setReturType(String returType) {
		this.returType = returType;
	}

	public String getAccountRetur() {
		return accountRetur;
	}

	public void setAccountRetur(String accountRetur) {
		this.accountRetur = accountRetur;
	}

	public String getLegStatus() {
		return legStatus;
	}

	public void setLegStatus(String legStatus) {
		this.legStatus = legStatus;
	}

	public String getExeType() {
		return exeType;
	}

	public void setExeType(String exeType) {
		this.exeType = exeType;
	}

	public String getProviderCode() {
		return providerCode;
	}

	public void setProviderCode(String providerCode) {
		this.providerCode = providerCode;
	}

}
