package com.mii.span.process.io;

public class GetSPANSummariesRequest {
	private String providerCode;
	private String sumType;
	private String status;
	private String dateTimeStart;
	private String dateTimeEnd;
	private String notification;
	private String legStatusInput;

	public String getProviderCode() {
		return providerCode;
	}
	public void setProviderCode(String providerCode) {
		this.providerCode = providerCode;
	}
	public String getSumType() {
		return sumType;
	}
	public void setSumType(String sumType) {
		this.sumType = sumType;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDateTimeStart() {
		return dateTimeStart;
	}
	public void setDateTimeStart(String dateTimeStart) {
		this.dateTimeStart = dateTimeStart;
	}
	public String getDateTimeEnd() {
		return dateTimeEnd;
	}
	public void setDateTimeEnd(String dateTimeEnd) {
		this.dateTimeEnd = dateTimeEnd;
	}
	public String getNotification() {
		return notification;
	}
	public void setNotification(String notification) {
		this.notification = notification;
	}
	public String getLegStatusInput() {
		return legStatusInput;
	}
	public void setLegStatusInput(String legStatusInput) {
		this.legStatusInput = legStatusInput;
	}
}
