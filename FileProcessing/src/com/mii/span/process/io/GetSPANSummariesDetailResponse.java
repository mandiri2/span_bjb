package com.mii.span.process.io;

import java.util.List;

import com.mii.span.process.io.helper.DetailsOutputList;
import com.mii.span.process.io.helper.SPANSummaries;

public class GetSPANSummariesDetailResponse {
	private List<DetailsOutputList> detailsOutputList;
	private List<SPANSummaries> searchByList;
	private String totalAmount;
	
	public String getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}
	public List<SPANSummaries> getSearchByList() {
		return searchByList;
	}
	public void setSearchByList(List<SPANSummaries> searchByList) {
		this.searchByList = searchByList;
	}
	public List<DetailsOutputList> getDetailsOutputList() {
		return detailsOutputList;
	}
	public void setDetailsOutputList(List<DetailsOutputList> detailsOutputList) {
		this.detailsOutputList = detailsOutputList;
	}
	
}
