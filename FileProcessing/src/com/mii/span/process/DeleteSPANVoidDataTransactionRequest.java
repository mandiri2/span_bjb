package com.mii.span.process;

public class DeleteSPANVoidDataTransactionRequest {
	private String fileName;
	private String documentNo;
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getDocumentNo() {
		return documentNo;
	}
	public void setDocumentNo(String documentNo) {
		this.documentNo = documentNo;
	}
}
