package com.mii.span.process;

import java.util.List;

import mii.span.doc.DataArea;
import mii.span.model.SpanVoidDataTrx;
import mii.span.test.SP2DXmltoObject;

import com.mii.constant.Constants;
import com.mii.dao.SpanVoidDataTrxDao;
import com.mii.dao.SystemParameterDAO;
import com.mii.models.SystemParameter;
import com.mii.span.dao.SPANDataValidationDao;
import com.mii.span.dao.SPANHostDataDetailsDao;
import com.mii.span.model.SPANDataValidation;
import com.mii.span.model.SpanSp2dVoidLog;

public class VoidDocumentNo {
	static String status = "";
	static String errormsg = "";
	
	public static void VoidDocumentNoProcess(VoidDocumentNumberRequest voidDocumentNumberRequest, SPANDataValidationDao spanDataValidationDao, 
			SystemParameterDAO systemParameterDao, SpanVoidDataTrxDao spanVoidDataTrxDao, SPANHostDataDetailsDao spanHostDataDetailsDao){
		String value = "";
		if(voidDocumentNumberRequest.getInvokeType().equals("1")){
			value = "1";
		}else{
			SystemParameter parameter = systemParameterDao.getDetailedParameterByParamName(Constants.VOIDDOCNO_INVOKE_TYPE_PARAM);
			value = parameter.getParam_value();
		}
		if(value.equals("1")){
			for(int i=0; i<voidDocumentNumberRequest.getVoidDocumentNumberVFilesList().size(); i++){
				SpanVoidDataTrx spanVoidDataTrx;
//				String proc_status = "";
				boolean updateSPANDataValidation = false;
//				String firstVoidFileAction = "0";
//				String validationIDIterate = "0";
				int recordOri = 0;
				int amountOri = 0;
				boolean voidFileFlag = false;
				boolean batchUpdateFlag = false;
				String batchSP2DNo = "XXX";
				if(voidDocumentNumberRequest.getVoidDocumentNumberVFilesList().get(i).getFileName().equals("")||
						voidDocumentNumberRequest.getVoidDocumentNumberVFilesList().get(i).getFileName()==null){
					if(voidDocumentNumberRequest.getVoidDocumentNumberVFilesList().size()>0){
						for(int j=0; j < voidDocumentNumberRequest.getVoidDocumentNumberVFilesList().get(i).getDocumentNoList().size(); j++){
//							int documentNOLength = voidDocumentNumberRequest.getVoidDocumentNumberVFilesList().get(i).getDocumentNoList().get(j).length();
							String fileName = selectSPANDataValidationSP2DNo(spanDataValidationDao, voidDocumentNumberRequest.getVoidDocumentNumberVFilesList().get(i).getDocumentNoList().get(j)); //Query mandiri.span.db.service:selectSPANDataValidationSP2DNo, procStatus = '1'
							if(!fileName.equals("")||fileName!=null){
								SPANDataValidation spanDataValidation = selectSPANDataValidation(fileName, spanDataValidationDao);
								List<DataArea> dataAreaList = SP2DXmltoObject.ConvertSP2DXMLToObject(spanDataValidation.getXmlFileName());
								
								recordOri = Integer.parseInt(spanDataValidation.getTotalRecord());
								amountOri = Integer.parseInt(spanDataValidation.getTotalAmount());
								for(DataArea dataArea : dataAreaList){
									if(dataArea.getDocumentNumber().length()>15){
										if(voidDocumentNumberRequest.getVoidDocumentNumberVFilesList().get(i).getDocumentNoList().get(j).equalsIgnoreCase
												(dataArea.getDocumentNumber())){
											updateSPANDataValidation = true;
											recordOri--;
											amountOri = amountOri - Integer.parseInt(dataArea.getAmount());
											spanVoidDataTrx = constructSpanVoidDataTrx(dataArea, voidDocumentNumberRequest.getVoidDocumentNumberVFilesList().get(i).getFileName());
											InsertSPANDataVoidTransaction(spanVoidDataTrx, spanVoidDataTrxDao);
											UpdateSPANHostDataDetails(spanHostDataDetailsDao,  voidDocumentNumberRequest.getVoidDocumentNumberVFilesList().get(i).getDocumentNoList().get(j));
										}else{
											//DO NOTHING
										}
									}else{
										String sp2dno = dataArea.getDocumentNumber().substring(0, 15);
										if(sp2dno.equals(voidDocumentNumberRequest.getVoidDocumentNumberVFilesList().get(i).getDocumentNoList().get(j))){
											updateSPANDataValidation = true;
											recordOri--;
											amountOri = amountOri - Integer.parseInt(dataArea.getAmount());
											spanVoidDataTrx = constructSpanVoidDataTrx(dataArea, voidDocumentNumberRequest.getVoidDocumentNumberVFilesList().get(i).getFileName());
											InsertSPANDataVoidTransaction(spanVoidDataTrx, spanVoidDataTrxDao);
//											UpdateSPANHostDataDetails(spanHostDataDetailsDao,  voidDocumentNumberRequest.getVoidDocumentNumberVFilesList().get(i).getDocumentNoList().get(j));
											batchSP2DNo = voidDocumentNumberRequest.getVoidDocumentNumberVFilesList().get(i).getDocumentNoList().get(j);
											batchUpdateFlag = true;
										}
									}
								}
							}
						}
					}
				}else{
					SPANDataValidation spanDataValidation = selectSPANDataValidation(voidDocumentNumberRequest.getVoidDocumentNumberVFilesList().get(i).getFileName(), spanDataValidationDao);
					try{
						recordOri = Integer.parseInt(spanDataValidation.getTotalRecord());
						amountOri = Integer.parseInt(spanDataValidation.getTotalAmount());
						if(spanDataValidation.getProcStatus().equals("1")){
							List<DataArea> dataAreaList = SP2DXmltoObject.ConvertSP2DXMLToObject(spanDataValidation.getXmlFileName());
							if(voidDocumentNumberRequest.getVoidDocumentNumberVFilesList().size()>0){
								for(int k=0; k<voidDocumentNumberRequest.getVoidDocumentNumberVFilesList().get(i).getDocumentNoList().size(); k++){
									int docNoLength = voidDocumentNumberRequest.getVoidDocumentNumberVFilesList().get(i).getDocumentNoList().get(k).length();
									for(DataArea dataArea : dataAreaList){
										if(docNoLength>15){
											if(dataArea.getDocumentNumber().equals(voidDocumentNumberRequest.getVoidDocumentNumberVFilesList().get(i).getDocumentNoList().get(k))){
												updateSPANDataValidation = true;
												recordOri--;
												amountOri = amountOri - Integer.parseInt(dataArea.getAmount());
												spanVoidDataTrx = constructSpanVoidDataTrx(dataArea, voidDocumentNumberRequest.getVoidDocumentNumberVFilesList().get(i).getFileName());
												InsertSPANDataVoidTransaction(spanVoidDataTrx, spanVoidDataTrxDao);
												UpdateSPANHostDataDetails(spanHostDataDetailsDao,  voidDocumentNumberRequest.getVoidDocumentNumberVFilesList().get(i).getDocumentNoList().get(k));
											}
										}else if(docNoLength==15){
											String sp2dno = dataArea.getDocumentNumber().substring(0, 15);
											if(sp2dno.equals(voidDocumentNumberRequest.getVoidDocumentNumberVFilesList().get(i).getDocumentNoList().get(k))){
												try{
													spanVoidDataTrx = constructSpanVoidDataTrx(dataArea, voidDocumentNumberRequest.getVoidDocumentNumberVFilesList().get(i).getFileName());
													InsertSPANDataVoidTransaction(spanVoidDataTrx, spanVoidDataTrxDao);
//													UpdateSPANHostDataDetails(spanHostDataDetailsDao,  voidDocumentNumberRequest.getVoidDocumentNumberVFilesList().get(i).getDocumentNoList().get(k));
													batchSP2DNo = voidDocumentNumberRequest.getVoidDocumentNumberVFilesList().get(i).getDocumentNoList().get(k);
													batchUpdateFlag = true;
													updateSPANDataValidation = true;
													recordOri--;
													amountOri = amountOri - Integer.parseInt(dataArea.getAmount());
												}catch(Exception e){
													System.out.println("Data area "+dataArea.getDocumentNumber()+" allready void.");
												}
											}
										}
									}
									if(dataAreaList.size()>1){
										if(recordOri==0){
											voidFileFlag = true; //VOID FILE | PROC_STATUS = 'V'
										}else{
											//doNothing
										}
									}else if(dataAreaList.size()==1){
										updateSPANDataValidation = true;
//										if(docNoLength>15){
//											if(dataAreaList.get(0).getDocumentNumber().equals(voidDocumentNumberRequest.getVoidDocumentNumberVFilesList().get(i).getDocumentNoList().get(k))){
//												recordOri--;
//												amountOri = amountOri - Integer.parseInt(dataAreaList.get(0).getAmount());
//												spanVoidDataTrx = constructSpanVoidDataTrx(dataAreaList.get(0), voidDocumentNumberRequest.getVoidDocumentNumberVFilesList().get(i).getFileName());
//												InsertSPANDataVoidTransaction(spanVoidDataTrx, spanVoidDataTrxDao);
//												UpdateSPANHostDataDetails(spanHostDataDetailsDao,  voidDocumentNumberRequest.getVoidDocumentNumberVFilesList().get(i).getDocumentNoList().get(k));
												voidFileFlag = true; //VOID FILE | PROC_STATUS = 'V'
//											}
//										}else if (docNoLength==15){
//											String sp2dno = dataAreaList.get(0).getDocumentNumber().substring(0, 15);
//											if(sp2dno.equals(voidDocumentNumberRequest.getVoidDocumentNumberVFilesList().get(i).getDocumentNoList().get(k))){
//												recordOri--;
//												amountOri = amountOri - Integer.parseInt(dataAreaList.get(0).getAmount());
//												spanVoidDataTrx = constructSpanVoidDataTrx(dataAreaList.get(0), voidDocumentNumberRequest.getVoidDocumentNumberVFilesList().get(i).getFileName());
//												InsertSPANDataVoidTransaction(spanVoidDataTrx, spanVoidDataTrxDao);
//												UpdateSPANHostDataDetails(spanHostDataDetailsDao, voidDocumentNumberRequest.getVoidDocumentNumberVFilesList().get(i).getDocumentNoList().get(k));
//												voidFileFlag = true; //VOID FILE | PROC_STATUS = 'V'
//											}
//										}
									}
								}
							}
							System.out.println("Void Success.");
						}else{
							status = "ERROR";
							errormsg = "Can't void this file, allready executed.";
							System.out.println("Void Failed.");
						}
					}catch(Exception e){
						status = "ERROR";
						errormsg = "Can't void this file, allready executed.";
						System.out.println("Void Failed.");
					}	
				}
				if(batchUpdateFlag){
					UpdateBulkSPANHostDataDetails(spanHostDataDetailsDao, batchSP2DNo);
				}
				if(updateSPANDataValidation){
					updateSPANDataValidation(recordOri, amountOri, voidDocumentNumberRequest.getVoidDocumentNumberVFilesList().get(i).getFileName(), 
							voidFileFlag, spanDataValidationDao); //UpdateSPANDataValidation
				}
			}
		}else{
			//DO Nothing
		}
		
	}

	private static String selectSPANDataValidationSP2DNo(SPANDataValidationDao spanDataValidationDao, String sp2dno) {
		sp2dno = "%"+sp2dno+"%";
		spanDataValidationDao.updateSpanDataValidationSp2dno(sp2dno);
		return null;
	}

	private static SpanVoidDataTrx constructSpanVoidDataTrx(DataArea dataArea, String fileName) {
		SpanVoidDataTrx spanVoidDataTrx = new SpanVoidDataTrx();
		spanVoidDataTrx.setFileName(fileName);
		spanVoidDataTrx.setDocumentNo(dataArea.getDocumentNumber());
		spanVoidDataTrx.setDocumentDate(dataArea.getDocumentDate());
		spanVoidDataTrx.setBeneficiaryName(dataArea.getBeneficiaryName());
		spanVoidDataTrx.setBeneficiaryBankCode(dataArea.getBeneficiaryBankCode());
		spanVoidDataTrx.setBeneficiaryBank(dataArea.getBeneficiaryBank());
		spanVoidDataTrx.setBeneficiaryAccount(dataArea.getBeneficiaryAccount());
		spanVoidDataTrx.setAmount(dataArea.getAmount());
		spanVoidDataTrx.setDescription(dataArea.getDescription());
		spanVoidDataTrx.setAgentBankCode(dataArea.getAgentBankCode());
		spanVoidDataTrx.setAgentBankAccountNo(dataArea.getAgentBankAccountNumber());
		spanVoidDataTrx.setAgentBankAccountName(dataArea.getAgentBankAccountName());
		spanVoidDataTrx.setPaymentMethod(dataArea.getPaymentMethod());
		spanVoidDataTrx.setSp2dCount(dataArea.getSP2DCount());
		return spanVoidDataTrx;
	}

	private static void updateSPANDataValidation(int recordOri, int amountOri, String fileName, boolean voidFileFlag, 
			SPANDataValidationDao spanDataValidationDao) {
		spanDataValidationDao.updateSpanDataValidationWhenVoid(recordOri, amountOri, fileName, voidFileFlag);
	}

	private static void UpdateSPANHostDataDetails(SPANHostDataDetailsDao spanHostDataDetailsDao, String documentNumber) {
		spanHostDataDetailsDao.updateVoidSpanHostDataDetails(documentNumber);
	}
	
	private static void UpdateBulkSPANHostDataDetails(SPANHostDataDetailsDao spanHostDataDetailsDao, String documentNumber) {
		spanHostDataDetailsDao.updateBulkVoidSpanHostDataDetails(documentNumber);
	}

	private static void InsertSPANDataVoidTransaction(SpanVoidDataTrx spanVoidDataTrx, SpanVoidDataTrxDao spanVoidDataTrxDao) {
		spanVoidDataTrxDao.InsertSPANDataVoidTransaction(spanVoidDataTrx);
	}

	private static SPANDataValidation selectSPANDataValidation(String fileName, SPANDataValidationDao spanDataValidationDao) {
		return spanDataValidationDao.getSPANDataValidationByFilename(fileName);
		
	}

	/*public static void insertSpanSp2dVoidLogs(
			List<SpanSp2dVoidLog> spanSp2dVoidLogs, SpanVoidDataTrxDao spanVoidDataTrxDao) {
		// TODO Auto-generated method stub
		spanVoidDataTrxDao.insertSpanSp2dVoidLogs(spanSp2dVoidLogs);
	}
	public static List<SpanSp2dVoidLog> viewSpanSp2dVoidLogs(String filenameFilter, String sp2dNoFilter, int min, int max,
			SpanVoidDataTrxDao spanVoidDataTrxDao) {
		// TODO Auto-generated method stub
		return spanVoidDataTrxDao.viewSpanSp2dVoidLogs(filenameFilter, sp2dNoFilter, min, max);
	}

	public static int countSpanSp2dVoidLogs(String filenameFilter, String sp2dNoFilter,
			SpanVoidDataTrxDao spanVoidDataTrxDao) {
		// TODO Auto-generated method stub
		return spanVoidDataTrxDao.countSpanSp2dVoidLogs(filenameFilter, sp2dNoFilter);
	}*/

	public static List<SpanSp2dVoidLog> checkSpanSp2dVoidLogsBySp2dNo(
			String sp2dNo, SpanVoidDataTrxDao spanVoidDataTrxDao) {
		// TODO Auto-generated method stub
		return spanVoidDataTrxDao.checkSpanSp2dVoidLogsBySp2dNo(sp2dNo);
	}
	
	public static List<SpanSp2dVoidLog> getAllSpanSp2dVoidLogs(
			SpanVoidDataTrxDao spanVoidDataTrxDao) {
		// TODO Auto-generated method stub
		return spanVoidDataTrxDao.getAllSpanSp2dVoidLogs();
	}
	
	public static List<String> getAllDocumentNumberDataDetailsByFilename(
			String fileName, SPANDataValidationDao spanDataValidationDao) {
		// TODO Auto-generated method stub
		return spanDataValidationDao.getAllDocumentNumberDataDetailsByFilename(fileName);
	}
}
