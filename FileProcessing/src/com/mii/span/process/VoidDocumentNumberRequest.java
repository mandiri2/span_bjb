package com.mii.span.process;

import java.util.List;

public class VoidDocumentNumberRequest {
	private String invokeType;
	private List<VoidDocumentNumberVFiles> VoidDocumentNumberVFilesList;
	
	public String getInvokeType() {
		return invokeType;
	}
	public void setInvokeType(String invokeType) {
		this.invokeType = invokeType;
	}
	public List<VoidDocumentNumberVFiles> getVoidDocumentNumberVFilesList() {
		return VoidDocumentNumberVFilesList;
	}
	public void setVoidDocumentNumberVFilesList(
			List<VoidDocumentNumberVFiles> voidDocumentNumberVFilesList) {
		VoidDocumentNumberVFilesList = voidDocumentNumberVFilesList;
	} 
}
