package com.mii.span.model;

import java.io.Serializable;

public class UserRole implements Serializable{
	private static final long serialVersionUID = 1L;
	private String userId;
	private String role;
	private String idx;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getIdx() {
		return idx;
	}
	public void setIdx(String idx) {
		this.idx = idx;
	}
	
}
