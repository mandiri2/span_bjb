package com.mii.span.model;

import java.io.Serializable;
import java.util.Date;

public class SorborAttr implements Serializable {
	private static final long serialVersionUID = 1L;
	private String bankCode,
				   docNo,
				   ftpFileName,
				   paymentMethods,
				   sorborNo,
				   checkAmount;
	private Date paymentDate,
				 sorborDate;
	public SorborAttr(){
		
	}
	public SorborAttr(
					  String docNo,
					  String ftpFileName,
					  String paymentMethods,
					  String sorborNo,
					  String checkAmount,
					  Date paymentDate,
					  Date sorborDate){

		
		this.docNo=docNo;
		this.ftpFileName=ftpFileName;
		this.paymentMethods=paymentMethods;
		this.sorborNo=sorborNo;
		this.setCheckAmount(checkAmount);
		this.paymentDate=paymentDate;
		this.sorborDate=sorborDate;
		
	}
	public String getBankCode() {
		return bankCode;
	}
	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}
	public String getDocNo() {
		return docNo;
	}
	public void setDocNo(String docNo) {
		this.docNo = docNo;
	}
	public String getFtpFileName() {
		return ftpFileName;
	}
	public void setFtpFileName(String ftpFileName) {
		this.ftpFileName = ftpFileName;
	}
	public String getPaymentMethods() {
		return paymentMethods;
	}
	public void setPaymentMethods(String paymentMethods) {
		this.paymentMethods = paymentMethods;
	}
	public String getSorborNo() {
		return sorborNo;
	}
	public void setSorborNo(String sorborNo) {
		this.sorborNo = sorborNo;
	}
	public Date getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}
	public Date getSorborDate() {
		return sorborDate;
	}
	public void setSorborDate(Date sorborDate) {
		this.sorborDate = sorborDate;
	}
	public String getCheckAmount() {
		return checkAmount;
	}
	public void setCheckAmount(String checkAmount) {
		this.checkAmount = checkAmount;
	}

	
	
	
}
