package com.mii.span.model;

public class SPANDataValidationErrorStatusModel {
	private String retryStatus;
	private String returStatus;
	
	public SPANDataValidationErrorStatusModel(String retryStatus,
			String returStatus) {
		super();
		this.retryStatus = retryStatus;
		this.returStatus = returStatus;
	}
	
	public SPANDataValidationErrorStatusModel() {
	}

	public String getRetryStatus() {
		return retryStatus;
	}
	public void setRetryStatus(String retryStatus) {
		this.retryStatus = retryStatus;
	}
	public String getReturStatus() {
		return returStatus;
	}
	public void setReturStatus(String returStatus) {
		this.returStatus = returStatus;
	}
}
