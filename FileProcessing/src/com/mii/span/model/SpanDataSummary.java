package com.mii.span.model;

import java.io.Serializable;

public class SpanDataSummary implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String updateFlagging;
	private String fileName;
	private String totalRecord;
	private String retry;
	private String retur;
	private String success;
	private String totalAmount;
	private String procStatus;
	private String returAck;
	private String successAck;
	private String retryL2;
	private String successL2;
	private String documentDate;
	private String remarkpm3;
	private String totalWait;
	
	private boolean selected;
	
	public String getRemarkpm3() {
		return remarkpm3;
	}

	public void setRemarkpm3(String remarkpm3) {
		this.remarkpm3 = remarkpm3;
	}

	public void setUpdateFlagging(String updateFlagging){
		this.updateFlagging=updateFlagging;
	}
	
	public String getUpdateFlagging(){
		return updateFlagging;
	}
	
	
	public void setFileName(String fileName){
		this.fileName=fileName;
	}
	
	public String getFileName(){
		return fileName;
	}
	
	public void setTotalRecord(String totalRecord){
		this.totalRecord=totalRecord;
	}
	
	public String getTotalRecord(){
		return totalRecord;
	}
	
	public void setRetur(String retur){
		this.retur=retur;
	}
	
	public String getRetur(){
		return retur;
	}
	
	public void setRetry(String retry){
		this.retry=retry;
	}
	
	public String getRetry(){
		return retry;
	}
	public void setSuccess(String success){
		this.success=success;
	}
	
	public String getSuccess(){
		return success;
	}
	
	public void setTotalAmount(String totalAmount){
		this.totalAmount=totalAmount;
	}
	
	public String getTotalAmount(){
		return totalAmount;
	}
	public void setProcStatus(String procStatus){
		this.procStatus=procStatus;
	}
	
	public String getProcStatus(){
		return procStatus;
	}
	
	public void setReturAck(String returAck){
		this.returAck=returAck;
	}
	
	public String getReturAck(){
		return returAck;
	}
	public void setSuccessAck(String successAck){
		this.successAck=successAck;
	}
	
	public String getSuccessAck(){
		return successAck;
	}
	public void setRetryL2(String retryL2){
		this.retryL2=retryL2;
	}
	
	public String getRetryL2(){
		return retryL2;
	}
	public void setSuccessL2(String successL2){
		this.successL2=successL2;
	}
	
	public String getSuccessL2(){
		return successL2;
	}
	public void setDocumentDate(String documentDate){
		this.documentDate=documentDate;
	}
	
	public String getDocumentDate(){
		return documentDate;
	}

	public String getTotalWait() {
		return totalWait;
	}

	public void setTotalWait(String totalWait) {
		this.totalWait = totalWait;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	
}
