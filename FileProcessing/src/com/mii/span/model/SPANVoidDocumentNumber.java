package com.mii.span.model;

import java.math.BigDecimal;

public class SPANVoidDocumentNumber {
	private String fileName;
	private String documentNumber;
	private String documentDate;
	private String beneficiaryName;
	private String beneficiaryBankCode;
	private String beneficiaryBank;
	private String beneficiaryAccount;
	private BigDecimal amount;
	private String description;
	private String agentBankCode;
	private String agentBankAccountNumber;
	private String agentBankAccountName;
	private String paymentMethod;
	private String sp2dCount;
	private String reason;
	private String createDate;
	private String status;
	
	private String creditAccNo;
	private String creditAccName;
	private String amountString;
	
	public SPANVoidDocumentNumber(String documentNumber, String creditAccNo, String creditAccName, String amountString) {
		super();
		this.documentNumber = documentNumber;
		this.creditAccNo = creditAccNo;
		this.creditAccName = creditAccName;
		this.amountString = amountString;
	}
	public String getFileName() {
		/*if (fileName.length() > 40 && !fileName.contains("\n")) {
			int length = fileName.length();
			String key1 = fileName.substring(0, length/2);
			String key2 = fileName.substring((length/2), length);
			fileName = key1+"\n"+key2;
		}*/
		return fileName;
	}
	public void setFileName(String fileName) {		
		this.fileName = fileName;
	}
	public String getDocumentNumber() {
		return documentNumber;
	}
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getDocumentDate() {
		return documentDate;
	}
	public void setDocumentDate(String documentDate) {
		this.documentDate = documentDate;
	}
	public String getBeneficiaryName() {
		return beneficiaryName;
	}
	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}
	public String getBeneficiaryBankCode() {
		return beneficiaryBankCode;
	}
	public void setBeneficiaryBankCode(String beneficiaryBankCode) {
		this.beneficiaryBankCode = beneficiaryBankCode;
	}
	public String getBeneficiaryBank() {
		return beneficiaryBank;
	}
	public void setBeneficiaryBank(String beneficiaryBank) {
		this.beneficiaryBank = beneficiaryBank;
	}
	public String getBeneficiaryAccount() {
		return beneficiaryAccount;
	}
	public void setBeneficiaryAccount(String beneficiaryAccount) {
		this.beneficiaryAccount = beneficiaryAccount;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getAgentBankCode() {
		return agentBankCode;
	}
	public void setAgentBankCode(String agentBankCode) {
		this.agentBankCode = agentBankCode;
	}
	public String getAgentBankAccountNumber() {
		return agentBankAccountNumber;
	}
	public void setAgentBankAccountNumber(String agentBankAccountNumber) {
		this.agentBankAccountNumber = agentBankAccountNumber;
	}
	public String getAgentBankAccountName() {
		return agentBankAccountName;
	}
	public void setAgentBankAccountName(String agentBankAccountName) {
		this.agentBankAccountName = agentBankAccountName;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public String getSp2dCount() {
		return sp2dCount;
	}
	public void setSp2dCount(String sp2dCount) {
		this.sp2dCount = sp2dCount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCreditAccNo() {
		return creditAccNo;
	}
	public void setCreditAccNo(String creditAccNo) {
		this.creditAccNo = creditAccNo;
	}
	public String getCreditAccName() {
		return creditAccName;
	}
	public void setCreditAccName(String creditAccName) {
		this.creditAccName = creditAccName;
	}
	public String getAmountString() {
		return amountString;
	}
	public void setAmountString(String amountString) {
		this.amountString = amountString;
	}

	
}
