package com.mii.span.model;

import java.io.Serializable;

public class SpanDataSummaryDetails implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String spanType;
	private String debitAccount;
	private String debitAccountType;
	private String docDate;
	private String creditAcctNo;
	private String creditAcctName;
	private String benefBankName;
	private String errorCode;
	private String errorMessage;
	private String debitAmount;
	private String documentNumber;
	private String remittanceNumber;
	
	private boolean selected;
	
	public String getRemittanceNumber() {
		return remittanceNumber;
	}

	public void setRemittanceNumber(String remittanceNumber) {
		this.remittanceNumber = remittanceNumber;
	}

	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	public void setSpanType(String spanType){
		this.spanType=spanType;
	}
	
	public String getSpanType(){
		return spanType;
	}
	
	public void setDebitAccount(String debitAccount){
		this.debitAccount=debitAccount;
	}
	
	public String getDebitAccount(){
		return debitAccount;
	}
	
	public void setDebitAccountType(String debitAccountType){
		this.debitAccountType=debitAccountType;
	}
	
	public String getDebitAccountType(){
		return debitAccountType;
	}
	
	public void setDocDate(String docDate){
		this.docDate=docDate;
	}
	
	public String getDocDate(){
		return docDate;
	}
	public void setCreditAcctNo(String creditAcctNo){
		this.creditAcctNo=creditAcctNo;
	}
	
	public String getCreditAcctNo(){
		return creditAcctNo;
	}
	
	public void setCreditAcctName(String creditAcctName){
		this.creditAcctName=creditAcctName;
	}
	
	public String getCreditAcctName(){
		return creditAcctName;
	}
	
	public void setBenefBankName(String benefBankName){
		this.benefBankName=benefBankName;
	}
	
	public String getBenefBankName(){
		return benefBankName;
	}
	public void setErrorCode(String errorCode){
		this.errorCode=errorCode;
	}
	
	public String getErrorCode(){
		return errorCode;
	}
	public void setErrorMessage(String errorMessage){
		this.errorMessage=errorMessage;
	}
	
	public String getErrorMessage(){
		return errorMessage;
	}
	public void setDebitAmount(String debitAmount){
		this.debitAmount=debitAmount;
	}
	
	public String getDebitAmount(){
		return debitAmount;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	
	
}
