package com.mii.span.handler;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedProperty;

import com.mii.dao.SpanVoidDataTrxDao;
import com.mii.dao.SystemParameterDAO;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.ApplicationInstance;
import com.mii.scoped.SessionBean;
import com.mii.span.dao.SPANDataValidationDao;
import com.mii.span.dao.SPANHostDataDetailsDao;
import com.mii.span.dao.SPANMonitoringVoidDepKeuDao;
import com.mii.span.model.SPANHostDataDetails;
import com.mii.span.model.SPANVoidDocumentNumber;
import com.mii.span.model.SpanMonitoringVoidDepKeu;
import com.mii.span.model.SpanSp2dVoidLog;
import com.mii.span.process.VoidDocumentNo;
import com.mii.span.process.VoidDocumentNumberRequest;
import com.mii.span.process.VoidDocumentNumberVFiles;
import com.wm.app.b2b.client.Context;
import com.wm.app.b2b.client.ServiceException;
import com.wm.data.IData;

public class MonitoringVoidDepkeuHandler implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private Boolean doShowGAJI;
	private Boolean doShowGAJIR;
	private String tabSelected; //GAJI - GAJIR
	
	private Boolean doShowVoidDocumentNumber;
	
	private String fileName;
	private String sp2dNumber;
	private String documentNumber;
	
	private Boolean doShowSearchMessage;
	private String searchMessage;
	
	private List < SpanMonitoringVoidDepKeu > monitoringVoidList;
	
	private SPANHostDataDetails spanHostDataDetail;
	private List <SPANVoidDocumentNumber> voidDocumentNumberList;
	private String filenameDetail;
	private String sp2dDetail;
	
	public static Map < String, SpanMonitoringVoidDepKeu > resultTemps = new HashMap < String, SpanMonitoringVoidDepKeu > ();
	
	private List<SpanSp2dVoidLog> spanSp2dVoidLogs;
	private List<SpanSp2dVoidLog> monitoringVoidSp2dList;
	
	@ManagedProperty(value="#{applicationBean}")
	private ApplicationBean applicationBean;
	
	@ManagedProperty(value="#{sessionBean}")
	private SessionBean sessionBean;

	public String filenameFilter;

	public String sp2dNoFilter;
	
	public MonitoringVoidDepkeuHandler() {
		applicationBean = ApplicationInstance.getInstance().getApplicationBean();
	}
	
	/*@PostConstruct
	public void init(){
		doClickGaji();
		documentNumber = "";
		spanHostDataDetail = new SPANHostDataDetails();
		setDoShowSearchMessage(false);
		spanSp2dVoidLogs = new ArrayList<>();
	}*/

	/*public void loadData() {
		//monitoringVoidList = new DetailDataModel();
		monitoringVoidList = getVoidListData();
	}*/
	
	/*private List<SpanMonitoringVoidDepKeu> getVoidListData() {
		// TODO Auto-generated method stub
		return null;
	}*/

	/*private List<SpanMonitoringVoidDepKeu> getMonitoringVoidListData() {
		// TODO Auto-generated method stub
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		List<SpanMonitoringVoidDepKeu> spanMonitoringVoidDepKeuList = new ArrayList<SpanMonitoringVoidDepKeu>();
		try{
			String docDate = sdf.format(new Date());
			String voidFlag = Constants.VOIDFLAG;
			spanMonitoringVoidDepKeuList = getSpanVoidDepKeuDAOBean().getSpanDataValidationSp2dNo(getFileName(), docDate, voidFlag, tabSelected, getSp2dNumber());
		}catch(Exception e){
			e.printStackTrace();
		}
		
		List < SpanMonitoringVoidDepKeu > detailsFinal = new ArrayList < SpanMonitoringVoidDepKeu > ();
		for (SpanMonitoringVoidDepKeu a: spanMonitoringVoidDepKeuList) {
			int jmlVoid = 0;
			try{
				jmlVoid = getSpanVoidDepKeuDAOBean().countSpanVoidDataTrx("0", a.getFileName(), a.getSp2dNumber());
			}catch(Exception e){
				e.printStackTrace();
			}
			if(jmlVoid > 0){
				a.setVoidStatus(true);
			}else{
				a.setVoidStatus(false);
			}
			detailsFinal.add(a);
		}
		if(detailsFinal.size()>0){
			return detailsFinal;
		}else{
			return null;
		}
	}*/

	/*public void loadVoidDocumentNumberList(SpanMonitoringVoidDepKeu monitoringVoid){
		filenameDetail = monitoringVoid.getFileName();
		sp2dDetail = monitoringVoid.getSp2dNumber();
		voidDocumentNumberList = getVoidDocumentNumberListData();
	}*/
	
	/*private List<SPANVoidDocumentNumber> getVoidDocumentNumberListData() {
		// TODO Auto-generated method stub
		List<SPANVoidDocumentNumber> SPANVoidDocumentNumberList = new ArrayList<SPANVoidDocumentNumber>();
		try{
			SPANVoidDocumentNumberList = getSpanVoidDepKeuDAOBean().getVoidDataTransactionListByFilenameSp2d("0", filenameDetail, sp2dDetail);
		}catch(Exception e){
			e.printStackTrace();
		}
		return SPANVoidDocumentNumberList;
	}*/

	/*private class DetailDataModel extends LazyDataModel < SpanMonitoringVoidDepKeu > {
		private static final long serialVersionUID = 1L;

		public DetailDataModel() {
		}

		@SuppressWarnings("rawtypes")
		@Override
		public List < SpanMonitoringVoidDepKeu > load(int startingAt, int maxPerPage,
		String sortField, SortOrder sortOrder,
		Map < String, String > filters) {
			Integer jmlAll = 0;
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			List<SpanMonitoringVoidDepKeu> spanMonitoringVoidDepKeuList = new ArrayList<SpanMonitoringVoidDepKeu>();
			try{
				String docDate = sdf.format(new Date());
				String voidFlag = Constants.VOIDFLAG;
				spanMonitoringVoidDepKeuList = getSpanVoidDepKeuDAOBean().getLazySpanDataValidationSp2dNo(getFileName(), docDate, voidFlag, tabSelected, getSp2dNumber(), startingAt, startingAt+maxPerPage);
				jmlAll = getSpanVoidDepKeuDAOBean().countSpanDataValidationSp2dNo(getFileName(), docDate, voidFlag, tabSelected, getSp2dNumber());
			}catch(Exception e){
				e.printStackTrace();
			}
			setRowCount(jmlAll);
			setPageSize(maxPerPage);
			
			List < SpanMonitoringVoidDepKeu > detailsFinal = new ArrayList < SpanMonitoringVoidDepKeu > ();
			for (SpanMonitoringVoidDepKeu a: spanMonitoringVoidDepKeuList) {
				Iterator entries = resultTemps.entrySet().iterator();
				while (entries.hasNext()) {
					Entry thisEntry = (Entry) entries.next();
					SpanMonitoringVoidDepKeu s = (SpanMonitoringVoidDepKeu) thisEntry.getValue();
					if (a.getSp2dNumber().equals(s.getSp2dNumber())) {
						if (s.isSelected()) {
							a.setSelected(true);
						} else {
							a.setSelected(false);
						}
					}
				}
				int jmlVoid = 0;
				try{
					jmlVoid = getSpanVoidDepKeuDAOBean().countSpanVoidDataTrx("0", a.getFileName(), a.getSp2dNumber());
				}catch(Exception e){
					e.printStackTrace();
				}
				if(jmlVoid > 0){
					a.setVoidStatus(true);
				}else{
					a.setVoidStatus(false);
				}
				detailsFinal.add(a);
			}
			if(detailsFinal.size()>0){
				return detailsFinal;
			}else{
				return null;
			}
		}
	}
	*/
	/*private class DetailVoidDataModel extends LazyDataModel < SPANVoidDocumentNumber > {
		private static final long serialVersionUID = 1L;

		public DetailVoidDataModel() {
		}

		@Override
		public List < SPANVoidDocumentNumber > load(int startingAt, int maxPerPage,
		String sortField, SortOrder sortOrder,
		Map < String, String > filters) {
			Integer jmlAll = 0;
			List<SPANVoidDocumentNumber> SPANVoidDocumentNumberList = new ArrayList<SPANVoidDocumentNumber>();
			try{
				SPANVoidDocumentNumberList = getSpanVoidDepKeuDAOBean().getVoidDataTransactionListByFilenameSp2d("0", filenameDetail, sp2dDetail);
				jmlAll = SPANVoidDocumentNumberList.size();
			}catch(Exception e){
				e.printStackTrace();
			}
			setRowCount(jmlAll);
			setPageSize(maxPerPage);
			
			List<SPANVoidDocumentNumber> detailFinal = new ArrayList<SPANVoidDocumentNumber>();
			for(int i=startingAt; i<SPANVoidDocumentNumberList.size(); i++){
				detailFinal.add(SPANVoidDocumentNumberList.get(i));
			}
			return detailFinal;
		}
	}*/
	
	/*private class VoidSp2dLogModel extends LazyDataModel < SpanSp2dVoidLog > {
		private static final long serialVersionUID = 1L;

		public VoidSp2dLogModel() {
		}

		@Override
		public List<SpanSp2dVoidLog> load(int startingAt, int maxPerPage,
		String sortField, SortOrder sortOrder,
		Map < String, String > filters) {
			try{
				filenameFilter = "%"+StringUtils.defaultIfEmpty(filters.get("filename"), "")+"%";
				sp2dNoFilter = "%"+StringUtils.defaultIfEmpty(filters.get("sp2dNo"), "")+"%";
				
				List<SpanSp2dVoidLog> result = new ArrayList<>();
				result = VoidDocumentNo.viewSpanSp2dVoidLogs(filenameFilter, sp2dNoFilter, startingAt, startingAt+maxPerPage, getSpanVoidDataTrxDAOBean());
				
				setRowCount(VoidDocumentNo.countSpanSp2dVoidLogs(filenameFilter, sp2dNoFilter, getSpanVoidDataTrxDAOBean()));
				setPageSize(maxPerPage);
				
				
				return result;
			}catch(Exception e){
				e.printStackTrace();
			}
			return null;
		}
	}*/
	
	/*public void processChecked(SpanMonitoringVoidDepKeu result) {
		SpanMonitoringVoidDepKeu r = new SpanMonitoringVoidDepKeu();
		r = (SpanMonitoringVoidDepKeu) SerializationUtils.clone(result);
		if (result.isSelected()) {
			resultTemps.put(result.getSp2dNumber(), result);
		} else {
			resultTemps.remove(result.getSp2dNumber());
		}
	}*/
	
	/*public void init(){
		setMonitoringVoidList(null);
		resultTemps.clear();
		setFileName("");
		setSp2dNumber("");
	}
	
	public void doClickGaji(){
		doShowGAJI = true;
		doShowGAJIR = false;
		setTabSelected(Constants.GAJI);
		changeTab();
	}
	
	public void doClickGajiR(){
		doShowGAJI = false;
		doShowGAJIR = true;
		setTabSelected(Constants.GAJIR);
		changeTab();
	}
	
	private void changeTab() {
		setMonitoringVoidList(null);
		resultTemps.clear();
		//loadData();
		setFileName("");
		setSp2dNumber("");
	}*/
	
	/*@SuppressWarnings("rawtypes")
	public void voidChecked(){
		SystemParameterDAO systemParameterDao = (SystemParameterDAO) applicationBean.getCorpGWContext().getBean("systemParameterDAO");
		VoidDocumentNumberRequest voidDocumentNumberRequest = new VoidDocumentNumberRequest();
		List<VoidDocumentNumberVFiles> voidDocumentNumberVFilesList = new ArrayList<VoidDocumentNumberVFiles>();
		Iterator entries = resultTemps.entrySet().iterator();
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
		
		while (entries.hasNext()) {
			Entry thisEntry = (Entry) entries.next();
			SpanMonitoringVoidDepKeu s = (SpanMonitoringVoidDepKeu) thisEntry.getValue();
			voidDocumentNumberRequest.setInvokeType("1");
			VoidDocumentNumberVFiles voidDocumentNumberVFiles = new VoidDocumentNumberVFiles();
			voidDocumentNumberVFiles.setFileName(s.getFileName());
			List<String> documentNoList = new ArrayList<String>();
			documentNoList.add(s.getSp2dNumber());
			voidDocumentNumberVFiles.setDocumentNoList(documentNoList);
			voidDocumentNumberVFilesList.add(voidDocumentNumberVFiles);
			
			createSpanSp2dvoidLogList(voidDocumentNumberVFiles.getFileName(), s.getSp2dNumber(), dateFormat.format(new Date()));
		}
		voidDocumentNumberRequest.setVoidDocumentNumberVFilesList(voidDocumentNumberVFilesList);
		VoidDocumentNo.VoidDocumentNoProcess(voidDocumentNumberRequest, getSPANDataValidationDao(), systemParameterDao, getSpanVoidDataTrxDAOBean(), getSPANHostDataDetailsDao());
		VoidDocumentNo.insertSpanSp2dVoidLogs(spanSp2dVoidLogs, getSpanVoidDataTrxDAOBean());
		System.out.println("VoidChecked Executed");
	}*/
	
	/*private void createSpanSp2dvoidLogList(String filename, String sp2dNumber,
			String datetimeVoid) {
		SpanSp2dVoidLog spanSp2dVoidLog = new SpanSp2dVoidLog();
		spanSp2dVoidLog.setFilename(filename);
		spanSp2dVoidLog.setSp2dNo(sp2dNumber);
		spanSp2dVoidLog.setDatetimeVoid(datetimeVoid);
		
		spanSp2dVoidLogs.add(spanSp2dVoidLog);
	}*/

	/*@SuppressWarnings("rawtypes")
	public void unvoidChecked(){
		Iterator entries = resultTemps.entrySet().iterator();
		while (entries.hasNext()) {
			Entry thisEntry = (Entry) entries.next();
			SpanMonitoringVoidDepKeu s = (SpanMonitoringVoidDepKeu) thisEntry.getValue();
			SPANDataValidationDao spanDataValidationDao = getSPANDataValidationDao();
			Integer jml = 0;
			jml = getSpanVoidDepKeuDAOBean().countVoid("0", s.getFileName(), s.getSp2dNumber());
			int totalAmountVoid = getSpanVoidDepKeuDAOBean().sumAmountVoid("0",  s.getFileName(), s.getSp2dNumber());
			int deleteSpanVoidDataTrx = getSpanVoidDepKeuDAOBean().deleteSpanVoidDataTrx("0", s.getFileName(), s.getSp2dNumber());
			if (jml>0) {
//				spanDataValidationDao.updateUnvoidSpanDataValidation(s.getTotalAmount(), s.getTotalRecord(), s.getFileName(), "1", "0"); //Masih bug
				spanDataValidationDao.updateUnvoidSpanDataValidation(Integer.toString(totalAmountVoid), Integer.toString(jml), s.getFileName(), "1", "0"); //Solved
			}
			
			if (deleteSpanVoidDataTrx > 0) {
				String headerMessage = "Success";
				String bodyMessage = "Unvoid Success ";
			}
		}
		System.out.println("UnVoidChecked Executed");
	}*/
	
	/*public void initDialogVoidDocumentNumber(){
		documentNumber = "";
		spanHostDataDetail = null;
		setDoShowVoidDocumentNumber(false);
		setDoShowSearchMessage(false);
	}*/
	
	/*public void searchDocumentNumber(){
		if(!documentNumber.equals("")&documentNumber!=null){
			if(documentNumber.length()==21){
				setDoShowSearchMessage(false);
				searchMessage = "";
				spanHostDataDetail = getSPANHostDataDetailsDao().getSearchDocumentNumber(documentNumber);
				try{
					if(spanHostDataDetail.getDocumentNumber()!=null||!spanHostDataDetail.getDocumentNumber().equals("")){
						int jml = getSpanVoidDepKeuDAOBean().countSearchDocumentNumber(documentNumber, "0");
						if(jml==0){
							if(spanHostDataDetail.getExecStatus().equals("1")){
								setDoShowVoidDocumentNumber(false);
								setDoShowSearchMessage(true);
								searchMessage = "*Document Number allready executed.*";
							}else{
								setDoShowVoidDocumentNumber(true);
							}
						}else{
							setDoShowVoidDocumentNumber(false);
							setDoShowSearchMessage(true);
							searchMessage = "*Document Number allready Void*";
						}
					}else{
						setDoShowVoidDocumentNumber(false);
						setDoShowSearchMessage(true);
						searchMessage = "*Document Number not found, or not extracted yet.*";
					}
				}catch(Exception e){
					setDoShowVoidDocumentNumber(false);
					setDoShowSearchMessage(true);
					searchMessage = "*Document Number not found, or not extracted yet.*";
				}
					
			}else{
				setDoShowSearchMessage(true);
				spanHostDataDetail = null;
				searchMessage = "*Document Number length must be 21*";
			}
		}else{
			setDoShowSearchMessage(true);
			spanHostDataDetail = null;
			searchMessage = "*Document Number can't be null*";
		}
	}*/
	
	public void doVoidDocumentNumberByScheduller(String documentNumber){
		spanHostDataDetail = getSPANHostDataDetailsDao().getSearchDocumentNumber(documentNumber);
		
		SystemParameterDAO systemParameterDao = (SystemParameterDAO) applicationBean.getCorpGWContext().getBean("sysParamDAO");
		VoidDocumentNumberRequest voidDocumentNumberRequest = new VoidDocumentNumberRequest();
		voidDocumentNumberRequest.setInvokeType("1");
		
		List<VoidDocumentNumberVFiles> voidDocumentNumberVFilesList = new ArrayList<VoidDocumentNumberVFiles>();
		VoidDocumentNumberVFiles voidDocumentNumberVFiles = new VoidDocumentNumberVFiles();
		voidDocumentNumberVFiles.setFileName(spanHostDataDetail.getFileName());
		List<String> documentNoList = new ArrayList<String>();
		documentNoList.add(spanHostDataDetail.getDocumentNumber());
		voidDocumentNumberVFiles.setDocumentNoList(documentNoList);
		voidDocumentNumberVFilesList.add(voidDocumentNumberVFiles);
		
		voidDocumentNumberRequest.setVoidDocumentNumberVFilesList(voidDocumentNumberVFilesList);
		VoidDocumentNo.VoidDocumentNoProcess(voidDocumentNumberRequest, getSPANDataValidationDao(), systemParameterDao, getSpanVoidDataTrxDAOBean(), getSPANHostDataDetailsDao());
		
		setMonitoringVoidList(null);
		resultTemps.clear();
		//loadData();
	}
	
	/*public void doUnvoidDocumentNumber(SPANVoidDocumentNumber spanVoidDocumentNumber){
		DeleteSPANVoidDataTransactionRequest deleteSPANVoidDataTransactionRequest = new DeleteSPANVoidDataTransactionRequest();
		deleteSPANVoidDataTransactionRequest.setDocumentNo(spanVoidDocumentNumber.getDocumentNumber());
		deleteSPANVoidDataTransactionRequest.setFileName(filenameDetail);
		DeleteSPANVoidDataTransaction.unvoidDocumentNumber(deleteSPANVoidDataTransactionRequest, getSpanVoidDataTrxDAOBean());
		
		setMonitoringVoidList(null);
		resultTemps.clear();
		loadData();
	}*/
	
	/**
	 * Method for invoke WebMesthods service
	 * @param isserver
	 * @param isusername
	 * @param ispassword
	 * @param packageName
	 * @param serviceName
	 * @param input
	 * @return
	 */
	public static IData invoke(String isserver, String isusername, String ispassword, String packageName, String serviceName, IData input){
		Context context = new Context();
		IData output = null;
		try {
			context.connect(isserver, isusername, ispassword);
			output = context.invoke(packageName, serviceName,input);
			context.disconnect();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return output;
	}
	
	private SpanVoidDataTrxDao getSpanVoidDataTrxDAOBean(){
		return (SpanVoidDataTrxDao) applicationBean.getCorpGWContext().getBean("SPANVoidDataTrxDao");
	}
	
	private SPANMonitoringVoidDepKeuDao getSpanVoidDepKeuDAOBean(){
		return (SPANMonitoringVoidDepKeuDao) applicationBean.getCorpGWContext().getBean("SPANMonitoringVoidDepKeuDao");
	}
	
	private SPANHostDataDetailsDao getSPANHostDataDetailsDao(){
		return (SPANHostDataDetailsDao) applicationBean.getCorpGWContext().getBean("SPANHostDataDetailsDao");
	}
	
	private SPANDataValidationDao getSPANDataValidationDao(){
		return (SPANDataValidationDao) applicationBean.getCorpGWContext().getBean("SPANDataValidationDao");
	}
	
	
	public ApplicationBean getApplicationBean() {
		return applicationBean;
	}

	public void setApplicationBean(ApplicationBean applicationBean) {
		this.applicationBean = applicationBean;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public Boolean getDoShowGAJI() {
		return doShowGAJI;
	}

	public void setDoShowGAJI(Boolean doShowGAJI) {
		this.doShowGAJI = doShowGAJI;
	}

	public Boolean getDoShowGAJIR() {
		return doShowGAJIR;
	}

	public void setDoShowGAJIR(Boolean doShowGAJIR) {
		this.doShowGAJIR = doShowGAJIR;
	}

	public String getTabSelected() {
		return tabSelected;
	}

	public void setTabSelected(String tabSelected) {
		this.tabSelected = tabSelected;
	}

	public String getSp2dNumber() {
		return sp2dNumber;
	}

	public void setSp2dNumber(String sp2dNumber) {
		this.sp2dNumber = sp2dNumber;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	public Boolean getDoShowVoidDocumentNumber() {
		return doShowVoidDocumentNumber;
	}

	public void setDoShowVoidDocumentNumber(Boolean doShowVoidDocumentNumber) {
		this.doShowVoidDocumentNumber = doShowVoidDocumentNumber;
	}

	public SPANHostDataDetails getSpanHostDataDetail() {
		return spanHostDataDetail;
	}

	public void setSpanHostDataDetail(SPANHostDataDetails spanHostDataDetail) {
		this.spanHostDataDetail = spanHostDataDetail;
	}

	public Boolean getDoShowSearchMessage() {
		return doShowSearchMessage;
	}

	public void setDoShowSearchMessage(Boolean doShowSearchMessage) {
		this.doShowSearchMessage = doShowSearchMessage;
	}

	public String getSearchMessage() {
		return searchMessage;
	}

	public void setSearchMessage(String searchMessage) {
		this.searchMessage = searchMessage;
	}

	public String getFilenameDetail() {
		return filenameDetail;
	}

	public void setFilenameDetail(String filenameDetail) {
		this.filenameDetail = filenameDetail;
	}

	public String getSp2dDetail() {
		return sp2dDetail;
	}

	public void setSp2dDetail(String sp2dDetail) {
		this.sp2dDetail = sp2dDetail;
	}

	public List < SpanMonitoringVoidDepKeu > getMonitoringVoidList() {
		return monitoringVoidList;
	}

	public void setMonitoringVoidList(List < SpanMonitoringVoidDepKeu > monitoringVoidList) {
		this.monitoringVoidList = monitoringVoidList;
	}

	public List <SPANVoidDocumentNumber> getVoidDocumentNumberList() {
		return voidDocumentNumberList;
	}

	public void setVoidDocumentNumberList(List <SPANVoidDocumentNumber> voidDocumentNumberList) {
		this.voidDocumentNumberList = voidDocumentNumberList;
	}

	public List<SpanSp2dVoidLog> getMonitoringVoidSp2dList() {
		return monitoringVoidSp2dList;
	}

	public void setMonitoringVoidSp2dList(List<SpanSp2dVoidLog> monitoringVoidSp2dList) {
		this.monitoringVoidSp2dList = monitoringVoidSp2dList;
	}
}
