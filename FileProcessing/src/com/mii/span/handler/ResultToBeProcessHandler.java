package com.mii.span.handler;

import java.io.Serializable;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.mii.constant.Constants;
import com.mii.dao.SystemParameterDAO;
import com.mii.json.AccountBalance;
import com.mii.models.SystemParameter;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.ApplicationInstance;
import com.mii.scoped.SessionBean;
import com.mii.span.dao.SPANDataValidationDao;
import com.mii.span.dao.ToBeProcessDao;
import com.mii.span.dao.UserDao;
import com.mii.span.model.SpanDataSummary;
import com.mii.span.model.SpanDataSummaryDetails;
import com.mii.span.process.GetSPANSummaries;
import com.mii.span.process.ToBeProcess;
import com.mii.span.process.io.GetSPANSummariesRequest;
import com.mii.span.process.io.GetSPANSummariesResponse;
import com.mii.span.process.io.ToBeProcessInput;
import com.mii.span.process.io.ToBeProcessOutput;
import com.mii.span.process.io.helper.DetailsOutputList;
import com.mii.span.process.io.helper.Files;
import com.mii.span.process.io.helper.OutputDetails;

@ManagedBean
@ViewScoped
public class ResultToBeProcessHandler implements Serializable {
	private static final long serialVersionUID = 1L;

	private SpanDataSummary spanDataSummary;
	private SpanDataSummaryDetails spanDataSummaryDetails;
	private SpanDataSummary spanDataSummaryTemp;
	private List<DetailsOutputList> detailList;
	private List<SpanDataSummary> summaryList;
	private Boolean doShowGAJI;
	private Boolean doShowGAJIR;
	private Boolean doShowRetry;
	private Boolean doShowRetur;
	private Boolean doShowSendACK;
	private String tabSelected; // GAJI - GAJIR

	private Boolean doShowDownloadFile;
	private String typeOfProcess;
	private String formatOfFile;
	private Boolean doShowPDFXLS;

	private String availableBalance;
	private String accountNumber;
	private String totalRecords;
	private String selectedAmount;

	@ManagedProperty(value = "#{applicationBean}")
	private static ApplicationBean applicationBean;

	@ManagedProperty(value = "#{sessionBean}")
	private SessionBean sessionBean;

	private String detailFileName;
	private String detailTotalRecord;
	private String detailTotalAmount;
	private String detailType;
	private String detailDebitAccount;
	private String detailDebitAccountType;
	private String detailDocumentDate;

	private Map<String, SpanDataSummary> resultTemps = new HashMap<String, SpanDataSummary>();
	private Map<String, DetailsOutputList> resultTempsDetails = new HashMap<String, DetailsOutputList>();
	private Map<String, SpanDataSummary> resultTempsACK = new HashMap<String, SpanDataSummary>();

	@PostConstruct
	public void init() {
		doClickGaji();
		formatOfFile = "2";
	}

	public void doClickGaji() {
		doShowGAJI = true;
		doShowGAJIR = false;
		tabSelected = Constants.GAJI;
		initChangeTab();
	}

	public void doClickGajiR() {
		doShowGAJI = false;
		doShowGAJIR = true;
		tabSelected = Constants.GAJIR;
		initChangeTab();
	}

	private void initChangeTab() {
		resultTemps.clear();
		setSummaryList(null);
		totalRecords = "0";
		selectedAmount = "0.00";
		availableBalance = null;
		accountNumber = null;
		doShowDownloadFile = false;
		secondaryTabInit();
	}

	private void secondaryTabInit() {
		doShowRetry = true;
		doShowRetur = false;
		doShowSendACK = false;
	}

	public void doClickRetry() {
		doShowRetry = true;
		doShowRetur = false;
		doShowSendACK = false;
		resultTemps.clear();
		loadAndCompareDataRetry();
		
		totalRecords = "0";
		selectedAmount = "0.00";
		availableBalance = null;
		accountNumber = null;
		doShowDownloadFile = false;
	}

	private void loadAndCompareDataRetry() {
		loadData();

		Map<String, SpanDataSummary> map1 = new LinkedHashMap<String, SpanDataSummary>();
		Map<String, SpanDataSummary> map2 = new LinkedHashMap<String, SpanDataSummary>();

		for (SpanDataSummary summary : summaryList) {
			map1.put(summary.getFileName(), summary);
		}

		doShowRetry = false;
		doShowRetur = true;
		doShowSendACK = false;
		resultTemps = new HashMap<String, SpanDataSummary>();
		resultTemps.clear();
		loadData();

		for (SpanDataSummary summary : summaryList) {
			map1.remove(summary.getFileName());
		}

		summaryList = new ArrayList<SpanDataSummary> (map1.values());
		resultTemps = map1;
		
		doShowRetry = true;
		doShowRetur = false;
		doShowSendACK = false;
	}

	public void doClickRetur() {
		doShowRetry = false;
		doShowRetur = true;
		doShowSendACK = false;
		resultTemps = new HashMap<String, SpanDataSummary>();
		resultTemps.clear();
		loadData();
		totalRecords = "0";
		selectedAmount = "0.00";
		availableBalance = null;
		accountNumber = null;
		doShowDownloadFile = false;
	}

	private String hiddenSelectedAmount = "0";

	public void doClickSendACK() {
		doShowRetry = false;
		doShowRetur = false;
		doShowSendACK = true;
		resultTemps.clear();
		loadAndCompareDataSendAck();
		
		totalRecords = "0";
		selectedAmount = "0.00";
		hiddenSelectedAmount = "0";
		availableBalance = null;
		accountNumber = null;
		doShowDownloadFile = false;
		doShowPDFXLS = false;
		resultTempsACK.clear();
	}

	private void loadAndCompareDataSendAck() {
		loadData();
		
		Map<String, SpanDataSummary> map1 = new LinkedHashMap<String, SpanDataSummary>();
		Map<String, SpanDataSummary> map2 = new LinkedHashMap<String, SpanDataSummary>();

		for (SpanDataSummary summary : summaryList) {
			map1.put(summary.getFileName(), summary);
		}

		//======================check if duplicate on retry
		doShowRetry = true;
		doShowRetur = false;
		doShowSendACK = false;
		resultTemps.clear();
		loadData();

		for (SpanDataSummary summary : summaryList) {
			map1.remove(summary.getFileName());
		}
		
		doShowRetry = false;
		doShowRetur = true;
		doShowSendACK = false;
		resultTemps.clear();
		loadData();

		for (SpanDataSummary summary : summaryList) {
			map1.remove(summary.getFileName());
		}

//		summaryList = new ArrayList<SpanDataSummary> (map1.values());
//		resultTemps = map1;
		
		//======================check if duplicate on retur only
		doShowRetry = false;
		doShowRetur = true;
		doShowSendACK = false;
//		resultTemps = new HashMap<String, SpanDataSummary>();
		resultTemps.clear();
		loadData();

		for (SpanDataSummary summary : summaryList) {
			map1.remove(summary.getFileName());
		}

		summaryList = new ArrayList<SpanDataSummary> (map1.values());
		resultTemps = map1;
		
		doShowRetry = false;
		doShowRetur = false;
		doShowSendACK = true;
	}

	public void processChecked(SpanDataSummary result) {		
		resultTemps.put(result.getFileName(), result);
		try {
			totalRecords = String
					.valueOf((Integer.parseInt(totalRecords) + Integer
							.parseInt(result.getTotalRecord())));
			DecimalFormat df = new DecimalFormat("###,###.00");
			BigInteger sum = new BigInteger(hiddenSelectedAmount);
			sum = sum.add(new BigInteger(result.getTotalAmount()));
			selectedAmount = (df.format(sum));
			hiddenSelectedAmount = sum.toString();
			if (selectedAmount.equals(".00")) {
				selectedAmount = "0.00";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (doShowSendACK) {
			if (result.getTotalAmount().equals("0")
					|| result.getTotalAmount().equals("")
					|| result.getTotalAmount() == null
					|| result.getTotalAmount().equals("0.00")
					|| result.getTotalAmount() == null) {
				if (result.isSelected()) {
					resultTempsACK.put(result.getFileName(), result);
				} else {
					resultTempsACK.remove(result.getFileName());
				}
			}
			if (resultTempsACK.size() > 0) {
				doShowDownloadFile = true;
			} else {
				doShowDownloadFile = false;
			}
		}
	}

	private static SystemParameterDAO getSystemParameterDaoBean() {
		applicationBean = ApplicationInstance.getInstance()
				.getApplicationBean();
		return (SystemParameterDAO) applicationBean.getFileProcessContext()
				.getBean("sysParamDAO");
	}

	private void loadData() {
		// setSummaryList(new DetailDataModel());
		summaryList = getSummaryListData();
	}

	private List<SpanDataSummary> getSummaryListData() {
		// TODO Auto-generated method stub
		SPANDataValidationDao spanDataValidationDao = getSpanDataValidationDaoBean();

		SystemParameterDAO systemParameterDao = getSystemParameterDaoBean();
		List<SpanDataSummary> details = new ArrayList<SpanDataSummary>();
		GetSPANSummariesRequest request = new GetSPANSummariesRequest();
		GetSPANSummariesResponse response = null;
		// FIXME masih hardcode
		request.setSumType("0");
		// Status = tab type (retry/retur/sendack)
		request.setStatus(getTabId());

		// Provider Code
		if (tabSelected.equals(Constants.GAJI)) {
			request.setProviderCode("SPN");
		} else {
			request.setProviderCode("SPN01");
		}

		try {
			response = GetSPANSummaries.getSPANSummaries(request,
					spanDataValidationDao, systemParameterDao, tabSelected);
		} catch (Exception e) {
			e.printStackTrace();
		}

		List<OutputDetails> OutputDetailsList = response.getOutputDetails();

		for (OutputDetails outputDetails : OutputDetailsList) {
			SpanDataSummary sds = new SpanDataSummary();
			sds.setFileName(outputDetails.getFileName());
			sds.setTotalAmount(outputDetails.getTotalAmount());
			sds.setTotalRecord(outputDetails.getTotalRecord());
			details.add(sds);

			// new on file processing
			processChecked(sds);
		}

		
		// new on file processing
		return details;
	}

	

	private SPANDataValidationDao getSpanDataValidationDaoBean() {
		applicationBean = ApplicationInstance.getInstance()
				.getApplicationBean();
		SPANDataValidationDao spanDataValidationDao = (SPANDataValidationDao) applicationBean
				.getCorpGWContext().getBean("SPANDataValidationDao");
		return spanDataValidationDao;
	}

	private ToBeProcessDao getToBeProcessDaoBean() {
		ToBeProcessDao toBeProcessDao = (ToBeProcessDao) applicationBean
				.getCorpGWContext().getBean("ToBeProcessDao");
		return toBeProcessDao;
	}

	public void ExecuteChecked() {
		boolean status = false;

		// hardcode tes
//		availableBalance = "999,999,999.00";
		/*if (doShowSendACK) {
			status = true;
		} else {
			try {
				if (getAvailableBalance().equals(".00")) {
					setAvailableBalance("0.00");
				}
				if (Integer.parseInt(getAvailableBalance().replace(".00", "")
						.replace(",", "")) >= Integer
						.parseInt(getSelectedAmount().replace(".00", "")
								.replace(",", ""))) {
					status = true;
				} else {
					String headerMessage = "Error";
					String bodyMessage = "Not enough balance to execute selected records.";
				}
			} catch (Exception e) {
				String headerMessage = "Error";
				String bodyMessage = "Please Inquiry First.";
			}
		}*/
		status = true;
		if (status) {
			// Construct Input
			ToBeProcessInput input = new ToBeProcessInput();
			input.setExeType("0");
			input.setTypeProcess(getTabId());
			String log = "";
			if (doShowRetry) {
				System.out.println("Send Retry Checked");
				input.setLegStatus("L1");
				log = "RETRY";
			} else if (doShowRetur) {
				System.out.println("Send Retur Checked");
				input.setLegStatus("L1");
				SystemParameter param = getSystemParameterDaoBean()
						.getDetailedParameterByParamName(
								Constants.PARAM_DEBITACCNO
										.concat(Constants.GAJIR));
				input.setAccountRetur(param.getParam_value());
				log = "RETUR";
			} else if (doShowSendACK) {
				System.out.println("Send ACK Checked");
				log = "SEND ACK";
			}
			List<Files> files = new ArrayList<Files>();

			Iterator entries = resultTemps.entrySet().iterator();
			while (entries.hasNext()) {
				Files fileData = new Files();
				Entry thisEntry = (Entry) entries.next();
				SpanDataSummary s = (SpanDataSummary) thisEntry.getValue();
				fileData.setFileName(s.getFileName());
				files.add(fileData);
			}
			input.setFiles(files);
			ToBeProcessOutput toBeProcessOutput = new ToBeProcessOutput();
			toBeProcessOutput = ToBeProcess.toBeProcess(input,
					getSpanDataValidationDaoBean(), getToBeProcessDaoBean(),
					getSystemParameterDaoBean());
			System.out.println(toBeProcessOutput.getStatus());
			String headerMessage = "Success";
			String bodyMessage = "Success to execute selected records.";
			totalRecords = "0";
			selectedAmount = "0.00";
			hiddenSelectedAmount = "0";
		}
	}

	public void InquirySaldo() {
		SystemParameterDAO parameterDao = getSystemParameterDaoBean();
		SystemParameter parameter = parameterDao
				.getDetailedParameterByParamName(Constants.PARAM_DEBITACCNO
						+ tabSelected);
		accountNumber = parameter.getParam_value();
		try {
			String availableBalance = AccountBalance.getAccountBalance(
					parameterDao, accountNumber);
			// setAvailableBalance(String.format("%1$,.2f",Float.parseFloat(availableBalance)));
			// //DO NOT USE - ERROR!!!!
			DecimalFormat df = new DecimalFormat("###,###.00");
			setAvailableBalance(df.format(new BigInteger((availableBalance))));
			if (availableBalance.equals(".00")) {
				// availableBalance = "0.00";
				setAvailableBalance("Error");
			}
		} catch (Exception e) {
			setAvailableBalance("Error");
		}
	}

	public void prepareForceRetur(SpanDataSummary summary) {
		spanDataSummaryTemp = summary;
	}

	public void ForceRetur() {
		// TODO Force Retur logic here
		System.out.println("Force Retur");
		spanDataSummaryTemp = null;
	}

	private String getTabId() {
		if (doShowRetry) {
			return "2";
		} else if (doShowRetur) {
			return "1";
		} else if (doShowSendACK) {
			return "3";
		}
		return null;
	}

	private String getTabIdDetails() {
		if (doShowRetry) {
			return "1";
		} else if (doShowRetur) {
			return "2";
		} else if (doShowSendACK) {
			return "3";
		}
		return null;
	}

	private UserDao getUserDAOBean() {
		return (UserDao) applicationBean.getCorpGWContext().getBean("userDAO");
	}

	public void clearPrepForceRetur() {
		spanDataSummaryTemp = null;
	}

	public void doDownloadFile() {
		doShowPDFXLS = true;
	}

	public ApplicationBean getApplicationBean() {
		return applicationBean;
	}

	public void setApplicationBean(ApplicationBean applicationBean) {
		this.applicationBean = applicationBean;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public Boolean getDoShowGAJI() {
		return doShowGAJI;
	}

	public void setDoShowGAJI(Boolean doShowGAJI) {
		this.doShowGAJI = doShowGAJI;
	}

	public Boolean getDoShowGAJIR() {
		return doShowGAJIR;
	}

	public void setDoShowGAJIR(Boolean doShowGAJIR) {
		this.doShowGAJIR = doShowGAJIR;
	}

	public Boolean getDoShowRetry() {
		return doShowRetry;
	}

	public void setDoShowRetry(Boolean doShowRetry) {
		this.doShowRetry = doShowRetry;
	}

	public Boolean getDoShowRetur() {
		return doShowRetur;
	}

	public void setDoShowRetur(Boolean doShowRetur) {
		this.doShowRetur = doShowRetur;
	}

	public Boolean getDoShowSendACK() {
		return doShowSendACK;
	}

	public void setDoShowSendACK(Boolean doShowSendACK) {
		this.doShowSendACK = doShowSendACK;
	}

	public String getTabSelected() {
		return tabSelected;
	}

	public void setTabSelected(String tabSelected) {
		this.tabSelected = tabSelected;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getSelectedAmount() {
		return selectedAmount;
	}

	public void setSelectedAmount(String selectedAmount) {
		this.selectedAmount = selectedAmount;
	}

	public String getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(String totalRecords) {
		this.totalRecords = totalRecords;
	}

	public String getAvailableBalance() {
		return availableBalance;
	}

	public void setAvailableBalance(String availableBalance) {
		this.availableBalance = availableBalance;
	}

	public SpanDataSummary getSpanDataSummary() {
		return spanDataSummary;
	}

	public void setSpanDataSummary(SpanDataSummary spanDataSummary) {
		this.spanDataSummary = spanDataSummary;
	}

	public SpanDataSummaryDetails getSpanDataSummaryDetails() {
		return spanDataSummaryDetails;
	}

	public void setSpanDataSummaryDetails(
			SpanDataSummaryDetails spanDataSummaryDetails) {
		this.spanDataSummaryDetails = spanDataSummaryDetails;
	}

	public SpanDataSummary getSpanDataSummaryTemp() {
		return spanDataSummaryTemp;
	}

	public void setSpanDataSummaryTemp(SpanDataSummary spanDataSummaryTemp) {
		this.spanDataSummaryTemp = spanDataSummaryTemp;
	}

	public Boolean getDoShowDownloadFile() {
		return doShowDownloadFile;
	}

	public void setDoShowDownloadFile(Boolean doShowDownloadFile) {
		this.doShowDownloadFile = doShowDownloadFile;
	}

	public String getTypeOfProcess() {
		return typeOfProcess;
	}

	public void setTypeOfProcess(String typeOfProcess) {
		this.typeOfProcess = typeOfProcess;
	}

	public String getFormatOfFile() {
		return formatOfFile;
	}

	public void setFormatOfFile(String formatOfFile) {
		this.formatOfFile = formatOfFile;
	}

	public Boolean getDoShowPDFXLS() {
		return doShowPDFXLS;
	}

	public void setDoShowPDFXLS(Boolean doShowPDFXLS) {
		this.doShowPDFXLS = doShowPDFXLS;
	}

	public String getHiddenSelectedAmount() {
		return hiddenSelectedAmount;
	}

	public void setHiddenSelectedAmount(String hiddenSelectedAmount) {
		this.hiddenSelectedAmount = hiddenSelectedAmount;
	}

	public String getDetailFileName() {
		return detailFileName;
	}

	public void setDetailFileName(String detailFileName) {
		this.detailFileName = detailFileName;
	}

	public String getDetailTotalRecord() {
		return detailTotalRecord;
	}

	public void setDetailTotalRecord(String detailTotalRecord) {
		this.detailTotalRecord = detailTotalRecord;
	}

	public String getDetailTotalAmount() {
		return detailTotalAmount;
	}

	public void setDetailTotalAmount(String detailTotalAmount) {
		this.detailTotalAmount = detailTotalAmount;
	}

	public String getDetailType() {
		return detailType;
	}

	public void setDetailType(String detailType) {
		this.detailType = detailType;
	}

	public String getDetailDebitAccount() {
		return detailDebitAccount;
	}

	public void setDetailDebitAccount(String detailDebitAccount) {
		this.detailDebitAccount = detailDebitAccount;
	}

	public String getDetailDebitAccountType() {
		return detailDebitAccountType;
	}

	public void setDetailDebitAccountType(String detailDebitAccountType) {
		this.detailDebitAccountType = detailDebitAccountType;
	}

	public String getDetailDocumentDate() {
		return detailDocumentDate;
	}

	public void setDetailDocumentDate(String detailDocumentDate) {
		this.detailDocumentDate = detailDocumentDate;
	}

	public List<SpanDataSummary> getSummaryList() {
		return summaryList;
	}

	public void setSummaryList(List<SpanDataSummary> summaryList) {
		this.summaryList = summaryList;
	}

	public List<DetailsOutputList> getDetailList() {
		return detailList;
	}

	public void setDetailList(List<DetailsOutputList> detailList) {
		this.detailList = detailList;
	}

}