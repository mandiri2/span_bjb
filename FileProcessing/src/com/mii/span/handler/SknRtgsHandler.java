package com.mii.span.handler;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import mii.span.util.PubUtil;

import com.mii.constant.Constants;
import com.mii.dao.SystemParameterDAO;
import com.mii.helpers.FTPHelper;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.ApplicationInstance;
import com.mii.scoped.SessionBean;
import com.mii.span.dao.SknRtgsDao;
import com.mii.span.dao.SorborAttrDao;
import com.mii.span.model.SknRtgs;
import com.mii.span.model.SorborAttr;

@ManagedBean
@ViewScoped
public class SknRtgsHandler implements Serializable{
	private static final long serialVersionUID = 1L;

	private SknRtgs sknRtgs;
	
	private String searchMessage;
	private boolean doShowSearchMessage;
	private boolean doShowUpdateRetur;
	
	private boolean doShowUpdateMessage;

	private boolean inputRefNo;

	private List<SknRtgs> sknRtgsList;
	
	@ManagedProperty(value = "#{applicationBean}")
	private ApplicationBean applicationBean;

	@ManagedProperty(value = "#{sessionBean}")
	private SessionBean sessionBean;

	private boolean doShowSearch = false;
	
	private boolean doBulkShow = false;

	private String host = null;
	private String username = null;
	private String password = null;
	private String spanPath = null; 
	private String localPath = null;
	private String backupLocalPath = null;
	private String bankName = null;
	private String bankCode = null;

	private int sknRtgsSeq;
	
	private List<String> docNoList;
	
	public void init() {
		applicationBean = setApplicationBean();
		sknRtgsList = new ArrayList<SknRtgs>();
		sknRtgsList = getAllOverbookingList();
		
		doShowUpdateMessage = false;
		sknRtgs = new SknRtgs();
		constructFTPParameter();
	}

	private ApplicationBean setApplicationBean() {
		return ApplicationInstance.getInstance()
				.getApplicationBean();
	}
	
	private List<SknRtgs> getAllOverbookingList() {
		SknRtgsDao sknRtgsDao = getSknRtgsDAOBean();
		return sknRtgsDao.getAllOverbookingList();
	}

	private void constructFTPParameter() {
		host = getSystemParameterDAOBean().getDetailedParameterByParamName(Constants.SPAN_DEPKEU_SFTP_IPADDRESS).getParam_value();
		username = getSystemParameterDAOBean().getDetailedParameterByParamName(Constants.SPAN_DEPKEU_SFTP_USERNAME).getParam_value();
		password = getSystemParameterDAOBean().getDetailedParameterByParamName(Constants.SPAN_DEPKEU_SFTP_PASSWORD).getParam_value();
		spanPath = getSystemParameterDAOBean().getDetailedParameterByParamName(Constants.SPAN_DEPKEU_SFTP_PUT_SORBOR_REMOTE_DIR).getParam_value();
		localPath = getSystemParameterDAOBean().getDetailedParameterByParamName(Constants.SPAN_CREATE_SORBOR_PATH).getParam_value();
		backupLocalPath = getSystemParameterDAOBean().getDetailedParameterByParamName(Constants.JAR_BACKUP_SORBOR).getParam_value();
		bankName = getSystemParameterDAOBean().getDetailedParameterByParamName(Constants.SPAN_BANK_NAME).getParam_value();
		bankCode = getSystemParameterDAOBean().getDetailedParameterByParamName(Constants.SPAN_BANK_CODE).getParam_value();
	}
	
	public void doSave(SknRtgs sknRtgs) {
		SknRtgsDao sknRtgsDao = getSknRtgsDAOBean();
//		boolean isAlreadyProcessed = sknRtgsDao.isAlreadyProcessed(sknRtgs.getDocNo());
//		if(!isAlreadyProcessed){
			sknRtgsDao.updateSknRtgs(sknRtgs);
			docNoList.add(sknRtgs.getDocNo());
			sknRtgsSeq++;
			if(sknRtgsSeq == sknRtgsList.size()){
				String fileName = createSorborFileByScheduller(docNoList);
				String jarFileName = fileName.replace("SBR", "jar");
						PubUtil.concatString(localPath, fileName.replace("SBR", "jar"));
//				boolean createdJar = createJar(PubUtil.concatString(localPath, fileName), PubUtil.concatString(localPath, jarFileName));
				boolean createdJar = createJar(PubUtil.concatString(localPath, fileName),fileName);
				if(createdJar){
					String status = sendFtp(jarFileName);
					if("true".equalsIgnoreCase(status)){
						PubUtil.moveFile(jarFileName, localPath, backupLocalPath);
					}
					else{
						System.out.println("FAILED TO SEND JAR TO FTP. Filename : "+jarFileName);
					}
				}
				else{
					System.out.println("FAILED TO CREATE JAR. Filename : "+fileName);
				}
				sknRtgsSeq = 0;
			}
		}
//	}
	
	/*private boolean createJar(String targetFile, String target) {
		try {
			PubUtil.runCreateJar(targetFile, target);
			return true;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}*/
	
	private boolean createJar(String sorborFile, String fileName) {
		PubUtil.runJarWithoutManifest(sorborFile,fileName);
		//PubUtil.runCreateJar(targetFile, target);
		return true;
	}

	private String sendFtp(String fileName) {
		boolean status = FTPHelper.uploadFileFTP(host, username, password, spanPath, localPath, fileName);
		return String.valueOf(status);
	}

	public String createSorborFile(String docNo){
		String fullPath = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		String bankName="BJB";
		String tanggalFile=sdf.format(new Date());
		String seqFile = "";
		String ext=".SBR";
		String filename = null;

		
		boolean overWrite=true;
		
		SorborAttrDao sorborAttrDao = getSorborAttrDAOBean();
		SorborAttr sorborAttr=sorborAttrDao.getSorborAttrById(docNo);
		try{
			seqFile=padLeft(String.valueOf(sorborAttrDao.getNextValFileSeq()),3,"0"); 
			filename = PubUtil.concatString(bankName, "_",tanggalFile,"_",seqFile,ext);
			fullPath = localPath.concat(filename);
			
			FileWriter fileWriter = new FileWriter(fullPath,overWrite);
			PrintWriter printWriter = new PrintWriter(fileWriter);
			printWriter.print(createSorborLine(sorborAttr));
			printWriter.close();
			System.out.println("here");
			
		}catch(Exception e){
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		return filename;
	}
	
	public String createSorborFileByScheduller(List<String> docNoList){
		String fullPath = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		String bankName="BJB";
		String tanggalFile=sdf.format(new Date());
		String seqFile = "";
		String ext=".SBR";
		String filename = null;

		
		boolean overWrite=true;
		
		try{
			SorborAttrDao sorborAttrDao = getSorborAttrDAOBean();
			seqFile=padLeft(String.valueOf(sorborAttrDao.getNextValFileSeq()),3,"0"); 
			filename = PubUtil.concatString(bankName, "_",tanggalFile,"_",seqFile,ext);
			fullPath = localPath.concat(filename);
			FileWriter fileWriter = new FileWriter(fullPath,overWrite);
			PrintWriter printWriter = new PrintWriter(fileWriter);
		
			for (String docNo : docNoList) {
				SorborAttr sorborAttr = sorborAttrDao.getSorborAttrById(docNo);
				printWriter.println(createSorborLine(sorborAttr));
			}
			printWriter.close();
			System.out.println("here");
			
		}catch(Exception e){
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		return filename;
	}
	
	public static String padLeft(String originalString, int length,
	         String padCharacter) {
	      String paddedString = originalString;
	      while (paddedString.length() < length) {
	         paddedString = padCharacter + paddedString;
	      }
	      return paddedString;
	   }

	
	public String createSorborLine(SorborAttr sorborAttr){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return PubUtil.concatString(sorborAttr.getBankCode(),"|",
									sdf.format(sorborAttr.getPaymentDate()),"|",
									sorborAttr.getDocNo(),"|",
									String.valueOf(sorborAttr.getCheckAmount()),"|",
									sorborAttr.getFtpFileName(),"|",
									sorborAttr.getPaymentMethods(),"|",
									sdf.format(sorborAttr.getSorborDate()),"|",
									sorborAttr.getSorborNo());
	}
	
	public List<String> createSorborFile(List<String> listDocNo){
		List<String> filenames = new ArrayList<String>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		String tanggalFile=sdf.format(new Date());
		String seqFile = "";
		String ext=Constants.SORBOREXT;
		
		boolean overWrite=true;
		
		SorborAttrDao sorborAttrDao = getSorborAttrDAOBean();
		List<SorborAttr> listSorborAttr=sorborAttrDao.getListSorborAttr(listDocNo);
		int listSorborAttrSize =listSorborAttr.size();
		try{
			FileWriter fileWriter=null;
			PrintWriter printWriter=null;
			for (int i=0; i<listSorborAttrSize; i++){
				if(i%100==0){
					if(printWriter!=null){
						printWriter.close();
					}
					seqFile=padLeft(String.valueOf(sorborAttrDao.getNextValFileSeq()),3,"0"); 
					String filename = PubUtil.concatString(bankName, "_",tanggalFile,"_",seqFile,ext);
					String fullPath =localPath.concat(filename);
					filenames.add(filename);
					fileWriter = new FileWriter(fullPath,overWrite);
				}
				SorborAttr sorborAttr = listSorborAttr.get(i);
				sorborAttr.setBankCode(bankCode);
				printWriter = new PrintWriter(fileWriter);
				printWriter.println(createSorborLine(sorborAttr));
			}	
			printWriter.close();
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return filenames;
	}

	public void createSorborByScheduller(){
		sknRtgsSeq = 0;
		docNoList = new ArrayList<String>();
		for(SknRtgs sknRtgs : sknRtgsList){
			doSave(sknRtgs);
		}
	}
	
	private SystemParameterDAO getSystemParameterDaoBean() {
		return (SystemParameterDAO) applicationBean.getFileProcessContext()
				.getBean("sysParamDAO");
	}

	private SknRtgsDao getSknRtgsDAOBean() {
		return (SknRtgsDao) applicationBean.getCorpGWContext().getBean("sknRtgsDao");
	}
	
	private SorborAttrDao getSorborAttrDAOBean() {
		return (SorborAttrDao) applicationBean.getCorpGWContext().getBean("sorborAttrDao");
	}
	
	private SystemParameterDAO getSystemParameterDAOBean(){
		return (SystemParameterDAO) applicationBean.getCorpGWContext().getBean("sysParamDAO");
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSpanPath() {
		return spanPath;
	}

	public void setSpanPath(String spanPath) {
		this.spanPath = spanPath;
	}

	public String getLocalPath() {
		return localPath;
	}

	public void setLocalPath(String localPath) {
		this.localPath = localPath;
	}

	public SknRtgs getSknRtgs() {
		return sknRtgs;
	}

	public void setSknRtgs(SknRtgs sknRtgs) {
		this.sknRtgs = sknRtgs;
	}

	public boolean isInputRefNo() {
		return inputRefNo;
	}

	public void setInputRefNo(boolean inputRefNo) {
		this.inputRefNo = inputRefNo;
	}

	public ApplicationBean getApplicationBean() {
		return applicationBean;
	}

	public void setApplicationBean(ApplicationBean applicationBean) {
		this.applicationBean = applicationBean;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public boolean isDoShowSearch() {
		return doShowSearch;
	}

	public void setDoShowSearch(boolean doShowSearch) {
		this.doShowSearch = doShowSearch;
	}

	public boolean isDoBulkShow() {
		return doBulkShow;
	}


	public void setDoBulkShow(boolean doBulkShow) {
		this.doBulkShow = doBulkShow;
	}

	public String getBackupLocalPath() {
		return backupLocalPath;
	}

	public void setBackupLocalPath(String backupLocalPath) {
		this.backupLocalPath = backupLocalPath;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public boolean isDoShowUpdateMessage() {
		return doShowUpdateMessage;
	}

	public void setDoShowUpdateMessage(boolean doShowUpdateMessage) {
		this.doShowUpdateMessage = doShowUpdateMessage;
	}

	public boolean isDoShowSearchMessage() {
		return doShowSearchMessage;
	}

	public void setDoShowSearchMessage(boolean doShowSearchMessage) {
		this.doShowSearchMessage = doShowSearchMessage;
	}

	public String getSearchMessage() {
		return searchMessage;
	}

	public void setSearchMessage(String searchMessage) {
		this.searchMessage = searchMessage;
	}

	public boolean isDoShowUpdateRetur() {
		return doShowUpdateRetur;
	}

	public void setDoShowUpdateRetur(boolean doShowUpdateRetur) {
		this.doShowUpdateRetur = doShowUpdateRetur;
	}
}
