package com.mii.span.dao;

import java.util.List;

import com.mii.helpers.InterfaceDAO;
import com.mii.span.model.User;
import com.mii.span.model.UserRole;
import com.mii.span.model.UserUpload;

public interface UserDao extends InterfaceDAO {
	public User getUserByIdBranch(String id, String branch);
	public List<User> getAllUser();
	public void saveUserToDB(User user);
	public void saveUserUploadToDB(UserUpload user);
	public void editUser(User user);
	public User deleteUser(String userID);
	public void deleteUserRoleByUsername(String username);
	public List<User> getAllUserLazyLoading(int maxPerPage, int startingAt);
	public List<User> getUserLazyLoadingByCategory(int maxRow, int minRow, String userId, String userName, String fullName, String branch, String roleId, List<String> status);	
	public int countAllUser();
	public int countUserByCategory(String userId, String userName, String fullName, String branch, String roleId, List<String> status);
	public User getDetailedUser(String user);
	public User getByUserName(String userName);
	public User getValidUserByUsername(String username);
	public UserRole getUserRoleByUsernameAndRole(String username, String roleName);
	public List<User> filterUsers(String user, int maxPerPage, int startingAt);
	public int countFilteredUser(String user);
	public UserRole deleteUserRole(String roleId);
	public void insertUserUpload(String USERID, String EMAIL, String PHONE,
			String USERNAME, String FULLNAME, String DIVISION,
			String DEPARTMENT,
			String CREATEWHO, String CREATEDATE, String BRANCH) ;
	public List<User> getAllUserUploadLazyLoading(int maxPerPage,int startingAt, String userID);
	public int countAllUserUpload(String userID);
	public void deleteAllUserUpload(String userID);
	public void updateUserBranchByBranchCode(String oldBranchCode, String newBranchCode);
	public int countUserUploadByUserName(String userName);
	public void insertUserRole(String userId, String roleId);
	public void updateUserRole(String userId, String roleId);
		
}
