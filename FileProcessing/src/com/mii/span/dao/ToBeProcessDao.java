package com.mii.span.dao;

import com.mii.helpers.InterfaceDAO;
import com.mii.span.model.SPANDataValidationErrorStatusModel;
import com.mii.span.process.io.SpanRetryOrReturInput;
import com.mii.span.process.io.SpanRetryOrReturOutput;

public interface ToBeProcessDao extends InterfaceDAO{
	public String getSequenceNo();
	public String getBatchIdSeq();
	public SPANDataValidationErrorStatusModel selectSPANDataValidationErrorStatus(String filename);
	public SpanRetryOrReturOutput spanRetryOrRetur(SpanRetryOrReturInput input);
}
