package com.mii.span.dao;

import java.io.StringWriter;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import mii.span.model.SelectFNDataValidationOutput;
import mii.span.model.SpanDataValidation;

import org.hibernate.transform.ResultTransformer;

import com.mii.helpers.BasicDaoImplement;
import com.mii.span.model.SPANDataValidation;
import com.mii.span.process.io.helper.DetailsOutputList;
import com.mii.span.process.io.helper.DownloadErrorResponse;
import com.mii.span.process.io.helper.OutputDetails;
import com.mii.span.process.io.helper.ReturCodes;
import com.mii.span.process.io.helper.SPANSummaries;
import com.mii.span.process.io.helper.StatusDetailOutput;

public class SPANDataValidationDaoImpl extends BasicDaoImplement implements
		SPANDataValidationDao {

	@SuppressWarnings("unchecked")
	public SPANDataValidation getSPANDataValidationByFilename(String fileName) {
		List<SPANDataValidation> spanDataValidationList = (List<SPANDataValidation>) getCurrentSession()
				.getNamedQuery("SPANValidation#getSPANDataValidationByFilename")
				.setString("fileName", fileName)
				.setResultTransformer(new ResultTransformer() {
					private static final long serialVersionUID = 1L;

					@Override
					public Object transformTuple(Object[] row, String[] arg1) {
						SPANDataValidation spanDataValidation;

						StringWriter w = new StringWriter();
						spanDataValidation = new SPANDataValidation(String
								.valueOf(row[0]), String.valueOf(row[1]),
								String.valueOf(row[2]), String.valueOf(row[3]),
								String.valueOf(row[4]), String.valueOf(row[5]));
						return spanDataValidation;
					}

					@SuppressWarnings("rawtypes")
					@Override
					public List transformList(List arg0) {
						return arg0;
					}
				}).list();
		SPANDataValidation spdv = new SPANDataValidation();
		for (SPANDataValidation spanDataValidation : spanDataValidationList) {
			spdv = spanDataValidation;
			break;
		}
		return spdv;
	}
	
	public List<String> getAllDocumentNumberDataDetailsByFilename(String fileName) {
		List<String> result = (List<String>) getCurrentSession()
				.getNamedQuery("SPANValidation#getAllDocumentNumberDataDetailsByFilename")
				.setString("fileName", fileName).list();
		return result;
	}
	

	@Override
	public void updateUnvoidSpanDataValidation(String totalAmount,
			String totalRecord, String fileName, String procStatus,
			String voidFlag) {
		getCurrentSession()
				.getNamedQuery("SPANValidation#updateUnvoidSpanDataValidation")
				.setString("totalAmount", totalAmount)
				.setString("totalRecord", totalRecord)
				.setString("fileName", fileName)
				.setString("procStatus", procStatus)
				.setString("voidFlag", voidFlag).executeUpdate();
	}

	@Override
	public void updateSpanDataValidationWhenVoid(int recordOri, int amountOri,
			String fileName, boolean voidFileFlag) {
		String query = "";
		if (voidFileFlag) {
			query = "SPANValidation#updateSpanDataValidationWithProcStatusWhenVoid";
		} else {
			query = "SPANValidation#updateSpanDataValidationWhenVoid";
		}
		getCurrentSession().getNamedQuery(query)
				.setString("recordOri", Integer.toString(recordOri))
				.setString("amountOri", Integer.toString(amountOri))
				.setString("fileName", fileName).executeUpdate();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SPANDataValidation> selectSpanDataValidationLateGAJILazyLoading(
			String debitAccount, String transactionFlag, int maxRow, int minRow) {
		List<SPANDataValidation> validation = new ArrayList<SPANDataValidation>();
		try {
			validation = (List<SPANDataValidation>) getCurrentSession()
					.getNamedQuery(
							"SPANValidation#selectSpanDataValidationLateGAJILazyLoading")
					.setString("debitAccount", debitAccount)
					.setString("transactionFlag", transactionFlag)
					.setBigInteger("MAX_ROW", BigInteger.valueOf(maxRow))
					.setBigInteger("MIN_ROW", BigInteger.valueOf(minRow))
					.setResultTransformer(new ResultTransformer() {
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							SPANDataValidation spanDataValidation;
							spanDataValidation = new SPANDataValidation(String
									.valueOf(row[0]), String.valueOf(row[1]),
									String.valueOf(row[2]), String
											.valueOf(row[3]), String
											.valueOf(row[4]), String
											.valueOf(row[5]), String
											.valueOf(row[6]));
							return spanDataValidation;
						}

						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return validation;
	}

	@Override
	public int countSpanDataValidationLateGAJI(String debitAccount,
			String transactionFlag) {
		try {
			BigInteger jml = (BigInteger) getCurrentSession()
					.getNamedQuery(
							"SPANValidation#countSpanDataValidationLateGAJI")
					.setString("debitAccount", debitAccount)
					.setString("transactionFlag", transactionFlag)
					.uniqueResult();
			return jml.intValue();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public void updateLateFileGaji(List<String> documentNumber) {
		try {
			getCurrentSession()
					.getNamedQuery("SPANValidation#updateLateFileGAJI")
					.setParameterList("documentNumberList", documentNumber)
					.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void updateFlagLateFile(List<String> documentNumber) {
		try {
			getCurrentSession()
					.getNamedQuery("SPANValidation#updateFlagLateFile")
					.setParameterList("documentNumberList", documentNumber)
					.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void updateSpanDataValidationSp2dno(String sp2dno) {
		try {
			getCurrentSession()
					.getNamedQuery(
							"SPANValidation#selectSpanDataValidationSp2dno")
					.setString("sp2dno", sp2dno).executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SPANSummaries> selectSPANDataValidationStatusParams(
			List<String> procStatus, String dateStart, String dateEnd, String noRekening) {
		List<SPANSummaries> spanSummaries = new ArrayList<SPANSummaries>();
		try {
			spanSummaries = (List<SPANSummaries>) getCurrentSession()
					.getNamedQuery(
							"SPANValidation#selectSpanDataValidationStatusParams")
					.setParameterList("procStatus", procStatus)
					.setString("dateStart", dateStart)
					.setString("dateEnd", dateEnd)
//					.setString("dateStart", dateStart + " 00:00:00")
//					.setString("dateEnd", dateEnd + " 23:59:59")
					.setString("noRekening", noRekening)
					.setResultTransformer(new ResultTransformer() {
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							SPANSummaries spanSummaries;
							spanSummaries = new SPANSummaries(String
									.valueOf(row[0]), String.valueOf(row[1]),
									String.valueOf(row[2]), String
											.valueOf(row[3]), String
											.valueOf(row[4]), String
											.valueOf(row[5]));
							return spanSummaries;
						}

						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return spanSummaries;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SPANSummaries> getSPANSummaries(List<String> procStatus,
			String legStatus, String tableResponse, String noRekening, String startDate, String endDate) {
		List<SPANSummaries> spanSummaries = new ArrayList<SPANSummaries>();
		try {
			spanSummaries = (List<SPANSummaries>) getCurrentSession()
					.getNamedQuery("SPANValidation#getSPANSummaries")
					.setParameterList("procStatus", procStatus)
					.setString("inputNoRekening", noRekening)
					.setString("startDate", startDate)
					.setString("endDate", endDate)
					.setResultTransformer(new ResultTransformer() {
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							SPANSummaries spanSummaries;
							spanSummaries = new SPANSummaries(String
									.valueOf(row[0]), String.valueOf(row[1])
									.trim(), String.valueOf(row[2]).trim(),
									String.valueOf(row[3]).trim(), String
											.valueOf(row[4]).trim(), String
											.valueOf(row[5]), String
											.valueOf(row[6]), String
											.valueOf(row[7]), String
											.valueOf(row[8]), String
											.valueOf(row[9]), String
											.valueOf(row[10]), String
											.valueOf(row[11]), String
											.valueOf(row[12]), String
											.valueOf(row[13]), String
											.valueOf(row[14]), String
											.valueOf(row[15]), String
											.valueOf(row[16]));
							return spanSummaries;
						}

						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return spanSummaries;
	}

	@Override
	public String getReturDataDetails(String filename) {
		String returACKStatus = "";
		try {
			BigInteger returACKStatusInt = (BigInteger) getCurrentSession()
					.getNamedQuery("SPANValidation#getReturDataDetails")
					.setString("filename", filename).uniqueResult();
			returACKStatus = returACKStatusInt.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return returACKStatus;
	}
	
	@Override
	public String getReturDataDetailsACK(String filename) {
		String returACKStatus = "";
		try {
			BigInteger returACKStatusInt = (BigInteger) getCurrentSession()
					.getNamedQuery("SPANValidation#getReturDataDetailsACK")
					.setString("filename", filename).uniqueResult();
			returACKStatus = returACKStatusInt.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return returACKStatus;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<OutputDetails> selectReturDetails(String legStatus,
			String tableResponse, String procStatus, String trxType,
			String noRekening) {
		List<OutputDetails> outputDetauls = new ArrayList<>();
		try {
			outputDetauls = getCurrentSession()
					.getNamedQuery("SPANValidation#selectReturDetails")
					.setString("inputNoRekening", noRekening)
					.setString("legStatus", legStatus)
					.setString("procStatus", procStatus)
					.setString("trxType", trxType)
					.setResultTransformer(new ResultTransformer() {
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							OutputDetails outputDetails;
							outputDetails = new OutputDetails(String
									.valueOf(row[0]), String.valueOf(row[1]),
									String.valueOf(row[2]));
							return outputDetails;
						}

						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return outputDetauls;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<OutputDetails> selectRetryDetails(String legStatus,
			String tableResponse, String procStatus, String trxType,
			String noRekening, String addingCondition) {
		List<OutputDetails> outputDetails = new ArrayList<>();

		try {
			outputDetails = getCurrentSession()
					.getNamedQuery("SPANValidation#selectRetryDetails")
					.setString("legStatus", legStatus)
					.setString("procStatus", procStatus)
					.setString("trxType", trxType)
					.setString("inputNoRekening", noRekening)
					.setResultTransformer(new ResultTransformer() {
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							OutputDetails outputDetails;
							outputDetails = new OutputDetails(String
									.valueOf(row[0]), String.valueOf(row[1]),
									String.valueOf(row[2]));
							return outputDetails;
						}

						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return outputDetails;
	}

	@Override
	public OutputDetails selectSuccessDetails(String filename, String legStatus) {
		OutputDetails od = new OutputDetails();

		try {
			od = (OutputDetails) getCurrentSession()
					.getNamedQuery("SPANValidation#selectSuccessDetails")
					.setString("filename", filename)
					.setString("legStatus", legStatus)
					.setResultTransformer(new ResultTransformer() {
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							OutputDetails odTemp;
							odTemp = new OutputDetails(String.valueOf(row[0]),
									String.valueOf(row[1]));
							return odTemp;
						}

						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return od;
	}

	@Override
	public StatusDetailOutput checkSuccessStatus(String fileName) {
		StatusDetailOutput sdo = new StatusDetailOutput();
		try {
			sdo = (StatusDetailOutput) getCurrentSession()
					.getNamedQuery("SPANValidation#checkSuccessDetails")
					.setString("filename", fileName)
					.setResultTransformer(new ResultTransformer() {
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							StatusDetailOutput sdoTemp;
							sdoTemp = new StatusDetailOutput(String
									.valueOf(row[0]), String.valueOf(row[1]));
							sdoTemp.setRETRYSTATS(sdoTemp.getRETRYSTATS()
									.replaceAll(".0", ""));
							sdoTemp.setRETURSTATS(sdoTemp.getRETURSTATS()
									.replaceAll(".0", ""));
							return sdoTemp;
						}

						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sdo;
	}

	@SuppressWarnings("unchecked")
	@Override
	public SelectFNDataValidationOutput selectFNDataValidation(String fileName) {
		List<SelectFNDataValidationOutput> output = new ArrayList<SelectFNDataValidationOutput>();
		SelectFNDataValidationOutput selectFNDataValidationOutput = new SelectFNDataValidationOutput();
		output = getCurrentSession()
				.getNamedQuery("SPANValidation#selectFNDataValidation")
				.setString("iFilename", "%" + fileName + "%")
				.setResultTransformer(new ResultTransformer() {
					private static final long serialVersionUID = 1L;

					@Override
					public Object transformTuple(Object[] row, String[] arg1) {
						SelectFNDataValidationOutput selectFNDataValidationOutput;
						selectFNDataValidationOutput = new SelectFNDataValidationOutput(
								String.valueOf(row[0]), String.valueOf(row[1]),
								String.valueOf(row[2]), String.valueOf(row[3]),
								String.valueOf(row[4]));
						return selectFNDataValidationOutput;
					}

					@SuppressWarnings("rawtypes")
					@Override
					public List transformList(List arg0) {
						return arg0;
					}
				}).list();

		for (SelectFNDataValidationOutput out : output) {
			selectFNDataValidationOutput = out;
		}
		return selectFNDataValidationOutput;
	}

	@Override
	public void updateProcStatusAndDescriptionByFilename(String procStatus,
			String description, String fileName) {
		getCurrentSession()
				.getNamedQuery(
						"SPANValidation#updateProcStatusAndDescriptionByFilename")
				.setString("procStatus", procStatus)
				.setString("description", description)
				.setString("fileName", fileName).executeUpdate();

	}

	@Override
	public void updateProcStatusAndDescriptionByFilenameForceACK(
			String procStatus, String description, String fileName) {
		getCurrentSession()
				.getNamedQuery(
						"SPANValidation#updateProcStatusAndDescriptionByFilenameForceACK")
				.setString("procStatus", procStatus)
				.setString("description", description)
				.setString("fileName", fileName).executeUpdate();
	}

	@Override
	public int getODBCTotalProcess(String fileName, String legStatus) {
		java.lang.Double result = (java.lang.Double) getCurrentSession()
				.getNamedQuery("SPANValidation#getODBCTotalProcess")
				.setString("fileName", fileName)
				.setString("legStatus", legStatus).uniqueResult();
		return result.intValue();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DetailsOutputList> getSPANSummaryDetails(String filename,
			String additionalCondition, String legStatus,
			String retryCondition, String processCondition) {
		if(additionalCondition==null){
			additionalCondition = "";
		}
		if(retryCondition==null){
			retryCondition = "";
		}
		if(processCondition==null){
			processCondition = "";
		}
		List<DetailsOutputList> result = new ArrayList<>();
		String sql = "SELECT b.msg_type_id, B.DEBITACCNO, B.AGENT_BANK_ACCT_NAME, B.DOC_DATE, B.CREDITACCNO, "
				+ " B.CREDITACCNAME, B.BENEFICIARYBANKNAME, a.ERRORCODE, a.ERRORMESSAGE, a.DEBITAMOUNT, a.remittanceno, "
				+ " b.trxdetailid, b.doc_number "
				+ " FROM  span_host_response a, span_host_data_details b "
				+ " WHERE "+additionalCondition
				+ " a.batchid = b.batchid "
				+ " AND a.trxdetailid = b.trxdetailid "
				+ " AND a.legstatus = '"+legStatus+"'"
				+ " AND a.batchid IN (SELECT batchid FROM span_host_data_failed WHERE file_name = '"+filename+"'"
				+ " AND legstatus = '"+legStatus+"' "+ retryCondition +" "+ processCondition
				+ " )";
		System.out.println(sql);
		result = getCurrentSession().createSQLQuery(sql)
//				.getNamedQuery("SPANValidation#getSPANSummaryDetails")
//				.setString("filename", filename)
//				.setString("additionalCondition", additionalCondition)
//				.setString("legStatus", legStatus)
//				.setString("retryCondition", retryCondition)
//				.setString("processCondition", processCondition)
				.setResultTransformer(new ResultTransformer() {
					private static final long serialVersionUID = 1L;

					@Override
					public Object transformTuple(Object[] row, String[] arg1) {
						DetailsOutputList detailsOutputList;
						detailsOutputList = new DetailsOutputList(String
								.valueOf(row[7]), String.valueOf(row[8]),
								String.valueOf(row[4]), String.valueOf(row[5]),
								String.valueOf(row[6]), null, String
										.valueOf(row[1]), String
										.valueOf(row[3]), null, String
										.valueOf(row[9]), String
										.valueOf(row[10]), String
										.valueOf(row[11]), String
										.valueOf(row[12]));
						return detailsOutputList;
					}

					@SuppressWarnings("rawtypes")
					@Override
					public List transformList(List arg0) {
						return arg0;
					}
				}).list();
		return result;
	}

	@Override
	public List<StatusDetailOutput> getRetryOrReturStatus(String filename,
			int totalProcess) {
		// TODO Auto-generated method stub
		List<StatusDetailOutput> result = new ArrayList<>();
		result = getCurrentSession()
				.getNamedQuery("SPANValidation#getRetryOrReturStatus")
				.setString("filename", filename)
				.setInteger("totalProcess", totalProcess)
				.setResultTransformer(new ResultTransformer() {
					private static final long serialVersionUID = 1L;

					@Override
					public Object transformTuple(Object[] row, String[] arg1) {
						StatusDetailOutput statusDetailOutput;
						statusDetailOutput = new StatusDetailOutput(String
								.valueOf(row[0]), String.valueOf(row[1]));
						return statusDetailOutput;
					}

					@SuppressWarnings("rawtypes")
					@Override
					public List transformList(List arg0) {
						return arg0;
					}
				}).list();
		return result;
	}

	@Override
	public List<DetailsOutputList> getHoldRecord(String messageId) {
		// TODO Auto-generated method stub
		List<DetailsOutputList> result = new ArrayList<>();
		result = getCurrentSession()
				.getNamedQuery("SPANValidation#getHoldRecord")
				.setString("messageId", messageId)
				.setResultTransformer(new ResultTransformer() {
					private static final long serialVersionUID = 1L;

					@Override
					public Object transformTuple(Object[] row, String[] arg1) {
						DetailsOutputList detailsOutputList;
						detailsOutputList = new DetailsOutputList(String
								.valueOf(row[5]), String.valueOf(row[6]),
								String.valueOf(row[2]), String.valueOf(row[3]),
								String.valueOf(row[4]), null, null, null, null,
								String.valueOf(row[7]), null, null, String
										.valueOf(row[0]));
						return detailsOutputList;
					}

					@SuppressWarnings("rawtypes")
					@Override
					public List transformList(List arg0) {
						return arg0;
					}
				}).list();

		return result;
	}

	@Override
	public String getLegStatus(String filename) {
		String result = "";
		result = (String) getCurrentSession()
				.getNamedQuery("SPANValidation#getLegStatus")
				.setString("fileName", filename).uniqueResult();
		return result;
	}

	@Override
	public List<SPANSummaries> selectSPANDataValidation(String filename) {
		List<SPANSummaries> spanSummaries = new ArrayList<SPANSummaries>();
		try {
			spanSummaries = (List<SPANSummaries>) getCurrentSession()
					.getNamedQuery(
							"SPANValidation#selectSPANDataValidation")
					
					.setString("filename", "%"+filename+"%")
					.setResultTransformer(new ResultTransformer() {
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							SPANSummaries spanSummaries;
							spanSummaries = new SPANSummaries(String
									.valueOf(row[0]), String.valueOf(row[1]),
									String.valueOf(row[2]), String
											.valueOf(row[3]), String
											.valueOf(row[4]), String
											.valueOf(row[5]), String
											.valueOf(row[6]), String
											.valueOf(row[7]), String
											.valueOf(row[8]), String
											.valueOf(row[9]), String
											.valueOf(row[10]), String
											.valueOf(row[11]), String
											.valueOf(row[12]));
							return spanSummaries;
						}

						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return spanSummaries;
	}

	@Override
	public List<ReturCodes> getReturCode(String rcId) {
		List<ReturCodes> result = new ArrayList<>();
		result = getCurrentSession()
				.getNamedQuery("SPANValidation#getReturCode")
				.setString("rcId", rcId)
				.setResultTransformer(new ResultTransformer() {
					private static final long serialVersionUID = 1L;

					@Override
					public Object transformTuple(Object[] row, String[] arg1) {
						ReturCodes returCodes;
						returCodes = new ReturCodes(String
								.valueOf(row[0]), String.valueOf(row[1]));
						return returCodes;
					}

					@SuppressWarnings("rawtypes")
					@Override
					public List transformList(List arg0) {
						return arg0;
					}
				}).list();

		return result;
	}

	@Override
	public List<DownloadErrorResponse> getDownloadErrorResponse(String paymentMethods, String filename) {
		List<DownloadErrorResponse> downloadErrorResponse = getCurrentSession()
				.getNamedQuery("SPANValidation#getDownloadErrorResponse")
//				.setString("paymentMethods", paymentMethods)
				.setString("filename", filename)
				.setResultTransformer(new ResultTransformer() {
					
					@Override
					public Object transformTuple(Object[] row, String[] arg1) {
						DownloadErrorResponse downloadErrorResponse = new DownloadErrorResponse();
						downloadErrorResponse.setFilename(String.valueOf(row[0]));
						downloadErrorResponse.setBatchid(String.valueOf(row[1]));
						downloadErrorResponse.setTrxDetailId(String.valueOf(row[2]));
						downloadErrorResponse.setAmount(String.valueOf(row[3]));
						downloadErrorResponse.setCreditAccNo(String.valueOf(row[4]));
						downloadErrorResponse.setCreditAccName(String.valueOf(row[5]));
						downloadErrorResponse.setErrorCode(String.valueOf(row[6]));
						downloadErrorResponse.setErrorMessage(String.valueOf(row[7]));
						downloadErrorResponse.setPaymentMethod(String.valueOf(row[8]));
						downloadErrorResponse.setDebitAccount(String.valueOf(row[9]));
						downloadErrorResponse.setDebitAccountType(String.valueOf(row[10]));
						downloadErrorResponse.setBenBankCode(String.valueOf(row[11]));
						downloadErrorResponse.setDocNumber(String.valueOf(row[12]));
						downloadErrorResponse.setBenBank(String.valueOf(row[13]));
						downloadErrorResponse.setDescription(String.valueOf(row[14]));
						return downloadErrorResponse;
					}
					
					@Override
					public List transformList(List arg0) {
						return arg0;
					}
				})
				.list();
		return downloadErrorResponse;
	}

	@Override
	public void updateForceAck(String filename) {
		getCurrentSession().createSQLQuery("UPDATE span_data_validation SET FORCED_ACK = '1' WHERE FILE_NAME = :filename")
			.setString("filename", filename)
			.executeUpdate();
	}

	@Override
	public void updateSpanHostResponse(String responseCode, String errorCode, String batchid, String trxDetailId) {
		getCurrentSession().createQuery("UPDATE SpanHostResponse SET responseCode = :responseCode,"
				+ " errorCode = :errorCode,"
				+ " reserve3 = 'forced ack'"
				+ " WHERE batchId = :batchId"
				+ " AND trxDetailId = :trxDetailId")
				.setString("responseCode", responseCode)
				.setString("errorCode", errorCode)
				.setString("batchId", batchid)
				.setString("trxDetailId", trxDetailId)
				.executeUpdate();
	}
	

	@Override
	public String getPostingAmount(String documentDate, String debitAccount) {
		String amount = (String) getCurrentSession().getNamedQuery("SPANValidation#getPostingAmount")
				.setString("documentDate", documentDate)
				.setString("debitAccount", debitAccount)
				.uniqueResult();
		return amount;
	}

	@SuppressWarnings("unchecked")
	@Override
	public SPANDataValidation getSPANDataValidationByFilenameForSummaryDetail(
			String fileName) {
		List<SPANDataValidation> spanDataValidationList = (List<SPANDataValidation>) getCurrentSession()
				.getNamedQuery("SPANValidation#getSPANDataValidationByFilenameForSummaryDetail")
				.setString("fileName", fileName)
				.setResultTransformer(new ResultTransformer() {
					private static final long serialVersionUID = 1L;

					@Override
					public Object transformTuple(Object[] row, String[] arg1) {
						SPANDataValidation spanDataValidation;
						spanDataValidation = new SPANDataValidation(
								String.valueOf(row[0]), 
								String.valueOf(row[1]),
								String.valueOf(row[2]), 
								String.valueOf(row[3]),
								String.valueOf(row[4]), 
								String.valueOf(row[5]),
								String.valueOf(row[6]),
								"");
						return spanDataValidation;
					}
					@SuppressWarnings("rawtypes")
					@Override
					public List transformList(List arg0) {
						return arg0;
					}
				}).list();
		SPANDataValidation spdv = new SPANDataValidation();
		for (SPANDataValidation spanDataValidation : spanDataValidationList) {
			spdv = spanDataValidation;
			break;
		}
		return spdv;
	}
	
	@Override
	public SpanDataValidation getSpanDataValidationBySpanHostDataDetails(String fileName) {
		SpanDataValidation result = (SpanDataValidation) getCurrentSession().
				getNamedQuery("SpanDataValidation#getSpanDataValidationBySpanHostDataDetails")
				.setString("fileName", fileName)
				.uniqueResult();
		return result;
	}
}
