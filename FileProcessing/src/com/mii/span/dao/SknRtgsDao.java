package com.mii.span.dao;

import java.util.List;

import com.mii.helpers.InterfaceDAO;
import com.mii.span.model.SknRtgs;

public interface SknRtgsDao extends InterfaceDAO {
	public boolean isAlreadyProcessed(String docnumber);
	public void updateSknRtgs(SknRtgs sknRtgs);
	public List<SknRtgs> getAllOverbookingList();
}
