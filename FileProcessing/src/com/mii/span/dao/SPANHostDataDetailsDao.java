package com.mii.span.dao;

import java.util.Date;
import java.util.List;

import mii.span.model.SpanHostResponseCode;

import com.mii.helpers.InterfaceDAO;
import com.mii.span.model.InternalReportBJB;
import com.mii.span.model.SPANHostDataDetails;

public interface SPANHostDataDetailsDao  extends InterfaceDAO{
	public List<SPANHostDataDetails> getAll();
	public List<SPANHostDataDetails> getAllReturLazyLoading(int maxRow, int minRow, String debitAccount);
	public List<SPANHostDataDetails> getAllRetryLazyLoading(int maxRow, int minRow, String debitAccount);
	public int countAllDetailsRetur(String debitAccount);
	public int countAllDetailsRetry(String debitAccount);
	public SPANHostDataDetails getSearchDocumentNumber(String documentNumber);
	public void updateVoidSpanHostDataDetails (String documentNumber);
	public void updateBulkVoidSpanHostDataDetails (String sp2dNumber);
	public List<InternalReportBJB> getTransactionReportLazyLoading(int maxRow, int minRow, Date startDocDate, Date endDocDate, String accountCodeNumber, String paymentMethod);
	public int getRowCountTransactionReport(Date start, Date end, String accountCodeNumber, String paymentMethod);
	public List<SpanHostResponseCode> getTransactionStatus();
}
