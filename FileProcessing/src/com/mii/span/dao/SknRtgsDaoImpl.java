package com.mii.span.dao;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import mii.span.util.PubUtil;

import org.hibernate.transform.ResultTransformer;

import com.mii.helpers.BasicDaoImplement;
import com.mii.span.model.SknRtgs;

public class SknRtgsDaoImpl extends BasicDaoImplement implements SknRtgsDao {
	@Override
	public List<SknRtgs> getAllOverbookingList() {
		// TODO Auto-generated method stub
		List<SknRtgs> sknRtgsList = new ArrayList<SknRtgs>();
		try{
			sknRtgsList = (List<SknRtgs>)getCurrentSession().
					getNamedQuery("SknRtgsHandler#getAllOverbookingList")
					.setResultTransformer(new ResultTransformer() {						
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							SknRtgs sknRtgs;
							try {
								sknRtgs = new SknRtgs(
										String.valueOf(row[0]), 
										String.valueOf(row[1]), 
										String.valueOf(row[2]),
										String.valueOf(row[3]),
										String.valueOf(PubUtil.stringIfNotNull(row[4])),
										String.valueOf(PubUtil.stringIfNotNull(row[5])),
										String.valueOf(row[6]),
										String.valueOf(row[7])
										);
								return sknRtgs;
							} catch (Exception e) {
								e.printStackTrace();
							}
							return null;
						}
						
						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).
					list();
		}catch(Exception e){
			e.printStackTrace();
		}
		return sknRtgsList;
	}

	@Override
	public boolean isAlreadyProcessed(String docnumber) {
		BigInteger count = (BigInteger) getCurrentSession().getNamedQuery("isAlreadyProcessed")
				.setString("docNumber", docnumber)
				.uniqueResult();
		if(!PubUtil.isBigIntegerEmpty(count)){
			return true;
		}
		return false;
	}
	
	@Override
	public void updateSknRtgs(SknRtgs sknRtgs) {
		
		
		getCurrentSession().getNamedQuery("updateSknRtgs").
		setString("REMITTANCE_NO", sknRtgs.getRemittanceNo()).
		setString("SORBOR_NO", sknRtgs.getsorborNo()).
		setString("CREDIT_ACC_NO", sknRtgs.getCreditAccNo()).
		setString("DOC_NO", sknRtgs.getDocNo()).
		executeUpdate();		
	}
}
