package com.mii.span.dao;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.transform.ResultTransformer;

import com.mii.helpers.BasicDaoImplement;
import com.mii.span.model.SPANDataValidationErrorStatusModel;
import com.mii.span.process.io.SpanRetryOrReturInput;
import com.mii.span.process.io.SpanRetryOrReturOutput;

public class ToBeProcessDaoImpl extends BasicDaoImplement implements ToBeProcessDao{

	@Override
	public String getSequenceNo() {
		BigInteger value = (BigInteger) getCurrentSession().getNamedQuery("ToBeProcess#getSequenceNo").uniqueResult();
		return value.toString();
	}

	@Override
	public String getBatchIdSeq() {
		BigInteger value = (BigInteger) getCurrentSession().getNamedQuery("ToBeProcess#getBatchIdSeq").uniqueResult();
		return value.toString();
	}

	@SuppressWarnings("unchecked")
	@Override
	public SPANDataValidationErrorStatusModel selectSPANDataValidationErrorStatus(String filename) {
		SPANDataValidationErrorStatusModel error = new SPANDataValidationErrorStatusModel();
		try{
			List<SPANDataValidationErrorStatusModel> result = new ArrayList<SPANDataValidationErrorStatusModel>();
			result = getCurrentSession().getNamedQuery("ToBeProcess#selectSPANDataValidationErrorStatus").
					setString("filename", filename).
					setResultTransformer(new ResultTransformer() {
						private static final long serialVersionUID = 1L;
						@Override
						public Object transformTuple(Object[] row, String[] arg1) {

							SPANDataValidationErrorStatusModel errorStatus;
							errorStatus = new SPANDataValidationErrorStatusModel(
									String.valueOf(row[0]), 
									String.valueOf(row[1]));
								return errorStatus;
						}
						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).list();
			error = result.get(0);
		}catch(Exception e){
			e.printStackTrace();
		}
		return error;
	}

	@SuppressWarnings("unchecked")
	@Override
	public SpanRetryOrReturOutput spanRetryOrRetur(SpanRetryOrReturInput input) {
		List<SpanRetryOrReturOutput> output = new ArrayList<SpanRetryOrReturOutput>();
		SpanRetryOrReturOutput toBeExecuteOutput = new SpanRetryOrReturOutput();
		output = getCurrentSession().getNamedQuery("ToBeProcess#spanRetryOrRetur")
				.setString("ibatchid", input.getiBatchId())
				.setString("itrxheaderid", input.getiTrxHeaderId())
				.setString("ifilename", input.getiFileName())
				.setString("iretry", input.getiRetry())
				.setString("icreditacct", input.getiCreditAcct())
				.setString("icreditacctype", input.getiCreditAccType())
				.setString("ichannelid", input.getiChannelId())
				.setString("istatus", input.getiStatus())
				.setString("ipriority", input.getiPriority())
				.setString("idebitaccountsc", input.getiDebitAccountSC())
				.setString("ilegstatus", input.getiLegStatus()).
				setResultTransformer(new ResultTransformer() {
					private static final long serialVersionUID = 1L;
					@Override
					public Object transformTuple(Object[] row, String[] arg1) {
						SpanRetryOrReturOutput toBeExecuteOutput;
						toBeExecuteOutput = new SpanRetryOrReturOutput(
								String.valueOf(row[0]), 
								String.valueOf(row[1]));
						return toBeExecuteOutput;
					}
					@SuppressWarnings("rawtypes")
					@Override
					public List transformList(List arg0) {
						return arg0;
					}}).list();
		
			for(SpanRetryOrReturOutput out : output){
				toBeExecuteOutput = out;
			}
		return toBeExecuteOutput;
	}

}
