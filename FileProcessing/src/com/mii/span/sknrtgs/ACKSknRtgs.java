package com.mii.span.sknrtgs;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mii.span.Log;
import mii.span.constant.MdrCfgMainConstant;
import mii.span.doc.ACKDataDocument;
import mii.span.process.model.MappingDataACKSpanRequest;
import mii.span.process.model.SentAckDataFileRequest;
import mii.span.process.model.SentAckDataFileResponse;
import mii.span.sftp.Services;
import mii.span.util.Atomic;
import mii.span.util.BusinessUtil;
import mii.span.util.PubUtil;

import com.mii.dao.SystemParameterDAO;
import com.mii.models.SystemParameter;

public class ACKSknRtgs{
	
	public void createACKSknRtgs(MappingDataACKSpanRequest request, SystemParameterDAO systemParameterDao){
		String ackString = "";
		try {
			ackString = mappingDataACKSpan(request);
			
			MappingACKExecutedResponse response = new MappingACKExecutedResponse();
			response.setACKDataString(ackString);
			response.setStatus("SUCCESS");
			response.setErrorMessage("");
			response.setDocumentType(request.getAckDataDocument().getDocumentType());
			
			String responseStatus = response.getStatus();
			String checkpoint = "SENDING";
			String statussend = "";
			
			if("SUCCESS".equalsIgnoreCase(responseStatus)){
				
				SentAckDataFileRequest sendAckRequest = new SentAckDataFileRequest();
				sendAckRequest.setAckDataString(response.getACKDataString());
				sendAckRequest.setDocumentType(response.getDocumentType());
				sendAckRequest.setErrorMessage(response.getErrorMessage());
				sendAckRequest.setFilename(request.getAckDataDocument().getFileName());
				/*
				 * CALL SERVICE SENDACKDATAFILE -> same service di spanDataValidation
				 */
				SentAckDataFileResponse sendAckResponse = new SentAckDataFileResponse();
				sendAckResponse = sentACKDataFile(sendAckRequest, systemParameterDao);
				
//				statussend = sendAckResponse.getStatusSend();
				
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static final String mappingDataACKSpan(MappingDataACKSpanRequest request) throws Exception {
		ACKDataDocument dataDoc = request.getAckDataDocument();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");            
		Date dateTime = null; 
		String newDateTime = null;
		
		String DocumentDate = dataDoc.getDocumentDate();
		
		if("XXXXXXXXXXXXXXX".equals(DocumentDate)){
		    	newDateTime = DocumentDate;
		}else{
			if(DocumentDate!=null && DocumentDate.length()>0){
				try{
					dateTime = sdf.parse(DocumentDate);	
					newDateTime = sdf.format(dateTime);
				} catch (ParseException ex) {
		            		throw new Exception(ex.getMessage());
			        }
			}
		}
			
		String DocumentType = dataDoc.getDocumentType();
		if(DocumentType==null || DocumentType.length()<=0)DocumentType="";
		
		String DocumentNo   = dataDoc.getDocumentNo();
		if(DocumentNo==null || DocumentNo.length()<=0)DocumentNo="";
		
		String FileName	    = dataDoc.getFileName();
		if(FileName==null || FileName.length()<=0)FileName="";
		
		String ReturnCode   = dataDoc.getReturnCode();
		if(ReturnCode==null || ReturnCode.length()<=0)ReturnCode="";
		
		String Description  = dataDoc.getDescription();
		if(Description==null || Description.trim().length()<=0)Description="SUKSES";
		
		String ACKDataString = newDateTime.concat("|").concat(PubUtil.padRight(DocumentType,4).concat("|")).concat(
					PubUtil.padRight(DocumentNo,21).concat("|")).concat(
					PubUtil.padRight(FileName,100).concat("|")).concat(
					PubUtil.padLeft(ReturnCode,"0",4).concat("|")).concat(
					PubUtil.padRight(Description,  100));
		
		return ACKDataString;
	}
	
	public static SentAckDataFileResponse sentACKDataFile(SentAckDataFileRequest request, SystemParameterDAO systemParameterDao){
		SentAckDataFileResponse response = new SentAckDataFileResponse();
		String Status = null;
		String ErrorMessage = request.getErrorMessage();
		String ACKFilename = null;
		String statussend = null;
		String DocumentType = request.getDocumentType();
		String filename = request.getFilename();
		String ACKDataString = request.getAckDataString();
		//Sequence	preparation for send ack to span
		try {
			SystemParameter systemParameterspanHost =  systemParameterDao.getDetailedParameterByParamName(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_IPADDRESS);
			String spanHost = systemParameterspanHost.getParam_value();
			SystemParameter systemParameterspanPort =  systemParameterDao.getDetailedParameterByParamName(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_PORT);
			String spanPort = systemParameterspanPort.getParam_value();
			SystemParameter systemParameterLOCAL_DIR =  systemParameterDao.getDetailedParameterByParamName(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_PUT_LOCAL_DIR);
			String LOCAL_DIR = systemParameterLOCAL_DIR.getParam_value();
			SystemParameter systemParameterspanUser =  systemParameterDao.getDetailedParameterByParamName(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_USERNAME);
			String spanUser = systemParameterspanUser.getParam_value();
			SystemParameter systemParameterspanPass =  systemParameterDao.getDetailedParameterByParamName(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_PASSWORD);
			String spanPass = systemParameterspanPass.getParam_value();
			SystemParameter systemParameterspanACKDir =  systemParameterDao.getDetailedParameterByParamName(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_PUT_REMOTE_DIR);
			String spanACKDir = systemParameterspanACKDir.getParam_value();
			SystemParameter systemParameterBACKUP_DIR =  systemParameterDao.getDetailedParameterByParamName(MdrCfgMainConstant.SPAN_DEPKEU_BJB_BACKUP_FTP_PUT_DIR);
			String BACKUP_DIR = systemParameterBACKUP_DIR.getParam_value();
			SystemParameter systemParametermdrHost =  systemParameterDao.getDetailedParameterByParamName(MdrCfgMainConstant.SPAN_DEPKEU_BJB_BACKUP_FTP_PUT_DIR);
			String mdrHost = systemParametermdrHost.getParam_value();
			SystemParameter systemParametermdrPort =  systemParameterDao.getDetailedParameterByParamName(MdrCfgMainConstant.SPAN_DEPKEU_BJB_BACKUP_FTP_PORT);
			String mdrPort = systemParametermdrPort.getParam_value();
			SystemParameter systemParametermdrUser =  systemParameterDao.getDetailedParameterByParamName(MdrCfgMainConstant.SPAN_DEPKEU_BJB_BACKUP_FTP_USERNAME);
			String mdrUser = systemParametermdrUser.getParam_value();
			SystemParameter systemParametermdrPass =  systemParameterDao.getDetailedParameterByParamName(MdrCfgMainConstant.SPAN_DEPKEU_BJB_BACKUP_FTP_PASSWORD);
			String mdrPass = systemParametermdrPass.getParam_value();
			SystemParameter systemParameterspanOutboundDir =  systemParameterDao.getDetailedParameterByParamName(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_GET_REMOTE_DIR);
			String spanOutboundDir = systemParameterspanOutboundDir.getParam_value();
			
			List<String> spanFolderNames = new ArrayList<String>();
			int indexValue = 0;
			//sequence for outbound folder
				spanFolderNames = BusinessUtil.getFolderName(spanOutboundDir);
				
				//loop spanFolderNames
					for(String spanFolderNames2 : spanFolderNames){
						indexValue = spanFolderNames2.indexOf(DocumentType);
						
						//branch	
							//%indexValue%>0
							if(indexValue>0){
								spanOutboundDir = spanFolderNames2;
							}
					}
			
			//sequence for inbound folder
					spanFolderNames = BusinessUtil.getFolderName(spanACKDir);
					
				//loop spanFolderNames
					for(String spanFolderNames2 : spanFolderNames){
						indexValue = spanFolderNames2.indexOf(DocumentType);
						
						//branch
							//%indexValue%>0
							if(indexValue>0){
								spanACKDir = PubUtil.concatString(spanFolderNames2, "_ACK");
							}
					}
			
					
			//(FA)
				String newFileNameFA = BusinessUtil.renameFileName(filename, PubUtil.concatString(DocumentType,"_FA"));
			
			//jar --> txt
				newFileNameFA = newFileNameFA.replace("jar", "txt");
				
			//xml --> txt
				newFileNameFA = newFileNameFA.replace("xml", "txt");
				
			//mandiri.span.util.atomic:ACKStringToBytes
				Object[] ACKStringToBytesResult = Atomic.ACKStringToBytes(ACKDataString);
				byte[] ACKbytes = BusinessUtil.bytesIfNotNull(ACKStringToBytesResult[0]);
				Status = BusinessUtil.stringIfNotNull(ACKStringToBytesResult[1]);
				ErrorMessage = BusinessUtil.stringIfNotNull(ACKStringToBytesResult[2]);
				
			//create file FA in /tmp
				PubUtil.bytesToFile(PubUtil.concatString(LOCAL_DIR,newFileNameFA), ACKbytes, null);
				
			System.out.println("success create file");
			
			//sequence SFTP - ACK 
				//sequence sent file ack to inbound
					//mandiri.span.util.atomic:createAndExtractJAR
						String[] createAndExtractJARResult = Atomic.createAndExtractJAR(newFileNameFA, LOCAL_DIR, "C");
						Status = BusinessUtil.stringIfNotNull(createAndExtractJARResult[0]);
						ErrorMessage = BusinessUtil.stringIfNotNull(createAndExtractJARResult[1]);
						
					//create jar file name
						String jarFileName = newFileNameFA.replace(".txt", ".jar");
					
					//send functional ack
					//mandiri.span.sftp.services:uploadFile
					statussend = Services.uploadFile(spanACKDir, LOCAL_DIR, jarFileName, spanHost, spanUser, spanPass);
					Status = statussend;
					
					//branch on Status\
						String StatusJAR = null;
						if("SUCCESS".equalsIgnoreCase(Status)){
							StatusJAR = "SUCCESS";
						}
						else{
							//do nothing
						}
						
				//debug log
			
			//delete F-ACK
				String status = PubUtil.deleteFile(PubUtil.concatString(LOCAL_DIR,newFileNameFA));
			
			//branch on /ErrorMessage
				
				//debug log
				
				if(ErrorMessage==null){
					Log.debugLog("SPAN", "sendACKDataFile", "INFO", 
							PubUtil.concatString(Status," to send ACK and delete file in SPAN folder : ",status)
							);
				}
				else{
					//default
					Log.debugLog("SPAN", "sendACKDataFile", "INFO", 
							PubUtil.concatString(Status," to send ACK and delete file in SPAN folder : ",status,"-",ErrorMessage)
							);
				}
			
			ACKFilename = jarFileName;
			ErrorMessage = "";
			Status = "SUCCESS";
					
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
		response.setStatus(Status);
		response.setErrorMessage(ErrorMessage);
		response.setAckFileName(ACKFilename);
		response.setStatusSend(statussend);
		
		return response;
	}
}
