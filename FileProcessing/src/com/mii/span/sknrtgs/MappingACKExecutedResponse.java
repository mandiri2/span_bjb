package com.mii.span.sknrtgs;

public class MappingACKExecutedResponse {
	public String ACKDataString,
	Status,
	ErrorMessage,
	DocumentType;

	public String getACKDataString() {
		return ACKDataString;
	}

	public void setACKDataString(String aCKDataString) {
		ACKDataString = aCKDataString;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public String getErrorMessage() {
		return ErrorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		ErrorMessage = errorMessage;
	}

	public String getDocumentType() {
		return DocumentType;
	}

	public void setDocumentType(String documentType) {
		DocumentType = documentType;
	}
}
