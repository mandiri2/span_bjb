package com.mii.span.scheduller;

import com.mii.constant.Constants;
import com.mii.span.handler.ResultToBeProcessHandler;

public class ResultToBeProcessScheduller {
	public void run(String accountType, String trxType){
		ResultToBeProcessHandler handler = new ResultToBeProcessHandler();
		
		//handler.InquirySaldo();
		String message = "Start RTBP Scheduller for ";
		if(Constants.GAJI.equals(accountType)){
			handler.doClickGaji();
			message = message.concat(Constants.GAJI+" - ");
		}else if(Constants.GAJIR.equals(accountType)){
			handler.doClickGajiR();
			message = message.concat(Constants.GAJIR+" - ");
		}
		
		if(Constants.RETUR.equals(trxType)){
			handler.doClickRetur();
			message = message.concat(Constants.RETUR);
		}else if(Constants.RETRY.equals(trxType)){
			handler.doClickRetry();
			message = message.concat(Constants.RETRY);
		}else if(Constants.SEND_ACK.equals(trxType)){
			handler.doClickSendACK();
			message = message.concat(Constants.SEND_ACK);
		}
		System.out.println(message);
		handler.ExecuteChecked();
	}
}
