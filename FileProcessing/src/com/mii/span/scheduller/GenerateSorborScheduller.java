package com.mii.span.scheduller;

import com.mii.span.handler.SknRtgsHandler;

public class GenerateSorborScheduller {
	public void run(){
		System.out.println("Start GenerateSorborScheduller");
		SknRtgsHandler handler = new SknRtgsHandler();
		handler.init();
		handler.createSorborByScheduller();
		System.out.println("End GenerateSorborScheduller");
	}
	
	public static void main(String[] args) {
		GenerateSorborScheduller scheduller = new GenerateSorborScheduller();
		scheduller.run();
	}
}
