package com.mii.models;

import java.io.Serializable;

public class ReconFileM implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String reconTime;
	private String caID;
	private String caName;
	private String settlementDate;
	private String currency;
	private String billingCode;
	private String ntb;
	private String ntpn;
	private String amount;
	private String noSakti;
	
	public String getReconTime() {
		return reconTime;
	}
	public void setReconTime(String reconTime) {
		this.reconTime = reconTime;
	}
	public String getCaID() {
		return caID;
	}
	public void setCaID(String caID) {
		this.caID = caID;
	}
	public String getCaName() {
		return caName;
	}
	public void setCaName(String caName) {
		this.caName = caName;
	}
	public String getSettlementDate() {
		return settlementDate;
	}
	public void setSettlementDate(String settlementDate) {
		this.settlementDate = settlementDate;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getBillingCode() {
		return billingCode;
	}
	public void setBillingCode(String billingCode) {
		this.billingCode = billingCode;
	}
	public String getNtb() {
		return ntb;
	}
	public void setNtb(String ntbB) {
		this.ntb = ntbB;
	}
	public String getNtpn() {
		return ntpn;
	}
	public void setNtpn(String ntpn) {
		this.ntpn = ntpn;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getNoSakti() {
		return noSakti;
	}
	public void setNoSakti(String noSakti) {
		this.noSakti = noSakti;
	}
	
}
