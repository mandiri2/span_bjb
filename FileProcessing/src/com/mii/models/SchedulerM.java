package com.mii.models;

import java.io.Serializable;

public class SchedulerM implements Serializable{

	private static final long serialVersionUID = 1L;

	private String schedulerID; 
	private String createDate;
	private String description;
	private String jobName; 
	private String executionService; 
	private String schedulerType; 
	private String date; 
	private String time; 
	private String interval; 
	private String startDate; 
	private String endDate; 
	private String startTime; 
	private String endTime; 
	private String maskYear; 
	private String maskMonth; 
	private String maskDay; 
	private String maskWeeklyDay; 
	private String maskHour; 
	private String maskMinute; 
	private String maskSecond;
	private String status;
	
	
	public String getSchedulerID() {
		return schedulerID;
	}
	public void setSchedulerID(String schedulerID) {
		this.schedulerID = schedulerID;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public String getExecutionService() {
		return executionService;
	}
	public void setExecutionService(String executionService) {
		this.executionService = executionService;
	}
	public String getSchedulerType() {
		return schedulerType;
	}
	public void setSchedulerType(String schedulerType) {
		this.schedulerType = schedulerType;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getInterval() {
		return interval;
	}
	public void setInterval(String interval) {
		this.interval = interval;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getMaskYear() {
		return maskYear;
	}
	public void setMaskYear(String maskYear) {
		this.maskYear = maskYear;
	}
	public String getMaskMonth() {
		return maskMonth;
	}
	public void setMaskMonth(String maskMonth) {
		this.maskMonth = maskMonth;
	}
	public String getMaskDay() {
		return maskDay;
	}
	public void setMaskDay(String maskDay) {
		this.maskDay = maskDay;
	}
	public String getMaskWeeklyDay() {
		return maskWeeklyDay;
	}
	public void setMaskWeeklyDay(String maskWeeklyDay) {
		this.maskWeeklyDay = maskWeeklyDay;
	}
	public String getMaskHour() {
		return maskHour;
	}
	public void setMaskHour(String maskHour) {
		this.maskHour = maskHour;
	}
	public String getMaskMinute() {
		return maskMinute;
	}
	public void setMaskMinute(String maskMinute) {
		this.maskMinute = maskMinute;
	}
	public String getMaskSecond() {
		return maskSecond;
	}
	public void setMaskSecond(String maskSecond) {
		this.maskSecond = maskSecond;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
