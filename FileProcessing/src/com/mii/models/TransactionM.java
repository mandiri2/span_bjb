package com.mii.models;

import java.io.Serializable;
import java.util.Date;

public class TransactionM implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String id;
	private String transactionCode;
	private Date createDate;
	private Date updateDate;
	private String bankID;
	private String billingInfo1;
	private String billingInfo2;
	private String billingInfo3;
	private String billingInfo4;
	private String billingInfo5;
	private String billingInfo6;
	private String billingInfo7;
	private String billingInfo8;
	private String billingInfo9;
	private String billingInfo10;
	private String branchCode;
	private String channelType;
	private String currency;
	private Date gmt;
	private Date localDatetime;
	private String settlementDate;
	private String switcherCode;
	private String terminalId;
	private String terminalLocation;
	private String transactionId;
	private String paymentCode;
	private String amount;
	private String customerName;
	private String billerAccountNumber;
	private String ntb;
	private String ntpn;
	private String noSakti;
	private String sourceacct;
	private String transactionType;
	private String referenceNo;
	private String pelimpahanID;
	private String reserved1;
	private String reserved2;
	private String reserved3;
	private String reserved4;
	private String reserved5;
	private String responseCode;
	
		
	public String getResponseCode() {
		return responseCode;
	}
	
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTransactionCode() {
		return transactionCode;
	}
	public void setTransactionCode(String transactionCode) {
		this.transactionCode = transactionCode;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public String getBankID() {
		return bankID;
	}
	public void setBankID(String bankID) {
		this.bankID = bankID;
	}
	public String getBillingInfo1() {
		return billingInfo1;
	}
	public void setBillingInfo1(String billingInfo1) {
		this.billingInfo1 = billingInfo1;
	}
	public String getBillingInfo2() {
		return billingInfo2;
	}
	public void setBillingInfo2(String billingInfo2) {
		this.billingInfo2 = billingInfo2;
	}
	public String getBillingInfo3() {
		return billingInfo3;
	}
	public void setBillingInfo3(String billingInfo3) {
		this.billingInfo3 = billingInfo3;
	}
	public String getBillingInfo4() {
		return billingInfo4;
	}
	public void setBillingInfo4(String billingInfo4) {
		this.billingInfo4 = billingInfo4;
	}
	public String getBillingInfo5() {
		return billingInfo5;
	}
	public void setBillingInfo5(String billingInfo5) {
		this.billingInfo5 = billingInfo5;
	}
	public String getBillingInfo6() {
		return billingInfo6;
	}
	public void setBillingInfo6(String billingInfo6) {
		this.billingInfo6 = billingInfo6;
	}
	public String getBillingInfo7() {
		return billingInfo7;
	}
	public void setBillingInfo7(String billingInfo7) {
		this.billingInfo7 = billingInfo7;
	}
	public String getBillingInfo8() {
		return billingInfo8;
	}
	public void setBillingInfo8(String billingInfo8) {
		this.billingInfo8 = billingInfo8;
	}
	public String getBillingInfo9() {
		return billingInfo9;
	}
	public void setBillingInfo9(String billingInfo9) {
		this.billingInfo9 = billingInfo9;
	}
	public String getBillingInfo10() {
		return billingInfo10;
	}
	public void setBillingInfo10(String billingInfo10) {
		this.billingInfo10 = billingInfo10;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getChannelType() {
		return channelType;
	}
	public void setChannelType(String channelType) {
		this.channelType = channelType;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public Date getGmt() {
		return gmt;
	}
	public void setGmt(Date gmt) {
		this.gmt = gmt;
	}
	public Date getLocalDatetime() {
		return localDatetime;
	}
	public void setLocalDatetime(Date localDatetime) {
		this.localDatetime = localDatetime;
	}
	public String getSettlementDate() {
		return settlementDate;
	}
	public void setSettlementDate(String settlementDate) {
		this.settlementDate = settlementDate;
	}
	public String getSwitcherCode() {
		return switcherCode;
	}
	public void setSwitcherCode(String switcherCode) {
		this.switcherCode = switcherCode;
	}
	public String getTerminalId() {
		return terminalId;
	}
	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}
	public String getTerminalLocation() {
		return terminalLocation;
	}
	public void setTerminalLocation(String terminalLocation) {
		this.terminalLocation = terminalLocation;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public String getPaymentCode() {
		return paymentCode;
	}
	public void setPaymentCode(String paymentCode) {
		this.paymentCode = paymentCode;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getBillerAccountNumber() {
		return billerAccountNumber;
	}
	public void setBillerAccountNumber(String billerAccountNumber) {
		this.billerAccountNumber = billerAccountNumber;
	}
	public String getNtb() {
		return ntb;
	}
	public void setNtb(String ntb) {
		this.ntb = ntb;
	}
	public String getNtpn() {
		return ntpn;
	}
	public void setNtpn(String ntpn) {
		this.ntpn = ntpn;
	}
	public String getNoSakti() {
		return noSakti;
	}
	public void setNoSakti(String noSakti) {
		this.noSakti = noSakti;
	}
	public String getSourceacct() {
		return sourceacct;
	}
	public void setSourceacct(String sourceacct) {
		this.sourceacct = sourceacct;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	public String getReferenceNo() {
		return referenceNo;
	}
	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}
	public String getPelimpahanID() {
		return pelimpahanID;
	}
	public void setPelimpahanID(String pelimpahanID) {
		this.pelimpahanID = pelimpahanID;
	}
	public String getReserved1() {
		return reserved1;
	}
	public void setReserved1(String reserved1) {
		this.reserved1 = reserved1;
	}
	public String getReserved2() {
		return reserved2;
	}
	public void setReserved2(String reserved2) {
		this.reserved2 = reserved2;
	}
	public String getReserved3() {
		return reserved3;
	}
	public void setReserved3(String reserved3) {
		this.reserved3 = reserved3;
	}
	public String getReserved4() {
		return reserved4;
	}
	public void setReserved4(String reserved4) {
		this.reserved4 = reserved4;
	}
	public String getReserved5() {
		return reserved5;
	}
	public void setReserved5(String reserved5) {
		this.reserved5 = reserved5;
	}
}
