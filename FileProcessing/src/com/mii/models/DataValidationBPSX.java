package com.mii.models;

import java.io.Serializable;

public class DataValidationBPSX implements Serializable{
	private static final long serialVersionUID = 1L;
	String id;
	String flagJurnal;
	String trxDebitCode;
	String debitAcctNo;
	String trxCreditCode;
	String creditAcctNo;
	String trxAmount;
	String reffNumber;
	String narrative1;
	String narrative2;
	String narrative3;
	String narrative4;
	String currency;
	String effectiveDate;
	String feeAmount;
	String costCenter;
	String branch;
	String tellerID;
	String reserveField;
	String flagAction;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFlagJurnal() {
		return flagJurnal;
	}
	public void setFlagJurnal(String flagJurnal) {
		this.flagJurnal = flagJurnal;
	}
	public String getTrxDebitCode() {
		return trxDebitCode;
	}
	public void setTrxDebitCode(String trxDebitCode) {
		this.trxDebitCode = trxDebitCode;
	}
	public String getDebitAcctNo() {
		return debitAcctNo;
	}
	public void setDebitAcctNo(String debitAcctNo) {
		this.debitAcctNo = debitAcctNo;
	}
	public String getTrxCreditCode() {
		return trxCreditCode;
	}
	public void setTrxCreditCode(String trxCreditCode) {
		this.trxCreditCode = trxCreditCode;
	}
	public String getCreditAcctNo() {
		return creditAcctNo;
	}
	public void setCreditAcctNo(String creditAcctNo) {
		this.creditAcctNo = creditAcctNo;
	}
	public String getTrxAmount() {
		return trxAmount;
	}
	public void setTrxAmount(String trxAmount) {
		this.trxAmount = trxAmount;
	}
	public String getReffNumber() {
		return reffNumber;
	}
	public void setReffNumber(String reffNumber) {
		this.reffNumber = reffNumber;
	}
	public String getNarrative1() {
		return narrative1;
	}
	public void setNarrative1(String narrative1) {
		this.narrative1 = narrative1;
	}
	public String getNarrative2() {
		return narrative2;
	}
	public void setNarrative2(String narrative2) {
		this.narrative2 = narrative2;
	}
	public String getNarrative3() {
		return narrative3;
	}
	public void setNarrative3(String narrative3) {
		this.narrative3 = narrative3;
	}
	public String getNarrative4() {
		return narrative4;
	}
	public void setNarrative4(String narrative4) {
		this.narrative4 = narrative4;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	public String getFeeAmount() {
		return feeAmount;
	}
	public void setFeeAmount(String feeAmount) {
		this.feeAmount = feeAmount;
	}
	public String getCostCenter() {
		return costCenter;
	}
	public void setCostCenter(String costCenter) {
		this.costCenter = costCenter;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public String getTellerID() {
		return tellerID;
	}
	public void setTellerID(String tellerID) {
		this.tellerID = tellerID;
	}
	public String getReserveField() {
		return reserveField;
	}
	public void setReserveField(String reserveField) {
		this.reserveField = reserveField;
	}
	public String getFlagAction() {
		return flagAction;
	}
	public void setFlagAction(String flagAction) {
		this.flagAction = flagAction;
	}
}
