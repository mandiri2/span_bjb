package com.mii.models;

import java.io.Serializable;

public class RTGSStepM implements Serializable{

	private static final long serialVersionUID = 1L;

	private String stepID;
	private String event;
	private String rtgsRequestDate;
	private String rtgsResponseDate;
	private String rtgsNoSaktiDate;
	private String rtgsRequestState;
	private String rtgsResponseState;
	private String rtgsNoSaktiState;
	private String finalState;
	private String accountCurrency;
	private String sourceAccountNumber;
	private String amount;
	private String noSakti;
	private String transactionID;
	private String referenceNo;
	private String uniqueIDFileName;
	private String bpsxFile;
	private String bppxFile;
	private String bpoxFile;
	private String reversed1;
	private String reversed2;
	private String reversed3;
	private String reversed4;
	private String reversed5;
	
	public String getStepID() {
		return stepID;
	}
	public void setStepID(String stepID) {
		this.stepID = stepID;
	}
	public String getEvent() {
		return event;
	}
	public void setEvent(String event) {
		this.event = event;
	}
	public String getRtgsRequestDate() {
		return rtgsRequestDate;
	}
	public void setRtgsRequestDate(String rtgsRequestDate) {
		this.rtgsRequestDate = rtgsRequestDate;
	}
	public String getRtgsResponseDate() {
		return rtgsResponseDate;
	}
	public void setRtgsResponseDate(String rtgsResponseDate) {
		this.rtgsResponseDate = rtgsResponseDate;
	}
	public String getRtgsNoSaktiDate() {
		return rtgsNoSaktiDate;
	}
	public void setRtgsNoSaktiDate(String rtgsNoSaktiDate) {
		this.rtgsNoSaktiDate = rtgsNoSaktiDate;
	}
	public String getRtgsRequestState() {
		return rtgsRequestState;
	}
	public void setRtgsRequestState(String rtgsRequestState) {
		this.rtgsRequestState = rtgsRequestState;
	}
	public String getRtgsResponseState() {
		return rtgsResponseState;
	}
	public void setRtgsResponseState(String rtgsResponseState) {
		this.rtgsResponseState = rtgsResponseState;
	}
	public String getRtgsNoSaktiState() {
		return rtgsNoSaktiState;
	}
	public void setRtgsNoSaktiState(String rtgsNoSaktiState) {
		this.rtgsNoSaktiState = rtgsNoSaktiState;
	}
	public String getFinalState() {
		return finalState;
	}
	public void setFinalState(String finalState) {
		this.finalState = finalState;
	}
	public String getAccountCurrency() {
		return accountCurrency;
	}
	public void setAccountCurrency(String accountCurrency) {
		this.accountCurrency = accountCurrency;
	}
	public String getSourceAccountNumber() {
		return sourceAccountNumber;
	}
	public void setSourceAccountNumber(String sourceAccountNumber) {
		this.sourceAccountNumber = sourceAccountNumber;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getNoSakti() {
		return noSakti;
	}
	public void setNoSakti(String noSakti) {
		this.noSakti = noSakti;
	}
	public String getTransactionID() {
		return transactionID;
	}
	public void setTransactionID(String transactionID) {
		this.transactionID = transactionID;
	}
	public String getReferenceNo() {
		return referenceNo;
	}
	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}
	public String getUniqueIDFileName() {
		return uniqueIDFileName;
	}
	public void setUniqueIDFileName(String uniqueIDFileName) {
		this.uniqueIDFileName = uniqueIDFileName;
	}
	public String getBpsxFile() {
		return bpsxFile;
	}
	public void setBpsxFile(String bpsxFile) {
		this.bpsxFile = bpsxFile;
	}
	public String getBppxFile() {
		return bppxFile;
	}
	public void setBppxFile(String bppxFile) {
		this.bppxFile = bppxFile;
	}
	public String getBpoxFile() {
		return bpoxFile;
	}
	public void setBpoxFile(String bpoxFile) {
		this.bpoxFile = bpoxFile;
	}
	public String getReversed1() {
		return reversed1;
	}
	public void setReversed1(String reversed1) {
		this.reversed1 = reversed1;
	}
	public String getReversed2() {
		return reversed2;
	}
	public void setReversed2(String reversed2) {
		this.reversed2 = reversed2;
	}
	public String getReversed3() {
		return reversed3;
	}
	public void setReversed3(String reversed3) {
		this.reversed3 = reversed3;
	}
	public String getReversed4() {
		return reversed4;
	}
	public void setReversed4(String reversed4) {
		this.reversed4 = reversed4;
	}
	public String getReversed5() {
		return reversed5;
	}
	public void setReversed5(String reversed5) {
		this.reversed5 = reversed5;
	}

}
