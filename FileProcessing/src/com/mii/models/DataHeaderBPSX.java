package com.mii.models;

import java.io.Serializable;

public class DataHeaderBPSX implements Serializable{
	private static final long serialVersionUID = 1L;
	private String batchid;
	private String id;
	private String BPSX_Filename;
	private String bankCode;
	private String bankAcctNo;
	private String bankStatementDt;
	private String currency;
	private String beginBalance;
	private String closeBalance;
	private String totalRecord;
	private String bpsxFileEvent;
	private String insertTimeLog;
	
	public String getBatchid() {
		return batchid;
	}
	public void setBatchid(String batchid) {
		this.batchid = batchid;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBPSX_Filename() {
		return BPSX_Filename;
	}
	public void setBPSX_Filename(String bPSX_Filename) {
		BPSX_Filename = bPSX_Filename;
	}
	public String getBankCode() {
		return bankCode;
	}
	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}
	public String getBankAcctNo() {
		return bankAcctNo;
	}
	public void setBankAcctNo(String bankAcctNo) {
		this.bankAcctNo = bankAcctNo;
	}
	public String getBankStatementDt() {
		return bankStatementDt;
	}
	public void setBankStatementDt(String bankStatementDt) {
		this.bankStatementDt = bankStatementDt;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getBeginBalance() {
		return beginBalance;
	}
	public void setBeginBalance(String beginBalance) {
		this.beginBalance = beginBalance;
	}
	public String getCloseBalance() {
		return closeBalance;
	}
	public void setCloseBalance(String closeBalance) {
		this.closeBalance = closeBalance;
	}
	public String getTotalRecord() {
		return totalRecord;
	}
	public void setTotalRecord(String totalRecord) {
		this.totalRecord = totalRecord;
	}
	public String getBpsxFileEvent() {
		return bpsxFileEvent;
	}
	public void setBpsxFileEvent(String bpsxFileEvent) {
		this.bpsxFileEvent = bpsxFileEvent;
	}
	public String getInsertTimeLog() {
		return insertTimeLog;
	}
	public void setInsertTimeLog(String insertTimeLog) {
		this.insertTimeLog = insertTimeLog;
	}
}
