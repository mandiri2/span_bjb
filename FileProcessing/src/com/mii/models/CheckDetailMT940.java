package com.mii.models;

import java.io.Serializable;

public class CheckDetailMT940 implements Serializable{
	private static final long serialVersionUID = 1L;
	private String checkStatus;
	private String batchid;
	private String totalRow;
	
	public String getCheckStatus() {
		return checkStatus;
	}
	public void setCheckStatus(String checkStatus) {
		this.checkStatus = checkStatus;
	}
	public String getBatchid() {
		return batchid;
	}
	public void setBatchid(String batchid) {
		this.batchid = batchid;
	}
	public String getTotalRow() {
		return totalRow;
	}
	public void setTotalRow(String totalRow) {
		this.totalRow = totalRow;
	}	
}
