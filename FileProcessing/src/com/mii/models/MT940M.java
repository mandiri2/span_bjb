package com.mii.models;

import java.io.Serializable;

public class MT940M implements Serializable{
	private static final long serialVersionUID = 1L;

	private String BATCHID;
	private String MT_FILENAME;
	private String BANK_CODE;
	private String BANK_ACCT_NO;
	private String BANK_STATEMENT_DATE;
	private String CURRENCY;
	private String BEGIN_BALANCE;
	private String CLOSE_BALANCE;
	private String STATUS;
	private String DESCRIPTION;
	private String BS_FILENAME;
	private String GENERATE_DATE;
	private String SEND_DATE;
	private String TOTAL_RECORD;
	private String BANK_TRX_CODE;
	private String DEBITCREDIT;     
	private String BANK_REF_NO;   
	private String TRX_DATE;     
	private String VALUE_DATE;      
	private String ORIGINAL_AMOUNT;
	
	public String getBATCHID() {
		return BATCHID;
	}
	public void setBATCHID(String bATCHID) {
		BATCHID = bATCHID;
	}
	public String getMT_FILENAME() {
		return MT_FILENAME;
	}
	public void setMT_FILENAME(String mT_FILENAME) {
		MT_FILENAME = mT_FILENAME;
	}
	public String getBANK_CODE() {
		return BANK_CODE;
	}
	public void setBANK_CODE(String bANK_CODE) {
		BANK_CODE = bANK_CODE;
	}
	public String getBANK_ACCT_NO() {
		return BANK_ACCT_NO;
	}
	public void setBANK_ACCT_NO(String bANK_ACCT_NO) {
		BANK_ACCT_NO = bANK_ACCT_NO;
	}
	public String getBANK_STATEMENT_DATE() {
		return BANK_STATEMENT_DATE;
	}
	public void setBANK_STATEMENT_DATE(String bANK_STATEMENT_DATE) {
		BANK_STATEMENT_DATE = bANK_STATEMENT_DATE;
	}
	public String getCURRENCY() {
		return CURRENCY;
	}
	public void setCURRENCY(String cURRENCY) {
		CURRENCY = cURRENCY;
	}
	public String getBEGIN_BALANCE() {
		return BEGIN_BALANCE;
	}
	public void setBEGIN_BALANCE(String bEGIN_BALANCE) {
		BEGIN_BALANCE = bEGIN_BALANCE;
	}
	public String getCLOSE_BALANCE() {
		return CLOSE_BALANCE;
	}
	public void setCLOSE_BALANCE(String cLOSE_BALANCE) {
		CLOSE_BALANCE = cLOSE_BALANCE;
	}
	public String getSTATUS() {
		return STATUS;
	}
	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}
	public String getDESCRIPTION() {
		return DESCRIPTION;
	}
	public void setDESCRIPTION(String dESCRIPTION) {
		DESCRIPTION = dESCRIPTION;
	}
	public String getBS_FILENAME() {
		return BS_FILENAME;
	}
	public void setBS_FILENAME(String bS_FILENAME) {
		BS_FILENAME = bS_FILENAME;
	}
	public String getGENERATE_DATE() {
		return GENERATE_DATE;
	}
	public void setGENERATE_DATE(String gENERATE_DATE) {
		GENERATE_DATE = gENERATE_DATE;
	}
	public String getSEND_DATE() {
		return SEND_DATE;
	}
	public void setSEND_DATE(String sEND_DATE) {
		SEND_DATE = sEND_DATE;
	}
	public String getTOTAL_RECORD() {
		return TOTAL_RECORD;
	}
	public void setTOTAL_RECORD(String tOTAL_RECORD) {
		TOTAL_RECORD = tOTAL_RECORD;
	}
	public String getBANK_TRX_CODE() {
		return BANK_TRX_CODE;
	}
	public void setBANK_TRX_CODE(String bANK_TRX_CODE) {
		BANK_TRX_CODE = bANK_TRX_CODE;
	}
	public String getDEBITCREDIT() {
		return DEBITCREDIT;
	}
	public void setDEBITCREDIT(String dEBITCREDIT) {
		DEBITCREDIT = dEBITCREDIT;
	}
	public String getBANK_REF_NO() {
		return BANK_REF_NO;
	}
	public void setBANK_REF_NO(String bANK_REF_NO) {
		BANK_REF_NO = bANK_REF_NO;
	}
	public String getTRX_DATE() {
		return TRX_DATE;
	}
	public void setTRX_DATE(String tRX_DATE) {
		TRX_DATE = tRX_DATE;
	}
	public String getVALUE_DATE() {
		return VALUE_DATE;
	}
	public void setVALUE_DATE(String vALUE_DATE) {
		VALUE_DATE = vALUE_DATE;
	}
	public String getORIGINAL_AMOUNT() {
		return ORIGINAL_AMOUNT;
	}
	public void setORIGINAL_AMOUNT(String oRIGINAL_AMOUNT) {
		ORIGINAL_AMOUNT = oRIGINAL_AMOUNT;
	}
	
}
