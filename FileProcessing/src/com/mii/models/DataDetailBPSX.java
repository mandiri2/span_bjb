package com.mii.models;

import java.io.Serializable;

public class DataDetailBPSX implements Serializable{
	private static final long serialVersionUID = 1L;
	private String detailBatchid;
	private String bankTrxCode;
	private String debitCredit;
	private String bankReffNo;
	private String trxDate;
	private String valueDate;
	private String originalAmount;
	private String rowNum;
	private String countLine;
	private String id;
	private String reqEvent;
	private String status;
	
	public DataDetailBPSX(String id, String detailBatchid, String countLine, String bankTrxCode, String debitCredit, String bankRefNo,
			String trxDate, String valueDate, String originalAmount, String reqEvent){
		this.id = id;
		this.detailBatchid = detailBatchid;
		this.countLine = countLine;
		this.bankTrxCode = bankTrxCode;
		this.debitCredit = debitCredit;
		this.bankReffNo = bankRefNo;
		this.trxDate = trxDate;
		this.valueDate = valueDate;
		this.originalAmount = originalAmount;
		this.reqEvent = reqEvent;
	}

	public String getDetailBatchid() {
		return detailBatchid;
	}

	public void setDetailBatchid(String detailBatchid) {
		this.detailBatchid = detailBatchid;
	}

	public String getBankTrxCode() {
		return bankTrxCode;
	}

	public void setBankTrxCode(String bankTrxCode) {
		this.bankTrxCode = bankTrxCode;
	}

	public String getDebitCredit() {
		return debitCredit;
	}

	public void setDebitCredit(String debitCredit) {
		this.debitCredit = debitCredit;
	}

	public String getBankReffNo() {
		return bankReffNo;
	}

	public void setBankReffNo(String bankReffNo) {
		this.bankReffNo = bankReffNo;
	}

	public String getTrxDate() {
		return trxDate;
	}

	public void setTrxDate(String trxDate) {
		this.trxDate = trxDate;
	}

	public String getValueDate() {
		return valueDate;
	}

	public void setValueDate(String valueDate) {
		this.valueDate = valueDate;
	}

	public String getOriginalAmount() {
		return originalAmount;
	}

	public void setOriginalAmount(String originalAmount) {
		this.originalAmount = originalAmount;
	}

	public String getRowNum() {
		return rowNum;
	}

	public void setRowNum(String rowNum) {
		this.rowNum = rowNum;
	}

	public String getCountLine() {
		return countLine;
	}

	public void setCountLine(String countLine) {
		this.countLine = countLine;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getReqEvent() {
		return reqEvent;
	}

	public void setReqEvent(String reqEvent) {
		this.reqEvent = reqEvent;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
