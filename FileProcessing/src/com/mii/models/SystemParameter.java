package com.mii.models;

import java.io.Serializable;
import java.util.Date;

public class SystemParameter implements Serializable {
	private static final long serialVersionUID = 1L;
	private String param_id;
	private String param_name;
	private String param_value;
	private String description;
	private Date insert_date;
	private Date update_date;
	private String change_who;
	private String is_enabled;

	public String getParam_id() {
		return param_id;
	}

	public void setParam_id(String param_id) {
		this.param_id = param_id;
	}

	public String getParam_name() {
		return param_name;
	}

	public void setParam_name(String param_name) {
		this.param_name = param_name;
	}

	public String getParam_value() {
		return param_value;
	}

	public void setParam_value(String param_value) {
		this.param_value = param_value;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getInsert_date() {
		return insert_date;
	}

	public void setInsert_date(Date insert_date) {
		this.insert_date = insert_date;
	}

	public Date getUpdate_date() {
		return update_date;
	}

	public void setUpdate_date(Date update_date) {
		this.update_date = update_date;
	}

	public String getChange_who() {
		return change_who;
	}

	public void setChange_who(String change_who) {
		this.change_who = change_who;
	}

	public String getIs_enabled() {
		return is_enabled;
	}

	public void setIs_enabled(String is_enabled) {
		this.is_enabled = is_enabled;
	}
}
