package com.mii.models;

import java.io.Serializable;
import java.math.BigDecimal;

public class DataDetailsMT940 implements Serializable{
	private static final long serialVersionUID = 1L;
	private String detailBatchid;	
	private String bankTrxCode; //PAYMENT_METHODS
	private String debitCredit; //DEBITCREDIT
	private String bankReffNo; //DOC_NUMBER
	private String trxDate; //DOC_DATE
	private String valueDate; //VALUEDATE
	private String originalAmount; //as CREDITTRFAMOUNT
	private String rowNum;
	private String countLine;
	private String id;
	private String status;
	
	public DataDetailsMT940(){
		
	}
	
	public String getDetailBatchid() {
		return detailBatchid;
	}
	public void setDetailBatchid(String detailBatchid) {
		this.detailBatchid = detailBatchid;
	}
	public String getBankTrxCode() {
		return bankTrxCode;
	}
	public void setBankTrxCode(String bankTrxCode) {
		this.bankTrxCode = bankTrxCode;
	}
	public String getDebitCredit() {
		return debitCredit;
	}
	public void setDebitCredit(String debitCredit) {
		this.debitCredit = debitCredit;
	}
	public String getBankReffNo() {
		return bankReffNo;
	}
	public void setBankReffNo(String bankReffNo) {
		this.bankReffNo = bankReffNo;
	}
	public String getTrxDate() {
		return trxDate;
	}
	public void setTrxDate(String trxDate) {
		this.trxDate = trxDate;
	}
	public String getValueDate() {
		return valueDate;
	}
	public void setValueDate(String valueDate) {
		this.valueDate = valueDate;
	}
	public String getOriginalAmount() {
		return originalAmount;
	}
	public void setOriginalAmount(String originalAmount) {
		this.originalAmount = originalAmount;
	}
	public String getRowNum() {
		return rowNum;
	}
	public void setRowNum(String rowNum) {
		this.rowNum = rowNum;
	}
	public String getCountLine() {
		return countLine;
	}
	public void setCountLine(String countLine) {
		this.countLine = countLine;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
