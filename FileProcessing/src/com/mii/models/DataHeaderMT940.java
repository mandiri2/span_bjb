package com.mii.models;

import java.io.Serializable;

public class DataHeaderMT940 implements Serializable{
	private static final long serialVersionUID = 1L;
	private String batchid;
	private String id;
	private String MT_Filename;
	private String bankCode;
	private String bankAcctNo;
	private String bankStatementDt;
	private String currency;
	private String beginBalance;
	private String closeBalance;
	private String totalRecord;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBatchid() {
		return batchid;
	}
	public void setBatchid(String batchid) {
		this.batchid = batchid;
	}
	public String getMT_Filename() {
		return MT_Filename;
	}
	public void setMT_Filename(String mT_Filename) {
		MT_Filename = mT_Filename;
	}
	public String getBankCode() {
		return bankCode;
	}
	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}
	public String getBankAcctNo() {
		return bankAcctNo;
	}
	public void setBankAcctNo(String bankAcctNo) {
		this.bankAcctNo = bankAcctNo;
	}
	public String getBankStatementDt() {
		return bankStatementDt;
	}
	public void setBankStatementDt(String bankStatementDt) {
		this.bankStatementDt = bankStatementDt;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getBeginBalance() {
		return beginBalance;
	}
	public void setBeginBalance(String beginBalance) {
		this.beginBalance = beginBalance;
	}
	public String getCloseBalance() {
		return closeBalance;
	}
	public void setCloseBalance(String closeBalance) {
		this.closeBalance = closeBalance;
	}
	public String getTotalRecord() {
		return totalRecord;
	}
	public void setTotalRecord(String totalRecord) {
		this.totalRecord = totalRecord;
	}
}
