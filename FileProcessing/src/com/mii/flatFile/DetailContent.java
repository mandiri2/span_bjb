package com.mii.flatFile;

import java.util.ArrayList;

import com.mii.models.ReconFileM;

public class DetailContent {

	public ArrayList<ReconFileM> reconFile(String[] content){
		ReconFileM reconFileM = new ReconFileM();
		ArrayList<ReconFileM> reconFileMs = new ArrayList<ReconFileM>();
		
		reconFileM.setCaID(content[0]);
		reconFileM.setCaName(content[1]);
		reconFileM.setSettlementDate(content[2]);
		reconFileM.setCurrency(content[3]);
		reconFileM.setBillingCode(content[4]);
		reconFileM.setNtb(content[5]);
		reconFileM.setNtpn(content[6]);
		reconFileM.setAmount(content[7]);
		reconFileM.setNoSakti(content[8]);
		
		reconFileMs.add(reconFileM);
		
		return reconFileMs;
	}
}
