package com.mii.flatFile;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import com.ancientprogramming.fixedformat4j.format.impl.FixedFormatManagerImpl;

public class FileFormatter {
	
	public ArrayList<String> getLine(Boolean firstLine, String fileName) throws IOException{
		String lineContent;
		int lineCount = 0;
		
		ArrayList<String> allOfline = new ArrayList<String>();
		BufferedReader br = new BufferedReader(new FileReader(fileName));
		
		while ((lineContent = br.readLine()) != null) { 
			if (lineCount == 0){
				if (firstLine == true) allOfline.add(lineContent);
			} else {
				allOfline.add(lineContent);
			}
			lineCount++;
		}
		
		br.close();
		
		return allOfline;
	}
	
	public String[] getContentByRegex(String content, String regex){
		return content.split(regex);
	}
	
	public String generateContentFixLenght(Object object){
		return new FixedFormatManagerImpl().export(object);
	}
	
	@SuppressWarnings("unchecked")
	public Object getContentByFixLength(String content, Class clazz){
		return new FixedFormatManagerImpl().load(clazz.asSubclass(clazz), content);
	}
	
}
