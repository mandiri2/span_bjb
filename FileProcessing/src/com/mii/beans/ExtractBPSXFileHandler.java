package com.mii.beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedProperty;
import com.mii.dao.SystemParameterDAO;
import com.mii.logger.Log;
import com.mii.models.SystemParameter;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.ApplicationInstance;
import com.mii.util.ExtractUtils;
import com.mii.util.FindFile;

@ManagedBean
@ViewScoped
public class ExtractBPSXFileHandler implements Serializable{
	private static final long serialVersionUID = 1L;
	@ManagedProperty(value="#{applicationBean}")
	private ApplicationBean applicationBean;
	private static Log log;
	
	public ExtractBPSXFileHandler() {
		log = Log.getInstance(ExtractBPSXFileHandler.class);
		applicationBean = ApplicationInstance.getInstance().getApplicationBean();
	}
	
	public void run(String schedulerID){
		ExtractBPSXFileHandler ext = new ExtractBPSXFileHandler();
		ExtractUtils etu = new ExtractUtils();
		//get data from db
		String pathNameMorning = ext.getParameter("RESOURCE_LOCAL_PATH_BPSX_MORNING");
		String pathNameNoon = ext.getParameter("RESOURCE_LOCAL_PATH_BPSX_NOON");
		String pattern = ext.getParameter("PATTERN_FILE_MT940");
		try{
			//check exist file morning
			File fil = new File(pathNameMorning);
			File[] files = fil.listFiles(new FindFile(pattern));
			if(files.length>0){
				System.out.println("[ExtractBPSXFileHandler] File morning found!");
				etu.doExtractMorning(pathNameMorning, "MORNING");
			}
			
			//check exist file noon
			File file = new File(pathNameNoon);
			File[] listFiles = file.listFiles(new FindFile(pattern));
			if(listFiles.length>0){
				System.out.println("[ExtractBPSXFileHandler] File noon found!");
				etu.doExtractNoon(pathNameNoon, "NOON");
			}
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}
		
	}
	
	public String getParameter(String param_name){
		List<SystemParameter> params = getParameterbyID(param_name);
		String param = "";
		for(SystemParameter sp : params){
			param = sp.getParam_value();
		}
		return param;
	}
	
	private List<SystemParameter> getParameterbyID(String param_name){
		SystemParameterDAO sysDAO = (SystemParameterDAO) applicationBean.getFileProcessContext().getBean("sysParamDAO");
		List<SystemParameter> params = new ArrayList<SystemParameter>(sysDAO.getValueParameter(param_name));
		return params;
	}
}
