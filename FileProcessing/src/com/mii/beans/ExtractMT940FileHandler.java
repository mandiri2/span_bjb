package com.mii.beans;

import java.io.File;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;







//import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;







//import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.mii.constant.CommonConstant;
import com.mii.dao.DataMT940DAO;
import com.mii.dao.SystemParameterDAO;
import com.mii.logger.Log;
import com.mii.models.DataDetailMT940;
//import com.mii.models.DataDetailsMT940;
import com.mii.models.DataHeaderMT940;
import com.mii.models.SystemParameter;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.ApplicationInstance;
import com.mii.scoped.SessionBean;
import com.mii.util.ExtractUtils;
import com.mii.util.FindFile;
import com.mii.util.MoveFile;

@ManagedBean
@ViewScoped
public class ExtractMT940FileHandler implements Serializable {
	/*private static final long serialVersionUID = 1L;
	@ManagedProperty(value="#{applicationBean}")
	private ApplicationBean applicationBean;
	@ManagedProperty(value="#{sessionBean}")
	private SessionBean sessionBean;
	private DataDetailMT940 ddtm;
	private DataHeaderMT940 ddhm;
	private SessionFactory sessionFactory;
	private List<DataDetailMT940> listData;
	private static Log log;
	
	public ExtractMT940FileHandler() {
		ddtm = new DataDetailMT940(null,null,null, null, null, null, null, null, null);
		ddhm = new DataHeaderMT940();
		log = Log.getInstance(ExtractMT940FileHandler.class);
		applicationBean = ApplicationInstance.getInstance().getApplicationBean();
	}
	

	public void run(String schedulerID){
		ExtractMT940FileHandler ext = new ExtractMT940FileHandler();

		try{
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = new Date();
			
			//get data from db
			String pathName = ext.getParameter("RESOURCE_LOCAL_PATH_MT940");
			String pattern = ext.getParameter("PATTERN_FILE_MT940");
			String destPath = ext.getParameter("BACKUP_LOCAL_PATH_MT940");
			String beneficiaryBankCode = ext.getParameter("BANK_CODE");
			
			String mtFilename = "";
			
			String sourcePathFile = "";
			String destPathFile = "";
			String statusExtract = "";
			String batchid = "";
			File fil = new File(pathName);
			File[] files = fil.listFiles(new FindFile(pattern));
			
			for (File file : files){
				log.debug("Extract File "+file.getName()+" start time = "+dateFormat.format(date));
				System.out.println("Extract File "+file.getName()+" start time = "+dateFormat.format(date));
			    mtFilename = file.getName();
			    sourcePathFile = pathName.concat(file.getName());
			    destPathFile = destPath.concat(file.getName());
			    //run extract file
			    statusExtract = ext.doExtractFile(sourcePathFile, mtFilename);
			    System.out.println("Status Extract is "+statusExtract);
			    if ("SUCCESS".equalsIgnoreCase(statusExtract)){
			    	log.debug("Status extract is "+statusExtract);
			    	
			    	Date date3 = new Date();
			    	System.out.println("Start Insert Header "+file.getName()+" start time = "+dateFormat.format(date3));
			    	//insert data header to database
			    	List<DataHeaderMT940> listHeader = ext.doInsertHeaderMT940(mtFilename, beneficiaryBankCode, ext.ddhm.getBankAcctNo(), ext.ddhm.getBankStatementDt(), 
							ext.ddhm.getCurrency(), ext.ddhm.getBeginBalance(), ext.ddhm.getTotalRecord(), ext.ddhm.getCloseBalance());
			    	for(DataHeaderMT940 hd : listHeader){
			    		batchid = hd.getBatchid();
			    	}

			    	Date date4 = new Date();
			    	System.out.println("Finish Insert Header "+file.getName()+" finish time = "+dateFormat.format(date4));
			    	
			    	//get batchid
					//batchid = ext.getBatchidMt(mtFilename);
			    	
			    	Date date5 = new Date();
			    	System.out.println("Create list detail, start time = "+dateFormat.format(date5));
					//insert data detail to database
			    	List<DataDetailMT940> listData = ext.listData;
			    	
			    	Date date6 = new Date();
			    	System.out.println("Create list detail, finish time = "+dateFormat.format(date6));
			    	
			    	Date date7 = new Date();
			    	System.out.println("Insert list detail, start time = "+dateFormat.format(date7));
			    	//insert batch data detail
			    	ext.batchInsert(listData, batchid);
			    	
			    	Date date8 = new Date();
			    	System.out.println("Insert list detail, finish time = "+dateFormat.format(date8));
			    	
			    	for (DataDetailMT940 dm : listData){
			    		//create insert data details
			    		ext.doInsertDetailMT940(batchid, dm.getBankTrxCode(), dm.getDebitCredit(), dm.getBankReffNo(), 
			    				dm.getTrxDate(), dm.getValueDate(), dm.getOriginalAmount(), dm.getCountLine());
			    	}
			    	
			    	//move file to backup
			    	MoveFile mf = new MoveFile();
			    	String moveStatus = mf.doMoveFile(sourcePathFile, destPathFile);
			    	log.debug("Status move file from "+sourcePathFile+" to "+destPathFile+" is "+moveStatus);
			    	System.out.println("Status move file from "+sourcePathFile+" to "+destPathFile+" is "+moveStatus);
			    	
			    	//update header to status 4
			    	ext.updateHeaderLog(batchid, "0", "4", "Data ready to be processed");
			    }else{
			    	log.debug("Failed to extract file =====> "+sourcePathFile);
			    	System.out.println("Failed to extract file =====> "+sourcePathFile);
			    }
			    Date date2 = new Date();
			    log.debug("Extract File "+file.getName()+" finish time = "+dateFormat.format(date2));
			    System.out.println("Extract File "+file.getName()+" finish time = "+dateFormat.format(date2));
			}	
		}catch(Exception e){
			log.error(e.getMessage());
		}
	}
	
	public String doExtractFile(String filePath, String mtFilename) throws Exception{
		String extractStatus = "";
		String reader = "";
		String header = "";
		String detail = "";
		String footer = "";
		int readerLength = 0;
		String countLine = "";
		String bankTrxCode = "";
		String debitCredit = "";
		String bankRefNo = "";
		String trxDate = "";
		String valueDate = "";
		String originalAmount = "";
		
		try{
			Scanner scan = new Scanner(new File(filePath));
			scan.useDelimiter(System.getProperty(CommonConstant.lineSeparator));
			List<DataDetailMT940> list = new ArrayList<DataDetailMT940>();
			ExtractUtils et = new ExtractUtils();
			
			while(scan.hasNext()){
				reader = et.checkRecordBPS(scan.next());
				System.out.println("Reader = ["+reader+"]");
				readerLength = reader.length();
				System.out.println("Reader Length = "+readerLength);
				
				if (readerLength == 46){
					header = reader;
					//System.out.println("header "+header);
				}else if(readerLength == 57){
					detail = reader;
					//extract detail data
					countLine = detail.substring(0, 6).replaceFirst("^0+(?!$)", "").trim();
					bankTrxCode = detail.substring(6, 9).trim();
					if(CommonConstant.CreditViaTellerTrxCodeEq.equalsIgnoreCase(bankTrxCode)){
						bankTrxCode = CommonConstant.CreditViaTellerTrxCode;
					}else if(CommonConstant.CreditViaATMTrxCodeEq.equalsIgnoreCase(bankTrxCode)){
						bankTrxCode = CommonConstant.CreditViaATMTrxCode;
					}else if(CommonConstant.PelimpahanTrxCodeEq.equalsIgnoreCase(bankTrxCode)){
						bankTrxCode = CommonConstant.PelimpahanTrxCode;
					}else if(CommonConstant.DebitViaTellerTrxCodeEq.equalsIgnoreCase(bankTrxCode)){
						bankTrxCode = CommonConstant.DebitViaTellerTrxCode;
					}else if(CommonConstant.DebitViaATMTrxCodeEq.equalsIgnoreCase(bankTrxCode)){
						bankTrxCode = CommonConstant.DebitViaATMTrxCode;
					}
					debitCredit = detail.substring(9, 10).trim();
					if(CommonConstant.PelimpahanTrxCode.equalsIgnoreCase(bankTrxCode)){
						bankRefNo = detail.substring(10, 26).trim();
					}else{
						bankRefNo = detail.substring(10, 26).replaceFirst("^0+(?!$)", "").trim();
					}
					trxDate = detail.substring(26, 34).trim();
					valueDate = detail.substring(34, 42).trim();
					originalAmount = detail.substring(42, 57).replaceFirst("^0+(?!$)", "").trim();
					
					list.add(new DataDetailMT940(null,null,countLine, bankTrxCode, debitCredit, bankRefNo, trxDate, valueDate, originalAmount));
					
					ddtm =  setDataDetail(null,countLine, bankTrxCode, debitCredit, bankRefNo, trxDate, valueDate, originalAmount);
					
				}else if(readerLength == 22){
					footer = reader;
				}else{
					
					break;
				}
			}
			scan.close();
			
				
			//extract the header
			String accountNo = header.substring(0, 20).trim();
			String bankStatementDt = header.substring(20, 28).trim();
			String currency = header.substring(28, 31).trim();
			String beginBalance = header.substring(31, 46).replaceFirst("^0+(?!$)", "").trim();
			
			//extract the footer
			String totalData = footer.substring(0, 6).trim();
			String endingBalance = footer.substring(6, 21).replaceFirst("^0+(?!$)", "").trim();
			
			ddhm = setDataHeader(accountNo, bankStatementDt, currency, beginBalance, totalData, endingBalance);
			
			listData = setListDataDetail(list);
		
		extractStatus = "SUCCESS";
		
		}catch(Exception e){
			extractStatus = "FAILED";
			log.error(e.getMessage());
			e.printStackTrace();
		}
		
		return extractStatus;
	}
	
	public List<DataHeaderMT940> doInsertHeaderMT940(String mtFilename,String bankCode, String accountNo, String bankStatementDt,
			String currency, String beginBalance, String totalData, String endingBalance){
		
		//ApplicationBean ap = new ApplicationBean();
		//ap.postConstruct();
		DataMT940DAO mtDao = (DataMT940DAO) applicationBean.getFileProcessContext().getBean("dataHeaderDAO");
		List<DataHeaderMT940> headerList = new ArrayList<DataHeaderMT940>(mtDao.insertDataHeader(mtFilename, bankCode, accountNo, bankStatementDt, currency, beginBalance, totalData, endingBalance));
		return headerList;
	}
	
	
	public void updateHeaderLog(String batchid, String status, String setStatus, String desc){
		//ApplicationBean ap = new ApplicationBean();
		//ap.postConstruct();
		DataMT940DAO dmt = (DataMT940DAO) applicationBean.getFileProcessContext().getBean("dataHeaderDAO");
		dmt.updateDataHeader(batchid, status, setStatus, desc);
	}
	
	public void doInsertDetailMT940(String batchid, String bankTrxCode, String debitCredit, String bankRefNo, String trxDate, 
			String valueDate, String originalAmount, String countLine){
		//ApplicationBean ap = new ApplicationBean();
		//ap.postConstruct();
		DataMT940DAO mtDao = (DataMT940DAO) applicationBean.getFileProcessContext().getBean("dataListDAO");
		mtDao.insertDataDetails(batchid, bankTrxCode, debitCredit, bankRefNo, trxDate, valueDate, originalAmount, countLine);
	}
	
	public String getBatchidMt(String mtFilename){
		//ApplicationBean ap = new ApplicationBean();
		//ap.postConstruct();
		DataMT940DAO dhdao = (DataMT940DAO) applicationBean.getFileProcessContext().getBean("dataHeaderDAO");
		String batchid = dhdao.getBatchid(mtFilename);
		return batchid;
	}
	
	public String getParameter(String param_name){
		List<SystemParameter> params = getParameterbyID(param_name);
		String param = "";
		for(SystemParameter sp : params){
			param = sp.getParam_value();
		}
		return param;
	}
	
	private List<SystemParameter> getParameterbyID(String param_name){
		//ApplicationBean ap = new ApplicationBean();
		//ap.postConstruct();
		SystemParameterDAO sysDAO = (SystemParameterDAO) applicationBean.getFileProcessContext().getBean("sysParamDAO");
		List<SystemParameter> params = new ArrayList<SystemParameter>(sysDAO.getValueParameter(param_name));
		return params;
	}
	
	public DataDetailMT940 setDataDetail(String batchid, String countLine, String bankTrxCode, String debitCredit, String bankRefNo, String trxDate, 
			String valueDate, String originalAmount){
		
		DataDetailMT940 dde = new DataDetailMT940(null, null, null, null, null, null, null, null, null);
		dde.setId(countLine);
		dde.setDetailBatchid(batchid);
		dde.setCountLine(countLine);
		dde.setBankTrxCode(bankTrxCode);
		dde.setDebitCredit(debitCredit);
		dde.setBankReffNo(bankRefNo);
		dde.setTrxDate(trxDate);
		dde.setValueDate(valueDate);
		dde.setOriginalAmount(originalAmount);
		
		return dde;
	}
	
	public List<DataDetailMT940> setListDataDetail(List<DataDetailMT940> listDataDetails){
		listData = listDataDetails;
		return listData;
	}
	
	public DataHeaderMT940 setDataHeader(String accountNo, String bankStatementDt, String currency, String beginBalance, 
			String totalData, String endingBalance){
		DataHeaderMT940 ddh = new DataHeaderMT940();
		ddh.setBankAcctNo(accountNo);
		ddh.setBankStatementDt(bankStatementDt);
		ddh.setCurrency(currency);
		ddh.setBeginBalance(beginBalance);
		ddh.setTotalRecord(totalData);
		ddh.setCloseBalance(endingBalance);
		
		return ddh;
	}
	
	private void batchInsert(List<DataDetailMT940> listDataDetails, String batchid){
		
		try{	
		sessionFactory = (SessionFactory) applicationBean.getFileProcessContext().getBean("laSessionFactoryKu");	
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		int i = 0;   
		for(DataDetailMT940 dm : listDataDetails){
		    DataDetailMT940 detail = new DataDetailMT940(dm.getCountLine(), batchid, dm.getCountLine(), 
		    		dm.getBankTrxCode(), dm.getDebitCredit(), dm.getBankReffNo(), dm.getTrxDate(), dm.getValueDate(), 
		    		dm.getOriginalAmount());
		    session.save(detail);
		    if ( i % 20 == 0 ) { //20, same as the JDBC batch size
		        //flush a batch of inserts and release memory:
		        session.flush();
		        session.clear();
		    }
		    i++;
		}
		   
		tx.commit();
		session.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	public DataDetailMT940 getDdtm() {
		return ddtm;
	}

	public void setDdtm(DataDetailMT940 ddtm) {
		this.ddtm = ddtm;
	}

	public DataHeaderMT940 getDdhm() {
		return ddhm;
	}

	public void setDdhm(DataHeaderMT940 ddhm) {
		this.ddhm = ddhm;
	}


	public ApplicationBean getApplicationBean() {
		return applicationBean;
	}


	public void setApplicationBean(ApplicationBean applicationBean) {
		this.applicationBean = applicationBean;
	}


	public SessionBean getSessionBean() {
		return sessionBean;
	}


	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}


	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}


	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	
*/	
	
}
