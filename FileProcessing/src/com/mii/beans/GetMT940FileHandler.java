package com.mii.beans;

import java.util.List;
import java.util.ArrayList;

import com.mii.dao.SystemParameterDAO;
import com.mii.models.SystemParameter;
import com.mii.scoped.ApplicationBean;
import com.mii.util.DownloadSFTPFiles;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

@ManagedBean
public class GetMT940FileHandler implements Serializable{
	private static final long serialVersionUID = 1L;
	@ManagedProperty(value="#{applicationBean}")
	private ApplicationBean applicationBean;

	public void run(String schedulerID) {
		GetMT940FileHandler gm = new GetMT940FileHandler();
		String host = gm.getParameter("SFTP_HOST_MT940");
		String user = gm.getParameter("SFTP_USER_MT940");
		String password = gm.getParameter("SFTP_PASS_MT940");
		String port = gm.getParameter("SFTP_PORT_MT940");
		String workingPath = gm.getParameter("SFTP_WORK_PATH_MT940");
		String localPath = gm.getParameter("SFTP_LOCAL_PATH_MT940");
		String privateKeyPath = gm.getParameter("PRIVATE_KEY_PATH_FILE");
		String isPrivateKey = gm.getParameter("IS_PRIVATE_KEY");
		String knownHost = gm.getParameter("KNOWN_HOST_KEY_PATH_FILE");
		String patternFile = gm.getParameter("PATTERN_FILE_MT940").substring(0, 3).concat("X");
		
		DownloadSFTPFiles ds = new DownloadSFTPFiles(host, user, password, port, workingPath, localPath, 
				privateKeyPath, isPrivateKey, knownHost, patternFile);
		String statusSFTP = ds.downloadFile();
		
		if ("SUCCESS".equalsIgnoreCase(statusSFTP)){
			System.out.println("get MT940 file is "+statusSFTP);
		}else{
			System.out.println("get MT940 file is "+statusSFTP);
		}
	}
	
	public String getParameter(String param_name){
		System.out.println("input param = "+param_name);
		List<SystemParameter> params = getParameterbyID(param_name);
		String param = "";
		for(SystemParameter sp : params){
			param = sp.getParam_value();
		}
		return param;
	}
	
	private List<SystemParameter> getParameterbyID(String param_name){
		ApplicationBean app = new ApplicationBean();
		app.postConstruct();
		SystemParameterDAO sysDAO = (SystemParameterDAO) app.getFileProcessContext().getBean("sysParamDAO");
		List<SystemParameter> params = new ArrayList<SystemParameter>(sysDAO.getValueParameter(param_name));
		return params;
	}
	
	public ApplicationBean getApplicationBean() {
		return applicationBean;
	}

	public void setApplicationBean(ApplicationBean applicationBean) {
		this.applicationBean = applicationBean;
	}

}
