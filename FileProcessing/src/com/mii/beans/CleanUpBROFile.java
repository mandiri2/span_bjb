package com.mii.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

import com.mii.dao.SystemParameterDAO;
import com.mii.helpers.EncryptUtils;
import com.mii.models.SystemParameter;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.ApplicationInstance;
import com.mii.util.DownloadSFTPFiles;

@ManagedBean
public class CleanUpBROFile implements Serializable{
	private static final long serialVersionUID = 1L;
	@ManagedProperty(value="#{applicationBean}")
	private ApplicationBean applicationBean;
	
	public CleanUpBROFile(){
		applicationBean = ApplicationInstance.getInstance().getApplicationBean();
	}
	
	public void run(String schedulerID){
		System.out.println("Scheduller is running...");
		/*CleanUpBROFile cb = new CleanUpBROFile();
		String host = cb.getParameter("SFTP_HOST_BPSX");
		String user = cb.getParameter("SFTP_USER_BPSX");
		//Add Decrypt 25 Jul 2016 -- Brian
		String password = EncryptUtils.DecodeDecrypt(cb.getParameter("SFTP_PASS_BPSX"));
		//--------------------------------
		String port = cb.getParameter("SFTP_PORT_BPSX");
		String workingPath = cb.getParameter("SFTP_WORK_PATH_BPSX");
		String localPath = cb.getParameter("SFTP_LOCAL_PATH_BROX");
		String privateKeyPath = cb.getParameter("PRIVATE_KEY_PATH_FILE");
		String isPrivateKey = cb.getParameter("IS_PRIVATE_KEY");
		String knownHost = cb.getParameter("KNOWN_HOST_KEY_PATH_FILE");
		String patternFile = cb.getParameter("PATTERN_FILE_BRO").substring(0, 3).concat("X");
		
		DownloadSFTPFiles ds = new DownloadSFTPFiles(host, user, password, port, workingPath, localPath, 
				privateKeyPath, isPrivateKey, knownHost, patternFile);
		String statusSFTP = ds.downloadFile();
		
		if ("SUCCESS".equalsIgnoreCase(statusSFTP)){
			System.out.println("[CleanUpBROFile] get clean up BROX file is "+statusSFTP);
		}else if("FAILED".equalsIgnoreCase(statusSFTP)){
			System.out.println("[CleanUpBROFile] get clean up BROX file is "+statusSFTP);
		}*/
	}
	
	public String getParameter(String param_name){
		List<SystemParameter> params = getParameterbyID(param_name);
		String param = "";
		for(SystemParameter sp : params){
			param = sp.getParam_value();
		}
		return param;
	}
	
	private List<SystemParameter> getParameterbyID(String param_name){
		SystemParameterDAO sysDAO = (SystemParameterDAO) applicationBean.getFileProcessContext().getBean("sysParamDAO");
		List<SystemParameter> params = new ArrayList<SystemParameter>(sysDAO.getValueParameter(param_name));
		return params;
	}

}
