package com.mii.beans;

import java.io.Serializable;

import com.mii.logger.Log;
import com.mii.util.BPSXValidationUtils;

public class BPSXDataValidation implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public void run(String schedulerID){
		//do validation data
		BPSXValidationUtils bpv = new BPSXValidationUtils();
		String validationStatus = bpv.doValidationBPSX();
		if(!"".equalsIgnoreCase(validationStatus) || !validationStatus.isEmpty()){
			System.out.println("[BPSXDataValidationHandler] Validation status is "+validationStatus);
		}
		Log.getInstance().debug("Validation status is "+validationStatus);
	}
	//
}
