package com.mii.beans;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedProperty;

import com.mii.constant.State;
import com.mii.dao.SystemParameterDAO;
import com.mii.logger.Log;
import com.mii.models.SystemParameter;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.ApplicationInstance;
import com.mii.sftp.SFTPPrivateKey;
import com.mii.util.FindFile;
import com.mii.util.MoveFile;

public class PutFileToEQ implements Serializable{
	private static final long serialVersionUID = 1L;
	@ManagedProperty(value="#{applicationBean}")
	private ApplicationBean applicationBean;
	private static Log log;
	
	public PutFileToEQ(){
		applicationBean = ApplicationInstance.getInstance().getApplicationBean();
		log = Log.getInstance(PutFileToEQ.class);
	}
	
	public void run(String schedulerID){
		try{
			PutFileToEQ ptq = new PutFileToEQ();
			String host = ptq.getParameter("SFTP_HOST_BPSX");
			String user = ptq.getParameter("SFTP_USER_BPSX");
			String workingPath = ptq.getParameter("SFTP_WORK_EQ_PATH_BPSX");
			String port = ptq.getParameter("SFTP_PORT_BPSX");
			String localPath = ptq.getParameter("SFTP_LOCAL_PATH_EQ");
			String privateKeyPath = ptq.getParameter("PRIVATE_KEY_PATH_FILE");
			String pattern = ptq.getParameter("PATTERN_FILE_EQ");
			String backupPath = ptq.getParameter("BACKUP_LOCAL_PATH_EQ");
			String knownHostKey = ptq.getParameter("KNOWN_HOST_KEY_PATH_FILE");
			
			File fil = new File(localPath);
			File[] files = fil.listFiles(new FindFile(pattern));
			for(File file : files){
				String eqFilename = file.getName();
				SFTPPrivateKey sftp = new SFTPPrivateKey(host, user, port, privateKeyPath, knownHostKey);
				
				//check connection sftp
				if(sftp.statusConnection() == 1){
					log.debug("SFTP connection to "+host+"is success!");
					
					//put file to sftp EQ
					String putStatus = sftp.putFile(localPath, workingPath, eqFilename);
					if(putStatus == State.Success){
						System.out.println("[PutFileToEq] Status put file "+eqFilename+" to EQ is "+putStatus);
						//move file to backup
						String sourcePathFile = localPath.concat(eqFilename);
						String destinationPathFile = backupPath.concat(eqFilename);
						MoveFile mv = new MoveFile();
						String statusBackup = mv.doMoveFile(sourcePathFile, destinationPathFile);
						log.debug("[PutFileToEq] Status move file from "+sourcePathFile+" to "+destinationPathFile+" is "+statusBackup);
					}else{
						log.debug("[PutFileToEq] Failed to put file "+eqFilename+" to EQ SFTP "+host);
					}
					
					//close sftp connection
					sftp.removeConnection();
				}else{
					System.out.println("[PutFileToEq] SFTP connection to "+host+" is failed!");
					log.debug("SFTP connection to "+host+"is failed!");
				}
			}
			
			
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public String getParameter(String param_name){
		List<SystemParameter> params = getParameterbyID(param_name);
		String param = "";
		for(SystemParameter sp : params){
			param = sp.getParam_value();
		}
		return param;
	}
	
	private List<SystemParameter> getParameterbyID(String param_name){
		//ApplicationBean ap = new ApplicationBean();
		//ap.postConstruct();
		SystemParameterDAO sysDAO = (SystemParameterDAO) applicationBean.getFileProcessContext().getBean("sysParamDAO");
		List<SystemParameter> params = new ArrayList<SystemParameter>(sysDAO.getValueParameter(param_name));
		return params;
	}

}
