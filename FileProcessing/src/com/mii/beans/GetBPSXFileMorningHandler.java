package com.mii.beans;

import java.util.List;
import java.util.ArrayList;

import com.mii.dao.SystemParameterDAO;
import com.mii.helpers.EncryptUtils;
import com.mii.models.SystemParameter;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.ApplicationInstance;
import com.mii.util.CommonDate;
import com.mii.util.DownloadSFTPFiles;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

@ManagedBean
public class GetBPSXFileMorningHandler implements Serializable{
	private static final long serialVersionUID = 1L;
	@ManagedProperty(value="#{applicationBean}")
	private ApplicationBean applicationBean;
	
	public GetBPSXFileMorningHandler(){
		applicationBean = ApplicationInstance.getInstance().getApplicationBean();
	}
	
	public void run(String schedulerID) {
		String time = CommonDate.getCurrentDateString("HH");
		if("09".equalsIgnoreCase(time)){
			runner(schedulerID);
		}
	}
	
	public void runner(String schedulerID) {
		GetBPSXFileMorningHandler gm = new GetBPSXFileMorningHandler();
		String host = gm.getParameter("SFTP_HOST_BPSX");
		String user = gm.getParameter("SFTP_USER_BPSX");
		//Add Decrypt 25 Jul 2016 -- Brian
		String password = EncryptUtils.DecodeDecrypt(gm.getParameter("SFTP_PASS_BPSX"));
		//--------------------------------
		String port = gm.getParameter("SFTP_PORT_BPSX");
		String workingPath = gm.getParameter("SFTP_WORK_PATH_BPSX");
		String localPath = gm.getParameter("SFTP_LOCAL_PATH_BPSX_MORNING");
		String privateKeyPath = gm.getParameter("PRIVATE_KEY_PATH_FILE");
		String isPrivateKey = gm.getParameter("IS_PRIVATE_KEY");
		String knownHost = gm.getParameter("KNOWN_HOST_KEY_PATH_FILE");
		String patternFile = gm.getParameter("PATTERN_FILE_MT940").substring(0, 3).concat("X");
		
		DownloadSFTPFiles ds = new DownloadSFTPFiles(host, user, password, port, workingPath, localPath, 
				privateKeyPath, isPrivateKey, knownHost, patternFile);
		String statusSFTP = ds.downloadFile();
		
		if ("SUCCESS".equalsIgnoreCase(statusSFTP)){
			System.out.println("[GetBPSXFileMorningHandler] get BPSX file morning is "+statusSFTP);
		}else if("FAILED".equalsIgnoreCase(statusSFTP)){
			System.out.println("[GetBPSXFileMorningHandler] get BPSX file morning is "+statusSFTP);
		}
	}
	
	public String getParameter(String param_name){
		List<SystemParameter> params = getParameterbyID(param_name);
		String param = "";
		for(SystemParameter sp : params){
			param = sp.getParam_value();
		}
		return param;
	}
	
	private List<SystemParameter> getParameterbyID(String param_name){
		SystemParameterDAO sysDAO = (SystemParameterDAO) applicationBean.getFileProcessContext().getBean("sysParamDAO");
		List<SystemParameter> params = new ArrayList<SystemParameter>(sysDAO.getValueParameter(param_name));
		return params;
	}
	
	public ApplicationBean getApplicationBean() {
		return applicationBean;
	}

	public void setApplicationBean(ApplicationBean applicationBean) {
		this.applicationBean = applicationBean;
	}

}
