package com.mii.beans;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import com.mii.constant.CommonConstant;
import com.mii.constant.ConstantNSCache;
import com.mii.constant.StepRecon;
import com.mii.file.TextFile;
import com.mii.flatFile.DetailContent;
import com.mii.flatFile.FileFormatter;
import com.mii.models.ReconFileM;
import com.mii.util.CommonDate;
import com.mii.util.ReconUtils;

public class ReconMPNFile {
	
	TextFile textFile = new TextFile();
	ReconUtils reconUtils = new ReconUtils();
	
	Boolean flag = true;
	String stepStatus = null;
	
	String workingDir;
	String backupDir;

	public void run(String schedulerID){
		workingDir 	= reconUtils.getSystemParameter(ConstantNSCache.directoryReconFile);
		backupDir	= reconUtils.getSystemParameter(ConstantNSCache.backupReconFile);
		processingReconMPN();
	}
	
	public void processingReconMPN(){
		checkReconFile();
		if (flag) coreReconFile();
	}
	
	public void checkReconFile(){
		if (textFile.checkAnyFile(workingDir)==1) {
			stepStatus(false, StepRecon.fileReconNotAvalilabelInLocal);
		} else {
			stepStatus(true, StepRecon.fileReconAvalilabelInLocal);
		}
		
	}
	
	public void coreReconFile(){
		File[] listReconFile = textFile.listAnyFile(workingDir);
		
		for (int i = 0; i < listReconFile.length; i++) {
			processingRecon(workingDir+listReconFile[i].getName());
			if (flag) textFile.moveFile(workingDir+listReconFile[i].getName(), backupDir+listReconFile[i].getName());
		}
	}
	
	public void processingRecon(String fileName){
		
		//init
		Boolean firstLine 	= false;
		String regex		= CommonConstant.ReconFileRegex;
		String reconTime	= CommonDate.getCurrentDateString("ddMMyyyy");
		
		//processing file
		FileFormatter fileFormatter = new FileFormatter();
		try {
			ArrayList<String> a = fileFormatter.getLine(firstLine,fileName);
			for (int i = 0; i < a.size(); i++) {
				String[] b = fileFormatter.getContentByRegex(a.get(i), regex);
				
				ArrayList<ReconFileM> detailContent = new DetailContent().reconFile(b);
				
				for (int j = 0; j < detailContent.size(); j++) {
					
					try {
						reconUtils.saveMPNReconFile(reconTime, detailContent.get(j).getCaID(), detailContent.get(j).getCaName(), detailContent.get(j).getSettlementDate(), detailContent.get(j).getCurrency(), detailContent.get(j).getBillingCode(), detailContent.get(j).getNtb(), detailContent.get(j).getNtpn(), detailContent.get(j).getAmount(), detailContent.get(j).getNoSakti());
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					try {
						reconUtils.updateNTPN(detailContent.get(j).getNtpn(), detailContent.get(j).getNtb(), detailContent.get(j).getBillingCode());
					} catch (Exception e) {
						e.printStackTrace();
					}
					
				}
				stepStatus(true, StepRecon.successReconciliateFile);
				System.out.println("[ReconMPNFile] Status processing recon file "+fileName+" is "+stepStatus);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	
	}
	
	public void stepStatus(Boolean flagStep, String stepResult){
		flag		= flagStep;
		stepStatus	= stepResult;
	}
}
