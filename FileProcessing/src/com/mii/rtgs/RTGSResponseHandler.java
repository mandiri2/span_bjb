package com.mii.rtgs;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import com.mii.constant.CommonConstant;
import com.mii.constant.Logger;
import com.mii.constant.ConstantNSCache;
import com.mii.constant.State;
import com.mii.constant.StepRTGS;
import com.mii.file.TextFile;
import com.mii.logger.Log;
import com.mii.logger.LoggerEvent;
import com.mii.models.RTGSM;
import com.mii.record.RTGSResponseFile;
import com.mii.sftp.SFTPPrivateKey;
import com.mii.util.CommonDate;
import com.mii.util.RTGSUtils;

public class RTGSResponseHandler {
	private static Log log = Log.getInstance(RTGSResponseHandler.class);
	
	Boolean flag 		= true;
	String stepStatus 	= null;
	
	ConcurrentHashMap<String, String> cacheParameter = new ConcurrentHashMap<String, String>();
	List<RTGSM> rtgsData = new ArrayList<RTGSM>();
	
	RTGSUtils rtgsUtils;
	TextFile textFile;
	
	private void initParameter(){
		rtgsUtils	= new RTGSUtils();
		textFile	= new TextFile();
		cacheParameter 		= rtgsUtils.putParameterToCache(rtgsUtils.getAllParameter());
		rtgsData	= rtgsUtils.getRTGSData("1");
	}
	
	private void mapParameterName(){
		hostBPP 			= cacheParameter.get(ConstantNSCache.hostBPP);
		usernameBPP			= cacheParameter.get(ConstantNSCache.usernameBPP);
		portBPP				= cacheParameter.get(ConstantNSCache.portBPP);
		privateKeyPathBPP	= cacheParameter.get(ConstantNSCache.privateKeyPathBPP);
		workingPathBPP		= cacheParameter.get(ConstantNSCache.workingPathBPP);
		workingPathBPO		= cacheParameter.get(ConstantNSCache.workingPathBPO);
		responsePathBPP		= cacheParameter.get(ConstantNSCache.responsePathBPP);
		fileNameBPPResponse	= cacheParameter.get(ConstantNSCache.fileNameBPPResponse);
		fileNameBPPRequest	= cacheParameter.get(ConstantNSCache.fileNameBPPRequest);
		backupResponseDirBPP= cacheParameter.get(ConstantNSCache.backupResponseDirBPP);
		knownHost			= cacheParameter.get(ConstantNSCache.knownHostKey);
	}
	
	public void run(){
		initParameter();
		if (cacheParameter.isEmpty()) {
			stepStatus(false, StepRTGS.failedInitiateDataRTGSRequest);
			LoggerEvent.infoEvent(log, Logger.initEvent, Logger.processRTGSResponse, stepStatus);
		} else {
			if (rtgsData.size()==0) {
				stepStatus(false, StepRTGS.failedInitiateDataRTGSRequest);
				LoggerEvent.infoEvent(log, Logger.initEvent, Logger.processRTGSResponse, stepStatus);
			} else {
				stepStatus(true, StepRTGS.successInitiateDataRTGSRequest);
				LoggerEvent.infoEvent(log, Logger.initEvent, Logger.processRTGSResponse, stepStatus);
				
				mapParameterName();
				processingRTGSResponse();
				
			}
		}		
	}
	
	private void processingRTGSResponse(){
		for (int i = 0; i < rtgsData.size(); i++) {
			try {
				paramUnikID 	= rtgsData.get(i).getReserved1();
				transactionID	= rtgsData.get(i).getTransactionID();
				
				bppResponseFile	= fileNameBPPResponse+paramUnikID;
				
				getRTGSResponseFile();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private void getRTGSResponseFile(){
		LoggerEvent.infoEvent(log, Logger.startEvent, Logger.processRTGSResponse, CommonConstant.emptyString);
		
		checkFileRTGSResponse();
		
		if (flag) readFileRTGSResponse();
		
		if (flag) extractRTGSResponse();
		
		if (flag) moveRTGSResponse();
		
		if (flag) {
			LoggerEvent.infoEvent(log, Logger.finishEvent, Logger.processRTGSResponse, StepRTGS.successRTGSResponse);
		} else {
			LoggerEvent.infoEvent(log, Logger.finishEvent, Logger.processRTGSResponse, StepRTGS.failedRTGSResponse);
		}
		
	}
	
	private void checkFileRTGSResponse(){
		
		if (textFile.checkSpecificFile(workingPathBPO, bppResponseFile)==1) {
			SFTPPrivateKey sftpPrivateKey = new SFTPPrivateKey(hostBPP, usernameBPP, portBPP, privateKeyPathBPP, knownHost);
			if (sftpPrivateKey.statusConnection()==0) stepStatus(false, StepRTGS.sftpBPOXNotConnected);
			if (flag) if (sftpPrivateKey.checkSpecificFile(responsePathBPP, "BPOX"+paramUnikID) == 1) stepStatus(false, StepRTGS.fileBPOXNotFound);
			if (flag) getFileRTGSResponse(sftpPrivateKey);
		} else {
			stepStatus(true, StepRTGS.fileAlreadyInLocalPath);
		}
		LoggerEvent.debugProcess(log, Logger.processRTGSResponse, Logger.detailProcessCheckBPOXFile, stepStatus);
		System.out.println("checkFileRTGSResponse() = "+stepStatus);
	}
	
	private void getFileRTGSResponse(SFTPPrivateKey sftpPrivateKey){
		String status = sftpPrivateKey.cutSpecificFile(responsePathBPP, workingPathBPO, bppResponseFile);
		if (status.equalsIgnoreCase(State.Failed)) {
			stepStatus(false, StepRTGS.failedDownloadBPOX);
		}else {
			stepStatus(true, StepRTGS.successDownloadBPOX);
		}
		if (sftpPrivateKey.statusConnection()==1) sftpPrivateKey.removeConnection();
		LoggerEvent.debugProcess(log, Logger.processRTGSResponse, Logger.detailProcessgetFileRTGSRsponse, stepStatus);
		System.out.println("getFileRTGSResponse() = "+stepStatus);
	}
	
	private void readFileRTGSResponse(){
		
		try {
			contentResponseRTGS = textFile.readFile(workingPathBPO, bppResponseFile);
			stepStatus(true, StepRTGS.successReadBPOX);
		} catch (Exception e) {
			stepStatus(false, StepRTGS.failedReadBPOX);
		}
		
		LoggerEvent.debugProcess(log, Logger.processRTGSResponse, Logger.detailProcessreadBPOXFile, stepStatus);
		System.out.println("readFileRTGSResponse() = "+stepStatus);
	}
	
	private void extractRTGSResponse(){
		
		RTGSResponseFile rtgsResponseFile = new RTGSUtils().readRTGSResponseFile(contentResponseRTGS);
		if (rtgsResponseFile.getResponse().equals(CommonConstant.BPPResultReject)) {
			stepStatus(true, StepRTGS.rejectResponseBPP);
			rtgsUtils.updateResponseRTGS(StepRTGS.rtgsResponse, "0", transactionID, CommonDate.stringToDate("ddMMyyyy", rtgsResponseFile.getTimeStamp()), rtgsResponseFile.getUserID(), rtgsResponseFile.getResponse(), rtgsResponseFile.getResponseDesc(), rtgsResponseFile.getReferenceNo(), bppResponseFile);
		} else {
			stepStatus(true, StepRTGS.successResponseBPP);
			rtgsUtils.updateResponseRTGS(StepRTGS.rtgsResponse, "2", transactionID, CommonDate.stringToDate("ddMMyyyy", rtgsResponseFile.getTimeStamp()), rtgsResponseFile.getUserID(), rtgsResponseFile.getResponse(), rtgsResponseFile.getResponseDesc(), rtgsResponseFile.getReferenceNo(), bppResponseFile);
		}
		LoggerEvent.debugProcess(log, Logger.processRTGSResponse, Logger.detailProcessextractBPOXFile, stepStatus);
		System.out.println("extractRTGSResponse() = "+stepStatus);
	}
	
	private void moveRTGSResponse(){
		if (textFile.getStatus()==State.Success) textFile.copyFile(workingPathBPO.concat(bppResponseFile), backupResponseDirBPP.concat(bppResponseFile));
		if (textFile.getStatus()==State.Success) textFile.deleteSpecificFile(responsePathBPP, bppResponseFile);
		
		LoggerEvent.debugProcess(log, Logger.processRTGSResponse, Logger.detailProcessmoveFile, textFile.getStatus());
		System.out.println("moveRTGSResponse() = "+stepStatus);
	}
	
	private void stepStatus(Boolean flagStep, String stepResult){
		flag		= flagStep;
		stepStatus	= stepResult;
	}
	
	String hostBPP, usernameBPP, portBPP, privateKeyPathBPP, workingPathBPP, responsePathBPP, fileNameBPPResponse, fileNameBPPRequest, backupResponseDirBPP, knownHost, workingPathBPO;
	String paramUnikID, transactionID, contentResponseRTGS, bppResponseFile;
	
}
