package com.mii.rtgs;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.mii.constant.CommonConstant;
import com.mii.constant.ConstantNSCache;
import com.mii.constant.Logger;
import com.mii.constant.State;
import com.mii.constant.StepRTGS;
import com.mii.file.TextFile;
import com.mii.logger.Log;
import com.mii.logger.LoggerEvent;
import com.mii.models.RTGSM;
import com.mii.models.TransactionM;
import com.mii.util.CommonDate;
import com.mii.util.RTGSUtils;
import com.mii.ws.WebServiceGetNoSaktiExecutor;
import com.mii.ws.getNoSakti.InParameter;
import com.mii.ws.getNoSakti.OutParameter;

public class GetNoSakti {
	private static Log log = Log.getInstance(GetNoSakti.class);

	public void run(){
		LoggerEvent.infoEvent(log, Logger.startEvent, Logger.processGetNoSakti, CommonConstant.emptyString);
		
		Boolean flag = true;
		InParameter input 	= new InParameter();
		OutParameter output = new OutParameter();
		
		RTGSUtils rtgsUtils = new RTGSUtils();
		TextFile textFile	= new TextFile();
		GetNoSakti gt = new GetNoSakti();
		
		String noSakti = null, status = null, bpsxFileName;
		String workingPathBPSX	= rtgsUtils.getSystemParameter(ConstantNSCache.workingPathBPSX);
		String workingPathBS = rtgsUtils.getSystemParameter(ConstantNSCache.localPathBS);
		
		List<RTGSM> rtgsData = rtgsUtils.getRTGSData("2");
		
		for (int i = 0; i < rtgsData.size(); i++) {
			try{
				//Check no sakti via ws to mw
				String referenceNo = rtgsData.get(i).getReferenceNumber();
				if (flag) {
					input.setRefNo(referenceNo);
					WebServiceGetNoSaktiExecutor webServiceGetNoSaktiExecutor = new WebServiceGetNoSaktiExecutor();
					output = webServiceGetNoSaktiExecutor.getNoSakti(input);
					
					if (output != null) {
						noSakti = output.getMir();
						System.out.println("[GetNoSakti] Reference Number = "+referenceNo+" with No Sakti = "+noSakti);
						status	= output.getStatus();
						if(noSakti != null){
							//update rtgs
							rtgsUtils.updateNoSaktiRTGS(StepRTGS.rtgsGetNoSakti, CommonDate.getCurrentDate("ddMMyyyy"), "3", referenceNo, noSakti, status);
							
							//update nosakti mpn_trx_log
							bpsxFileName = rtgsData.get(i).getBpsxName();
							String[] bpsxFile= bpsxFileName.split(CommonConstant.andSeparator);
							for (int k = 0; k < bpsxFile.length; k++) {
								
								try {
									ArrayList<String> listReferenceNo = textFile.getSpecificContentInFolder(workingPathBPSX, bpsxFile[k], CommonConstant.lineSeparator, CommonConstant.BPSXContentReferenceContentLineLength, CommonConstant.BPSXContentReferenceNoStartIdx, CommonConstant.BPSXContentReferenceNoEndIdx);
									
									for (int j = 0; j < listReferenceNo.size(); j++){
										try {
											rtgsUtils.updateNoSakti(listReferenceNo.get(j), noSakti);
										} catch (Exception e) {
											e.printStackTrace();
										}
									}
									
									if (textFile.getStatus()==State.Success) {
										//copy file to path generate BS
										textFile.copyFile(workingPathBPSX.concat(bpsxFile[k]), workingPathBS.concat(bpsxFile[k]));
										//textFile.deleteSpecificFile(workingPathBPSX, bpsxFile[k]);
									}
								} catch (Exception e) {
									e.printStackTrace();
								}
								
							}
							System.out.println("[GetNoSakti]Get Nomor Sakti = "+noSakti);
						}
						
					} 
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
		//validate manual MIR
		gt.validateManualMIR(workingPathBPSX);
		
		LoggerEvent.infoEvent(log, Logger.finishEvent, Logger.processGetNoSakti, "noSakti "+noSakti);
		
	}
	
	private void validateManualMIR(String workingPathBPSX){
		RTGSUtils rtgsUtils = new RTGSUtils();
		TextFile textFile = new TextFile();
		String workingPathBS = rtgsUtils.getSystemParameter(ConstantNSCache.localPathBS);
			try {
					List<TransactionM> rtgsData = rtgsUtils.getRTGSData10("10");
					//System.out.println("size rtgsData = "+rtgsData.size());
					for (int i = 0; i < rtgsData.size(); i++) {
						try {

							String settlementDate = rtgsData.get(i).getNtpn();
							System.out.println("settlementDate = "+settlementDate);
							String reffNo = rtgsData.get(i).getPelimpahanID();
							System.out.println("Reff No = "+reffNo);
							String currentYear = CommonDate.getCurrentDateString("YYYY");
							String filename = "BPS"+currentYear+settlementDate;
			 
							//check file
							System.out.println("Check file "+filename+" in "+workingPathBPSX);
							File[] file = textFile.listFile(workingPathBPSX, filename);
							System.out.println("total file = "+file.length);
							for (int j = 0; j < file.length; j++) {
								try {
									//copy file to path generate BS
									textFile.copyFile(workingPathBPSX.concat(file[i].getName()), workingPathBS.concat(file[i].getName()));
									
									//textFile.moveFile(workingPathBPSX.concat(file[i].getName()), workingPathBS.concat(file[i].getName()));
									//textFile.deleteSpecificFile(workingPathBPSX, file[i].getName());
									
									//update flag again
									rtgsUtils.updateRTGSFlagByReff(reffNo);
								} catch (Exception e) {
									e.printStackTrace();
								}
			 
							}
						} catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}
					}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
}
