package com.mii.rtgs;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
//import java.util.List;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import com.mii.constant.CommonConstant;
import com.mii.constant.Logger;
import com.mii.constant.ConstantNSCache;
import com.mii.constant.State;
import com.mii.constant.StepRTGS;
import com.mii.file.TextFile;
import com.mii.logger.Log;
import com.mii.logger.LoggerEvent;
import com.mii.models.RTGSM;
//import com.mii.models.TransactionM;
import com.mii.record.BPSXFooterFile;
import com.mii.record.BPSXHeaderFile;
import com.mii.sftp.SFTPPrivateKey;
import com.mii.util.CommonDate;
import com.mii.util.CommonUtils;
import com.mii.util.RTGSUtils;

public class RTGSRequestHandler{
	
	private static Log log = Log.getInstance(RTGSRequestHandler.class);
	
	Boolean flag = true;
	String stepStatus = null;
	
	ConcurrentHashMap<String, String> cacheParameter = new ConcurrentHashMap<String, String>();
	ArrayList<RTGSM> listRTGS = new ArrayList<RTGSM>();
	
	RTGSUtils rtgsUtils;
	TextFile textFile;
	
	public RTGSRequestHandler() {
		initiateProcess();
	}
	
	private void initiateProcess(){
		initParameterCache();
		if (cacheParameter.isEmpty()) {
			stepStatus(false, StepRTGS.failedInitiateDataRTGSRequest);
			LoggerEvent.infoEvent(log, Logger.initEvent, Logger.processRTGSRequest, stepStatus);
		} else {
			mapParameterName();
			rtgsData();
			
			stepStatus(true, StepRTGS.successInitiateDataRTGSRequest);
			LoggerEvent.infoEvent(log, Logger.initEvent, Logger.processRTGSRequest, stepStatus);
		}
	}

	private void initParameterCache(){
		rtgsUtils	= new RTGSUtils();
		textFile	= new TextFile();
		cacheParameter = rtgsUtils.putParameterToCache(rtgsUtils.getAllParameter());
	}
	
	private void mapParameterName(){
		hostBPP 			= cacheParameter.get(ConstantNSCache.hostBPP);
		usernameBPP			= cacheParameter.get(ConstantNSCache.usernameBPP);
		portBPP				= cacheParameter.get(ConstantNSCache.portBPP);
		privateKeyPathBPP	= cacheParameter.get(ConstantNSCache.privateKeyPathBPP);
		remotePathBPP		= cacheParameter.get(ConstantNSCache.requestPathBPP);
		workingPathBPP		= cacheParameter.get(ConstantNSCache.workingPathBPP);
		fileNameBPPRequest	= cacheParameter.get(ConstantNSCache.fileNameBPPRequest);
		backupRequestDirBPP	= cacheParameter.get(ConstantNSCache.backupRequestDirBPP);
		knownHost			= cacheParameter.get(ConstantNSCache.knownHostKey);
		
		workingPathBPSX		= cacheParameter.get(ConstantNSCache.workingPathBPSX);
		backupDirBPSX		= cacheParameter.get(ConstantNSCache.backupDirBPSX);
		fileNameBPSX		= cacheParameter.get(ConstantNSCache.filePatternBPSX);
		
		destinationAccountNumber	=cacheParameter.get(ConstantNSCache.destinationAccountNumber); 
		destinationAccountName		=cacheParameter.get(ConstantNSCache.destinationAccountName); 
		bankCode			=cacheParameter.get(ConstantNSCache.bankCode); 
		description1		=cacheParameter.get(ConstantNSCache.description1); 
		description2		=cacheParameter.get(ConstantNSCache.description2); 
		description3		=cacheParameter.get(ConstantNSCache.description3); 
		description4		=cacheParameter.get(ConstantNSCache.description4); 
		userID				=cacheParameter.get(ConstantNSCache.userID); 
		reserved1			=cacheParameter.get(ConstantNSCache.reserved1); 
		reserved2			=cacheParameter.get(ConstantNSCache.reserved2); 
		reserved3			=cacheParameter.get(ConstantNSCache.reserved3); 
		reserved4			=cacheParameter.get(ConstantNSCache.reserved4); 
	}
	
	private void rtgsData(){
		transactionDate	= CommonDate.getCurrentDate("ddMMyyyy");
		paymentType		= CommonConstant.BPPPaymentTye;
	}
	
	public String executeRTGS(String executeDate){
		fileNameBPSX	= fileNameBPSX+executeDate;
		if (flag) run();
		return stepStatus;
	}
	
	private void run(){
		LoggerEvent.infoEvent(log, Logger.startEvent, Logger.processRTGSRequest, CommonConstant.emptyString);
		
		//checking data --> is already execute?
		//checkStatusBPSX();
		
		checkBPSXFile();
		
		if (flag) readBPSXFile();
		
		if (flag) generateRTGSFileRequest();
		
		if (flag) putFileRTGSRequest();
			
		if (flag) moveFile();
		
		if (flag) insertRTGSRequest();
		
		if (flag) removeAllOfHashMap();
		
		if (flag) {
			LoggerEvent.infoEvent(log, Logger.finishEvent, Logger.processRTGSRequest, StepRTGS.successRTGSRequest);
			stepStatus = StepRTGS.successRTGSRequest;
		} else {
			LoggerEvent.infoEvent(log, Logger.finishEvent, Logger.processRTGSRequest, StepRTGS.failedRTGSRequest);
		}
		
	}
	
	private void stepStatus(Boolean flagStep, String stepResult){
		flag		= flagStep;
		stepStatus	= stepResult;
	}
	
	/*private void checkStatusBPSX(){
		//check rtgs data in database
		List<TransactionM> rtgsData = rtgsUtils.checkRTGSData(fileNameBPSX);
		String bpsName = "";
		if(rtgsData.size() > 0){
			for(TransactionM tm : rtgsData){
				bpsName = tm.getPaymentCode();
			}
			
			stepStatus(false, "File "+bpsName+" is already executed!");
		}else{
			stepStatus(true, "BPS file is new file");
		}
		LoggerEvent.debugProcess(log, Logger.processRTGSRequest, Logger.detailProcessCheckDataBPS, stepStatus);
		System.out.println("checkStatusBPSX() = "+stepStatus);
	}*/
	
	private void checkBPSXFile(){
		
		if (textFile.checkFile(workingPathBPSX, fileNameBPSX)==1) {
			stepStatus(false, StepRTGS.fileBPSXFinalNotAvalilabelInLocal);
		} else {
			stepStatus(true, StepRTGS.fileBPSXFinalAvalilabelInLocal);
		}
		LoggerEvent.debugProcess(log, Logger.processRTGSRequest, Logger.detailProcessCheckBPSXFile, stepStatus);
		System.out.println("checkBPSXFile() = "+stepStatus);
	}
	
	private void readBPSXFile(){
		String accountCurrency, sourceAccountNumber;
		long endingBalance = 0, beginningBalance= 0, amount = 0, totalAmount = 0;
		
		String contentHeader, contentFooter;
//		ArrayList<String> contentDetail = new ArrayList<String>();
		HashMap<String, String> temp = new HashMap<String, String>();
		
		File[] files 	= textFile.listFile(workingPathBPSX, fileNameBPSX);
		
		for (int i = 0; i < files.length; i++) {
			String bpsxName		= files[i].getName();
			//get content
			contentHeader = textFile.getSpecificLineFile(files[i], CommonConstant.lineSeparator, CommonConstant.BPSXContentHeaderLength).get(0);
//			contentDetail = textFile.getSpecificLineFile(files[i], CommonConstant.lineSeparator, CommonConstant.BPSXContentDetailLength);
			contentFooter = textFile.getSpecificLineFile(files[i], CommonConstant.lineSeparator, CommonConstant.BPSXContentFooterLength).get(0);
			
			//get data
			BPSXHeaderFile bpsxHeaderFile = rtgsUtils.readBPSXHeaderFile(contentHeader);
			BPSXFooterFile bpsxFooterFile = rtgsUtils.readBPSXFooterFile(contentFooter);
			
			accountCurrency 	= bpsxHeaderFile.getCurrency();
			sourceAccountNumber	= bpsxHeaderFile.getAccountNo().trim();
			beginningBalance 	= Long.parseLong(CommonUtils.unPadLeft(bpsxHeaderFile.getBeginningBalance(), '0'));
			//System.out.println("Beginning Balance = "+beginningBalance);
			endingBalance 		= Long.parseLong(CommonUtils.unPadLeft(bpsxFooterFile.getEndingBalance(), '0'));
			//System.out.println("Ending balance = "+endingBalance);
			//amount				= endingBalance-beginningBalance;
			amount				= endingBalance;
			//System.out.println("Total Amount = "+amount);
			
			String key = accountCurrency+CommonConstant.stripSeparator+sourceAccountNumber+CommonConstant.stripSeparator;

			if (temp.containsKey(key)) {
				totalAmount = amount + Long.parseLong(temp.get(key).split(CommonConstant.andSeparator)[0]);
				bpsxName	= bpsxName + temp.get(key).split(CommonConstant.andSeparator)[1];
			}else{
				totalAmount = amount;
			}
			//System.out.println("Total amount = "+totalAmount);
			
			temp.put(key, Long.toString(totalAmount)+CommonConstant.andSeparator+bpsxName);
			
		}
		
		for(Entry<String, String> entry: temp.entrySet()) {
	        RTGSM rtgsm = new RTGSM();
	        String BPPFilenameUnikID = "";
	        String transactionID = "";
	        //String BPPFilenameUnikID 	= CommonConstant.BPPFilenameUnikID;
	        //String transactionID		= CommonConstant.transactionID;
	        
	        BPPFilenameUnikID = CommonDate.getCurrentDateString("yyyyMMddHHmmss");
	        System.out.println("[RTGSRequest Service] BPPFilename = "+fileNameBPPRequest.concat(BPPFilenameUnikID));
	        
	        //for test generate id only
	        transactionID = UUID.randomUUID().toString();
	        System.out.println("[RTGSRequest Service] Transaction ID first = "+transactionID);
	        
	        transactionID = UUID.randomUUID().toString();
	        System.out.println("[RTGSRequest Service] Transaction ID final = "+transactionID);
	        rtgsm.setTransactionID(transactionID);
	        rtgsm.setAccountCurrency(entry.getKey().split(CommonConstant.stripSeparator)[0]);
	        rtgsm.setSourceAccountNumber(entry.getKey().split(CommonConstant.stripSeparator)[1]);
	        rtgsm.setBpsxName(entry.getValue().split(CommonConstant.andSeparator)[1]);
	        rtgsm.setBppxName(fileNameBPPRequest.concat(BPPFilenameUnikID));
	        rtgsm.setReserved1(BPPFilenameUnikID);
	        //System.out.println("Amount = "+CommonUtils.padleft(entry.getValue().split(CommonConstant.andSeparator)[0], 15, '0'));
	        rtgsm.setTransactionAmount(CommonUtils.padleft(entry.getValue().split(CommonConstant.andSeparator)[0], 15, '0'));
	        
	        listRTGS.add(rtgsm);
	    }
		stepStatus(true, StepRTGS.succesReadBPSX);
		try {
			
		} catch (Exception e) {
			stepStatus(false, StepRTGS.failedReadBPSX);
			e.printStackTrace();
			LoggerEvent.errorProcess(log, Logger.processRTGSRequest, Logger.detailProcessreadBPSXFile, e.getMessage());
		}
		LoggerEvent.debugProcess(log, Logger.processRTGSRequest, Logger.detailProcessreadBPSXFile, stepStatus);
		System.out.println("readBPSXFile() = "+stepStatus);
	}
	
	private void generateRTGSFileRequest(){
		try {
			//get date for description1 in RTGS
			DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
			Date date = new Date();
			description1 = dateFormat.format(date).concat("-");
			
			for (int i = 0; i < listRTGS.size(); i++) {
				
				String content = rtgsUtils.generateRTGSRequestFile(listRTGS.get(i).getAccountCurrency(), listRTGS.get(i).getSourceAccountNumber(), destinationAccountNumber, destinationAccountName, listRTGS.get(i).getTransactionAmount(), paymentType, bankCode, description1, description2, description3, description4, referenceNo);
				
				textFile.writeToFile(content, workingPathBPP, listRTGS.get(i).getBppxName());
				
				if (textFile.getStatus()==State.Failed) {
					listRTGS.remove(i);
					stepStatus(false, StepRTGS.failedGenerateRTGSRequestFile);
				} else {
					stepStatus(true, StepRTGS.successGenerateRTGSRequestFile+" "+listRTGS.get(i).getBppxName());
				}
			}
			
		} catch (Exception e) {
			stepStatus(false, StepRTGS.failedGenerateRTGSRequestFile);
			e.printStackTrace();
			LoggerEvent.errorProcess(log, Logger.processRTGSRequest, Logger.detailProcessgenerateRTGSFileRequest, e.getMessage());
		}
		LoggerEvent.debugProcess(log, Logger.processRTGSRequest, Logger.detailProcessgenerateRTGSFileRequest, stepStatus);
		System.out.println("generateRTGSFileRequest() = "+stepStatus);
	}
	
	private void putFileRTGSRequest(){
		try {
			SFTPPrivateKey sftpPrivateKey = new SFTPPrivateKey(hostBPP, usernameBPP, portBPP, privateKeyPathBPP, knownHost);
			if (sftpPrivateKey.statusConnection()==0) stepStatus(false, StepRTGS.sftpRTGSNotConnected);
			
			for (int i = 0; i < listRTGS.size(); i++) {
				if (flag) {
					String status=sftpPrivateKey.putFile(workingPathBPP, remotePathBPP, listRTGS.get(i).getBppxName());
					
					if (status.equalsIgnoreCase(State.Failed)) {
						stepStatus(false, StepRTGS.failedPutRTGSRequest);
					} else {
						stepStatus(true, StepRTGS.succesPutRTGSRequest);
					}
				}
			}
			
			if (sftpPrivateKey.statusConnection()==1) sftpPrivateKey.removeConnection();
		} catch (Exception e) {
			stepStatus(false, StepRTGS.failedPutRTGSRequest);
			e.printStackTrace();
			LoggerEvent.errorProcess(log, Logger.processRTGSRequest, Logger.detailProcessputFileRTGSRequest, e.getMessage());
		}
		LoggerEvent.debugProcess(log, Logger.processRTGSRequest, Logger.detailProcessputFileRTGSRequest, stepStatus);
		System.out.println("putFileRTGSRequest() = "+stepStatus);
	}
	
	private void moveFile(){
		try {
//			if (textFile.getStatus().equalsIgnoreCase(State.Success)){
//				textFile.copyAllFileInFolder(workingPathBPSX, backupDirBPSX);
//				textFile.copyAllFileInFolder(workingPathBPP, backupRequestDirBPP);
//			}
//			
//			if (textFile.getStatus().equalsIgnoreCase(State.Success)) textFile.deleteAllFileInFolder(workingPathBPP);
//			
//			if (textFile.getStatus().equalsIgnoreCase(State.Success)) stepStatus(true, StepRTGS.successMoveFile);
			
			for (int i = 0; i < listRTGS.size(); i++) {
				if (textFile.getStatus().equalsIgnoreCase(State.Success)){
					textFile.copyFile(workingPathBPSX.concat(listRTGS.get(i).getBpsxName()), backupDirBPSX.concat(listRTGS.get(i).getBpsxName()));
					textFile.copyFile(workingPathBPP.concat(listRTGS.get(i).getBppxName()), backupRequestDirBPP.concat(listRTGS.get(i).getBppxName()));
				}
				
				if (textFile.getStatus().equalsIgnoreCase(State.Success)) textFile.deleteSpecificFile(workingPathBPP, listRTGS.get(i).getBppxName());
				
				if (textFile.getStatus().equalsIgnoreCase(State.Success)) stepStatus(true, StepRTGS.successMoveFile);
			}
			
		} catch (Exception e) {
			stepStatus(false, StepRTGS.failedMoveFile);
			e.printStackTrace();
			LoggerEvent.errorProcess(log, Logger.processRTGSRequest, Logger.detailProcessmoveFile, e.getMessage());
		}
		LoggerEvent.debugProcess(log, Logger.processRTGSRequest, Logger.detailProcessmoveFile, textFile.getStatus());
		System.out.println("moveFile() = "+stepStatus);
	}
	
	private void insertRTGSRequest(){
		System.out.println("insertRTGSRequest() is running");
		for (int i = 0; i < listRTGS.size(); i++) {
			rtgsUtils.insertRTGS(StepRTGS.rtgsRequest, listRTGS.get(i).getTransactionID(), transactionDate, updateDate, transactionFlag, listRTGS.get(i).getAccountCurrency(), listRTGS.get(i).getSourceAccountNumber(), destinationAccountNumber, destinationAccountName, listRTGS.get(i).getTransactionAmount(), paymentType, bankCode, description1, description2, description3, description4, listRTGS.get(i).getReserved1(), reserved2, reserved3, reserved4, listRTGS.get(i).getBpsxName(), listRTGS.get(i).getBppxName());
		}
	}
	
	private void removeAllOfHashMap(){
		cacheParameter.clear();
		
		LoggerEvent.debugProcess(log, Logger.processRTGSRequest, Logger.detailProcessremoveAllOfHashMap, CommonConstant.emptyString);
	}
	
	String hostBPP, usernameBPP, portBPP, privateKeyPathBPP, remotePathBPP, workingPathBPP, fileNameBPPRequest, backupRequestDirBPP, knownHost;
	String workingPathBPSX, fileNameBPSX, backupDirBPSX;
	
	String paymentType;
	Date transactionDate, updateDate;
	String destinationAccountNumber, destinationAccountName, bankCode, description1, description2, description3, description4, referenceNo, userID, response, responseDesc, reserved1, reserved2, reserved3, reserved4="";
	
	String transactionFlag = "1";
	
}
