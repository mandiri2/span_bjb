package com.mii.file;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Scanner;
import com.mii.constant.CommonConstant;
import com.mii.constant.State;
import com.mii.logger.Log;
import com.mii.logger.LoggerEvent;
import com.mii.util.ExtractUtils;
import com.mii.util.FindFile;

public class TextFile {
	
	private static final Log log = Log.getInstance(TextFile.class);
	String status;
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void writeToFile(String content, String path, String fileName){
		try{
			File file = new File(path.concat(fileName));

			if (!file.exists()) file.createNewFile();

			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(content);
			bw.close();
			
			setStatus(State.Success);
		} catch (IOException e) {
			setStatus(State.Failed);
			e.printStackTrace();
			LoggerEvent.errorProcess(log, this.getClass().getName()+"/"+this.getClass().getMethods().toString(), e.getMessage());
		}
	}
	
	public String readFile(String path, String fileName){
		String content = null;
		try{
            FileReader fileReader = new FileReader(path.concat(fileName));
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            
            content = bufferedReader.readLine();
            
            bufferedReader.close();   
			
			setStatus(State.Success);
		} catch (IOException e) {
			setStatus(State.Failed);
			e.printStackTrace();
			LoggerEvent.errorProcess(log, this.getClass().getName()+"/"+this.getClass().getMethods().toString(), e.getMessage());
		}
		return content;
	}
	
	public String readFirstLineFileInFolder(String path, String fileName){
		File file = new File(path.concat(fileName));
		String line = null;
        try {
            Scanner scan = new Scanner(file);
            line = scan.nextLine();
            scan.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            LoggerEvent.errorProcess(log, this.getClass().getName()+"/"+this.getClass().getMethods().toString(), e.getMessage());
        }
        return line;
	}
	
	public String readLasLineFileInFolder(String path, String fileName) {
		
		String strLine = null, tmp;
		try {
			FileInputStream in = new FileInputStream(path.concat(fileName));
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			
			while ((tmp = br.readLine()) != null) strLine = tmp;
			
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
			LoggerEvent.errorProcess(log, this.getClass().getName()+"/"+this.getClass().getMethods().toString(), e.getMessage());
		}
		
		return strLine;
	}
	
	public String readFirstLineFile(File file){
		String line = null;
        try {
            Scanner scan = new Scanner(file);
            line = scan.nextLine();
            scan.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            LoggerEvent.errorProcess(log, this.getClass().getName()+"/"+this.getClass().getMethods().toString(), e.getMessage());
        }
        return line;
	}
	
	public ArrayList<String> readFirstLineAllFileInFolder(String path){
		File[] files = new File(path).listFiles();
		ArrayList<String> lines = new ArrayList<String>();
		
		for (int i = 0; i < files.length; i++) lines.add(readFirstLineFileInFolder(path, files[i].getName()));
		
        return lines;
	}
	
	public ArrayList<String> readLasLineAllFileInFolder(String path) {
		File[] files = new File(path).listFiles();
		ArrayList<String> lines = new ArrayList<String>();
		
		for (int i = 0; i < files.length; i++) lines.add(readLasLineFileInFolder(path, files[i].getName()));
		
		return lines;
	}
	
	public ArrayList<String> readFirstAndLastLineFileInFolder(String path){
		File[] files = new File(path).listFiles();
		ArrayList<String> lines = new ArrayList<String>();
		
		for (int i = 0; i < files.length; i++) lines.add(readFirstLineFileInFolder(path, files[i].getName())+CommonConstant.kresSeparator+readLasLineFileInFolder(path, files[i].getName())+CommonConstant.kresSeparator+files[i].getName());
		
		return lines;
	}
	
	public void moveFile(String sourceFullFileName, String destinationFullFileName){
		
		InputStream inStream = null;
		OutputStream outStream = null;
		int length;
		
		try{
			File sourceFile = new File(sourceFullFileName);
			File destFile = new File(destinationFullFileName);
			inStream = new FileInputStream(sourceFile);
			outStream = new FileOutputStream(destFile);
			byte[] buffer = new byte[1024];
			
			while((length = inStream.read(buffer)) > 0){
				outStream.write(buffer, 0, length);
			}
			inStream.close();
			outStream.close();
			
			//delete source file
			sourceFile.delete();
			setStatus(State.Success);
		}catch(Exception e){
			setStatus(State.Failed);
			e.printStackTrace();
		}
	}
	
	public void copyFile(String source, String destination){
		
		FileChannel inputChannel = null;
		FileChannel outputChannel = null;
		
		try {
			inputChannel 	= new FileInputStream(source).getChannel();
			outputChannel 	= new FileOutputStream(destination).getChannel();
			outputChannel.transferFrom(inputChannel, 0, inputChannel.size());
			setStatus(State.Success);
		} catch (FileNotFoundException e) {
			setStatus(State.Failed);
			e.printStackTrace();
			LoggerEvent.errorProcess(log, this.getClass().getName()+"/"+this.getClass().getMethods().toString(), e.getMessage());
		} catch (IOException e) {
			setStatus(State.Failed);
			e.printStackTrace();
			LoggerEvent.errorProcess(log, this.getClass().getName()+"/"+this.getClass().getMethods().toString(), e.getMessage());
		} finally {
			try {
				inputChannel.close();
				outputChannel.close();
			} catch (IOException e) {
				e.printStackTrace();
				LoggerEvent.errorProcess(log, this.getClass().getName()+"/"+this.getClass().getMethods().toString(), e.getMessage());
			}
		}
		
	}
	
	public void copyAllFileInFolder(String source, String destination){
		File[] files = new File(source).listFiles();
		
		for (int i = 0; i < files.length; i++) copyFile(files[i].getPath(), destination.concat(files[i].getName()));
	
	}
	
	public void deleteAllFileInFolder(String source){
		File[] files = new File(source).listFiles();
		
		for (int i = 0; i < files.length; i++) files[i].delete();
	}
	
	public void deleteSpecificFile(String path, String fileName){
		File file = new File(path.concat(fileName));
		file.delete();
	}
	
	public int checkFile(String dirPath, String fileNamePattern){
		try {
			File dir = new File(dirPath);
			
			File[] files = dir.listFiles(new FindFile(fileNamePattern+"*"));
			
			return files.length == 0 ? 1 : 0;
		} catch (Exception e) {
			e.printStackTrace();
			LoggerEvent.errorProcess(log, this.getClass().getName()+"/"+this.getClass().getMethods().toString(), e.getMessage());
			return 1;
		}
		
	}
	
	public int checkSpecificFile(String dirPath, String fileName){
		try {
			File dir = new File(dirPath);
			File[] files = dir.listFiles(new FindFile(fileName));
			
			return files.length == 0 ? 1 : 0;
		} catch (Exception e) {
			e.printStackTrace();
			LoggerEvent.errorProcess(log, this.getClass().getName()+"/"+this.getClass().getMethods().toString(), e.getMessage());
			return 1;
		}
		
	}
	
	public int checkAnyFile(String dirPath){
		try {
			File dir = new File(dirPath);
			File[] files = dir.listFiles();
			
			return files.length == 0 ? 1 : 0;
		} catch (Exception e) {
			e.printStackTrace();
			LoggerEvent.errorProcess(log, this.getClass().getName()+"/"+this.getClass().getMethods().toString(), e.getMessage());
			return 1;
		}
		
	}
	
	public File[] listFile(String dirPath, String filePattern){
		try {
			File dir = new File(dirPath);
			
			File[] files = dir.listFiles(new FindFile(filePattern.concat("*")));
			
			return files;
		} catch (Exception e) {
			e.printStackTrace();
			LoggerEvent.errorProcess(log, this.getClass().getName()+"/"+this.getClass().getMethods().toString(), e.getMessage());
			return null;
		}
		
	}
	
	public File[] listAnyFile(String dirPath){
		try {
			File dir = new File(dirPath);
			File[] files = dir.listFiles();
			
			return files;
		} catch (Exception e) {
			e.printStackTrace();
			LoggerEvent.errorProcess(log, this.getClass().getName()+"/"+this.getClass().getMethods().toString(), e.getMessage());
			return null;
		}
		
	}
	
	public void saveSpecificContent(String dirPath, String separator, String regex, int lineLength, int startIdx, int endIdx, String dirPathToSave, String fileNameToSave){
		String content	= null;
		String reader	= null;
		Scanner scan 	= null;
		
		try {
			scan = new Scanner(new File(dirPath));
			scan.useDelimiter(System.getProperty(separator));
			
			while(scan.hasNext()){
				reader = scan.next();
				if(reader.length() == lineLength) {
					if (content == null) {
						content = reader.substring(startIdx, endIdx);
					} else {
						content = content+regex+reader.substring(startIdx, endIdx);
					}
				}
			}
			scan.close();
			
			if (content != null) writeToFile(content, dirPathToSave, fileNameToSave);
			setStatus(State.Success);
		} catch (FileNotFoundException e) {
			setStatus(State.Failed);
			e.printStackTrace();
			LoggerEvent.errorProcess(log, this.getClass().getName()+"/"+this.getClass().getMethods().toString(), e.getMessage());
		}
		
	}
	
	public ArrayList<String> getSpecificContentInFolder(String dirPath, String fileName, String separator, int lineLength, int startIdx, int endIdx){
		String reader	= null;
		Scanner scan 	= null;
		ArrayList<String> listContent = new ArrayList<String>();
		try {
			scan = new Scanner(new File(dirPath.concat(fileName)));
			scan.useDelimiter(System.getProperty(separator));
			
			while(scan.hasNext()){
				reader = scan.next();
				if(reader.length() == lineLength) listContent.add(reader.substring(startIdx, endIdx));
			}
			scan.close();
			
			setStatus(State.Success);
			return listContent;
		} catch (FileNotFoundException e) {
			setStatus(State.Failed);
			e.printStackTrace();
			LoggerEvent.errorProcess(log, this.getClass().getName()+"/"+this.getClass().getMethods().toString(), e.getMessage());
			return null;
		}
		
	}
	
	public ArrayList<String> getSpecificLineFile(File file, String separator, int lineLength){
		String reader	= null;
		Scanner scan 	= null;
		ArrayList<String> listContent = new ArrayList<String>();
		ExtractUtils et = new ExtractUtils();
		try {
			scan = new Scanner(file);
			scan.useDelimiter(System.getProperty(separator));
			
			while(scan.hasNext()){
				reader = et.checkRecordBPS(scan.next());
				System.out.println("Reader = ["+reader+"]");
				
				if(reader.length() == lineLength) {
					listContent.add(reader);
				}
			}
			scan.close();
			
			setStatus(State.Success);
			return listContent;
		} catch (FileNotFoundException e) {
			setStatus(State.Failed);
			e.printStackTrace();
			LoggerEvent.errorProcess(log, this.getClass().getName()+"/"+this.getClass().getMethods().toString(), e.getMessage());
			return null;
		}
		
	}
	
	FilenameFilter txtFilter = new FilenameFilter() {
	    public boolean accept(File file, String name) {
	        return name.endsWith(CommonConstant.textFileFormat) ? true : false;
	    }
	};
	
	FilenameFilter bpsxFilter = new FilenameFilter() {
	    public boolean accept(File file, String name) {
	        return name.startsWith("BPSX") ? true : false;
	    }
	};
}
