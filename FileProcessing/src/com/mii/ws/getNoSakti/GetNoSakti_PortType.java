/**
 * GetNoSakti_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.mii.ws.getNoSakti;

public interface GetNoSakti_PortType extends java.rmi.Remote {
    public com.mii.ws.getNoSakti.OutParameter getNoSakti(com.mii.ws.getNoSakti.InParameter inParameter) throws java.rmi.RemoteException;
}
