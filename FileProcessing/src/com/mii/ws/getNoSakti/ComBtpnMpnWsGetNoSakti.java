/**
 * ComBtpnMpnWsGetNoSakti.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.mii.ws.getNoSakti;

public interface ComBtpnMpnWsGetNoSakti extends javax.xml.rpc.Service {
    public java.lang.String getcom_btpn_mpn_ws_GetNoSakti_PortAddress();

    public com.mii.ws.getNoSakti.GetNoSakti_PortType getcom_btpn_mpn_ws_GetNoSakti_Port() throws javax.xml.rpc.ServiceException;

    public com.mii.ws.getNoSakti.GetNoSakti_PortType getcom_btpn_mpn_ws_GetNoSakti_Port(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
