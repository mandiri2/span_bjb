/**
 * ComBtpnMpnWsGetNoSaktiLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.mii.ws.getNoSakti;

import com.mii.ws.WebServiceConfigurator;

public class ComBtpnMpnWsGetNoSaktiLocator extends org.apache.axis.client.Service implements com.mii.ws.getNoSakti.ComBtpnMpnWsGetNoSakti {

	WebServiceConfigurator conf = new WebServiceConfigurator();
	
    public ComBtpnMpnWsGetNoSaktiLocator() {
    }


    public ComBtpnMpnWsGetNoSaktiLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public ComBtpnMpnWsGetNoSaktiLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for com_btpn_mpn_ws_GetNoSakti_Port
    private java.lang.String com_btpn_mpn_ws_GetNoSakti_Port_address = conf.getAddressGetNoSakti();

    public java.lang.String getcom_btpn_mpn_ws_GetNoSakti_PortAddress() {
        return com_btpn_mpn_ws_GetNoSakti_Port_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String com_btpn_mpn_ws_GetNoSakti_PortWSDDServiceName = "com_btpn_mpn_ws_GetNoSakti_Port";

    public java.lang.String getcom_btpn_mpn_ws_GetNoSakti_PortWSDDServiceName() {
        return com_btpn_mpn_ws_GetNoSakti_PortWSDDServiceName;
    }

    public void setcom_btpn_mpn_ws_GetNoSakti_PortWSDDServiceName(java.lang.String name) {
        com_btpn_mpn_ws_GetNoSakti_PortWSDDServiceName = name;
    }

    public com.mii.ws.getNoSakti.GetNoSakti_PortType getcom_btpn_mpn_ws_GetNoSakti_Port() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(com_btpn_mpn_ws_GetNoSakti_Port_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getcom_btpn_mpn_ws_GetNoSakti_Port(endpoint);
    }

    public com.mii.ws.getNoSakti.GetNoSakti_PortType getcom_btpn_mpn_ws_GetNoSakti_Port(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.mii.ws.getNoSakti.Com_btpn_mpn_ws_GetNoSakti_BinderStub _stub = new com.mii.ws.getNoSakti.Com_btpn_mpn_ws_GetNoSakti_BinderStub(portAddress, this);
            _stub.setPortName(getcom_btpn_mpn_ws_GetNoSakti_PortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setcom_btpn_mpn_ws_GetNoSakti_PortEndpointAddress(java.lang.String address) {
        com_btpn_mpn_ws_GetNoSakti_Port_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.mii.ws.getNoSakti.GetNoSakti_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.mii.ws.getNoSakti.Com_btpn_mpn_ws_GetNoSakti_BinderStub _stub = new com.mii.ws.getNoSakti.Com_btpn_mpn_ws_GetNoSakti_BinderStub(new java.net.URL(com_btpn_mpn_ws_GetNoSakti_Port_address), this);
                _stub.setPortName(getcom_btpn_mpn_ws_GetNoSakti_PortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("com_btpn_mpn_ws_GetNoSakti_Port".equals(inputPortName)) {
            return getcom_btpn_mpn_ws_GetNoSakti_Port();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://APPMDWDEV03.dev.corp.btpn.co.id/com.btpn.mpn.ws:GetNoSakti", "com.btpn.mpn.ws.GetNoSakti");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://APPMDWDEV03.dev.corp.btpn.co.id/com.btpn.mpn.ws:GetNoSakti", "com_btpn_mpn_ws_GetNoSakti_Port"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("com_btpn_mpn_ws_GetNoSakti_Port".equals(portName)) {
            setcom_btpn_mpn_ws_GetNoSakti_PortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
