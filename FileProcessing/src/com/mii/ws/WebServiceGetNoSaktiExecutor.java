package com.mii.ws;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import com.mii.ws.getNoSakti.ComBtpnMpnWsGetNoSaktiLocator;
import com.mii.ws.getNoSakti.InParameter;
import com.mii.ws.getNoSakti.OutParameter;

public class WebServiceGetNoSaktiExecutor {
	public OutParameter getNoSakti(InParameter input){
		ComBtpnMpnWsGetNoSaktiLocator locator = new ComBtpnMpnWsGetNoSaktiLocator();
		OutParameter output = new OutParameter();
		try {
			output = locator.getcom_btpn_mpn_ws_GetNoSakti_Port().getNoSakti(input);
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return output;
	}
}
