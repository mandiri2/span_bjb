package com.mii.ws;

import java.util.concurrent.ConcurrentHashMap;

import com.mii.constant.CommonConstant;
import com.mii.constant.ConstantNSCache;
import com.mii.constant.State;
import com.mii.helpers.EncryptUtils;
import com.mii.logger.Log;
import com.mii.logger.LoggerEvent;
import com.mii.util.WSUtils;

public class WebServiceConfigurator {
	
	private static Log log = Log.getInstance(WebServiceConfigurator.class);
	
	String addressGetNoSakti, usernameGetNoSakti, passwordGetNoSakti;
	
	ConcurrentHashMap<String, String> cacheParameter;
	
	public WebServiceConfigurator() {
		initiateProcess();
	}
	
	private void initiateProcess(){
		WSUtils wsUtils = new WSUtils();
		cacheParameter	= wsUtils.putParameterToCache(wsUtils.getAllParameter());
		if (cacheParameter.isEmpty()) {
			LoggerEvent.errorProcess(log, this.getClass().getName()+"/"+this.getClass().getMethods().toString(), "System Parameter"+State.NotFound);
			wsDefaultValue();
		} else {
			mapParameterName();
		}
	}
	
	private void mapParameterName(){
		addressGetNoSakti	= cacheParameter.get(ConstantNSCache.wsAddressGetNoSakti);
		usernameGetNoSakti	= cacheParameter.get(ConstantNSCache.wsUsernameGetNoSakti);
		//Add Decrypt 25 Jul 2016 -- Brian
		passwordGetNoSakti	= EncryptUtils.DecodeDecrypt(cacheParameter.get(ConstantNSCache.wsPasswordGetNoSakti));
		//--------------------------------
	}
	
	private void wsDefaultValue(){
		addressGetNoSakti	= CommonConstant.WSDefaultAddressGetNoSakti;
		usernameGetNoSakti	= CommonConstant.WSDefaultUsernameGetNoSakti;
		passwordGetNoSakti	= CommonConstant.WSDefaultPasswordGetNoSakti;
	}
	
	public String getAddressGetNoSakti() {
		return addressGetNoSakti;
	}

	public String getUsernameGetNoSakti() {
		return usernameGetNoSakti;
	}

	public String getPasswordGetNoSakti() {
		return passwordGetNoSakti;
	}
	
}
