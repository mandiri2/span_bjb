package com.mii.helpers;

import java.io.Serializable;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.mii.logger.Log;
import com.mii.logger.LoggerEvent;

@SuppressWarnings("rawtypes")
public class BasicDaoImplement extends HibernateDaoSupport implements InterfaceDAO{
	
	private static final Log log = Log.getInstance(BasicDaoImplement.class);
	
	public Session getCurrentSession(){
		Session session = null;
	     try{
	       session = getHibernateTemplate().getSessionFactory().getCurrentSession();
	     } catch (Exception e) {
	    	 e.printStackTrace();
	    	 LoggerEvent.errorProcess(log, this.getClass().getName()+"/"+this.getClass().getMethods().toString(), e.getMessage());
	     }
	     return session;
	  }
	
	public void save(Object obj) {
		try{			
			getHibernateTemplate().saveOrUpdate(obj);
			//getHibernateTemplate().save(obj);
		}catch (Exception e) {
			e.printStackTrace();
			LoggerEvent.errorProcess(log, this.getClass().getName()+"/"+this.getClass().getMethods().toString(), e.getMessage());
		}
	}

	@Override
	public Object get(Class clazz, Serializable id) {
		return getHibernateTemplate().get(clazz, id);
	}

	@Override
	public void delete(Class clazz, Serializable id) {
		try{
			Object object = get(clazz, id);
			getHibernateTemplate().delete(object);
		}catch (Exception e) {
			e.printStackTrace();
			LoggerEvent.errorProcess(log, this.getClass().getName()+"/"+this.getClass().getMethods().toString(), e.getMessage());
		}
		
	}

	@Override
	public void update(Object obj) {
		try{
			getHibernateTemplate().update(obj);
		}catch (Exception e) {
			e.printStackTrace();
			LoggerEvent.errorProcess(log, this.getClass().getName()+"/"+this.getClass().getMethods().toString(), e.getMessage());
		}
	}

	@Override
	public void delete(Object object) {
		try {
			getHibernateTemplate().delete(object);
		} catch (DataAccessException e) {
			e.printStackTrace();
			LoggerEvent.errorProcess(log, this.getClass().getName()+"/"+this.getClass().getMethods().toString(), e.getMessage());
		}
	}

	@Override
	public Query getQuery(String hql) {
		Query query = null;
		try {
			query = getCurrentSession().createQuery(hql);
			return query;
		} catch (Exception e) {
			e.printStackTrace();
			LoggerEvent.errorProcess(log, this.getClass().getName()+"/"+this.getClass().getMethods().toString(), e.getMessage());
		}
		return query;
		
	}

	@Override
	public void trueSave(Object object) {
		try{			
			getHibernateTemplate().save(object);
		}catch (Exception e) {
			e.printStackTrace();
			LoggerEvent.errorProcess(log, this.getClass().getName()+"/"+this.getClass().getMethods().toString(), e.getMessage());
		}
	}
}
