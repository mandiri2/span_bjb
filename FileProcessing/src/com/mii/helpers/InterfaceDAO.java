package com.mii.helpers;

import java.io.Serializable;

import org.hibernate.Query;

@SuppressWarnings("rawtypes")
public interface InterfaceDAO {
	
	public Object get(Class clazz, Serializable id);
	public void delete(Class clazz, Serializable id);
	public void update(Object obj);
	public void save(Object obj);
	public void delete(Object object);
	public Query getQuery(String hql);
	public void trueSave(Object object);
}
