package com.mii.test;

import com.mii.gateway.SchedulerInterface;

public class SchedulerTest {

	public static void main(String[] args){
		SchedulerInterface schedulerInterface = new SchedulerInterface();
		
		String schedulerID = schedulerInterface.createScheduller("Test","testest","OneTimeTask","2015/12/22","14:00:00",null,null,null,null,null,null,null,null,null,null,null,null);
		schedulerInterface.startScheduller(schedulerID);
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		schedulerInterface.stopScheduller(schedulerID);
		
	}
}
