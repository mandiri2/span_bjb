package com.mii.test;

import java.util.ArrayList;

import com.mii.constant.ConstantNSCache;
import com.mii.models.SystemParameter;

public class HardcodedData {

	public ArrayList<SystemParameter> dataSystemParameter(){

		ArrayList<SystemParameter> params = new ArrayList<SystemParameter>();
		
		params.add(setParams(ConstantNSCache.localPathBS, "/Users/admin/Desktop/FileProcessingPath/BS/Working/"));
		
		params.add(setParams(ConstantNSCache.requestPathBPP, "/Users/admin/Desktop/FileProcessingPath/BPP/Working/"));
		params.add(setParams(ConstantNSCache.fileNameBPPRequest, "BPP"));
		params.add(setParams(ConstantNSCache.backupRequestDirBPP, "/Users/admin/Desktop/FileProcessingPath/BPP/Backup/"));
		
		params.add(setParams(ConstantNSCache.workingPathBPSX, "/Users/admin/Desktop/FileProcessingPath/BPSX/Working/")); 
		params.add(setParams(ConstantNSCache.backupDirBPSX, "/Users/admin/Desktop/FileProcessingPath/BPSX/Backup/"));
		
		params.add(setParams(ConstantNSCache.destinationAccountNumber, "01234567890"));
		params.add(setParams(ConstantNSCache.destinationAccountName, "Bank Indonesia"));
		params.add(setParams(ConstantNSCache.bankCode, "bankCode123"));
		params.add(setParams(ConstantNSCache.description1, "description1"));
		params.add(setParams(ConstantNSCache.description2, "description2"));
		params.add(setParams(ConstantNSCache.description3, "description3"));
		params.add(setParams(ConstantNSCache.description4, "description4"));
		
		return params;
	}
	
	private SystemParameter setParams(String paramName, String paramValue){
		SystemParameter systemParameter = new SystemParameter();
		
		systemParameter.setParam_name(paramName);
		systemParameter.setParam_value(paramValue);
		
		return systemParameter;
	}
}
