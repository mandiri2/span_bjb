package com.mii.test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.mii.models.SystemParameter;

public class DataSystemParameter {
	
	public List<SystemParameter> getAllValueParameter(){
		List<SystemParameter> params = new ArrayList<SystemParameter>();
		
		params.add(setParams("PRM-20151230-1004-0594987","BS_LocalPath","/home/madhi/Depkeu/BS/BPSXWorking","PRM-20151230-1004-0594987",new Date(),new Date(),"cms2","Y"));
		params.add(setParams("PRM-20151230-1005-0364988","RTGS_BPP_Host","10.1.83.106","PRM-20151230-1005-0364988",new Date(),new Date(),"cms2","Y"));
		params.add(setParams("PRM-20151230-1006-0264989","RTGS_BPP_Username","RTGS_BPP_Username","PRM-20151230-1006-0264989",new Date(),new Date(),"cms2","Y"));
		params.add(setParams("PRM-20151230-1007-0024990","RTGS_BPP_Port","RTGS_BPP_Port","PRM-20151230-1007-0024990",new Date(),new Date(),"cms2","Y"));
		params.add(setParams("PRM-20151230-1007-0184991","RTGS_BPP_ProvateKeyPath","RTGS_BPP_ProvateKeyPath","PRM-20151230-1007-0184991",new Date(),new Date(),"cms2","Y"));
		params.add(setParams("PRM-20151230-1007-0434992","RTGS_BPP_WorkingPath","/home/madhi/Depkeu/BPP/Working/","PRM-20151230-1007-0434992",new Date(),new Date(),"cms2","Y"));
		params.add(setParams("PRM-20151230-1008-0254993","RTGS_BPP_RequestPath","RTGS_BPP_RequestPath","PRM-20151230-1008-0254993",new Date(),new Date(),"cms2","Y"));
		params.add(setParams("PRM-20151230-1008-0424994","RTGS_BPP_ResponsePath","RTGS_BPP_ResponsePath","PRM-20151230-1008-0424994",new Date(),new Date(),"cms2","Y"));
		params.add(setParams("PRM-20151230-1008-0544995","RTGS_BPP_FileNameResponse","BPOX","PRM-20151230-1008-0544995",new Date(),new Date(),"cms2","Y"));
		params.add(setParams("PRM-20151230-1009-0094996","RTGS_BPP_FileNameRequest","BPPX","PRM-20151230-1009-0094996",new Date(),new Date(),"cms2","Y"));
		params.add(setParams("PRM-20151230-1009-0404997","RTGS_BPP_BackupRequestDir","/home/madhi/Depkeu/BPP/Backup/","PRM-20151230-1009-0404997",new Date(),new Date(),"cms2","Y"));
		params.add(setParams("PRM-20151230-1010-0054998","RTGS_BPP_BackupResponseDir","/home/madhi/Depkeu/BPOX/Backup/","PRM-20151230-1010-0054998",new Date(),new Date(),"cms2","Y"));
		params.add(setParams("PRM-20151230-1010-0314999","RTGS_BPSX_Host","RTGS_BPSX_Host","PRM-20151230-1010-0314999",new Date(),new Date(),"cms2","Y"));
		params.add(setParams("PRM-20151230-1010-0455000","RTGS_BPSX_UserName","RTGS_BPSX_UserName","PRM-20151230-1010-0455000",new Date(),new Date(),"cms2","Y"));
		params.add(setParams("PRM-20151230-1010-0555001","RTGS_BPSX_Port","RTGS_BPSX_Port","PRM-20151230-1010-0555001",new Date(),new Date(),"cms2","Y"));
		params.add(setParams("PRM-20151230-1011-0045002","RTGS_BPSX_PrivateKeyPath","RTGS_BPSX_PrivateKeyPath","PRM-20151230-1011-0045002",new Date(),new Date(),"cms2","Y"));
		params.add(setParams("PRM-20151230-1011-0195003","RTGS_BPSX_RemotePath","RTGS_BPSX_RemotePath","PRM-20151230-1011-0195003",new Date(),new Date(),"cms2","Y"));
		params.add(setParams("PRM-20151230-1011-0315004","RTGS_BPSX_WorkingPath","/home/madhi/Depkeu/BPSX/Working/","PRM-20151230-1011-0315004",new Date(),new Date(),"cms2","Y"));
		params.add(setParams("PRM-20151230-1012-0155005","RTGS_BPSX_BackupDir","/home/madhi/Depkeu/BPSX/Backup/","PRM-20151230-1012-0155005",new Date(),new Date(),"cms2","Y"));
		params.add(setParams("PRM-20151230-1013-0145006","RTGS_TRX_DestAcctNo","RTGS_TRX_DestAcctNo","PRM-20151230-1013-0145006",new Date(),new Date(),"cms2","Y"));
		params.add(setParams("PRM-20151230-1013-0255007","RTGS_TRX_DestAcctName","RTGS_TRX_DestAcctName","PRM-20151230-1013-0255007",new Date(),new Date(),"cms2","Y"));
		params.add(setParams("PRM-20151230-1013-0365008","RTGS_TRX_BankCode","RTGS_TRX_BankCode","PRM-20151230-1013-0365008",new Date(),new Date(),"cms2","Y"));
		params.add(setParams("PRM-20151230-1013-0465009","RTGS_TRX_Desc1","RTGS_TRX_Desc1","PRM-20151230-1013-0465009",new Date(),new Date(),"cms2","Y"));
		params.add(setParams("PRM-20151230-1013-0525010","RTGS_TRX_Desc2","RTGS_TRX_Desc2","PRM-20151230-1013-0525010",new Date(),new Date(),"cms2","Y"));
		params.add(setParams("PRM-20151230-1014-0005011","RTGS_TRX_Desc3","RTGS_TRX_Desc3","PRM-20151230-1014-0005011",new Date(),new Date(),"cms2","Y"));
		params.add(setParams("PRM-20151230-1014-0085012","RTGS_TRX_Desc4","RTGS_TRX_Desc4","PRM-20151230-1014-0085012",new Date(),new Date(),"cms2","Y"));
		params.add(setParams("PRM-20151230-1014-0385013","RTGS_TRX_UserID","RTGS_TRX_UserID","PRM-20151230-1014-0385013",new Date(),new Date(),"cms2","Y"));
		params.add(setParams("PRM-20151230-1015-0195014","RTGS_TRX_Reserved1","RTGS_TRX_Reserved1","PRM-20151230-1015-0195014",new Date(),new Date(),"cms2","Y"));
		params.add(setParams("PRM-20151230-1015-0245015","RTGS_TRX_Reserved2","RTGS_TRX_Reserved2","PRM-20151230-1015-0245015",new Date(),new Date(),"cms2","Y"));
		params.add(setParams("PRM-20151230-1015-0315016","RTGS_TRX_Reserved3","RTGS_TRX_Reserved3","PRM-20151230-1015-0315016",new Date(),new Date(),"cms2","Y"));
		params.add(setParams("PRM-20151230-1015-0395017","RTGS_TRX_Reserved4","RTGS_TRX_Reserved4","PRM-20151230-1015-0395017",new Date(),new Date(),"cms2","Y"));
		params.add(setParams("PRM-20151230-1015-0525018","ReconFile_TRX_FileName","ReconFile_TRX_FileName","PRM-20151230-1015-0525018",new Date(),new Date(),"cms2","Y"));
		params.add(setParams("PRM-20151230-1016-0205019","WS_Addr_GetNoSakti","http://10.1.76.195:5555/ws/com.btpn.mpn.ws.GetNoSakti/com_btpn_mpn_ws_GetNoSakti_Port","PRM-20151230-1016-0205019",new Date(),new Date(),"cms2","Y"));
		params.add(setParams("PRM-20151230-1017-0315020","WS_UserName_GetNoSakti","Administrator","PRM-20151230-1017-0315020",new Date(),new Date(),"cms2","Y"));
		params.add(setParams("PRM-20151230-1017-0455021","WS_Password_GetNoSakti","manage","PRM-20151230-1017-0455021",new Date(),new Date(),"cms2","Y"));
		params.add(setParams("PRM-20151230-1021-0575022","PATH_FILE_REPORT_RESULT","/home/madhi/Depkeu/Report/Result/","PRM-20151230-1021-0575022",new Date(),new Date(),"cms1","Y"));
		params.add(setParams("PRM-20151230-0104-0405023","KODE_KANTOR_PUSAT","001","PRM-20151230-0104-0405023",new Date(),new Date(),"cms1","Y"));
		params.add(setParams("PRM-20151219-0324-0480005","KODE_BANK_PERSEPSI","014","param1",new Date(),new Date(),"cms1","0"));
		params.add(setParams("PRM-20151219-0324-0480007","INQUIRY_REQUEST_TYPE","301100","param2",new Date(),new Date(),"cms1","0"));
		params.add(setParams("PRM-20151219-0324-0480008","PAYMENT_REQUEST_TYPE","500100","param3",new Date(),new Date(),"cms1","0"));
		params.add(setParams("PRM-20151219-0324-0480006","REKENING_TITIPAN_TRANSAKSI","110203912893213","param4",new Date(),new Date(),"cms1","0"));
		params.add(setParams("PRM-20151219-0324-0480009","DEFAULT_ROLE_ID_UPLOAD_USER","RLO-20151219-0718-0460008","param4",new Date(),new Date(),"cms1","0"));
		params.add(setParams("PRM-20151219-0324-0480010","PAYMENT_CHANNEL_TYPE","7012","param4",new Date(),new Date(),"cms1","0"));
		params.add(setParams("PRM-20151219-0324-0480011","PAYMENT_ROLE_ID_SUPERVISOR","RLO-20151219-0718-0460010","param4",new Date(),new Date(),"cms1","0"));
		params.add(setParams("PRM-20151219-0324-0490012","WEB_SERVICE_INTERNAL_IP","10.1.83.106","param4",new Date(),new Date(),"cms1","0"));
		params.add(setParams("PRM-20151219-0324-0490013","WEB_SERVICE_INTERNAL_PORT","8583","param4",new Date(),new Date(),"cms1","0"));
		params.add(setParams("PRM-20151219-0324-0480012","WEB_SERVICE_IP","10.1.76.195","param4",new Date(),new Date(),"1","0"));
		params.add(setParams("PRM-20151219-0324-0480013","WEB_SERVICE_PORT","5555","param4",new Date(),new Date(),"cms1","0"));
		params.add(setParams("PRM-20151219-0324-0480014","WEB_SERVICE_USERNAME","Administrator","param4",new Date(),new Date(),"cms1","0"));
		params.add(setParams("PRM-20151219-0324-0480015","WEB_SERVICE_PASSWORD","manage","param4",new Date(),new Date(),"cms1","0"));
		params.add(setParams("PRM-20151219-0324-0480016","BANK_CODE","520007778687","bank code",new Date(),new Date(),"ADMIN","Y"));
		params.add(setParams("PRM-20151219-0324-0480017","JAR_BANK_STATEMENT_PATH","/home/madhi/Depkeu/BS/jar/","jar bank statement path",new Date(),new Date(),"ADMIN","Y"));
		params.add(setParams("PRM-20151219-0324-0480024","LIMIT_ROW_BANK_STATEMEMT","10000","limit row in bank statement file",new Date(),new Date(),"ADMIN","Y"));
		params.add(setParams("PRM-20151219-0324-0480025","RESOURCE_LOCAL_PATH_MT940","/home/madhi/Depkeu/BPSfile/","resource path local MT940",new Date(),new Date(),"ADMIN","Y"));
		params.add(setParams("PRM-20151219-0324-0480026","BACKUP_LOCAL_PATH_MT940","/home/madhi/Depkeu/BPSfile/backup/","backup path local MT940",new Date(),new Date(),"ADMIN","Y"));
		params.add(setParams("PRM-20151219-0324-0480027","PATTERN_FILE_MT940","BPS*","pattern file MT940",new Date(),new Date(),"ADMIN","Y"));
		params.add(setParams("PRM-20151219-0324-0480028","XML_BANK_STATEMENT_PATH","/home/madhi/Depkeu/BS/xml/","xml path bank statement",new Date(),new Date(),"ADMIN","Y"));
		params.add(setParams("PRM-20151219-0324-0480029","PRIVATE_KEY_PATH_FILE","C:\\private.ppk","private key path file",new Date(),new Date(),"ADMIN","Y"));
		params.add(setParams("PRM-20151219-0324-0480030","IS_PRIVATE_KEY","true","is private key",new Date(),new Date(),"ADMIN","Y"));
		params.add(setParams("PRM-20151219-0324-0480031","KEYSTORE_FILE_PATH","/home/madhi/Depkeu/key/mdrspan.jks","keystore file",new Date(),new Date(),"ADMIN","Y"));
		params.add(setParams("PRM-20151219-0324-0480032","PRIVATE_KEY_USER","mdrspan","private key user",new Date(),new Date(),"ADMIN","Y"));
		params.add(setParams("PRM-20151219-0324-0480033","PRIVATE_KEY_PASS","mandirispan","private key pass",new Date(),new Date(),"ADMIN","Y"));
		params.add(setParams("PRM-20151219-0324-0480034","KEYSTORE_PASS","mandirispan","keystore pass",new Date(),new Date(),"ADMIN","Y"));
		params.add(setParams("PRM-20151219-0324-0480060","BPS_TARGET_DIRECTORY","/home/madhi/TestBPSManual/","param5",new Date(),new Date(),"cms1","Y"));
		params.add(setParams("PRM-20151219-0324-0480061","SFTP_USER","madhi","param5",new Date(),new Date(),"cms1","Y"));
		params.add(setParams("PRM-20151219-0324-0480062","SFTP_HOST","10.1.83.106","param5",new Date(),new Date(),"cms1","Y"));
		params.add(setParams("PRM-20151219-0324-0480063","SFTP_PORT","10022","param5",new Date(),new Date(),"cms1","Y"));
		params.add(setParams("PRM-20151219-0324-0480064","SFTP_PASS","P@ssw0rd","param5",new Date(),new Date(),"cms1","Y"));
		params.add(setParams("PRM-20151219-0324-0480065","PAYMENT_REPORT_DIR","/home/madhi/Report/","param5",new Date(),new Date(),"cms1","Y"));
		params.add(setParams("PRM-20151219-0324-0480066","LDAP_ADDRESS_URL","ldap://10.1.76.9","param5",new Date(),new Date(),"cms1","Y"));
		params.add(setParams("PRM-20151219-0324-0480067","LDAP_SEARCH_BASE","OU=IT,OU=Kantor Pusat,DC=dev,DC=corp,DC=btpn,DC=co,DC=id","param5",new Date(),new Date(),"cms1","Y"));
		params.add(setParams("PRM-20151219-0324-0480068","LDAP_DOMAIN","CN=","param5",new Date(),new Date(),"cms1","Y"));
		
		return params;
	}

	private SystemParameter setParams(String param_id, String paramName, String paramValue, String description, Date insert_date, Date update_date, String change_who, String is_enabled){
		SystemParameter systemParameter = new SystemParameter();
		
		systemParameter.setParam_id(param_id);
		systemParameter.setParam_name(paramName);
		systemParameter.setParam_value(paramValue);
		systemParameter.setDescription(description);
		systemParameter.setInsert_date(insert_date);
		systemParameter.setUpdate_date(update_date);
		systemParameter.setChange_who(change_who);
		systemParameter.setIs_enabled(is_enabled);
		
		return systemParameter;
	}
}
