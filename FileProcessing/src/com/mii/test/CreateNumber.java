package com.mii.test;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.time.DateUtils;

/*import com.itextpdf.text.log.SysoCounter;
import com.mii.constant.CommonConstant;
import com.mii.util.CommonUtils;*/

public class CreateNumber {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*for(int i = 1; i <=1407; i++){
			System.out.println(CommonUtils.padleft(String.valueOf(i), 7, '0'));
		}*/
		
		/*tring a = "BPO20160217980812";
		System.out.println("a = "+a.substring(3));
		
		System.out.println("Transaction ID = "+CommonConstant.transactionID);*/
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
		Date date = new Date();
		Date nextTime = DateUtils.addMinutes(date, 5);
		
		System.out.println("Date now = "+dateFormat.format(date));
		System.out.println("Time now = "+timeFormat.format(date));
		System.out.println("next Time = "+timeFormat.format(nextTime));
		
		String billerCode = "8";
		String branchCode = "4721";
		Integer seq = 1012;
		BigInteger random = new BigDecimal(Math.round(Math.random()*1000L)).toBigInteger();
		String referenceNumber = billerCode.concat(branchCode.concat(String.format("%03d", random).concat(String.format("%04d", seq))));
		
		System.out.println("refrence no = "+referenceNumber);
	}

}
