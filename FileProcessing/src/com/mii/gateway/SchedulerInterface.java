package com.mii.gateway;

import com.mii.scheduler.SchedulerConfig;

public class SchedulerInterface {
	
	public String createScheduller(String jobName, String executionService, String schedulerType, String  date, String time, String interval, String startDate, String endDate, String startTime, String endTime, String maskYear, String maskMonth, String maskDay, String maskWeeklyDay, String maskHour, String maskMinute, String maskSecond){
		return new SchedulerConfig().createScheduler(jobName, executionService, schedulerType, date, time, interval, startDate, endDate, startTime, endTime, maskYear, maskMonth, maskDay, maskWeeklyDay, maskHour, maskMinute, maskSecond, null);
	}
	
	public void startScheduller(String schedulerID){
		new SchedulerConfig().startScheduler(schedulerID);
	}
	
	public void stopScheduller(String schedulerID){
		new SchedulerConfig().stopScheduler(schedulerID);
	}
	
	public String statusScheduler(String schedulerID){
		return new SchedulerConfig().statusScheduler(schedulerID);
	}
	
	public void pauseScheduller(String schedulerID){
		new SchedulerConfig().pauseScheduler(schedulerID);
	}
	
	public void resumeScheduller(String schedulerID){
		new SchedulerConfig().resumeScheduler(schedulerID);
	}
	
	public void summaryScheduller(String schedulerID){
		new SchedulerConfig().summaryScheduler(schedulerID);
	}
	
}
