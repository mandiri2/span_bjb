package com.mii.gateway;

import com.mii.rtgs.RTGSRequestHandler;

public class RTGSInterfaceV2 {
	
	public String execute(String executeDate){
		return run(executeDate);
	}

	private String run(String executeDate){
		return new RTGSRequestHandler().executeRTGS(executeDate);
	}
}
