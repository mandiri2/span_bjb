package com.mii.dao;

import com.mii.helpers.InterfaceDAO;

import mii.span.doc.MdrCfgMain;

public interface MdrCfgMainDao extends InterfaceDAO {
	MdrCfgMain getByKeyName(String keyName);
}
