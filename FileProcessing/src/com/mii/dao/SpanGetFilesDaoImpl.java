package com.mii.dao;

import org.hibernate.Query;

import com.mii.helpers.BasicDaoImplement;

public class SpanGetFilesDaoImpl extends BasicDaoImplement implements SpanGetFilesDao {

	@Override
	public void updateSpanGetFiles(String totalRetry, String filename) {
		String hql = "UPDATE SpanGetFiles SET totalRetry = :totalRetry WHERE filename = :filename";
		Query query = getCurrentSession().createQuery(hql);
		query.setLong("totalRetry", Long.valueOf(totalRetry));
		query.setString("filename", filename);
		query.executeUpdate();
	}

}
