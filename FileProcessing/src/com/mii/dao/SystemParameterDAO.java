package com.mii.dao;

import java.util.List;

import com.mii.helpers.InterfaceDAO;
import com.mii.models.SystemParameter;

public interface SystemParameterDAO extends InterfaceDAO {
	public List<SystemParameter> getValueParameter(String param_name);
	public List<SystemParameter> getAllValueParameter();
	public void updateParamValue(String paramValue, String paramName);
	public void updateParameter(String name, String value);
	public String getValueByParamName(String paramName);

	public SystemParameter getDetailedParameterByParamName(String paramName);
}
