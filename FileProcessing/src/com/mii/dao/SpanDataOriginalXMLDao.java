package com.mii.dao;

import mii.span.model.SpanDataOriginalXML;

import com.mii.helpers.InterfaceDAO;

public interface SpanDataOriginalXMLDao extends InterfaceDAO{
	void saveSpanDataOriginalXml(SpanDataOriginalXML spanDataOriginalXML);
}
