package com.mii.dao;

import java.util.ArrayList;
import java.util.List;

import com.mii.helpers.BasicDaoImplement;
import com.mii.logger.Log;
import com.mii.logger.LoggerEvent;
import com.mii.models.SchedulerM;

public class SchedulerDaoImpl extends BasicDaoImplement implements SchedulerDao{
	
	private static final Log log = Log.getInstance(SchedulerDaoImpl.class);

	@SuppressWarnings("unchecked")
	@Override
	public List<SchedulerM> getAllScheduler() {
		List<SchedulerM> schedulerMs = new ArrayList<SchedulerM>();
		schedulerMs = (List<SchedulerM>) getCurrentSession().getNamedQuery("scheduler#getAllScheduler").list();
		return schedulerMs;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SchedulerM> getScheduler(String schedulerID) {
		List<SchedulerM> schedulerMs = new ArrayList<SchedulerM>();
		schedulerMs = (List<SchedulerM>) getCurrentSession().getNamedQuery("scheduler#getSchedulerbyID").list();
		return schedulerMs;
	}

	@Override
	public int saveScheduler(SchedulerM schedulerM) {
		try {
			save(schedulerM);
			return 1;
		} catch (Exception e) {
			LoggerEvent.errorProcess(log, this.getClass().getName()+"/"+this.getClass().getMethods().toString(), e.getMessage());
			return 0;
		}
		
	}

	@Override
	public SchedulerM deleteScheduler(String schedulerID) {
		delete(SchedulerM.class, schedulerID);
		return null;
	}

	@Override
	public SchedulerM updateScheduler(SchedulerM schedulerM) {
		update(schedulerM);
		return null;
	}

}
