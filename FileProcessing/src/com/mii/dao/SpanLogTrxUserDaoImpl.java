package com.mii.dao;

import java.util.List;

import org.hibernate.transform.ResultTransformer;

import mii.span.process.model.InsertSPANLogTrxUserResponse;
import mii.span.util.BusinessUtil;

import com.mii.helpers.BasicDaoImplement;

public class SpanLogTrxUserDaoImpl extends BasicDaoImplement implements SpanLogTrxUserDao {

	@Override
	public InsertSPANLogTrxUserResponse insertSPANLogTrxUser(String iFileName,
			String iUserId) {
		@SuppressWarnings("unchecked")
		List<InsertSPANLogTrxUserResponse> list = (List<InsertSPANLogTrxUserResponse>) getCurrentSession().getNamedQuery("SpanLogTrxUser#insertSPANLogTrxUser")
				.setString("ifilename", iFileName)
				.setString("iuserid", iUserId)
				.setResultTransformer(new ResultTransformer() {
					
					private static final long serialVersionUID = 1L;
					
					@Override
					public Object transformTuple(Object[] row, String[] arg1){
						InsertSPANLogTrxUserResponse response = new InsertSPANLogTrxUserResponse(String.valueOf(row[0]) , String.valueOf(row[1]));
						return response;
					}
					
					@SuppressWarnings("rawtypes")
					@Override
					public List transformList(List arg0) {
						return arg0;
					}
				}).list();
		
		if(!BusinessUtil.isListEmpty(list)){
			return list.get(0);
		}
		
		return null;
	}
	 
}
