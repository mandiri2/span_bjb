package com.mii.dao;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Restrictions;

import mii.span.model.SpanDataValidationSP2DNo;
import mii.span.util.BusinessUtil;

import com.mii.helpers.BasicDaoImplement;

public class SpanDataValidationSP2DNoDaoImpl extends BasicDaoImplement implements SpanDataValidationSP2DNoDao{

	@Override
	public List<SpanDataValidationSP2DNo> getByFilename(String iFilename) {
		return (List<SpanDataValidationSP2DNo>) getCurrentSession()
				.createCriteria(SpanDataValidationSP2DNo.class)
				.add(Restrictions.eq("FILE_NAME", iFilename))
				.list();
	}

	@Override
	public void insertBatch(List<SpanDataValidationSP2DNo> list) {
		if(!BusinessUtil.isListEmpty(list)){
			for(SpanDataValidationSP2DNo spanDataValidationSP2DNo : list){
				setDefaultValue(spanDataValidationSP2DNo);
				save(spanDataValidationSP2DNo);
			}
		}
		
	}
	
	/**
	 * set default value karena default value di postgre tidak jalan
	 * @param spanDataValidationSP2DNo
	 */
	private void setDefaultValue(
			SpanDataValidationSP2DNo spanDataValidationSP2DNo) {
		
		if(StringUtils.isEmpty(spanDataValidationSP2DNo.getPROC_STATUS_FILE())){
			spanDataValidationSP2DNo.setPROC_STATUS_FILE("1");
		}
		
		if(StringUtils.isEmpty(spanDataValidationSP2DNo.getPROC_STATUS_SP2DNO())){
			spanDataValidationSP2DNo.setPROC_STATUS_SP2DNO("1");
		}
		
		if(StringUtils.isEmpty(spanDataValidationSP2DNo.getVOID_FLAG())){
			spanDataValidationSP2DNo.setVOID_FLAG("0");;
		}
		
		if(spanDataValidationSP2DNo.getCREATE_DATE()==null){
			spanDataValidationSP2DNo.setCREATE_DATE(new Date());
		}
		
//		if(StringUtils.isEmpty(spanDataValidationSP2DNo.get)){
//			spanDataValidationSP2DNo.set;
//		}

	}

}
