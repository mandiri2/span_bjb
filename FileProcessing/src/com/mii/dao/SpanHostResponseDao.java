package com.mii.dao;

import java.util.List;

import mii.span.model.SpanHostResponse;

import com.mii.helpers.InterfaceDAO;

public interface SpanHostResponseDao extends InterfaceDAO {
	void saveSpanHostResponse(SpanHostResponse spanHostResponse);
	public List<SpanHostResponse> getTransaksiRekeningSama(String accountNo);
	public SpanHostResponse getResponseByHostDataDetails(String batchId, String trxHeaderId, String trxDetailId);
}
