package com.mii.dao;

import java.util.List;

import mii.span.model.Provider;

import com.mii.helpers.InterfaceDAO;

public interface ProviderDao extends InterfaceDAO {
	List<Provider> getByProviderCodeAlias(String providerCodeAlias);
	List<Provider> getByProviderAccount(String providerAccount);
}
