package com.mii.dao;

import java.util.List;

import org.hibernate.transform.ResultTransformer;

import mii.span.model.SpanHostResponse;
import mii.span.model.SpanRekeningKoran;
import mii.span.model.ToBeExecuteOutput;

import com.mii.helpers.BasicDaoImplement;

public class SpanHostResponseDaoImpl extends BasicDaoImplement implements SpanHostResponseDao {

	@Override
	public void saveSpanHostResponse(SpanHostResponse spanHostResponse) {
		setDefaultValueSpanHostResponse(spanHostResponse);
		save(spanHostResponse);
	}

	private void setDefaultValueSpanHostResponse(
			SpanHostResponse spanHostResponse) {
		// TODO Auto-generated method stub
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SpanHostResponse> getTransaksiRekeningSama(String accountNo) {
		return getCurrentSession().getNamedQuery("SpanHostResponse#getTransaksiRekeningSama").
				setString("accountNo", accountNo).
				setResultTransformer(new ResultTransformer() {
					private static final long serialVersionUID = 1L;
					@Override
					public Object transformTuple(Object[] row, String[] arg1) {
						SpanHostResponse spanHostResponse;
						spanHostResponse = new SpanHostResponse(
								String.valueOf(row[0]), 
								String.valueOf(row[1]),
								String.valueOf(row[2]));
						return spanHostResponse;
					}
					@SuppressWarnings("rawtypes")
					@Override
					public List transformList(List arg0) {
						return arg0;
					}}).list();
	}

	@Override
	public SpanHostResponse getResponseByHostDataDetails(String batchId,
			String trxHeaderId, String trxDetailId) {
		SpanHostResponse result = (SpanHostResponse) getCurrentSession().getNamedQuery("SpanHostResponse#getResponseByHostDataDetails")
				.setString("batchId", batchId)
				.setString("trxHeaderId", trxHeaderId)
				.setString("trxDetailId", trxDetailId)
				.uniqueResult();
		return result;
	}
	
}
