package com.mii.dao;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.transform.ResultTransformer;

import mii.span.model.SelectFNDataValidationOutput;
import mii.span.model.SpanDataToBeExecute;
import mii.span.model.ToBeExecuteOutput;

import com.mii.helpers.BasicDaoImplement;

public class ToBeExecuteDaoImpl extends BasicDaoImplement implements ToBeExecuteDao{

	@SuppressWarnings("unchecked")
	@Override
	public List<SpanDataToBeExecute> selectAllDataToBeExecuted(String accountNo) {
		List<SpanDataToBeExecute> spanDataToBeExecuteList = new ArrayList<SpanDataToBeExecute>();
		try {
			spanDataToBeExecuteList = (List<SpanDataToBeExecute>)getCurrentSession().
					getNamedQuery("ToBeExecuted#selectAllDataToBeExecuted")
					.setString("accountNo", accountNo).
					setResultTransformer(new ResultTransformer() {						
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							SpanDataToBeExecute spanDataToBeExecute;
							try {
								spanDataToBeExecute = new SpanDataToBeExecute(
										String.valueOf(row[0]), 
										String.valueOf(row[1]), 
										String.valueOf(row[2]),
										String.valueOf(row[3]));
								return spanDataToBeExecute;
							} catch (Exception e) {
								e.printStackTrace();
							}
							return null;
						}
						
						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return spanDataToBeExecuteList;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public SelectFNDataValidationOutput selectFNDataValidation(String fileName) {
		List<SelectFNDataValidationOutput> output = new ArrayList<SelectFNDataValidationOutput>();
		SelectFNDataValidationOutput selectFNDataValidationOutput = new SelectFNDataValidationOutput();
		output = getCurrentSession().getNamedQuery("ToBeExecuted#selectFNDataValidation")
				.setString("iFilename", "%"+fileName+"%").
				setResultTransformer(new ResultTransformer() {
					private static final long serialVersionUID = 1L;
					@Override
					public Object transformTuple(Object[] row, String[] arg1) {
						SelectFNDataValidationOutput selectFNDataValidationOutput;
						selectFNDataValidationOutput = new SelectFNDataValidationOutput(
								String.valueOf(row[0]), 
								String.valueOf(row[1]),
								String.valueOf(row[2]),
								String.valueOf(row[3]),
								String.valueOf(row[4]));
						return selectFNDataValidationOutput;
					}
					@SuppressWarnings("rawtypes")
					@Override
					public List transformList(List arg0) {
						return arg0;
					}}).list();
			
			for(SelectFNDataValidationOutput out : output){
				selectFNDataValidationOutput = out;
			}
		return selectFNDataValidationOutput;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ToBeExecuteOutput spanExecByFilename(String fileName, String scFlag,
			String glAccount, String asynchronous, String payrolltype,
			String retryNo, String settlement, String scDate) {
		List<ToBeExecuteOutput> output = new ArrayList<ToBeExecuteOutput>();
		ToBeExecuteOutput toBeExecuteOutput = new ToBeExecuteOutput();
		output = getCurrentSession().getNamedQuery("ToBeExecuted#spanExecByFilename")
				.setString("ifilename", fileName)
				.setString("iscflag", scFlag)
				.setString("iglaccount", glAccount)
				.setString("iasync", asynchronous)
				.setString("ipayroltype", payrolltype)
				.setString("iretryno", retryNo)
				.setString("isettlement", settlement)
				.setString("iscdate", scDate).
				setResultTransformer(new ResultTransformer() {
					private static final long serialVersionUID = 1L;
					@Override
					public Object transformTuple(Object[] row, String[] arg1) {
						ToBeExecuteOutput toBeExecuteOutput;
						toBeExecuteOutput = new ToBeExecuteOutput(
								String.valueOf(row[0]), 
								String.valueOf(row[1]));
						return toBeExecuteOutput;
					}
					@SuppressWarnings("rawtypes")
					@Override
					public List transformList(List arg0) {
						return arg0;
					}}).list();
			
			for(ToBeExecuteOutput out : output){
				toBeExecuteOutput = out;
			}
		return toBeExecuteOutput;
	}

	@Override
	public int countFirstAvailableBalance(String accountType) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		BigInteger count = (BigInteger) getCurrentSession().getNamedQuery("ToBeExecuted#countFirstAvailableBalance")
				.setString("inquiryDate", sdf.format(new Date()))
				.setString("accountType", accountType)
				.uniqueResult();
		return count.intValue();
	}

}
