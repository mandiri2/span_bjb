package com.mii.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.mii.helpers.BasicDaoImplement;
import com.mii.logger.Log;
import com.mii.logger.LoggerEvent;
import com.mii.models.RTGSM;
import com.mii.models.TransactionM;

public class RTGSDAOImpl extends BasicDaoImplement implements RTGSDAO{

	private static Log log = Log.getInstance(RTGSDAOImpl.class);
	
	@Override
	public int saveRTGS(RTGSM rtgsm) {
		try {
			save(rtgsm);
			return 1;
		} catch (Exception e) {
			LoggerEvent.errorProcess(log, this.getClass().getName()+"/"+this.getClass().getMethods().toString(), e.getMessage());
			return 0;
		}
	}

	@Override
	public RTGSM updateRTGS(RTGSM rtgsm) {
		update(rtgsm);
		return null;
	}
	
	@Override
	public void updateRTGSResponse(RTGSM rtgsm){
		String state = rtgsm.getState();
		String transactionFlag = rtgsm.getTransactionFlag();
		String transactionID = rtgsm.getTransactionID();
		Date updateDate = rtgsm.getUpdateDate();
		String userID = rtgsm.getUserID();
		String response = rtgsm.getResponse();
		String responseDesc = rtgsm.getResponseDesc();
		String referenceNumber = rtgsm.getReferenceNumber();
		String bpoxName = rtgsm.getBpoxName();
	
		getCurrentSession().getNamedQuery("RTGSM#updateRTGSResponseData").setString("state", state).setString("transactionFlag", transactionFlag).setString("transactionID", transactionID).
		setDate("updateDate", updateDate).setString("userID", userID).setString("response", response).setString("responseDesc", responseDesc).setString("referenceNumber", referenceNumber).
		setString("bpoxName", bpoxName).executeUpdate();
	}
	
	@Override
	public void updateRTGSNoSakti(RTGSM rtgsm){
		String state = rtgsm.getState();
		Date updateDate = rtgsm.getUpdateDate();
		String transactionFlag = rtgsm.getTransactionFlag();
		String reserved3 = rtgsm.getReserved3();
		String referenceNumber = rtgsm.getReferenceNumber();
		String noSakti = rtgsm.getNoSakti();
		
		getCurrentSession().getNamedQuery("RTGSM#updateRTGSNoSakti").setString("state", state).setDate("updateDate", updateDate).
		setString("transactionFlag", transactionFlag).setString("reserved3", reserved3).setString("referenceNumber", referenceNumber).setString("noSakti", noSakti).executeUpdate();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<RTGSM> getBPSXFile(String referenceNo) {
		List<RTGSM> listBPSXFile = new ArrayList<RTGSM>();
		try {
			listBPSXFile = (List<RTGSM>) getCurrentSession().getNamedQuery("RTGSM#getBPSXFile").setString("referenceNo", referenceNo).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listBPSXFile;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<RTGSM> getRTGSbyState(String state) {
		List<RTGSM> listRTGSState = new ArrayList<RTGSM>();
		try {
			listRTGSState = (List<RTGSM>) getCurrentSession().getNamedQuery("RTGSM#getRTGSState").setString("state", state).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listRTGSState;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<RTGSM> getRTGSbyFlag(String transactionFlag) {
		List<RTGSM> listRTGSState = new ArrayList<RTGSM>();
		try {
			listRTGSState = (List<RTGSM>) getCurrentSession().getNamedQuery("RTGSM#getRTGSFlag").setString("transactionFlag", transactionFlag).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listRTGSState;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TransactionM> getRTGSbyFlag10(String transactionFlag) {
		List<TransactionM> listRTGSState = new ArrayList<TransactionM>();
		try {
			listRTGSState = (List<TransactionM>) getCurrentSession().getNamedQuery("RTGSM#getRTGSFlag10").setString("transactionFlag", transactionFlag).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listRTGSState;
	}
	
	@Override
	public void updateRTGSFlag(String reffNo){
		try{
			getCurrentSession().getNamedQuery("RTGSM#updateRTGSFlag").setString("reffNo", reffNo).executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TransactionM> checkRTGSData(String BPSFilename) {
		List<TransactionM> listRTGSState = new ArrayList<TransactionM>();
		try {
			listRTGSState = (List<TransactionM>) getCurrentSession().getNamedQuery("RTGSM#checkRTGSData").setString("bpsFilename", BPSFilename+"%").list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listRTGSState;
	}

}
