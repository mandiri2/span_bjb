package com.mii.dao;

import java.util.ArrayList;
import java.util.List;

import mii.span.model.SpanVoidDataTrx;

import org.hibernate.Query;

import com.mii.helpers.BasicDaoImplement;
import com.mii.span.model.SpanSp2dVoidLog;

public class SpanVoidDataTrxDaoImpl extends BasicDaoImplement implements SpanVoidDataTrxDao {

	@Override
	public void InsertSPANDataVoidTransaction(SpanVoidDataTrx spanVoidDataTrx) {
		getCurrentSession()
				.getNamedQuery("SpanVoidDataTrx#InsertSPANDataVoidTransaction")
				.setString("fileName", spanVoidDataTrx.getFileName())
				.setString("documentNo", spanVoidDataTrx.getDocumentNo())
				.setString("documentDate", spanVoidDataTrx.getDocumentDate())
				.setString("beneficiaryName",
						spanVoidDataTrx.getBeneficiaryName())
				.setString("beneficiaryBankCode",
						spanVoidDataTrx.getBeneficiaryBankCode())
				.setString("beneficiaryBank",
						spanVoidDataTrx.getBeneficiaryBank())
				.setString("beneficiaryAccount",
						spanVoidDataTrx.getBeneficiaryAccount())
				.setString("amount", spanVoidDataTrx.getAmount())
				.setString("description", spanVoidDataTrx.getDescription())
				.setString("agentBankCode", spanVoidDataTrx.getAgentBankCode())
				.setString("agentBankAccountNo",
						spanVoidDataTrx.getAgentBankAccountNo())
				.setString("agentBankAccountName",
						spanVoidDataTrx.getAgentBankAccountName())
				.setString("paymentMethod", spanVoidDataTrx.getPaymentMethod())
				.setString("sp2dCount", spanVoidDataTrx.getSp2dCount())
				.executeUpdate();
	}
	
	@Override
	public List<SpanVoidDataTrx> getByFileNameAndStatus(String fileName,
			String status) {
		
		String hql = "FROM SpanVoidDataTrx s WHERE s.fileName = :fileName AND s.status = :status ";
		Query query = getCurrentSession().createQuery(hql);
		query.setString("fileName", fileName);
		query.setString("status", status);
		return (List<SpanVoidDataTrx>) query.list();
	}

	@Override
	public List<SpanSp2dVoidLog> checkSpanSp2dVoidLogsBySp2dNo(String sp2dNo) {
		// TODO Auto-generated method stub
		List<SpanSp2dVoidLog> result = new ArrayList<>();
		
		result = getCurrentSession()
		.getNamedQuery("SPANMonitoringVoidDepKeu#checkSpanSp2dVoidLogsBySp2dNo")
		.setString("sp2dNo", sp2dNo).list();
		
		return result;
	}	
	@Override
	public List<SpanSp2dVoidLog> getAllSpanSp2dVoidLogs() {
		// TODO Auto-generated method stub
		List<SpanSp2dVoidLog> result = new ArrayList<>();
		
		result = getCurrentSession()
		.getNamedQuery("SPANMonitoringVoidDepKeu#getAllSpanSp2dVoidLogs").list();
		
		return result;
	}	
}
