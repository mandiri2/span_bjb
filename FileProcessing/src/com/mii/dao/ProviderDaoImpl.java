package com.mii.dao;

import java.util.List;

import org.hibernate.Query;

import mii.span.model.Provider;

import com.mii.helpers.BasicDaoImplement;

public class ProviderDaoImpl extends BasicDaoImplement implements ProviderDao {

	@Override
	public List<Provider> getByProviderCodeAlias(String providerCodeAlias) {
		String hql = "FROM Provider p WHERE p.providerCodeAlias = :providerCodeAlias";
		Query query = getCurrentSession().createQuery(hql);
		query.setString("providerCodeAlias", providerCodeAlias);
		return (List<Provider>)query.list();
	}

	@Override
	public List<Provider> getByProviderAccount(String providerAccount) {
		String hql = "FROM Provider p WHERE p.providerAccount = :providerAccount";
		Query query = getCurrentSession().createQuery(hql);
		query.setString("providerAccount", providerAccount);
		return (List<Provider>)query.list();
	}

}
