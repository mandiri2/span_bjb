package com.mii.dao;

import java.math.BigInteger;
import java.util.List;

import mii.span.doc.DataFileNACK;
import mii.span.model.SpanDataValidation;

import org.hibernate.Query;

import com.mii.helpers.InterfaceDAO;

public interface SpanDataValidationDao extends InterfaceDAO {
	List<SpanDataValidation> getByFileName(String fileName);
	List<SpanDataValidation> getByEqualFileName(String fileName);
	BigInteger getBatchIDSeq();
	BigInteger spanSequenceNo();
	SpanDataValidation getByCompositeId(String fileName, String batchId, String trxHeaderId);
	List<SpanDataValidation> selectSPANDataValidationStatus(String procStatus1, String procStatus2);
	List<SpanDataValidation> selectSPANDataExecuted(String procStatus);
	List<SpanDataValidation> likeByFileName(String fileName);
	List<SpanDataValidation> selectExtractFileSPAN(String procStatus, String rowNum);
	void udpateChangeStatus(String batchID, String statusChange);
	List<SpanDataValidation> selectSPANDataValidationStatusByTrxType(String procStatus, String trxType1, String trxType2);
	void updateTotalRecAmtSendToHost(String totalSendToHost, String totalAmountSendToHost, String fileName);
	void saveSpanDataValidation(SpanDataValidation spanDataValidation);
	int selectCountSPANFileRejected(String fileName);
	boolean isStillProcessing(String docDate);
	boolean isSp2dExist(String docDate);
	DataFileNACK selectSPANDataValidationNACK(String procStatus,
			String documentDate, String responseCode, String providerCode,
			String filename);
	
	
	//ExtractSP2D
	public int countOngoingTransaction();
	//GetNACKUpdate
	public List<SpanDataValidation> getNACKUpdateList(String accountType);
	
}
