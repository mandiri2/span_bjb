package com.mii.dao;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import mii.span.iso.model.ReinquiryMessage;
import mii.span.model.BulkFTReqPRC;

import org.hibernate.HibernateException;
import org.hibernate.transform.ResultTransformer;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;

import com.mii.constant.Constants;
import com.mii.helpers.BasicDaoImplement;

public class BulkFTDaoImpl extends BasicDaoImplement implements BulkFTDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<BulkFTReqPRC> getAll() {
		List<BulkFTReqPRC> prc = new ArrayList<>();

		try {
			prc = (List<BulkFTReqPRC>) getCurrentSession()
					.getNamedQuery("bulkFT#getAll")
					.setResultTransformer(new ResultTransformer() {
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] arg0, String[] row) {
							BulkFTReqPRC prc = new BulkFTReqPRC(String
									.valueOf(row[0]), String.valueOf(row[1]),
									String.valueOf(row[2]));

							return prc;
						}

						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).list();
		} catch (Exception e) {
			e.printStackTrace();
		}

		for (BulkFTReqPRC p : prc) {
			System.out.println(p);
		}

		return prc;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BulkFTReqPRC> getAllUnprocess(String intLimit) {
		List<BulkFTReqPRC> prc = new ArrayList<>();

		try {
			prc = (List<BulkFTReqPRC>) getCurrentSession()
					.getNamedQuery("bulkFT#getAllUnprocess")
					.setBigInteger("limit", new BigInteger(intLimit))
					.setResultTransformer(new ResultTransformer() {
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							BulkFTReqPRC prc = new BulkFTReqPRC(
									String.valueOf(row[0]), 
									String.valueOf(row[1]),
									String.valueOf(row[2]), 
									String.valueOf(row[3]), 
									String.valueOf(row[4]), 
									String.valueOf(row[5]), 
									String.valueOf(row[6]), 
									String.valueOf(row[7]), 
									String.valueOf(row[8]), 
									String.valueOf(row[9]), 
									String.valueOf(row[10]), 
									String.valueOf(row[11]), 
									String.valueOf(row[12]), 
									String.valueOf(row[13]), 
									String.valueOf(row[14]), 
									String.valueOf(row[15]), 
									String.valueOf(row[16]), 
									String.valueOf(row[17]), 
									String.valueOf(row[18]), 
									String.valueOf(row[19]), 
									String.valueOf(row[20]), 
									String.valueOf(row[21]), 
									String.valueOf(row[22]), 
									String.valueOf(row[23]), 
									String.valueOf(row[24]), 
									String.valueOf(row[25]), 
									String.valueOf(row[26]), 
									String.valueOf(row[27]), 
									String.valueOf(row[28]), 
									String.valueOf(row[29]), 
									String.valueOf(row[30]),
									String.valueOf(row[31]),
									String.valueOf(row[32]),
									String.valueOf(row[33]),
									String.valueOf(row[34]),
									String.valueOf(row[35]));
							return prc;
						}

						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).list();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return prc;
	}

	@Override
	public void updatePrcStatus(BulkFTReqPRC prc) {
		getCurrentSession().getNamedQuery("bulkFT#updatePrcStatus")
				.setString("batchid", prc.getBatchid())
				.setString("trxheaderid", prc.getTrxheaderid())
				.setString("trxdetailid", prc.getTrxdetailid()).executeUpdate();
	}
	
	@Override
	public void updatePrcStatusBatch(List<String> trxDetailIDList) {
		try {
			getCurrentSession().getNamedQuery("bulkFT#updatePrcStatusBatch")
			.setParameterList("trxdetailid", trxDetailIDList)
			.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void insertRSP(BulkFTReqPRC prc, ISOMsg response) {
		String errorCode = null;
		String errorMessage = null;
		if(response.getString(39).equals(Constants.ISO_IF400_SUCCESS_CODE)){
			errorCode = "0";
		}else{
			errorCode = response.getString(39);
			errorMessage = (String) getCurrentSession()
					.getNamedQuery("bulkFT#selectRC")
					.setString("rc", errorCode)
					.uniqueResult();
		}
		String debitrefno = null;
		if(!prc.getDebitrefno().equals("")){
			debitrefno = prc.getDebitrefno();
		}
		String creditrefno = null;
		if(!prc.getCreditrefno().equals("")){
			creditrefno = prc.getCreditrefno();
		}
		String organizationdirname = null;
		if(!prc.getOrganizationdirname().equals("")){
			organizationdirname = prc.getOrganizationdirname();
		}
		String swiftmethod = null;
		if(!prc.getSwiftmethod().equals("")){
			swiftmethod = prc.getSwiftmethod();
		}
		String tracenumberori = response.getString(7)+response.getString(11);
		try{
			if (response.getString(3).equals("480000")){
				tracenumberori = response.getString(7)+response.getString(47);
				logger.info("[INFO] Trace number ori used for reinquiry response ... "+tracenumberori);
			}
		}catch(Exception e){
			System.out.println("Force insert RSP . . . ");
		}
		getCurrentSession().getNamedQuery("bulkFT#insertRSP")
		.setString("batchid", prc.getBatchid())
		.setString("trxheaderid", prc.getTrxheaderid())
		.setString("trxdetailid", prc.getTrxdetailid())
		.setString("debitaccno", prc.getDebitaccno())
		.setString("debitacccurrcode", prc.getDebitacccurrcode())
		.setString("debitamount", prc.getDebitamount())
		.setString("chargeinstruction", prc.getChargeinstruction())
		.setString("creditaccno", prc.getCreditaccno())
		.setString("creditaccname", prc.getCreditaccname())
		.setString("credittrfcurr", prc.getCredittrfcurr())
		.setString("credittrfamount", prc.getCredittrfamount())
		.setString("trxremark1", prc.getTrxremark1())
		.setString("trxremark2", prc.getTrxremark2())
		.setString("trxremark3", prc.getTrxremark3())
		.setString("trxremark4", prc.getTrxremark4())
		.setString("ftservices", prc.getFtservices())
		.setString("beneficiarybankcode", prc.getBeneficiarybankcode())
		.setString("beneficiarybankname", prc.getBeneficiarybankname())
		.setString("debitrefno", debitrefno)
		.setString("creditrefno", creditrefno)
		.setString("organizationdirname", organizationdirname)
		.setString("swiftmethod", swiftmethod)
		.setString("responsecode", response.getString(39))
		.setString("errorcode", errorCode)
		.setString("errormessage", errorMessage)
		.setString("remittanceno", null)
		.setString("postingtimestamp", response.getString(7))
		.setString("tellerid", null)
		.setString("journalseqno", null)
		.setString("reserve1", null)
		.setString("reserve2", null)
		.setString("reserve3", null)
		.setString("reserve4", null)
		.setString("reserve5", null)
		.setString("instructioncode1", prc.getInstructioncode())
		.setString("legstatus", prc.getLegstatus())
		.setString("tracenumber", response.getString(7)+response.getString(11))
		.setString("tracenumberori", tracenumberori)
		.setString("docNumber", prc.getDocumentNumber())
		.executeUpdate();	
	}
	
	@Override
	public void updateRSP(BulkFTReqPRC prc, ISOMsg msg) {
		try {
			getCurrentSession().getNamedQuery("bulkFT#updateRSP")
			.setString("rc", msg.getString(39))
			.setString("tracenumber", msg.getString(7)+msg.getString(11))
			.setString("batchid", prc.getBatchid())
			.setString("trxheaderid", prc.getTrxheaderid())
			.setString("trxdetailid", prc.getTrxdetailid())
			.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public BigInteger getNextStan() {
		BigInteger STAN = (BigInteger) getCurrentSession()
				.getNamedQuery("bulkFT#getNextStan")
				.uniqueResult();
		return STAN;
	}
	
	@Override
	public void insertReqLog(ISOMsg msg) {
		try {
			getCurrentSession().getNamedQuery("bulkFT#insertReqLog")
			.setString("stan", msg.getString(11))
			.setString("raw_request", new String(msg.pack()))
			.setString("raw_response", null)
			.setString("transmission", msg.getString(7))
			.executeUpdate();
			System.out.println("log response inserted cme_iso_log ...");
		} catch (HibernateException | ISOException e) {
			e.printStackTrace();
		}
		
	}
	
	@Override
	public void insertReqLog(ISOMsg msg, String action) {
		try {
			getCurrentSession().getNamedQuery("bulkFT#insertReqLogAction")
			.setString("stan", msg.getString(11))
			.setString("raw_request", new String(msg.pack()))
			.setString("raw_response", null)
			.setString("transmission", msg.getString(7))
			.setString("action", action)
			.executeUpdate();
			System.out.println("log response inserted cme_iso_log with action...");
		} catch (HibernateException | ISOException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void updateResLog(ISOMsg msg) {
		try {
			getCurrentSession().getNamedQuery("bulkFT#insertResLog")
			.setString("response", new String(msg.pack()))
			.setString("stan", msg.getString(11))
			.setString("transmission", msg.getString(7))
			.executeUpdate();
			logger.debug("Success update response cme_iso_log ..." + msg.getString(7) + msg.getString(11));
		} catch (HibernateException | ISOException e) {
			logger.debug("Failed update response cme_iso_log ...");
			e.printStackTrace();
		}
	}
	
	@Override
	public void updateResLogReinquiry(ISOMsg msg) {
		try {
			getCurrentSession().getNamedQuery("bulkFT#insertResLog")
			.setString("response", new String(msg.pack()))
			.setString("stan", msg.getString(47))
			.setString("transmission", msg.getString(7))
			.executeUpdate();
			logger.debug("Success update response reinquiry cme_iso_log 747..." + msg.getString(7) + msg.getString(47));
		} catch (HibernateException | ISOException e) {
			logger.debug("Failed update response cme_iso_log ...");
			e.printStackTrace();
		}
	}

	@Override
	public BigInteger checkResponseExist(BulkFTReqPRC prc) {
		BigInteger count = new BigInteger("0");
		try {
			count = (BigInteger) getCurrentSession().getNamedQuery("bulkFT#checkResponseExist")
			.setString("batchid", prc.getBatchid())
			.setString("trxheaderid", prc.getTrxheaderid())
			.setString("trxdetailid", prc.getTrxdetailid())
			.uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return count;
	}

	@Override
	public void updateRetryCount(BulkFTReqPRC prc) {
		try {
			getCurrentSession().getNamedQuery("bulkFT#updateRetryCountPRC")
			.setString("count", prc.getRetryCount())
			.setString("batchid", prc.getBatchid())
			.setString("trxheaderid", prc.getTrxheaderid())
			.setString("trxdetailid", prc.getTrxdetailid())
			.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BulkFTReqPRC> getAllUnprocessRetryOrPosting(String intLimit, String type) {
		List<BulkFTReqPRC> prc = new ArrayList<>();
		String namedQuery = "";
		
		if (type.equalsIgnoreCase("posting")) {
			namedQuery = "bulkFT#getAllUnprocessPosting";
		}
		else if (type.equalsIgnoreCase("retry")){
			namedQuery = "bulkFT#getAllUnprocessRetry";
		}

		try {
			prc = (List<BulkFTReqPRC>) getCurrentSession()
					.getNamedQuery(namedQuery)
					.setBigInteger("limit", new BigInteger(intLimit))
					.setResultTransformer(new ResultTransformer() {
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							BulkFTReqPRC prc = new BulkFTReqPRC(
									String.valueOf(row[0]), 
									String.valueOf(row[1]),
									String.valueOf(row[2]), 
									String.valueOf(row[3]), 
									String.valueOf(row[4]), 
									String.valueOf(row[5]), 
									String.valueOf(row[6]), 
									String.valueOf(row[7]), 
									String.valueOf(row[8]), 
									String.valueOf(row[9]), 
									String.valueOf(row[10]), 
									String.valueOf(row[11]), 
									String.valueOf(row[12]), 
									String.valueOf(row[13]), 
									String.valueOf(row[14]), 
									String.valueOf(row[15]), 
									String.valueOf(row[16]), 
									String.valueOf(row[17]), 
									String.valueOf(row[18]), 
									String.valueOf(row[19]), 
									String.valueOf(row[20]), 
									String.valueOf(row[21]), 
									String.valueOf(row[22]), 
									String.valueOf(row[23]), 
									String.valueOf(row[24]), 
									String.valueOf(row[25]), 
									String.valueOf(row[26]), 
									String.valueOf(row[27]), 
									String.valueOf(row[28]), 
									String.valueOf(row[29]), 
									String.valueOf(row[30]),
									String.valueOf(row[31]),
									String.valueOf(row[32]),
									String.valueOf(row[33]),
									String.valueOf(row[34]),
									String.valueOf(row[35]));
							return prc;
						}

						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).list();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return prc;
	}
	
	@Override
	public int getCountRetryOrRetur(BulkFTReqPRC p){
		BigInteger count = null;
		try {
			count = (BigInteger) getCurrentSession().getNamedQuery("bulkFT#getCountRetryOrRetur")
					.setString("trxdetailid", p.getTrxdetailid())
					.setString("creditaccno", p.getCreditaccno())
					.uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return count.intValue();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BulkFTReqPRC> getAllUnprocessPenihilan(String intLimit) {
		List<BulkFTReqPRC> prcPenihilan = new ArrayList<>();
		
		try {
			prcPenihilan = (List<BulkFTReqPRC>) getCurrentSession()
			.getNamedQuery("bulkFT#getAllUnprocessPenihilan")
			.setBigInteger("limit", new BigInteger(intLimit))
			.setResultTransformer(new ResultTransformer() {
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							BulkFTReqPRC prc = new BulkFTReqPRC(
									String.valueOf(row[0]), 
									String.valueOf(row[1]),
									String.valueOf(row[2]), 
									String.valueOf(row[3]), 
									String.valueOf(row[4]), 
									String.valueOf(row[5]), 
									String.valueOf(row[6]), 
									String.valueOf(row[7]), 
									String.valueOf(row[8]), 
									String.valueOf(row[9]), 
									String.valueOf(row[10]), 
									String.valueOf(row[11]), 
									String.valueOf(row[12]), 
									String.valueOf(row[13]), 
									String.valueOf(row[14]), 
									String.valueOf(row[15]), 
									String.valueOf(row[16]), 
									String.valueOf(row[17]), 
									String.valueOf(row[18]), 
									String.valueOf(row[19]), 
									String.valueOf(row[20]), 
									String.valueOf(row[21]), 
									String.valueOf(row[22]), 
									String.valueOf(row[23]), 
									String.valueOf(row[24]), 
									String.valueOf(row[25]), 
									String.valueOf(row[26]), 
									String.valueOf(row[27]), 
									String.valueOf(row[28]), 
									String.valueOf(row[29]), 
									String.valueOf(row[30]),
									String.valueOf(row[31]),
									String.valueOf(row[32]),
									String.valueOf(row[33]),
									String.valueOf(row[34]));
							return prc;
						}

						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return prcPenihilan;
	}

	@Override
	public void updateAccountValidationStatus(BulkFTReqPRC p, String status, String branch) {
		try {
			getCurrentSession().getNamedQuery("bulkFT#updateAccountValidationStatus")
			.setString("batchid", p.getBatchid())
			.setString("trxheaderid", p.getTrxheaderid())
			.setString("trxdetailid", p.getTrxdetailid())
			.setString("status", status)
			.setString("branch", branch)
			.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public int getHangedTransaction(double timeout) {
		BigInteger hangedTransaction = (BigInteger) getCurrentSession().getNamedQuery("bulkFT#getHangedTransaction")
				.setDouble("timeout", timeout)
				.uniqueResult();
		
		return hangedTransaction.intValue();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ReinquiryMessage> getYetRespondedRequest(String limit, String timeout) {
		List<ReinquiryMessage> listReinquiry = new ArrayList<>();
		
		listReinquiry = getCurrentSession().getNamedQuery("bulkFT#getYetRespondedRequest")
				.setBigInteger("limit", new BigInteger(limit))
				.setDouble("timeout", Double.parseDouble(timeout))
				.setResultTransformer(new ResultTransformer() {
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							ReinquiryMessage rm = new ReinquiryMessage(
									String.valueOf(row[0]), 
									String.valueOf(row[1]),
									String.valueOf(row[2]));
							return rm;
						}

						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).list();
		
		
		return listReinquiry;
	}
	
}
