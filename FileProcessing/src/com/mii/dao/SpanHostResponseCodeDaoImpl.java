package com.mii.dao;

import java.util.List;

import org.hibernate.criterion.Restrictions;

import mii.span.model.SpanHostResponseCode;

import com.mii.helpers.BasicDaoImplement;

public class SpanHostResponseCodeDaoImpl extends BasicDaoImplement implements SpanHostResponseCodeDao{

	@Override
	public List<SpanHostResponseCode> getByRcId(String rcId) {
		//SELECT * FROM SPAN_HOST_RESPONSE_CODE WHERE RC_ID = "STRING"
		
		return (List<SpanHostResponseCode>) getCurrentSession().createCriteria(SpanHostResponseCode.class)
		.add(Restrictions.eq("RC_ID", rcId))
		.list();
	}
	
}
