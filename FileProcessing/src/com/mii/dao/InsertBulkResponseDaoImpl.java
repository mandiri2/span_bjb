package com.mii.dao;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.transform.ResultTransformer;

import mii.span.model.helper.CheckDataFailedOutput;
import mii.span.model.helper.SelectSPANDataValidationByStatusOutput;
import mii.span.model.helper.SpanHostResponseOutput;

import com.mii.helpers.BasicDaoImplement;

public class InsertBulkResponseDaoImpl extends BasicDaoImplement implements InsertBulkResponseDao{
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SelectSPANDataValidationByStatusOutput> selectSPANDataValidationStatusByTrxType(
			String procStatus) {
		List<SelectSPANDataValidationByStatusOutput> validation = new ArrayList<SelectSPANDataValidationByStatusOutput>();
		try{
			validation = (List<SelectSPANDataValidationByStatusOutput>) getCurrentSession().
					getNamedQuery("InsertBulkResponse#selectSPANDataValidationStatusByTrxType")
					.setString("procStatus",procStatus)
					.setString("trxType0", "0")
					.setString("trxType1", "1").
					setResultTransformer(new ResultTransformer() {
						private static final long serialVersionUID = 1L;
						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							SelectSPANDataValidationByStatusOutput selectSPANDataValidationByStatusOutput;
							selectSPANDataValidationByStatusOutput = new SelectSPANDataValidationByStatusOutput(
									String.valueOf(row[0]), 
									String.valueOf(row[1]),
									String.valueOf(row[2]), 
									String.valueOf(row[3]), 
									String.valueOf(row[4]), 
									String.valueOf(row[5]),
									String.valueOf(row[6]),
									String.valueOf(row[7]),
									String.valueOf(row[8]),
									String.valueOf(row[9]).trim(),
									String.valueOf(row[10]),
									String.valueOf(row[11]),
									String.valueOf(row[12]),
									String.valueOf(row[13]),
									String.valueOf(row[14]),
									String.valueOf(row[15]),
									String.valueOf(row[16]),
									String.valueOf(row[17]),
									String.valueOf(row[18]),
									String.valueOf(row[19]),
									String.valueOf(row[20]),
									String.valueOf(row[21]));
							return selectSPANDataValidationByStatusOutput;
						}
						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}}).list();
		}catch(Exception e){
			e.printStackTrace();
		}
		return validation;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CheckDataFailedOutput> checkDataFailed(String fileName) {
		List<CheckDataFailedOutput> output = new ArrayList<CheckDataFailedOutput>();
		try{
			output = (List<CheckDataFailedOutput>) getCurrentSession().
					getNamedQuery("InsertBulkResponse#checkDataFailed")
					.setString("fileName",fileName).
					setResultTransformer(new ResultTransformer() {
						private static final long serialVersionUID = 1L;
						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							CheckDataFailedOutput checkDataFailedOutput;
							checkDataFailedOutput = new CheckDataFailedOutput(
									String.valueOf(row[0]), 
									String.valueOf(row[1]),
									String.valueOf(row[2]), 
									String.valueOf(row[3]), 
									String.valueOf(row[4]), 
									String.valueOf(row[5]),
									String.valueOf(row[6]),
									String.valueOf(row[7]),
									String.valueOf(row[8]),
									String.valueOf(row[9]),
									String.valueOf(row[10]),
									String.valueOf(row[11]),
									String.valueOf(row[12]),
									String.valueOf(row[13]));
							return checkDataFailedOutput;
						}
						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}}).list();
		}catch(Exception e){
			e.printStackTrace();
		}
		return output;
	}

	@Override
	public int getTotalRecSendToHost(String batchId, String trxHeaderId) {
		try{
			BigInteger jml = (BigInteger) getCurrentSession().getNamedQuery("InsertBulkResponse#getTotalRecSendToHost")
				.setString("batchId",batchId)
				.setString("trxHeaderId",trxHeaderId).uniqueResult();
			return jml.intValue();
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	@SuppressWarnings("unchecked")
	@Override
	public SpanHostResponseOutput spanHostResponse(String ifilename,
			String ibatchid, String itrxheaderid, String itotalrecord,
			String ilegstatus) {
		List<SpanHostResponseOutput> output = new ArrayList<SpanHostResponseOutput>();
		SpanHostResponseOutput spanHostResponseOutput = new SpanHostResponseOutput();
		try{
			output = getCurrentSession().getNamedQuery("InsertBulkResponse#spanHostResponse")
				.setString("ifilename", ifilename)
				.setString("ibatchid", ibatchid)
				.setString("itrxheaderid", itrxheaderid)
				.setString("itotalrecord", itotalrecord)
				.setString("ilegstatus", ilegstatus).
				setResultTransformer(new ResultTransformer() {
					private static final long serialVersionUID = 1L;
					@Override
					public Object transformTuple(Object[] row, String[] arg1) {
						SpanHostResponseOutput spanHostResponseOutput;
						spanHostResponseOutput = new SpanHostResponseOutput(
								String.valueOf(row[0]), 
								String.valueOf(row[1]),
								String.valueOf(row[2]));
						return spanHostResponseOutput;
					}
					@SuppressWarnings("rawtypes")
					@Override
					public List transformList(List arg0) {
						return arg0;
					}}).list();
			
			for(SpanHostResponseOutput out : output){
				spanHostResponseOutput = out;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return spanHostResponseOutput;
	}
	
	
}
