package com.mii.dao;

import java.util.List;

import com.mii.helpers.InterfaceDAO;
import com.mii.models.TransactionM;

public interface TransactionDAO extends InterfaceDAO{
	public List<TransactionM> getNTPNIDPelimpahanbyIDBank(String idBank);
	public List<TransactionM> getNTPNIDByRefNo(String bankRefNo);
	public void updateNTPN(String ntpn, String ntb,String kodeBilling);
	public TransactionM updateNoSakti(TransactionM transactionM);
	public List<TransactionM> getNoSaktiByRefNo(String reffNumber);
}
