package com.mii.dao;

import java.math.BigDecimal;
import java.util.List;

import mii.span.model.THQEQBJBModel;

import com.mii.helpers.InterfaceDAO;

public interface THEQBJBDao extends InterfaceDAO{
	public List<THQEQBJBModel> getTHList(int limitTH);
	public void deleteAquiredData(List<BigDecimal> idList);
}
