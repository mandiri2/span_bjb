package com.mii.dao;

import java.math.BigInteger;

import org.hibernate.Query;

import com.mii.helpers.BasicDaoImplement;

public class SpanUserLogDaoImpl extends BasicDaoImplement implements SpanUserLogDao {

	@Override
	public BigInteger countSPANUserLog(String filename) {
		/*select count(*) as countSPANUserLog from span_user_log where description like '%${filename}%'*/
		String hql = "SELECT COUNT(*) FROM SpanUserLog WHERE DESCRIPTION like :filename";
		Query query = getCurrentSession().createQuery(hql);
		query.setString("filename", "%" + filename + "%");
		return(BigInteger)query.uniqueResult();
	}

	@Override
	public String getUserID(String fileName, String rowNumber) {
		/*SELECT USERID FROM (SELECT n.*, ROWNUM totalrow FROM (
		select * from span_user_log where description like '%${filename}%' 
		and user_action like 'ExecutedAction%'
		order by create_date desc
		) n WHERE ROWNUM <= ${rowNumber}) WHERE totalrow = 1*/
		
		Query query = getCurrentSession().getNamedQuery("SpanUserLog#getUserID");
		query.setString("filename", "%"+fileName+"%");
		query.setString("rownumber", rowNumber);
		return (String)query.uniqueResult();
	}

}
