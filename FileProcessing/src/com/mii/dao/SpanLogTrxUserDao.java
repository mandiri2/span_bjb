package com.mii.dao;

import mii.span.process.model.InsertSPANLogTrxUserResponse;

import com.mii.helpers.InterfaceDAO;

public interface SpanLogTrxUserDao extends InterfaceDAO{
	InsertSPANLogTrxUserResponse insertSPANLogTrxUser(String iFileName, String iUserId);
}
