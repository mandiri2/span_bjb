package com.mii.dao;

import java.util.List;

import com.mii.helpers.InterfaceDAO;
import com.mii.models.DataHeaderBPSX;
import com.mii.models.DataValidationBPSX;

public interface DataBPSXDAO extends InterfaceDAO{
	public List<DataHeaderBPSX> insertDataHeader(String mtFilename, String bankCode, String accountNo, 
			String bankStatementDt, String currency, String beginBalance, String totalData, String endingBalance, String bpsxFileEvent, String insertTimeLog);
	public List<DataHeaderBPSX> getDataHeader(String status);
	public List<DataValidationBPSX> getValidateBPSX(String iBatchid, String iBPSXFilename, 
			String iBPSXFileEvent, String iCurrency, String iRownum);
	public void moveBRPDataToLog(String iBatchid, String iStatus);
	public void updateFinalFileFlag(String iBatchid, String iFinalFlag);
	public void updateDataHeaderBPSX(String iBatchid, String setStatus, String setBRPFilename,String Description);
	public void updateDataHeaderStatus(String iBatchid, String iStatus, String iSetStatus, String iDesc);
}
