package com.mii.dao;

import java.util.ArrayList;
import java.util.List;

import com.mii.helpers.BasicDaoImplement;
import com.mii.logger.Log;
import com.mii.logger.LoggerEvent;
import com.mii.models.TransactionM;

@SuppressWarnings("unchecked")
public class TransactionDAOImpl extends BasicDaoImplement implements TransactionDAO{
	private static Log log = Log.getInstance(TransactionDAOImpl.class);
	
	@Override
	public List<TransactionM> getNTPNIDPelimpahanbyIDBank(String idBank){
		List<TransactionM> transactionMs = new ArrayList<TransactionM>();
		try {
			transactionMs = (List<TransactionM>) getCurrentSession().getNamedQuery("transaction#getNTPN_PelimpahanbyIDBank").list();
		} catch (Exception e) {
			LoggerEvent.errorProcess(log, this.getClass().getName()+"/"+this.getClass().getMethods().toString(), e.getMessage());
		}
		return transactionMs;
	}
	
	@Override
	public List<TransactionM> getNTPNIDByRefNo(String bankRefNo){
		List<TransactionM> transactionMs = new ArrayList<TransactionM>();
		try{
			transactionMs = (List<TransactionM>) getCurrentSession().getNamedQuery("DataNTPN#getNTPN").setString("bankRefNo", bankRefNo).list();
		}catch(Exception e){
			LoggerEvent.errorProcess(log, this.getClass().getName()+"/"+this.getClass().getMethods().toString(), e.getMessage());
			e.printStackTrace();
		}
		return transactionMs;
	}
	
	@Override
	public void updateNTPN(String ntpn, String ntb, String kodeBilling) {
		getCurrentSession().getNamedQuery("transaction#updateNTPN").setString("ntpn", ntpn).setString("ntb", ntb).setString("kodeBilling", kodeBilling).executeUpdate();
	}

	@Override
	public TransactionM updateNoSakti(TransactionM transactionM) {
		String ntb = transactionM.getNtb();
		String noSakti = transactionM.getNoSakti();
		
		getCurrentSession().getNamedQuery("transaction#updateNoSakti").setString("noSakti", noSakti).setString("ntb", ntb).executeUpdate();
		return null;
	}
	
	@Override
	public List<TransactionM> getNoSaktiByRefNo(String referenceNum){
		List<TransactionM> transactionMs = new ArrayList<TransactionM>();
		try{
			transactionMs = (List<TransactionM>) getCurrentSession().getNamedQuery("DataNoSakti#getNoSakti").setString("referenceNum", referenceNum).list();
		}catch(Exception e){
			LoggerEvent.errorProcess(log, this.getClass().getName()+"/"+this.getClass().getMethods().toString(), e.getMessage());
			e.printStackTrace();
		}
		return transactionMs;
	}
}
