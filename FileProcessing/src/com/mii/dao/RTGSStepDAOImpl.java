package com.mii.dao;

import java.util.ArrayList;
import java.util.List;

import com.mii.helpers.BasicDaoImplement;
import com.mii.logger.Log;
import com.mii.logger.LoggerEvent;
import com.mii.models.RTGSStepM;

public class RTGSStepDAOImpl extends BasicDaoImplement implements RTGSStepDAO {

	private static Log log = Log.getInstance(RTGSStepDAOImpl.class);
	
	@Override
	public int saveRTGSStep(RTGSStepM rtgsStepM) {
		try {
			save(rtgsStepM);
			return 1;
		} catch (Exception e) {
			LoggerEvent.errorProcess(log, this.getClass().getName()+"/"+this.getClass().getMethods().toString(), e.getMessage());
			return 0;
		}
	}

	@Override
	public RTGSStepM updateRTGSStep(RTGSStepM rtgsStepM) {
		update(rtgsStepM);
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<RTGSStepM> getAllStepByEvent(String event) {
		List<RTGSStepM> listRTGSStep = new ArrayList<RTGSStepM>();
		try {
			listRTGSStep = (List<RTGSStepM>) getCurrentSession().getNamedQuery("bpsxDetail#getAllBPSXDetailbyEvent").setString(event, "event").list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listRTGSStep;
	}

}
