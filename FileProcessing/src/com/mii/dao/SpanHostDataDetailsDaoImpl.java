package com.mii.dao;

import java.util.ArrayList;
import java.util.List;

import mii.span.model.SpanHostDataDetails;
import mii.span.process.model.ACKExecutedDoc;
import mii.span.process.model.GetExecutedTransactionSummaryResponse;
import mii.span.process.model.GetTotalRecAmtSendToHostResponse;
import mii.span.process.model.GetWaitTransactionSummaryResponse;
import mii.span.process.model.SpanHostRequestResponse;
import mii.span.util.BusinessUtil;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.ResultTransformer;

import com.mii.helpers.BasicDaoImplement;
import com.mii.span.model.SPANDataValidation;

public class SpanHostDataDetailsDaoImpl extends BasicDaoImplement implements SpanHostDataDetailsDao {

	@Override
	public List<ACKExecutedDoc> selectSPANHostResponse(String filename) {
		return (List<ACKExecutedDoc>) getCurrentSession().getNamedQuery("selectSPANHostResponse")
				.setString("filename", filename).list();
	}

	@Override
	public List<SpanHostDataDetails> selectSPANHostDetails(String batchId, String trxHeaderId) {
		return (List<SpanHostDataDetails>) getCurrentSession().createCriteria(SpanHostDataDetails.class)
				.add(Restrictions.eq("BATCHID", batchId)).add(Restrictions.eq("TRXHEADERID", trxHeaderId))
				.add(Restrictions.or(Restrictions.or(Restrictions.eq("STATUS", "1"), Restrictions.eq("STATUS", "2")),
						Restrictions.eq("STATUS", "3")))
				.add(Restrictions.eq("EXEC_STATUS", "1")).list();
	}

	@Override
	public GetExecutedTransactionSummaryResponse getExecutedTransactionSummary(String batchId, String trxHeaderId) {
		/**
		 * select count(trxdetailid) as totalRecord, sum(amount) as totalAmount
		 * from SPAN_HOST_DATA_DETAILS where batchid='${batchID}' and
		 * trxheaderid='${trxHeaderID}' and exec_status = '1'
		 */
		Query query = getCurrentSession().getNamedQuery("SpanHostDataDetails#getExecutedTransactionSummary");
		query.setString("batchid", batchId);
		query.setString("trxheaderid", trxHeaderId);
		List<Object[]> list = (List<Object[]>) query.list();
		if (!BusinessUtil.isListEmpty(list)) {
			GetExecutedTransactionSummaryResponse response = new GetExecutedTransactionSummaryResponse();
			for (Object[] objects : list) {
				response.setTotalExecutedRec(objects[0].toString());
				response.setTotalExecutedAmount(objects[1].toString());
			}
			return response;
		}
		return null;
	}

	@Override
	public GetWaitTransactionSummaryResponse getWaitTransactionSummary(String batchId, String trxHeaderId) {
		/**
		 * select count(trxdetailid) as totalRecord, sum(total_amount) as
		 * totalAmount from SPAN_HOST_DATA_DETAILS where batchid='${batchID}'
		 * and trxheaderid='${trxHeaderID}' and exec_status = '0'
		 */
		Query query = getCurrentSession().getNamedQuery("SpanHostDataDetails#getWaitTransactionSummary");
		query.setString("batchid", batchId);
		query.setString("trxheaderid", trxHeaderId);
		List<Object[]> list = (List<Object[]>) query.list();
		if (!BusinessUtil.isListEmpty(list)) {
			GetWaitTransactionSummaryResponse response = new GetWaitTransactionSummaryResponse();
			for (Object[] objects : list) {
				response.setTotalWaitRec(objects[0].toString());
				response.setTotalWaitAmount(objects[1].toString());
			}
			return response;
		}
		return null;
	}

	@Override
	public GetTotalRecAmtSendToHostResponse getTotalRecAmtSendToHost(String batchId, String trxHeaderId) {

		/*
		 * select count(trxdetailid) as totalRecSendToHost, NVL(sum(amount),0)
		 * as totAmtSendToHost from span_host_data_details where
		 * batchid='${batchID}' and trxheaderid='${trxHeaderID}' and status='0'
		 * and exec_status = '1'
		 */

		Query query = getCurrentSession().getNamedQuery("SpanHostDataDetails#getTotalRecAmtSendToHost");
		query.setString("batchid", batchId);
		query.setString("trxheaderid", trxHeaderId);
		List<Object[]> list = (List<Object[]>) query.list();
		if (!BusinessUtil.isListEmpty(list)) {
			GetTotalRecAmtSendToHostResponse response = new GetTotalRecAmtSendToHostResponse();
			for (Object[] objects : list) {
				response.setTotalRecSendToHost(objects[0].toString());
				response.setTotalAmtSendToHost(objects[1].toString());
			}
			return response;
		}

		return null;
	}

	@Override
	public SpanHostRequestResponse spanHostRequest(String iBatchId, String iTrxHeaderId, String iStatus,
			String iPriority, String iChannelId, String iFilename, String iDebitAccountSc, String iSettlement) {
		List<SpanHostRequestResponse> list = (List<SpanHostRequestResponse>) getCurrentSession()
				.getNamedQuery("SpanHostDataDetails#spanHostRequest").setString("ibatchid", iBatchId)
				.setString("itrxheaderid", iTrxHeaderId).setString("istatus", iStatus).setString("ipriority", iPriority)
				.setString("ichannelid", iChannelId).setString("ifilename", iFilename)
				.setString("isettlement", iSettlement).setString("idebitaccountsc", iDebitAccountSc)
				.setResultTransformer(new ResultTransformer() {

					private static final long serialVersionUID = 1L;

					@Override
					public Object transformTuple(Object[] row, String[] arg1) {
						SpanHostRequestResponse spanHostRequestResponse = new SpanHostRequestResponse();
						spanHostRequestResponse.setStatus(String.valueOf(row[0]));
						spanHostRequestResponse.setErrorMessage(String.valueOf(row[1]));
						return spanHostRequestResponse;
					}

					@Override
					public List transformList(List arg0) {
						return arg0;
					}
				}).list();

		if (!BusinessUtil.isListEmpty(list)) {
			return list.get(0);
		}

		return null;
	}

	@Override
	public void saveSpanHostDataDetails(SpanHostDataDetails spanHostDataDetails) {
		setDefaultValueSpanHostDataDetails(spanHostDataDetails);
		save(spanHostDataDetails);
	}

	/**
	 * set default value, karena di postgre default value nya tidak jalan ketika
	 * diinsert
	 */
	private void setDefaultValueSpanHostDataDetails(SpanHostDataDetails spanHostDataDetails) {
		if (spanHostDataDetails != null) {

			if (StringUtils.isEmpty(spanHostDataDetails.getINSTRUCTIONCODE1())) {
				spanHostDataDetails.setINSTRUCTIONCODE1("1");
			}

			if (spanHostDataDetails.getRESPCOUNTCHECKER() == null) {
				spanHostDataDetails.setRESPCOUNTCHECKER(0l);
			}

			if (StringUtils.isEmpty(spanHostDataDetails.getSORBOR_MARK())) {
				spanHostDataDetails.setSORBOR_MARK("0");
			}

			if (StringUtils.isEmpty(spanHostDataDetails.getEXEC_STATUS())) {
				spanHostDataDetails.setEXEC_STATUS("0");
			}
		}
	}

	@Override
	public SpanHostDataDetails getLastSpanHostDataDetailsByDocNumber(String docNumber) {
		@SuppressWarnings("unchecked")
		List<SpanHostDataDetails> list = (List<SpanHostDataDetails>) getCurrentSession()
				.getNamedQuery("SpanHostDataDetails#getLastSpanHostDataDetailsByDocNumber")
				.setString("DOC_NUMBER", docNumber).setMaxResults(1).list();
		SpanHostDataDetails result = new SpanHostDataDetails();
		for (SpanHostDataDetails a : list) {
			result = a;
			break;
		}
		return result;

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SPANDataValidation> getVoidDataToday() {
		List<SPANDataValidation> details = new ArrayList<SPANDataValidation>();
		try {
			details = (List<SPANDataValidation>) getCurrentSession()
					.getNamedQuery("SpanHostDataDetails#getVoidDataToday")
					.setResultTransformer(new ResultTransformer() {
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							SPANDataValidation spanVoidDocumentNumber;
							spanVoidDocumentNumber = new SPANDataValidation();
							spanVoidDocumentNumber.setFileName(String.valueOf(row[0]));
							spanVoidDocumentNumber.setDocNumber(String.valueOf(row[2]));
							spanVoidDocumentNumber.setDocumentDate(String.valueOf(row[1]));
							return spanVoidDocumentNumber;
						}

						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return details;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SPANDataValidation> getACKDataTempNow() {
		List<SPANDataValidation> details = new ArrayList<SPANDataValidation>();
		try {
			details = (List<SPANDataValidation>) getCurrentSession()
					.getNamedQuery("SpanHostDataDetails#getACKDataTempNow")
					.setResultTransformer(new ResultTransformer() {
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							SPANDataValidation spanVoidDocumentNumber;
							spanVoidDocumentNumber = new SPANDataValidation();
							spanVoidDocumentNumber.setFileName(String.valueOf(row[0]));
							spanVoidDocumentNumber.setDocNumber(String.valueOf(row[2]));
							spanVoidDocumentNumber.setDocumentDate(String.valueOf(row[1]));
							return spanVoidDocumentNumber;
						}

						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return details;
	}

	@Override
	public void updateACKFlag(List<String> inputList) {
		getCurrentSession().getNamedQuery("SpanHostDataDetails#updateACKFlag")
			.setParameterList("inputList", inputList).executeUpdate();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SPANDataValidation> getACKReturDataNow() {
		List<SPANDataValidation> details = new ArrayList<SPANDataValidation>();
		try {
			details = (List<SPANDataValidation>) getCurrentSession()
					.getNamedQuery("SpanHostDataDetails#getACKReturDataNow")
					.setResultTransformer(new ResultTransformer() {
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							SPANDataValidation spanVoidDocumentNumber;
							spanVoidDocumentNumber = new SPANDataValidation();
							spanVoidDocumentNumber.setFileName(String.valueOf(row[0]));
							spanVoidDocumentNumber.setDocNumber(String.valueOf(row[2]));
							spanVoidDocumentNumber.setDocumentDate(String.valueOf(row[1]));
							spanVoidDocumentNumber.setResponseCode(String.valueOf(row[3]));
							spanVoidDocumentNumber.setErrorMessage(String.valueOf(row[4]));
							return spanVoidDocumentNumber;
						}

						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return details;
	}

}
