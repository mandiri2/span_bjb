package com.mii.dao;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import mii.span.util.BusinessUtil;

import org.hibernate.Query;
import org.hibernate.transform.ResultTransformer;

import com.mii.helpers.BasicDaoImplement;
import com.mii.logger.Log;
import com.mii.models.DataDetailsMT940;


@SuppressWarnings("unchecked")
public class DataMT940DAOImpl extends BasicDaoImplement implements DataMT940DAO{
	private static Log log = Log.getInstance(DataMT940DAOImpl.class);
	
	@Override
	public List<DataDetailsMT940> getDataDetailMT940(String docdate, String status, String rowNum, String norek){
		//dataDetailMT940.hbm.xml
		//System.out.println("iBatchid =====> "+iBatchid);
		//System.out.println("status =====> "+status);
		//System.out.println("rowNum =====> "+rowNum);
		List<DataDetailsMT940> listDetail = new ArrayList<DataDetailsMT940>();
		//List<DataDetailsMT940> listDetail2 = new ArrayList<DataDetailsMT940>();
				
		try{
			//flagging 10000 from store procedure
			getCurrentSession()
				.getNamedQuery("DataDetailMT940#getDataDetail")
				.setParameter("idocdate", docdate)
				.setParameter("istatus", status)
				.setParameter("irownum", rowNum)
				.setString("idebitaccno", norek)
				.list();
			
			//get 10000 from database
			/*listDetail = (List<DataDetailsMT940>) getCurrentSession().getNamedQuery("DataDetailMT940#getDataDetail2").setString("id", iBatchid).
					setString("status", "X").list();
			for (DataDetailsMT940 dm : listDetail){
				System.out.println("Bank Ref No di dao = "+dm.getBankReffNo());
				System.out.println("line Num di dao = "+dm.getCountLine());
			}
			
			//flagging after get 10000 from database
			getCurrentSession().getNamedQuery("DataDetailMT940#updateDataDetail").setString("id", iBatchid).
			setString("status", "X").executeUpdate();*/
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
		}
		
		return listDetail;
	}
	
	@Override
	public int checkStatusDetailMT940(String docDate, String checkStatus, String errorCode, String norek){
		//checkStatusDetailMT940.hbm.xml
		BigInteger jml = null;
		try{
			jml = (BigInteger) getCurrentSession().getNamedQuery("CheckDetailMT940#checkDetail").
					setString("docDate", docDate).
					setString("status", checkStatus).
					setString("errorCode", errorCode).
					setString("debitaccno", norek).
					uniqueResult();	
		}catch(Exception e){
			log.error(e.getMessage());
			e.printStackTrace();
			return 0;
		}
		
		return jml.intValue();
	}
	
	@Override
	public List<DataDetailsMT940> getDataProcessed(String rowNum, String norek){
		try {
			List<DataDetailsMT940> list = getCurrentSession().getNamedQuery("DataDetailMT940#getDataDetail2")
					.setString("idebitaccno", norek)
					.setString("irownum", rowNum)
					.setResultTransformer(new ResultTransformer() {
						private static final long serialVersionUID = 1L;
						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							DataDetailsMT940 dataDetailsMT940 = new DataDetailsMT940();
							dataDetailsMT940.setBankReffNo(String.valueOf(row[0]));
							dataDetailsMT940.setBankTrxCode(String.valueOf(row[1]));
							dataDetailsMT940.setDebitCredit(String.valueOf(row[2]));
							dataDetailsMT940.setOriginalAmount(String.valueOf(row[3]));
							dataDetailsMT940.setTrxDate(String.valueOf(row[4]));
							dataDetailsMT940.setValueDate(String.valueOf(row[5]));
							return dataDetailsMT940;
						}
						
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					})
					.list();
			
			if(!BusinessUtil.isListEmpty(list)){
				return list;
			}
			return null;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void updateExecuteBs(String norek) {
		try {
			String hql = "UPDATE SpanHostResponse SET bsStatus = '1' WHERE bsStatus='X' AND debitAccNo = :norek ";
			Query query = getCurrentSession().createQuery(hql);
			query.setString("norek", norek);
			query.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}

	@Override
	public List<DataDetailsMT940> getPenihilan(String docDate, String max) {
		List<DataDetailsMT940> list = getCurrentSession().getNamedQuery("CheckDetailMT940#checkDetailPenihilan")
				.setString("docDate", docDate)
				.setString("max", max)
				.setResultTransformer(new ResultTransformer() {
					private static final long serialVersionUID = 1L;
					@Override
					public Object transformTuple(Object[] row, String[] arg1) {
						DataDetailsMT940 dataDetailsMT940 = new DataDetailsMT940();
						dataDetailsMT940.setBankReffNo(String.valueOf(row[0]));
						dataDetailsMT940.setBankTrxCode(String.valueOf(row[1]));
						dataDetailsMT940.setDebitCredit(String.valueOf(row[2]));
						dataDetailsMT940.setOriginalAmount(String.valueOf(row[3]));
						dataDetailsMT940.setTrxDate(String.valueOf(row[4]));
						dataDetailsMT940.setValueDate(String.valueOf(row[5]));
						return dataDetailsMT940;
					}
					
					@Override
					public List transformList(List arg0) {
						return arg0;
					}
				})
				.list();
		return list;
	}

	@Override
	public void updatePenihilan(String docDate, String max) {
		getCurrentSession().getNamedQuery("CheckDetailMT940#updatePenihilan")
			.setString("docDate", docDate)
			.setString("max", max)
			.executeUpdate();
	}

	@Override
	public int getTotalPenihilan(String docDate) {
		BigInteger count = (BigInteger) getCurrentSession().getNamedQuery("CheckDetailMT940#getTotalPenihilan")
				.setString("docDate", docDate)
				.uniqueResult();
		if(BusinessUtil.isBigIntegerEmpty(count)){
			return 0;
		}
		return count.intValue();
	}
}
