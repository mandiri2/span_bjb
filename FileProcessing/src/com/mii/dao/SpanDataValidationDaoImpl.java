package com.mii.dao;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mii.span.doc.DataFileNACK;
import mii.span.model.SpanDataToBeExecute;
import mii.span.model.SpanDataValidation;
import mii.span.util.BusinessUtil;
import mii.span.util.PubUtil;
import mii.utils.CommonDate;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.ResultTransformer;

import com.mii.helpers.BasicDaoImplement;

public class SpanDataValidationDaoImpl extends BasicDaoImplement implements SpanDataValidationDao {

	@Override
	public List<SpanDataValidation> getByFileName(String fileName) {
		String hql = "FROM SpanDataValidation s WHERE s.fileName like :fileName";
		Query query = getCurrentSession().createQuery(hql);
		query.setString("fileName", "%" + fileName + "%");
		return query.list();
	}
	
	@Override
	public List<SpanDataValidation> getByEqualFileName(String fileName) {
		String hql = "FROM SpanDataValidation s WHERE s.fileName = :fileName";
		Query query = getCurrentSession().createQuery(hql);
		query.setString("fileName", fileName );
		return query.list();
	}
	
	@Override
	public List<SpanDataValidation> likeByFileName(String fileName) {
		String hql = "FROM SpanDataValidation s WHERE s.fileName like :fileName";
		Query query = getCurrentSession().createQuery(hql);
		query.setString("fileName", fileName);
		return (List<SpanDataValidation>) query.list();
	}
	
	@Override
	public BigInteger getBatchIDSeq() {
		return (BigInteger) getCurrentSession().getNamedQuery("getBatchIDSeq").uniqueResult();
	}

	@Override
	public BigInteger spanSequenceNo() {
		return (BigInteger) getCurrentSession().getNamedQuery("spanSequenceNo").uniqueResult();
	}

	@Override
	public SpanDataValidation getByCompositeId(String fileName, String batchId,
			String trxHeaderId) {
		String hql = "FROM SpanDataValidation s WHERE s.fileName = :fileName AND s.batchId = :batchId AND s.trxHeaderId = :trxHeaderId";
		Query query = getCurrentSession().createQuery(hql);
		query.setString("fileName", fileName);
		query.setString("batchId", batchId);
		query.setString("trxHeaderId", trxHeaderId);
		
		List<SpanDataValidation> list = (List<SpanDataValidation>) query.list();
		if(!BusinessUtil.isListEmpty(list)){
			return list.get(0);
		}
		return null;
	}

	@Override
	public List<SpanDataValidation> selectSPANDataValidationStatus(
			String procStatus1, String procStatus2) {
		String hql = "FROM SpanDataValidation s WHERE s.procStatuc = :procStatuc1 OR s.procStatuc = :procStatuc2";
		Query query = getCurrentSession().createQuery(hql);
		query.setString("procStatuc1", procStatus1);
		query.setString("procStatuc2", procStatus2);
		return (List<SpanDataValidation>) query.list();
	}

	@Override
	public List<SpanDataValidation> selectSPANDataExecuted(String procStatus) {
		String hql = "FROM SpanDataValidation s WHERE s.procStatuc = :procStatus";
		Query query = getCurrentSession().createQuery(hql);
		query.setString("procStatus", procStatus);
		return (List<SpanDataValidation>) query.list();
	}

	@Override
	public List<SpanDataValidation> selectExtractFileSPAN(String procStatus, String rowNum) {
		/**
		 * select file_name, batchid, trxtype, payroll_type, sc_settlement, create_date,
			document_date, debit_account_type, debit_account, xml_file_name, void_flag,
			rejected_file_flag, extract_file_flag, exec_file_flag, trxheaderid,
			total_record, total_amount, proc_status, response_code
			from span_data_validation where proc_status = '${procStatus}' 
			and (document_date = to_char(sysdate, 'yyyy-MM-dd')
			or document_date >= to_char(sysdate, 'yyyy-MM-dd')) 
			and (extract_file_flag = '0' or extract_file_flag is null) and void_flag is null
			and response_code = '0000' and proc_status <> 'R'
			and rownum <= ${rowNum}
		**/
		System.out.println("selectExtractFileSPAN");
		return (List<SpanDataValidation>)getCurrentSession()
		.createCriteria(SpanDataValidation.class)	//where
		.add(Restrictions.eq("procStatuc", procStatus))//proc_status = '${procStatus}' 
		.add(Restrictions.or(Restrictions.eq("documentDate", BusinessUtil.getDateByPattern("yyyy-MM-dd")) , //and (document_date = to_char(sysdate, 'yyyy-MM-dd')or document_date >= to_char(sysdate, 'yyyy-MM-dd')) 
							Restrictions.ge("documentDate", CommonDate.dateToString("yyyy-MM-dd", new Date())
							)
				)
			)
		.add(Restrictions.or(Restrictions.eq("extractFileFlag", "0") , //and (extract_file_flag = '0' or extract_file_flag is null)
							Restrictions.isNull("extractFileFlag")
				)
			)
		.add(Restrictions.isNull("voidFlag")) //and void_flag is null
		.add(Restrictions.eq("responseCode", "0000")) //and response_code = '0000'
		.add(Restrictions.ne("procStatuc", "R"))	//and proc_status <> 'R'
		.setMaxResults(Integer.valueOf(rowNum)) //and rownum <= ${rowNum}
		.list();
	}

	@Override
	public void udpateChangeStatus(String batchID, String statusChange) {
		/**
		 *
		 * Update span_data_validation
		 * set change_status =statusChange
		 * where batchid = batchID;
		 *
		 */
		String hql = "UPDATE SpanDataValidation SET changeStatus = :statusChange WHERE batchId = :batchID";
		Query query = getCurrentSession().createQuery(hql);
		query.setString("statusChange", statusChange);
		query.setString("batchID", batchID);
		query.executeUpdate();
	}

	@Override
	public List<SpanDataValidation> selectSPANDataValidationStatusByTrxType(
			String procStatus, String trxType1, String trxType2) {
		return (List<SpanDataValidation>) getCurrentSession().createCriteria(SpanDataValidation.class)
				.add(Restrictions.eq("procStatuc", procStatus))
				.add(Restrictions.or(Restrictions.eq("trxType", trxType1), Restrictions.eq("trxType", trxType2)))
				.list();
	}

	@Override
	public void updateTotalRecAmtSendToHost(String totalSendToHost,
			String totalAmountSendToHost, String fileName) {
		String hql = "UPDATE SpanDataValidation SET totalSendToHost = :totalSendToHost, totalAmountSendToHost = :totalAmountSendToHost WHERE fileName = :fileName";
		Query query = getCurrentSession().createQuery(hql);
		query.setString("totalSendToHost", totalSendToHost);
		query.setString("totalAmountSendToHost", totalAmountSendToHost);
		query.setString("fileName", fileName);
		query.executeUpdate();
	}

	@Override
	public void saveSpanDataValidation(SpanDataValidation spanDataValidation) {
		setDefaultValueSpanDataValidation(spanDataValidation);
		save(spanDataValidation);
	}
	
	/**
	 * set default value, karena di postgre default value nya tidak jalan ketika diinsert
	 */
	private void setDefaultValueSpanDataValidation(
			SpanDataValidation spanDataValidation) {
		if(spanDataValidation!=null){
			
			if(StringUtils.isEmpty(spanDataValidation.getRetryStatus())){
				spanDataValidation.setRetryStatus("0");
			}
			
			if(StringUtils.isEmpty(spanDataValidation.getReturStatus())){
				spanDataValidation.setReturStatus("0");
			}
			
//			if(spanDataValidation.getRetryNumberL1()==0){
//				spanDataValidation.setRetryNumberL1(0);
//			}
			
			if(StringUtils.isEmpty(spanDataValidation.getForcedAck())){
				spanDataValidation.setForcedAck("0");
			}
			
//			if(spanDataValidation.getRetryNumberL2()==0){
//				spanDataValidation.setRetryNumberL2(0);
//			}
				
//			if(StringUtils.isEmpty(spanDataValidation.getLegStatus()==null)){
//				spanDataValidation.setLegStatus(0);
//			}
			
			if(StringUtils.isEmpty(spanDataValidation.getFtpStatus())){
				spanDataValidation.setFtpStatus("0");
			}
			
			if(StringUtils.isEmpty(spanDataValidation.getLateUpdateFlagging())){
				spanDataValidation.setLateUpdateFlagging("0");
			}
			
			//dicomment karena butuh null value
//			if(StringUtils.isEmpty(spanDataValidation.getRejectedFileFlag())){
//				spanDataValidation.setRejectedFileFlag("0");
//			}
			
//			if(StringUtils.isEmpty(spanDataValidation.get)){
//				spanDataValidation.set
//			}
		}
		
	}

	@Override
	public int selectCountSPANFileRejected(String fileName) {
		BigInteger countBig = new BigInteger("0");
		try {
			countBig = (BigInteger) getCurrentSession().getNamedQuery("SPANValidation#selectCountSPANFileRejected")
					.setString("filename", fileName)
					.uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return countBig.intValue();
	}

	@Override
	public int countOngoingTransaction() {
		BigInteger countBig = new BigInteger("0");
		try {
			countBig = (BigInteger) getCurrentSession().getNamedQuery("SPANValidation#countOngoingTransaction")
					.uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return countBig.intValue();
	}

	@Override
	public boolean isStillProcessing(String docDate) {
		String hql = "SELECT COUNT(*) FROM SpanDataValidation s WHERE s.documentDate = :documentDate"
				+ " AND s.responseCode != '005'"
				+ " AND (s.procStatuc = '1'"
				+ " OR s.procStatuc = 'X'"
				+ " OR s.procStatuc = '2'"
				+ " OR s.procStatuc = '3'"
				+ " OR s.procStatuc = '4')";
		Query query = getCurrentSession().createQuery(hql);
		query.setString("documentDate", docDate);
		Long count = (Long)query.uniqueResult();
		if(BusinessUtil.isLongEmpty(count)){
			return false;
		}
		return true;
	}

	@Override
	public boolean isSp2dExist(String docDate) {
		String hql = "SELECT COUNT(*) FROM SpanDataValidation s WHERE s.documentDate = :documentDate";
		Query query = getCurrentSession().createQuery(hql);
		query.setString("documentDate", docDate);
		Long count = (Long) query.uniqueResult();
		if(BusinessUtil.isLongEmpty(count)){
			return false;
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public DataFileNACK selectSPANDataValidationNACK(String procStatus,
			String documentDate, String responseCode, String providerCode,
			String filename) {
		DataFileNACK dataFileNACK = new DataFileNACK();
		try {
			String hql = "select fileName, xmlFileName "
					+ "from SpanDataValidation "
					+ "where procStatuc='"+procStatus+"' "
					+ "and documentDate='"+documentDate+"' "
					+ "and responseCode='"+responseCode+"' "
					+ "and fileName = '"+filename+"' ";
			Query query = getCurrentSession().createQuery(hql);
			List<SpanDataValidation> resultList = new ArrayList<SpanDataValidation>();
			resultList = query.					
					setResultTransformer(new ResultTransformer() {						
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							SpanDataValidation spanDataValidation;
							try {
								spanDataValidation = new SpanDataValidation(
										String.valueOf(row[0]),
										String.valueOf(row[1]));
								return spanDataValidation;
							} catch (Exception e) {
								e.printStackTrace();
							}
							return null;
						}
						
						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).list();
			SpanDataValidation sdv = resultList.get(0);
			dataFileNACK.setFilename(sdv.getFileName());
			dataFileNACK.setXmlfilename(sdv.getXmlFileName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dataFileNACK;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SpanDataValidation> getNACKUpdateList(String accountType) {
		List<SpanDataValidation> SpanDataValidationList = new ArrayList<SpanDataValidation>();
		try {
			SpanDataValidationList = (List<SpanDataValidation>)getCurrentSession().
					getNamedQuery("SpanDataValidation#getNACKUpdateList")
					.setString("accountType", accountType).
					setResultTransformer(new ResultTransformer() {						
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							SpanDataValidation spanDataValidation;
							try {
								spanDataValidation = new SpanDataValidation(
										String.valueOf(row[0]));
								return spanDataValidation;
							} catch (Exception e) {
								e.printStackTrace();
							}
							return null;
						}
						
						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SpanDataValidationList;
	}

	
}
