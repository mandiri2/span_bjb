package com.mii.dao;

import mii.span.model.SpanDocumentNoDetailList;

import com.mii.helpers.BasicDaoImplement;

public class SpanDocumentNoDetailListDaoImpl extends BasicDaoImplement implements SpanDocumentNoDetailListDao {

	@Override
	public void saveSpanDocumentNoDetailList(
			SpanDocumentNoDetailList spanDocumentNoDetailList) {
		setDefaultValue(spanDocumentNoDetailList);
		save(spanDocumentNoDetailList);
	}

	private void setDefaultValue(
			SpanDocumentNoDetailList spanDocumentNoDetailList) {
		
	}
	
}
