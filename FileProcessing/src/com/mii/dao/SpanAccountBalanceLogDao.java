package com.mii.dao;

import com.mii.helpers.InterfaceDAO;

public interface SpanAccountBalanceLogDao extends InterfaceDAO {
	String getLastBeginBalance(String accountType, String DocDateBeginingBal);
	String getLastCloseBalance(String accountType, String DocDateCloseBal);
	void updateBeginBalance(String beginBalance, String accountType, String inquiryDate);
}
