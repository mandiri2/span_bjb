package com.mii.dao;
import java.util.ArrayList;
import java.util.List;

import com.mii.helpers.BasicDaoImplement;
//import com.mii.logger.Log;
import com.mii.models.DataHeaderBPSX;
import com.mii.models.DataValidationBPSX;

@SuppressWarnings("unchecked")
public class DataBPSXDAOImpl extends BasicDaoImplement implements DataBPSXDAO{
	//private static Log log = Log.getInstance(DataBPSXDAOImpl.class);
	@Override
	public List<DataHeaderBPSX> insertDataHeader(String mtFilename, String bankCode, String accountNo, 
			String bankStatementDt, String currency, String beginBalance, String totalData, 
			String endingBalance, String bpsxFileEvent, String insertTimeLog){
		List<DataHeaderBPSX> listHeader = new ArrayList<DataHeaderBPSX>();
		try{
			listHeader = (List<DataHeaderBPSX>) getCurrentSession().getNamedQuery("insertDataHeaderBPSX#dataHeader").
					setParameter("mtFilename", mtFilename).
					setParameter("bankCode", bankCode).
					setParameter("accountNo", accountNo).
					setParameter("bankStatementDt", bankStatementDt).
					setParameter("currency", currency).
					setParameter("beginBalance", beginBalance).
					setParameter("totalData", totalData).
					setParameter("endingBalance", endingBalance).
					setParameter("bpsxFileEvent", bpsxFileEvent).
					setParameter("insertTimeLog", insertTimeLog).
					list();
		}catch(Exception e){
			e.printStackTrace();
		}
		return listHeader;
	}
	
	@Override
	public List<DataHeaderBPSX> getDataHeader(String status){
		List<DataHeaderBPSX> listHeader = new ArrayList<DataHeaderBPSX>();
		try{
			listHeader = (List<DataHeaderBPSX>) getCurrentSession().getNamedQuery("DataHeaderBPSX#getDataHeader").setString("status", status).list();
		}catch(Exception e){
			//log.error(e.getMessage());
			e.printStackTrace();
		}
		return listHeader;
	}
	
	@Override
	public List<DataValidationBPSX> getValidateBPSX(String iBatchid, String iBPSXFilename, 
			String iBPSXFileEvent, String iCurrency, String iRownum){
		List<DataValidationBPSX> listData = new ArrayList<DataValidationBPSX>();
		try{
			listData = (List<DataValidationBPSX>) getCurrentSession().getNamedQuery("DataDetailBPSX#getDataValidBPSX").
					setParameter("iBatchid", iBatchid).
					setParameter("iBPSXFilename", iBPSXFilename).
					setParameter("iBPSXFileEvent", iBPSXFileEvent).
					setParameter("iCurrency", iCurrency).
					setParameter("iRownum", iRownum).
					list();
		}catch(Exception e){
			//log.error(e.getMessage());
			e.printStackTrace();
		}
		return listData;
	}
	
	@Override
	public void moveBRPDataToLog(String iBatchid, String iStatus){
		try{
			getCurrentSession().getNamedQuery("moveDataBRPToLog#dataLog").setParameter("iBatchid", iBatchid).setParameter("iStatus", iStatus).executeUpdate();
		}catch(Exception e){
			//log.error(e.getMessage());
			e.printStackTrace();
		}
	}
	
	@Override
	public void updateFinalFileFlag(String iBatchid, String iFinalFlag){
		try{
			getCurrentSession().getNamedQuery("updateFinalFileFlag#updateFlag").setParameter("iBatchid", iBatchid).setParameter("iFinalFlag", iFinalFlag).executeUpdate();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void updateDataHeaderBPSX(String iBatchid, String setStatus, String setBRPFilename,String Description){
		try{
			getCurrentSession().getNamedQuery("updateBPSXHeader#updateBPSXHeader").setParameter("iBatchid", iBatchid).
			setParameter("setStatus", setStatus).
			setParameter("setBRPFilename", setBRPFilename).
			setParameter("Description", Description).
			executeUpdate();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void updateDataHeaderStatus(String iBatchid, String iStatus, String iSetStatus, String iDesc){
		try{
			getCurrentSession().getNamedQuery("updateBPSXHeader#updateHeaderStatus").setParameter("iBatchid", iBatchid).
			setParameter("iStatus", iStatus).
			setParameter("iSetStatus", iSetStatus).
			setParameter("iDesc", iDesc).
			executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
