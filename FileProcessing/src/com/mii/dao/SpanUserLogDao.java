package com.mii.dao;

import java.math.BigInteger;

import com.mii.helpers.InterfaceDAO;

public interface SpanUserLogDao extends InterfaceDAO {
	BigInteger countSPANUserLog(String filename);
	String getUserID(String fileName, String rowNumber);
}
