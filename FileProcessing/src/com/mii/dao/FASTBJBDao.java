package com.mii.dao;

import mii.span.model.FASTGapuraAccount;

import com.mii.helpers.InterfaceDAO;

public interface FASTBJBDao extends InterfaceDAO{
	public int validateUserAccountBJB(String accountName, String accountNo);
	public FASTGapuraAccount validateUserAccountFASTBJB(String accountName, String accountNumber);
}
