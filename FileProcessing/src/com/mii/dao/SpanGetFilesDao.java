package com.mii.dao;

import com.mii.helpers.InterfaceDAO;

public interface SpanGetFilesDao extends InterfaceDAO{
	void updateSpanGetFiles(String totalRetry, String filename);
}
