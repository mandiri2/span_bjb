package com.mii.dao;

import java.util.List;

import com.mii.helpers.InterfaceDAO;
import com.mii.models.RTGSM;
import com.mii.models.TransactionM;

public interface RTGSDAO extends InterfaceDAO{

	public int saveRTGS(RTGSM rtgsm);
	public RTGSM updateRTGS(RTGSM rtgsm);
	public List<RTGSM> getBPSXFile(String referenceNo);
	public List<RTGSM> getRTGSbyState(String state);
	public List<RTGSM> getRTGSbyFlag(String transactionFlag);
	public void updateRTGSResponse(RTGSM rtgsm);
	public void updateRTGSNoSakti(RTGSM rtgsm); 
	public void updateRTGSFlag(String reffNo);
	public List<TransactionM> getRTGSbyFlag10(String transactionFlag);
	public List<TransactionM> checkRTGSData(String BPSFilename);
}
