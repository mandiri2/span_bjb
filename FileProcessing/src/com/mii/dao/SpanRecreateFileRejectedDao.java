package com.mii.dao;

import java.util.List;

import mii.span.model.SpanRecreateFileRejected;
import mii.span.model.SpanRejectedFileHis;

import com.mii.helpers.InterfaceDAO;

public interface SpanRecreateFileRejectedDao extends InterfaceDAO{
	List<SpanRecreateFileRejected> getByNewFileName(String newFileName);
	void saveSpanRecreateFileRejected(SpanRecreateFileRejected fileRejected);
	void saveSpanRejectedFileHis(SpanRejectedFileHis fileHis);
	
	SpanRejectedFileHis selectRejectedFileHisByFilename(String fileName);
}
