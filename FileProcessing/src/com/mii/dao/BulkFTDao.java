package com.mii.dao;

import java.math.BigInteger;
import java.util.List;

import mii.span.iso.model.ReinquiryMessage;
import mii.span.model.BulkFTReqPRC;

import org.jpos.iso.ISOMsg;

import com.mii.helpers.InterfaceDAO;

public interface BulkFTDao extends InterfaceDAO {
	List<BulkFTReqPRC> getAll();
	List<BulkFTReqPRC> getAllUnprocess(String intLimit);
	void updatePrcStatus(BulkFTReqPRC prc);
	void insertRSP(BulkFTReqPRC prc, ISOMsg response);
	BigInteger getNextStan();
	void insertReqLog(ISOMsg msg);
	void insertReqLog(ISOMsg msg, String action);
	void updateResLog(ISOMsg msg);
	void updateResLogReinquiry(ISOMsg msg);
	BigInteger checkResponseExist(BulkFTReqPRC prc);
	void updateRetryCount(BulkFTReqPRC prc);
	List<BulkFTReqPRC> getAllUnprocessRetryOrPosting(String param_value, String type);
	void updateRSP(BulkFTReqPRC prc, ISOMsg msg);
	void updatePrcStatusBatch(List<String> trxDetailIDList);
	int getCountRetryOrRetur(BulkFTReqPRC p);
	List<BulkFTReqPRC> getAllUnprocessPenihilan(String param_value);
	void updateAccountValidationStatus(BulkFTReqPRC p, String status, String branch);
	int getHangedTransaction(double timeout);
	List<ReinquiryMessage> getYetRespondedRequest(String limit, String timeout);
}
