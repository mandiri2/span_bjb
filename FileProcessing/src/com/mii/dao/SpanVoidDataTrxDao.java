package com.mii.dao;

import java.util.List;

import mii.span.model.SpanVoidDataTrx;

import com.mii.helpers.InterfaceDAO;
import com.mii.span.model.SpanSp2dVoidLog;

public interface SpanVoidDataTrxDao extends InterfaceDAO {
	List<SpanVoidDataTrx> getByFileNameAndStatus(String fileName, String status);

	public void InsertSPANDataVoidTransaction(SpanVoidDataTrx spanVoidDataTrx);
	/*public void insertSpanSp2dVoidLogs(List<SpanSp2dVoidLog> spanSp2dVoidLogs);
	public List<SpanSp2dVoidLog> viewSpanSp2dVoidLogs(String filenameFilter, String sp2dNoFilter, int min, int max);
	public int countSpanSp2dVoidLogs(String filenameFilter, String sp2dNoFilter);*/

	/*public void updateDao(String sql);
	public void deleteSPANVoidDataTransaction(String filename, String documentNumber);
	public String selectSpanVoidDataTrxAmountByDocNumber(String documentNumber);*/

	List<SpanSp2dVoidLog> checkSpanSp2dVoidLogsBySp2dNo(String sp2dNo);

	List<SpanSp2dVoidLog> getAllSpanSp2dVoidLogs();
}
