package com.mii.dao;

import java.util.Date;

import mii.span.model.SpanDataOriginalXML;

import com.mii.helpers.BasicDaoImplement;

public class SpanDataOriginalXMLDaoImpl extends BasicDaoImplement implements SpanDataOriginalXMLDao{

	@Override
	public void saveSpanDataOriginalXml(SpanDataOriginalXML spanDataOriginalXML) {
		setDefaultValue(spanDataOriginalXML);
		save(spanDataOriginalXML);
	}

	private void setDefaultValue(SpanDataOriginalXML spanDataOriginalXML) {
		spanDataOriginalXML.setCREATE_DATE(new Date());
	}
	
}
