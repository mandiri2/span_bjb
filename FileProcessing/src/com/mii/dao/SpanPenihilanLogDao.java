package com.mii.dao;

import mii.span.model.BulkFTReqPRC;

import org.jpos.iso.ISOMsg;

import com.mii.helpers.InterfaceDAO;

public interface SpanPenihilanLogDao extends InterfaceDAO{
	boolean isStillProsesPenihilan();
	int getSequencePenihilan(String documentDate);
	void updatePenihilanLog(BulkFTReqPRC prc, ISOMsg msg);
}
