package com.mii.dao;

import java.util.List;

import com.mii.helpers.InterfaceDAO;
import com.mii.models.MT940M;

public interface MT940DAO extends InterfaceDAO{
	public List<MT940M> getMT940(int rownum);
}
