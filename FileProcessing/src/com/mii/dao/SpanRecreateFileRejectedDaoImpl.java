package com.mii.dao;

import java.util.List;

import org.hibernate.Query;

import mii.span.model.SpanRecreateFileRejected;
import mii.span.model.SpanRejectedFileHis;

import com.mii.helpers.BasicDaoImplement;

public class SpanRecreateFileRejectedDaoImpl extends BasicDaoImplement implements SpanRecreateFileRejectedDao {

	@Override
	public List<SpanRecreateFileRejected> getByNewFileName(
			String newFileName) {
		String hql = "FROM SpanRecreateFileRejected s WHERE s.newFileName = :newFileName";
		Query query = getCurrentSession().createQuery(hql);
		query.setString("newFileName", newFileName);
		return (List<SpanRecreateFileRejected>) query.list();
	}

	@Override
	public void saveSpanRecreateFileRejected(
			SpanRecreateFileRejected fileRejected) {
		setDefaultValueSpanRecreateFileRejected(fileRejected);
		save(fileRejected);
	}
	
	@Override
	public void saveSpanRejectedFileHis(SpanRejectedFileHis fileHis) {
		save(fileHis);	
	}

	private void setDefaultValueSpanRecreateFileRejected(
			SpanRecreateFileRejected fileRejected) {
		if (fileRejected != null) {
			// TODO set default value
		}
		
	}

	@Override
	public SpanRejectedFileHis selectRejectedFileHisByFilename(String fileName) {
		SpanRejectedFileHis his = (SpanRejectedFileHis) getCurrentSession()
				.getNamedQuery("SpanRejectedFileHis#selectRejectedFileHisByFilename")
				.setString("fileName", fileName)
				.uniqueResult();
		return his;
	}

}
