package com.mii.dao;

import mii.span.model.SpanDocumentNoDetailList;

import com.mii.helpers.InterfaceDAO;

public interface SpanDocumentNoDetailListDao extends InterfaceDAO{
	void saveSpanDocumentNoDetailList(SpanDocumentNoDetailList spanDocumentNoDetailList);
}
