package com.mii.dao;

import java.util.List;

import mii.span.model.SpanHostDataDetails;
import mii.span.process.model.ACKExecutedDoc;
import mii.span.process.model.GetExecutedTransactionSummaryResponse;
import mii.span.process.model.GetTotalRecAmtSendToHostResponse;
import mii.span.process.model.GetWaitTransactionSummaryResponse;
import mii.span.process.model.SpanHostRequestResponse;

import com.mii.helpers.InterfaceDAO;
import com.mii.span.model.SPANDataValidation;

public interface SpanHostDataDetailsDao extends InterfaceDAO{
	List<ACKExecutedDoc> selectSPANHostResponse(String filename);
	List<SpanHostDataDetails> selectSPANHostDetails(String batchId, String trxHeaderId);
	GetExecutedTransactionSummaryResponse getExecutedTransactionSummary(String batchId, String trxHeaderId);
	GetWaitTransactionSummaryResponse getWaitTransactionSummary(String batchId, String trxHeaderId);
	GetTotalRecAmtSendToHostResponse getTotalRecAmtSendToHost(String batchId, String trxHeaderId);
	SpanHostRequestResponse spanHostRequest(String iBatchId, String iTrxHeaderId, String iStatus, String iPriority, String iChannelId, 
			String iFilename, String iDebitAccountSc, String iSettlement);
	void saveSpanHostDataDetails(SpanHostDataDetails spanHostDataDetails);
	public SpanHostDataDetails getLastSpanHostDataDetailsByDocNumber(String docNumber);
	List<SPANDataValidation> getVoidDataToday();
	List<SPANDataValidation> getACKDataTempNow();
	public void updateACKFlag(List<String> inputList);
	List<SPANDataValidation> getACKReturDataNow();
}
