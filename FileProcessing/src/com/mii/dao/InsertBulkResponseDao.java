package com.mii.dao;

import java.util.List;

import mii.span.model.helper.CheckDataFailedOutput;
import mii.span.model.helper.SelectSPANDataValidationByStatusOutput;
import mii.span.model.helper.SpanHostResponseOutput;

import com.mii.helpers.InterfaceDAO;

public interface InsertBulkResponseDao extends InterfaceDAO{
	
	public List<SelectSPANDataValidationByStatusOutput> selectSPANDataValidationStatusByTrxType(String procStatus);
	public List<CheckDataFailedOutput> checkDataFailed(String fileName);
	public int getTotalRecSendToHost(String batchId, String trxHeaderId);
	public SpanHostResponseOutput spanHostResponse (String ifilename, String ibatchid, String itrxheaderid, String itotalrecord, String ilegstatus);
	
}
