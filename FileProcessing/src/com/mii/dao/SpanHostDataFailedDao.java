package com.mii.dao;

import java.util.List;

import mii.span.model.SpanHostDataFailed;
import mii.span.process.model.GetSPANDataFailedLastStatusResponse;

import com.mii.helpers.InterfaceDAO;

public interface SpanHostDataFailedDao extends InterfaceDAO {
	List<GetSPANDataFailedLastStatusResponse>getSPANDataFailedLastStatus(String iFileName);
	List<SpanHostDataFailed> getSCFirstData(String fileName, String totalProccess, String legStatus);
	void saveSpanHostDataFailed(SpanHostDataFailed spanHostDataFailed);
	public SpanHostDataFailed getSpanHostDataFailed(String filename);
}
