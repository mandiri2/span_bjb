package com.mii.dao;

import com.mii.helpers.BasicDaoImplement;
import com.mii.models.ReconFileM;

public class MPNReconFIleDAOImpl extends BasicDaoImplement implements MPNReconFIleDAO{

	@Override
	public int saveMPNRecon(ReconFileM reconFileM) {
		try {
			save(reconFileM);
			return 1;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

}
