package com.mii.dao;

import java.util.List;

import com.mii.helpers.InterfaceDAO;
import com.mii.models.SchedulerM;

public interface SchedulerDao extends InterfaceDAO{

	public List<SchedulerM> getAllScheduler();
	public List<SchedulerM> getScheduler(String schedulerID);
	public int saveScheduler(SchedulerM schedulerM);
	public SchedulerM deleteScheduler(String schedulerID);
	public SchedulerM updateScheduler(SchedulerM schedulerM);
	
}
