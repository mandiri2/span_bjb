package com.mii.dao;

import java.util.List;
import java.util.ArrayList;

import mii.span.util.BusinessUtil;

import org.hibernate.criterion.Restrictions;

import com.mii.constant.Constants;
import com.mii.helpers.BasicDaoImplement;
import com.mii.logger.Log;
import com.mii.logger.LoggerEvent;
import com.mii.models.SystemParameter;

@SuppressWarnings("unchecked")
public class SystemParameterDAOImpl extends BasicDaoImplement implements SystemParameterDAO{
	
	private static final Log log = Log.getInstance(SystemParameterDAOImpl.class);
	
	@Override
	public List<SystemParameter> getValueParameter(String param_name){
		List<SystemParameter> params = new ArrayList<SystemParameter>();
		
		try{
			params = (List<SystemParameter>) getCurrentSession().getNamedQuery("Param#getParameter").setString("param_name", param_name).list();
		}catch(Exception e){
			e.printStackTrace();
			LoggerEvent.errorProcess(log, this.getClass().getName()+"/"+this.getClass().getMethods().toString(), e.getMessage());
		}
		return params;
	}
	
	@Override
	public List<SystemParameter> getAllValueParameter() {
		List<SystemParameter> params = new ArrayList<SystemParameter>();
		
		try{
			params = (List<SystemParameter>) getCurrentSession().getNamedQuery("Param#getAllParameter").list();
			
		}catch(Exception e){
			e.printStackTrace();
			LoggerEvent.errorProcess(log, this.getClass().getName()+"/"+this.getClass().getMethods().toString(), e.getMessage());
		}
		
		return params;
	}

	@Override
	public void updateParamValue(String value, String paramName) {
		try {
			getCurrentSession().getNamedQuery("Param#updateParamValue")
			.setString("value", value)
			.setString("paramName", paramName)
			.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override	
	public void updateParameter(String name, String value) {
		try{
			getCurrentSession().getNamedQuery("Param#updateParameter").
			setString("name", name).
			setString("value", value)
			.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Override
	public String getValueByParamName(String paramName) {
		try {
			List<SystemParameter> systemParameters = getCurrentSession().createCriteria(SystemParameter.class)
			.add(Restrictions.eq("param_name", paramName))
			.list();
			if(!BusinessUtil.isListEmpty(systemParameters)){
				return systemParameters.get(0).getParam_value();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public SystemParameter getDetailedParameterByParamName(String paramName) {
		// TODO Auto-generated method stub
		SystemParameter systemParameter = new SystemParameter();
		try{
			List<String> listStatus = new ArrayList<String>();
			listStatus.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
			listStatus.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
			systemParameter = (SystemParameter) getCurrentSession().getNamedQuery("TParameter#getSystemParameterByParamName")
					.setString("PARAM_NAME", paramName)				
					.setParameterList("status", listStatus)	
					.uniqueResult();
		}catch(Exception e){
			e.printStackTrace();
		}
		return systemParameter;
	}

}
