package com.mii.dao;

import java.util.List;

import mii.span.model.SelectFNDataValidationOutput;
import mii.span.model.SpanDataToBeExecute;
import mii.span.model.ToBeExecuteOutput;

import com.mii.helpers.InterfaceDAO;

public interface ToBeExecuteDao extends InterfaceDAO{
	public List<SpanDataToBeExecute> selectAllDataToBeExecuted(String accountNo);
	public SelectFNDataValidationOutput selectFNDataValidation(String fileName);
	public ToBeExecuteOutput spanExecByFilename(String fileName, String scFlag, String glAccount, String asynchronous, String payrolltype,
			String retryNo, String settlement, String scDate);
	
	public int countFirstAvailableBalance(String accountType);
}
