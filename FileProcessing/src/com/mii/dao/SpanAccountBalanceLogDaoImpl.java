package com.mii.dao;

import java.util.List;

import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import mii.span.model.SpanAccountBalanceLog;
import mii.span.util.BusinessUtil;

import com.mii.helpers.BasicDaoImplement;

public class SpanAccountBalanceLogDaoImpl extends BasicDaoImplement implements SpanAccountBalanceLogDao {

	@SuppressWarnings("unchecked")
	@Override
	public String getLastBeginBalance(String accountType, String docDateBeginingBal) {
		List<SpanAccountBalanceLog> log = (List<SpanAccountBalanceLog>) getCurrentSession()
				.createCriteria(SpanAccountBalanceLog.class)
				.add(Restrictions.eq("accountType", accountType))
				.add(Restrictions.eq("inquiryDate", docDateBeginingBal))
				.addOrder(Order.desc("inquiryDate"))
				.setMaxResults(1)
				.list();
		if(!BusinessUtil.isListEmpty(log)){
			return log.get(0).getBeginBalance();
		}
		
		return "0";
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String getLastCloseBalance(String accountType, String DocDateCloseBal) {
		List<SpanAccountBalanceLog> log = (List<SpanAccountBalanceLog>) getCurrentSession()
				.createCriteria(SpanAccountBalanceLog.class)
				.add(Restrictions.eq("accountType", accountType))
				.add(Restrictions.eq("inquiryDate", DocDateCloseBal))
				.addOrder(Order.desc("inquiryDate"))
				.setMaxResults(1)
				.list();
		if(!BusinessUtil.isListEmpty(log)){
			return log.get(0).getBeginBalance();
		}
		
		return "0";
	}

	@Override
	public void updateBeginBalance(String beginBalance, String accountType, String inquiryDate) {
		getCurrentSession().getNamedQuery("updateBeginBalance")
		.setString("beginBalance", beginBalance)
		.setString("accountType", accountType)
		.setString("inquiryDate", inquiryDate)
		.executeUpdate();
	}

	
}
