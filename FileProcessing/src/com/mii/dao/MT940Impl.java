package com.mii.dao;

import java.util.ArrayList;
import java.util.List;

import com.mii.helpers.BasicDaoImplement;
import com.mii.models.MT940M;

public class MT940Impl extends BasicDaoImplement implements MT940DAO{
	@SuppressWarnings("unchecked")
	public List<MT940M> getMT940(int rownum) {
		List<MT940M> mt940ms = new ArrayList<MT940M>();
		try {
			mt940ms = (List<MT940M>) getCurrentSession().getNamedQuery("mt940#getMT940Data").list();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return mt940ms;
	}
}
