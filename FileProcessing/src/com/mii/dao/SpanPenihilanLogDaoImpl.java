package com.mii.dao;

import java.math.BigInteger;
import java.util.Date;

import mii.span.model.BulkFTReqPRC;
import mii.span.util.BusinessUtil;

import org.jpos.iso.ISOMsg;

import com.mii.helpers.BasicDaoImplement;

public class SpanPenihilanLogDaoImpl extends BasicDaoImplement implements SpanPenihilanLogDao {

	@Override
	public boolean isStillProsesPenihilan() {
			
		String hql = "SELECT COUNT(*) FROM SpanPenihilanLog s WHERE s.procStatus = '1'";
		Long count = (Long) getCurrentSession().createQuery(hql).uniqueResult();
		if(BusinessUtil.isLongEmpty(count)){
			return false;
		}
		return true;
	}

	@Override
	public int getSequencePenihilan(String documentDate) {
		BigInteger seq = (BigInteger) getCurrentSession().getNamedQuery("SPANPenihilanLog#getSequencePenihilan")
				.setString("documentDate", documentDate)
				.uniqueResult();
		
		if(BusinessUtil.isBigIntegerEmpty(seq)){
			return 0;
		}
		return seq.intValue();
	}
		
	public void updatePenihilanLog(BulkFTReqPRC prc, ISOMsg msg) {
		try {
			getCurrentSession().getNamedQuery("SPANPenihilanLog#updatePenihilanLog")
			.setString("responseCode", msg.getString(39))
			.setString("procStatus", "2")
			.setDate("updateDate", new Date())
			.setString("batchID", prc.getBatchid())
			.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
