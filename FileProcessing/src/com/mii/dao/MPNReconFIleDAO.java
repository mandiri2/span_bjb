package com.mii.dao;

import com.mii.helpers.InterfaceDAO;
import com.mii.models.ReconFileM;

public interface MPNReconFIleDAO extends InterfaceDAO{

	public int saveMPNRecon(ReconFileM reconFileM);
}
