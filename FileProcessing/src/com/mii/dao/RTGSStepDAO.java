package com.mii.dao;

import java.util.List;

import com.mii.helpers.InterfaceDAO;
import com.mii.models.RTGSStepM;

public interface RTGSStepDAO extends InterfaceDAO {

	public int saveRTGSStep(RTGSStepM rtgsStepM);
	public RTGSStepM updateRTGSStep(RTGSStepM rtgsStepM);
	public List<RTGSStepM> getAllStepByEvent(String event);
	
}
