package com.mii.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import mii.span.model.THQEQBJBModel;

import com.mii.helpers.BasicDaoImplement;

public class THEQBJBDaoImpl extends BasicDaoImplement implements THEQBJBDao{

	@SuppressWarnings("unchecked")
	@Override
	public List<THQEQBJBModel> getTHList(int limitTH) {
		List<THQEQBJBModel> result = new ArrayList<THQEQBJBModel>();
		result = (List<THQEQBJBModel>) getCurrentSession().getNamedQuery("THEQBJB#getListTH")
				.setMaxResults(limitTH).list();
		return result;
	}
	
	@Override
	public void deleteAquiredData(List<BigDecimal> idList) {
		if(idList.size()>0){
			//Delete 3rd table
			getCurrentSession().getNamedQuery("THEQBJB#deleteAquiredData")
			.setParameterList("idList", idList) 
			.executeUpdate();
			//Delete 1st table
			getCurrentSession().getNamedQuery("THEQBJB#deleteAquiredDataSecond")
			.setParameterList("idList", idList) 
			.executeUpdate();
		}
	}
	
}
