package com.mii.dao;

import com.mii.helpers.BasicDaoImplement;

import mii.span.doc.MdrCfgMain;

public class MdrCfgMainDaoImpl extends BasicDaoImplement implements MdrCfgMainDao {

	@Override
	public MdrCfgMain getByKeyName(String keyName) {
		return (MdrCfgMain) get(MdrCfgMain.class, keyName);
	}

}
