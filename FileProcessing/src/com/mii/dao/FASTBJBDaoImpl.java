package com.mii.dao;

import java.math.BigInteger;
import java.util.List;

import mii.span.model.FASTGapuraAccount;

import org.hibernate.transform.ResultTransformer;

import com.mii.constant.Constants;
import com.mii.helpers.BasicDaoImplement;

public class FASTBJBDaoImpl extends BasicDaoImplement implements FASTBJBDao {

	@Override
	public int validateUserAccountBJB(String accountName, String accountNo) {
		try {
			BigInteger jml = (BigInteger) getCurrentSession()
					.getNamedQuery("FASTBJB#validateUserAccountBJB")
					.setString("accountName", accountName)
					.setString("accountNo", accountNo)
					.uniqueResult();
			return jml.intValue();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public FASTGapuraAccount validateUserAccountFASTBJB(String accountName,
			String accountNumber) {
		FASTGapuraAccount account = new FASTGapuraAccount();
		try {
			account = (FASTGapuraAccount) getCurrentSession()
					.getNamedQuery("FASTBJB#validateUserAccountFASTBJB")
					.setString("accountName", accountName)
					.setString("accountNo", accountNumber)
					.setResultTransformer(new ResultTransformer() {
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg0) {
							FASTGapuraAccount acc = new FASTGapuraAccount(
									String.valueOf(row[0]), 
									String.valueOf(row[1]));
							return acc;
						}

						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).uniqueResult();
			if (account == null) {
				account = new FASTGapuraAccount();
				account.setCount("0");
				account.setIsValid(Constants.FAST_NOT_FOUND);
			}
			return account;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return account;
	}

}
