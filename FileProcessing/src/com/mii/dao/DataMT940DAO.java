package com.mii.dao;

import com.mii.helpers.InterfaceDAO;
import com.mii.models.DataDetailsMT940;

import java.util.List;

public interface DataMT940DAO extends InterfaceDAO{
	public List<DataDetailsMT940> getDataDetailMT940(String iBatchid, String status, String rowNum, String norek);
	public int checkStatusDetailMT940(String docDate, String checkStatus, String errorCode, String norek);
	public List<DataDetailsMT940> getDataProcessed(String rowNum, String norek);
	public void updateExecuteBs(String norek);
	public List<DataDetailsMT940> getPenihilan(String docDate, String max);
	public void updatePenihilan(String docDate, String max);
	public int getTotalPenihilan(String docDate);
}
