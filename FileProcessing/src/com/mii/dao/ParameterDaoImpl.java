package com.mii.dao;

import java.util.List;

import com.mii.helpers.BasicDaoImplement;

import mii.span.model.Parameter;

public class ParameterDaoImpl extends BasicDaoImplement implements ParameterDao {

	@Override
	public List<Parameter> getParameters() {
		return getCurrentSession().createQuery("FROM Parameter").list();
	}

}
