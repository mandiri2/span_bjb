package com.mii.dao;

import java.util.List;

import mii.span.model.SpanHostDataFailed;
import mii.span.process.model.GetSPANDataFailedLastStatusResponse;

import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.ResultTransformer;

import com.mii.helpers.BasicDaoImplement;

public class SpanHostDataFailedDaoImpl extends BasicDaoImplement implements SpanHostDataFailedDao {

	@Override
	public List<GetSPANDataFailedLastStatusResponse> getSPANDataFailedLastStatus(
			String iFileName) {
		/**
		 * select NVL(SUCCESS_STATUS,0),  NVL(RETRY_STATUS,0), 
			NVL(RETUR_STATUS,0), NVL(WAIT_STATUS,0), 
			NVL(SUM_STATUS_WAT,0), NVL(SUM_STATUS_RTY,0), NVL(SUM_STATUS_RTU,0), NVL(TOTAL_PROCESS,0), legstatus
			from SPAN_HOST_DATA_FAILED where FILE_NAME='${iFileName}'  
			and TOTAL_PROCESS=(select COUNT(*) as totalProcess from SPAN_HOST_DATA_FAILED where FILE_NAME = '${iFileName}' )
		 */
		return (List<GetSPANDataFailedLastStatusResponse>) getCurrentSession()
					.getNamedQuery("getSPANDataFailedLastStatus")
					.setString("iFileName", iFileName)
					.setResultTransformer(new ResultTransformer() {
							private static final long serialVersionUID = 1L;
							
							@Override
							public Object transformTuple(Object[] row, String[] arg1) {
								GetSPANDataFailedLastStatusResponse getSPANDataFailedLastStatusResponse = new GetSPANDataFailedLastStatusResponse();
								getSPANDataFailedLastStatusResponse.setTotalSuccess(row[0].toString());
								getSPANDataFailedLastStatusResponse.setTotalRetry(row[1].toString());
								getSPANDataFailedLastStatusResponse.setTotalRetur(row[2].toString());
								getSPANDataFailedLastStatusResponse.setTotalWait(row[3].toString());
								getSPANDataFailedLastStatusResponse.setSumStatusWat(row[4].toString());
								getSPANDataFailedLastStatusResponse.setSumStatusRty(row[5].toString());
								getSPANDataFailedLastStatusResponse.setSumStatusRtu(row[6].toString());
								getSPANDataFailedLastStatusResponse.setTotalProcess(row[7].toString());
								getSPANDataFailedLastStatusResponse.setLegstatus(row[8].toString());
								
								return getSPANDataFailedLastStatusResponse;
							}
							
							@SuppressWarnings("rawtypes")
							@Override
							public List transformList(List arg0) {
								return arg0;
							}
						})
					.list();
	}

	@Override
	public List<SpanHostDataFailed> getSCFirstData(String fileName,
			String totalProccess, String legStatus) {
		return (List<SpanHostDataFailed>)getCurrentSession().createCriteria(SpanHostDataFailed.class)
				.add(Restrictions.eq("fileName", fileName))
				.add(Restrictions.eq("totalProcess", totalProccess))
				.add(Restrictions.eq("legStatus", legStatus))
				.list();
	}

	@Override
	public void saveSpanHostDataFailed(SpanHostDataFailed spanHostDataFailed) {
		setDefaultValue(spanHostDataFailed);
		save(spanHostDataFailed);
	}

	private void setDefaultValue(SpanHostDataFailed spanHostDataFailed) {
		
		if(spanHostDataFailed!=null){
			if(spanHostDataFailed.getRetryStatus()==null){
				spanHostDataFailed.setRetryStatus(0l);
			}
			
			if(spanHostDataFailed.getReturStatus()==null){
				spanHostDataFailed.setRetryStatus(0l);
			}
			
			if(spanHostDataFailed.getSuccessStatus()==null){
				spanHostDataFailed.setSuccessStatus(0l);
			}
			
			if(spanHostDataFailed.getTotalProcess()==null){
				spanHostDataFailed.setTotalProcess(0l);
			}
			
			if(spanHostDataFailed.getWaitStatus()==null){
				spanHostDataFailed.setWaitStatus(0l);
			}
		}
		
	}

	@Override
	public SpanHostDataFailed getSpanHostDataFailed(String filename) {
		SpanHostDataFailed result = (SpanHostDataFailed) getCurrentSession()
				.getNamedQuery("SpanHostDataFailed#getSpanHostDataFailed")
				.setString("fileName", filename)
				.setMaxResults(1)
				.uniqueResult();
		return result;
	}

}
