package com.mii.dao;

import java.util.List;

import mii.span.model.SpanHostResponseCode;

import com.mii.helpers.InterfaceDAO;

public interface SpanHostResponseCodeDao extends InterfaceDAO{
	List<SpanHostResponseCode> getByRcId(String rcId);
}
