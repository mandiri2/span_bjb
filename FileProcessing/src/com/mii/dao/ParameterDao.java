package com.mii.dao;

import java.util.List;

import com.mii.helpers.InterfaceDAO;

import mii.span.model.Parameter;

public interface ParameterDao extends InterfaceDAO {
	List<Parameter> getParameters();
}
