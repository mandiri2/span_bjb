package com.mii.dao;

import java.util.List;

import mii.span.model.SpanDataValidationSP2DNo;

import com.mii.helpers.InterfaceDAO;

public interface SpanDataValidationSP2DNoDao extends InterfaceDAO{
	List<SpanDataValidationSP2DNo> getByFilename(String iFilename);
	void insertBatch(List<SpanDataValidationSP2DNo> list);
}
