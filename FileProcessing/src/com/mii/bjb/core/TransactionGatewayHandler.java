/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mii.bjb.core;

import com.json.JSONException;
import com.json.JSONObject;
import id.co.vsi.common.crypto.SymetricCryptoHandler;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import org.apache.log4j.Logger;

/**
 *
 * @author C996
 */
public class TransactionGatewayHandler {
    private static final Logger logger = Logger.getLogger(TransactionGatewayHandler.class.getName());
    final String cid = "TG-CB-00-0000017";
    final byte cEndMessageByte = 3;
    
    public String checkTH(String iP, int port, JSONObject mpi)
        throws UnknownHostException, IOException, JSONException, NoSuchAlgorithmException {
        
        String result="";
        JSONObject json_in;
        JSONObject json_out;
        ByteArrayOutputStream mybytearray = null;
        
        //tanggal untuk ke TG
        DateFormat dateFormat = new SimpleDateFormat("MMddHHmmss");
        DateFormat dateFormat2 = new SimpleDateFormat("yyyyMMddHHmmss");
        Date date = new Date();
        String dt = String.valueOf(dateFormat.format(date));
        String dt2 = String.valueOf(dateFormat2.format(date));
        
        try {
            Socket client = new Socket();
            client.connect(new InetSocketAddress(iP, port), 30000);
            System.out.println("is connected :"+client.isConnected());
            InputStream clientIn = client.getInputStream();
            OutputStream clientOut = client.getOutputStream();
            
            json_out = new JSONObject();
            json_out.put("MT", "9200");
            json_out.put("PC", "001001");
            json_out.put("PCC", "5");
            json_out.put("CC", "0001");
            json_out.put("MC", "90023");
            json_out.put("CID", cid);
            json_out.put("DT", dt2);
            json_out.put("ST", dt);
            json_out.put("FC", "TH");

            JSONObject MPI_OUT = mpi;
//            MPI_OUT.put("ZLEAN", acc_num);
//            MPI_OUT.put("ZLVTOZ", acc_num);
//            MPI_OUT.put("ZLVFRZ", acc_num);
//            MPI_OUT.put("PGNUM", acc_num);

            json_out.put("MPI", MPI_OUT);
            json_out.put("SID", "");
            json_out.put("SPPU", "");
            json_out.put("SPPW", "");
            
            System.out.println("Request Message to GSS : " + json_out.toString());
            mybytearray = new ByteArrayOutputStream();
            mybytearray.write(json_out.toString().getBytes());
            mybytearray.write(cEndMessageByte);
            
            clientOut.write(mybytearray.toByteArray());

            mybytearray = new ByteArrayOutputStream();
            byte mybyte = cEndMessageByte;

            while ((mybyte = (byte) clientIn.read()) != cEndMessageByte) {
//            while ((mybyte = (byte) clientIn.read()) != -0x01) {
                mybytearray.write(mybyte);
//                System.out.print((char) mybyte);
            }
            
            result = new String(mybytearray.toByteArray());
//            logger.info("Response Message From GSS : " + result);
            
            clientIn.close();
            clientOut.close();
            client.close();
            
        } catch (ConnectException ce) {
            result="";
            logger.error("Module Check Balance Agent Error");
            logger.error(ce.toString());
            logger.error("Cannot connect to the server.");

        } catch (IOException ie) {
            result="";
            logger.error("Module Check Balance Agent Error");
            logger.error(ie.toString());
            logger.error("I/O Error.");

        } catch (Exception e) {
            result="";
            logger.error("Module Check Balance Agent Error");
            logger.error(e.toString());
        }
        
        return result;
    }
}
