package com.mii.bjb.core;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import com.json.JSONObject;
import com.mii.constant.Constants;
import com.mii.dao.SystemParameterDAO;
import com.mii.models.SystemParameter;

import id.co.vsi.common.crypto.SymetricCryptoHandler;


public class GapuraLoopConnector {
	private static final byte C_END_MESSAGE_BYTE = 3;
	private static String GapuraIP = "";
    private static int GapuraPort = 0;

	public static String connect(byte[] request, SystemParameterDAO systemParameterDao){
        String response = "";
        
		try {
	        /*SystemParameter parameterIP = systemParameterDao.getValueParameter(Constants.GAPURA_IP).get(0);
	        String gapuraIP = parameterIP.getParam_value();
			SystemParameter parameterPORT = systemParameterDao.getValueParameter(Constants.GAPURA_PORT).get(0);
			int gapuraPORT = Integer.parseInt(parameterPORT.getParam_value());*/
			
			System.out.println("Connecting to Gapura Loop...");
			String gapuraIP = "10.6.226.160";
			int gapuraPORT = 10012;
			
			Socket client = new Socket();
            client.connect(new InetSocketAddress(gapuraIP, gapuraPORT), 30000);
            System.out.println("Connected : "+client.isConnected());
            
            response = writeReadSocket(client, request);

            System.out.println("[GAPURA LOOP]Response Message From GSS : " + response);
            return response;
		} catch (UnknownHostException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (Exception e){
			e.printStackTrace();
		}
		return null;
    }


	private static String writeReadSocket(Socket client, byte[] request) {
		// TODO Auto-generated method stub
		try {
			ByteArrayOutputStream tRequestByteStream = new ByteArrayOutputStream();
                        
            System.out.println("request : "+request);
   
            tRequestByteStream.write(request);
            client.getOutputStream().write(tRequestByteStream.toByteArray());
            
            byte tMessageByte;
            StringBuffer sb = new StringBuffer();

            while ((tMessageByte = (byte) client.getInputStream().read()) != 3) {
                sb.append((char) tMessageByte);
            }
            String response = sb.toString();
            
            return response;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}


	/*public static String hashString(int act, String input) {
		String key = "tgadapterbjb2012";
		String EncyptMode = "AES/ECB/PKCS5Padding";
	    String output = input;
	    String[] encryptMode = EncyptMode.split("/");
    
	    try {
	        SymetricCryptoHandler crypto = new SymetricCryptoHandler(encryptMode[0], encryptMode[1], encryptMode[2], key, null);
	        output = crypto.getCryptoMessage(act, input);
	    } catch (NoSuchAlgorithmException noSuchAlgorithmException) {
	        System.err.println("NoSuchAlgorithmException: " + noSuchAlgorithmException);
	    } catch (NoSuchPaddingException noSuchPaddingException) {
	        System.err.println("NoSuchPaddingException: " + noSuchPaddingException);
	    } catch (InvalidKeyException invalidKeyException) {
	        System.err.println("InvalidKeyException: " + invalidKeyException);
	    } catch (IllegalBlockSizeException illegalBlockSizeException) {
	        System.err.println("IllegalBlockSizeException: " + illegalBlockSizeException);
	    } catch (BadPaddingException badPaddingException) {
	        System.err.println("BadPaddingException: " + badPaddingException);
	    } catch (InvalidAlgorithmParameterException invalidAlgorithmParameterException) {
	        System.err.println("InvalidAlgorithmParameterException: " + invalidAlgorithmParameterException);
	    }
	    return output;
	}*/
        
	}
