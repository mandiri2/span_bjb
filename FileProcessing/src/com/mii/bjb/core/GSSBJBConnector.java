/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mii.bjb.core;

import com.google.gson.Gson;
import com.json.JSONObject;
import com.mii.constant.Constants;
import com.mii.constant.JsonConstant;
import com.mii.dao.SystemParameterDAO;
import com.mii.json.TransactionHistory;
import com.mii.json.model.BaseMessage;
import com.mii.models.SystemParameter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import mii.span.model.SpanRekeningKoran;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

/**
 *
 * @author C996
 */
public class GSSBJBConnector {

    /**
     * @param args the command line arguments
     */
	/*public static void main(String[] args) {
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMyy");
		List<SpanRekeningKoran> resultList;
		try {
			resultList = sendRequestGSS(null, "0072932706001", sdf.parse("310116"), sdf.parse("310116"));
			System.out.println(resultList.size());
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}*/
	
	public static List<SpanRekeningKoran> getGSSCreditOnly(SystemParameterDAO systemParameterDao, String accountNo, Date start, Date end){
		List<SpanRekeningKoran> resultList = sendRequestGSS(systemParameterDao, accountNo, start, end);
		List<SpanRekeningKoran> resultCreditOnly = new ArrayList<SpanRekeningKoran>();
		for(SpanRekeningKoran s : resultList){
			if (StringUtils.isEmpty(s.getCredit())) {
				continue;
			} else {
				resultCreditOnly.add(s);
			}
		}
		return resultCreditOnly;
	}
	
    public static List<SpanRekeningKoran> sendRequestGSS(SystemParameterDAO systemParameterDao, String accountNo, Date start, Date end){
		
        String GapuraIP = systemParameterDao.getValueByParamName(Constants.GSS_IP);
        int GapuraPort = Integer.parseInt(systemParameterDao.getValueByParamName(Constants.GSS_PORT));
//    	String GapuraIP = "10.6.226.160";
//		int GapuraPort = 10012;
    	
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyy");
        
        try {
        	List<SpanRekeningKoran> resultList = new ArrayList<SpanRekeningKoran>();
        	String respCode = "0000";
        	int i = 1;
        	int count = 1;
        	while(respCode.equals("0000")){
        		List<SpanRekeningKoran> resultTemp;
                JSONObject mpi = new JSONObject();
                mpi.put("ZLEAN", accountNo);
                mpi.put("ZLVTOZ", sdf.format(end));
                mpi.put("ZLVFRZ", sdf.format(start));
                mpi.put("PGNUM", Integer.toString(i));
                TransactionGatewayHandler tgh = new TransactionGatewayHandler();
                String result = tgh.checkTH(GapuraIP, GapuraPort, mpi);
                System.out.println("Response : "+result);
                Gson gson = new Gson();
        		BaseMessage base = gson.fromJson(result, BaseMessage.class);
        		respCode = base.getRC();
        		System.out.println("rc : "+respCode);
                resultTemp = TransactionHistory.unpackTransactionHistory(result);
                resultList.addAll(resultTemp);
                System.out.println("Iterate ke - "+count++);
                if(i<3){
                    i++;
                }
        	}
            return resultList;
        } catch (Exception ex) {
        	ex.printStackTrace();
        	return null;
        }
    }
}
