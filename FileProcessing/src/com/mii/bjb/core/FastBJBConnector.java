package com.mii.bjb.core;

import mii.span.model.FASTGapuraAccount;
import mii.span.util.BusinessUtil;

import com.mii.constant.CommonConstant;
import com.mii.dao.FASTBJBDao;
import com.mii.dao.SystemParameterDAO;
import com.mii.json.AccountBalance;

public class FastBJBConnector {
	public static String CheckAccountBJB(String accountName, String accountNumber, SystemParameterDAO systemParameterDao) {
		FASTBJBDao fastBJBDao = (FASTBJBDao) BusinessUtil.getDao("fASTBJBDao");
		int count = fastBJBDao.validateUserAccountBJB(accountName, accountNumber);
		if (count > 0) {
			return CommonConstant.FAST_FOUND;
		} else {
			if (checkAccountAndName(systemParameterDao, accountName, accountNumber)) {
				return CommonConstant.FAST_FOUND;
			} else {
				return CommonConstant.FAST_NOT_FOUND;
			}
		}
	}
	
	public static FASTGapuraAccount CheckAccountFASTBJB(String accountName, String accountNumber, SystemParameterDAO systemParameterDao) {
		FASTGapuraAccount account = new FASTGapuraAccount();
		FASTBJBDao fastBJBDao = (FASTBJBDao) BusinessUtil.getDao("fASTBJBDao");
		account = fastBJBDao.validateUserAccountFASTBJB(accountName, accountNumber);
		if (Integer.parseInt(account.getCount()) > 0) {
			return account;
		} else {
			account = checkAccountAndNameGapura(systemParameterDao, accountName, accountNumber);
			return account;
		}
	}
	
	public static boolean checkAccountAndName(SystemParameterDAO systemParameterDao, String accountName, String  accountNumber){
		return AccountBalance.checkAccountAndName(systemParameterDao, accountNumber, accountName);
	}
	
	public static FASTGapuraAccount checkAccountAndNameGapura(SystemParameterDAO systemParameterDao, String accountName, String  accountNumber){
		return AccountBalance.checkAccountAndNameGapura(systemParameterDao, accountNumber, accountName);
	}
}
