/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mii.bjb.core;

import com.json.JSONObject;
import java.util.Iterator;
import org.apache.log4j.Logger;

/**
 *
 * @author C996
 */
public class TestTH {
    
    private static String GapuraIP = "";
    private static int GapuraPort = 0;
    private static final Logger logger = Logger.getLogger(TestTH.class.getName());
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        GapuraIP = "10.6.226.160";
        GapuraPort = 10012;
        
        logger.info("GAPURA IP : " + GapuraIP);
        logger.info("GAPURA PORT : " + GapuraPort);
        try {
            JSONObject mpi = new JSONObject();
            mpi.put("ZLEAN", "0072932706001");
//            mpi.put("ZLEAN", "0026643155100");
            mpi.put("ZLVTOZ", "311016");
            mpi.put("ZLVFRZ", "011016");
            mpi.put("PGNUM", "1");
            TransactionGatewayHandler tgh = new TransactionGatewayHandler();
            String result = tgh.checkTH(GapuraIP, GapuraPort, mpi);
            logger.info("Response Message From GSS : " + result);
            System.out.println("Response : "+result);
            JSONObject json_in = new JSONObject(result);
            JSONObject MPO_IN = json_in.getJSONObject("MPO");
            Iterator<String> itrOutput = MPO_IN.keys();
            String key = "";
            String value = "";
            while (itrOutput.hasNext()) {
                key = itrOutput.next();
                value = MPO_IN.getString(key);
                logger.info(key + " = " + value);
            }
        } catch (Exception ex) {
            logger.error("== Main Program Error ==");
            logger.error(ex.toString());
            logger.error("Run The App Server Again....!!!");
            System.exit(0);
        }
    }
}
