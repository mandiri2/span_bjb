package com.mii.util;

import java.io.File;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import javax.faces.bean.ManagedProperty;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import com.mii.constant.CommonConstant;
import com.mii.dao.DataBPSXDAO;
//import com.mii.dao.DataMT940DAO;
import com.mii.dao.SystemParameterDAO;
import com.mii.logger.Log;
import com.mii.models.DataDetailBPSX;
import com.mii.models.DataHeaderBPSX;
import com.mii.models.SystemParameter;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.ApplicationInstance;

public class ExtractUtils implements Serializable{
	private static final long serialVersionUID = 1L;
	@ManagedProperty(value = "#{applicationBean}")
	private ApplicationBean applicationBean;
	private static Log log;
	private DataDetailBPSX ddb;
	private DataHeaderBPSX dhb;
	private List<DataDetailBPSX> listData;
	private SessionFactory sessionFactory;
	
	public ExtractUtils(){
		ddb = new DataDetailBPSX(null, null, null, null, null, null, null, null, null, null);
		dhb = new DataHeaderBPSX();
		applicationBean = ApplicationInstance.getInstance().getApplicationBean();
		log = Log.getInstance(ExtractUtils.class);
	}
	
	public String doExtractMorning(String sourcePath, String eventFile){
		ExtractUtils ext = new ExtractUtils();
		String extractMorningStatus = "";
		String pattern = ext.getParameter("PATTERN_FILE_MT940");
		String destPath = ext.getParameter("BACKUP_LOCAL_PATH_BPSX_MORNING");
		String beneficiaryBankCode = ext.getParameter("BANK_CODE");
		String mtFilename = "";
		String sourcePathFile = "";
		String destPathFile = "";
		String batchid = "";
		
		
		try{
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = new Date();
			
			File fil = new File(sourcePath);
			File[] files = fil.listFiles(new FindFile(pattern));
			for (File file : files){
				//log.debug("Extract File "+file.getName()+" start time = "+dateFormat.format(date));
				System.out.println("Extract File "+file.getName()+" start time = "+dateFormat.format(date));
			    mtFilename = file.getName();
			    sourcePathFile = sourcePath.concat(file.getName());
			    destPathFile = destPath.concat(file.getName());
			    //run extract file
			    extractMorningStatus = ext.doExtractFile(sourcePathFile, mtFilename, eventFile);
			    System.out.println("Status extract morning = "+extractMorningStatus);
			    
			    if ("SUCCESS".equalsIgnoreCase(extractMorningStatus)){
			    	//log.debug("Status extract is "+statusExtract);
			    	
			    	Date date3 = new Date();
			    	System.out.println("Start Insert Header "+file.getName()+" start time = "+dateFormat.format(date3));
			    	//insert data header to database
			    	List<DataHeaderBPSX> listHeader = ext.doInsertHeaderBPSX(mtFilename, beneficiaryBankCode, ext.dhb.getBankAcctNo(), ext.dhb.getBankStatementDt(), 
							ext.dhb.getCurrency(), ext.dhb.getBeginBalance(), ext.dhb.getTotalRecord(), ext.dhb.getCloseBalance(), ext.dhb.getBpsxFileEvent(),
							ext.dhb.getInsertTimeLog());
			    	for(DataHeaderBPSX hd : listHeader){
			    		batchid = hd.getBatchid();
			    	}

			    	List<DataDetailBPSX> listData = ext.listData;

			    	ext.batchInsert(listData, batchid);

			    	//move file to backup
			    	MoveFile mf = new MoveFile();
			    	String moveStatus = mf.doMoveFile(sourcePathFile, destPathFile);
			    	log.debug("Status move file from "+sourcePathFile+" to "+destPathFile+" is "+moveStatus);
			    	System.out.println("Status move file from "+sourcePathFile+" to "+destPathFile+" is "+moveStatus);
			    	
			    	//update header 4 status
			    	ext.updateHeaderLog(batchid, "0", "4", "Data ready to be validated");
			    }else{
			    	log.debug("Failed to extract file =====> "+sourcePathFile);
			    	System.out.println("Failed to extract file =====> "+sourcePathFile);
			    }
			    Date date2 = new Date();
			    //log.debug("Extract File "+file.getName()+" finish time = "+dateFormat.format(date2));
			    System.out.println("Extract File "+file.getName()+" finish time = "+dateFormat.format(date2));
			}
			
		}catch(Exception e){
			extractMorningStatus = "FAILED";
			log.error(e.getMessage());
			e.printStackTrace();
		}
		
		return extractMorningStatus;
	}
	
	public String doExtractNoon(String sourcePath, String eventFile){
		ExtractUtils ext = new ExtractUtils();
		String extractNoonStatus = "";
		String pattern = ext.getParameter("PATTERN_FILE_MT940");
		String destPath = ext.getParameter("BACKUP_LOCAL_PATH_BPSX_NOON");
		String beneficiaryBankCode = ext.getParameter("BANK_CODE");
		String mtFilename = "";
		String sourcePathFile = "";
		String destPathFile = "";
		String batchid = "";
		
		
		try{
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = new Date();
			
			File fil = new File(sourcePath);
			File[] files = fil.listFiles(new FindFile(pattern));
			for (File file : files){
				//log.debug("Extract File "+file.getName()+" start time = "+dateFormat.format(date));
				System.out.println("Extract File "+file.getName()+" start time = "+dateFormat.format(date));
			    mtFilename = file.getName();
			    sourcePathFile = sourcePath.concat(file.getName());
			    destPathFile = destPath.concat(file.getName());
			    //run extract file
			    extractNoonStatus = ext.doExtractFile(sourcePathFile, mtFilename, eventFile);
			    
			    if ("SUCCESS".equalsIgnoreCase(extractNoonStatus)){
			    	
			    	List<DataHeaderBPSX> listHeader = ext.doInsertHeaderBPSX(mtFilename, beneficiaryBankCode, ext.dhb.getBankAcctNo(), ext.dhb.getBankStatementDt(), 
							ext.dhb.getCurrency(), ext.dhb.getBeginBalance(), ext.dhb.getTotalRecord(), ext.dhb.getCloseBalance(), ext.dhb.getBpsxFileEvent(),
							ext.dhb.getInsertTimeLog());
			    	for(DataHeaderBPSX hd : listHeader){
			    		batchid = hd.getBatchid();
			    	}

			    	List<DataDetailBPSX> listData = ext.listData;

			    	ext.batchInsert(listData, batchid);

			    	//move file to backup
			    	MoveFile mf = new MoveFile();
			    	String moveStatus = mf.doMoveFile(sourcePathFile, destPathFile);
			    	log.debug("Status move file from "+sourcePathFile+" to "+destPathFile+" is "+moveStatus);
			    	System.out.println("Status move file from "+sourcePathFile+" to "+destPathFile+" is "+moveStatus);
			    	
			    	//update header 4 status
			    	ext.updateHeaderLog(batchid, "0", "4", "Data ready to be validated");
			    }else{
			    	log.debug("Failed to extract file =====> "+sourcePathFile);
			    	System.out.println("Failed to extract file =====> "+sourcePathFile);
			    }
			    Date date2 = new Date();
			    //log.debug("Extract File "+file.getName()+" finish time = "+dateFormat.format(date2));
			    System.out.println("Extract File "+file.getName()+" finish time = "+dateFormat.format(date2));
			}
			
		}catch(Exception e){
			extractNoonStatus = "FAILED";
			log.error(e.getMessage());
			e.printStackTrace();
		}
		
		return extractNoonStatus;
	}
	
	public String doExtractFile(String filePath, String mtFilename, String bpsxFileEvent) throws Exception{
		String extractStatus = "";
		String reader = "";
		String header = "";
		String detail = "";
		String footer = "";
		int readerLength = 0;
		String countLine = "";
		String bankTrxCode = "";
		String debitCredit = "";
		String bankRefNo = "";
		String trxDate = "";
		String valueDate = "";
		String originalAmount = "";
		
		try{
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = new Date();
			String insertTimeLog = sdf.format(date);
			Scanner scan = new Scanner(new File(filePath));
			scan.useDelimiter(System.getProperty(CommonConstant.lineSeparator));
			List<DataDetailBPSX> list = new ArrayList<DataDetailBPSX>();
			ExtractUtils et = new ExtractUtils();
			
			while(scan.hasNext()){
				reader = et.checkRecordBPS(scan.next());
				//System.out.println("Reader = ["+reader+"]");
				readerLength = reader.length();
				//System.out.println("Reader Length = "+readerLength);
				
				if (readerLength == 46){
					header = reader;
					//System.out.println("header "+header);
				}else if(readerLength == 57){
					detail = reader;
					//extract detail data
					countLine = detail.substring(0, 6).replaceFirst("^0+(?!$)", "").trim();
					bankTrxCode = detail.substring(6, 9).trim();
					if(CommonConstant.CreditViaTellerTrxCodeEq.equalsIgnoreCase(bankTrxCode)){
						bankTrxCode = CommonConstant.CreditViaTellerTrxCode;
					}else if(CommonConstant.CreditViaATMTrxCodeEq.equalsIgnoreCase(bankTrxCode)){
						bankTrxCode = CommonConstant.CreditViaATMTrxCode;
					}else if(CommonConstant.PelimpahanTrxCodeEq.equalsIgnoreCase(bankTrxCode)){
						bankTrxCode = CommonConstant.PelimpahanTrxCode;
					}else if(CommonConstant.DebitViaTellerTrxCodeEq.equalsIgnoreCase(bankTrxCode)){
						bankTrxCode = CommonConstant.DebitViaTellerTrxCode;
					}else if(CommonConstant.DebitViaATMTrxCodeEq.equalsIgnoreCase(bankTrxCode)){
						bankTrxCode = CommonConstant.DebitViaATMTrxCode;
					}
					debitCredit = detail.substring(9, 10).trim();
					if(CommonConstant.PelimpahanTrxCode.equalsIgnoreCase(bankTrxCode)){
						bankRefNo = detail.substring(10, 26).trim();
					}else{
						bankRefNo = detail.substring(10, 26).replaceFirst("^0+(?!$)", "").trim();
					}
					trxDate = detail.substring(26, 34).trim();
					valueDate = detail.substring(34, 42).trim();
					originalAmount = detail.substring(42, 57).replaceFirst("^0+(?!$)", "").trim();
					
					list.add(new DataDetailBPSX(null,null,countLine, bankTrxCode, debitCredit, bankRefNo, trxDate, valueDate, originalAmount, bpsxFileEvent));
					
					ddb =  setDataDetail(null,countLine, bankTrxCode, debitCredit, bankRefNo, trxDate, valueDate, originalAmount, bpsxFileEvent);
					
				}else if(readerLength == 22){
					footer = reader;
					//System.out.println("Footer = "+footer);
				}else{
					
					break;
				}
			}
			scan.close();
			
				
			//extract the header
			String accountNo = header.substring(0, 20).trim();
			String bankStatementDt = header.substring(20, 28).trim();
			String currency = header.substring(28, 31).trim();
			String beginBalance = header.substring(31, 46).replaceFirst("^0+(?!$)", "").trim();
			
			//extract the footer
			String totalData = footer.substring(0, 6).trim();
			String endingBalance = footer.substring(6, 21).replaceFirst("^0+(?!$)", "").trim();
			
			dhb = setDataHeader(accountNo, bankStatementDt, currency, beginBalance, totalData, endingBalance, bpsxFileEvent, insertTimeLog);
			
			listData = setListDataDetail(list);
		
		extractStatus = "SUCCESS";
		
		}catch(Exception e){
			extractStatus = "FAILED";
			log.error(e.getMessage());
			e.printStackTrace();
		}
		
		return extractStatus;
	}
	
	public void updateHeaderLog(String batchid, String status, String setStatus, String desc){
		//ApplicationBean ap = new ApplicationBean();
		//ap.postConstruct();
		DataBPSXDAO mtDao = (DataBPSXDAO) applicationBean.getFileProcessContext().getBean("dataHeaderBPSXDAO");
		mtDao.updateDataHeaderStatus(batchid, status, setStatus, desc);
	}
	
	public String getParameter(String param_name){
		List<SystemParameter> params = getParameterbyID(param_name);
		String param = "";
		for(SystemParameter sp : params){
			param = sp.getParam_value();
		}
		return param;
	}
	
	public String checkRecordBPS(String text){
		String modifiedText = "";
		String temp = "";
			try{
				temp = text.substring(0, 1);
				if(temp.replaceFirst(" ", "*").equalsIgnoreCase("*")){
					modifiedText = text.substring(0, 22);
				}else{
					modifiedText = text.trim();
				}

			}catch(Exception e){
				e.printStackTrace();
			}
		return modifiedText;
	}
	
	private List<SystemParameter> getParameterbyID(String param_name){
		SystemParameterDAO sysDAO = (SystemParameterDAO) applicationBean.getFileProcessContext().getBean("sysParamDAO");
		List<SystemParameter> params = new ArrayList<SystemParameter>(sysDAO.getValueParameter(param_name));
		return params;
	}
	
	public List<DataHeaderBPSX> doInsertHeaderBPSX(String mtFilename,String bankCode, String accountNo, String bankStatementDt,
			String currency, String beginBalance, String totalData, String endingBalance, String bpsxFileEvent, String insertTimeLog){
		
		DataBPSXDAO mtDao = (DataBPSXDAO) applicationBean.getFileProcessContext().getBean("dataHeaderBPSXDAO");
		List<DataHeaderBPSX> headerList = new ArrayList<DataHeaderBPSX>(mtDao.insertDataHeader(mtFilename, bankCode, accountNo, bankStatementDt, 
				currency, beginBalance, totalData, endingBalance, bpsxFileEvent, insertTimeLog));
		return headerList;
	}
	
	public DataHeaderBPSX setDataHeader(String accountNo, String bankStatementDt, String currency, String beginBalance, 
			String totalData, String endingBalance, String bpsxFileEvent, String insertTimeLog){
		DataHeaderBPSX ddh = new DataHeaderBPSX();
		ddh.setBankAcctNo(accountNo);
		ddh.setBankStatementDt(bankStatementDt);
		ddh.setCurrency(currency);
		ddh.setBeginBalance(beginBalance);
		ddh.setTotalRecord(totalData);
		ddh.setCloseBalance(endingBalance);
		ddh.setBpsxFileEvent(bpsxFileEvent);
		ddh.setInsertTimeLog(insertTimeLog);
		
		return ddh;
	}
	
	public DataDetailBPSX setDataDetail(String batchid, String countLine, String bankTrxCode, String debitCredit, String bankRefNo, String trxDate, 
			String valueDate, String originalAmount, String reqEvent){
		
		DataDetailBPSX dde = new DataDetailBPSX(null, null, null, null, null, null, null, null, null, null);
		dde.setId(countLine);
		dde.setDetailBatchid(batchid);
		dde.setCountLine(countLine);
		dde.setBankTrxCode(bankTrxCode);
		dde.setDebitCredit(debitCredit);
		dde.setBankReffNo(bankRefNo);
		dde.setTrxDate(trxDate);
		dde.setValueDate(valueDate);
		dde.setOriginalAmount(originalAmount);
		dde.setReqEvent(reqEvent);
		
		return dde;
	}
	
private void batchInsert(List<DataDetailBPSX> listDataDetails, String batchid){
		
		try{	
		sessionFactory = (SessionFactory) applicationBean.getFileProcessContext().getBean("laSessionFactoryKu");	
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		int i = 0;   
		for(DataDetailBPSX dm : listDataDetails){
		    DataDetailBPSX detail = new DataDetailBPSX(dm.getCountLine(), batchid, dm.getCountLine(), 
		    		dm.getBankTrxCode(), dm.getDebitCredit(), dm.getBankReffNo(), dm.getTrxDate(), dm.getValueDate(), 
		    		dm.getOriginalAmount(), dm.getReqEvent());
		    session.save(detail);
		    if ( i % 20 == 0 ) { //20, same as the JDBC batch size
		        //flush a batch of inserts and release memory:
		        session.flush();
		        session.clear();
		    }
		    i++;
		}
		   
		tx.commit();
		session.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}

	public List<DataDetailBPSX> setListDataDetail(List<DataDetailBPSX> listDataDetails){
		listData = listDataDetails;
		return listData;
	}
	
	public DataDetailBPSX getDdb() {
		return ddb;
	}

	public void setDdb(DataDetailBPSX ddb) {
		this.ddb = ddb;
	}

	public DataHeaderBPSX getDhb() {
		return dhb;
	}

	public void setDhb(DataHeaderBPSX dhb) {
		this.dhb = dhb;
	}
	
}
