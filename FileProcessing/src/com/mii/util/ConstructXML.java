package com.mii.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.faces.bean.ManagedProperty;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import mii.span.model.SpanRekeningKoran;
import mii.span.scheduller.odbc.GenerateBankStatement;
import mii.span.util.BusinessUtil;
import mii.span.util.PubUtil;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.mii.constant.Constants;
import com.mii.logger.Log;
import com.mii.models.DataDetailsMT940;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.ApplicationInstance;
import com.mii.scoped.SessionBean;
import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;

public class ConstructXML implements Serializable{
	private static final long serialVersionUID = 1L;
	@ManagedProperty(value="#{applicationBean}")
	private ApplicationBean applicationBean;
	@ManagedProperty(value="#{sessionBean}")
	private SessionBean sessionBean;
	private static Log log = Log.getInstance(ConstructXML.class);
	
	private static final String DEBIT = "DEBIT";
	private static final String CREDIT = "CREDIT";
	private static final String PENIHILAN = "PENIHILAN";
	
	public ConstructXML() {
		applicationBean = ApplicationInstance.getInstance().getApplicationBean();
	}
	
	private long generateLineAreaElement(Element rootElement, Document doc, int lineArea, String typeBs, String bankReffNo,
			String trxDate, String valueDate, String originalAmount, long totalAmt, String accountType, String transactionCode){
		Element LineArea = doc.createElement("LineArea");
        rootElement.appendChild(LineArea);
        
        	//Line number element
        	String countRow = null;
        	String bankTrxCode = null;
        	String debitCredit = null;
        	
        	if(lineArea==0){
        		countRow = "";
        	}
        	else{
        		countRow = String.valueOf(lineArea);
        	}
        	
        	if(typeBs.equals(ConstructXML.CREDIT)){
        		bankTrxCode = getTrxCodeForDropping(transactionCode, accountType);
        		debitCredit = Constants.CREDIT_CODE;;
        		trxDate = PubUtil.dateTimeFormat(trxDate.trim(), "ddMMMyy", "yyyy-MM-dd");
        		valueDate = trxDate;
        		originalAmount = sanitizeOriginalAmountCredit(originalAmount);
        	}
        	else if (typeBs.equals(ConstructXML.DEBIT)){
        		bankTrxCode =Constants.DEFAULT_TRANSACTION_CODE_DEBIT;
        		debitCredit = Constants.DEBIT_CODE;;
        	}
        	else if(typeBs.equals(ConstructXML.PENIHILAN)){
        		bankTrxCode = Constants.DEFAULT_TRANSACTION_CODE_PENIHILAN;
        		debitCredit = Constants.DEBIT_CODE;
        	}
        	else{
        		bankTrxCode = "";
        		debitCredit = "";
        	}
        	
        	Element LineNumber = doc.createElement("LineNumber");
        	LineNumber.appendChild(doc.createTextNode(String.valueOf(countRow)));
        	LineArea.appendChild(LineNumber);
        	
        	//BankTrxCode element
        	Element BankTrxCode = doc.createElement("BankTransactionCode");
//        	BankTrxCode.appendChild(doc.createTextNode(String.valueOf(dList.getBankTrxCode())));
        	BankTrxCode.appendChild(doc.createTextNode(bankTrxCode));
        	LineArea.appendChild(BankTrxCode);
        	
        	//DebitCredit element
        	Element DebitCredit = doc.createElement("DebitCredit");
        	DebitCredit.appendChild(doc.createTextNode(debitCredit));
        	LineArea.appendChild(DebitCredit);
        	
        	//BankReferenceNumber element
        	Element BankReferenceNumber = doc.createElement("BankReferenceNumber");
        	BankReferenceNumber.appendChild(doc.createTextNode(bankReffNo));
        	LineArea.appendChild(BankReferenceNumber);
        	
        	//TransactionDate element
        	Element TransactionDate = doc.createElement("TransactionDate");
        	TransactionDate.appendChild(doc.createTextNode(trxDate));
        	LineArea.appendChild(TransactionDate);
        	
        	//ValueDate element
        	Element ValueDate = doc.createElement("ValueDate");
        	ValueDate.appendChild(doc.createTextNode(valueDate));
        	LineArea.appendChild(ValueDate);
        	
        	//OriginalAmount element
        	Element OriginalAmount = doc.createElement("OriginalAmount");
        	if(StringUtils.isEmpty(countRow)){
        		OriginalAmount.appendChild(doc.createTextNode(""));
        	}else{
        		OriginalAmount.appendChild(doc.createTextNode(BusinessUtil.convertAmount(originalAmount)));
        	}
        	LineArea.appendChild(OriginalAmount);
        	
        	//count Amount
        	if(StringUtils.isEmpty(originalAmount)){
        		originalAmount = "0";
        	}
        	long tempAmt = Long.parseLong(originalAmount);
        	
        	//New Enhance for calculate total amount footer BS
        	if(typeBs.equals(ConstructXML.CREDIT)){
        		totalAmt = totalAmt + tempAmt;
        	}
        	
        	return totalAmt;
	}

	private static String getTrxCodeForDropping(String transactionCode, String accountType) {
		String bankTrxCode = Constants.DEFAULT_TRANSACTION_CODE_CREDIT;
		try{
			if(accountType.equals(Constants.GAJI)){
				if(transactionCode.equals(Constants.DEFAULT_TRANSACTION_CODE_DROPPING)){
					return transactionCode;
				}else{
					return bankTrxCode;
				}
			}else{
				return bankTrxCode;
			}
		}catch(Exception e){
			System.out.println("[ERROR] GSS Return Unknown Value or Account Type is Null.");
			return bankTrxCode;
		}
	}
	
	private String sanitizeOriginalAmountCredit(String originalAmount) {
		if(StringUtils.isNotEmpty(originalAmount)){
			originalAmount = originalAmount.trim().replace(",", "").replace("CR", "");
		}
		return originalAmount;
	}

	public String doConstruct(List<SpanRekeningKoran> credit, int totalPenihilan, String mtFilename, String bankCode, String bankAcctNo,
			String bankStatementDT, String currency, String beginBalance, String closeBalance, String xmlPath, 
			Map<String, Object>mapLineArea, int maxPerPage, String lastDocDate, int totalDebit, String accountType) throws IOException{
		
		int totalRowPerBs = 0;
		boolean isEmptyList = true;
		int lineArea = (Integer) mapLineArea.get(Constants.LINE_AREA_ROW);
		long totalAmt = (Long) mapLineArea.get(Constants.TOTAL_AMOUNT_CREDIT);
		String statusConstruct = "";
		String root = "BANK_STATEMENT";
		int sisa = 0;
		FileOutputStream fileOutputStream = null;
		try{
			ConstructXML cx = new ConstructXML();
		    DateFormat newSdf = new SimpleDateFormat("yyyy-MM-dd");
		    Date date3 = null;
			
			System.out.println("Generate Bank Statement of "+mtFilename);
			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();

			// BANK STATEMENT elements
			Document doc = documentBuilder.newDocument();
			Element rootElement = doc.createElement(root);
			doc.appendChild(rootElement);

			// ApplicationArea elements
			Element ApplicationArea = doc.createElement("ApplicationArea");
			rootElement.appendChild(ApplicationArea);

				// ApplicationAreaSenderIdentifier elements
				Element ApplicationAreaSenderIdentifier = doc.createElement("ApplicationAreaSenderIdentifier");
				ApplicationAreaSenderIdentifier.appendChild(doc.createTextNode("BJB"));
				ApplicationArea.appendChild(ApplicationAreaSenderIdentifier);

				// ApplicationAreaReceiverIdentifier elements
				Element ApplicationAreaReceiverIdentifier = doc.createElement("ApplicationAreaReceiverIdentifier");
				ApplicationAreaReceiverIdentifier.appendChild(doc.createTextNode("SPAN"));
				ApplicationArea.appendChild(ApplicationAreaReceiverIdentifier);
		
				// ApplicationAreaCreationDateTime elements
				DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd:HHmmss");
				Date date = new Date();
				String createDate = dateFormat.format(date);
				Element ApplicationAreaCreationDateTime = doc.createElement("ApplicationAreaCreationDateTime");
				ApplicationAreaCreationDateTime.appendChild(doc.createTextNode(createDate));
				ApplicationArea.appendChild(ApplicationAreaCreationDateTime);

				// ApplicationAreaMessageIdentifier elements
				Element ApplicationAreaMessageIdentifier = doc.createElement("ApplicationAreaMessageIdentifier");
				ApplicationAreaMessageIdentifier.appendChild(doc.createTextNode(mtFilename));
				ApplicationArea.appendChild(ApplicationAreaMessageIdentifier);
				
				// ApplicationAreaMessageTypeIndicator elements
				Element ApplicationAreaMessageTypeIndicator = doc.createElement("ApplicationAreaMessageTypeIndicator");
				ApplicationAreaMessageTypeIndicator.appendChild(doc.createTextNode("BS"));
				ApplicationArea.appendChild(ApplicationAreaMessageTypeIndicator);
				
				// ApplicationAreaMessageVersionText elements
				Element ApplicationAreaMessageVersionText = doc.createElement("ApplicationAreaMessageVersionText");
				ApplicationAreaMessageVersionText.appendChild(doc.createTextNode("1.0"));
				ApplicationArea.appendChild(ApplicationAreaMessageVersionText);
				
			// HeaderArea elements
			Element HeaderArea = doc.createElement("HeaderArea");
			rootElement.appendChild(HeaderArea);
			
				// BankCode elements
				Element BankCode = doc.createElement("BankCode");
				BankCode.appendChild(doc.createTextNode(bankCode));
				//BankCode.appendChild(doc.createTextNode("520007778687"));
				HeaderArea.appendChild(BankCode);
				
				// BankAccountNumber elements
				Element BankAccountNumber = doc.createElement("BankAccountNumber");
				BankAccountNumber.appendChild(doc.createTextNode(bankAcctNo));
				//BankAccountNumber.appendChild(doc.createTextNode("11908089977"));
				HeaderArea.appendChild(BankAccountNumber);
				
				// BankStatementDate elements
				Element BankStatementDate = doc.createElement("BankStatementDate");
				try {
					date3 = newSdf.parse(bankStatementDT);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				BankStatementDate.appendChild(doc.createTextNode(newSdf.format(date3)));
				//BankStatementDate.appendChild(doc.createTextNode("2015-11-19"));
				HeaderArea.appendChild(BankStatementDate);
				
				// Currency elements
				Element Currency = doc.createElement("Currency");
				Currency.appendChild(doc.createTextNode(currency));
				//Currency.appendChild(doc.createTextNode("IDR"));
				HeaderArea.appendChild(Currency);
				
				// BeginningBalance elements
				Element BeginningBalance = doc.createElement("BeginningBalance");
				//BeginningBalance.appendChild(doc.createTextNode(beginBalance));
				BeginningBalance.appendChild(doc.createTextNode(cx.convertAmount(beginBalance)));
				HeaderArea.appendChild(BeginningBalance);
				
				// EndingBalance elements
				Element EndingBalance = doc.createElement("EndingBalance");
				//EndingBalance.appendChild(doc.createTextNode(closeBalance));
				EndingBalance.appendChild(doc.createTextNode(cx.convertAmount(closeBalance)));
				HeaderArea.appendChild(EndingBalance);
			
			//line transaction history credit
			if(!BusinessUtil.isListEmpty(credit)){
				isEmptyList=false;
				sisa = getMaxPerPage(credit.size(), maxPerPage);
				List<SpanRekeningKoran> tempSpanRekeningKoran = new ArrayList<SpanRekeningKoran>();
				for(int i=0; i<sisa;i++){
					lineArea++;
					totalRowPerBs++;
					SpanRekeningKoran spanRekeningKoran = credit.get(i);
					totalAmt = cx.generateLineAreaElement(rootElement, doc, lineArea, ConstructXML.CREDIT, spanRekeningKoran.getBankReferenceNumber(), 
							spanRekeningKoran.getTransactionDate(), spanRekeningKoran.getTransactionDate(), 
							spanRekeningKoran.getCredit(), totalAmt, accountType, spanRekeningKoran.getBankTransactionCode());
					maxPerPage--;
					tempSpanRekeningKoran.add(spanRekeningKoran);
				}
				for(SpanRekeningKoran spanRekeningKoran : tempSpanRekeningKoran){
					credit.remove(spanRekeningKoran);
				}
				
			}
			
			//Line Area Elements debit
			if(maxPerPage>0
					&& totalDebit > 0){
				List<DataDetailsMT940> detailListMT940 = GenerateBankStatement.getDataDetailMT940List(lastDocDate, "0", 
						String.valueOf(maxPerPage), bankAcctNo);
				BusinessUtil.logWithSysout(log, PubUtil.concat("Start time mapping data BS = ", dateFormat.format(new Date())," with size detail list = "+detailListMT940.size()));
				if(!BusinessUtil.isListEmpty(detailListMT940)){
					isEmptyList=false;
					for(DataDetailsMT940 mt940 : detailListMT940){
						lineArea++;
						totalRowPerBs++;
						cx.generateLineAreaElement(rootElement, doc, lineArea, ConstructXML.DEBIT, mt940.getBankReffNo(), 
								mt940.getTrxDate(), mt940.getValueDate(), mt940.getOriginalAmount(), totalAmt, accountType, "");
						maxPerPage--;
					}
				}
			}
			
			//Line area penihilan
			if(maxPerPage > 0 
					&& totalPenihilan>0){
				List<DataDetailsMT940> penihilan = GenerateBankStatement.getPenihilan(lastDocDate, String.valueOf(maxPerPage));
				if(!BusinessUtil.isListEmpty(penihilan)){
					isEmptyList=false;
					for(DataDetailsMT940 nihil : penihilan){
						lineArea++;
						totalRowPerBs++;
						cx.generateLineAreaElement(rootElement, doc, lineArea, ConstructXML.PENIHILAN, nihil.getBankReffNo(), 
								nihil.getTrxDate(), nihil.getValueDate(), nihil.getOriginalAmount(), totalAmt, accountType, "");
						maxPerPage--;
					}
				}
			}
			
			mapLineArea.put(Constants.LINE_AREA_ROW, lineArea);
			mapLineArea.put(Constants.TOTAL_AMOUNT_CREDIT, totalAmt);
			
			//line area jika kosong
			if(isEmptyList){
				cx.generateLineAreaElement(rootElement, doc, 0, "", "", 
						"", "", "", 0, accountType, "");
			}
			
			//Footer Area Element
			Element FooterArea = doc.createElement("Footer");
			rootElement.appendChild(FooterArea);
			
			// Total Count elements
			String countLines = null;
			if(totalRowPerBs<1){
				countLines = "0";
			}
			else{
				countLines = String.valueOf(totalRowPerBs);
			}
			
			Element TotalCount = doc.createElement("TotalCount");
			TotalCount.appendChild(doc.createTextNode(countLines));
			//TotalCount.appendChild(doc.createTextNode(countRow));
			FooterArea.appendChild(TotalCount);
				
			// Total Amount elements
			Element TotalAmount = doc.createElement("TotalAmount");
//			TotalAmount.appendChild(doc.createTextNode(cx.convertAmount(String.valueOf(totalAmt))));
			TotalAmount.appendChild(doc.createTextNode(cx.convertAmount(String.valueOf(closeBalance))));
			FooterArea.appendChild(TotalAmount);

			//DOMSource source = new DOMSource(doc);
			//build filename BS
			DateFormat dft = new SimpleDateFormat("yyyyMMdd");
			DateFormat dft2 = new SimpleDateFormat("HHmmss");
			Date date1 = new Date();
			Date date2 = new Date();
			String createDate1 = dft.format(date1); //for date
			String createDate2 = dft2.format(date2); //for time
			
			String bsFilename = bankCode.concat("_".concat("BS".concat("_".concat("I".concat("_".concat(bankAcctNo.concat("_".concat(createDate1.concat("_".concat(createDate2.concat(".xml")))))))))));
			String filePath = xmlPath.concat(bsFilename);
			
			OutputFormat format = new OutputFormat(doc);
			format.setIndenting(true);
			
			fileOutputStream = new FileOutputStream(new File(filePath));
			XMLSerializer serializer = new XMLSerializer(fileOutputStream, format);
			serializer.serialize(doc);

			System.out.println("File "+bsFilename+" is saved!");
			statusConstruct = "SUCCESS".concat(":".concat(bsFilename));	
			
			bsFilename = bsFilename.replace("xml", "jar");
		  } catch (ParserConfigurationException pce) {
			statusConstruct = "FAILED".concat(":".concat(bankCode.concat("_BS_I_xxxxxxxxxxxxxxxxxxxx_xxxxxxxx_xxxxxx.xml")));
			log.error(pce.getMessage());
			pce.printStackTrace();
		  }catch (IOException e) {
			statusConstruct = "FAILED".concat(":".concat(bankCode.concat("_BS_I_xxxxxxxxxxxxxxxxxxxx_xxxxxxxx_xxxxxx.xml")));
			log.error(e.getMessage());
			e.printStackTrace();
		  }catch(Exception e){
			  statusConstruct = "FAILED".concat(":".concat(bankCode.concat("_BS_I_xxxxxxxxxxxxxxxxxxxx_xxxxxxxx_xxxxxx.xml")));
			  e.printStackTrace();
		  }finally{
			  if(fileOutputStream!=null)fileOutputStream.close();
			  
		  }
		return statusConstruct;
	}

	private int getMaxPerPage(int sisa, int maxPerPage) {
		return sisa<maxPerPage ? sisa : maxPerPage;
	}
	
	private String convertAmount(String amount){
		String convertedAmount = "";
		Double temp = 0.0;
		try{
			NumberFormat anotherFormat = NumberFormat.getNumberInstance(Locale.US);
			DecimalFormat anotherDFormat = (DecimalFormat) anotherFormat;
	        anotherDFormat.applyPattern("#.00");//use 2 digit after decimal
	        //anotherDFormat.applyPattern("#.0"); //use 1 digit after decimal
	        anotherDFormat.setGroupingUsed(true);
	        anotherDFormat.setGroupingSize(3);
	         
	        temp = Double.parseDouble(amount);
	        convertedAmount = String.valueOf(anotherDFormat.format(temp));
	        
	        //to kick out char ","
	        convertedAmount = convertedAmount.replace(",", "");
	        
	        if(".00".equalsIgnoreCase(convertedAmount)){
	        	convertedAmount = "0.00";
	        }
		}catch(Exception e){
			e.printStackTrace();
		}
		return convertedAmount;
	}
	
	/*public static void main(String[] args) {
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMMyy");
		System.out.println(sdf.format(new Date()));
	}*/
}
