package com.mii.util;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.mii.logger.Log;
import com.mii.models.DataValidationBPSX;

public class ConstructEQFile implements Serializable{
	private static final long serialVersionUID = 1L;
	private Log log;
	
	public ConstructEQFile(){
		log = Log.getInstance(ConstructEQFile.class); 
	}
	
	public String doConstruct(List<DataValidationBPSX> dataValidation){
		String buildString = "";
		String flagJurnal = "";
		String trxDebitCode = "";
		String debitAcctNo = "";
		String trxCreditCode = "";
		String creditAcctNo = "";
		String trxAmount = "";
		String reffNumber = "";
		String narrative1 = "";
		String narrative2 = "";
		String narrative3 = "";
		String narrative4 = "";
		String currency = "";
		String effectiveDate = "";
		String feeAmount = "";
		String costCenter = "";
		String branch = "";
		String tellerID = "";
		String reserveField = "";
		String flagAction = "";
		String detail = "";
		try{
			ConstructEQFile eq = new ConstructEQFile();
			StringBuilder sb = new StringBuilder();
			for(DataValidationBPSX dsx : dataValidation){
				flagJurnal = StringUtils.rightPad(eq.nullToEmptyString(dsx.getFlagJurnal()), 1, " ");
				trxDebitCode = StringUtils.rightPad(eq.nullToEmptyString(dsx.getTrxDebitCode()), 3, " ");
				debitAcctNo = StringUtils.rightPad(eq.nullToEmptyString(dsx.getDebitAcctNo()), 20, " ");
				trxCreditCode = StringUtils.rightPad(eq.nullToEmptyString(dsx.getTrxCreditCode()), 3, " ");
				creditAcctNo = StringUtils.rightPad(eq.nullToEmptyString(dsx.getCreditAcctNo()), 20, " ");
				trxAmount = StringUtils.leftPad(eq.nullToEmptyString(dsx.getTrxAmount()), 15, "0");
				reffNumber = StringUtils.rightPad(eq.nullToEmptyString(dsx.getReffNumber()), 12, " ");
				narrative1 = StringUtils.rightPad(eq.nullToEmptyString(dsx.getNarrative1()), 35, " ");
				narrative2 = StringUtils.rightPad(eq.nullToEmptyString(dsx.getNarrative2()), 35, " ");
				narrative3 = StringUtils.rightPad(eq.nullToEmptyString(dsx.getNarrative3()), 35, " ");
				narrative4 = StringUtils.rightPad(eq.nullToEmptyString(dsx.getNarrative4()), 35, " ");
				currency = StringUtils.rightPad(eq.nullToEmptyString(dsx.getCurrency()), 3, " ");
				effectiveDate = StringUtils.rightPad(eq.nullToEmptyString(dsx.getEffectiveDate()), 8, " ");
				feeAmount = StringUtils.leftPad(eq.nullToEmptyString(dsx.getFeeAmount()), 15, "0");
				costCenter = StringUtils.rightPad(eq.nullToEmptyString(dsx.getCostCenter()), 6, " ");
				branch = StringUtils.rightPad(eq.nullToEmptyString(dsx.getBranch()), 4, " ");
				tellerID = StringUtils.rightPad(eq.nullToEmptyString(dsx.getTellerID()), 4, " ");
				reserveField = StringUtils.rightPad(eq.nullToEmptyString(dsx.getReserveField()), 35, " ");
				flagAction = StringUtils.rightPad(eq.nullToEmptyString(dsx.getFlagAction()), 1, " ");
				
				detail = flagJurnal.concat(trxDebitCode.concat(debitAcctNo.concat(trxCreditCode.concat(creditAcctNo.concat(trxAmount.concat(
						reffNumber.concat(narrative1.concat(narrative2.concat(narrative3.concat(narrative4.concat(currency.concat(effectiveDate.concat(
						feeAmount.concat(costCenter.concat(branch.concat(tellerID.concat(reserveField.concat("\n"))))))))))))))))));
				sb.append(detail);
			}
			buildString = sb.toString().concat(flagAction);
		}catch(Exception e){
			buildString = "";
			log.error(e.getMessage());
			e.printStackTrace();
		}
		
		return buildString;
	}
	
	private String nullToEmptyString(String str) {
	    return str == null ? "" : str;
	}

}
