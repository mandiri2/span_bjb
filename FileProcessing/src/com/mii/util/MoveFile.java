package com.mii.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

public class MoveFile {
	
	public String doMoveFile(String resourcePathFile, String destinationPathFile){
		String moveStatus = "";
		InputStream inStream = null;
		OutputStream outStream = null;
		int length;
		
		try{
			File sourceFile = new File(resourcePathFile);
			File destFile = new File(destinationPathFile);
			inStream = new FileInputStream(sourceFile);
			outStream = new FileOutputStream(destFile);
			byte[] buffer = new byte[1024];
			
			while((length = inStream.read(buffer)) > 0){
				outStream.write(buffer, 0, length);
			}
			inStream.close();
			outStream.close();
			
			//delete source file
			sourceFile.delete();
			moveStatus = "SUCCESS";
		}catch(Exception e){
			moveStatus = "FAILED";
			e.printStackTrace();
		}
		
		return moveStatus;
	}
}
