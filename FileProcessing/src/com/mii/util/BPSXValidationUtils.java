package com.mii.util;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedProperty;

import com.mii.constant.State;
import com.mii.dao.DataBPSXDAO;
import com.mii.dao.SystemParameterDAO;
//import com.mii.file.TextFile;
import com.mii.logger.Log;
import com.mii.models.DataHeaderBPSX;
import com.mii.models.DataValidationBPSX;
import com.mii.models.SystemParameter;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.ApplicationInstance;

public class BPSXValidationUtils implements Serializable{
	private static final long serialVersionUID = 1L;
	@ManagedProperty(value = "#{applicationBean}")
	private ApplicationBean applicationBean;
	private static Log log;
	
	public BPSXValidationUtils(){
		applicationBean = ApplicationInstance.getInstance().getApplicationBean();
		log = Log.getInstance(BPSXValidationUtils.class);
	}
	
	public String doValidationBPSX(){
		BPSXValidationUtils bps = new BPSXValidationUtils();
		ConstructEQFile eq = new ConstructEQFile(); 
		List<DataValidationBPSX> validList = new ArrayList<DataValidationBPSX>();
		String batchid = "";
		String bpsxFilename = "";
		String currency = "";
		String totalRecord = "";
		String bpsxFileEvent = "";
		String validationStatus = "";
		String finalFlag = "";
		String rowNum = bps.getParameter("LIMIT_ROW_BANK_STATEMEMT");
		String sourcePathBPSX = bps.getParameter("BACKUP_LOCAL_PATH_BPSX_NOON");
		String destPathBPSX = bps.getParameter("RTGS_BPSX_WorkingPath");
		//String pathBSFile = bps.getParameter("RESOURCE_LOCAL_PATH_MT940");
		try{
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = new Date();
			
			//get header to be validated
			List<DataHeaderBPSX> headList = bps.getDataHeaderBPSX("4");
			if(headList.size()>0){
				for(DataHeaderBPSX dhb : headList){
					batchid = dhb.getBatchid();
					bpsxFileEvent = dhb.getBpsxFileEvent();
					totalRecord = dhb.getTotalRecord();
					bpsxFilename = dhb.getBPSX_Filename();
					currency = dhb.getCurrency();
					
					System.out.println("[BPSXDataValidation Service] Validate BPSX File "+bpsxFilename+" with batchid = "+batchid+" start time = "+dateFormat.format(date));
					
					int totalRows = Integer.parseInt(totalRecord);
					if(totalRows > 0){
						//get validation data in stored procedure
						validList = bps.getDataValidation(batchid, bpsxFilename, bpsxFileEvent, currency, rowNum);

						//Check final file
						if("NOON".equalsIgnoreCase(bpsxFileEvent)){
							if(validList.size()>0){
								//it's not final file
								log.debug("[BPSXDataValidation Service] BPSX File "+bpsxFilename+" is not a Final File!");
								finalFlag = "0";
							}else{
								log.debug("[BPSXDataValidation Service] BPSX File "+bpsxFilename+" is a Final File!");
								//update final file flag
								bps.updateFinalFileFlag(batchid,"1");
								finalFlag = "1";
							}
						}
						
						//Construct EQ File
						if(validList.size()>0){
							String buildString = eq.doConstruct(validList);
							if(buildString.length()>0){
								String brpFile = bps.createBRPFile(buildString);
								String brpFilename = brpFile.substring(8);
								String statusCreateBrpFile = brpFile.substring(0, 7);
								
								if(State.Success.equalsIgnoreCase(statusCreateBrpFile)){
									//update header status
									bps.updateBPSXHeader(batchid, "4", brpFilename, "BRP File has been generated!");
									
									//move to log
									String moveStatus = bps.moveDataBRPToLog("4");
									System.out.println("Move Status BRP Data is "+moveStatus);
								}else{
									//update header status
									bps.updateBPSXHeader(batchid, "R", brpFilename, "BRP File "+brpFilename+" failed to be generated!");
								}
							}else{
								Log.getInstance().debug("[BPSXDataValidation Service] No text File");
								System.out.println("No text file!");
								
								//update header status
								bps.updateBPSXHeader(batchid, "S", "BRP".concat(dateFormat.format(date)), "BRP File has no content string!");
								
								//move header to log
								DataBPSXDAO dbdao = (DataBPSXDAO) applicationBean.getFileProcessContext().getBean("dataHeaderBPSXDAO");
								dbdao.moveBRPDataToLog(batchid, "S");
							}
						}else{
							Log.getInstance().debug("[BPSXDataValidation Service] No data found!");
							System.out.println("[BPSXDataValidation Service] No data found!");
							
							//update header status
							bps.updateBPSXHeader(batchid, "G", "BRP".concat(dateFormat.format(date)), "BRP File has no data to be validated!");
							
							//check morning file
							if("MORNING".equalsIgnoreCase(bpsxFileEvent)){
								//move header to log
								DataBPSXDAO dbdao = (DataBPSXDAO) applicationBean.getFileProcessContext().getBean("dataHeaderBPSXDAO");
								dbdao.moveBRPDataToLog(batchid, "G");
							}
							
							if("1".equalsIgnoreCase(finalFlag)){
								//move file BPS to RTGS Path
								String resourcePathFile = sourcePathBPSX.concat(bpsxFilename);
								String destinationPathFile = destPathBPSX.concat(bpsxFilename);
								MoveFile mv = new MoveFile();
								mv.doMoveFile(resourcePathFile, destinationPathFile);
								
								//copy file BPS to Path Generate Bank Statement
								/*String destBSPath = pathBSFile.concat(bpsxFilename);
								String sourceBSPath = destinationPathFile;
								TextFile tf = new TextFile();
								tf.copyFile(sourceBSPath, destBSPath);*/
								
								//move header to log
								DataBPSXDAO dbdao = (DataBPSXDAO) applicationBean.getFileProcessContext().getBean("dataHeaderBPSXDAO");
								dbdao.moveBRPDataToLog(batchid, "G");
							}
						}
						
					}else{
						//update header status
						bps.updateBPSXHeader(batchid, "M", "BRP".concat(dateFormat.format(date)), "No rows to be validated");
						
						//move header to log
						DataBPSXDAO dbdao = (DataBPSXDAO) applicationBean.getFileProcessContext().getBean("dataHeaderBPSXDAO");
						dbdao.moveBRPDataToLog(batchid, "M");
						log.debug("[BPSXDataValidation Service] No rows to be validated");
						System.out.println("[BPSXDataValidation Service] No rows to be validated");
					}
				}
				validationStatus = "Success".concat("||".concat(bpsxFilename));
				System.out.println("[BPSXValidation Service] Validation status is "+validationStatus);
			}else{
				log.debug("[BPSXDataValidation Service] No data found to be validate!");
			}
			
			
		}catch(Exception e){
			validationStatus = "Failed".concat("||".concat(bpsxFilename));
			System.out.println("[BPSXValidation Service] Validation status is "+validationStatus);
			log.error(e.getMessage());
			e.printStackTrace();
		}
		
		return validationStatus;
	}
	
	public String createBRPFile(String textFile){
		BPSXValidationUtils bps = new BPSXValidationUtils();
		WriteToFile wtf = new WriteToFile();
		String createStatus = "";
		String putPathBRP = bps.getParameter("SFTP_LOCAL_PATH_EQ");
		DateFormat dateFormat = new SimpleDateFormat("yyMMddHHmm");
		Date date = new Date();
		String brpFilename = "BRP".concat(dateFormat.format(date));
		try{
			createStatus = wtf.doWriteFile(textFile, brpFilename, putPathBRP);
			System.out.println("[BPSXDataValidation Service] Generate BRP File "+brpFilename+" finish time = "+dateFormat.format(date));
			createStatus = createStatus.concat("|".concat(brpFilename));
		}catch(Exception e){
			createStatus = "Failed";
			createStatus = createStatus.concat("|".concat(brpFilename));
			log.error("[BPSXDataValidation Service] Failed to create File BRP "+brpFilename+" !");
			e.printStackTrace();
		}
		return createStatus;
	}
	
	public String moveDataBRPToLog(String iStatus){
		String moveStatus = "";
		String batchid = "";
		BPSXValidationUtils bps = new BPSXValidationUtils();
		try{
			List<DataHeaderBPSX> headList = bps.getDataHeaderBPSX(iStatus);
			if(headList.size()>0){
				for(DataHeaderBPSX dhb : headList){
					batchid = dhb.getBatchid();
					DataBPSXDAO dbdao = (DataBPSXDAO) applicationBean.getFileProcessContext().getBean("dataHeaderBPSXDAO");
					dbdao.moveBRPDataToLog(batchid, iStatus);
				}
			}
			moveStatus = "Success";
		}catch(Exception e){
			moveStatus = "Failed";
			log.debug("Failed to move data BRP!");
			e.printStackTrace();
		}
		return moveStatus;
	}
	
	private void updateFinalFileFlag(String iBatchid, String iFinalFlag){
		DataBPSXDAO dbdao = (DataBPSXDAO) applicationBean.getFileProcessContext().getBean("dataHeaderBPSXDAO");
		dbdao.updateFinalFileFlag(iBatchid, iFinalFlag);
	}
	
	private List<DataHeaderBPSX> getDataHeaderBPSX(String status){
		DataBPSXDAO dbdao = (DataBPSXDAO) applicationBean.getFileProcessContext().getBean("dataHeaderBPSXDAO");
		List<DataHeaderBPSX> dataHeader = new ArrayList<DataHeaderBPSX>(dbdao.getDataHeader(status));
		return dataHeader;
	}
	
	private List<DataValidationBPSX> getDataValidation(String iBatchid, String iBPSXFilename, 
			String iBPSXFileEvent, String iCurrency, String iRownum){
		DataBPSXDAO dbdao = (DataBPSXDAO) applicationBean.getFileProcessContext().getBean("dataHeaderBPSXDAO");
		List<DataValidationBPSX> dataValidation = new ArrayList<DataValidationBPSX>(dbdao.getValidateBPSX(iBatchid, iBPSXFilename, 
				iBPSXFileEvent, iCurrency, iRownum));
		return dataValidation;
	}
	
	public String getParameter(String param_name){
		List<SystemParameter> params = getParameterbyID(param_name);
		String param = "";
		for(SystemParameter sp : params){
			param = sp.getParam_value();
		}
		return param;
	}
	
	private List<SystemParameter> getParameterbyID(String param_name){
		SystemParameterDAO sysDAO = (SystemParameterDAO) applicationBean.getFileProcessContext().getBean("sysParamDAO");
		List<SystemParameter> params = new ArrayList<SystemParameter>(sysDAO.getValueParameter(param_name));
		return params;
	}
	
	private void updateBPSXHeader(String iBatchid, String setStatus, String setBRPFilename,String Description){
		DataBPSXDAO dbdao = (DataBPSXDAO) applicationBean.getFileProcessContext().getBean("dataHeaderBPSXDAO");
		dbdao.updateDataHeaderBPSX(iBatchid, setStatus, setBRPFilename, Description);
	}

}
