package com.mii.util;

import java.io.Serializable;
import java.util.ArrayList;

import javax.faces.bean.ManagedProperty;

import com.mii.dao.MPNReconFIleDAO;
import com.mii.dao.SystemParameterDAO;
import com.mii.dao.TransactionDAO;
import com.mii.models.ReconFileM;
import com.mii.models.SystemParameter;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.ApplicationInstance;

public class ReconUtils implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@ManagedProperty(value = "#{applicationBean}")
	private ApplicationBean applicationBean;

	public ReconUtils() {
		applicationBean = ApplicationInstance.getInstance().getApplicationBean();
	}
	
	public void updateNTPN(String ntpn, String ntb,String kodeBilling){
		((TransactionDAO) applicationBean.getFileProcessContext().getBean("transactionDao")).updateNTPN(ntpn, ntb, kodeBilling);
	}
	
	public String getSystemParameter(String param_name){
		ArrayList<SystemParameter> parameters = (ArrayList<SystemParameter>) ((SystemParameterDAO) applicationBean.getFileProcessContext().getBean("sysParamDAO")).getValueParameter(param_name);
		return parameters.get(0).getParam_value();
	}
	
	public void saveMPNReconFile(String reconTime, String caID, String caName, String settlementDate, String currency, String billingCode, String ntb, String ntpn, String amount, String noSakti){
		ReconFileM reconFileM = new ReconFileM();
		
		reconFileM.setReconTime(reconTime);
		reconFileM.setCaID(caID);
		reconFileM.setCaName(caName);
		reconFileM.setSettlementDate(settlementDate);
		reconFileM.setCurrency(currency);
		reconFileM.setBillingCode(billingCode);
		reconFileM.setNtb(ntb);
		reconFileM.setNtpn(ntpn);
		reconFileM.setAmount(amount);
		reconFileM.setNoSakti(noSakti);
		
		((MPNReconFIleDAO) applicationBean.getFileProcessContext().getBean("mpnReconFileDao")).saveMPNRecon(reconFileM);
	}
}
