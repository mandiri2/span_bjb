package com.mii.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import javax.faces.bean.ManagedProperty;

import com.mii.constant.CommonConstant;
import com.mii.constant.Scheduler;
import com.mii.dao.DataMT940DAO;
import com.mii.dao.RTGSDAO;
import com.mii.dao.RTGSStepDAO;
import com.mii.dao.SystemParameterDAO;
import com.mii.dao.TransactionDAO;
import com.mii.flatFile.FileFormatter;
import com.mii.logger.Log;
import com.mii.logger.LoggerEvent;
import com.mii.models.RTGSM;
import com.mii.models.RTGSStepM;
import com.mii.models.SystemParameter;
import com.mii.models.TransactionM;
import com.mii.record.BPSXDetailFile;
import com.mii.record.BPSXFooterFile;
import com.mii.record.BPSXHeaderFile;
import com.mii.record.RTGSRequestFile;
import com.mii.record.RTGSResponseFile;
import com.mii.scheduler.SchedulerConfig;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.ApplicationInstance;

public class RTGSUtils implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private static final Log log = Log.getInstance(RTGSUtils.class);
	
	@ManagedProperty(value = "#{applicationBean}")
	private ApplicationBean applicationBean;
	
	public RTGSUtils() {
		applicationBean = ApplicationInstance.getInstance().getApplicationBean();
	}
	
	public void insertRTGS(String state, String transactionID, Date transactionDate, Date updateDate, String transactionFlag, String accountCurrency, String sourceAccountNumber, String destinationAccountNumber, String destinationAccountName, String transactionAmount, String paymentType, String bankCode, String description1, String description2, String description3, String description4, String reserved1, String reserved2, String reserved3, String reserved4, String bpsxName, String bppxName){
		RTGSM rtgsm = new RTGSM();
		
		rtgsm.setState(state);
		rtgsm.setTransactionID(transactionID);
		rtgsm.setTransactionDate(transactionDate);
		rtgsm.setUpdateDate(updateDate);
		rtgsm.setTransactionFlag(transactionFlag);
		rtgsm.setAccountCurrency(accountCurrency);
		rtgsm.setSourceAccountNumber(sourceAccountNumber);
		rtgsm.setDestinationAccountNumber(destinationAccountNumber);
		rtgsm.setDestinationAccountName(destinationAccountName);
		rtgsm.setTransactionAmount(transactionAmount);
		rtgsm.setPaymentType(paymentType);
		rtgsm.setBankCode(bankCode);
		rtgsm.setDescription1(description1);
		rtgsm.setDescription2(description2);
		rtgsm.setDescription3(description3);
		rtgsm.setDescription4(description4);
		rtgsm.setBpsxName(bpsxName);
		rtgsm.setBppxName(bppxName);
		rtgsm.setReserved1(reserved1);
		rtgsm.setReserved2(reserved2);
		rtgsm.setReserved3(reserved3);
		rtgsm.setReserved4(reserved4);
		
		((RTGSDAO) applicationBean.getFileProcessContext().getBean("rtgsDao")).saveRTGS(rtgsm);
	}
	
	public void updateResponseRTGS(String state, String transactionFlag, String transactionID, Date timeStamp, String userID, String response, String responseDesc, String referenceNo, String bpoxName){
		RTGSM rtgsm = new RTGSM();
		
		rtgsm.setState(state);
		rtgsm.setTransactionFlag(transactionFlag);
		rtgsm.setTransactionID(transactionID);
		rtgsm.setUpdateDate(timeStamp);
		rtgsm.setUserID(userID);
		rtgsm.setResponse(response);
		rtgsm.setResponseDesc(responseDesc);
		rtgsm.setReferenceNumber(referenceNo);
		rtgsm.setBpoxName(bpoxName);
		
		((RTGSDAO) applicationBean.getFileProcessContext().getBean("rtgsDao")).updateRTGSResponse(rtgsm);
	}
	
	public void updateNoSaktiRTGS(String state, Date timeStamp, String transactionFlag, String referenceNo, String noSakti, String reserved3){
		RTGSM rtgsm = new RTGSM();
		
		rtgsm.setState(state);
		rtgsm.setUpdateDate(timeStamp);
		rtgsm.setTransactionFlag(transactionFlag);
		rtgsm.setReserved3(reserved3);
		rtgsm.setReferenceNumber(referenceNo);
		rtgsm.setNoSakti(noSakti);
		
		((RTGSDAO) applicationBean.getFileProcessContext().getBean("rtgsDao")).updateRTGSNoSakti(rtgsm);
	}
	
	public String getBPSXFileRTGS(String referenceNo){
		ArrayList<RTGSM> parameters = (ArrayList<RTGSM>) ((RTGSDAO) applicationBean.getFileProcessContext().getBean("rtgsDao")).getBPSXFile(referenceNo);
		return parameters.get(0).getBpsxName();
	}
	
	public List<RTGSM> getRTGSData(String flag){
		return (ArrayList<RTGSM>) ((RTGSDAO) applicationBean.getFileProcessContext().getBean("rtgsDao")).getRTGSbyFlag(flag);
	}
	
	public List<TransactionM> getRTGSData10(String flag){
		return (ArrayList<TransactionM>) ((RTGSDAO) applicationBean.getFileProcessContext().getBean("rtgsDao")).getRTGSbyFlag10(flag);
	}
	
	public List<TransactionM> checkRTGSData(String BPSFilename){
		return (ArrayList<TransactionM>) ((RTGSDAO) applicationBean.getFileProcessContext().getBean("rtgsDao")).checkRTGSData(BPSFilename);
	}
	
	public void updateRTGSFlagByReff(String reffNo){
		((RTGSDAO) applicationBean.getFileProcessContext().getBean("rtgsDao")).updateRTGSFlag(reffNo);
	}
	
	public void insertStepRTGSRequest(String stepID, String event, String rtgsRequestState, String accountCurrency, String sourceAccountNumber, String amount, String transactionID, String uniqueIDFileName, String bpsxFile, String bppxFile){
		RTGSStepM rtgsStepM = new RTGSStepM();
		
		rtgsStepM.setStepID(stepID);
		rtgsStepM.setEvent(event);
		rtgsStepM.setRtgsRequestDate(CommonDate.getCurrentDateString("ddMMyyyy"));
		rtgsStepM.setRtgsRequestState(rtgsRequestState);
		rtgsStepM.setAccountCurrency(accountCurrency);
		rtgsStepM.setSourceAccountNumber(sourceAccountNumber);
		rtgsStepM.setAmount(amount);
		rtgsStepM.setTransactionID(transactionID);
		rtgsStepM.setUniqueIDFileName(uniqueIDFileName);
		rtgsStepM.setBpsxFile(bpsxFile);
		rtgsStepM.setBppxFile(bppxFile);
		
		((RTGSStepDAO) applicationBean.getFileProcessContext().getBean("rtgsStepDao")).saveRTGSStep(rtgsStepM);
		
	}
	
	public void updateStepRTGSRequest(String stepID, String event, String rtgsResponseState, String referenceNo, String bpoxFile){
		RTGSStepM rtgsStepM = new RTGSStepM();
		
		rtgsStepM.setStepID(stepID);
		rtgsStepM.setEvent(event);
		rtgsStepM.setRtgsResponseDate(CommonDate.getCurrentDateString("ddMMyyyy"));
		rtgsStepM.setRtgsResponseState(rtgsResponseState);
		rtgsStepM.setReferenceNo(referenceNo);
		rtgsStepM.setBpoxFile(bpoxFile);
		
		((RTGSStepDAO) applicationBean.getFileProcessContext().getBean("rtgsStepDao")).updateRTGSStep(rtgsStepM);
		
	}
	
	public List<RTGSStepM> getAllStepByEvent(String event){
		return (List<RTGSStepM>) ((RTGSStepDAO) applicationBean.getFileProcessContext().getBean("rtgsStepDao")).getAllStepByEvent(event);
	}
	
	public void updateNoSakti(String ntb, String noSakti){
		TransactionM transactionM = new TransactionM();
		
		transactionM.setNtb(ntb);
		transactionM.setNoSakti(noSakti);
		
		((TransactionDAO) applicationBean.getFileProcessContext().getBean("transactionDao")).updateNoSakti(transactionM);
	}
	
	public void updateNoSaktiMT940(String refNum, String iBatchid){
		DataMT940DAO dmt = (DataMT940DAO) applicationBean.getFileProcessContext().getBean("dataListDAO");
//		dmt.updateNoSaktiToBS(refNum, iBatchid);
	}
	
	public String generateRTGSRequestFile(String accountCurrency, String sourceAccountNumber, String destinationAccountNumber, String destinationAccountName, String transactionAmount, String paymentType, String bankCode, String description1, String description2, String description3, String description4, String referenceNo){
		RTGSRequestFile rtgsRequestFile = new RTGSRequestFile();
		
		rtgsRequestFile.setAccountCurrency(accountCurrency);
		rtgsRequestFile.setSourceAccountNumber(sourceAccountNumber);
		rtgsRequestFile.setDestinationAccountNumber(destinationAccountNumber);
		rtgsRequestFile.setDestinationAccountName(destinationAccountName);
		rtgsRequestFile.setTransactionAmount(transactionAmount);
		rtgsRequestFile.setPaymentType(paymentType);
		rtgsRequestFile.setBankCode(bankCode);
		rtgsRequestFile.setDescription1(description1);
		rtgsRequestFile.setDescription2(description2);
		rtgsRequestFile.setDescription3(description3);
		rtgsRequestFile.setDescription4(description4);
		rtgsRequestFile.setReferenceNo(referenceNo);
		
		return new FileFormatter().generateContentFixLenght(rtgsRequestFile);
	}
	
	public RTGSResponseFile readRTGSResponseFile(String content){
		return (RTGSResponseFile) new FileFormatter().getContentByFixLength(content, RTGSResponseFile.class);
	}
	
	public BPSXHeaderFile readBPSXHeaderFile(String content){
		return (BPSXHeaderFile) new FileFormatter().getContentByFixLength(content, BPSXHeaderFile.class);
	}
	
	public BPSXDetailFile readBPSXDetailFile(String content){
		return (BPSXDetailFile) new FileFormatter().getContentByFixLength(content, BPSXDetailFile.class);
	}
	
	public BPSXFooterFile readBPSXFooterFile(String content){
		return (BPSXFooterFile) new FileFormatter().getContentByFixLength(content, BPSXFooterFile.class);
	}
	
	public void runSchedulerGetResponseRTGSFile(HashMap<String, String> schedulerParameter){
		try {
			String jobName		= CommonConstant.BPPSchedulerNameGetFileResponse;
			String schedulerType= Scheduler.RepeatInterval;
			String interval		= CommonConstant.BPPSchedulerIntervalGetFileResponse;
			String startDate	= CommonDate.getCurrentDateString("yyyy/MM/dd");
			String endDate		= CommonDate.getNextDateString("yyyy/MM/dd");
			String startTime	= CommonDate.getCurrentDateString("HH:mm:ss");
			String endTime		= CommonConstant.BPPSchedulerEndTimeGetFileResponse;
			String executionService	= CommonConstant.BPPSchedulerServiceGetFileResponse;
			
			String schedulerID = SchedulerConfig.getInstance().createScheduler( jobName, executionService, schedulerType, "", "", interval, startDate, endDate, startTime, endTime, "", "", "", "", "", "", "", schedulerParameter);
			
			SchedulerConfig.getInstance().startScheduler(schedulerID);
		} catch (Exception e) {
			e.printStackTrace();
			LoggerEvent.errorProcess(log, this.getClass().getName()+"/"+this.getClass().getMethods().toString(), e.getMessage());
		}
	}
	
	public void runSchedulerGetNoSakti(HashMap<String, String> parameter){
		try {
			String jobName		= "SchedulerGetNoSakti";
			String schedulerType= Scheduler.RepeatInterval;
			String interval		= "30";
			String startDate	= CommonDate.getCurrentDateString("yyyy/MM/dd");
			String endDate		= CommonDate.getNextDateString("yyyy/MM/dd");
			String startTime	= CommonDate.getCurrentDateString("HH:mm:ss");
			String endTime		= "08:59:00";
			String executionService	= "GetNoSakti";
			
			String schedulerID = SchedulerConfig.getInstance().createScheduler(jobName, executionService, schedulerType, "", "", interval, startDate, endDate, startTime, endTime, "", "", "", "", "", "", "", parameter);
			
			SchedulerConfig.getInstance().startScheduler(schedulerID);
		} catch (Exception e) {
			e.printStackTrace();
			LoggerEvent.errorProcess(log, this.getClass().getName()+"/"+this.getClass().getMethods().toString(), e.getMessage());
		}
	}

	public void stopSchedulerRTGS(String schedulerID){
		SchedulerConfig.getInstance().stopScheduler(schedulerID);
	}
	
	public List<SystemParameter> getAllParameter(){
		return (List<SystemParameter>) ((SystemParameterDAO) applicationBean.getFileProcessContext().getBean("sysParamDAO")).getAllValueParameter();
	}
	
	public String getSystemParameter(String param_name){
		ArrayList<SystemParameter> parameters = (ArrayList<SystemParameter>) ((SystemParameterDAO) applicationBean.getFileProcessContext().getBean("sysParamDAO")).getValueParameter(param_name);
		return parameters.get(0).getParam_value();
	}
	
	public ConcurrentHashMap<String, String> putParameterToCache(List<SystemParameter> params){

		ConcurrentHashMap<String, String> cacheParameter = new ConcurrentHashMap<String, String>();
		for (int i = 0; i < params.size(); i++)cacheParameter.put(params.get(i).getParam_name(), params.get(i).getParam_value()==null ? "":params.get(i).getParam_value());
		
		return cacheParameter;
	}
}
