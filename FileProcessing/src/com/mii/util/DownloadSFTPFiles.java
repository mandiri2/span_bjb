package com.mii.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.jcraft.jsch.Channel;  
import com.jcraft.jsch.ChannelSftp;  
import com.jcraft.jsch.JSch;  
import com.jcraft.jsch.Session; 

public class DownloadSFTPFiles {
	String host;
	String username;
	String password;
	String port;
	String workingPath;
	String localPath;
	String privateKeyPath;
	String isPrivateKey;
	String knownHostKey;
	String patternFile;
	
	Session     session     = null;  
    Channel     channel     = null;  
    ChannelSftp channelSftp = null; 
	
	public DownloadSFTPFiles(String host, String username, String password, String port,
			String workingPath, String localPath, String privateKeyPath, String isPrivateKey, String knownHostKey, String patternFile){
		this.host = host;
		this.username = username;
		this.password = password;
		this.port = port;
		this.workingPath = workingPath;
		this.localPath = localPath;
		this.privateKeyPath = privateKeyPath;
		this.isPrivateKey = isPrivateKey;
		this.knownHostKey = knownHostKey;
		this.patternFile = patternFile;
	}
	
	
	public String downloadFile(){
		
		String statusDownloadSFTP ="";
		
		
		try{
			//int port_ = Integer.parseInt(port);
			JSch jsch = new JSch(); 
			List<String> xFile = new ArrayList<String>();
			List<String> anchorFile = new ArrayList<String>();
			
	        if("true".equalsIgnoreCase(isPrivateKey)){
	        	jsch.setKnownHosts(knownHostKey);
	        	jsch.addIdentity(privateKeyPath);
	        	
	        }else{
	        	session.setPassword(password);
		        session.setConfig("StrictHostKeyChecking", "no");
	        }
	         
	        //session = jsch.getSession(username,host,port_);  
	        session = jsch.getSession(username, host);
	        session.connect();  
	        channel = session.openChannel("sftp");  
	        channel.connect();
	        
	        if (!channel.isConnected()) {
	        	System.out.println("can't connect to sftp");
	        }else{
	        	//System.out.println("success connect to sftp "+host);
	        	channelSftp = (ChannelSftp)channel;
	        	channelSftp.lcd(localPath);
	        	
	        	// Get a listing of the remote directory
                @SuppressWarnings("unchecked")
                Vector<ChannelSftp.LsEntry> list = channelSftp.ls(workingPath);
                for(ChannelSftp.LsEntry oList : list){
                	if (!oList.getAttrs().isDir()) {
                		//System.out.println("Filename = "+oList.getFilename());
                		//System.out.println("Contain BPSX? "+oList.getFilename().substring(0, 4).equalsIgnoreCase(patternFile));
                		if(patternFile.equalsIgnoreCase(oList.getFilename().substring(0, 4))){
        					String a = oList.getFilename().substring(0, 3);
        					String b = oList.getFilename().substring(4);
        					//System.out.println("get add into xFile = "+a+b);
        					xFile.add(a+b);
        				}
                		
                		if(patternFile.equalsIgnoreCase(oList.getFilename().substring(0, 4))){
                    		anchorFile.add(oList.getFilename());
                    	}
                	}
                }
                
                for(int i = 0; i < xFile.size(); i++){
                		 //System.out.println("get file = "+xFile.get(i));
                		 //channelSftp.get(oList.getFilename(), oList.getFilename());
                		 System.out.println("Download file = "+xFile.get(i));
                		 System.out.println("Get File from = "+workingPath.concat(xFile.get(i)));
                		 System.out.println("Move to = "+localPath);
                		 try{
                			 channelSftp.get(workingPath.concat(xFile.get(i)), localPath);
                			 System.out.println("Success get file = "+xFile.get(i));
                		 
                			 //delete file from sftp
                			 channelSftp.rm(workingPath.concat(xFile.get(i)));
                			 System.out.println("Success delete file = "+workingPath.concat(xFile.get(i)));
                		 }catch(Exception e){
                			 e.printStackTrace();
                		 }
                		 statusDownloadSFTP = "SUCCESS";
                }
                
                for(int i = 0; i < anchorFile.size(); i++){
                	channelSftp.rm(workingPath.concat(anchorFile.get(i)));
                }
                
                
	        }
		}catch(Exception e){
			statusDownloadSFTP = "FAILED";
			e.printStackTrace();
		}finally{
			channel.disconnect();
			session.disconnect();
		}
		
		return statusDownloadSFTP;
	}
	
}
