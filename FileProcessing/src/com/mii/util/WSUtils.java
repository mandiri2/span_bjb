package com.mii.util;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

import javax.faces.bean.ManagedProperty;

import com.mii.dao.SystemParameterDAO;
import com.mii.models.SystemParameter;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.ApplicationInstance;

public class WSUtils {
	@ManagedProperty(value = "#{applicationBean}")
	private ApplicationBean applicationBean;
	
	public WSUtils() {
		applicationBean = ApplicationInstance.getInstance().getApplicationBean();
	}

	public ConcurrentHashMap<String, String> putParameterToCache(ArrayList<SystemParameter> params){
		
		ConcurrentHashMap<String, String> cacheParameter = new ConcurrentHashMap<String, String>();
		for (int i = 0; i < params.size(); i++) cacheParameter.put(params.get(i).getParam_name(), params.get(i).getParam_value());
		
		return cacheParameter;
	}
	
	public ArrayList<SystemParameter> getAllParameter(){
		return (ArrayList<SystemParameter>) ((SystemParameterDAO) applicationBean.getFileProcessContext().getBean("sysParamDAO")).getAllValueParameter();
	}
}
