package com.mii.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.FileOutputStream;
import java.lang.reflect.Field;
import java.security.KeyStore;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.ws.custom.security.WSConstants;
import org.apache.ws.custom.security.WSSConfig;
import org.apache.ws.custom.security.components.crypto.Crypto;
import org.apache.ws.custom.security.components.crypto.Merlin;
import org.apache.ws.custom.security.message.WSSecHeader;
import org.apache.ws.custom.security.message.WSSecSignature;
import org.apache.ws.custom.security.util.Loader;
import org.apache.xml.security.utils.XMLUtils;
import org.w3c.dom.Document;

public class DigitalSignature {
	
	public static SOAPMessage loadDocument(String filePath) {
		
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        docFactory.setNamespaceAware(true);
 
        Document xmlDoc;
        SOAPMessage soapMessage = null;
		try {
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			xmlDoc = docBuilder.parse(new File(filePath));
			
			 // You can load XML document easily by using axis2 lib
	        // XMLUtils.newDocument(new FileInputStream(filePath));
	 
	        // Wrap xml document with SOAPEnvelope
	        soapMessage = MessageFactory.newInstance().createMessage();
	        soapMessage.getSOAPBody().addDocument(xmlDoc);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
        return soapMessage;
    }


	public void persistDocument(Document doc, String file) throws TransformerException, IOException {
	
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(file);
			/*OutputFormat format = new OutputFormat(doc);
			format.setIndenting(true);
			XMLSerializer serializer = new XMLSerializer(fos, format);
			serializer.serialize(doc);*/
			DOMSource source = new DOMSource(doc);
			StreamResult rslt = new StreamResult(fos);
			transformer.transform(source, rslt);
		} finally {
			try {
				fos.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	
	@SuppressWarnings("rawtypes")
	public Document signSOAPMessage(SOAPMessage soapEnvelope, String filePath, 
    		String keystorePass, String privateKeyUser, String privateKeyPass, Class iClass ) throws Exception
    {
		WSSConfig.init();
    	WSSecSignature builder = new WSSecSignature();
    	DOMResult result = new DOMResult();
    	//XMLSerializer serial = new XMLSerializer();
    	WSSecHeader secHeader = new WSSecHeader();
    	Crypto crypto = new Merlin();               
    	
    	//get SOAP content and insert security header
        Source src = soapEnvelope.getSOAPPart().getContent();
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer(); 
        
        transformer.transform(src, result);
        Document doc = (Document) result.getNode();               
        secHeader.insertSecurityHeader(doc);
                
        //Load JKS keystore using Merlin provider
        KeyStore keyStore = KeyStore.getInstance("JKS");
        ClassLoader loader = Loader.getClassLoader(iClass);
        
        System.out.println("IgnoreLineBreaks : "+XMLUtils.ignoreLineBreaks());
        
        String lineBreakPropName = "org.apache.xml.security.ignoreLineBreaks";
        System.out.println("Prop value : "+System.getProperty(lineBreakPropName));
        WSSConfig.init();
        /*Field f = XMLUtils.class.getDeclaredField("ignoreLineBreaks");
		f.setAccessible(true);
        f.set(null, Boolean.TRUE);*/
        System.setProperty(lineBreakPropName, "true");
        System.out.println("Prop value : "+System.getProperty(lineBreakPropName));
        System.out.println("IgnoreLineBreaks : "+XMLUtils.ignoreLineBreaks());
        
        InputStream input = Merlin.loadInputStream(loader, filePath);
        keyStore.load(input, keystorePass.toCharArray());
        ((Merlin)crypto).setKeyStore(keyStore);
        
        //sign document
        builder.setUserInfo(privateKeyUser, privateKeyPass);
        builder.setKeyIdentifierType(WSConstants.BST_DIRECT_REFERENCE);
        Document signedDoc = builder.build(doc, crypto, secHeader);
        
		return signedDoc;
    	
    }
}
