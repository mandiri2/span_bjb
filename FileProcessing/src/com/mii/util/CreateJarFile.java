package com.mii.util;

import java.io.*;
//import java.util.regex.*;
//import java.util.*;
//import java.text.*;
import java.util.jar.*;
//import org.apache.commons.lang.StringEscapeUtils;
//import mii.project.source.threadpool.*;
//import java.util.zip.*;
//import java.math.BigDecimal;

public class CreateJarFile {
	public static void runCreateJar(String fileInput, String fileTarget) throws IOException
	{
	  Manifest manifest = new Manifest();
	  manifest.getMainAttributes().put(Attributes.Name.MANIFEST_VERSION, "1.0");
	  JarOutputStream target = new JarOutputStream(new FileOutputStream(fileTarget), manifest);
	  add(new File(fileInput), target);
	  target.close();
	}

	private static void add(File source, JarOutputStream target) throws IOException
	{
	  BufferedInputStream in = null;
	  try
	  {
	    if (source.isDirectory())
	    {
	      //not optimize for directory
	      String name = source.getPath().replace("\\", "/");
	      if (!name.isEmpty())
	      {
	        if (!name.endsWith("/"))
	          name += "/";
	      }
	      for (File nestedFile: source.listFiles())
	        add(nestedFile, target);
	      return;
	    }
	    source.getPath().replace("\\", "/");
	    JarEntry entry = new JarEntry(source.getName());
	    entry.setTime(source.lastModified());
	    target.putNextEntry(entry);
	    in = new BufferedInputStream(new FileInputStream(source));

	    byte[] buffer = new byte[1024];
	    while (true)
	    {
	      int count = in.read(buffer);
	      if (count == -1)
	        break;
	      target.write(buffer, 0, count);
	    }
	    target.closeEntry();
	  }
	  finally
	  {
	    if (in != null)
	      in.close();
	  }
	}
}
