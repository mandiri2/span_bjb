package com.mii.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.io.Writer;

import com.mii.logger.Log;

public class WriteToFile implements Serializable{
	private static final long serialVersionUID = 1L;
	private Log log;
	
	public WriteToFile(){
		log = Log.getInstance(ConstructEQFile.class); 
	}
	
	
	public String doWriteFile(String text, String filename, String pathFile){
		String writeStatus = "";
		try{
			File statText = new File(pathFile+filename);
            FileOutputStream is = new FileOutputStream(statText);
            OutputStreamWriter osw = new OutputStreamWriter(is);    
            Writer w = new BufferedWriter(osw);
            w.write(text);
            w.close();
            writeStatus = "Success";
		}catch(IOException e){
			writeStatus = "Failed";
			log.error(e.getMessage());
			e.printStackTrace();
		}
		return writeStatus;
	}
}
