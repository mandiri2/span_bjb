package com.mii.scheduler;

import com.mii.constant.CommonConstant;
import com.mii.constant.Scheduler;

public class SchedulerType {

	public String run(String schedulerType, String  date, String time, String interval, String startDate, String endDate, String startTime, String endTime, String maskYear, String maskMonth, String maskDay, String maskWeeklyDay, String maskHour, String maskMinute, String maskSecond){
		
		if (schedulerType.equals(Scheduler.OneTimeTask)) {
			return oneTimeTaskScheduler(date, time);
		} else if (schedulerType.equals(Scheduler.RepeatInterval)) {
			return repeatIntervalScheduler(interval, startDate, endDate, startTime, endTime);
		} else if (schedulerType.equals(Scheduler.RepeatTime)) {
			return repeatTimeScheduler(interval, startDate, endDate, startTime, endTime, maskYear,  maskMonth,  maskDay,  maskWeeklyDay,  maskHour,  maskMinute,  maskSecond);
		} else {
			return CommonConstant.emptyString;
		}
		
	}
	
	private String oneTimeTaskScheduler(String  date, String time){
		//get Time
        String hour 	= getHour(time);
        String minutes 	= getMinute(time);
        String second	= getSecond(time);
		
        //get Date
        String day 		= getDay(date);
        String month 	= getMonth(date);
        String year 	= getYear(date);
        
        return second+" " + minutes + " " + hour + " " + day + " " + month + " ? " + year;
	}
	
	private String repeatIntervalScheduler(String interval, String startDate, String endDate, String startTime, String endTime){
		//get Time
        String hour 	= (Integer.parseInt(getHour(endTime))-Integer.parseInt(getHour(startTime)) == 0) ? getHour(startTime) : getHour(startTime)+"-"+getHour(endTime);
        String minutes 	= (Integer.parseInt(getMinute(endTime))-Integer.parseInt(getMinute(startTime)) == 0) ? getMinute(startTime) : getMinute(startTime)+"-"+getMinute(endTime);
        String second	= (Integer.parseInt(getSecond(endTime))-Integer.parseInt(getSecond(startTime)) == 0) ? getSecond(startTime) : getSecond(startTime)+"-"+getSecond(endTime);
		
        //get Date
        String day 		= (Integer.parseInt(getDay(endDate))-Integer.parseInt(getDay(startDate)) == 0) ? getDay(startDate) : getDay(startDate)+"-"+getDay(endDate);
        String month 	= (Integer.parseInt(getMonth(endDate))-Integer.parseInt(getMonth(startDate)) == 0) ? getMonth(startDate) : getMonth(startDate)+"-"+getMonth(endDate);
        String year 	= (Integer.parseInt(getYear(endDate))-Integer.parseInt(getYear(startDate)) == 0) ? getYear(startDate) : getYear(startDate)+"-"+getYear(endDate);
        
        if (Integer.parseInt(interval) > 60) {
        	//interval each minutes
        	System.out.println("each minute = "+second+" " + minutes+"/"+Integer.parseInt(interval)/60+"" + " " + hour + " " + day + " " + month + " ? " + year);
        	//return second+" " + minutes+"/"+Integer.parseInt(interval)/60+"" + " " + hour + " " + day + " " + month + " ? " + year;
        	return second+" " + minutes+"/"+Integer.parseInt(interval)/60+"" + " " + "*" + " " + "*" + " " + "*" + " ? " + year;
		} else if (Integer.parseInt(interval) > 3600) {
			//interval each hours
			System.out.println("Each hour = "+second+" " + minutes + " " + hour+"/"+Integer.parseInt(interval)/3600+"" + " " + day + " " + month + " ? " + year);
			return second+" " + minutes + " " + hour+"/"+Integer.parseInt(interval)/3600+"" + " " + "*" + " " + "*" + " ? " + year;
		} else {
			//interval each seconds
			System.out.println("Each second = "+second+"/"+interval+" " + minutes + " " + hour + " " + day + " " + month + " ? " + year);
			return second+"/"+interval+" " + "*" + " " + "*" + " " + "*" + " " + "*" + " ? " + year;
		}
        
	}
	
	private String repeatTimeScheduler(String interval, String startDate, String endDate, String startTime, String endTime, 
										String maskYear, String maskMonth, String maskDay, String maskWeeklyDay, String maskHour, String maskMinute, String maskSecond){
		
		/*System.out.println("interval = "+interval);
		System.out.println("startDate = "+startDate);
		System.out.println("endDate = "+endDate);
		System.out.println("startTime = "+startTime);
		System.out.println("endTime = "+endTime);
		System.out.println("maskYear = "+maskYear);
		System.out.println("maskMonth = "+maskMonth);
		System.out.println("maskDay = "+maskDay);
		System.out.println("maskWeeklyDay = "+maskWeeklyDay);
		System.out.println("maskHour = "+maskHour);
		System.out.println("maskMinute = "+maskMinute);
		System.out.println("maskSecond = "+maskSecond);*/
		
		//get Time
        //String hour 	= getHour(startTime)+"-"+getHour(endTime);
        //String minutes 	= getMinute(startTime)+"-"+getMinute(endTime);
        //String second	= getSecond(startTime)+"-"+getSecond(endTime);
        
       /* System.out.println("String hour = "+hour);
        System.out.println("String minutes = "+minutes);
        System.out.println("String second = "+second);*/
		
        //get Date
        //String day 		= getDay(startDate)+"-"+getDay(endDate);
        //String month 	= getMonth(startDate)+"-"+getMonth(endDate);
        String year 	= getYear(startDate)+"-"+getYear(endDate);
        
        /*System.out.println("String day = "+day);
        System.out.println("String month = "+month);
        System.out.println("String year = "+year);*/
        
        //System.out.println("Result = "+second+" " + minutes + " " + hour + " " + day + " " + month + " ? " + year);
        
        if(maskSecond == null) maskSecond = "*";
        if(maskMinute == null) maskMinute = "*";
        if(maskHour.equalsIgnoreCase("-")) maskHour = "*";
        if(maskDay == null) maskDay = "*";
        if(maskMonth == null) maskMonth = "*";
        
        return maskSecond+" " + maskMinute + " " + maskHour + " " + maskDay + " " + maskMonth + " ? " + year;
	}
	
	private String getHour(String time){
		return (time.split("\\:")[0].charAt(0)) == '0' ? time.split("\\:")[0].substring(1) : time.split("\\:")[0];
	}
	
	private String getMinute(String time){
        return (time.split("\\:")[1]).charAt(0) == '0' ? time.split("\\:")[1].substring(1) : time.split("\\:")[1];
	}
	
	private String getSecond(String time){
		return (time.split("\\:")[2]).charAt(0) == '0' ? time.split("\\:")[2].substring(1) : time.split("\\:")[2];
	}
	
	private String getDay(String date){
        return date.split("\\/")[2];
	}
	
	private String getMonth(String date){
		return date.split("\\/")[1];
	}
	
	private String getYear(String date){
		return date.split("\\/")[0];
	}
}
