package com.mii.scheduler;

import java.text.SimpleDateFormat;
import java.util.Date;

import mii.span.scheduller.odbc.ACKLister;
import mii.span.scheduller.odbc.AckExecuted;
import mii.span.scheduller.odbc.ExtractSP2DFile;
import mii.span.scheduller.odbc.GenerateBankStatement;
import mii.span.scheduller.odbc.GetNACKUpdate;
import mii.span.scheduller.odbc.GetTransactionHistory;
import mii.span.scheduller.odbc.InsertBulkResponse;
import mii.span.scheduller.odbc.NewForceNACKScheduler;
import mii.span.scheduller.odbc.Penihilan;
import mii.span.scheduller.odbc.PostingScheduller;
import mii.span.scheduller.odbc.PutBankStatement;
import mii.span.scheduller.odbc.RejectedFilesScheduller;
import mii.span.scheduller.odbc.Request;
import mii.span.scheduller.odbc.SpanDataValidationSP2DNoScheduller;
import mii.span.scheduller.odbc.ToBeExecute;
import mii.span.scheduller.odbc.ValidateDataDetails;
import mii.span.scheduller.odbc.VoidLister;
import mii.span.util.BusinessUtil;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;
import org.quartz.PersistJobDataAfterExecution;

import com.mii.beans.CleanUpBROFile;
import com.mii.constant.Constants;
import com.mii.dao.SystemParameterDAO;
import com.mii.logger.Log;
import com.mii.logger.LoggerEvent;
import com.mii.models.SystemParameter;
import com.mii.span.scheduller.GenerateSorborScheduller;
import com.mii.span.scheduller.ResultToBeProcessScheduller;

@PersistJobDataAfterExecution
@DisallowConcurrentExecution
public class ExecutorScheduler implements Job{
	
	private static final Log log = Log.getInstance(ExecutorScheduler.class);

	@Override
	public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
		JobKey jobKey = jobExecutionContext.getJobDetail().getKey();
		SystemParameterDAO systemParameterDao = (SystemParameterDAO) BusinessUtil.getDao("sysParamDAO");
		SystemParameter flagSpanDataValidation = (SystemParameter) systemParameterDao.getValueParameter(Constants.SCH_RUN_FLAG_SPANDATAVALIDATION).get(0);
		SystemParameter flagExecuteTransaction = (SystemParameter) systemParameterDao.getValueParameter(Constants.SCH_RUN_FLAG_EXECUTETRANSACTION).get(0);
		SystemParameter flagResponseTransaction = (SystemParameter) systemParameterDao.getValueParameter(Constants.SCH_RUN_FLAG_RESPONSETRANSACTION).get(0);
		SystemParameter flagReportTransaction = (SystemParameter) systemParameterDao.getValueParameter(Constants.SCH_RUN_FLAG_REPORTTRANSACTION).get(0);
		SystemParameter flagGenerateBankStatement = (SystemParameter) systemParameterDao.getValueParameter(Constants.SCH_RUN_FLAG_CREATEBANKSTATEMENT).get(0);
		SystemParameter flagSendBankStatement = (SystemParameter) systemParameterDao.getValueParameter(Constants.SCH_RUN_FLAG_SENDBANKSTATEMENT).get(0);
		SystemParameter flagPenihilan = (SystemParameter) systemParameterDao.getValueParameter(Constants.SCH_RUN_FLAG_PENIHILAN).get(0);
		SystemParameter flagAutoRTBP = (SystemParameter) systemParameterDao.getValueParameter(Constants.SCH_RUN_FLAG_AUTO_RTBP).get(0);
		SystemParameter flagACKLister = (SystemParameter) systemParameterDao.getValueParameter(Constants.SCH_RUN_FLAG_ACK_LISTER).get(0);
		SystemParameter flagAutoOBSorbor = (SystemParameter) systemParameterDao.getValueParameter(Constants.SCH_RUN_FLAG_AUTO_OB_SORBOR).get(0);
		SystemParameter flagGetRekeningKoranData = (SystemParameter) systemParameterDao.getValueParameter(Constants.SCH_RUN_FLAG_GET_REKENING_KORAN_DATA).get(0);
		
		SystemParameter startSpanDataValidation = (SystemParameter) systemParameterDao.getValueParameter(Constants.SCH_START_FLAG_SPANDATAVALIDATION).get(0);
		SystemParameter startExecuteTransaction = (SystemParameter) systemParameterDao.getValueParameter(Constants.SCH_START_FLAG_EXECUTETRANSACTION).get(0);
		SystemParameter startResponseTransaction = (SystemParameter) systemParameterDao.getValueParameter(Constants.SCH_START_FLAG_RESPONSETRANSACTION).get(0);
		SystemParameter startReportTransaction = (SystemParameter) systemParameterDao.getValueParameter(Constants.SCH_START_FLAG_REPORTTRANSACTION).get(0);
		SystemParameter startSendBankStatement = (SystemParameter) systemParameterDao.getValueParameter(Constants.SCH_START_FLAG_SENDBANKSTATEMENT).get(0);
		SystemParameter startCutOffTime = (SystemParameter) systemParameterDao.getValueParameter(Constants.SPAN_CUT_OFF_TIME).get(0);
		SystemParameter startAutoRTBP = (SystemParameter) systemParameterDao.getValueParameter(Constants.SCH_START_FLAG_AUTO_RTBP).get(0);
		SystemParameter startACKLister = (SystemParameter) systemParameterDao.getValueParameter(Constants.SCH_START_FLAG_ACK_LISTER).get(0);
		SystemParameter startAutoOBSorbor = (SystemParameter) systemParameterDao.getValueParameter(Constants.SCH_START_FLAG_AUTO_OB_SORBOR).get(0);
		SystemParameter startGetRekeningKoranData = (SystemParameter) systemParameterDao.getValueParameter(Constants.SCH_START_FLAG_GET_REKENING_KORAN_DATA).get(0);
		
		SystemParameter endSpanDataValidation = (SystemParameter) systemParameterDao.getValueParameter(Constants.SCH_END_FLAG_SPANDATAVALIDATION).get(0);
//		SystemParameter endExecuteTransaction = (SystemParameter) systemParameterDao.getValueParameter(Constants.SCH_END_FLAG_EXECUTETRANSACTION).get(0);
		SystemParameter endResponseTransaction = (SystemParameter) systemParameterDao.getValueParameter(Constants.SCH_END_FLAG_RESPONSETRANSACTION).get(0);
		SystemParameter endReportTransaction = (SystemParameter) systemParameterDao.getValueParameter(Constants.SCH_END_FLAG_REPORTTRANSACTION).get(0);
		SystemParameter endSendBankStatement = (SystemParameter) systemParameterDao.getValueParameter(Constants.SCH_END_FLAG_SENDBANKSTATEMENT).get(0);
		SystemParameter endPenihilan = (SystemParameter) systemParameterDao.getValueParameter(Constants.SCH_END_FLAG_PENIHILAN).get(0);
		SystemParameter endAutoRTBP = (SystemParameter) systemParameterDao.getValueParameter(Constants.SCH_END_FLAG_AUTO_RTBP).get(0);
		SystemParameter endACKLister = (SystemParameter) systemParameterDao.getValueParameter(Constants.SCH_END_FLAG_ACK_LISTER).get(0);
		SystemParameter endAutoOBSorbor = (SystemParameter) systemParameterDao.getValueParameter(Constants.SCH_END_FLAG_AUTO_OB_SORBOR).get(0);
		SystemParameter endGetRekeningKoranData = (SystemParameter) systemParameterDao.getValueParameter(Constants.SCH_END_FLAG_GET_REKENING_KORAN_DATA).get(0);
		
		String schName = jobKey.toString().split("\\#")[1];
		switch (schName) {
			//SpanDataValidation Group
			case "SpanDataValidation":				if(check(flagSpanDataValidation,startSpanDataValidation,endSpanDataValidation)){new ValidateDataDetails().runSpanDataValidation();}break;
			case "ExtractSP2D":						if(check(flagSpanDataValidation,startSpanDataValidation,endSpanDataValidation)){new ExtractSP2DFile().run();}break;
			case "SpanDataValidationSP2DNo":		if(check(flagSpanDataValidation,startSpanDataValidation,endSpanDataValidation)){new SpanDataValidationSP2DNoScheduller().run();}break;
			case "SpanDataValidationForRejectFile":	if(check(flagSpanDataValidation,startSpanDataValidation,endSpanDataValidation)){new RejectedFilesScheduller().run();}break;
			
			//Execute Transaction Group
			case "ToBeExecuteGAJI": 				if(check(flagExecuteTransaction,startExecuteTransaction,startCutOffTime)){new ToBeExecute().run(Constants.GAJI);}break;
			case "ToBeExecuteGAJIR": 				if(check(flagExecuteTransaction,startExecuteTransaction,startCutOffTime)){new ToBeExecute().run(Constants.GAJIR);}break;
			
			//Response Transaction Group
			case "ExecuteDataReq":					if(check(flagResponseTransaction,startResponseTransaction,endResponseTransaction)){new Request().runExecuteDataReq();}break;
			case "InsertBulkFT":					if(check(flagResponseTransaction,startResponseTransaction,endResponseTransaction)){new Request().runInsertBulkFT();}break;
			case "PostingTransaction":				if(check(flagResponseTransaction,startResponseTransaction,endResponseTransaction)){new PostingScheduller().run();}break;
			case "InsertBulkResponse": 				if(check(flagResponseTransaction,startResponseTransaction,endResponseTransaction)){new InsertBulkResponse().run();}break;
			
			//ACKLister
			case "ACKLister" :						if(check(flagACKLister, startACKLister, endACKLister)){new ACKLister().autoSendAck();}break;
			//AutoOBSorbor
			case "AutoOBSorbor" :					if(check(flagAutoOBSorbor, startAutoOBSorbor, endAutoOBSorbor)){new GenerateSorborScheduller().run();}break;
			
			//Auto RTBP Group
			case "AutoReturGaji":					if(check(flagAutoRTBP,startAutoRTBP,endAutoRTBP)){new ResultToBeProcessScheduller().run(Constants.GAJI,Constants.RETUR);}break;
			case "AutoReturGajiR":					if(check(flagAutoRTBP,startAutoRTBP,endAutoRTBP)){new ResultToBeProcessScheduller().run(Constants.GAJIR,Constants.RETUR);}break;
			case "AutoRetryGaji":					if(check(flagAutoRTBP,startAutoRTBP,endAutoRTBP)){new ResultToBeProcessScheduller().run(Constants.GAJI,Constants.RETRY);}break;
			case "AutoRetryGajiR":					if(check(flagAutoRTBP,startAutoRTBP,endAutoRTBP)){new ResultToBeProcessScheduller().run(Constants.GAJIR,Constants.RETRY);}break;
			case "AutoSendACKGaji":					if(check(flagAutoRTBP,startAutoRTBP,endAutoRTBP)){new ResultToBeProcessScheduller().run(Constants.GAJI,Constants.SEND_ACK);}break;
			case "AutoSendACKGajiR":				if(check(flagAutoRTBP,startAutoRTBP,endAutoRTBP)){new ResultToBeProcessScheduller().run(Constants.GAJIR,Constants.SEND_ACK);}break;
			
			//ReportTransaction Group
			case "ACKExecuted": 					if(check(flagReportTransaction,startReportTransaction,endReportTransaction)){new AckExecuted().run();}break;
			
			//Penihilan Group
			case "ProcessPenihilan"	:				if(check(flagPenihilan,startCutOffTime,endPenihilan)){new Penihilan().run();}break;
			case "ForceNACK":						if(check(flagPenihilan,startCutOffTime,endPenihilan)){new GetNACKUpdate().run();}break;
			case "NewForceNACK":					if(check(flagPenihilan,startCutOffTime,endPenihilan)){new NewForceNACKScheduler().run();}break;
			case "ACKVoid":							if(check(flagPenihilan,startCutOffTime,endPenihilan)){new VoidLister().run();}break;
			
			//GetRekeningKoranData Group
			case "GetRekeningKoranData":			if(check(flagGetRekeningKoranData,startGetRekeningKoranData,endGetRekeningKoranData)){new GetTransactionHistory().run();}break;
			
			//GenerateBS Group
			case "GenerateBankStatement":			if(checkBSOnly(flagGenerateBankStatement)){new GenerateBankStatement().run();}break;
			
			//SendBS Group
			case "PutBankStatement":				if(check(flagSendBankStatement,startSendBankStatement,endSendBankStatement)){new PutBankStatement().run();}break;

			case "TestScheduller": 					new CleanUpBROFile().run(jobKey.toString());break;
			
			default: break;
		
		}
		System.out.println("End Switch "+schName);
		
		LoggerEvent.debugProcess(log, this.getClass().getName(), jobKey + " executing at " + new Date());
	}
	
	public boolean check(SystemParameter systemParameter, SystemParameter startParameter, SystemParameter endParameter){
		boolean status = false;
		SimpleDateFormat sdf = new SimpleDateFormat(Constants.BS_HLP_HOUR_FORMAT);
		String hour = sdf.format(new Date());
		if((Integer.parseInt(startParameter.getParam_value()) <= Integer.parseInt(hour) && 
				Integer.parseInt(endParameter.getParam_value()) >= Integer.parseInt(hour))){
			if(systemParameter.getParam_value().equals("1")){
				status = true;
			}
		}
		return status;
	}
	
	public boolean checkBSOnly(SystemParameter systemParameter){
		return systemParameter.getParam_value().equals("1");
	}

}
