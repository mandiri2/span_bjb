package com.mii.scheduler;

import java.util.HashMap;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.matchers.GroupMatcher;

import com.mii.cache.CommonHashMap;
import com.mii.constant.State;
import com.mii.logger.Log;
import com.mii.logger.LoggerEvent;

public class SchedulerFactory {
	
	private static final Log log = Log.getInstance(SchedulerFactory.class);
	
	private void putScheduler(String schedulerID, Scheduler scheduler){
		CommonHashMap.schedulerID.put(schedulerID, scheduler);
	}
	
	private Scheduler getScheduler(String schedulerID){
		return CommonHashMap.schedulerID.get(schedulerID);
	}
	
	private void removeScheduler(String schedulerID){
		CommonHashMap.schedulerID.remove(schedulerID);
	}
	
	private void putStateScheduler(String schedulerID, String state){
		CommonHashMap.schedulerState.put(schedulerID, state);
	}
	
	private void updateStateSCheduler(String schedulerID, String state){
		CommonHashMap.schedulerState.replace(schedulerID, state);
	}
	
	private String getStateScheduler(String schedulerID){
		return CommonHashMap.schedulerState.get(schedulerID);
	}
	
	private void removeStateScheduler(String schedulerID){
		CommonHashMap.schedulerState.remove(schedulerID);
	}
	
	protected void initScheduler(String schedulerID) throws SchedulerException{
		
		StdSchedulerFactory stdSchedulerFactory = new StdSchedulerFactory();
		Scheduler scheduler = stdSchedulerFactory.getScheduler();
		
		putScheduler(schedulerID, scheduler);		
	}
		
	protected void startScheduler(String schedulerID){
		try{
			Scheduler scheduler = getScheduler(schedulerID);
			scheduler.start();
			putStateScheduler(schedulerID, State.Started);
		}catch(Exception e){
			e.printStackTrace();
			LoggerEvent.errorProcess(log, this.getClass().getName()+"/"+this.getClass().getMethods().toString(), e.getMessage());
		}
	}
	
	protected void stopScheduler(String schedulerID){
		try{
			Scheduler scheduler = getScheduler(schedulerID);
			
			scheduler.shutdown();
			
			updateStateSCheduler(schedulerID, State.Shutdown);
		}catch(Exception e){
			e.printStackTrace();
			LoggerEvent.errorProcess(log, this.getClass().getName()+"/"+this.getClass().getMethods().toString(), e.getMessage());
		}
	}
	
	protected String statusScheduler(String schedulerID){
		try {
			if (getStateScheduler(schedulerID)==State.Started) {
				return State.Started;
			}else if (getStateScheduler(schedulerID)==State.Shutdown) {
				return State.Shutdown;
			}else if (getStateScheduler(schedulerID)==State.Standby) {
				return State.Standby;
			}else {
				return State.NotFound;
			}
		} catch (Exception e) {
			e.printStackTrace();
			LoggerEvent.errorProcess(log, this.getClass().getName()+"/"+this.getClass().getMethods().toString(), e.getMessage());
			return State.Error;
		}
	}
	
	protected void pauseScheduler(String schedulerID){
		try{
			Scheduler scheduler = getScheduler(schedulerID);
			//scheduler.pauseAll();
			
			//pause using jobkey
			for (String groupName : scheduler.getJobGroupNames()){
				for (JobKey jobKey : scheduler.getJobKeys(GroupMatcher.jobGroupEquals(groupName))) {
					String jobName = jobKey.getName();
//					String jobGroup = jobKey.getGroup();
					
					if(schedulerID.equalsIgnoreCase(jobName)){
//						System.out.println("jobname = "+jobName);
//						System.out.println("jobgroup = "+jobGroup);
						scheduler.pauseJob(jobKey);
					}
				}
			}
							
				  
			
			System.out.println("[PAUSE SCHEDULLER]Pause scheduller with ID = "+schedulerID);
			
			
			
			updateStateSCheduler(schedulerID, State.Standby);
		}catch(Exception e){
			e.printStackTrace();
			LoggerEvent.errorProcess(log, this.getClass().getName()+"/"+this.getClass().getMethods().toString(), e.getMessage());
		}
		
	}
	
	protected void resumeScheduler(String schedulerID){
		try{
			Scheduler scheduler = getScheduler(schedulerID);
			//scheduler.resumeAll();
			
			
			//resume using jobkey
			for (String groupName : scheduler.getJobGroupNames()){
				for (JobKey jobKey : scheduler.getJobKeys(GroupMatcher.jobGroupEquals(groupName))) {
					String jobName = jobKey.getName();
//					String jobGroup = jobKey.getGroup();
					
					if(schedulerID.equalsIgnoreCase(jobName)){
//						System.out.println("jobname = "+jobName);
//						System.out.println("jobgroup = "+jobGroup);
						scheduler.resumeJob(jobKey);
					}
				}
			}
			System.out.println("[RESUME SCHEDULLER]Resume scheduller with ID = "+schedulerID);
			
			
			updateStateSCheduler(schedulerID, State.Started);
		}catch(Exception e){
			e.printStackTrace();
			LoggerEvent.errorProcess(log, this.getClass().getName()+"/"+this.getClass().getMethods().toString(), e.getMessage());
		}
		
	}
	
	protected void createJob(String schedulerID, String jobName, String executionService, String schedulerType, String  date, String time, String interval, String startDate, String endDate, String startTime, String endTime, String maskYear, String maskMonth, String maskDay, String maskWeeklyDay, String maskHour, String maskMinute, String maskSecond, HashMap<String, String> parameter) throws Exception{
		String dateExecution = new SchedulerType().run(schedulerType, date, time, interval, startDate, endDate, startTime, endTime, maskYear, maskMonth, maskDay, maskWeeklyDay, maskHour, maskMinute, maskSecond);
		
		registerJob( schedulerID,  jobName,  dateExecution,  executionService, parameter);
	}
	
	private void registerJob(String schedulerID, String jobName, String dateExecution, String executionService, HashMap<String, String> parameter) throws Exception {
		Scheduler scheduler = getScheduler(schedulerID);
		
		new JobFactory().createJob(scheduler, "Trigger#"+schedulerID, schedulerID, "Group#"+schedulerID, dateExecution, parameter);
	}
	
	protected void summaryScheduler(String schedulerID) throws SchedulerException{
		Scheduler scheduler = getScheduler(schedulerID);
		
		scheduler.shutdown();
		
		removeScheduler(schedulerID);
		removeStateScheduler(schedulerID);
		
		scheduler=null;
	}
	
}
