package com.mii.scheduler;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

import java.util.Date;
import java.util.HashMap;

import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;

import com.mii.logger.Log;
import com.mii.logger.LoggerEvent;

public class JobFactory {
	
	private static final Log log = Log.getInstance(JobFactory.class);
	
	protected void createJob(Scheduler scheduler, String triggerName,String jobName, String groupName, String dateExecution, HashMap<String, String> parameter) throws SchedulerException{
		JobDetail job 		= newJob(ExecutorScheduler.class).withIdentity(jobName, groupName).build();
		CronTrigger trigger = newTrigger().withIdentity(triggerName, groupName).withSchedule(cronSchedule(dateExecution)).build();
	
		job.getJobDataMap().put(job.getKey().toString(), parameter);
		job.isConcurrentExectionDisallowed();
		Date ft = scheduler.scheduleJob(job, trigger);
		
		System.out.println("Job name = "+jobName);
		System.out.println("Gorup name = "+groupName);
		System.out.println("[JOB KEY] Job Key registered = "+job.getKey().toString());
		LoggerEvent.debugProcess(log, this.getClass().getName(), job.getKey() + " has been scheduled to run at: " + ft + " and repeat based on expression: "+ trigger.getCronExpression());
		
	}
}
