package com.mii.scheduler;

import java.util.HashMap;

import org.quartz.SchedulerException;

import com.mii.util.CommonDate;
import com.mii.logger.Log;
import com.mii.logger.LoggerEvent;
import com.mii.scheduler.SchedulerFactory;

public class SchedulerConfig {
	
	private static final Log log = Log.getInstance(SchedulerConfig.class);
	  
	private SchedulerFactory schedulerFactory;
	public static SchedulerConfig instance;
	
	public static SchedulerConfig getInstance(){
	    if (instance == null) {
	    	instance 			= new SchedulerConfig();
	    }
	    
	    return instance;
	}
	
	public String createScheduler(String jobName, String executionService, String schedulerType, String  date, String time, String interval, String startDate, String endDate, String startTime, String endTime, String maskYear, String maskMonth, String maskDay, String maskWeeklyDay, String maskHour, String maskMinute, String maskSecond, HashMap<String, String> parameter){
		
		String schedulerID = "";
		try{
			schedulerID = jobName+"#"+CommonDate.getCurrentDateString("ddMMyyyyHHmmss")+"#"+executionService;
			
			schedulerFactory = new SchedulerFactory();
			schedulerFactory.initScheduler(schedulerID);
			
			schedulerFactory.createJob(schedulerID, jobName, executionService, schedulerType, date, time, interval, startDate, endDate, startTime, endTime, maskYear, maskMonth, maskDay, maskWeeklyDay, maskHour, maskMinute, maskSecond, parameter);
		}catch(Exception e){
			e.printStackTrace();
			LoggerEvent.errorProcess(log, this.getClass().getName()+"/"+this.getClass().getMethods().toString(), e.getMessage());
		}
		return schedulerID;
	}

	public void startScheduler(String schedulerID){
		try{
			schedulerFactory = new SchedulerFactory();
			schedulerFactory.startScheduler(schedulerID);
		}catch(Exception e){
			e.printStackTrace();
			LoggerEvent.errorProcess(log, this.getClass().getName()+"/"+this.getClass().getMethods().toString(), e.getMessage());
		}
	}
	
	public void stopScheduler(String schedulerID){
		try{
			schedulerFactory = new SchedulerFactory();
			schedulerFactory.stopScheduler(schedulerID);
		}catch(Exception e){
			e.printStackTrace();
			LoggerEvent.errorProcess(log, this.getClass().getName()+"/"+this.getClass().getMethods().toString(), e.getMessage());
		}
	}
	
	public String statusScheduler(String schedulerID){
		schedulerFactory = new SchedulerFactory();
		return schedulerFactory.statusScheduler(schedulerID);
	}
	
	public void pauseScheduler(String schedulerID){
		try{
			schedulerFactory = new SchedulerFactory();
			schedulerFactory.pauseScheduler(schedulerID);
		}catch(Exception e){
			e.printStackTrace();
			LoggerEvent.errorProcess(log, this.getClass().getName()+"/"+this.getClass().getMethods().toString(), e.getMessage());
		}
	}
	
	public void resumeScheduler(String schedulerID){
		try{
			schedulerFactory = new SchedulerFactory();
			schedulerFactory.resumeScheduler(schedulerID);
		}catch(Exception e){
			e.printStackTrace();
			LoggerEvent.errorProcess(log, this.getClass().getName()+"/"+this.getClass().getMethods().toString(), e.getMessage());
		}
	}
	
	public void summaryScheduler(String schedulerID){
		try{
			schedulerFactory = new SchedulerFactory();
			schedulerFactory.summaryScheduler(schedulerID);
		}catch(SchedulerException e){
			e.printStackTrace();
			LoggerEvent.errorProcess(log, this.getClass().getName()+"/"+this.getClass().getMethods().toString(), e.getMessage());
		}
	}
}
