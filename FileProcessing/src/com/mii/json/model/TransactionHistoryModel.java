package com.mii.json.model;

public class TransactionHistoryModel {

	private String ZLAB;
	private String ZLEAN;
	private String ZLAN;
	private String ZLAS;
	private String ZLCUS;
	private String ZLCLC;
	private String ZLCAS;
	private String ZLBRNM;
	private String ZLVFRZ;
	private String ZLVTOZ;
	private String ZLSTN;
	private String ZLSHN;
	private String ZLCUN;
	private String ZLDAS;
	private String ZLVFRE;
	private String ZLVTOE;
	private String ZHACZC;
	private String ZLATD;
	private String ZLCCYD;
	private String ZLEANE;
	private String ZLIBAN;
	private String ZLHD1;
	private String ZLHD2;
	private String ZHTIT2;
	private String ZHDRF;
	private String ZHNR;
	private String ZHVDT;
	private String ZHT2DT;
	private String ZHVFR;
	private String ZHAMDR;
	private String ZHAMCR;
	private String ZHBALD;
	private String ZHRBAL;
	private String ZHAB;
	private String ZHAN;
	private String ZHAS;
	private String ZHCTP;
	private String ZHACZ;
	private String ZHEVNK;
	private String ZHSTN;
	private String ZHSDAT;
	private String ZHTCD;
	private String ZHPOD;
	private String ZHPSQ;
	private String ZHPBR;
	private String ZHBRNM;
	private String ZHNAR2;
	private String ZHNAR3;
	private String ZHNAR4;
	private String ZHA001;
	private String ZHA002;
	private String ZHA003;
	private String ZHA004;
	private String ZHA005;
	private String ZHA006;
	private String ZHOCCY;
	private String ZHOAMT;

	public TransactionHistoryModel() {
		super();
	}

	public String getZLAB() {
		return ZLAB;
	}

	public void setZLAB(String zLAB) {
		ZLAB = zLAB;
	}

	public String getZLEAN() {
		return ZLEAN;
	}

	public void setZLEAN(String zLEAN) {
		ZLEAN = zLEAN;
	}

	public String getZLAN() {
		return ZLAN;
	}

	public void setZLAN(String zLAN) {
		ZLAN = zLAN;
	}

	public String getZLAS() {
		return ZLAS;
	}

	public void setZLAS(String zLAS) {
		ZLAS = zLAS;
	}

	public String getZLCUS() {
		return ZLCUS;
	}

	public void setZLCUS(String zLCUS) {
		ZLCUS = zLCUS;
	}

	public String getZLCLC() {
		return ZLCLC;
	}

	public void setZLCLC(String zLCLC) {
		ZLCLC = zLCLC;
	}

	public String getZLCAS() {
		return ZLCAS;
	}

	public void setZLCAS(String zLCAS) {
		ZLCAS = zLCAS;
	}

	public String getZLBRNM() {
		return ZLBRNM;
	}

	public void setZLBRNM(String zLBRNM) {
		ZLBRNM = zLBRNM;
	}

	public String getZLVFRZ() {
		return ZLVFRZ;
	}

	public void setZLVFRZ(String zLVFRZ) {
		ZLVFRZ = zLVFRZ;
	}

	public String getZLVTOZ() {
		return ZLVTOZ;
	}

	public void setZLVTOZ(String zLVTOZ) {
		ZLVTOZ = zLVTOZ;
	}

	public String getZLSTN() {
		return ZLSTN;
	}

	public void setZLSTN(String zLSTN) {
		ZLSTN = zLSTN;
	}

	public String getZLSHN() {
		return ZLSHN;
	}

	public void setZLSHN(String zLSHN) {
		ZLSHN = zLSHN;
	}

	public String getZLCUN() {
		return ZLCUN;
	}

	public void setZLCUN(String zLCUN) {
		ZLCUN = zLCUN;
	}

	public String getZLDAS() {
		return ZLDAS;
	}

	public void setZLDAS(String zLDAS) {
		ZLDAS = zLDAS;
	}

	public String getZLVFRE() {
		return ZLVFRE;
	}

	public void setZLVFRE(String zLVFRE) {
		ZLVFRE = zLVFRE;
	}

	public String getZLVTOE() {
		return ZLVTOE;
	}

	public void setZLVTOE(String zLVTOE) {
		ZLVTOE = zLVTOE;
	}

	public String getZHACZC() {
		return ZHACZC;
	}

	public void setZHACZC(String zHACZC) {
		ZHACZC = zHACZC;
	}

	public String getZLATD() {
		return ZLATD;
	}

	public void setZLATD(String zLATD) {
		ZLATD = zLATD;
	}

	public String getZLCCYD() {
		return ZLCCYD;
	}

	public void setZLCCYD(String zLCCYD) {
		ZLCCYD = zLCCYD;
	}

	public String getZLEANE() {
		return ZLEANE;
	}

	public void setZLEANE(String zLEANE) {
		ZLEANE = zLEANE;
	}

	public String getZLIBAN() {
		return ZLIBAN;
	}

	public void setZLIBAN(String zLIBAN) {
		ZLIBAN = zLIBAN;
	}

	public String getZLHD1() {
		return ZLHD1;
	}

	public void setZLHD1(String zLHD1) {
		ZLHD1 = zLHD1;
	}

	public String getZLHD2() {
		return ZLHD2;
	}

	public void setZLHD2(String zLHD2) {
		ZLHD2 = zLHD2;
	}

	public String getZHTIT2() {
		return ZHTIT2;
	}

	public void setZHTIT2(String zHTIT2) {
		ZHTIT2 = zHTIT2;
	}

	public String getZHDRF() {
		return ZHDRF;
	}

	public void setZHDRF(String zHDRF) {
		ZHDRF = zHDRF;
	}

	public String getZHNR() {
		return ZHNR;
	}

	public void setZHNR(String zHNR) {
		ZHNR = zHNR;
	}

	public String getZHVDT() {
		return ZHVDT;
	}

	public void setZHVDT(String zHVDT) {
		ZHVDT = zHVDT;
	}

	public String getZHT2DT() {
		return ZHT2DT;
	}

	public void setZHT2DT(String zHT2DT) {
		ZHT2DT = zHT2DT;
	}

	public String getZHVFR() {
		return ZHVFR;
	}

	public void setZHVFR(String zHVFR) {
		ZHVFR = zHVFR;
	}

	public String getZHAMDR() {
		return ZHAMDR;
	}

	public void setZHAMDR(String zHAMDR) {
		ZHAMDR = zHAMDR;
	}

	public String getZHAMCR() {
		return ZHAMCR;
	}

	public void setZHAMCR(String zHAMCR) {
		ZHAMCR = zHAMCR;
	}

	public String getZHBALD() {
		return ZHBALD;
	}

	public void setZHBALD(String zHBALD) {
		ZHBALD = zHBALD;
	}

	public String getZHRBAL() {
		return ZHRBAL;
	}

	public void setZHRBAL(String zHRBAL) {
		ZHRBAL = zHRBAL;
	}

	public String getZHAB() {
		return ZHAB;
	}

	public void setZHAB(String zHAB) {
		ZHAB = zHAB;
	}

	public String getZHAN() {
		return ZHAN;
	}

	public void setZHAN(String zHAN) {
		ZHAN = zHAN;
	}

	public String getZHAS() {
		return ZHAS;
	}

	public void setZHAS(String zHAS) {
		ZHAS = zHAS;
	}

	public String getZHCTP() {
		return ZHCTP;
	}

	public void setZHCTP(String zHCTP) {
		ZHCTP = zHCTP;
	}

	public String getZHACZ() {
		return ZHACZ;
	}

	public void setZHACZ(String zHACZ) {
		ZHACZ = zHACZ;
	}

	public String getZHEVNK() {
		return ZHEVNK;
	}

	public void setZHEVNK(String zHEVNK) {
		ZHEVNK = zHEVNK;
	}

	public String getZHSTN() {
		return ZHSTN;
	}

	public void setZHSTN(String zHSTN) {
		ZHSTN = zHSTN;
	}

	public String getZHSDAT() {
		return ZHSDAT;
	}

	public void setZHSDAT(String zHSDAT) {
		ZHSDAT = zHSDAT;
	}

	public String getZHTCD() {
		return ZHTCD;
	}

	public void setZHTCD(String zHTCD) {
		ZHTCD = zHTCD;
	}

	public String getZHPOD() {
		return ZHPOD;
	}

	public void setZHPOD(String zHPOD) {
		ZHPOD = zHPOD;
	}

	public String getZHPSQ() {
		return ZHPSQ;
	}

	public void setZHPSQ(String zHPSQ) {
		ZHPSQ = zHPSQ;
	}

	public String getZHPBR() {
		return ZHPBR;
	}

	public void setZHPBR(String zHPBR) {
		ZHPBR = zHPBR;
	}

	public String getZHBRNM() {
		return ZHBRNM;
	}

	public void setZHBRNM(String zHBRNM) {
		ZHBRNM = zHBRNM;
	}

	public String getZHNAR2() {
		return ZHNAR2;
	}

	public void setZHNAR2(String zHNAR2) {
		ZHNAR2 = zHNAR2;
	}

	public String getZHNAR3() {
		return ZHNAR3;
	}

	public void setZHNAR3(String zHNAR3) {
		ZHNAR3 = zHNAR3;
	}

	public String getZHNAR4() {
		return ZHNAR4;
	}

	public void setZHNAR4(String zHNAR4) {
		ZHNAR4 = zHNAR4;
	}

	public String getZHA001() {
		return ZHA001;
	}

	public void setZHA001(String zHA001) {
		ZHA001 = zHA001;
	}

	public String getZHA002() {
		return ZHA002;
	}

	public void setZHA002(String zHA002) {
		ZHA002 = zHA002;
	}

	public String getZHA003() {
		return ZHA003;
	}

	public void setZHA003(String zHA003) {
		ZHA003 = zHA003;
	}

	public String getZHA004() {
		return ZHA004;
	}

	public void setZHA004(String zHA004) {
		ZHA004 = zHA004;
	}

	public String getZHA005() {
		return ZHA005;
	}

	public void setZHA005(String zHA005) {
		ZHA005 = zHA005;
	}

	public String getZHA006() {
		return ZHA006;
	}

	public void setZHA006(String zHA006) {
		ZHA006 = zHA006;
	}

	public String getZHOCCY() {
		return ZHOCCY;
	}

	public void setZHOCCY(String zHOCCY) {
		ZHOCCY = zHOCCY;
	}

	public String getZHOAMT() {
		return ZHOAMT;
	}

	public void setZHOAMT(String zHOAMT) {
		ZHOAMT = zHOAMT;
	}

}
