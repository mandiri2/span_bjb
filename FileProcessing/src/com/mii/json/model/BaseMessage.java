package com.mii.json.model;

import java.util.List;

import com.mii.json.AccountBalance;

public class BaseMessage {

	private String MT;
	private String PC;
	private String PCC;
	private String CC;
	private String MC;
	private String CID;
	private String DT;
	private String ST;
	private String RC;
	private Object RCMSG;
	private String FC;
	private Object MPI;
	private Object MPO;
	private String SID;
	private String SPPU;
	private String SPPW;
	private String AUDITUID;
	private String BRANCHCD;
	private String REMOTEIP;

	public String getMT() {
		return MT;
	}
	
	public void setMT(String MT) {
		this.MT = MT;
	}
	
	public String getPC() {
		return PC;
	}

	public void setPC(String pC) {
		PC = pC;
	}

	public String getPCC() {
		return PCC;
	}

	public void setPCC(String pCC) {
		PCC = pCC;
	}

	public String getCC() {
		return CC;
	}

	public void setCC(String cC) {
		CC = cC;
	}

	public String getMC() {
		return MC;
	}

	public void setMC(String mC) {
		MC = mC;
	}

	public String getCID() {
		return CID;
	}

	public void setCID(String cID) {
		CID = cID;
	}

	public String getDT() {
		return DT;
	}

	public void setDT(String dT) {
		DT = dT;
	}

	public String getST() {
		return ST;
	}

	public void setST(String sT) {
		ST = sT;
	}

	public String getRC() {
		return RC;
	}

	public void setRC(String rC) {
		RC = rC;
	}

	public String getFC() {
		return FC;
	}

	public void setFC(String fC) {
		FC = fC;
	}

	public Object getMPI() {
		return MPI;
	}

	public void setMPI(Object mPI) {
		MPI = mPI;
	}

	public String getSID() {
		return SID;
	}

	public void setSID(String sID) {
		SID = sID;
	}

	public String getSPPU() {
		return SPPU;
	}

	public void setSPPU(String sPPU) {
		SPPU = sPPU;
	}

	public String getSPPW() {
		return SPPW;
	}

	public void setSPPW(String sPPW) {
		SPPW = sPPW;
	}

	public String getAUDITUID() {
		return AUDITUID;
	}

	public void setAUDITUID(String aUDITUID) {
		AUDITUID = aUDITUID;
	}

	public String getBRANCHCD() {
		return BRANCHCD;
	}

	public void setBRANCHCD(String bRANCHCD) {
		BRANCHCD = bRANCHCD;
	}

	public String getREMOTEIP() {
		return REMOTEIP;
	}

	public void setREMOTEIP(String rEMOTEIP) {
		REMOTEIP = rEMOTEIP;
	}

	public Object getRCMSG() {
		return RCMSG;
	}

	public void setRCMSG(Object rCMSG) {
		RCMSG = rCMSG;
	}

	public Object getMPO() {
		return MPO;
	}

	public void setMPO(Object mPO) {
		MPO = mPO;
	}

}
