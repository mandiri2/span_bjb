package com.mii.json;

import java.util.Comparator;

import mii.span.model.SpanRekeningKoran;


public class RekKoranComparator implements Comparator<SpanRekeningKoran>{

	@Override
	public int compare(SpanRekeningKoran o1, SpanRekeningKoran o2) {
		return Double.valueOf(o1.getKey()).compareTo(Double.valueOf(o2.getKey()));
	}

}
