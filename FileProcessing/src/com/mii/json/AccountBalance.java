package com.mii.json;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.logging.Logger;

import mii.span.model.FASTGapuraAccount;

import com.google.gson.Gson;
import com.mii.bjb.core.GapuraConnector;
import com.mii.constant.Constants;
import com.mii.dao.SystemParameterDAO;
import com.mii.json.model.AccountBalanceModel;
import com.mii.json.model.BaseMessage;
import com.mii.models.SystemParameter;

public class AccountBalance {
	
	static Logger logger = Logger.getAnonymousLogger();
	
	public static String STA() {
		SimpleDateFormat sdf = new SimpleDateFormat("ddHHmmssSSS");
		return sdf.format(new Date()).substring(1, 10);
	}
	
	public static String packAccountBalance(String remoteIp, String CID, String accountNo) {
		BaseMessage base = new BaseMessage();
		AccountBalanceModel ab = new AccountBalanceModel();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		
		base.setPCC("5");
		base.setCC("0001");
		base.setDT(sdf.format(new Date()));
		base.setST(STA());
		base.setPC("001001");
		base.setMC("90023");
		base.setMT("9200");
		base.setFC("AB");
		base.setREMOTEIP(remoteIp);
		base.setSID("singleUserIDWebTeller");
		base.setCID(CID);
		
		//Internal Account//
		/*ab.setZLAB("0001");
		ab.setZLAS("360");
		ab.setZLAN("800459");*/
		
		//External Account
		ab.setZLEAN(accountNo);
//		ab.setZLEAN("0023269392100");
		
		base.setMPI(ab);
		
		Gson gson = new Gson();
		String request = gson.toJson(base);
		System.out.println("AccountBalance request message : "+request);
		return request;
		
	}
	
	public static String getAccountBalance(SystemParameterDAO systemParameterDao, String accountNo){
		SystemParameter systemParameterIP = systemParameterDao.getValueParameter(Constants.SPAN_LOCAL_IP).get(0);
		SystemParameter systemParameterCID = systemParameterDao.getValueParameter(Constants.GAPURA_CID).get(0);
		String request = packAccountBalance(systemParameterIP.getParam_value(), systemParameterCID.getParam_value(), accountNo);
		String response = GapuraConnector.connect(request, systemParameterDao);
		AccountBalanceModel abm = unpackAccountBalance(response);
		String balance = abm.getZLBAL();
		return balance;
	}
	
	public static boolean checkAccountAndName(SystemParameterDAO systemParameterDao, String accountNo, String accountName){
		SystemParameter systemParameterIP = systemParameterDao.getValueParameter(Constants.SPAN_LOCAL_IP).get(0);
		SystemParameter systemParameterCID = systemParameterDao.getValueParameter(Constants.GAPURA_CID).get(0);
		String request = packAccountBalance(systemParameterIP.getParam_value(), systemParameterCID.getParam_value(), accountNo);
		String response = GapuraConnector.connect(request, systemParameterDao);
		AccountBalanceModel abm = unpackAccountBalance(response);
		String oAccountName = abm.getZLCUN();
		boolean status = false;
		if(compareAccountNo(oAccountName, accountName, Constants.START_INDEX_NAME_VALIDATION, Constants.END_INDEX_NAME_VALIDATION)){
			status = true;
		}
		return status;
	}
	
	@SuppressWarnings("unchecked")
	public static AccountBalanceModel unpackAccountBalance(String response) {
		Gson gson = new Gson();
		BaseMessage base = gson.fromJson(response, BaseMessage.class);
		
		AccountBalanceModel abm = new AccountBalanceModel();
		String ZLBAL = Constants.ERROR;
		String ZLCUN = Constants.ERROR;
		String ZLAB = "";
		abm.setZLBAL(ZLBAL);
		abm.setZLCUN(ZLCUN);
		abm.setZLAB(ZLAB);
		try{
			abm.setZLRC(base.getRC());
		}catch(Exception e){
			e.printStackTrace();
		}
		try{
			Map<String, Object> jsonMapList = (Map<String, Object>) base.getMPO();
			for(int i=0; i<jsonMapList.size(); i++){
				Map<String, Object> jsonABList = (Map<String, Object>) jsonMapList.get(Integer.toString(i));
				ZLBAL = jsonABList.get("ZLBAL").toString();
				ZLBAL = ZLBAL.trim();
				ZLBAL = ZLBAL.replace(",", "");
//				ZLSHN = jsonABList.get("ZLSHN").toString();
				ZLCUN = jsonABList.get("ZLCUN").toString();
				ZLAB = jsonABList.get("ZLAB").toString();
				abm.setZLBAL(ZLBAL);
				abm.setZLCUN(ZLCUN);
				abm.setZLAB(ZLAB);
			}
		}catch(Exception e){
			System.out.println("Error Parsing JSON Response Account Balance");
			e.printStackTrace();
		}
		return abm;
	}

	public static FASTGapuraAccount checkAccountAndNameGapura(
			SystemParameterDAO systemParameterDao, String accountNumber,
			String accountName) {
		
		FASTGapuraAccount account = new FASTGapuraAccount();
		
		SystemParameter systemParameterIP = systemParameterDao.getValueParameter(Constants.SPAN_LOCAL_IP).get(0);
		SystemParameter systemParameterCID = systemParameterDao.getValueParameter(Constants.GAPURA_CID).get(0);
		String request = packAccountBalance(systemParameterIP.getParam_value(), systemParameterCID.getParam_value(), accountNumber);
		
		try{
			String response = GapuraConnector.connect(request, systemParameterDao);
			AccountBalanceModel abm = unpackAccountBalance(response);
			if(abm.getZLRC().equalsIgnoreCase(Constants.GAPURA_RC_SUCCESS) || abm.getZLRC().equalsIgnoreCase(Constants.GAPURA_RC_NOT_FOUND)){
				if(Constants.ERROR.equalsIgnoreCase(abm.getZLCUN())){
					account.setDescriptionDetail(Constants.SALAH_NO_REKENING);
					account.setBranch(abm.getZLAB());
					account.setIsValid(Constants.FAST_NOT_FOUND);
				}
				else{
					String oAccountName = abm.getZLCUN();
					if(compareAccountNo(oAccountName, accountName, Constants.START_INDEX_NAME_VALIDATION, Constants.END_INDEX_NAME_VALIDATION)){
						account.setDescriptionDetail(Constants.FAST_FOUND);
						account.setBranch(abm.getZLAB());
						account.setIsValid(Constants.FAST_FOUND);
					}
					else {
						account.setDescriptionDetail(Constants.SALAH_NAMA);
						account.setBranch(abm.getZLAB());
						account.setIsValid(Constants.FAST_NOT_FOUND);
					}
				}
			}else{
				account.setDescriptionDetail(Constants.GAPURA_TIMEOUT);
				account.setIsValid(Constants.GAPURA_CONNECTION_TIMEOUT);
			}
		}catch(Exception e){
			account.setDescriptionDetail(Constants.GAPURA_TIMEOUT);
			account.setIsValid(Constants.GAPURA_CONNECTION_TIMEOUT);
		}
		
		return account;
	}
	
	private static boolean compareAccountNo(String string1, String string2, int startIndex, int endIndex){
		if(substringIfFit(string1, startIndex, endIndex).equals(substringIfFit(string2, startIndex, endIndex))){
			return true;
		}
		return false;
	}
	
	private static String substringIfFit(String input, int startIndex, int endIndex){
		if(input.length()>endIndex){
			return input.substring(startIndex, endIndex);
		}
		return input;
	}


}
