package com.mii.json;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.FileReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import mii.span.model.SpanRekeningKoran;

import org.apache.commons.lang.StringUtils;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import com.json.JSONObject;
import com.mii.bjb.core.GapuraConnector;
import com.mii.bjb.core.GapuraLoopConnector;
import com.mii.constant.Constants;
import com.mii.constant.JsonConstant;
import com.mii.dao.SystemParameterDAO;
import com.mii.json.model.BaseMessage;
import com.mii.json.model.TransactionHistoryModel;

public class TransactionHistory {
	final static String cid = "TG-CB-00-0000017";
	final static byte cEndMessageByte = 3;

	public static String getTransactionHistoryGeneral(
			SystemParameterDAO systemParameterDao, String accountNumber,
			Date start, Date end) {
		String systemParameterIP = systemParameterDao.getValueByParamName(Constants.SPAN_LOCAL_IP);
		String systemParameterCID = systemParameterDao.getValueByParamName(Constants.GAPURA_CID);

		// pack request
		String request = packTransactionHistory(systemParameterIP, systemParameterCID, accountNumber, start, end);

		//get history gapura connector
		return GapuraConnector.connect(request, systemParameterDao);
		
		// ***********get history gapura loop connector (new ivan kata tito)
		// pack request
		/*byte[] request = packTransactionHistory(systemParameterIP, systemParameterCID, accountNumber, start, end);
		return GapuraLoopConnector.connect(request, systemParameterDao);*/
	}

	public static List<SpanRekeningKoran> getTransactionHistory(
			SystemParameterDAO systemParameterDao, String accountNumber,
			Date start, Date end) {
		List<SpanRekeningKoran> list = unpackTransactionHistory(getTransactionHistoryGeneral(
				systemParameterDao, accountNumber, start, end));
		return list;
	}

	public static List<SpanRekeningKoran> getTransactionHistoryCreditOnly(
			SystemParameterDAO systemParameterDao, String accountNumber,
			Date start, Date end) {
		List<SpanRekeningKoran> list = unpackTransactionHistoryCreditOnly(getTransactionHistoryGeneral(
				systemParameterDao, accountNumber, start, end));
		return list;
	}

	public static String packTransactionHistory(String remoteIp, String CID,
			String accountNo, Date start, Date end) {
		// fixing request message
		
		BaseMessage base = new BaseMessage();
		TransactionHistoryModel th = new TransactionHistoryModel();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		SimpleDateFormat thDate = new SimpleDateFormat("ddMMYY");
		
		base.setPCC("5");
		base.setCC("0001");
		base.setDT(sdf.format(new Date()));
		base.setST("22147483647");
		base.setPC("001001");
		base.setMC("90023");
		base.setMT("9200");
		base.setFC("TH");
		base.setREMOTEIP(remoteIp);
		base.setSID("singleUserIDWebTeller");
		base.setCID(CID);
		
		th.setZLEAN(accountNo);
		th.setZLVFRZ(thDate.format(start));
		th.setZLVTOZ(thDate.format(end));
		
		base.setMPI(th);
		
		Gson gson = new Gson();
		String request = gson.toJson(base);
		System.out.println("TransactionHistory request message : "+request);
		return request;
		 
		
		/*//=======================gapura loop==============================
		JSONObject json_out;
		ByteArrayOutputStream mybytearray = null;

		// tanggal untuk ke TG
		//DateFormat dateFormat = new SimpleDateFormat("MMddHHmmss");
		DateFormat dateFormat2 = new SimpleDateFormat("yyyyMMddHHmmss");
		DateFormat thDate = new SimpleDateFormat("ddMMYY");
		Date date = new Date();
		//String dt = String.valueOf(dateFormat.format(date));
		String sdf = String.valueOf(dateFormat2.format(date));

		//========================param test==============================
		CID = cid;
//		accountNo = "0026643155100";
		accountNo = "0072932706001";
		try {
			start = new SimpleDateFormat("ddMMyyyy").parse("01102016");
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
//		end = new Date();
		//========================param test==============================
		
		byte[] result;
		try {
			json_out = new JSONObject();
			json_out.put("MT", "9200");
			json_out.put("PC", "001001");
			json_out.put("PCC", "5");
			json_out.put("CC", "0001");
			json_out.put("MC", "90023");
			json_out.put("CID", CID);
			json_out.put("DT", sdf);
			//json_out.put("ST", "0101000000");
			json_out.put("ST", "22147483647");
			json_out.put("FC", "TH");

			JSONObject mpi = new JSONObject();
			// mpi.put("ZLEAN", "0072932706001");
			mpi.put("ZLEAN", accountNo);
//			mpi.put("ZLVTOZ", thDate.format(end));
			mpi.put("ZLVTOZ", "311016");
			mpi.put("ZLVFRZ", thDate.format(start));
			mpi.put("PGNUM", "1");
			JSONObject MPI_OUT = mpi;
			// MPI_OUT.put("ZLEAN", acc_num);
			// MPI_OUT.put("ZLVTOZ", acc_num);
			// MPI_OUT.put("ZLVFRZ", acc_num);
			// MPI_OUT.put("PGNUM", acc_num);

			json_out.put("MPI", MPI_OUT);
			json_out.put("SID", "singleUserIDWebTeller");
			json_out.put("SPPU", "");
			json_out.put("SPPW", "");

			System.out
					.println("[GAPURA LOOP]Get Balance ( Request Message to TG ) : "
							+ json_out.toString());
			mybytearray = new ByteArrayOutputStream();
			mybytearray.write(json_out.toString().getBytes());
			mybytearray.write(cEndMessageByte);

			result = mybytearray.toByteArray();
		} catch (Exception e) {
			result = null;
			System.out.println("[GAPURA LOOP]Module Check Balance Agent Error");
			System.out.println("[GAPURA LOOP]" + e.toString());
		}
		return result;
		//=======================gapura loop==============================
	*/
	}

	@SuppressWarnings("unchecked")
	public static List<SpanRekeningKoran> unpackTransactionHistory(
			String response) {
		BaseMessage base = getBaseMessage(response);
		List<SpanRekeningKoran> listRekKoran = new ArrayList<SpanRekeningKoran>();
		try {
			Map<String, Object> jsonMapList = getJsonMapList(base);
			Set<String> keys = jsonMapList.keySet();
			for (Iterator<String> it = keys.iterator(); it.hasNext();) {
				String key = (String) it.next();
				// get what needed
				LinkedTreeMap<String, Object> map = (LinkedTreeMap<String, Object>) jsonMapList
						.get(key);
				String ZHAMDR = map.get(JsonConstant.ZHAMDR).toString();
				String ZHAMCR = map.get(JsonConstant.ZHAMCR).toString();
				String ZHPOD = map.get(JsonConstant.ZHPOD).toString();
				String ZHRBAL = map.get(JsonConstant.ZHRBAL).toString();
				String ZHTCD = "";
				try{
					ZHTCD = map.get("ZHTCD").toString();
				}catch(Exception e){}
				String ZHNR = map.get(JsonConstant.ZHNR).toString();

				SpanRekeningKoran rek = setRekeningKoran(key, ZHAMDR, ZHAMCR,
						ZHPOD, ZHRBAL, ZHTCD, ZHNR);
				listRekKoran.add(rek);
			}

			// sorting by key
			Collections.sort(listRekKoran, new RekKoranComparator()); // JAVA 7
			// listRekKoran.sort(new RekKoranComparator()); //FIXME JAVA8
			for (SpanRekeningKoran s : listRekKoran) {
				System.out.println("key " + s.getKey() + " "
						+ s.getTransactionDate());
			}
		} catch (Exception e) {
			System.out
					.println("Error Parsing JSON Response Transaction History");
			e.printStackTrace();
		}
		return listRekKoran;
	}

	@SuppressWarnings("unchecked")
	public static List<SpanRekeningKoran> unpackTransactionHistoryCreditOnly(
			String response) {
		BaseMessage base = getBaseMessage(response);
		List<SpanRekeningKoran> listRekKoran = new ArrayList<SpanRekeningKoran>();
		try {
			Map<String, Object> jsonMapList = getJsonMapList(base);
			Set<String> keys = jsonMapList.keySet();
			for (Iterator<String> it = keys.iterator(); it.hasNext();) {
				String key = (String) it.next();
				LinkedTreeMap<String, Object> map = (LinkedTreeMap<String, Object>) jsonMapList
						.get(key);
				String ZHAMCR = map.get(JsonConstant.ZHAMCR).toString();
				if (StringUtils.isEmpty(ZHAMCR)) {
					continue;
				} else {
					String ZHAMDR = map.get(JsonConstant.ZHAMDR).toString();
					String ZHPOD = map.get(JsonConstant.ZHPOD).toString();
					String ZHRBAL = map.get(JsonConstant.ZHRBAL).toString();
					String ZHTCD = "";
					try{
						ZHTCD = map.get("ZHTCD").toString();
					}catch(Exception e){}
					String ZHNR = map.get(JsonConstant.ZHNR).toString();
					SpanRekeningKoran rek = setRekeningKoran(key, ZHAMDR,
							ZHAMCR, ZHPOD, ZHRBAL, ZHTCD, ZHNR);
					listRekKoran.add(rek);
				}
			}
		} catch (Exception e) {
			System.out
					.println("Error Parsing JSON Response Transaction History CREDIT ONLY");
			e.printStackTrace();
		}
		return listRekKoran;
	}

	private static Map<String, Object> getJsonMapList(BaseMessage base) {
		Map<String, Object> jsonMapList = (Map<String, Object>) base.getMPO();
		// System.out.println("json map list size : " + jsonMapList.size());
		return jsonMapList;
	}

	private static BaseMessage getBaseMessage(String response) {
		Gson gson = new Gson();
		BaseMessage base = gson.fromJson(response, BaseMessage.class);
		return base;
	}

	private static SpanRekeningKoran setRekeningKoran(String key,
			String ZHAMDR, String ZHAMCR, String ZHPOD, String ZHRBAL,
			String ZHTCD, String ZHNR) {
		SpanRekeningKoran rek = new SpanRekeningKoran();
		rek.setKey(Integer.parseInt(key));
		rek.setTransactionDate(ZHPOD);
		rek.setBankTransactionCode(ZHTCD);
		rek.setDebit(ZHAMDR);
		rek.setCredit(ZHAMCR);
		rek.setBankReferenceNumber(ZHNR.replaceAll("SPAN ", ""));
		rek.setTotalAmount(ZHRBAL);
		return rek;
	}

	// method util
	public static List<SpanRekeningKoran> byPassRekeningKoranCreditOnly() {
		System.out
				.println("*********************PLEASE DISABLE byPassRekeningKoranCreditOnly ");
		BufferedReader br = null;
		FileReader fileReader = null;
		StringBuilder sb = null;
		try {
			fileReader = new FileReader("/opt/response.txt");
			br = new BufferedReader(fileReader);
			sb = new StringBuilder();
			String line = br.readLine();

			while (line != null) {
				sb.append(line);
				sb.append(System.lineSeparator()); // untuk menambahkan \n untuk
													// line baru
				line = br.readLine();
			}
			String everything = sb.toString();
			// System.out.println(everything);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)
					br.close();
				if (fileReader != null)
					fileReader.close();
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}

		return unpackTransactionHistoryCreditOnly(sb.toString());
	}

}
