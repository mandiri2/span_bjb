package com.mii.sftp;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import com.mii.constant.State;
import com.mii.logger.Log;
import com.mii.logger.LoggerEvent;

public class SFTPPrivateKey {
	
	private static final Log log = Log.getInstance(SFTPPrivateKey.class);

	String host;
	String username;
	String port;
	String privateKeyPath;
	String knownHost;
	
	Session     session     = null;  
    Channel     channel     = null;  
    ChannelSftp channelSftp = null; 
    
    public SFTPPrivateKey(String host, String username, String port, String privateKeyPath, String knownHost) {
    	this.host = host;
		this.username = username;
		this.port = port;
		this.privateKeyPath = privateKeyPath;
		this.knownHost = knownHost;
		
		createConnection();
	}
    
    private void createConnection(){
    	
    	try {
			JSch jsch = new JSch();
			jsch.setKnownHosts(knownHost);
			jsch.addIdentity(privateKeyPath);
			
			
			//session = jsch.getSession(username,host,Integer.parseInt(port));
			session = jsch.getSession(username, host);
	        session.connect();  
	        
	        channel = session.openChannel("sftp");  
	        channel.connect();
	        
		} catch (JSchException e) {
			e.printStackTrace();
			LoggerEvent.errorProcess(log, this.getClass().getName()+"/"+this.getClass().getMethods().toString(), e.getMessage());
		}
    }
    
    public int statusConnection(){
    	return channel.isConnected() ? 1 : 0;
    }
    
    public void removeConnection(){
    	channel.disconnect();
    	session.disconnect();
    }
    
    @SuppressWarnings("unchecked")
	public int checkFile(String workingPath){
    	Vector<ChannelSftp.LsEntry> list = null;
    	List<String> xFile = new ArrayList<String>();
		try {
			channelSftp = (ChannelSftp)channel;
			
	    	list = channelSftp.ls(workingPath);
			list.size();
			
			for (int i = 0; i < list.size(); i++) {
				if (list.get(i).getFilename().substring(4, 5).equalsIgnoreCase("X")) xFile.add(list.get(i).getFilename());
			}
		} catch (SftpException e) {
			e.printStackTrace();
			LoggerEvent.errorProcess(log, this.getClass().getName()+"/"+this.getClass().getMethods().toString(), e.getMessage());
		}
        
    	return xFile.isEmpty() ? 1 : 0;
    }
    
    @SuppressWarnings("unchecked")
	public int checkSpecificFile(String workingPath, String fileName){
    	Vector<ChannelSftp.LsEntry> list = null;
    	List<String> xFile = new ArrayList<String>();
		try {
			channelSftp = (ChannelSftp)channel;
			
	    	list = channelSftp.ls(workingPath);
			
			for (int i = 0; i < list.size(); i++) {
				if (list.get(i).getFilename().equalsIgnoreCase(fileName)) xFile.add(list.get(i).getFilename());
			}
		} catch (SftpException e) {
			e.printStackTrace();
			LoggerEvent.errorProcess(log, this.getClass().getName()+"/"+this.getClass().getMethods().toString(), e.getMessage());
		}
        
    	return xFile.isEmpty() ? 1 : 0;
    }
    
    @SuppressWarnings("unchecked")
	public String cutFile(String workingPath, String localPath){
    	Vector<ChannelSftp.LsEntry> list = null;
    	List<String> xFile = new ArrayList<String>();
		try {
			channelSftp = (ChannelSftp)channel;
			
	    	list = channelSftp.ls(workingPath);
	    	for (int i = 0; i < list.size(); i++) {
				if (list.get(i).getFilename().substring(4, 5).equalsIgnoreCase("X")){
					String a = list.get(i).getFilename().substring(0, 3);
					String b = list.get(i).getFilename().substring(5);
					xFile.add(a+b);
				}
			}
	    	
	    	for (int i = 0; i < xFile.size(); i++) {
	    		channelSftp.get(workingPath.concat(xFile.get(i)), localPath.concat(xFile.get(i)));
	    		
    			channelSftp.rm(workingPath.concat(xFile.get(i)));
			}
	    	
	    	return State.Success;
		} catch (SftpException e) {
			e.printStackTrace();
			LoggerEvent.errorProcess(log, this.getClass().getName()+"/"+this.getClass().getMethods().toString(), e.getMessage());
			return State.Failed;
		}
    }
    
    @SuppressWarnings("unchecked")
	public String cutSpecificFile(String workingPath, String localPath, String fileName){
    	Vector<ChannelSftp.LsEntry> list = null;
    	List<String> xFile = new ArrayList<String>();
    	//List<String> anchorFile = new ArrayList<String>();
		try {
			channelSftp = (ChannelSftp)channel;
			
	    	list = channelSftp.ls(workingPath);
	    	for (int i = 0; i < list.size(); i++) {
	    		if (list.get(i).getFilename().equalsIgnoreCase(fileName)) {
	    			xFile.add(fileName);
	    			
	    		}
	    	}
			
	    	for (int i = 0; i < xFile.size(); i++) {
	    		channelSftp.get(workingPath.concat(xFile.get(i)), localPath.concat(xFile.get(i)));
	    		
    			channelSftp.rm(workingPath.concat(xFile.get(i)));
    			
    			String a = xFile.get(i).substring(0, 3);
    			String b = xFile.get(i).substring(3);
    			String c = a.concat("X".concat(b));
    			
    			channelSftp.rm(workingPath.concat(c));
			}
	    	
	    	return State.Success;
		} catch (SftpException e) {
			e.printStackTrace();
			LoggerEvent.errorProcess(log, this.getClass().getName()+"/"+this.getClass().getMethods().toString(), e.getMessage());
			return State.Failed;
		}
    }
    
    @SuppressWarnings("unchecked")
	public String getFile(String workingPath, String localPath){
    	Vector<ChannelSftp.LsEntry> list = null;
		try {
			channelSftp = (ChannelSftp)channel;
			
	    	list = channelSftp.ls(workingPath);
	    	
	    	for(ChannelSftp.LsEntry oList : list) if (!oList.getAttrs().isDir()) channelSftp.get(workingPath.concat(oList.getFilename()), localPath.concat(oList.getFilename()));
	    	
	    	return State.Success;
		} catch (SftpException e) {
			e.printStackTrace();
			LoggerEvent.errorProcess(log, this.getClass().getName()+"/"+this.getClass().getMethods().toString(), e.getMessage());
			return State.Failed;
		}
    }
    
    public String putFile(String workingPath, String localPath, String fileName){
    	try {
			channelSftp = (ChannelSftp)channel;
			
	    	channelSftp.put(workingPath.concat(fileName), localPath);
	    	
	    	return State.Success;
		} catch (SftpException e) {
			e.printStackTrace();
			LoggerEvent.errorProcess(log, this.getClass().getName()+"/"+this.getClass().getMethods().toString(), e.getMessage());
			return State.Failed;
		}
    }
    
}
