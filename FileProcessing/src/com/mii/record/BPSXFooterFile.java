package com.mii.record;

import com.ancientprogramming.fixedformat4j.annotation.Align;
import com.ancientprogramming.fixedformat4j.annotation.Field;
import com.ancientprogramming.fixedformat4j.annotation.Record;

@Record
public class BPSXFooterFile {

	private String totalData;
	private String endingBalance;
	private String endLine;

	@Field(offset=1, length=6, align = Align.LEFT, paddingChar = ' ')
	public String getTotalData() {
		return totalData;
	}
	public void setTotalData(String totalData) {
		this.totalData = totalData;
	}
	
	@Field(offset=7, length=15, align = Align.LEFT, paddingChar = ' ')
	public String getEndingBalance() {
		return endingBalance;
	}
	public void setEndingBalance(String endingBalance) {
		this.endingBalance = endingBalance;
	}
	
	@Field(offset=22, length=1, align = Align.LEFT, paddingChar = ' ')
	public String getEndLine() {
		return endLine;
	}
	public void setEndLine(String endLine) {
		this.endLine = endLine;
	}
}
