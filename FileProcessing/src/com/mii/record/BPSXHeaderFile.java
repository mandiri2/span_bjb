package com.mii.record;

import com.ancientprogramming.fixedformat4j.annotation.Align;
import com.ancientprogramming.fixedformat4j.annotation.Field;
import com.ancientprogramming.fixedformat4j.annotation.Record;

@Record
public class BPSXHeaderFile {

	private String accountNo;
	private String bankStatementDate;
	private String currency;
	private String beginningBalance;
	
	@Field(offset=1, length=20, align = Align.LEFT, paddingChar = ' ')
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	
	@Field(offset=21, length=8, align = Align.LEFT, paddingChar = ' ')
	public String getBankStatementDate() {
		return bankStatementDate;
	}
	public void setBankStatementDate(String bankStatementDate) {
		this.bankStatementDate = bankStatementDate;
	}
	
	@Field(offset=29, length=3, align = Align.LEFT, paddingChar = ' ')
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	@Field(offset=32, length=15, align = Align.LEFT, paddingChar = ' ')
	public String getBeginningBalance() {
		return beginningBalance;
	}
	public void setBeginningBalance(String beginningBalance) {
		this.beginningBalance = beginningBalance;
	}
}
