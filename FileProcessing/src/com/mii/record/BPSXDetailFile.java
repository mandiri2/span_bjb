package com.mii.record;

import com.ancientprogramming.fixedformat4j.annotation.Align;
import com.ancientprogramming.fixedformat4j.annotation.Field;
import com.ancientprogramming.fixedformat4j.annotation.Record;

@Record
public class BPSXDetailFile {

	private String transactionNo;//4
	private String transactionCode;//3
	private String debitCredit;//1
	private String bankRefNo;//16
	private String transactionDate;//8
	private String valueDate;//8
	private String originalAmount;//15
	
	@Field(offset=1, length=6, align = Align.LEFT, paddingChar = ' ')
	public String getTransactionNo() {
		return transactionNo;
	}
	public void setTransactionNo(String transactionNo) {
		this.transactionNo = transactionNo;
	}
	
	@Field(offset=7, length=3, align = Align.LEFT, paddingChar = ' ')
	public String getTransactionCode() {
		return transactionCode;
	}
	public void setTransactionCode(String transactionCode) {
		this.transactionCode = transactionCode;
	}
	
	@Field(offset=10, length=1, align = Align.LEFT, paddingChar = ' ')
	public String getDebitCredit() {
		return debitCredit;
	}
	public void setDebitCredit(String debitCredit) {
		this.debitCredit = debitCredit;
	}
	
	@Field(offset=11, length=16, align = Align.LEFT, paddingChar = ' ')
	public String getBankRefNo() {
		return bankRefNo;
	}
	public void setBankRefNo(String bankRefNo) {
		this.bankRefNo = bankRefNo;
	}
	
	@Field(offset=27, length=8, align = Align.LEFT, paddingChar = ' ')
	public String getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}
	
	@Field(offset=35, length=8, align = Align.LEFT, paddingChar = ' ')
	public String getValueDate() {
		return valueDate;
	}
	public void setValueDate(String valueDate) {
		this.valueDate = valueDate;
	}
	
	@Field(offset=43, length=15, align = Align.LEFT, paddingChar = ' ')
	public String getOriginalAmount() {
		return originalAmount;
	}
	public void setOriginalAmount(String originalAmount) {
		this.originalAmount = originalAmount;
	}

}
