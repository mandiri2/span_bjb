package com.mii.record;

import com.ancientprogramming.fixedformat4j.annotation.Align;
import com.ancientprogramming.fixedformat4j.annotation.Field;
import com.ancientprogramming.fixedformat4j.annotation.Record;

@Record
public class RTGSResponseFile {

	private String accountCurrency;
	private String sourceAccountNumber;
	private String destinationAccountNumber;
	private String destinationAccountName;
	private String transactionAmount;
	private String paymentType;
	private String bankCode;
	private String description1;
	private String description2;
	private String description3;
	private String description4;
	private String referenceNo;
	private String timeStamp;
	private String userID;
	private String response;
	private String responseDesc;
	
	@Field(offset=1, length=3, align = Align.LEFT, paddingChar = ' ')
	public String getAccountCurrency() {
		return accountCurrency;
	}
	public void setAccountCurrency(String accountCurrency) {
		this.accountCurrency = accountCurrency;
	}
	
	@Field(offset=4, length=20, align = Align.LEFT, paddingChar = ' ')
	public String getSourceAccountNumber() {
		return sourceAccountNumber;
	}
	public void setSourceAccountNumber(String sourceAccountNumber) {
		this.sourceAccountNumber = sourceAccountNumber;
	}
	
	@Field(offset=24, length=20, align = Align.LEFT, paddingChar = ' ')
	public String getDestinationAccountNumber() {
		return destinationAccountNumber;
	}
	public void setDestinationAccountNumber(String destinationAccountNumber) {
		this.destinationAccountNumber = destinationAccountNumber;
	}
	
	@Field(offset=44, length=35, align = Align.LEFT, paddingChar = ' ')
	public String getDestinationAccountName() {
		return destinationAccountName;
	}
	public void setDestinationAccountName(String destinationAccountName) {
		this.destinationAccountName = destinationAccountName;
	}
	
	@Field(offset=79, length=15, align = Align.LEFT, paddingChar = '0')
	public String getTransactionAmount() {
		if(transactionAmount.isEmpty() || "".equalsIgnoreCase(transactionAmount)){
			transactionAmount = "0";
		}
		return transactionAmount;
	}
	public void setTransactionAmount(String transactionAmount) {
		this.transactionAmount = transactionAmount;
	}
	
	@Field(offset=94, length=3, align = Align.LEFT, paddingChar = ' ')
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	
	@Field(offset=97, length=10, align = Align.LEFT, paddingChar = ' ')
	public String getBankCode() {
		return bankCode;
	}
	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}
	
	@Field(offset=107, length=35, align = Align.LEFT, paddingChar = ' ')
	public String getDescription1() {
		return description1;
	}
	public void setDescription1(String description1) {
		this.description1 = description1;
	}
	
	@Field(offset=142, length=35, align = Align.LEFT, paddingChar = ' ')
	public String getDescription2() {
		return description2;
	}
	public void setDescription2(String description2) {
		this.description2 = description2;
	}
	
	@Field(offset=177, length=35, align = Align.LEFT, paddingChar = ' ')
	public String getDescription3() {
		return description3;
	}
	public void setDescription3(String description3) {
		this.description3 = description3;
	}
	
	@Field(offset=212, length=35, align = Align.LEFT, paddingChar = ' ')
	public String getDescription4() {
		return description4;
	}
	public void setDescription4(String description4) {
		this.description4 = description4;
	}
	
	@Field(offset=247, length=16, align = Align.LEFT, paddingChar = ' ')
	public String getReferenceNo() {
		return referenceNo.trim();
	}
	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}
	
	@Field(offset=263, length=26, align = Align.LEFT, paddingChar = ' ')
	public String getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
	
	@Field(offset=289, length=4, align = Align.LEFT, paddingChar = ' ')
	public String getUserID() {
		return userID;
	}
	public void setUserID(String userID) {
		this.userID = userID;
	}
	
	@Field(offset=293, length=1, align = Align.LEFT, paddingChar = ' ')
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	
	@Field(offset=294, length=37, align = Align.LEFT, paddingChar = ' ')
	public String getResponseDesc() {
		return responseDesc;
	}
	public void setResponseDesc(String responseDesc) {
		this.responseDesc = responseDesc;
	}
}
