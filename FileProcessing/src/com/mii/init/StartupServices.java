package com.mii.init;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.mii.constant.CommonConstant;
import com.mii.logger.Log;
import com.mii.logger.LoggerConfig;

public class StartupServices implements ServletContextListener{

	private static final Log log = Log.getInstance(StartupServices.class);
	
	private void initiateLog4j(){
		LoggerConfig loggerConfig = new LoggerConfig();
		loggerConfig.configLog(CommonConstant.defaultLog4jFile);
		log.debug("init services");
	}

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		initiateLog4j();
	}
}
