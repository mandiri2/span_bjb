package com.mii.scoped;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

@ManagedBean(name="sessionBean")
@SessionScoped
@Resource(name="jdbc/Corp_GW")
public class SessionBean implements Serializable {
	private static final long serialVersionUID = 1L;	
	//private User user;
	private Map<String, Object> paramMap = new HashMap<String, Object>();
	//private MenuModel menuModel; 
	private Set<Integer> actionList = new HashSet<Integer>();
	private String test;
	
	@ManagedProperty(value="#{applicationBean}")
	private ApplicationBean applicationBean;
	
	@PostConstruct
	public void init(){
	}	

	public String getTest() {
		return test;
	}

	public void setTest(String test) {
		this.test = test;
	}
	
	public boolean hasAction(Integer menuId){
		return actionList.contains(menuId);
	}
	
	public void putParam(String key, Object value){
		paramMap.put(key, value);
	}
	public Object getAndRemoveParam(String key){
		return paramMap.remove(key);
	}
	public ApplicationBean getApplicationBean() {
		return applicationBean;
	}

	public void setApplicationBean(ApplicationBean applicationBean) {
		this.applicationBean = applicationBean;
	}

}
