package com.mii.scoped;

import javax.faces.bean.ManagedProperty;

public class ApplicationInstance {
	@ManagedProperty(value="#{applicationBean}")
	private ApplicationBean applicationBean;

	public static ApplicationInstance instance;
	
	public static ApplicationInstance getInstance(){
	    if (instance == null) {
	    	instance = new ApplicationInstance();
	    	instance.setApplicationBean();
	    }
	    
	    return instance;
	}
	
	private void setApplicationBean(){
		applicationBean = new ApplicationBean();
		applicationBean.postConstruct();
	}
	
	public ApplicationBean getApplicationBean(){
		return applicationBean;
	}
}
