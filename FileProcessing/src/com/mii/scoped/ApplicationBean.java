package com.mii.scoped;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.mii.constant.CommonConstant;

@ManagedBean(eager=true, name="applicationBean")
@ApplicationScoped
@Resource(name="jdbc/Corp_GW")
public class ApplicationBean implements Serializable{
	private static final long serialVersionUID = 1L;
	private ApplicationContext fileProcessing;
	private ApplicationContext corpGWContext;
	
	public ApplicationBean(){}
	
	@PostConstruct
	public void postConstruct(){
		fileProcessing = new ClassPathXmlApplicationContext(CommonConstant.contextFile);
		corpGWContext = new ClassPathXmlApplicationContext(CommonConstant.contextFile);
	}
	
	public ApplicationContext getFileProcessContext() {
		return fileProcessing;
	}

	public void setFileProcessContext(ApplicationContext fileProcessing) {
		this.fileProcessing = fileProcessing;
	}

	public ApplicationContext getCorpGWContext() {
		return corpGWContext;
	}

	public void setCorpGWContext(ApplicationContext corpGWContext) {
		this.corpGWContext = corpGWContext;
	}
}
