package com.mii.logger;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class Log {
	private Logger m_logger = null;
	private static String m_loggerName = "MPNG2";
	private static String m_mutex = "mutex";
	private static boolean m_isInitialized = false;

	public static Log getInstance() {
		return new Log();
	}

	@SuppressWarnings("rawtypes")
	public static Log getInstance(Class clazz) {
		return new Log(m_loggerName);
	}

	public static Log getInstance(String name) {
		return new Log(name);
	}

	public static void init(Properties runtimeProperties) {
		try {
			PropertyConfigurator.configure(runtimeProperties);
		} catch (Exception ex) {
			initLog4J();
		}

		synchronized (m_mutex) {
			m_isInitialized = true;
		}
	}

	public static void init(String configFile) {
		try {
			FileInputStream log4jFileInputStream = new FileInputStream(configFile);
			Properties log4jruntimeProperties = new Properties();
			log4jruntimeProperties.load(log4jFileInputStream);

			PropertyConfigurator.configure(log4jruntimeProperties);

			if (log4jFileInputStream != null)
				try {
					log4jFileInputStream.close();
				} catch (Exception localException1) {}
		} catch (Exception ex) {
			initLog4J();
			ex.printStackTrace();
		}

		synchronized (m_mutex) {
			m_isInitialized = true;
		}
	}

	public boolean isDebugEnabled() {
		return this.m_logger.isDebugEnabled();
	}

	public boolean isInfoEnabled() {
		return this.m_logger.isInfoEnabled();
	}

	public boolean isWarnEnabled() {
		return this.m_logger.isEnabledFor(Level.WARN);
	}

	public boolean isErrorEnabled() {
		return this.m_logger.isEnabledFor(Level.ERROR);
	}

	public boolean isFatalEnabled() {
		return this.m_logger.isEnabledFor(Level.FATAL);
	}

	public void debug(Object message) {
		try {
			if (this.m_logger.isDebugEnabled())
				this.m_logger.debug(message);
		} catch (Throwable localThrowable) {}
	}

	public void debug(Object message, Throwable throwable) {
		try {
			if (this.m_logger.isDebugEnabled())
				this.m_logger.debug(message, throwable);
		} catch (Throwable localThrowable) {}
	}

	public void info(Object message) {
		try {
			if (this.m_logger.isInfoEnabled())
				this.m_logger.info(message);
		} catch (Throwable localThrowable) {}
	}

	public void info(Object message, Throwable throwable) {
		try {
			if (this.m_logger.isInfoEnabled())
				this.m_logger.info(message, throwable);
		} catch (Throwable localThrowable) {}
	}

	public void warn(Object message) {
		try {
			if (this.m_logger.isEnabledFor(Level.WARN))
				this.m_logger.warn(message);
		} catch (Throwable localThrowable) {}
	}

	public void warn(Object message, Throwable throwable) {
		try {
			if (this.m_logger.isEnabledFor(Level.WARN))
				this.m_logger.warn(message, throwable);
		} catch (Throwable localThrowable) {}
	}

	public void error(Object message) {
		try {
			if (this.m_logger.isEnabledFor(Level.ERROR))
				this.m_logger.error(message);
		} catch (Throwable localThrowable) {}
	}

	public void error(Object message, Throwable throwable) {
		try {
			if (this.m_logger.isEnabledFor(Level.ERROR))
				this.m_logger.error(message, throwable);
		} catch (Throwable localThrowable) {}
	}

	public void fatal(Object message) {
		try {
			if (this.m_logger.isEnabledFor(Level.FATAL))
				this.m_logger.fatal(message);
		} catch (Throwable localThrowable) {}
	}

	public void fatal(Object message, Throwable throwable) {
		try {
			if (this.m_logger.isEnabledFor(Level.FATAL))
				this.m_logger.fatal(message, throwable);
		} catch (Throwable localThrowable) {}
	}

	private static void init() {
		if (!m_isInitialized) {
			initLog4J();
			synchronized (m_mutex) {
				m_isInitialized = true;
			}
		}
	}

	private static void initLog4J() {
		try {
			InputStream propertiesInputStream = Thread.currentThread()
					.getContextClassLoader()
					.getResourceAsStream("log4j.properties");
			Properties log4jProperties = new Properties();
			log4jProperties.load(propertiesInputStream);
			PropertyConfigurator.configure(log4jProperties);
		} catch (Exception ex) {
			BasicConfigurator.configure();
		}
	}

	private Log() {
		init();
		this.m_logger = Logger.getRootLogger();
	}

	@SuppressWarnings("rawtypes")
	private Log(Class clazz) {
		init();
		this.m_logger = Logger.getLogger(clazz);
	}

	private Log(String name) {
		init();
		this.m_logger = Logger.getLogger(name);
	}
}
