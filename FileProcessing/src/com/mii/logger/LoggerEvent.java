package com.mii.logger;

public class LoggerEvent {

	public static void infoEvent(Log log, String event, String process, String result){
		log.info("["+process+"]"+" "+event+" "+result);
	}
	
	public static void debugEvent(Log log, String event, String process, String result){
		log.debug("["+process+"]"+" "+event+" "+result);
	}
	
	public static void debugProcess(Log log, String process, String detailProcess, String result){
		log.debug("["+process+"]"+" "+detailProcess+" "+result);
	}
	
	public static void debugProcess(Log log, String className, String result){
		log.debug("["+className+"]"+" "+result);
	}
	
	public static void warnProcess(Log log, String process, String detailProcess, String result){
		log.warn("["+process+"]"+" "+detailProcess+" "+result);
	}
	
	public static void errorProcess(Log log, String process, String detailProcess, String result){
		log.error("["+process+"]"+" "+detailProcess+" "+result);
	}
	
	public static void errorProcess(Log log, String className, String result){
		log.error("["+className+"]"+" "+result);
	}
}
