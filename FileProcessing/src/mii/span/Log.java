package mii.span;

import mii.span.util.PubUtil;

public class Log {
	
	public static void debugLog(String sysID, String svcName, String type, String message){
		String concatMsg = PubUtil.concat("[",sysID,"]","[",svcName,"]","[",type,"]",message);
		System.out.println(concatMsg);
	}
	
}
