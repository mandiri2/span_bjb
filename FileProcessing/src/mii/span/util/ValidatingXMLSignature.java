package mii.span.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.security.KeyStore;
import java.security.cert.X509Certificate;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.ws.custom.security.WSSConfig;
import org.apache.ws.custom.security.WSSecurityEngine;
import org.apache.ws.custom.security.WSSecurityEngineResult;
import org.apache.ws.custom.security.components.crypto.Merlin;
import org.apache.ws.custom.security.util.Loader;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public final class ValidatingXMLSignature
{
  static final ValidatingXMLSignature _instance = new ValidatingXMLSignature();
  
  static ValidatingXMLSignature _newInstance()
  {
    return new ValidatingXMLSignature();
  }
  
  static ValidatingXMLSignature _cast(Object paramObject)
  {
    return (ValidatingXMLSignature)paramObject;
  }
  
//  public static final void validatingSignature(IData paramIData) throws ServiceException
//  {
//    IDataCursor localIDataCursor = paramIData.getCursor();
//    try
//    {
//      String str = IDataUtil.getString(localIDataCursor, "filePath");
//      localObject1 = IDataUtil.getString(localIDataCursor, "ksPath");
//      localObject2 = IDataUtil.getString(localIDataCursor, "ksPassword");
//      
//      ValidatingXMLSignature localValidatingXMLSignature = new ValidatingXMLSignature();
//      
//      Document localDocument = localValidatingXMLSignature.loadDocumentAlt(str);
//      Class localClass = localValidatingXMLSignature.getClass();
//      
//      boolean bool = localValidatingXMLSignature.checkSignedDoc(localDocument, (String)localObject1, (String)localObject2, localClass);
//      
//      IDataUtil.put(localIDataCursor, "status", bool ? "valid" : "invalid");
//    }
//    catch (Exception localException)
//    {
//      Object localObject1 = new StringWriter();
//      Object localObject2 = new PrintWriter((Writer)localObject1);
//      localException.printStackTrace((PrintWriter)localObject2);
//      IDataUtil.put(localIDataCursor, "status", "error");
//      IDataUtil.put(localIDataCursor, "ex", ((StringWriter)localObject1).toString());
//    }
//    finally
//    {
//      localIDataCursor.destroy();
//    }
//  }
  
  public Document loadDocumentAlt(String paramString) throws ParserConfigurationException, SAXException, IOException
  {
    DocumentBuilderFactory localDocumentBuilderFactory = DocumentBuilderFactory.newInstance();
    localDocumentBuilderFactory.setNamespaceAware(true);
    
    DocumentBuilder localDocumentBuilder = localDocumentBuilderFactory.newDocumentBuilder();
    Document localDocument = localDocumentBuilder.parse(new File(paramString));
    
    return localDocument;
  }
  
  public boolean checkSignedDoc(Document paramDocument, String paramString1, String paramString2, Class paramClass)
  {
    boolean bool = false;
    try
    {
      Merlin localMerlin = new Merlin();
      
      WSSecurityEngine localObject1 = new WSSecurityEngine();
      KeyStore localObject2 = KeyStore.getInstance("JKS");
      
      ClassLoader localClassLoader = Loader.getClassLoader(paramClass);
      InputStream localInputStream = Merlin.loadInputStream(localClassLoader, paramString1);
      
      ((KeyStore)localObject2).load(localInputStream, paramString2.toCharArray());
      ((Merlin)localMerlin).setKeyStore((KeyStore)localObject2);
      
      WSSConfig localWSSConfig = WSSConfig.getNewInstance();
      localWSSConfig.setWsiBSPCompliant(false);
      ((WSSecurityEngine)localObject1).setWssConfig(localWSSConfig);
      
      List<WSSecurityEngineResult> localList = ((WSSecurityEngine)localObject1).processSecurityHeader(paramDocument, null, null, localMerlin);
      
      for (WSSecurityEngineResult localWSSecurityEngineResult : localList) {
        if (localWSSecurityEngineResult.get("binary-security-token") != null)
        {
          X509Certificate localX509Certificate = (X509Certificate)localWSSecurityEngineResult.get("x509-certificate");
          if ((localX509Certificate != null) && (localX509Certificate.getSubjectDN() != null)) {
            bool = true;
          }
        }
      }
    }
    catch (Exception localException)
    {
      Object localObject1 = new StringWriter();
      Object localObject2 = new PrintWriter((Writer)localObject1);
      localException.printStackTrace((PrintWriter)localObject2);
      localException.printStackTrace();
      bool = false;
    }
    return bool;
  }
}
