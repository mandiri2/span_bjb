package mii.span.util;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.jar.Attributes;
import java.util.jar.JarEntry;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import mii.span.doc.SP2DDocument;

import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import com.zehon.ftp.FTP;

public class PubUtil {
	
	/**
	 * pub.string:padLeft
	 */
	public static String padLeft(String inString, String padString, int size){
		
		if(inString==null || padString==null || inString.length()>=size)return inString;
		
		StringBuilder appended = new StringBuilder();
		
		for(int i=0; i<(size-inString.length()); i++){
			appended.append(padString);
		}
		appended.append(inString);
		
		return appended.toString();
	}
	
	public static String padRight(String s, int n) {
		return String.format("%1$-" + n + "s", s);
	}
	
	
	/**
	 * pub.string:concat
	 */
	public static String concat(String ... values){
		StringBuilder builder = new StringBuilder();
		if(values!=null){
			for(String value : values){
				builder.append(value);
			}
		}
		return builder.toString();
	}
	
	/**
	 * empty string if null
	 */
	public static String emptyStringIfNull(String value){
		if(value==null)return "";
		return value;
	}
	
	/**
	 * xmlNodeToDocument
	 */
	public static Document xmlNodeToDocument(String xmlData) throws JDOMException, IOException{
		InputStream stream = null;
		try {
			stream = new ByteArrayInputStream(xmlData.getBytes("UTF-8"));
			SAXBuilder jdomBuilder = new SAXBuilder();
			return jdomBuilder.build(stream);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(stream!=null){
				stream.close();
			}
		}
		return null;
	}
	
	/**
	 * @throws IOException 
	 * 
	 */
	public static String bytesToFile(String fileName, byte[] bytes, String append) throws IOException{
		FileOutputStream fos = null;
		String length = null;
		try {
			fos = new FileOutputStream(fileName);
			fos.write(bytes);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(fos!=null){
				fos.close();
			}
		}
		return length;
	}
	
	public static String deleteFile(String fileName){
		String status = null;
		
		try {
			File  file = new File(fileName);
			if(file.delete()){
				status = "true";
			}
			else{
				status = "false";
			}
		} catch (Exception e) {
			status = "false";
			e.printStackTrace();
		}
		return status;
	}
	
	public static void sendFileFTP(byte[] content, String serverhost, String serverport, String username, 
			String password, String ftpDestFolder, String nameOfFileToStore) throws IOException{
		InputStream is = null;
		int status=-1;
		try {
			is = new BufferedInputStream(new ByteArrayInputStream(content));
			status = FTP.sendFile(is, nameOfFileToStore, ftpDestFolder, serverhost, username, password);
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			if(is!=null)is.close();
		}
	}
	
	public static String[] tokenize(String inString, String delim){
		return inString.split(Pattern.quote(delim));
	}
	
	public static List<String> toListString(String[] input){
		List<String> output = new ArrayList<String>();
		
		if (input != null){
			for (int i = 0; i < input.length; i++){
				output.add(input[i]);
			}
		}
		
		return output;
	}
	
	public static String[] toArrayString(List<String> input){
		String[] output = null;
		if (input != null) {
			output = new String[input.size()];
			for (int i = 0; i < input.size(); i++) {
				output[i] = input.get(i);
			}
		}
		return output;
	}
	
	public static String makeString(String[] elementList, String separator){
		StringBuilder sb = new StringBuilder();
		
		if (elementList != null){
			for(int i = 0; i < elementList.length; i++){
				if (i < elementList.length-1)
					sb.append(elementList[i]).append("_");
				else
					sb.append(elementList[i]);
			}
		}
		
		return sb.toString();
	}
	
	public static String createXMLSpanDocument(SP2DDocument sp2dDoc)
			throws JAXBException {
		String xmlString = "";
		JAXBContext context = JAXBContext.newInstance(SP2DDocument.class);
		Marshaller m = context.createMarshaller();

		// To format XML
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE); 

		StringWriter sw = new StringWriter();
		m.marshal(sp2dDoc, sw);
		xmlString = sw.toString();

		return xmlString;
	}
	
	/**
	 * for String date value
	 */
	public static String dateTimeFormat(String date, String currentFormat, String newFormat){
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(currentFormat);
			Date d = null;
			d = sdf.parse(date);
			sdf.applyPattern(newFormat);
			return sdf.format(d);
		} catch (ParseException e) {
			e.printStackTrace();
			return date;
		}
	}
	
	/**
	 * for Date value
	 */
	public static String dateTimeFormat(Date date, String currentFormat, String newFormat){

		SimpleDateFormat sdf = new SimpleDateFormat(newFormat);
		String dateString = null;
		try {
			dateString = sdf.format(date);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dateString; 
	}
	
	public static SP2DDocument createSPANDocument(String xmlFileName) throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance(SP2DDocument.class);
		StringReader reader = new StringReader(xmlFileName);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		SP2DDocument spandoc = (SP2DDocument) jaxbUnmarshaller.unmarshal(reader);
		return spandoc;
	}
	
	/**
	 * xmlValuesToDocument <br />
	 * ubah string value ke object menggunakan jaxb
	 * @throws JAXBException 
	 */
	public static Object xmlValuesToDocument(Class yourClass, String xmlValues) throws JAXBException{
		JAXBContext jaxbContext = JAXBContext.newInstance(yourClass);
		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
		
		StringReader reader = new StringReader(xmlValues);
		return unmarshaller.unmarshal(reader);
	}
	
	/**
	 * ubah object menjadi string xml menggunakan jaxb
	 * @return
	 * @throws JAXBException 
	 */
	public static String documentToXmlValues(Class yourClass, Object yourObject) throws JAXBException{
		JAXBContext jaxbContext = JAXBContext.newInstance(yourClass);
		Marshaller marshaller = jaxbContext.createMarshaller();
		StringWriter sw = new StringWriter();
		marshaller.marshal(yourObject, sw);
		return sw.toString();
	}
	
	public static String addInts(String num1, String num2){
		String result;
		Integer val1;
		Integer val2;
		
		if(num1==null){
			val1 = 0;
		}
		else{
			val1 = Integer.valueOf(num1);
		}
		
		if(num2==null){
			val2 = 0;
		}
		else{
			val2 = Integer.valueOf(num2);
		}
		
		result = String.valueOf((val1+val2));
		
		return result;
	}
	
	public static String addFloats(String num1, String num2, String precision){
		String result;
		Float val1;
		Float val2;
		
		if(num1==null){
			val1 = 0f;
		}
		else{
			val1 = Float.valueOf(num1);
		}
		
		if(num2==null){
			val2 = 0f;
		}
		else{
			val2 = Float.valueOf(num2);
		}
		
		result = String.valueOf((val1+val2));
		
		return result;
	}
	
	/**
	 * ubah string to date sesuai dengan pattern
	 * @param valueDate
	 * @param pattern
	 * @return
	 */
	public static Date stringToDate(String valueDate, String pattern){
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(pattern);
			return sdf.parse(valueDate);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
		
	}
	
	public static int getIntegerFrom2ByteArray(byte[] byteArr) {
		return ((byteArr[0] & 0xFF) << 8 | (byteArr[1] & 0xFF));
	}
	
	public static byte[] intTo2ByteArray(int a)
	{
	    byte[] ret = new byte[2];
	    ret[1] = (byte) (a & 0xFF);   
	    ret[0] = (byte) ((a >> 8) & 0xFF);
	    
	    return ret;
	}
	
	public static byte[] intTo4ByteArray(int a)
	{
	    byte[] ret = new byte[4];
	    ret[3] = (byte) (a & 0xFF);   
	    ret[2] = (byte) ((a >> 8) & 0xFF);
	    ret[1] = (byte) ((a >> 16) & 0xFF);
	    ret[0] = (byte) ((a >> 24) & 0xFF);
	    
	    return ret;
	}
	
	public static byte[] concateByteArray(byte[] a, byte[] b){
		byte[] c = new byte[a.length + b.length];
		System.arraycopy(a, 0, c, 0, a.length);
		System.arraycopy(b, 0, c, a.length, b.length);
		return c;
	}
	
	public static List mapToList(Map map){
		List list = null;
		try {
			list = new ArrayList(map.values());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	public static boolean isListNotEmpty(List list){
		return !isListEmpty(list);
	}
	
	public static boolean isListEmpty(List list){
		if(list!=null 
				&& list.size()>0){
			return false;
		}
		return true;
	}
	
	public static String concatString(String... strings){
		StringBuilder builder = new StringBuilder();
		
		if (strings != null){
			for (String s : strings){
				builder.append(s);
			}
		}
		
		return builder.toString();
	}

	public static void moveFile(String filename, String sourcePath, String backupPath) {
		try{
			File file = new File(concatString(sourcePath, filename));
			if(file.renameTo(new File(concatString(backupPath,file.getName())))){
				System.out.println("File is moved successful!");
			}else{
				System.out.println("File is failed to move!");
			}
		    
		}catch(Exception e){
			System.out.println("Exception occured when moving file");
		    e.printStackTrace();
		}
		
	}
	
	public static void runCreateJar(String fileInput, String fileTarget) throws IOException{
		  Manifest manifest = new Manifest();
		  manifest.getMainAttributes().put(Attributes.Name.MANIFEST_VERSION, "1.0");
		  JarOutputStream target = new JarOutputStream(new FileOutputStream(fileTarget), manifest);
		  add(new File(fileInput), target);
		  target.close();
	}
	
	private static void add(File source, JarOutputStream target) throws IOException
	{
		BufferedInputStream in = null;
		try {
			if (source.isDirectory()) {
				// not optimize for directory
				String name = source.getPath().replace("\\", "/");
				if (!name.isEmpty()) {
					if (!name.endsWith("/"))
						name += "/";
				}
				for (File nestedFile : source.listFiles())
					add(nestedFile, target);
				return;
			}
			source.getPath().replace("\\", "/");
			JarEntry entry = new JarEntry(source.getName());
			entry.setTime(source.lastModified());
			target.putNextEntry(entry);
			in = new BufferedInputStream(new FileInputStream(source));

			byte[] buffer = new byte[1024];
			while (true) {
				int count = in.read(buffer);
				if (count == -1)
					break;
				target.write(buffer, 0, count);
			}
			target.closeEntry();
		} finally {
			if (in != null)
				in.close();
		}
	}

	/**
	 * Return String jika object adalah instance of String dan tidak null
	 */
	public static String stringIfNotNull(Object object){
		if(object!=null
				&& object instanceof String){
			return (String)object;
		}
		return null;
	}

	/**
	 * check if big integer 0 or null
	 * @param value
	 * @return
	 */
	public static boolean isBigIntegerEmpty(BigInteger value){
		if(value==null
				|| value.compareTo(BigInteger.ZERO)==0){
			return true;
		}
		return false;
	}
	
	public static void runJarWithoutManifest(String sorborFile,String fileName){
		byte[] buffer = new byte[1024];

    	try{
    		String outputName=sorborFile.replace(".SBR", ".jar");
    		FileOutputStream fos = new FileOutputStream(outputName);
    		ZipOutputStream zos = new ZipOutputStream(fos);
    		ZipEntry ze= new ZipEntry(fileName);
    		zos.putNextEntry(ze);
    		FileInputStream in = new FileInputStream(sorborFile);

    		int len;
    		while ((len = in.read(buffer)) > 0) {
    			zos.write(buffer, 0, len);
    		}

    		in.close();
    		zos.closeEntry();

    		//remember close it
    		zos.close();

    		System.out.println("Done");

    	}catch(IOException ex){
    	   ex.printStackTrace();
    	}
	}
}
