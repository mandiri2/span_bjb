package mii.span.util;

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
import java.io.File;
import java.io.InputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.apache.ws.custom.security.WSConstants;
import org.apache.ws.custom.security.WSSConfig;
import org.apache.ws.custom.security.WSSecurityEngine;
import org.apache.ws.custom.security.WSSecurityEngineResult;
import org.apache.ws.custom.security.WSSecurityException;
import org.apache.ws.custom.security.components.crypto.CredentialException;
import org.apache.ws.custom.security.components.crypto.Crypto;
import org.apache.ws.custom.security.components.crypto.Merlin;
import org.apache.ws.custom.security.message.WSSecHeader;
import org.apache.ws.custom.security.message.WSSecSignature;
import org.apache.ws.custom.security.util.Loader;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import org.apache.jcp.xml.custom.dsig.internal.*;
import org.apache.jcp.xml.custom.dsig.internal.dom.*;

import mii.span.process.model.SignXmlFileResponse;

public class Signature {
	
	public static SignXmlFileResponse signXMLFile(String filename, String keystoreFile, String keystorePass, String privateKeyUser,
			String privateKeyPass, String signedFile){
		
		SignXmlFileResponse response = new SignXmlFileResponse();
		
		// pipeline
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		String status=null;
		String error = null;

		// get class of wssignature (in WM folder name is class)
		wssignature wss = new wssignature();
		Class iClass = wss.getClass();

		try {
			//load file and wrap xml with SOAPEnvelope
			SOAPMessage msg  = wss.loadDocument(filename);

			//Sign soap using keystore  
		        Document signedDoc = wss.signSOAPMessage(msg, keystoreFile, keystorePass, privateKeyUser, privateKeyPass, iClass);
		        
			//Verify signature
			//wss.checkSignedDoc(signedDoc, keystoreFile, keystorePass, iClass);
		        
			//persist document to file    
		 	wss.persistDocument(signedDoc, signedFile);
		    	   
			//Process finished
			status = "OK";
		    		
		        } catch (FileNotFoundException e) {
		    		e.printStackTrace();
		    		e.printStackTrace(pw);			
		    	} catch (TransformerException e) {
		    		e.printStackTrace();
		    		e.printStackTrace(pw);
		    	} catch (ParserConfigurationException e) {
		    		e.printStackTrace();
		    		e.printStackTrace(pw);
		    	} catch (SAXException e) {
		    		e.printStackTrace();
		    		e.printStackTrace(pw);
		    	} catch (IOException e) {
		    		e.printStackTrace();
		    		e.printStackTrace(pw);
		    	} catch (SOAPException e) {
		    		e.printStackTrace();
		    		e.printStackTrace(pw);
		    	} catch (KeyStoreException e) {
		    		e.printStackTrace();
		    		e.printStackTrace(pw);
		    	} catch (NoSuchAlgorithmException e) {
		    		e.printStackTrace();
		    		e.printStackTrace(pw);
		    	} catch (CertificateException e) {
		    		e.printStackTrace();
		    		e.printStackTrace(pw);
		    	} catch (CredentialException e) {
		    		e.printStackTrace();
		    		e.printStackTrace(pw);
		    	} catch (Exception e) {
		    		e.printStackTrace();
		    		e.printStackTrace(pw);
		    	} finally {
			error = sw.toString();
			if (error.isEmpty() == false) {  
				status = "NOT OK";      	
		        }
		}
		
		response.setStatus(status);
		response.setError(error);
		return response;
	}
}
