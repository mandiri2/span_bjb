package mii.span.util;

import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.Date;

import mii.span.util.ValidatingXMLSignature;

import org.w3c.dom.Document;

public class ValidatingSignature {
	
	/**
	 * return "valid", "invalid", "error"
	 */
	public static String validatingSignature(String filePath, String ksPath, String ksPassword){
		String status;
		
		ValidatingXMLSignature val = new ValidatingXMLSignature();
		
		try {
			Document doc = val.loadDocumentAlt(filePath);
			Class iClass = val.getClass();
			
			boolean valid;
			valid = val.checkSignedDoc(doc, ksPath, ksPassword, iClass);
			
			status = valid ? "valid" : "invalid";
		} catch(FileNotFoundException e1){
			System.out.println("File not found in "+ksPath);
			status="notfound";
		} catch (Exception e) {
			status="error";
			e.printStackTrace();
		}
//		status = "valid"; //FIXME TODO Bypass validate sign.
		return status;
	}
	
	public static String validatingSignatureManual(String filePath, String ksPath, String ksPassword){
		String status;
		
		ValidatingXMLSignature val = new ValidatingXMLSignature();
		
		try {
			Document doc = val.loadDocumentAlt(filePath);
			Class iClass = val.getClass();
			
			boolean valid;
			valid = val.checkSignedDoc(doc, ksPath, ksPassword, iClass);
			
			status = valid ? "valid" : "invalid";
		} catch (Exception e) {
			status="error";
			e.printStackTrace();
		}
		status = "valid"; //FIXME TODO Bypass validate sign.
		return status;
	}
}
