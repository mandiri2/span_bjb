package mii.span.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.jar.Attributes;
import java.util.jar.JarEntry;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;
import java.util.zip.CRC32;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import mii.span.doc.ACKDataDocument;

public class Atomic {
	
	/**
	 * mandiri.span.util.atomic:mappingDataACKSpan
	 * @throws Exception 
	 */
	public static String mappingDataACKSpan(ACKDataDocument ackDataDocument) throws Exception{
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");            
		Date dateTime = null; 
		String newDateTime = null;
		
		String DocumentDate = ackDataDocument.getDocumentDate();
		
		if("XXXXXXXXXXXXXXX".equals(DocumentDate)){
		    	newDateTime = DocumentDate;
		}else{
			if(DocumentDate!=null && DocumentDate.length()>0){
				try{
					dateTime = sdf.parse(DocumentDate);	
					newDateTime = sdf.format(dateTime);
				} catch (ParseException ex) {
		            		throw new Exception(ex.getMessage());
			        }
			}
		}
			
		String DocumentType = ackDataDocument.getDocumentType();
		if(DocumentType==null || DocumentType.length()<=0)DocumentType="";
		
		String DocumentNo   = ackDataDocument.getDocumentNo();
		if(DocumentNo==null || DocumentNo.length()<=0)DocumentNo="";
		
		String FileName	    = ackDataDocument.getFileName();
		if(FileName==null || FileName.length()<=0)FileName="";
		
		String ReturnCode   = ackDataDocument.getReturnCode(); 
		if(ReturnCode==null || ReturnCode.length()<=0)ReturnCode="";
		
		String Description  = ackDataDocument.getDescription(); 
		if(Description==null || Description.trim().length()<=0)Description="SUKSES";
		
		String ACKDataString = newDateTime.concat("|").concat(padRight(DocumentType," ",4).concat("|")).concat(
					padRight(DocumentNo," ",21).concat("|")).concat(
					padRight(FileName," ",100).concat("|")).concat(
					padLeft(ReturnCode,"0",4).concat("|")).concat(
					padRight(Description, " ", 100));
		
		return ACKDataString;
		
	}
	
	/** Share code **/
	
	public static String padLeft(String inString, String padString, int length){
	    if(inString==null || inString.length()<1) {
			inString = "0";
	//		System.out.println("Pad Left 2 -->"+inString+" length : "+inString.length());       
		}
	//    System.out.println("Pad Left -->"+inString+" length : "+inString.length());       
	    
	    while(true){           
	        if(inString.length()<length){                    
	            inString = padString.concat(inString);
	//              System.out.println(inString);        
	        } else break;
	    }
	    return inString;
	}
	
	public static String padRight(String inString, String padString, int length){
		if(inString==null || inString.length()<1) {
			inString = " ";		
	//		System.out.println("Pad Right 2 -->"+inString+" length : "+inString.length()); 
	    }
	//	System.out.println("Pad Right -->"+inString+" length : "+inString.length());
	    while(true){    
	        if(inString.length()<length){                
	            inString = inString.concat(padString);
	//              System.out.println(inString);        
	        } else break;
	    }
	    
	    return inString;
	}
	
	/**
	 * mandiri.span.util.atomic:ACKStringToBytes <br />
	 * return Object[] <br />
	 * Object[0] ACKbytes <br />
	 * Object[1] Status <br />
	 * Object[2] ErrorMessage <br />
	 */
	public static Object[] ACKStringToBytes(String string){
		
		Object[] objects = new Object[3];
		
		
		String sourceString = string;
		byte[] bytes = null;
		String Status = "";
		String ErrorMessage = "";
		
		try{
			bytes = sourceString.getBytes();
			Status = "SUCCESS";
		}catch(Exception e){
			e.printStackTrace();
			Status = "ERROR";
			ErrorMessage = e.getMessage();
		}
		
		objects[0] = bytes;
		objects[1] = Status;
		objects[2] = ErrorMessage;
		
		return objects;
	}
	
	/**
	 * mandiri.span.util.atomic:createAndExtractJAR
	 * return String[] <br />
	 * string[0] Status <br />
	 * string[1] ErrorMessage <br />
	 * @throws IOException 
	 */
	public static String[] createAndExtractJAR(String zipFileName, String localPath, String zipType) throws IOException{
		String[] strings = new String[2];
		
		String Status = "SUCCESS";
		String ErrorMessage = "";
		
		if("C".equals(zipType)){
			String fileName = "";
			FileOutputStream fileOutputStream = null;
			FileInputStream fis = null;
			ZipOutputStream s = null;
			try{
				CRC32 crc = new CRC32();
				zipFileName = zipFileName.replace("txt","jar");
				fileOutputStream = new FileOutputStream(localPath.concat(zipFileName));
				s = new ZipOutputStream((OutputStream)fileOutputStream);
			    	fileName = zipFileName.replace("jar","xml");
			    	fileName = zipFileName.replace("jar","txt");
				s.setLevel(6);
				fis = null;
				ZipEntry entry = null;
			    
		//		int newFileName = localPath.indexOf("SPAN")+5+9;//"TempFile"
				int newFileName = localPath.length();
		
				String[] filenames = new String[2];
				filenames[0] = localPath.concat("META-INF/MANIFEST.MF");
				filenames[1] = localPath.concat(fileName);
				byte[] buf;
		
				File f = null; 
				long fl = 0;
		
				for(int x=0; x<filenames.length; x++){
		
					f = new File(filenames[x]);
					fl = f.length();
					buf = new byte[Integer.parseInt(String.valueOf(f.length()))];
		
					fis = new FileInputStream(filenames[x]);
			    		fis.read(buf, 0, buf.length);
		
				    	entry = new ZipEntry(filenames[x].substring(newFileName));
		
				    	entry.setSize((long) buf.length);
			    		crc.reset();
				    	crc.update(buf);
				    	entry.setCrc(crc.getValue());
			    		s.putNextEntry(entry);
				    	s.write(buf, 0, buf.length);	    	
				    }
		
				s.finish();
		    		s.close();
				
			}catch(Exception e){
				Status = "ERROR";
				ErrorMessage = e.getMessage();
				e.printStackTrace();
			}finally{
				if(fileOutputStream!=null)fileOutputStream.close();
				if(fis!=null)fis.close();
				if(s!=null)s.close();
			}
			
		}else if("E".equals(zipType)){
			try{
				//localPath = "C:\\Data\\Temp\\DataTest\\SPAN\\";
				//zipFileName = "810000980990_SP2D_O_20121011_121048_002.jar";
				    
				byte[] buf = new byte[1024];
				    
				ZipInputStream zipinputstream = new ZipInputStream(new FileInputStream(localPath.concat(zipFileName)));
				ZipEntry zipentry;
			//System.out.println("zip input stream = "+zipinputstream);
						    
				String entryName = "";
				String directory = "";
		
				boolean dirStatus = true;
				    
				File newFile = null;
				FileOutputStream fileoutputstream;
				//System.out.println("masuk sebelum while");
				while (true) {
				
					zipentry = zipinputstream.getNextEntry();
				
				    	if(zipentry==null)break;
				    	
				    	entryName = zipentry.getName();
				
				    	newFile = new File(localPath.concat(entryName));
				    	directory = newFile.getParent();
				      
				    	if (entryName.startsWith("META-INF")) {
				    		if(dirStatus){/*DO NOTHING*/}
				    	  	else dirStatus = false;
				    	}else{
				    		fileoutputstream = new FileOutputStream(localPath.concat(entryName));
				    		int n;
				    		while ((n = zipinputstream.read(buf, 0, 1024)) > -1){
				    			fileoutputstream.write(buf, 0, n);
				    		}
				    		fileoutputstream.close();
				    		zipinputstream.closeEntry();
				    		zipentry = zipinputstream.getNextEntry();
				    	}
				}
				zipinputstream.close();
				
			}catch(Exception e){
				Status = "ERROR";
				ErrorMessage = e.getMessage();
				e.printStackTrace();
			}
		
		}
		
		strings[0] = Status;
		strings[1] = ErrorMessage;
		return strings;
	}
	
	/**
	 * mandiri.span.util.atomic:createJAR
	 */
	public static void createJAR(String fileInput, String fileOutput){
		try{
			runCreateJar(fileInput, fileOutput);
		}
		catch(Exception exc){
			exc.printStackTrace();
		}
	}
	
	/**
	 * SHARE CODE
	 */
	public static void runCreateJar(String fileInput, String fileTarget) throws IOException{
	  Manifest manifest = new Manifest();
	  manifest.getMainAttributes().put(Attributes.Name.MANIFEST_VERSION, "1.0");
	  JarOutputStream target = new JarOutputStream(new FileOutputStream(fileTarget), manifest);
	  add(new File(fileInput), target);
	  target.close();
	}
	
	private static void add(File source, JarOutputStream target) throws IOException
	{
	  BufferedInputStream in = null;
	  try
	  {
	    if (source.isDirectory())
	    {
	      //not optimize for directory
	      String name = source.getPath().replace("\\", "/");
	      if (!name.isEmpty())
	      {
	        if (!name.endsWith("/"))
	          name += "/";
	        /*
	        JarEntry entry = new JarEntry(name);
	        entry.setTime(source.lastModified());
	        target.putNextEntry(entry);
	        target.closeEntry();
	        */
	      }
	      for (File nestedFile: source.listFiles())
	        add(nestedFile, target);
	      return;
	    }
	
	    //JarEntry entry = new JarEntry(source.getPath().replace("\\", "/"));
	    source.getPath().replace("\\", "/");
	    JarEntry entry = new JarEntry(source.getName());
	    entry.setTime(source.lastModified());
	    target.putNextEntry(entry);
	    in = new BufferedInputStream(new FileInputStream(source));
	
	    byte[] buffer = new byte[1024];
	    while (true)
	    {
	      int count = in.read(buffer);
	      if (count == -1)
	        break;
	      target.write(buffer, 0, count);
	    }
	    target.closeEntry();
	  }
	  finally
	  {
	    if (in != null)
	      in.close();
	  }
	}
	
	/**
	 * mandiri.span.util.atomic:getSleep
	 */
	public static String getSleep(String ms){
		String Status = "SUCCESS";
		
		try{
			Thread t = new Thread();
			t.sleep(Integer.parseInt(ms));

		}catch(InterruptedException e){
			e.printStackTrace();
			Status = "ERROR";
		}
		
		return Status;
	}
}
