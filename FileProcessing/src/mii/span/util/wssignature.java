package mii.span.util;

import com.wm.app.b2b.server.ServiceException;
import com.wm.data.IData;
import com.wm.data.IDataCursor;
import com.wm.data.IDataUtil;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.apache.ws.custom.security.WSSConfig;
import org.apache.ws.custom.security.WSSecurityEngine;
import org.apache.ws.custom.security.WSSecurityEngineResult;
import org.apache.ws.custom.security.WSSecurityException;
import org.apache.ws.custom.security.components.crypto.CredentialException;
import org.apache.ws.custom.security.components.crypto.Merlin;
import org.apache.ws.custom.security.message.WSSecHeader;
import org.apache.ws.custom.security.message.WSSecSignature;
import org.apache.ws.custom.security.util.Loader;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public final class wssignature
{
  static final wssignature _instance = new wssignature();
  
  static wssignature _newInstance()
  {
    return new wssignature();
  }
  
  static wssignature _cast(Object paramObject)
  {
    return (wssignature)paramObject;
  }
  
  public static final void signXMLFile(String filename, String keystoreFile, String keystorePass, String privateKeyUser, String privateKeyPass, String signedFile) throws ServiceException{
    String status = null;
    String error = null;
	  
	StringWriter localStringWriter = new StringWriter();
    PrintWriter localPrintWriter = new PrintWriter(localStringWriter);
    String str1 = filename;
    String str2 = keystoreFile;
    String str3 = keystorePass;
    String str4 = privateKeyUser;
    String str5 = privateKeyPass;
    String str6 = signedFile;
    String str7 = null;
    String str8 = null;
    
    wssignature localwssignature = new wssignature();
    Class localClass = localwssignature.getClass();
    try
    {
      SOAPMessage localSOAPMessage = loadDocument(str1);
      
      Document localDocument = localwssignature.signSOAPMessage(localSOAPMessage, str2, str3, str4, str5, localClass);
      
      localwssignature.persistDocument(localDocument, str6);
      
      str7 = "OK";
    }
    catch (FileNotFoundException localFileNotFoundException)
    {
      localFileNotFoundException.printStackTrace(localPrintWriter);
    }
    catch (TransformerException localTransformerException)
    {
      localTransformerException.printStackTrace(localPrintWriter);
    }
    catch (ParserConfigurationException localParserConfigurationException)
    {
      localParserConfigurationException.printStackTrace(localPrintWriter);
    }
    catch (SAXException localSAXException)
    {
      localSAXException.printStackTrace(localPrintWriter);
    }
    catch (IOException localIOException)
    {
      localIOException.printStackTrace(localPrintWriter);
    }
    catch (SOAPException localSOAPException)
    {
      localSOAPException.printStackTrace(localPrintWriter);
    }
    catch (KeyStoreException localKeyStoreException)
    {
      localKeyStoreException.printStackTrace(localPrintWriter);
    }
    catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
    {
      localNoSuchAlgorithmException.printStackTrace(localPrintWriter);
    }
    catch (CertificateException localCertificateException)
    {
      localCertificateException.printStackTrace(localPrintWriter);
    }
    catch (CredentialException localCredentialException)
    {
      localCredentialException.printStackTrace(localPrintWriter);
    }
    catch (Exception localException)
    {
      localException.printStackTrace(localPrintWriter);
    }
    finally
    {
      str8 = localStringWriter.toString();
      if (!str8.isEmpty())
      {
        str7 = "NOT OK";
        status = str7;
        error = str8;
        
        System.out.println("str 7 "+str7+" str 8 "+str8);
      }
    }
  }
  
  public static SOAPMessage loadDocument(String paramString)
    throws ParserConfigurationException, SAXException, IOException, SOAPException
  {
    DocumentBuilderFactory localDocumentBuilderFactory = DocumentBuilderFactory.newInstance();
    localDocumentBuilderFactory.setNamespaceAware(true);
    
    DocumentBuilder localDocumentBuilder = localDocumentBuilderFactory.newDocumentBuilder();
    Document localDocument = localDocumentBuilder.parse(new File(paramString));
    
    SOAPMessage localSOAPMessage = MessageFactory.newInstance().createMessage();
    localSOAPMessage.getSOAPBody().addDocument(localDocument);
    
    return localSOAPMessage;
  }
  
  public void persistDocument(Document paramDocument, String paramString)
    throws FileNotFoundException, TransformerException
  {
    TransformerFactory localTransformerFactory = TransformerFactory.newInstance();
    
    Transformer localTransformer = localTransformerFactory.newTransformer();
    
    FileOutputStream localFileOutputStream = null;
    try
    {
      localFileOutputStream = new FileOutputStream(paramString);
      DOMSource localDOMSource = new DOMSource(paramDocument);
      StreamResult localStreamResult = new StreamResult(localFileOutputStream);
      localTransformer.transform(localDOMSource, localStreamResult); return;
    }
    finally
    {
      try
      {
        localFileOutputStream.close();
      }
      catch (Exception localException2)
      {
        localException2.printStackTrace();
      }
    }
  }
  
  public Document signSOAPMessage(SOAPMessage paramSOAPMessage, String paramString1, String paramString2, String paramString3, String paramString4, Class paramClass)
    throws Exception
  {
    WSSecSignature localWSSecSignature = new WSSecSignature();
    DOMResult localDOMResult = new DOMResult();
    WSSecHeader localWSSecHeader = new WSSecHeader();
    Merlin localMerlin = new Merlin();
    
    Source localSource = paramSOAPMessage.getSOAPPart().getContent();
    TransformerFactory localTransformerFactory = TransformerFactory.newInstance();
    Transformer localTransformer = localTransformerFactory.newTransformer();
    
    localTransformer.transform(localSource, localDOMResult);
    Document localDocument1 = (Document)localDOMResult.getNode();
    localWSSecHeader.insertSecurityHeader(localDocument1);
    
    KeyStore localKeyStore = KeyStore.getInstance("JKS");
    ClassLoader localClassLoader = Loader.getClassLoader(paramClass);
    
    InputStream localInputStream = Merlin.loadInputStream(localClassLoader, paramString1);
    localKeyStore.load(localInputStream, paramString2.toCharArray());
    ((Merlin)localMerlin).setKeyStore(localKeyStore);
    
    localWSSecSignature.setUserInfo(paramString3, paramString4);
    localWSSecSignature.setKeyIdentifierType(1);
    Document localDocument2 = localWSSecSignature.build(localDocument1, localMerlin, localWSSecHeader);
    
    return localDocument2;
  }
  
  public void checkSignedDoc(Document paramDocument, String paramString1, String paramString2, Class paramClass)
    throws WSSecurityException, FileNotFoundException, ParserConfigurationException, SAXException, IOException, SOAPException, KeyStoreException, CredentialException, NoSuchAlgorithmException, CertificateException
  {
    Merlin localMerlin = new Merlin();
    WSSecurityEngine localWSSecurityEngine = new WSSecurityEngine();
    
    KeyStore localKeyStore = KeyStore.getInstance("JKS");
    ClassLoader localClassLoader = Loader.getClassLoader(paramClass);
    InputStream localInputStream = Merlin.loadInputStream(localClassLoader, paramString1);
    localKeyStore.load(localInputStream, paramString2.toCharArray());
    ((Merlin)localMerlin).setKeyStore(localKeyStore);
    
    WSSConfig localWSSConfig = WSSConfig.getNewInstance();
    localWSSConfig.setWsiBSPCompliant(false);
    localWSSecurityEngine.setWssConfig(localWSSConfig);
    
    List<WSSecurityEngineResult> localList = localWSSecurityEngine.processSecurityHeader(paramDocument, null, null, localMerlin);
    for (WSSecurityEngineResult localWSSecurityEngineResult : localList) {
      if (localWSSecurityEngineResult.get("binary-security-token") != null)
      {
        System.out.println(localWSSecurityEngineResult.get("binary-security-token"));
        
        X509Certificate localX509Certificate = (X509Certificate)localWSSecurityEngineResult.get("x509-certificate");
        
        System.out.println(localX509Certificate.getSubjectDN());
      }
    }
  }
}
