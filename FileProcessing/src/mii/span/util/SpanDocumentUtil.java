package mii.span.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import mii.span.constant.SpanDocumentConstant;
import mii.span.doc.ApplicationArea;
import mii.span.doc.DataArea;
import mii.span.doc.Footer;
import mii.span.doc.SP2DDocument;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;

public class SpanDocumentUtil {
	
	/**
	 * GET SP2D Or Spt
	 */
	public static Element getSp2dOrSpt(Document document){
		Element envelope = document.getRootElement();
		Element body = envelope.getChild("Body",envelope.getNamespace());
		//check if SP2D or SPT
		Element sp2dOrSpt = (sp2dOrSpt=body.getChild("SP2D"))!=null ? sp2dOrSpt : body.getChild("SPT");
		return sp2dOrSpt;
	}
	
	/**
	 * mengambil value dari ApplicationAreaMessageTypeIndicator yang ada di xml
	 * input SP2D / SPT
	 */
	public static String getApplicationAreaMessageTypeIndicatorValue(Element sp2dOrSpt){
		
		Element ApplicationArea = sp2dOrSpt.getChild("ApplicationArea");
		Element ApplicationAreaMessageTypeIndicator = ApplicationArea.getChild("ApplicationAreaMessageTypeIndicator");
		
		return ApplicationAreaMessageTypeIndicator.getValue();
	}
	
	/**
	 * Convert JDOM Document to SpanDocument menggunakan element getSp2dOrSpt() sebagai parameter
	 */
	public static SP2DDocument jDomToSpanDocument(Element SP2D){
		SP2DDocument  spanDocument = new SP2DDocument();
		
		ApplicationArea applicationArea = initApplicationArea(SP2D.getChild(SpanDocumentConstant.APPLICATION_AREA));
		List<DataArea> dataAreas = initDataAreas(SP2D.getChildren(SpanDocumentConstant.DATA_AREA)); //getList
		Footer footer = initFooter(SP2D.getChild(SpanDocumentConstant.FOOTER));
		
		spanDocument.setApplicationArea(applicationArea);
		spanDocument.setDataArea(dataAreas);
		spanDocument.setFooter(footer);
		
		return spanDocument;
	}
	
	/**
	 * Convert JDOM Document to spandocument menggunakan jdomdocumentasli
	 */
	public static SP2DDocument jDomToSpanDocument(Document document){
		SP2DDocument  spanDocument = new SP2DDocument();
		
		Element SP2D = getSp2dOrSpt(document);
		ApplicationArea applicationArea = initApplicationArea(SP2D.getChild(SpanDocumentConstant.APPLICATION_AREA));
		List<DataArea> dataAreas = initDataAreas(SP2D.getChildren(SpanDocumentConstant.DATA_AREA)); //getList
		Footer footer = initFooter(SP2D.getChild(SpanDocumentConstant.FOOTER));
		
		spanDocument.setApplicationArea(applicationArea);
		spanDocument.setDataArea(dataAreas);
		spanDocument.setFooter(footer);
		
		return spanDocument;
	}
	
	
	/**
	 * mangambil data area
	 * @throws IOException 
	 * @throws JDOMException 
	 */
	public static List<DataArea> getDataArea(String xmlValues) throws JDOMException, IOException{
		Document document = PubUtil.xmlNodeToDocument(xmlValues);
		Element sp2dOrSpt = SpanDocumentUtil.getSp2dOrSpt(document);
		SP2DDocument spanDocument = SpanDocumentUtil.jDomToSpanDocument(sp2dOrSpt);
		return spanDocument.getDataArea();
	}
	
	/**private method**/
	private static Footer initFooter(Element footerDom) {
		Footer footer = new Footer();
		
		footer.setTotalCount(footerDom.getChildText(SpanDocumentConstant.TOTAL_COUNT));
		footer.setTotalAmount(footerDom.getChildText(SpanDocumentConstant.TOTAL_AMOUNT));
		footer.setTotalBatchCount(footerDom.getChildText(SpanDocumentConstant.TOTAL_BATCH_COUNT));
		
		return footer;
	}

	private static List<DataArea> initDataAreas(List<Element> dataAreaDoms) {
		List<DataArea> dataAreas = new ArrayList<DataArea>();
		
		for(Element dataAreaDom : dataAreaDoms){
			DataArea dataArea = new DataArea();
			
			dataArea.setDocumentDate(dataAreaDom.getChildText(SpanDocumentConstant.DOCUMENT_DATE));
			dataArea.setDocumentNumber(dataAreaDom.getChildText(SpanDocumentConstant.DOCUMENT_NUMBER));
			dataArea.setBeneficiaryName(dataAreaDom.getChildText(SpanDocumentConstant.BENEFICIARY_NAME));
			dataArea.setBeneficiaryBankCode(dataAreaDom.getChildText(SpanDocumentConstant.BENEFICIARY_BANK_CODE));
			dataArea.setBeneficiaryBank(dataAreaDom.getChildText(SpanDocumentConstant.BENEFICIARY_BANK));
			dataArea.setBeneficiaryAccount(dataAreaDom.getChildText(SpanDocumentConstant.BENEFICIARY_ACCOUNT));
			dataArea.setAmount(dataAreaDom.getChildText(SpanDocumentConstant.AMOUNT));
			dataArea.setCurrencyTarget(dataAreaDom.getChildText(SpanDocumentConstant.CURRENCY_TARGET));
			dataArea.setDescription(dataAreaDom.getChildText(SpanDocumentConstant.DESCRIPTION));
			dataArea.setAgentBankCode(dataAreaDom.getChildText(SpanDocumentConstant.AGENT_BANK_CODE));
			dataArea.setAgentBankAccountNumber(dataAreaDom.getChildText(SpanDocumentConstant.AGENT_BANK_ACCOUNT_NUMBER));
			dataArea.setAgentBankAccountName(dataAreaDom.getChildText(SpanDocumentConstant.AGENT_BANK_ACCOUNT_NAME));
			dataArea.setEmailAddress(dataAreaDom.getChildText(SpanDocumentConstant.EMAIL_ADDRESS));
			dataArea.setSwiftCode(dataAreaDom.getChildText(SpanDocumentConstant.SWIFT_CODE));
			dataArea.setIBANCode(dataAreaDom.getChildText(SpanDocumentConstant.IBAN_CODE));
			dataArea.setPaymentMethod(dataAreaDom.getChildText(SpanDocumentConstant.PAYMENT_METHOD));
			dataArea.setSP2DCount(dataAreaDom.getChildText(SpanDocumentConstant.SP2D_COUNT));
			
			dataAreas.add(dataArea);
		}
		return dataAreas;
	}

	private static ApplicationArea initApplicationArea(Element applicationAreaDom) {
		ApplicationArea applicationArea = new ApplicationArea();
		
		applicationArea.setApplicationAreaSenderIdentifier(applicationAreaDom.getChildText(SpanDocumentConstant.APPLICATION_AREA_SENDER_IDENTIFIER));
		applicationArea.setApplicationAreaReceiverIdentifier(applicationAreaDom.getChildText(SpanDocumentConstant.APPLICATION_AREA_RECEIVER_IDENTIFIER));
		applicationArea.setApplicationAreaDetailSenderIdentifier(applicationAreaDom.getChildText(SpanDocumentConstant.APPLICATION_AREA_DETAIL_SENDER_IDENTIFIER));
		applicationArea.setApplicationAreaDetailReceiverIdentifier(applicationAreaDom.getChildText(SpanDocumentConstant.APPLICATION_AREA_DETAIL_RECEIVER_IDENTIFIER));
		applicationArea.setApplicationAreaCreationDateTime(applicationAreaDom.getChildText(SpanDocumentConstant.APPLICATION_AREA_CREATION_DATE_TIME));
		applicationArea.setApplicationAreaMessageIdentifier(applicationAreaDom.getChildText(SpanDocumentConstant.APPLICATION_AREA_MESSAGE_IDENTIFIER));
		applicationArea.setApplicationAreaMessageTypeIndicator(applicationAreaDom.getChildText(SpanDocumentConstant.APPLICATION_AREA_MESSAGE_TYPE_INDICATOR));
		applicationArea.setApplicationAreaMessageVersionText(applicationAreaDom.getChildText(SpanDocumentConstant.APPLICATION_AREA_MESSAGE_VERSION_TEXT));
		
		return applicationArea;
	}
}
