package mii.span.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.zip.CRC32;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import mii.span.constant.SParameterConstant;
import mii.span.db.Adapter;
import mii.span.db.adapter.Odbc;
import mii.span.model.SpanHostDataDetails;
import mii.span.model.SpanHostDataFailed;
import mii.span.model.SpanHostResponse;
import mii.span.process.model.CheckingAmountTrxRequest;
import mii.span.process.model.CreateAndExtractJarResponse;
import mii.span.process.model.GetExecutedTransactionSummaryResponse;
import mii.span.process.model.GetSPANDataFailedLastStatusResponse;
import mii.span.process.model.GetTotalRecAmtSendToHostResponse;
import mii.span.process.model.GetWaitTransactionSummaryResponse;
import mii.span.process.model.RetryDetails;
import mii.span.process.model.SelectSParameterResponse;
import mii.span.process.model.SpanStatus;
import mii.span.process.model.SpanWaitSummary;
import mii.span.process.model.SuccessDetail;
import mii.utils.CommonDate;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;

import com.mii.constant.DaoConstant;
import com.mii.helpers.InterfaceDAO;
import com.mii.logger.Log;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.ApplicationInstance;

public class BusinessUtil {
	
	public static String getDateByPattern(String pattren){
		if(pattren!=null){
			SimpleDateFormat sdf = new SimpleDateFormat(pattren);
			try {
				String date = sdf.format(new Date());
				return date;
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
			
		}
		return null;
		
	}
	
	
	public static List<String> getFolderName(String spanPath){
		List<String> spanFolderNames = Arrays.asList(PubUtil.tokenize(spanPath, "|"));
		int iteration = 1;
		String mainPath = null;
		List<String> newSpanFolderNames = new ArrayList<String>();
		
		for(String spanFolderName : spanFolderNames){
			if(iteration==1){
				mainPath =  spanFolderName;
			}
			else{
				newSpanFolderNames.add(spanFolderName);
			}
			iteration++;
		}
		
		spanFolderNames = new ArrayList<String>();
		
		for(String newSpanFolderName : newSpanFolderNames){
			newSpanFolderName = PubUtil.concat(mainPath,"/",newSpanFolderName);
			spanFolderNames.add(newSpanFolderName);
		}
		
		return spanFolderNames;
	}
	
	public static CreateAndExtractJarResponse createAndExtractJar(String zipFileName, String localPath, String zipType) throws IOException{
		CreateAndExtractJarResponse response = new CreateAndExtractJarResponse();
		
		String Status = "SUCCESS";
		String ErrorMessage = "";
		
		if("C".equals(zipType)){
			String fileName = "";
			try{
				CRC32 crc = new CRC32();
				zipFileName = zipFileName.replace("txt","jar");		
				ZipOutputStream s = new ZipOutputStream((OutputStream) new FileOutputStream(localPath.concat(zipFileName)));
			    	fileName = zipFileName.replace("jar","xml");
			    	fileName = zipFileName.replace("jar","txt");
				s.setLevel(6);
				FileInputStream fis = null;
				ZipEntry entry = null;
			    
		//		int newFileName = localPath.indexOf("SPAN")+5+9;//"TempFile"
				int newFileName = localPath.length();
		
				String[] filenames = new String[2];
				filenames[0] = localPath.concat("META-INF/MANIFEST.MF");
				filenames[1] = localPath.concat(fileName);
				byte[] buf;
		
				File f = null; 
				long fl = 0;
		
				for(int x=0; x<filenames.length; x++){
		
					f = new File(filenames[x]);
					fl = f.length();
					buf = new byte[Integer.parseInt(String.valueOf(f.length()))];
		
					fis = new FileInputStream(filenames[x]);
			    		fis.read(buf, 0, buf.length);
		
				    	entry = new ZipEntry(filenames[x].substring(newFileName));
		
				    	entry.setSize((long) buf.length);
			    		crc.reset();
				    	crc.update(buf);
				    	entry.setCrc(crc.getValue());
			    		s.putNextEntry(entry);
				    	s.write(buf, 0, buf.length);	    	
				    }
		
				s.finish();
		    		s.close();
				
			}catch(Exception e){
				Status = "ERROR";
				ErrorMessage = e.getMessage();
				e.printStackTrace();
		}
		}else if("E".equals(zipType)){
			ZipInputStream zipinputstream = null;
			FileInputStream inputStream = null;
			FileOutputStream fileoutputstream = null;
			try{
				//localPath = "C:\\Data\\Temp\\DataTest\\SPAN\\";
				//zipFileName = "810000980990_SP2D_O_20121011_121048_002.jar";
				    
				byte[] buf = new byte[1024];
				inputStream = new FileInputStream(localPath.concat(zipFileName));
				System.out.println("******* try extract data "+localPath.concat(zipFileName));
				zipinputstream = new ZipInputStream(inputStream);
				ZipEntry zipentry;
			//System.out.println("zip input stream = "+zipinputstream);
						    
				String entryName = "";
				String directory = "";
		
				boolean dirStatus = true;
				    
				File newFile = null;
				//System.out.println("masuk sebelum while");
				while (true) {
				
					zipentry = zipinputstream.getNextEntry();
				
				    	if(zipentry==null)break;
				    	
				    	entryName = zipentry.getName();
				
				    	newFile = new File(localPath.concat(entryName));
				    	System.out.println(localPath.concat("**************** try extract data 2") + localPath.concat(entryName));
				    	System.out.println(newFile.getParent());
				    	
				    	directory = newFile.getParent();
				      
				    	if (entryName.startsWith("META-INF")) {
				    		if(dirStatus){/*DO NOTHING*/}
				    	  	else dirStatus = false;
				    	}else{
				    		fileoutputstream = new FileOutputStream(localPath.concat(entryName));
				    		int n;
				    		while ((n = zipinputstream.read(buf, 0, 1024)) > -1){
				    			fileoutputstream.write(buf, 0, n);
				    		}
				    		fileoutputstream.close();
				    		zipinputstream.closeEntry();
				    		zipentry = zipinputstream.getNextEntry();
				    		System.out.println(localPath.concat("**************** create file berhasil"));
				    	}
				}
				zipinputstream.close();
				
			}catch(Exception e){
				Status = "ERROR";
				ErrorMessage = e.getMessage();
				e.printStackTrace();
			}finally{
				if(inputStream!=null)inputStream.close();
				if(zipinputstream!=null)zipinputstream.close();
				if(fileoutputstream!=null)fileoutputstream.close();
			}
		
		}
		
		response.setStatus(Status);
		response.setErrorMessage(ErrorMessage);
		
		return response;
	}
	
	/**
	 * Return String jika object adalah instance of String dan tidak null
	 */
	public static String stringIfNotNull(Object object){
		if(object!=null
				&& object instanceof String){
			return (String)object;
		}
		return null;
	}
	
	/**
	 * DELETE FILE <br />
	 * return OK IF SUCCESS <br />
	 * return null if false
	 */
	public static String deleteFile(String fileName){
		try {
			File file = new File(fileName);
			if(file.delete()){
				return "OK";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * mandiri.span.util:batchID
	 */
	public static String batchID(){
		String dateTime = BusinessUtil.getDateByPattern("yyyyMMdd");
		String batchIDSeq = Odbc.getBatchIDSeq();
		String batchID = PubUtil.padLeft(batchIDSeq, "0", 10);
		batchID = PubUtil.concat("SP-",dateTime,"-",batchID);
		return batchID;
	}
	
	/**
	 * mandiri.span.util:trxHeaderID
	 */
	public static String trxHeaderID(){
		String dateTime = BusinessUtil.getDateByPattern("yyyyMMddHHmmss");
		String sequenceNo = mii.span.db.service.Odbc.spanSequenceNo(null);
		sequenceNo = sequenceNo.substring(6);
		String trxHeaderID = PubUtil.concat(dateTime, sequenceNo);
		return trxHeaderID;
	}
	
	/**
	 * mandiri.span.util:getValidFormat <br />
	 * String[0] amountLength <br />
	 * String[1] amountFormat <br />
	 * String[2] creditAccountLengthOverbooking <br />
	 * String[3] creditAccountLengthSKN <br />
	 * String[4] creditAccountLengthRTGS <br />
	 */
	public static String[] getValidFormat(){
		String[] strings = new String[5];
		
		String amountLength = null;
		String amountFormat = null;
		String creditAccountLengthOverbooking = null;
		String creditAccountLengthSKN = null;
		String creditAccountLengthRTGS = null;
		
		//o mandiri.span.db.service.odbc:selectSParameter
		SelectSParameterResponse selectSParameterResponse = mii.span.db.service.Odbc.selectSParameter(SParameterConstant._20160804P01);
		amountLength = selectSParameterResponse.getValue();
		//c mandiri.span.db.service.odbc:selectSParameter
		
		//branch /amountLength
			if(amountLength == null){
				amountLength = "17"; //default value
			}
		
		//Comments	-- get creditaccount  for IBU
			selectSParameterResponse = mii.span.db.service.Odbc.selectSParameter(SParameterConstant._20160804P02);
			creditAccountLengthOverbooking = BusinessUtil.stringIfNotNull(selectSParameterResponse.getValue());
		 
		//branch creditAccountLengthOverbooking
			if(creditAccountLengthOverbooking==null){
				creditAccountLengthOverbooking = "19"; //default value
			}
			
		//Comments	-- get creditaccount  for LBU
			selectSParameterResponse = mii.span.db.service.Odbc.selectSParameter(SParameterConstant._20160804P03);
			creditAccountLengthSKN = BusinessUtil.stringIfNotNull(selectSParameterResponse.getValue());
			
		//branch creditAccountLengthSKN
			if(creditAccountLengthSKN==null){
				creditAccountLengthSKN = "34";
			}
			
		//Comments	-- get creditaccount  for RBU
			selectSParameterResponse = mii.span.db.service.Odbc.selectSParameter(SParameterConstant._20160804P04);
			creditAccountLengthRTGS = BusinessUtil.stringIfNotNull(selectSParameterResponse.getValue());
			
		//branch creditAccountLengthRTGS
			if(creditAccountLengthRTGS == null){
				creditAccountLengthRTGS = "34";
			}
			
		//Comments	-- get amount format
			selectSParameterResponse = mii.span.db.service.Odbc.selectSParameter(SParameterConstant._20160804P05);
			amountFormat = BusinessUtil.stringIfNotNull(selectSParameterResponse.getValue());
			
		//branch amountFormat
			if(amountFormat==null){
				amountFormat = "[0-9]{0,15}([,.][0-9]{1,2})?";
			}
			
		strings[0] = amountLength;
		strings[1] = amountFormat;
		strings[2] = creditAccountLengthOverbooking;
		strings[3] = creditAccountLengthSKN;
		strings[4] = creditAccountLengthRTGS;
		
		return strings;
	}
	
	/**
	 * mandiri.span.util:renameFileName
	 */
	public static String renameFileName(String filename, String Customer){
		
		filename = filename.replace("FTP_", "");
		
		int dotValue = filename.indexOf(".");
		int underValue = filename.indexOf("_");
		String date = getDateByPattern("yyyyMMdd");
		String time = getDateByPattern("HHmmss");
		
		String newFileName = filename.substring(0, dotValue);
		String extFileName = filename.substring(dotValue);
		Customer = PubUtil.concat("_",Customer,"_",date);
		String prefixFileName = filename.substring(0, underValue);
		
		newFileName = PubUtil.concat(prefixFileName, Customer);
		extFileName = PubUtil.concat("_",time,extFileName);
		
		newFileName = PubUtil.concat(newFileName, extFileName);
		
		return newFileName;
	}
	
	/**
	 * return byte[] if not null
	 */
	public static byte[] bytesIfNotNull(Object value){
		if(value instanceof byte[]){
			return (byte[])value;
		}
		return null;
	}
	
	/**
	 * 
	 */
	public static Object getDao(String dao){
		ApplicationBean applicationBean = ApplicationInstance.getInstance().getApplicationBean();
		return applicationBean.getFileProcessContext().getBean(dao);
	}
	
	/**
	 * coning object menggunakan jaxb
	 * @throws JAXBException 
	 */
	public static Object cloningObject(Class yourClass, Object yourObject) throws JAXBException{
		JAXBContext jaxbContext = JAXBContext.newInstance(yourClass);
		Marshaller marshaller = jaxbContext.createMarshaller();
		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
		
		StringWriter sw = new StringWriter();
		marshaller.marshal(yourObject, sw);
		
		StringReader reader = new StringReader(sw.toString());
		return unmarshaller.unmarshal(reader);
	}
	
	public static final String addDate(String inputFormat, String inputDate, String addDays, String addMonths, String addYears, 
			String addHours, String addMinutes, String addSeconds, String outputFormat){
		String rs = addDate2(inputDate, inputFormat, outputFormat, addYears, addMonths, addDays, addHours, addMinutes, addSeconds);
		return rs;
	}
	
	private static String addDate2(String date, String fmtIn, String fmtOut, String addYears, String addMOnths, String addDays, String addHour, String addMins, String addSecs) {
	      try {
	         SimpleDateFormat sdf = new SimpleDateFormat(fmtIn);
	
	         java.util.Date dt = sdf.parse(date);
	
	         Calendar cld = Calendar.getInstance();
	
	         cld.setTime(dt);
	
	         cld.add(Calendar.YEAR, stoi(addYears));
	         cld.add(Calendar.MONTH, stoi(addMOnths));
	         cld.add(Calendar.DATE, stoi(addDays));
	         cld.add(Calendar.HOUR, stoi(addHour));
	         cld.add(Calendar.MINUTE, stoi(addMins));
	         cld.add(Calendar.SECOND, stoi(addSecs));
	
	         dt = cld.getTime();
	
	         SimpleDateFormat sdf2 = new SimpleDateFormat(fmtOut);
	
	         return sdf2.format(dt);
	      } catch (Exception e) {
	         throw new RuntimeException(e);
	      }
	}
	
	private static int stoi(String is) {
		if (is==null) return 0;
	
	    if (is.length()<1) return 0;
	
	    return Integer.parseInt(is);
	}
	
	/**
	 * mandiri.span.util:checkingMVATransaction
	 */
//	public static void checkingMVATransaction(String BATCHID){
//		String realAccount = null;
//		String status = null;
//		String mvaCompanyCode = null;
//		String trxDetailIDForMVA = null;
//		String remark3 = null;
//		String sqlUpdate = null;
//		try {
//			List<SpanHostDataDetails> results = Adapter.selectSPANDataMvaError(BATCHID);
//			if(results!=null){
//				for(SpanHostDataDetails results2 : results){
//					ReplaceMvaAccResponse replaceMvaAccResponse = Service.replaceMvaAcc(results2.getBEN_ACCOUNT(), results2.getAMOUNT());
//					realAccount = replaceMvaAccResponse.getRealAccount();
//					status = replaceMvaAccResponse.getStatus();
//					mvaCompanyCode = replaceMvaAccResponse.getMvaCompanyCode();
//					trxDetailIDForMVA = replaceMvaAccResponse.getTrxDetailIdForMva();
//					
//					if("00".equalsIgnoreCase(status)){
//						remark3 = trxDetailIDForMVA;
//						sqlUpdate = "update SPAN_HOST_DATA_DETAILS SET BEN_ACCOUNT = '"+realAccount+"', CREDITACCNO = '"+realAccount+"', "
//								+ "TRXREMARK3 = '"+remark3+"', STATUS='0', MVA_STATUS='00', MVA_DESC= '"+mvaCompanyCode+"' where BATCHID = '"+BATCHID+"' "
//								+ "and TRXDETAILID = '"+results2.getTRXDETAILID()+"'";
//					}
//					else{
//						sqlUpdate = "update SPAN_HOST_DATA_DETAILS SET MVA_STATUS='"+status+"' where BATCHID = '"+BATCHID+"' and "
//								+ "TRXDETAILID = '"+results2.getTRXDETAILID()+"'";
//					}
//					
//					Adapter.updateSPAN(sqlUpdate);
//					
//				}
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
	
	public static void logWithSysout(Log log, String value){
		log.debug(value);
		System.out.println(value);
	}
	
	/**
	 * true if list not null and list.size != 0
	 */
	public static boolean isListEmpty(List list){
		if(list!=null 
				&& list.size()>0){
			return false;
		}
		return true;
	}
	
	/**
	 * check if big integer 0 or null
	 * @param value
	 * @return
	 */
	public static boolean isBigIntegerEmpty(BigInteger value){
		if(value==null
				|| value.compareTo(BigInteger.ZERO)==0){
			return true;
		}
		return false;
	}
	
	/**
	 * check if long is 0l or null
	 * @return
	 */
	public static boolean isLongEmpty(Long value){
		if(value==null
				|| value<1){
			return true;
		}
		return false;
	}
	
	public static double sizeListToDouble(List list){
		if(BusinessUtil.isListEmpty(list)){
			return Double.valueOf("0");
		}
		else{
			return Double.valueOf(String.valueOf(list.size()));
		}
	}
	
	public static String convertAmount(String amount){
		String convertedAmount = "";
		Double temp = 0.0;
		try{
			NumberFormat anotherFormat = NumberFormat.getNumberInstance(Locale.US);
			DecimalFormat anotherDFormat = (DecimalFormat) anotherFormat;
	        anotherDFormat.applyPattern("#.00");//use 2 digit after decimal
	        //anotherDFormat.applyPattern("#.0"); //use 1 digit after decimal
	        anotherDFormat.setGroupingUsed(true);
	        anotherDFormat.setGroupingSize(3);
	         
	        temp = Double.parseDouble(amount);
	        convertedAmount = String.valueOf(anotherDFormat.format(temp));
	        
	        //to kick out char ","
	        convertedAmount = convertedAmount.replace(",", "");
	        
	        if(".00".equalsIgnoreCase(convertedAmount)){
	        	convertedAmount = "0.00";
	        }
		}catch(Exception e){
			e.printStackTrace();
		}
		return convertedAmount;
	}
	
	/**
	 * mandiri.span.util:checkingAmountTrx
	 */
	public static String checkingAmountTrx(CheckingAmountTrxRequest request){
		InterfaceDAO dao = (InterfaceDAO) BusinessUtil.getDao(DaoConstant.ProviderDao);
		String totalAmountOut = null;
		String BatchID = request.getBatchID();
		String TrxHeaderID = request.getTrxHeaderID();
		String fileName = request.getFileName();
		String trxType = request.getTrxType();
		String totalRecord = request.getTotalRecord();
		String totalAmount = request.getTotalAmount();
		String totalRecSendToHost = request.getTotalRecSendToHost();
		String totalAmtSendToHost = request.getTotalAmtSendToHost();
		String waitStatus = request.getWaitStatus();
		String retryStatus = request.getRetryStatus();
		String returStatus = request.getReturStatus();
		String legStatus = request.getLegStatus(); 
		String postingTime = null;
		String createDate = null;
		String errorMessage = null;
		SpanStatus spanStatus = new SpanStatus();
		String sqlUpdate = null;
		String errorCode = null;
		SpanWaitSummary spanWaitSummary = new SpanWaitSummary();
		GetTotalRecAmtSendToHostResponse getTotalRecAmtSendToHostResponse = new GetTotalRecAmtSendToHostResponse();
		GetSPANDataFailedLastStatusResponse spanDataFailedLastStatus = new GetSPANDataFailedLastStatusResponse();
		//o sequnce.init.value
			//o map
			postingTime = CommonDate.getCurrentDateString("yyyyMMdd HHmmss");
			createDate = CommonDate.getCurrentDateString("dd-MM-yyyy");
			//c map
			//o branch.totalRec.SendToHost
				//o map.null
				if(StringUtils.isEmpty(totalRecSendToHost)){
					totalRecSendToHost = "0";
				}
				//c map.null
				//o map.default
				else{
					//do nothing
				}
				//c map.default
			//c branch.totalRec.SendToHost
			//o branch.totalAmtSendToHost
				//o null.map
				if(StringUtils.isEmpty(totalAmtSendToHost)){
					totalAmtSendToHost ="0";
				}
				//c null.map
				//o default.map
				else{
					//do nothing
				}
				//c default.map
			//c branch.totalAmtSendToHost	
			//o branch.trxType
				//o 1.map
				if("1".equalsIgnoreCase(trxType)){
					trxType ="2";
				}
				//c 1.map
			//c branch.trxType
		//c sequnce.init.value
		//o sequence
		try {
			//o branch.totalAmount
				//o 0.sequence
				if("0".equalsIgnoreCase(totalAmount)){
					//o mandiri.span.db.adapter:selectSPANHostDetails get transaction with status 1,2 (amount = 0)
					List<SpanHostDataDetails> results = Adapter.selectSPANHostDetails(BatchID, TrxHeaderID);
					//c mandiri.span.db.adapter:selectSPANHostDetails get transaction with status 1,2 (amount = 0)
					//o sequence
						//o loop.force.result.transaction
						for(SpanHostDataDetails results2 : results){
							//o sequence
								//o branch.results.status
									//o 2.map
									if("2".equalsIgnoreCase(results2.getSTATUS())){
										errorMessage = "Void Transaction";
									}
									//c 2.map
									//o default.sequence.mvaerror
									else{
										//o branch.result.mvastatus
											if("B8".equalsIgnoreCase(results2.getMVA_STATUS())){
												errorMessage = "UBP Message : Already Paid";
											}
											else if("B5".equalsIgnoreCase(results2.getMVA_STATUS())){
												errorMessage = "UBP Message : Bill Key is not Found";
											}
											else if("01".equalsIgnoreCase(results2.getMVA_STATUS())){
												errorMessage = "UBP Message : General Error";
											}
											else if(results2.getMVA_STATUS()==null){
												errorMessage = "                              ";
											}
											else{
												errorMessage = "                              ";
											}
										//c branch.result.mvastatus
										
									}
									//c default.sequence.mvaerror
									
								//c branch
								//o mandiri.span.db.adapter.odbc:insertSPANHostResponse
								SpanHostResponse spanHostResponse = new SpanHostResponse();
								
								spanHostResponse.setBatchId(results2.getBATCHID());
								spanHostResponse.setTrxHeaderId(results2.getTRXHEADERID());
								spanHostResponse.setTrxDetailId(results2.getTRXDETAILID());
								spanHostResponse.setDebitAccNo(results2.getDEBITACCNO());
								spanHostResponse.setDebitAcccurrCode(results2.getDEBITACCCURRCODE());
								spanHostResponse.setDebitAmount(results2.getAMOUNT());
								spanHostResponse.setChargeInstruction(results2.getCHARGEINSTRUCTION());
								spanHostResponse.setCreditAccNo(results2.getCREDITACCNO());
								spanHostResponse.setCreditAccName(results2.getCREDITACCNAME());
								spanHostResponse.setCreditTrfCurr(results2.getCREDITTRFCURR());
								spanHostResponse.setCreditTrfAmount(results2.getCREDITTRFAMOUNT());
								spanHostResponse.setTrxRemark1(results2.getTRXREMARK1());
								spanHostResponse.setTrxRemark2(results2.getTRXREMARK2());
								spanHostResponse.setTrxRemark3(results2.getTRXREMARK3());
								spanHostResponse.setTrxRemark4(results2.getTRXREMARK4());
								spanHostResponse.setFtServices(results2.getFTSERVICES());
								spanHostResponse.setBeneficiaryBankCode(results2.getBENEFICIARYBANKCODE());
								spanHostResponse.setBeneficiaryBankName(results2.getBENEFICIARYBANKNAME());
								spanHostResponse.setDebitRefNo(null);
								spanHostResponse.setCreditRefNo(null);
								spanHostResponse.setOrganizationDirName(null);
								spanHostResponse.setSwiftMethod(null);
								spanHostResponse.setResponseCode("0");
								spanHostResponse.setErrorCode("0");
								spanHostResponse.setErrorMessage(errorMessage);
								spanHostResponse.setRemittanceNo("0");
								spanHostResponse.setPostingTimeStamp(postingTime);
								spanHostResponse.setTellerId("0");
								spanHostResponse.setJournalSequenceNo("0");
								spanHostResponse.setReserve1(results2.getRESERVE1());
								spanHostResponse.setReserve2(results2.getRESERVE2());
								spanHostResponse.setReserve3(results2.getRESERVE3());
								spanHostResponse.setReserve4(results2.getRESERVE4());
								spanHostResponse.setReserve5(results2.getRESERVE5());
								spanHostResponse.setInstructionCode1(results2.getINSTRUCTIONCODE1());
								spanHostResponse.setResponseFlag("1");
								spanHostResponse.setLegStatus(results2.getLEGSTATUS());
								
								Odbc.insertSPANHostResponse(spanHostResponse);
								//c mandiri.span.db.adapter.odbc:insertSPANHostResponse	
							//c sequence.results.status
						}
						//c loop.force.result.transaction
						//o sequence
							//o mandiri.span.db.adapter.odbc:getExecutedTransactionSummary
							GetExecutedTransactionSummaryResponse spanExecutedSummary = Odbc.getExecutedTransactionSummary(BatchID, TrxHeaderID);
							//c mandiri.span.db.adapter.odbc:getExecutedTransactionSummary
							//o branch
								//o 3.sequence.execution.wait.transaction
								if("3".equalsIgnoreCase(waitStatus)){
									//o mandiri.span.db.service.odbc:getSPANDataFailedLastStatus
									spanDataFailedLastStatus = mii.span.db.service.Odbc.getSPANDataFailedLastStatus(fileName);
									//c mandiri.span.db.service.odbc:getSPANDataFailedLastStatus
									//o mandiri.span.db.adapter.odbc:getWaitTransactionSummary check waiting transaction
									GetWaitTransactionSummaryResponse getWaitTransactionSummaryResponse = Odbc.getWaitTransactionSummary(BatchID, TrxHeaderID);
									spanWaitSummary.setTotalRecord(getWaitTransactionSummaryResponse.getTotalWaitRec());
									spanWaitSummary.setTotalAmount(getWaitTransactionSummaryResponse.getTotalWaitAmount());
									//c mandiri.span.db.adapter.odbc:getWaitTransactionSummary check waiting transaction
									//o branch.totalRecord
										//o 0.sequence
										if("0".equalsIgnoreCase(spanWaitSummary.getTotalRecord())){
											//o map
											spanDataFailedLastStatus.setTotalSuccess(PubUtil.addInts(spanExecutedSummary.getTotalExecutedRec(), spanDataFailedLastStatus.getTotalSuccess()));
											spanDataFailedLastStatus.setTotalRetur("0");
											spanDataFailedLastStatus.setTotalRetry("0");
											spanDataFailedLastStatus.setTotalWait("0");
											spanDataFailedLastStatus.setSumStatusWat("1");
											spanDataFailedLastStatus.setSumStatusRty("1");
											spanDataFailedLastStatus.setSumStatusRtu("1");
											spanDataFailedLastStatus.setTotalProcess(PubUtil.addInts(spanDataFailedLastStatus.getTotalProcess(), "1"));
											
											
											spanStatus.setWaitStatus("1");
											spanStatus.setProcStatus("4");
											spanStatus.setProcDescription("Force Success");
											spanStatus.setResponseCode("0000");
											//c map
											//o map
												/*sqlUpdate = "update SPAN_DATA_VALIDATION set WAIT_STATUS = '"+spanStatus.getWaitStatus()+"',"
														+ "TOTAL_SENDTOHOST = '"+totalRecSendToHost+"', "
														+ "TOTAL_AMOUNT_SENDTOHOST = '"+totalAmtSendToHost+"',"
														+ "PROC_STATUS = '"+spanStatus.getProcStatus()+"',"
														+ "PROC_DESCRIPTION = '"+spanStatus.getProcDescription()+"',"
														+ "RESPONSE_cODE = '"+spanStatus.getResponseCode()+"'"
														+ "where FILE_NAME = '"+fileName+"' and BATCHID = '"+BatchID+"'";*/
												
												sqlUpdate = "UPDATE SpanDataValidation SET waitStatus = '"+spanStatus.getWaitStatus()+"',"
														+ " totalSendToHost = '"+totalRecSendToHost+"',"
														+ " totalAmountSendToHost = '"+totalAmtSendToHost+"',"
														+ " procStatuc = '"+spanStatus.getProcStatus()+"',"
														+ " procDescription = '"+spanStatus.getProcDescription()+"',"
														+ " responseCode = '"+spanStatus.getResponseCode()+"'"
														+ " WHERE fileName = '"+fileName+"' AND batchId = '"+BatchID+"'";
											//c map
										}
										//c 0.sequence
										//o default.sequence
										else{
											//o map
											spanDataFailedLastStatus.setTotalSuccess(PubUtil.addInts(spanExecutedSummary.getTotalExecutedRec(), spanDataFailedLastStatus.getTotalSuccess()));
											spanDataFailedLastStatus.setTotalRetur("0");
											spanDataFailedLastStatus.setTotalRetry("0");
											spanDataFailedLastStatus.setTotalWait(spanWaitSummary.getTotalRecord());
											spanDataFailedLastStatus.setSumStatusWat("2");
											spanDataFailedLastStatus.setSumStatusRty("1");
											spanDataFailedLastStatus.setSumStatusRtu("1");
											spanDataFailedLastStatus.setTotalProcess(PubUtil.addInts(spanDataFailedLastStatus.getTotalProcess(), "1"));
											
											
											spanStatus.setWaitStatus("0");
											spanStatus.setProcStatus("4");
											spanStatus.setProcDescription("Force Success");
											spanStatus.setResponseCode("0000");
											//c map
											//o map
											/*sqlUpdate = "update SPAN_DATA_VALIDATION set WAIT_STATUS = '"+spanStatus.getWaitStatus()+"',"
													+ "TOTAL_SENDTOHOST = '"+totalRecSendToHost+"', "
													+ "TOTAL_AMOUNT_SENDTOHOST = '"+totalAmtSendToHost+"',"
													+ "PROC_STATUS = '"+spanStatus.getProcStatus()+"',"
													+ "PROC_DESCRIPTION = '"+spanStatus.getProcDescription()+"',"
													+ "RESPONSE_cODE = '"+spanStatus.getResponseCode()+"'"
													+ "where FILE_NAME = '"+fileName+"' and BATCHID = '"+BatchID+"'";*/
											
											sqlUpdate = "UPDATE SpanDataValidation SET waitStatus = '"+spanStatus.getWaitStatus()+"',"
													+ " totalSendToHost = '"+totalRecSendToHost+"',"
													+ " totalAmountSendToHost = '"+totalAmtSendToHost+"',"
													+ " procStatuc = '"+spanStatus.getProcStatus()+"',"
													+ " procDescription = '"+spanStatus.getProcDescription()+"',"
													+ " responseCode = '"+spanStatus.getResponseCode()+"'"
													+ " WHERE fileName = '"+fileName+"' AND batchId = '"+BatchID+"'";
											//c map
										}
										//c default.sequence
									//c branch.totalRecord
									//o mandiri.span.db.adapter.odbc:insertSPANDataFailed
									SpanHostDataFailed spanHostDataFailed = new SpanHostDataFailed();
									spanHostDataFailed.setFileName(fileName);
									spanHostDataFailed.setBatchId(BatchID);
									spanHostDataFailed.setTrxHeaderId(TrxHeaderID);
									spanHostDataFailed.setRetryStatus(Long.valueOf(spanDataFailedLastStatus.getTotalRetry()));
									spanHostDataFailed.setReturStatus(Long.valueOf(spanDataFailedLastStatus.getTotalRetur()));
									spanHostDataFailed.setSuccessStatus(Long.valueOf(spanDataFailedLastStatus.getTotalSuccess()));
									spanHostDataFailed.setProcStatus(spanStatus.getProcStatus());
									spanHostDataFailed.setProcDescription(spanStatus.getProcDescription());
									spanHostDataFailed.setTotalProcess(Long.valueOf(spanDataFailedLastStatus.getTotalProcess()));
									spanHostDataFailed.setSumStatusRty(Long.valueOf(spanDataFailedLastStatus.getSumStatusRty()));
									spanHostDataFailed.setSumStatusRtu(Long.valueOf(spanDataFailedLastStatus.getSumStatusRtu()));
									spanHostDataFailed.setTrxType(trxType);
									spanHostDataFailed.setLegStatus(spanDataFailedLastStatus.getLegstatus());
									spanHostDataFailed.setWaitStatus(Long.valueOf(spanDataFailedLastStatus.getTotalWait()));
									spanHostDataFailed.setSumStatusWat(Long.valueOf(spanDataFailedLastStatus.getSumStatusWat()));
									
									Odbc.insertSPANDataFailed(spanHostDataFailed);
									//c mandiri.span.db.adapter.odbc:insertSPANDataFailed	
									//o mandiri.span.db.service:updateSPAN
									Query query = dao.getQuery(sqlUpdate);
									mii.span.db.Service.updateSPAN(query);
									//c mandiri.span.db.service:updateSPAN
								}
								//c 3.sequence.execution.wait.transaction
								//o default.firsttimeexecution
								else{
									//o mandiri.span.db.adapter.odbc:getWaitTransactionSummary
									GetWaitTransactionSummaryResponse getWaitTransactionSummaryResponse = Odbc.getWaitTransactionSummary(BatchID, TrxHeaderID);
									spanWaitSummary.setTotalRecord(getWaitTransactionSummaryResponse.getTotalWaitRec());
									spanWaitSummary.setTotalAmount(getWaitTransactionSummaryResponse.getTotalWaitAmount());
									//c mandiri.span.db.adapter.odbc:getWaitTransactionSummary
									//o branch.spanWaitSummaryTotalRecord
										//o 0.sequence.allTransactionHasBeenExecuted
										if("0".equalsIgnoreCase(spanWaitSummary.getTotalRecord())){
											//o map
											spanDataFailedLastStatus.setTotalSuccess(spanExecutedSummary.getTotalExecutedRec());
											spanDataFailedLastStatus.setTotalRetur("0");
											spanDataFailedLastStatus.setTotalRetry("0");
											spanDataFailedLastStatus.setTotalWait("0");
											spanDataFailedLastStatus.setSumStatusWat("1");
											spanDataFailedLastStatus.setSumStatusRty("1");
											spanDataFailedLastStatus.setSumStatusRtu("1");
											
											spanStatus.setWaitStatus("1");
											spanStatus.setProcStatus("4");
											spanStatus.setProcDescription("Force to Success");
											spanStatus.setResponseCode("0000");
											//c map
											//o map
											/*sqlUpdate ="update SPAN_DATA_VALIDATION set WAIT_STATUS = '"+spanStatus.getWaitStatus()+"',"
													+ "TOTAL_SENDTOHOST = '"+totalRecSendToHost+"', "
													+ "TOTAL_AMOUNT_SENDTOHOST = '"+totalAmtSendToHost+"',"
													+ "PROC_STATUS = '"+spanStatus.getProcStatus()+"',"
													+ "PROC_DESCRIPTION = '"+spanStatus.getProcDescription()+"',"
													+ "RESPONSE_cODE = '"+spanStatus.getResponseCode()+"'"
													+ "where FILE_NAME = '"+fileName+"' and BATCHID = '"+BatchID+"'";*/
											
											sqlUpdate = "UPDATE SpanDataValidation SET waitStatus = '"+spanStatus.getWaitStatus()+"',"
													+ " totalSendToHost = '"+totalRecSendToHost+"',"
													+ " totalAmountSendToHost = '"+totalAmtSendToHost+"',"
													+ " procStatuc = '"+spanStatus.getProcStatus()+"',"
													+ " procDescription = '"+spanStatus.getProcDescription()+"',"
													+ " responseCode = '"+spanStatus.getResponseCode()+"'"
													+ " WHERE fileName = '"+fileName+"' AND batchId = '"+BatchID+"'";
											//c map
										}
										//c 0.sequence.allTransactionHasBeenExecuted
										//o default.sequence
										else{
											//o map
											spanDataFailedLastStatus.setTotalSuccess(spanExecutedSummary.getTotalExecutedRec());
											spanDataFailedLastStatus.setTotalRetur("0");
											spanDataFailedLastStatus.setTotalRetry("0");
											spanDataFailedLastStatus.setTotalWait(spanWaitSummary.getTotalRecord());
											spanDataFailedLastStatus.setSumStatusWat("2");
											spanDataFailedLastStatus.setSumStatusRty("1");
											spanDataFailedLastStatus.setSumStatusRtu("1");
											
											spanStatus.setWaitStatus("0");
											spanStatus.setProcStatus("4");
											spanStatus.setProcDescription("Force to Success");
											spanStatus.setResponseCode("0000");
											//c map
											//o map
											/*sqlUpdate = "update SPAN_DATA_VALIDATION set WAIT_STATUS = '"+spanStatus.getWaitStatus()+"',"
													+ "TOTAL_SENDTOHOST = '"+totalRecSendToHost+"', "
													+ "TOTAL_AMOUNT_SENDTOHOST = '"+totalAmtSendToHost+"', "
													+ "PROC_STATUS = '"+spanStatus.getProcStatus()+"', "
													+ "PROC_DESCRIPTION = '"+spanStatus.getProcDescription()+"',"
													+ "RESPONSE_cODE = '"+spanStatus.getResponseCode()+"'"
													+ "where FILE_NAME = '"+fileName+"' and BATCHID = '"+BatchID+"'";*/
											
											sqlUpdate = "UPDATE SpanDataValidation SET waitStatus = '"+spanStatus.getWaitStatus()+"',"
													+ " totalSendToHost = '"+totalRecSendToHost+"',"
													+ " totalAmountSendToHost = '"+totalAmtSendToHost+"',"
													+ " procStatuc = '"+spanStatus.getProcStatus()+"',"
													+ " procDescription = '"+spanStatus.getProcDescription()+"',"
													+ " responseCode = '"+spanStatus.getResponseCode()+"'"
													+ " WHERE fileName = '"+fileName+"' AND batchId = '"+BatchID+"'";
											//c map
										}
										//c default.sequence
									//c branch.spanWaitSummaryTotalRecord
									//o mandiri.span.db.adapter.odbc:insertSPANDataFailed
									SpanHostDataFailed spanHostDataFailed = new SpanHostDataFailed();
									spanHostDataFailed.setFileName(fileName);
									spanHostDataFailed.setBatchId(BatchID);
									spanHostDataFailed.setTrxHeaderId(TrxHeaderID);
									spanHostDataFailed.setRetryStatus(Long.valueOf(spanDataFailedLastStatus.getTotalRetry()));
									spanHostDataFailed.setReturStatus(Long.valueOf(spanDataFailedLastStatus.getTotalRetur()));
									spanHostDataFailed.setSuccessStatus(Long.valueOf(spanDataFailedLastStatus.getTotalSuccess()));
									spanHostDataFailed.setProcStatus(spanStatus.getProcStatus());
									spanHostDataFailed.setProcDescription(spanStatus.getProcDescription());
									spanHostDataFailed.setTotalProcess(1l);
									spanHostDataFailed.setSumStatusRty(Long.valueOf(spanDataFailedLastStatus.getSumStatusRty()));
									spanHostDataFailed.setSumStatusRtu(Long.valueOf(spanDataFailedLastStatus.getSumStatusRtu()));
									spanHostDataFailed.setTrxType(trxType);
									spanHostDataFailed.setLegStatus(spanDataFailedLastStatus.getLegstatus());
									spanHostDataFailed.setWaitStatus(Long.valueOf(spanDataFailedLastStatus.getTotalWait()));
									spanHostDataFailed.setSumStatusWat(Long.valueOf(spanDataFailedLastStatus.getSumStatusWat()));
									
									Odbc.insertSPANDataFailed(spanHostDataFailed);
									//c mandiri.span.db.adapter.odbc:insertSPANDataFailed
									//o mandiri.span.db.service:updateSPAN
									Query query = dao.getQuery(sqlUpdate);
									mii.span.db.Service.updateSPAN(query);
									//c mandiri.span.db.service:updateSPAN
								}
								//c default.firsttimeexecution
							//c branch
								
						//c sequence
					//c sequence
				}
				//c 0.sequence
				//o default.sequence
				else{
					//o mandiri.span.db.adapter:selectSPANHostDetails
					//get transaction with status 1,2 (amount = 0)
					List<SpanHostDataDetails> results = Adapter.selectSPANHostDetails(BatchID, TrxHeaderID);
					//c mandiri.span.db.adapter:selectSPANHostDetails
					//o branch.countRecord
						//o 0.sequence.allRecordSendToHost
						if(results == null 
								|| results.size()==0){
							//o mandiri.span.db.adapter.odbc:getTotalRecAmtSendToHost
							getTotalRecAmtSendToHostResponse = Odbc.getTotalRecAmtSendToHost(BatchID, TrxHeaderID);
							//c mandiri.span.db.adapter.odbc:getTotalRecAmtSendToHost
							//o branch.on.waitStatus
								//o 3.map
								if("3".equalsIgnoreCase(waitStatus)){	//sum total request send to host
									totalRecSendToHost = PubUtil.addInts(totalRecSendToHost, getTotalRecAmtSendToHostResponse.getTotalRecSendToHost());
									totalAmtSendToHost = PubUtil.addFloats(totalAmtSendToHost, getTotalRecAmtSendToHostResponse.getTotalAmtSendToHost(), "0");
								}
								//c 3.map
								//o default.branch.firstTimeExecution
								else{
									//o map
									if("3".equalsIgnoreCase(retryStatus)
											|| "3".equalsIgnoreCase(returStatus)){
										//no need to update total send to host
										//do nothing
									} 
									//c map
									//o default.map
									else{
										totalRecSendToHost = getTotalRecAmtSendToHostResponse.getTotalRecSendToHost();
										totalAmtSendToHost = getTotalRecAmtSendToHostResponse.getTotalAmtSendToHost();
									}
									//c default.map
								}
								//c default.branch.firstTimeExecution
							//c branch.on.waitStatus
							//o mandiri.span.db.adapter.odbc:updateTotalRecAmtSendToHost
							Odbc.updateTotalRecAmtSendToHost(totalRecSendToHost, totalAmtSendToHost, fileName);
							//c mandiri.span.db.adapter.odbc:updateTotalRecAmtSendToHost
						}
						//c 0.sequence.allRecordSendToHost
						//o default
						else{
							//o map
							RetryDetails retryDetails = new RetryDetails();
							retryDetails.setTotalRetry("0");
							retryDetails.setAddRetryStatement("");
							retryDetails.setTotalRetryAmount("0");
							
							SuccessDetail successDetail = new SuccessDetail();
							successDetail.setTotalSuccess("0");
							//c map
							//o loop.over.results
							for(SpanHostDataDetails results2 : results){
								//o sequence
									//o branch.results.status
										//o 2.map
										if("2".equalsIgnoreCase(results2.getSTATUS())){	//void | force success
											errorCode = "0";
											errorMessage = "Void Transaction";
											successDetail.setTotalSuccess(PubUtil.addInts(successDetail.getTotalSuccess(), "1"));
										}
										//c 2.map
										//o 3.map
										else if("3".equalsIgnoreCase(results2.getSTATUS())){ //PM 3 | force retry
											retryDetails.setTotalRetry(PubUtil.addInts(retryDetails.getTotalRetry(), "1"));
											retryDetails.setTotalRetryAmount(PubUtil.addFloats(retryDetails.getTotalRetryAmount(), results2.getAMOUNT(), "0"));
											errorMessage = "Unsupported transaction";
											errorCode = "024";
										}
										//c 3.map
										//o default.sequence.amount0,MVAerror forceretry
										else{
											//o map.count.force.retry
											errorCode = "024";
											errorMessage ="Unsupported transaction";
											retryDetails.setTotalRetry(PubUtil.addInts(retryDetails.getTotalRetry(), "1"));
											retryDetails.setTotalRetryAmount(PubUtil.addFloats(retryDetails.getTotalRetryAmount(), results2.getAMOUNT(), "0"));
											//c map.count.force.retry
											//o branch.results.mva_status
												if("B8".equalsIgnoreCase(results2.getMVA_STATUS())){
													errorCode = "025";
													errorMessage = "UBP Message : Already Paid";
												}
												else if("B5".equalsIgnoreCase(results2.getMVA_STATUS())){
													errorCode = "026";
													errorMessage = "UBP Message : Bill Key is not Found";
												}
												else if("01".equalsIgnoreCase(results2.getMVA_STATUS())){
													errorCode = "027";
													errorMessage = "UBP Message : General Error";
												}
												else if(results2.getMVA_STATUS()==null){
													errorCode = "029";
													errorMessage = "                              ";
												}
												else{
													errorCode = "028";
													errorMessage = "                              ";
												}
											//c branch.results.mva_status
										}
										//c default.sequence.amount0,MVAerror forceretry
									//c branch.results.status
									//o mandiri.span.db.adapter.odbc:insertSPANHostResponse
									SpanHostResponse spanResponse = new SpanHostResponse();
									spanResponse.setBatchId(results2.getBATCHID());
									spanResponse.setTrxHeaderId(results2.getTRXHEADERID());
									spanResponse.setTrxDetailId(results2.getTRXDETAILID());
									spanResponse.setDebitAccNo(results2.getDEBITACCNO());
									spanResponse.setDebitAcccurrCode(results2.getDEBITACCCURRCODE());
									spanResponse.setDebitAmount(results2.getAMOUNT());
									spanResponse.setChargeInstruction(results2.getCHARGEINSTRUCTION());
									spanResponse.setCreditAccNo(results2.getCREDITACCNO());
									spanResponse.setCreditAccName(results2.getCREDITACCNAME());
									spanResponse.setCreditTrfCurr(results2.getCREDITTRFCURR());
									spanResponse.setCreditTrfAmount(results2.getCREDITTRFAMOUNT());
									spanResponse.setTrxRemark1(results2.getTRXREMARK1());
									spanResponse.setTrxRemark2(results2.getTRXREMARK2());
									spanResponse.setTrxRemark3(results2.getTRXREMARK3());
									spanResponse.setTrxRemark4(results2.getTRXREMARK4());
									spanResponse.setFtServices(results2.getFTSERVICES());
									spanResponse.setBeneficiaryBankCode(results2.getBENEFICIARYBANKCODE());
									spanResponse.setBeneficiaryBankName(results2.getBENEFICIARYBANKNAME());
									spanResponse.setDebitRefNo(null);
									spanResponse.setCreditRefNo(null);
									spanResponse.setOrganizationDirName(null);
									spanResponse.setSwiftMethod(null);
									spanResponse.setResponseCode(errorCode);
									spanResponse.setErrorCode(errorCode);
									spanResponse.setErrorMessage(errorMessage);
									spanResponse.setRemittanceNo("0");
									spanResponse.setPostingTimeStamp(postingTime);
									spanResponse.setTellerId("0");
									spanResponse.setJournalSequenceNo("0");
									spanResponse.setReserve1(results2.getRESERVE1());
									spanResponse.setReserve2(results2.getRESERVE2());
									spanResponse.setReserve3(results2.getRESERVE3());
									spanResponse.setReserve4(results2.getRESERVE4());
									spanResponse.setReserve5(results2.getRESERVE5());
									spanResponse.setInstructionCode1(results2.getINSTRUCTIONCODE1());
									spanResponse.setResponseFlag("1");
									spanResponse.setLegStatus(results2.getLEGSTATUS());
									
									Odbc.insertSPANHostResponse(spanResponse);
									//c mandiri.span.db.adapter.odbc:insertSPANHostResponse	
								//c sequence
							}
							//c loop.over.results
							//o sequence
								//o mandiri.span.db.adapter.odbc:getExecutedTransactionSummary
								GetExecutedTransactionSummaryResponse spanExecutedSummary = Odbc.getExecutedTransactionSummary(BatchID, TrxHeaderID);
								//c mandiri.span.db.adapter.odbc:getExecutedTransactionSummary
								//o mandiri.span.db.adapter.odbc:getTotalRecAmtSendToHost
								getTotalRecAmtSendToHostResponse = Odbc.getTotalRecAmtSendToHost(BatchID, TrxHeaderID);
								//c mandiri.span.db.adapter.odbc:getTotalRecAmtSendToHost
								//o branch
									//o sequence
									if(results.size() == Integer.valueOf(spanExecutedSummary.getTotalExecutedRec())){
										//o branch
											//o sequence execution wait transaction
											if("3".equalsIgnoreCase(waitStatus)){
												//o mandiri.span.db.service.odbc:getSPANDataFailedLastStatus
												spanDataFailedLastStatus = mii.span.db.service.Odbc.getSPANDataFailedLastStatus(fileName);
												//c mandiri.span.db.service.odbc:getSPANDataFailedLastStatus
												//o mandiri.span.db.adapter.odbc:getWaitTransactionSummary
												GetWaitTransactionSummaryResponse getWaitTransactionSummaryResponse = Odbc.getWaitTransactionSummary(BatchID, TrxHeaderID);
												spanWaitSummary.setTotalRecord(getWaitTransactionSummaryResponse.getTotalWaitRec());
												spanWaitSummary.setTotalAmount(getWaitTransactionSummaryResponse.getTotalWaitAmount());
												//c mandiri.span.db.adapter.odbc:getWaitTransactionSummary
												//o branch.on.spanwaitsummary.totalRecord
													//o 0.sequence
													if(spanWaitSummary.getTotalRecord()==null
															|| "0".equalsIgnoreCase(spanWaitSummary.getTotalRecord())){
														//o map
														spanDataFailedLastStatus.setTotalRetur("0");
														spanDataFailedLastStatus.setTotalRetry("0");
														spanDataFailedLastStatus.setTotalWait("0");
														spanDataFailedLastStatus.setSumStatusWat("1");
														spanDataFailedLastStatus.setSumStatusRty("1");
														spanDataFailedLastStatus.setSumStatusRtu("1");
														spanDataFailedLastStatus.setTotalProcess(PubUtil.addInts(spanDataFailedLastStatus.getTotalProcess(), "1"));
														
														spanStatus.setWaitStatus("1");
														spanStatus.setProcStatus("4");
														spanStatus.setProcDescription("Force Success");
														spanStatus.setResponseCode("0000");
														
														retryDetails.setAddRetryStatement(" ");
														//c map
														//o branch count for force RETRY
															//o sequence
															if(retryDetails.getTotalRetry() != null
																	&& Integer.parseInt(retryDetails.getTotalRetry())>0){
																//o map
																spanDataFailedLastStatus.setTotalRetry(PubUtil.addInts(spanDataFailedLastStatus.getTotalRetry(), retryDetails.getTotalRetry()));
																spanDataFailedLastStatus.setSumStatusRty("2");
																spanStatus.setProcDescription("Force Retry");
																//c map
																//o map retry add query
																retryDetails.setAddRetryStatement(",  RETRY_STATUS = '0'");
																//c map retry add query
															}
															//c sequence
															//o default.map
															else{
																spanDataFailedLastStatus.setTotalSuccess(PubUtil.addInts(spanExecutedSummary.getTotalExecutedRec(), spanDataFailedLastStatus.getTotalSuccess()));
																
															}
															//c default.map
														//c branch count for force RETRY
														//o map
														/*sqlUpdate = "update SPAN_DATA_VALIDATION set WAIT_STATUS = '"+spanStatus.getWaitStatus()+"',"
														+ "TOTAL_SENDTOHOST = '"+totalRecSendToHost+"',"
														+ "TOTAL_AMOUNT_SENDTOHOST = '"+totalAmtSendToHost+"',"
														+ "PROC_STATUS = '"+spanStatus.getProcStatus()+"',"
														+ "PROC_DESCRIPTION = '"+spanStatus.getProcDescription()+"',"
														+ "RESPONSE_CODE = '"+spanStatus.getResponseCode()+"'"+retryDetails.getAddRetryStatement()+" "
														+ "where FILE_NAME = '"+fileName+"' and BATCHID = '"+BatchID+"'";*/
														
														sqlUpdate = "UPDATE SpanDataValidation SET waitStatus = '"+spanStatus.getWaitStatus()+"',"
														+ " totalSendToHost = '"+totalRecSendToHost+"',"
														+ " totalAmountSendToHost = '"+totalAmtSendToHost+"',"
														+ " procStatuc = '"+spanStatus.getProcStatus()+"',"
														+ " procDescription = '"+spanStatus.getProcDescription()+"',"
														+ " responseCode = '"+spanStatus.getResponseCode()+"' "+retryDetails.getAddRetryStatement()+" "
														+ "	where fileName = '"+fileName+"' AND batchId = '"+BatchID+"'";
														//c map	
													}
													//c 0.sequence
													//o default
													else{
														//o map
														spanDataFailedLastStatus.setTotalRetur("0");
														spanDataFailedLastStatus.setTotalRetry("0");
														spanDataFailedLastStatus.setTotalWait(spanWaitSummary.getTotalRecord());
														spanDataFailedLastStatus.setSumStatusWat("2");
														spanDataFailedLastStatus.setSumStatusRty("1");
														spanDataFailedLastStatus.setSumStatusRtu("1");
														spanDataFailedLastStatus.setTotalProcess(PubUtil.addInts(spanDataFailedLastStatus.getTotalProcess(), "1"));
														
														spanStatus.setWaitStatus("0");
														spanStatus.setProcStatus("4");
														spanStatus.setProcDescription("Force to Succes");
														spanStatus.setResponseCode("0000");
														
														retryDetails.setAddRetryStatement(" ");
														
														//c map
														//o branch count for force RETRY
															//o sequence
															if(retryDetails.getTotalRetry()!=null 
																	&& Integer.valueOf(retryDetails.getTotalRetry())>0){
																//o map
																spanDataFailedLastStatus.setTotalRetry(PubUtil.addInts(spanDataFailedLastStatus.getTotalRetry(), retryDetails.getTotalRetry()));
																spanDataFailedLastStatus.setSumStatusRty("2");
																//c map
																//o map -- retry add query
																spanStatus.setProcDescription("force retry");
																retryDetails.setAddRetryStatement(",  RETRY_STATUS = '0'");
																//c map -- retry add query
															}
															//c sequence
															//o default.map
															else{
																spanDataFailedLastStatus.setTotalSuccess(PubUtil.addInts(spanDataFailedLastStatus.getTotalSuccess(), spanExecutedSummary.getTotalExecutedRec()));
															}
															//c default.map
														//c branch count for force RETRY
														//o map
														/*sqlUpdate = "update SPAN_DATA_VALIDATION set WAIT_STATUS = '"+spanStatus.getWaitStatus()+"',"
															+ "TOTAL_SENDTOHOST = '"+totalRecSendToHost+"',"
															+ "TOTAL_AMOUNT_SENDTOHOST = '"+totalAmtSendToHost+"',"
															+ "PROC_STATUS = '"+spanStatus.getProcStatus()+"',"
															+ "PROC_DESCRIPTION = '"+spanStatus.getProcDescription()+"',"
															+ "RESPONSE_cODE = '"+spanStatus.getResponseCode()+"'"
															+ ""+retryDetails.getAddRetryStatement()+""
															+ "where FILE_NAME = '"+fileName+"' and BATCHID = '"+BatchID+"'";*/
														
														sqlUpdate = "UPDATE SpanDataValidation SET waitStatus = '"+spanStatus.getWaitStatus()+"',"
															+ " totalSendToHost = '"+totalRecSendToHost+"',"
															+ " totalAmountSendToHost = '"+totalAmtSendToHost+"',"
															+ " procStatuc = '"+spanStatus.getProcStatus()+"',"
															+ " procDescription = '"+spanStatus.getProcDescription()+"',"
															+ " responseCode = '"+spanStatus.getResponseCode()+"'"+retryDetails.getAddRetryStatement()+" "
															+ "	WHERE fileName = '"+fileName+"' AND batchId = '"+BatchID+"'";
														//c map	
													}
													//c default
												//c branch.on.spanwaitsummary.totalRecord
												//o mandiri.span.db.adapter.odbc:insertSPANDataFailed
												SpanHostDataFailed spanHostDataFailed = new SpanHostDataFailed();
												spanHostDataFailed.setFileName(fileName);
												spanHostDataFailed.setBatchId(BatchID);
												spanHostDataFailed.setTrxHeaderId(TrxHeaderID);
												spanHostDataFailed.setRetryStatus(Long.valueOf(spanDataFailedLastStatus.getTotalRetry()));
												spanHostDataFailed.setReturStatus(Long.valueOf(spanDataFailedLastStatus.getTotalRetur()));
												spanHostDataFailed.setSuccessStatus(Long.valueOf(spanDataFailedLastStatus.getTotalSuccess()));
												spanHostDataFailed.setProcStatus(spanStatus.getProcStatus());
												spanHostDataFailed.setProcDescription(spanStatus.getProcDescription());
												spanHostDataFailed.setTotalProcess(Long.valueOf(spanDataFailedLastStatus.getTotalProcess()));
												spanHostDataFailed.setSumStatusRty(Long.valueOf(spanDataFailedLastStatus.getSumStatusRty()));
												spanHostDataFailed.setSumStatusRtu(Long.valueOf(spanDataFailedLastStatus.getSumStatusRtu()));
												spanHostDataFailed.setTrxType(trxType);
												spanHostDataFailed.setLegStatus(spanDataFailedLastStatus.getLegstatus());
												spanHostDataFailed.setWaitStatus(Long.valueOf(spanDataFailedLastStatus.getTotalWait()));
												spanHostDataFailed.setSumStatusWat(Long.valueOf(spanDataFailedLastStatus.getSumStatusWat()));
												
												Odbc.insertSPANDataFailed(spanHostDataFailed);
												//c mandiri.span.db.adapter.odbc:insertSPANDataFailed	
												//o mandiri.span.db.service:updateSPAN
												Query query = dao.getQuery(sqlUpdate);
												mii.span.db.Service.updateSPAN(query);
												//c mandiri.span.db.service:updateSPAN
											}
											//c sequence execution wait transaction
											//o sequence execution PM3 retry transaction
											else if("3".equalsIgnoreCase(retryStatus)){
												//o mandiri.span.db.service.odbc:getSPANDataFailedLastStatus
												spanDataFailedLastStatus = mii.span.db.service.Odbc.getSPANDataFailedLastStatus(fileName);
												//c mandiri.span.db.service.odbc:getSPANDataFailedLastStatus
												//o map
												spanDataFailedLastStatus.setSumStatusRty("2");
												spanDataFailedLastStatus.setTotalProcess(PubUtil.addInts(spanDataFailedLastStatus.getTotalProcess(), "1"));
												spanStatus.setProcStatus("4");
												spanStatus.setProcDescription("Force Retry");
												spanStatus.setResponseCode("0000");
												retryDetails.setAddRetryStatement(" ");
												//c map
												//o map
												/*sqlUpdate = "update SPAN_DATA_VALIDATION set PROC_STATUS = '"+spanStatus.getProcStatus()+"', "
														+ "PROC_DESCRIPTION = '"+spanStatus.getProcDescription()+"',"
														+ "RESPONSE_CODE = '"+spanStatus.getResponseCode()+"'"
														+ "where FILE_NAME = '"+fileName+"' and BATCHID = '"+BatchID+"'";*/
												
												sqlUpdate = "UPDATE SpanDataValidation set procStatuc = '"+spanStatus.getProcStatus()+"',"
														+ " procDescription = '"+spanStatus.getProcDescription()+"',"
														+ " responseCode = '"+spanStatus.getResponseCode()+"'"
														+ " WHERE fileName = '"+fileName+"' AND batchId = '"+BatchID+"'";
												//c map
												//o mandiri.span.db.adapter.odbc:insertSPANDataFailed
												SpanHostDataFailed spanHostDataFailed = new SpanHostDataFailed();
												spanHostDataFailed.setFileName(fileName);
												spanHostDataFailed.setBatchId(BatchID);
												spanHostDataFailed.setTrxHeaderId(TrxHeaderID);
												spanHostDataFailed.setRetryStatus(Long.valueOf(spanDataFailedLastStatus.getTotalRetry()));
												spanHostDataFailed.setReturStatus(Long.valueOf(spanDataFailedLastStatus.getTotalRetur()));
												spanHostDataFailed.setSuccessStatus(Long.valueOf(spanDataFailedLastStatus.getTotalSuccess()));
												spanHostDataFailed.setProcStatus(spanStatus.getProcStatus());
												spanHostDataFailed.setProcDescription(spanStatus.getProcDescription());
												spanHostDataFailed.setTotalProcess(Long.valueOf(spanDataFailedLastStatus.getTotalProcess()));
												spanHostDataFailed.setSumStatusRty(Long.valueOf(spanDataFailedLastStatus.getSumStatusRty()));
												spanHostDataFailed.setSumStatusRtu(Long.valueOf(spanDataFailedLastStatus.getSumStatusRtu()));
												spanHostDataFailed.setTrxType(trxType);
												spanHostDataFailed.setLegStatus(spanDataFailedLastStatus.getLegstatus());
												spanHostDataFailed.setWaitStatus(Long.valueOf(spanDataFailedLastStatus.getTotalWait()));
												spanHostDataFailed.setSumStatusWat(Long.valueOf(spanDataFailedLastStatus.getSumStatusWat()));
												
												Odbc.insertSPANDataFailed(spanHostDataFailed);
												//c mandiri.span.db.adapter.odbc:insertSPANDataFailed
												//o mandiri.span.db.service:updateSPAN
												Query query = dao.getQuery(sqlUpdate);
												mii.span.db.Service.updateSPAN(query);
												//c mandiri.span.db.service:updateSPAN
											}
											//c sequence execution PM3 retry transaction
											//o default.sequence
											else{
												//o mandiri.span.db.adapter.odbc:getWaitTransactionSummary
												GetWaitTransactionSummaryResponse getWaitTransactionSummaryResponse = Odbc.getWaitTransactionSummary(BatchID, TrxHeaderID);
												spanWaitSummary.setTotalRecord(getWaitTransactionSummaryResponse.getTotalWaitRec());
												spanWaitSummary.setTotalAmount(getWaitTransactionSummaryResponse.getTotalWaitAmount());
												//c mandiri.span.db.adapter.odbc:getWaitTransactionSummary
												//o branch.spanWaitSummary.totalRecord
													//o 0.sequence
													if("0".equalsIgnoreCase(spanWaitSummary.getTotalRecord())){
														//o map
														spanDataFailedLastStatus.setTotalSuccess(successDetail.getTotalSuccess());
														spanDataFailedLastStatus.setTotalRetur("0");
														spanDataFailedLastStatus.setTotalRetry("0");
														spanDataFailedLastStatus.setTotalWait("0");
														spanDataFailedLastStatus.setSumStatusWat("1");
														spanDataFailedLastStatus.setSumStatusRty("1");
														spanDataFailedLastStatus.setSumStatusRtu("1");
														
														spanStatus.setWaitStatus("1");
														spanStatus.setProcStatus("4");
														spanStatus.setProcDescription("Force Success");
														spanStatus.setResponseCode("0000");
														
														retryDetails.setAddRetryStatement(" ");
														//c map
														//o branch count for force RETRY
															//o sequence
															if(retryDetails.getTotalRetry()!=null 
																	&& Integer.valueOf(retryDetails.getTotalRetry())>0){
																//o map
																totalAmtSendToHost = PubUtil.addFloats(totalAmtSendToHost, retryDetails.getTotalRetryAmount(), "0");
																spanDataFailedLastStatus.setTotalRetry(PubUtil.addInts(spanDataFailedLastStatus.getTotalRetry(), retryDetails.getTotalRetry()));
																spanDataFailedLastStatus.setSumStatusRty("2");
																//c map
																//o map -- retry add query
																spanStatus.setProcDescription(PubUtil.concat(spanStatus.getProcDescription(),"; Force To Retry"));
																retryDetails.setAddRetryStatement(",  RETRY_STATUS = '0'");
																//c map -- retry add query
															}
															//c sequence
														//c branch count for force RETRY
														//o map
														/*sqlUpdate = "update SPAN_DATA_VALIDATION set WAIT_STATUS = '"+spanStatus.getWaitStatus()+"',"
																+ "TOTAL_SENDTOHOST = '"+totalRecSendToHost+"', "
																+ "TOTAL_AMOUNT_SENDTOHOST = '"+totalAmtSendToHost+"', "
																+ "PROC_STATUS = '"+spanStatus.getProcStatus()+"',"
																+ "PROC_DESCRIPTION = '"+spanStatus.getProcDescription()+"', "
																+ "RESPONSE_cODE = '"+spanStatus.getResponseCode()+"'"+retryDetails.getAddRetryStatement()+" "
																+ "where FILE_NAME = '"+fileName+"' and BATCHID = '"+BatchID+"'";*/
														
														sqlUpdate = "UPDATE SpanDataValidation set waitStatus = '"+spanStatus.getWaitStatus()+"',"
																+ " totalSendToHost = '"+totalRecSendToHost+"',"
																+ " totalAmountSendToHost = '"+totalAmtSendToHost+"',"
																+ " procStatuc = '"+spanStatus.getProcStatus()+"',"
																+ " procDescription = '"+spanStatus.getProcDescription()+"',"
																+ " responseCode = '"+spanStatus.getResponseCode()+"'"+retryDetails.getAddRetryStatement()+""
																+ " where fileName = '"+fileName+"' AND batchId = '"+BatchID+"'";
														//c map	
													}
													//c 0.sequence
													//o default.sequence
													else{
														//o map
														spanDataFailedLastStatus.setTotalSuccess(successDetail.getTotalSuccess());
														spanDataFailedLastStatus.setTotalRetur("0");
														spanDataFailedLastStatus.setTotalRetry("0");
														spanDataFailedLastStatus.setTotalWait(spanWaitSummary.getTotalRecord());
														spanDataFailedLastStatus.setSumStatusWat("2");
														spanDataFailedLastStatus.setSumStatusRty("1");
														spanDataFailedLastStatus.setSumStatusRtu("1");
														
														spanStatus.setWaitStatus("0");
														spanStatus.setProcStatus("4");
														spanStatus.setProcDescription("Force to Success");
														spanStatus.setResponseCode("0000");
														
														retryDetails.setAddRetryStatement(" ");
														//c map
														//o branch -- count for force RETRY
															//o sequence
															if(retryDetails.getTotalRetry()!=null 
																	&& Integer.valueOf(retryDetails.getTotalRetry())>0){
																//o map
																totalAmtSendToHost = PubUtil.addFloats(totalAmtSendToHost, retryDetails.getTotalRetryAmount(), "0");
																spanDataFailedLastStatus.setTotalRetry(PubUtil.addInts(spanDataFailedLastStatus.getTotalRetry(), retryDetails.getTotalRetry()));
																spanDataFailedLastStatus.setSumStatusRty("2");
																//c map
																//o map -- retry add query
																spanStatus.setProcDescription(PubUtil.concat(spanStatus.getProcDescription(),"; Force To Retry"));
																retryDetails.setAddRetryStatement(",  RETRY_STATUS = '0'");
																//c map -- retry add query
															}
															//c sequence
														//c branch -- count for force RETRY
														//o map
														/*sqlUpdate = "update SPAN_DATA_VALIDATION set WAIT_STATUS = '"+spanStatus.getWaitStatus()+"', "
																+ "TOTAL_SENDTOHOST = '"+totalRecSendToHost+"', "
																+ "TOTAL_AMOUNT_SENDTOHOST = '"+totalAmtSendToHost+"', "
																+ "PROC_STATUS = '"+spanStatus.getProcStatus()+"', "
																+ "PROC_DESCRIPTION = '"+spanStatus.getProcDescription()+"', "
																+ "RESPONSE_cODE = '"+spanStatus.getResponseCode()+"' "+retryDetails.getAddRetryStatement()+" "
																+ "where FILE_NAME = '"+fileName+"' and BATCHID = '"+BatchID+"'";*/
														
														sqlUpdate = "UPDATE SpanDataValidation set waitStatus = '"+spanStatus.getWaitStatus()+"',"
																+ " totalSendToHost = '"+totalRecSendToHost+"',"
																+ " totalAmountSendToHost = '"+totalAmtSendToHost+"',"
																+ " procStatuc = '"+spanStatus.getProcStatus()+"',"
																+ " procDescription = '"+spanStatus.getProcDescription()+"',"
																+ " responseCode = '"+spanStatus.getResponseCode()+"' "+retryDetails.getAddRetryStatement()+""
																+ " where fileName = '"+fileName+"' AND batchId = '"+BatchID+"'";
														//c map	
													}
													//c default.sequence
												//c branch
												//o mandiri.span.db.adapter.odbc:insertSPANDataFailed
												SpanHostDataFailed spanHostDataFailed = new SpanHostDataFailed();
												spanHostDataFailed.setFileName(fileName);
												spanHostDataFailed.setBatchId(BatchID);
												spanHostDataFailed.setTrxHeaderId(TrxHeaderID);
												spanHostDataFailed.setRetryStatus(Long.valueOf(spanDataFailedLastStatus.getTotalRetry()));
												spanHostDataFailed.setReturStatus(Long.valueOf(spanDataFailedLastStatus.getTotalRetur()));
												spanHostDataFailed.setSuccessStatus(Long.valueOf(spanDataFailedLastStatus.getTotalSuccess()));
												spanHostDataFailed.setProcStatus("4");
												spanHostDataFailed.setProcDescription("Force to Success");
												spanHostDataFailed.setTotalProcess(Long.valueOf("1"));
												spanHostDataFailed.setSumStatusRty(Long.valueOf(spanDataFailedLastStatus.getSumStatusRty()));
												spanHostDataFailed.setSumStatusRtu(Long.valueOf(spanDataFailedLastStatus.getSumStatusRtu()));
												spanHostDataFailed.setTrxType(trxType);
												spanHostDataFailed.setLegStatus("L1");
												spanHostDataFailed.setWaitStatus(Long.valueOf(spanDataFailedLastStatus.getTotalWait()));
												spanHostDataFailed.setSumStatusWat(Long.valueOf(spanDataFailedLastStatus.getSumStatusWat()));
													
												Odbc.insertSPANDataFailed(spanHostDataFailed);
												//c mandiri.span.db.adapter.odbc:insertSPANDataFailed	
												//o mandiri.span.db.service:updateSPAN
												Query query = dao.getQuery(sqlUpdate);
												mii.span.db.Service.updateSPAN(query);
												//c mandiri.span.db.service:updateSPAN
											}
											//c default.sequence
										//c branch
										//o map - set total amount 0
										totalAmount = "0";
										//c map	- set total amount 0
									}
									//c sequence
									//o default.sequence
									else{
										//o branch.on.waitStatus
											//o 3.map
											if("3".equalsIgnoreCase(waitStatus)){
												totalRecSendToHost = PubUtil.addInts(totalRecSendToHost, getTotalRecAmtSendToHostResponse.getTotalRecSendToHost());
												totalAmtSendToHost = PubUtil.addFloats(totalAmtSendToHost, getTotalRecAmtSendToHostResponse.getTotalAmtSendToHost(), "0");
											}
											//c 3.map
											//o default
											else{
												//o branch
													//o map
													if("3".equalsIgnoreCase(retryStatus) ||
															"3".equalsIgnoreCase(returStatus)){
														//do nothing
													}
													//c map
													//o default.map
													else{
														totalRecSendToHost = getTotalRecAmtSendToHostResponse.getTotalRecSendToHost();
														totalAmtSendToHost = getTotalRecAmtSendToHostResponse.getTotalAmtSendToHost();
													}
													//c default.map
												//c branch
											}
											//c default
										//c branch.on.waitStatus
										//o mandiri.span.db.adapter.odbc:updateTotalRecAmtSendToHost
										Odbc.updateTotalRecAmtSendToHost(totalRecSendToHost, totalAmtSendToHost, fileName);	
										//c mandiri.span.db.adapter.odbc:updateTotalRecAmtSendToHost	
									}
									//c default.sequence
								//c branch
							//c sequence
						}
						//c default
					//c branch.countRecord
				}
				//c default.sequence
			//c branch.totalAmount
		} catch (Exception e) {
			e.printStackTrace();
		}		
		//o sequence		
		//o map
		totalAmountOut = totalAmount;
		//c map
		return totalAmountOut;
	}
}
