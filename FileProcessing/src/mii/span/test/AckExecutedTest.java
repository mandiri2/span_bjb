package mii.span.test;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mii.span.scheduller.odbc.ACKLister;
import mii.span.scheduller.odbc.AckExecuted;
import mii.span.scheduller.odbc.ExtractSP2DFile;
import mii.span.scheduller.odbc.GenerateBankStatement;
import mii.span.scheduller.odbc.GetTransactionHistory;
import mii.span.scheduller.odbc.NewForceNACKScheduler;
import mii.span.scheduller.odbc.SpanDataValidationSP2DNoScheduller;
import mii.span.scheduller.odbc.ToBeExecute;
import mii.span.scheduller.odbc.ValidateDataDetails;

import com.mii.constant.Constants;
import com.mii.span.scheduller.GenerateSorborScheduller;
import com.mii.span.scheduller.ResultToBeProcessScheduller;

public class AckExecutedTest extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) {
//		AckExecuted.ackExecuted();
		
		//generate Bank Statement
//		GenerateBankStatement.go(Constants.GAJI);
		
		/*NewForceNACKScheduler newForceNACKScheduler = new NewForceNACKScheduler();
		newForceNACKScheduler.run();*/
		new ValidateDataDetails().runSpanDataValidation();
//		new ACKLister().autoSendAck();
//		new SpanDataValidationSP2DNoScheduller().run();
//		new ExtractSP2DFile().run();
//		new SpanDataValidationSP2DNoScheduller().run();
//		NewForceNACKScheduler newForceNACKScheduler = new NewForceNACKScheduler();
//		newForceNACKScheduler.run();
//		new ToBeExecute().run(Constants.GAJI);
//		new ACKLister().autoSendAck();
//		new GetTransactionHistory().run();
		
//		new AckExecuted().run();
		
//		SpanDataValidationSP2DNoScheduller.spanDataValidationSP2DNo();
	}
}
