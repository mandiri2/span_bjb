package mii.span.test;


import java.io.File;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

public class JAXBExample {
	public static void main(String[] args) {

	 try {

		File file = new File("D:\\file.xml");
		JAXBContext jaxbContext = JAXBContext.newInstance(Customer.class);

		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		Customer customer = (Customer) jaxbUnmarshaller.unmarshal(file);
		//Customer customer = (Customer) jaxbUnmarshaller.un;
/**
StringReader reader = new StringReader("xml string here");
Person person = (Person) unmarshaller.unmarshal(reader);
**/
		
		System.out.println(customer.getAge());
		System.out.println(customer.getId());
		System.out.println(customer.getName());

	  } catch (JAXBException e) {
		e.printStackTrace();
	  }

	}
}