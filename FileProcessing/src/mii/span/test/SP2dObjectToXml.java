package mii.span.test;

import java.io.File;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;

import mii.span.doc.SP2DDocument;
import mii.span.util.PubUtil;
import mii.span.util.SpanDocumentUtil;

public class SP2dObjectToXml {
	public static void main(String[] args) {
		SAXBuilder saxBuilder = new SAXBuilder();
		try {
			Document document = saxBuilder.build(new File("/sp2d.xml"));
			SP2DDocument sp2dDocument = SpanDocumentUtil.jDomToSpanDocument(document);
			
			JAXBContext jaxbContext = JAXBContext.newInstance(SP2DDocument.class);
			
			Marshaller marshaller = jaxbContext.createMarshaller();
			
			StringWriter sw = new StringWriter();
			marshaller.marshal(sp2dDocument, sw);
			System.out.println(sw.toString());
			
//			System.out.println(PubUtil.documentToXmlValues(SP2DDocument.class, sp2dDocument)); //test pake object
//			System.out.println(spandoc.getApplicationArea().getApplicationAreaMessageTypeIndicator());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
