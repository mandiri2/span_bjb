package mii.span.test;

import mii.span.process.model.SignXmlFileResponse;
import mii.span.util.Atomic;
import mii.span.util.PubUtil;
import mii.span.util.Signature;

public class FileGenerator {
	
	public static void main(String[] args) {
		//change_filename
		String fileName = "520008000990_SP2D_O_20161006_000001_SIT.xml";
		createFile(fileName);
	}
	
	public static void createFile(String fileName){
		String dirOutbound = "D:/sftp/createFile/";
		fileName = PubUtil.concat(dirOutbound, fileName);
		
		SignXmlFileResponse signXmlFileResponse = signXML(fileName, fileName);
		
		if("OK".equalsIgnoreCase(signXmlFileResponse.getStatus())){
			String fileJar = fileName.replace("xml", "jar");
			createJAR(fileName, fileJar);
			System.out.println("berhasil");
		}
		else{
			System.out.println("gagal");
		}
	}
	
	public static SignXmlFileResponse signXML(String filename, String signedFile){
//		String keystoreFile = "D:/keystore_sit.jks";
		String keystoreFile = "D:/backupFileSpan/keystore_sit.jks";
		String keystorePass = "lgcnsspan";
		String privateKeyUser = "span";
		String privateKeyPass = "span";
		
		SignXmlFileResponse signXmlFileResponse = Signature.signXMLFile(filename, keystoreFile, keystorePass, privateKeyUser, privateKeyPass, signedFile);
		return signXmlFileResponse;
		
	}
	
	public static  void createJAR(String fileInput, String fileOutput){
		Atomic.createJAR(fileInput, fileOutput);
	}
}
