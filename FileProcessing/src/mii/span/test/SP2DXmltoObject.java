package mii.span.test;

import java.io.File;
import java.io.StringReader;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import mii.span.doc.DataArea;
import mii.span.doc.SP2DDocument;

public class SP2DXmltoObject {
	public static void main(String[] args) {

		 try {

			File file = new File("D:\\SP2D.xml");
			JAXBContext jaxbContext = JAXBContext.newInstance(SP2DDocument.class);

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			SP2DDocument spandoc = (SP2DDocument) jaxbUnmarshaller.unmarshal(file);
			//Customer customer = (Customer) jaxbUnmarshaller.un;
	/**
	StringReader reader = new StringReader("xml string here");
	Person person = (Person) unmarshaller.unmarshal(reader);
	**/
			List<DataArea> dataAreaList = spandoc.getDataArea();
			System.out.println("size " + dataAreaList.size());
			int i = 0;
			for (Iterator<DataArea> iterator = dataAreaList.iterator(); iterator.hasNext();) {
				DataArea dataArea = (DataArea) iterator.next();
				
				System.out.println(i);
				System.out.println(dataArea.getAgentBankAccountName());
				System.out.println(dataArea.getAgentBankAccountNumber());
				System.out.println(dataArea.getAmount());
				System.out.println(dataArea.getBeneficiaryName());
				System.out.println("");
				i++;
				
			}
			System.out.println("test");
//			System.out.println(customer.getId());
//			System.out.println(customer.getName());

		  } catch (JAXBException e) {
			e.printStackTrace();
		  }catch (Exception e){
			  e.printStackTrace();
		  }

		}

	public static List<DataArea> ConvertSP2DXMLToObject(String SP2DXML){
		try{
			JAXBContext jaxbContext = JAXBContext.newInstance(SP2DDocument.class);
	
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			
			StringReader reader = new StringReader(SP2DXML);
			SP2DDocument spandoc = (SP2DDocument) jaxbUnmarshaller.unmarshal(reader);
			
			return spandoc.getDataArea();
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
}
