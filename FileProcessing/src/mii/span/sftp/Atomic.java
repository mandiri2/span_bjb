package mii.span.sftp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import mii.span.process.model.DeleteFileRequest;

import com.mii.helpers.FTPHelper;
import com.mii.source.SFTPClient;

public class Atomic {
	
	/**
	 * mandiri.span.sftp.atomic:getFiles
	 * @param spanPath
	 * @param localPath
	 * @param host
	 * @param username
	 * @param password
	 * @param totalFileNames
	 * @return
	 */
	public static String[] getFiles(String spanPath, String localPath, String host, String username, String password, int totalFileNames){
		String fileNames [] = null;
		try{
			fileNames = FTPHelper.getFileNames(host, username, password, spanPath, localPath, totalFileNames);
//			fileNames = SFTPClient.getFileNames(host, username, password, spanPath, localPath, totalFileNames);
			System.out.println("fileNames size : "+fileNames.length);
		}catch(Exception e){
			e.printStackTrace();
		}
		return fileNames;
	}
	
	/**
	 * Get Files From Local
	 */
	public static String[] getLocalFilesManual(String localPathManual){
		String fileNames [] = null;
		try{
			File folder = new File(localPathManual);
			File[] listOfFiles = folder.listFiles();
			for (int i = 0; i < listOfFiles.length; i++) {
		      if (listOfFiles[i].isFile()) {
		    	  fileNames = new String [listOfFiles.length];
		    	  fileNames[i] = listOfFiles[i].getName();
		    	  System.out.println("File " + listOfFiles[i].getName());
		      }
		    }
		}catch(Exception e){
			e.printStackTrace();
		}
		return fileNames;
	}
	
	/**
	 * mandiri.span.sftp.atomic:uploadFile
	 * 
	 */
	public static String uploadFile(String spanPath, String localPath, String fileName, String host, String username, String password){
		String Status = null;
		
		boolean status = true;
		
		try{
			status = FTPHelper.uploadFileFTP(host, username, password, spanPath, localPath, fileName);
//			status = SFTPClient.uploadFileSFTP(host, username, password, spanPath, localPath, fileName);
			System.out.println("File Sent.");
		}catch(Exception e){
			System.out.println("Failed to Send.");
			e.printStackTrace();
		}
		
		Status = String.valueOf(status);
		
		return Status;
	}
	
	/**
	 * mandiri.span.sftp.atomic:deleteFile
	 */
	public static String deleteFile(DeleteFileRequest request){
		
		String nameOfFileToDelete = request.getNameOfFileToDelete();
		String serverName = request.getServerName();
		String spanPath = request.getSpanPath();
		String username = request.getUsername();
		String password = request.getPassword();
		
		boolean status = true;
		
		try{
			status = FTPHelper.deleteFileFTP(nameOfFileToDelete, serverName, username, password, spanPath);
//			status = SFTPClient.deleteFileSFTP(nameOfFileToDelete, serverName, username, password, spanPath);
		
		}catch(Exception e){
			status = false;
			e.printStackTrace();
		}
		
		return String.valueOf(status);
	}
	
	/**
	 * Delete File Local
	 */
	public static String deleteFileLocalManual(String localPathManual, String fileName, String localPathManualBackup){
		//BACKUP
		try{
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
			InputStream inStream = null;
			OutputStream outStream = null;
			File afile =new File(localPathManual.concat(fileName));
    	    File bfile =new File(localPathManualBackup.concat(fileName.concat(sdf.format(new Date()))));

    	    inStream = new FileInputStream(afile);
    	    outStream = new FileOutputStream(bfile);

    	    byte[] buffer = new byte[1024];

    	    int length;
    	    //copy the file content in bytes
    	    while ((length = inStream.read(buffer)) > 0){

    	    	outStream.write(buffer, 0, length);

    	    }

    	    inStream.close();
    	    outStream.close();

    	    System.out.println("File is backup successful!");
		}catch(Exception e){
			System.out.println("Failed To Backup Manual Upload File.");
			e.printStackTrace();
		}
		//DELETE
		boolean status = true;
		File file = new File(localPathManual.concat(fileName));
		if(file.delete()){
			System.out.println(file.getName() + " Berhasil dihapus dari folder local upload manual.");
		}else{
			status = false;
		}
		return String.valueOf(status);
	}
}
