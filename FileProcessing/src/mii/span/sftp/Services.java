package mii.span.sftp;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedProperty;

import com.mii.dao.SystemParameterDAO;
import com.mii.models.SystemParameter;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.ApplicationInstance;

import mii.span.constant.MdrCfgMainConstant;
import mii.span.constant.SParameterConstant;
import mii.span.db.Service;
import mii.span.db.adapter.Odbc;
import mii.span.doc.FileSFTP;
import mii.span.process.model.CreateAndExtractJarResponse;
import mii.span.process.model.DataFiles;
import mii.span.util.BusinessUtil;
import mii.span.util.PubUtil;
import mii.span.util.ValidatingSignature;

public class Services {
	
	@ManagedProperty(value = "#{applicationBean}")
	private static ApplicationBean applicationBean;
	
	private static SystemParameterDAO getSystemParameterDaoBean(){
		applicationBean = ApplicationInstance.getInstance().getApplicationBean();
		return (SystemParameterDAO) applicationBean.getFileProcessContext().getBean("sysParamDAO");
	}
	
	/**
	 * mandiri.span.sftp.services:getFiles <br />
	 * RETURN LIST OF OBJECT <br />
	 * String Status <br />
	 * String ErrorMessage <br />
	 * Document List Of FileSFTP <br />
	 */

	static class Config {

		public String spanPath;
		public String filePattern;
		public String localPath;
		public String host;
		public String username;
		public String password;
		public int maxFilesNames;
		public String timeToGet;
		public String timeToGetParams2;
		public String timeToGetParams;
		public String localPathManual;
		public String localPathManualBackup;
	}

	public static DataFiles getFiles() {
		DataFiles response = new DataFiles();

		Config config = getConfig();
		
		List<String> spanFolderNames = BusinessUtil.getFolderName(config.spanPath);
		
		//TEST SYSOUT
		System.out.println("host : "+config.host);
		System.out.println("username : "+config.username);
		System.out.println("password : "+config.password);
		System.out.println("spanPath : "+config.spanPath);
		
		System.out.println("Check perbend directory");
		String[] fileNames = constructFileNames(config, spanFolderNames);
//		System.out.println("Length : " + fileNames.length);
		if(fileNames!=null){
			for (String fileName : fileNames) {
				// getFiles
				System.out.println("Lookup file in Depkeu SFTP");
				if (fileName == null) {
					continue;
				}else{
					response = processFiles(fileName, response, config.localPath, config.filePattern);
				}
				
			}
		}
		
		//MANUAL UPLOAD SP2D//
		System.out.println("Check manual upload");
		String [] fileNamesManual = Atomic.getLocalFilesManual(config.localPathManual);
		if(fileNamesManual!=null){
			for (String fileNameManual : fileNamesManual) {
				// getFiles
				System.out.println("Lookup file in Local Upload Dir");
				if (fileNameManual == null) {
					continue;
				}else{
					response = processFiles(fileNameManual, response, config.localPathManual, config.filePattern);
					System.out.println("Error Message : "+response.getErrorMessage());

					// deleteFile
					Atomic.deleteFileLocalManual(config.localPathManual, fileNameManual, config.localPathManualBackup);
				}
			}
		}

		return response;
	}
	
	private static DataFiles processFiles(String fileName, DataFiles response, String localPath, String filePattern){
		String Status = null;
		String ErrorMessage = null;
		try {
			System.out.println(PubUtil.concat("GET FILES FROM ",localPath, " ", fileName));

			boolean isXmlFile = fileName.indexOf(".xml") > 0;
			boolean isJarFile = fileName.indexOf(".jar") > 0;
			String fullFileName = PubUtil.concat(localPath, fileName);

			// branch
			if (isXmlFile || isJarFile) {

				// branch
				if (isXmlFile) {
					response.setStatus("SUCCESS");
				}
				if (isJarFile) { // extract jar file
					CreateAndExtractJarResponse createAndExtractJarResponse =  BusinessUtil.createAndExtractJar(fileName, localPath, "E");
					Status = createAndExtractJarResponse.getStatus();
					ErrorMessage = createAndExtractJarResponse.getErrorMessage();
				}
//				SystemParameter systemParameterFilePattern = (SystemParameter) getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_FILE_PATTERN);
				boolean isContainFilePattern = fileName.indexOf(filePattern) > 0;
				byte[] bytes = null;
				String fileNameOri = null;

				// branch on status
				if ("SUCCESS".equalsIgnoreCase(Status)) {

					fileNameOri = fileName;
					fileName = fileName.replace(".jar", ".xml");
					fullFileName = fullFileName.replace(".jar", ".xml");

					// branch
					if (isContainFilePattern) {
						String statusFN = Service.selectFNDataValidation(fileName);

						// branch statusFN
						if (!"SUCCESS".equalsIgnoreCase(statusFN)) {
							bytes = Files.readAllBytes(Paths.get(fullFileName));
							response.setStatus(validatingSignature(fileName));

							// branch on Status
							if (response.isValid()) {
								response.setStatus("SUCCESS");
								System.out.println("DIGITAL SIGNATURE SESUAI ");
							} else {
								response.setStatus("ERROR");
								response.setErrorMessage("A005|Data sukses diambil dari Intermediate server , sukses menghapus , sukses melakukan extract file, Tanggal creation sama dengan tanggal sistem namun Digital signature tidak sesuai ");
								System.out.println("A005|Data sukses diambil dari Intermediate server , sukses menghapus , sukses melakukan extract file, Tanggal creation sama dengan tanggal sistem namun Digital signature tidak sesuai ");
							}
						} else {
							response.setStatus("ERROR");
							response.setErrorMessage(PubUtil
									.concat("A009|Data has already exist in soa system : ",
											fileName));
						}

					} else {
						// do nothing
					}
					// deleteFile
					BusinessUtil.deleteFile(fullFileName);
				} else {
					// send ack with error message description, failed
					// extract file
					response.setErrorMessage("A003|Data sukses diambil dari Intermediate server , sukses menghapus  namun gagal melakukan extract file ");
					//Wipe XML//
					String fullFileNameFailExtract = fullFileName.replace(".jar", ".xml");
					BusinessUtil.deleteFile(fullFileNameFailExtract);
				}

				// collecting filename
				FileSFTP fromItem = constructFromItem(response, fileName, bytes, fileNameOri);

				response.getFiles().add(fromItem);
				response.setStatus("SUCCESS");

			} else {
				response.setStatus("ERROR");
				response.setErrorMessage("File format not match");
				// deleteFile
				BusinessUtil.deleteFile(fullFileName);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Error Message 2: "+response.getErrorMessage());
		return response;
	}
	
	private static FileSFTP constructFromItem(DataFiles response, String fileName, byte[] bytes,
			String fileNameOri) {
		FileSFTP fromItem = new FileSFTP();
		fromItem.setBytes(bytes);
		fromItem.setFileName(fileName);
		fromItem.setStatus(response.getStatus());
		fromItem.setErrorMessage(response.getErrorMessage());
		fromItem.setFileNameOri(fileNameOri);
		return fromItem;
	}
	
	/**
	 * validate signature dari span
	 * @param fileName
	 * @return
	 */
	private static String validatingSignature(String fileName) {
		String status;
//		String filePath = Odbc.selectMdrCfgMain(MdrCfgMainConstant.SPAN_DEPKEU_FILE_PATH).getValue1();
		SystemParameter systemParameterfilePath =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_FILE_PATH).get(0);
		String filePath = systemParameterfilePath.getParam_value();
		filePath = PubUtil.concat(filePath, fileName);
		SystemParameter systemParameterfilePathManual =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_MANUAL_PUT_LOCAL_DIR).get(0);
		String filePathManual = systemParameterfilePathManual.getParam_value();
		filePathManual = PubUtil.concat(filePathManual, fileName);
//		String ksPassword = Odbc.selectMdrCfgMain(MdrCfgMainConstant.SPAN_DEPKEU_KEYSTORE_PASSWORD).getValue1();
		SystemParameter systemParameterspanHostksPassword =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_KEYSTORE_PASSWORD).get(0);
		String ksPassword = systemParameterspanHostksPassword.getParam_value();
//		String ksPath = Odbc.selectMdrCfgMain(MdrCfgMainConstant.SPAN_DEPKEU_KEYSTORE_PATH).getValue1();
		SystemParameter systemParameterspanHostksPath =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_KEYSTORE_PATH).get(0);
		String ksPath = systemParameterspanHostksPath.getParam_value();
		
		status = ValidatingSignature.validatingSignature(filePath, ksPath, ksPassword);
		if("notfound".equals(status)){
			System.out.println("Lookup from manual upload dir.");
			status = ValidatingSignature.validatingSignatureManual(filePathManual, ksPath, ksPassword);
		}
		return status;
	}

	private static String[] constructFileNames(Config config,
			List<String> spanFolderNames) {
		List<String> newFileNames = new ArrayList<String>();

		for (String spanFolderName : spanFolderNames) {
			String[] fileNames = Atomic.getFiles(spanFolderName,
					config.localPath, config.host, config.username,
					config.password, config.maxFilesNames);
			for (String fileName : fileNames) {
				if(fileName!=null){
					newFileNames.add(fileName);
				}
			}
		}
		
		String[] fileNames = null;
		if(newFileNames.size()>0){
			fileNames = newFileNames.toArray(new String[0]);
		}
		return fileNames;
	}
	 
	private static Config getConfig() {
		Config config = new Config();
		SystemParameter systemParameterspanOutboundDir =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_GET_REMOTE_DIR).get(0);
		SystemParameter systemParameterspanfilePattern =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_FILE_PATTERN).get(0);
		SystemParameter systemParameterspanlocalPath =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_PUT_LOCAL_DIR).get(0);
		SystemParameter systemParameterspanmaxFilesNames =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_GET_MAX_FILENAMES).get(0);
		SystemParameter systemParameterspanHost =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_IPADDRESS).get(0);
		SystemParameter systemParameterspanUser =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_USERNAME).get(0);
		SystemParameter systemParameterspanPass =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_PASSWORD).get(0);
		SystemParameter systemParameterlocalPathManual =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_MANUAL_PUT_LOCAL_DIR).get(0);
		SystemParameter systemParameterlocalPathManualBackup =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_MANUAL_PUT_LOCAL_BACKUP_DIR).get(0);
		
		config.spanPath = systemParameterspanOutboundDir.getParam_value();
		config.filePattern = systemParameterspanfilePattern.getParam_value();
		config.filePattern = systemParameterspanfilePattern.getParam_value().replace("*", "");
		config.localPath = systemParameterspanlocalPath.getParam_value();
		config.host = systemParameterspanHost.getParam_value();
		config.username = systemParameterspanUser.getParam_value();
		config.password = systemParameterspanPass.getParam_value();
		config.maxFilesNames = Integer.parseInt(systemParameterspanmaxFilesNames.getParam_value());
		config.timeToGet = BusinessUtil.getDateByPattern("HHmmss");
		config.timeToGetParams2 = Odbc.selectSParameter(SParameterConstant._20130314P03).getValue();
		config.timeToGetParams = Odbc.selectSParameter(SParameterConstant._20130314P02).getValue();
		config.localPathManual = systemParameterlocalPathManual.getParam_value();
		config.localPathManualBackup = systemParameterlocalPathManualBackup.getParam_value();
		return config;
	}

	/**
	 * mandiri.span.sftp.services:uploadFile
	 */
	public static String uploadFile(String spanPath, String localPath,
			String fileName, String host, String username, String password) {
		String Status = null;

		Status = Atomic.uploadFile(spanPath, localPath, fileName, host,
				username, password);

		// branch on Status
		if ("true".equalsIgnoreCase(Status)) {
			Status = "SUCCESS";
		} else {
			// default
			Status = "Failed";
		}

		// debug log

		return Status;
	}

}
