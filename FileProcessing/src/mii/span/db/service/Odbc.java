package mii.span.db.service;

import java.util.Date;
import java.util.List;

import mii.span.db.Adapter;
import mii.span.model.Parameter;
import mii.span.model.SpanDataOriginalXML;
import mii.span.model.SpanDataValidation;
import mii.span.model.SpanGetFiles;
import mii.span.model.SpanHostDataDetails;
import mii.span.process.model.GetSCFirstDataResponse;
import mii.span.process.model.GetSPANDataFailedLastStatusResponse;
import mii.span.process.model.InsertSPANLogTrxUserResponse;
import mii.span.process.model.SelectSParameterResponse;
import mii.span.util.BusinessUtil;
import mii.span.util.PubUtil;

public class Odbc {
	
	/**
	 * mandiri.span.db.adapter.odbc:selectGetFileRetry
	 */
	public static String selectGetFileRetry(String filename){
		SpanGetFiles spanGetFiles = mii.span.db.adapter.Odbc.selectGetFileRetry(filename);
		if(spanGetFiles!=null 
				&& spanGetFiles.getTotalRetry()!=null){
			return spanGetFiles.getTotalRetry().toString();
		}
		return null;
	}
	
	/**
	 * //mandiri.span.db.service.odbc:insertGetFileRetry
	 */
	public static String insertGetFileRetry(String FILE_NAME, String TOTAL_RETRY, String DESCRIPTION, String CREATE_DATE, 
			String UPDATE_DATE){
		try {
			SpanGetFiles spanGetFiles = new SpanGetFiles();
			spanGetFiles.setFilename(FILE_NAME);
			spanGetFiles.setTotalRetry(Long.valueOf(TOTAL_RETRY));
			spanGetFiles.setDescription(DESCRIPTION);
			spanGetFiles.setCreateDate(new Date());
			mii.span.db.adapter.Odbc.insertGetFileRetry(spanGetFiles);
			return "OK";
		} catch (Exception e) {
			e.printStackTrace();
			return "ERROR";
		}
	}
	
	/**
	 * mandiri.span.db.service.odbc:updateGetFileRetry
	 */
	public static void updateGetFileRetry(String TOTAL_RETRY, String filename){
		mii.span.db.adapter.Odbc.updateGetFileRetry(TOTAL_RETRY, filename);
	}
	
	/**
	 * mandiri.span.db.service:spanSequenceNo
	 */
	public static String spanSequenceNo(String prefixSeq){
		
		//custom
		prefixSeq = PubUtil.emptyStringIfNull(prefixSeq);
		
		String spanSeqNo = mii.span.db.adapter.Odbc.getSequenceNo();
		spanSeqNo = PubUtil.padLeft(spanSeqNo, "0", 10);
		spanSeqNo = PubUtil.concat(prefixSeq, spanSeqNo);
		return spanSeqNo;
	}
	
	/**
	 * mandiri.span.db.service.odbc:selectSParameter <br />
	 * return String[] <br />
	 * String[0] value <br />
	 * String[1] description <br />
	 */
	public static SelectSParameterResponse selectSParameter(String paramID){
		SelectSParameterResponse response = new SelectSParameterResponse();
		
		Parameter sParameter = mii.span.db.adapter.Odbc.selectSParameter(paramID);
		
		response.setValue(sParameter.getValue());
		response.setDescription(sParameter.getValue());
		
		return response;
	}
	
	/**
	 * mandiri.span.db.service.odbc:getSPANDataFailedLastStatus
	 */
	public static GetSPANDataFailedLastStatusResponse getSPANDataFailedLastStatus(String iFileName){
		GetSPANDataFailedLastStatusResponse response = null;
		List<GetSPANDataFailedLastStatusResponse> results = mii.span.db.adapter.Odbc.getSPANDataFailedLastStatus(iFileName);
		
		if(!BusinessUtil.isListEmpty(results)){
			return results.get(0);
		}
		else{
			response = new GetSPANDataFailedLastStatusResponse();
			response.setTotalSuccess("0");
			response.setTotalRetur("0");
			response.setTotalRetry("0");
			response.setTotalWait("0");
			response.setSumStatusWat("0");
			response.setSumStatusRty("0");
			response.setSumStatusRtu("0");
			response.setTotalProcess("0");
			response.setLegstatus("L1");
			
			return response;
		}
	}
	
	/**
	 * mandiri.span.db.service.odbc:selectCountSPANUserLog
	 */
	public static String selectCountSPANUserLog(String filename){
		//o mandiri.span.db.adapter.odbc:selectCountSPANUserLog
		String countSPANUserLog = mii.span.db.adapter.Odbc.selectCountSPANUserLog(filename);
		//c mandiri.span.db.adapter.odbc:selectCountSPANUserLog
		
		return countSPANUserLog;
	}
	
	/**
	 * mandiri.span.db.service.odbc:getUserID
	 */
	public static String getUserID(String filename, String rowNumber){
			return mii.span.db.adapter.Odbc.getUserID(filename, rowNumber);
	}
	
	/**
	 * mandiri.span.db.service.odbc:insertSPANLogTrxUser
	 */
	public static InsertSPANLogTrxUserResponse insertSPANLogTrxUser(String iFILENAME, String iUSERID){
		String PROC_STATUS = null;
		InsertSPANLogTrxUserResponse insertSPANLogTrxUserResponse = null;
		//o mandiri.span.db.adapter.odbc:selectSPANDataValidation
		List<SpanDataValidation> spanDataValidations = mii.span.db.adapter.Odbc.selectSpanDataValidation(iFILENAME);
		SpanDataValidation spanDataValidation = new SpanDataValidation();
		if(spanDataValidations!=null){
			spanDataValidation = spanDataValidations.get(0);
			PROC_STATUS = spanDataValidation.getProcStatuc();
		}
		//c mandiri.span.db.adapter.odbc:selectSPANDataValidation
		//o branch
		if("4".equalsIgnoreCase(PROC_STATUS)
				|| "5".equalsIgnoreCase(PROC_STATUS)
				|| "7".equalsIgnoreCase(PROC_STATUS)){
			//o mandiri.span.db.adapter.odbc:insertSPANLogTrxUser
			insertSPANLogTrxUserResponse = mii.span.db.adapter.Odbc.insertSPANLogTrxUser(iFILENAME, iUSERID);
			//c mandiri.span.db.adapter.odbc:insertSPANLogTrxUser
		}
		else{
			//do nothing
		}
		//c branch
		
		return insertSPANLogTrxUserResponse;
	}
	
	/**
	 * mandiri.span.db.service.odbc:insertSPANHostDetailsBatch
	 */
	public static void insertSPANHostDetailsBatch(List<SpanHostDataDetails> SPANHostBatch){
		mii.span.db.adapter.Odbc.insertSPANHostDetailsBatch(SPANHostBatch);
	}
	
	/**
	 * mandiri.span.db.service.odbc:insertSPANDataOriginalXML
	 */
	public static String insertSPANDataOriginalXML(SpanDataOriginalXML spanOriData){
		return Adapter.insertSPANDataOriginalXML(spanOriData);
	}
}
