package mii.span.db.service;

import java.util.List;

import mii.span.model.SpanHostDataFailed;
import mii.span.process.model.GetSCFirstDataResponse;

public class SC {
	/**
	 * mandiri.span.db.service.sc:getSCFirstData
	 */
	public static GetSCFirstDataResponse getSCFirstData(String filename, String totalProcess, String legStatus){
		GetSCFirstDataResponse response = new GetSCFirstDataResponse();
		//o mandiri.span.db.adapter.sc:getSCFirstData
		List<SpanHostDataFailed> spanHostDataFaileds = mii.span.db.adapter.SC.getSCFirstData(filename, totalProcess, legStatus);
		SpanHostDataFailed spanHostDataFailed = new SpanHostDataFailed();
		if(spanHostDataFaileds!=null){
			spanHostDataFailed = spanHostDataFaileds.get(0);
		}
		response.setBATCHID(spanHostDataFailed.getBatchId());
		response.setTRXHEADERID(spanHostDataFailed.getTrxHeaderId());
		response.setRETRY_STATUS(spanHostDataFailed.getRetryStatus().toString());
		response.setRETUR_STATUS(spanHostDataFailed.getReturStatus().toString());
		response.setSUCCESS_STATUS(spanHostDataFailed.getSuccessStatus().toString());
		response.setCREATE_DATE(spanHostDataFailed.getCreateDate().toString());
		response.setSUM_STATUS_RTY(spanHostDataFailed.getSumStatusRty().toString());
		response.setSUM_STATUS_RTU(spanHostDataFailed.getSumStatusRtu().toString());
		response.setTRXTYPE(spanHostDataFailed.getTrxType());
		//c mandiri.span.db.adapter.sc:getSCFirstData
		return response;
	}
}
