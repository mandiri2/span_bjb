package mii.span.db;

import java.util.List;

import org.hibernate.Query;

import com.mii.constant.DaoConstant;
import com.mii.dao.ParameterDao;
import com.mii.dao.ProviderDao;
import com.mii.dao.SpanDataOriginalXMLDao;
import com.mii.dao.SpanDataValidationDao;
import com.mii.dao.SpanDocumentNoDetailListDao;
import com.mii.dao.SpanHostDataDetailsDao;
import com.mii.dao.SpanHostResponseCodeDao;
import com.mii.dao.SpanRecreateFileRejectedDao;
import com.mii.dao.SpanVoidDataTrxDao;
import com.mii.helpers.BasicDaoImplement;
import com.mii.helpers.InterfaceDAO;

import mii.span.model.ClientParams;
import mii.span.model.Provider;
import mii.span.model.SpanDataOriginalXML;
import mii.span.model.SpanDataValidation;
import mii.span.model.SpanDocumentNoDetailList;
import mii.span.model.SpanHostDataDetails;
import mii.span.model.SpanHostResponseCode;
import mii.span.model.SpanRecreateFileRejected;
import mii.span.model.SpanVoidDataTrx;
import mii.span.util.BusinessUtil;
import mii.span.process.model.UpdateSpanResponse;


public class Adapter  extends BasicDaoImplement{
	
	/**
	 * mandiri.span.db.adapter:getProviderInfo
	 */
	public static List<Provider> getProviderInfo(String PROVIDER_CODE_ALIAS){
		ProviderDao dao = (ProviderDao) BusinessUtil.getDao(DaoConstant.ProviderDao);
		return dao.getByProviderCodeAlias(PROVIDER_CODE_ALIAS);
	}
	
	/**
	 * mandiri.span.db.adapter:insertSPANDocumentNOListBatch
	 */
	public static void insertSPANDocumentNOListBatch(List<SpanDocumentNoDetailList> spanDocumentNoDetailLists){
		if(spanDocumentNoDetailLists!=null){
			SpanDocumentNoDetailListDao dao = (SpanDocumentNoDetailListDao)BusinessUtil.getDao(DaoConstant.SpanDocumentNoDetailListDao);
			for(SpanDocumentNoDetailList spanDocumentNoDetailList : spanDocumentNoDetailLists){
				dao.saveSpanDocumentNoDetailList(spanDocumentNoDetailList);
			}
		}
	}
	
	/**
	 * mandiri.span.db.adapter:selectClientParameters
	 */
	public static ClientParams selectClientParameters(String clientKey) {
		/*
		select client_id, bank_code, skn_code, city_code, 
		rtgs_code, swift_code, bic_key, bank_name, bic_skn
		from c_client_parameters where client_key = '${clientKey}'
		*/
		ClientParams clientParam = new ClientParams();
		return clientParam;
	}
	
	/**
	 * mandiri.span.db.adapter.odbc:insertSPANHostDetailsBatch
	 * @param insertSPANHostDetailsListAS1
	 */
	public static void insertSPANHostDetailsBatch(List<SpanHostDataDetails> insertSPANHostDetailsListAS1) {
		SpanHostDataDetailsDao dao = (SpanHostDataDetailsDao) BusinessUtil.getDao(DaoConstant.SpanHostDataDetailsDao);
		if(insertSPANHostDetailsListAS1 != null){
			for(SpanHostDataDetails spanHostDataDetails : insertSPANHostDetailsListAS1){
				dao.saveSpanHostDataDetails(spanHostDataDetails);
			}
		}
	}

	public static void udpateChangeStatus(String batchID, String statusChange) {
		
		String hql = "UPDATE SpanDataValidation SET changeStatus = :statusChange WHERE batchId = :batchID";
		
	}

	/**
	 * mandiri.span.db.adapter:selectExtractFileSPAN
	 * @param procStatus
	 * @param rowNum
	 * @return
	 */
	public static List<SpanDataValidation> selectExtractFileSPAN(String procStatus, String rowNum) {
		SpanDataValidationDao dao = (SpanDataValidationDao)BusinessUtil.getDao(DaoConstant.SpanDataValidationDao);
		return dao.selectExtractFileSPAN(procStatus, rowNum);
	}

	/**
	 * mandiri.span.db.adapter:selectSPANFileRejected
	 */
	public static List<SpanRecreateFileRejected> selectSpanFileRejected(String newFilename){
		SpanRecreateFileRejectedDao dao = (SpanRecreateFileRejectedDao) BusinessUtil.getDao(DaoConstant.SpanRecreateFileRejectedDao);
		return dao.getByNewFileName(newFilename);
	}
	
	/**
	 * mandiri.span.db.adapter:selectSPANVoidDataTransaction
	 */
	public static List<SpanVoidDataTrx> selectSpanVoidDataTransaction(String fileName, String status){
		SpanVoidDataTrxDao dao = (SpanVoidDataTrxDao)BusinessUtil.getDao(DaoConstant.SpanVoidDataTrxDao);
		return dao.getByFileNameAndStatus(fileName, status);
	}
	
	/**
	 * mandiri.span.db.adapter:selectSPANDataMvaError
	 */
	public static List<SpanHostDataDetails> selectSPANDataMvaError(String BATCHID){
		//TODO
		return null;
	}
	
	/**
	 * mandiri.span.db.adapter:selectSPANHostDetails
	 */
	public static List<SpanHostDataDetails> selectSPANHostDetails(String batchId, String trxHeaderId){
		SpanHostDataDetailsDao dao = (SpanHostDataDetailsDao) BusinessUtil.getDao(DaoConstant.SpanHostDataDetailsDao);
		return dao.selectSPANHostDetails(batchId, trxHeaderId);
	}
	
	/**
	 * mandiri.span.db.adapter:selectOriginalFileName
	 * @param oriFN
	 * @return
	 */
	public static String selectOriginalFileName(String oriFN) {
		/*
		 * select original_file_name from span_recreate_file_rejected where new_file_name = '${fileName}'
		 */
		SpanRecreateFileRejectedDao dao = (SpanRecreateFileRejectedDao) BusinessUtil.getDao(DaoConstant.SpanRecreateFileRejectedDao);
		List<SpanRecreateFileRejected> list = dao.getByNewFileName(oriFN);
		if(!BusinessUtil.isListEmpty(list)){
			return list.get(0).getOriginalFileName();
		}
		return null;
	}
	
	/**
	 * mandiri.span.db.adapter:getReturCode
	 */
	public static List<SpanHostResponseCode> getReturCode(String rcId){
		SpanHostResponseCodeDao dao = (SpanHostResponseCodeDao) BusinessUtil.getDao(DaoConstant.SpanHostResponseCodeDao);
		return dao.getByRcId(rcId);
	}
	
	/**
	 * mandiri.span.db.adapter:updateChangeStatus
	 */
	public static void updateChangeStatus(String batchId, String changeStatus){
		SpanDataValidationDao dao = (SpanDataValidationDao) BusinessUtil.getDao(DaoConstant.SpanDataValidationDao);
		dao.udpateChangeStatus(batchId, changeStatus);
	}
	
	/**
	 * mandiri.span.db.adapter:insertSPANDataOriginalXML
	 */
	public static String insertSPANDataOriginalXML(SpanDataOriginalXML spanOriData){
		try {
			SpanDataOriginalXMLDao dao = (SpanDataOriginalXMLDao) BusinessUtil.getDao(DaoConstant.SpanDataOriginalXMLDao);
			return "SUCCESS";
		} catch (Exception e) {
			e.printStackTrace();
			return "ERROR";
		}
	}
}
