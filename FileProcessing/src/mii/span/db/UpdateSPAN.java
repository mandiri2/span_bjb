package mii.span.db;

import mii.span.process.model.UpdateSpanResponse;

import org.hibernate.Query;

import com.mii.helpers.InterfaceDAO;

public interface UpdateSPAN extends InterfaceDAO{

	public UpdateSpanResponse updateSPAN(Query query);
}
