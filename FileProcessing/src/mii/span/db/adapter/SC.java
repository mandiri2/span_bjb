package mii.span.db.adapter;

import java.util.List;

import com.mii.constant.DaoConstant;
import com.mii.dao.SpanHostDataFailedDao;

import mii.span.model.SpanHostDataFailed;
import mii.span.util.BusinessUtil;

public class SC {
	/**
	 * mandiri.span.db.adapter.sc:getSCFirstData
	 */
	public static List<SpanHostDataFailed> getSCFirstData(String fileName, String totalProccess, String legStatus){
		SpanHostDataFailedDao dao = (SpanHostDataFailedDao) BusinessUtil.getDao(DaoConstant.SpanHostDataFailedDao);
		return dao.getSCFirstData(fileName, totalProccess, legStatus);
	}
}
