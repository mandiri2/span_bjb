package mii.span.db.adapter;

import java.util.Date;
import java.util.List;

import com.mii.constant.DaoConstant;
import com.mii.dao.MdrCfgMainDao;
import com.mii.dao.ParameterDao;
import com.mii.dao.ProviderDao;
import com.mii.dao.SpanDataValidationDao;
import com.mii.dao.SpanDataValidationSP2DNoDao;
import com.mii.dao.SpanGetFilesDao;
import com.mii.dao.SpanHostDataDetailsDao;
import com.mii.dao.SpanHostDataFailedDao;
import com.mii.dao.SpanHostResponseDao;
import com.mii.dao.SpanLogTrxUserDao;
import com.mii.dao.SpanRecreateFileRejectedDao;
import com.mii.dao.SpanUserLogDao;
import com.mii.dao.SpanUserLogDaoImpl;
import com.mii.dao.SystemParameterDAO;

import mii.span.doc.MdrCfgMain;
import mii.span.model.Parameter;
import mii.span.model.Provider;
import mii.span.model.SpanDataValidation;
import mii.span.model.SpanDataValidationSP2DNo;
import mii.span.model.SpanGetFiles;
import mii.span.model.SpanHostDataDetails;
import mii.span.model.SpanHostDataFailed;
import mii.span.model.SpanHostResponse;
import mii.span.model.SpanRecreateFileRejected;
import mii.span.model.SpanRejectedFileHis;
import mii.span.process.model.ACKExecutedDoc;
import mii.span.process.model.GetExecutedTransactionSummaryResponse;
import mii.span.process.model.GetSPANDataFailedLastStatusResponse;
import mii.span.process.model.GetTotalRecAmtSendToHostResponse;
import mii.span.process.model.GetWaitTransactionSummaryResponse;
import mii.span.process.model.InsertSPANLogTrxUserResponse;
import mii.span.process.model.SpanHostRequestResponse;
import mii.span.util.BusinessUtil;

public class Odbc {
	
	public static Parameter selectSParameter(String paramId){
		ParameterDao parameterDao = (ParameterDao) BusinessUtil.getDao("parameterDao");
		return (Parameter)parameterDao.get(Parameter.class, paramId);
	}
	
	public static MdrCfgMain selectMdrCfgMain(String key){
		MdrCfgMainDao mdrCfgMainDao = (MdrCfgMainDao) BusinessUtil.getDao("mdrCfgMainDao");
		return (MdrCfgMain)mdrCfgMainDao.get(MdrCfgMain.class, key);
	}
	
	/**
	 * return array of string <br />
	 * [0]bathcId <br />
	 * [1]procStatus <br />
	 * [2]trxType <br />
	 * [3]documentDate <br />
	 * [4]execFileFlag <br />
	 */
	public static List<SpanDataValidation> selectFNDataValidation(String fileName){
		SpanDataValidationDao dao = (SpanDataValidationDao) BusinessUtil.getDao(DaoConstant.SpanDataValidationDao);
		return (List<SpanDataValidation>) dao.getByFileName(fileName);
	}
	
	/**
	 * mandiri.span.db.adapter.odbc:selectSPANDataValidationStatus
	 */
	public static List<SpanDataValidation> selectSPANDataValidationStatus(String procStatus1, String procStatus2){
		SpanDataValidationDao dao = (SpanDataValidationDao) BusinessUtil.getDao(DaoConstant.SpanDataValidationDao);
		return dao.selectSPANDataValidationStatus(procStatus1, procStatus2);
	}
	
	/**
	 * mandiri.span.db.service:selectCountSPANFileRejected
	 */
	public static int selectCountSPANFileRejected(String fileName){
		SpanDataValidationDao dao = (SpanDataValidationDao) BusinessUtil.getDao(DaoConstant.SpanDataValidationDao);
		return dao.selectCountSPANFileRejected(fileName);
	}
			
	/**
	 * mandiri.span.db.adapter.odbc:insertGetFileRetry
	 */
	public static void insertGetFiles(){
		//TODO buat implementnya
	}
	
	/**
	 * mandiri.span.db.adapter.odbc:updateGetFileRetry
	 */
	public static void updateGetFileRetry(String totalRetry, String filename){
		SpanGetFilesDao dao = (SpanGetFilesDao) BusinessUtil.getDao(DaoConstant.SpanGetFilesDao);
		dao.updateSpanGetFiles(totalRetry, filename);
	}
	
	/**
	 * mandiri.span.db.adapter.odbc:getBatchIDSeq
	 */
	public static String getBatchIDSeq(){
		SpanDataValidationDao dao = (SpanDataValidationDao) BusinessUtil.getDao("spanDataValidationDao");
		return dao.getBatchIDSeq().toString();
		
	}
	
	/**
	 * mandiri.span.db.adapter:getSequenceNo
	 */
	public static String getSequenceNo(){
		SpanDataValidationDao dao = (SpanDataValidationDao) BusinessUtil.getDao("spanDataValidationDao");
		return dao.spanSequenceNo().toString();
	}
	
	/**
	 * mandiri.span.db.adapter.odbc:getProviderInfoDoc
	 */
	public static Provider getProviderInfoDoc(String accountNo){
		ProviderDao dao = (ProviderDao) BusinessUtil.getDao(DaoConstant.ProviderDao);
		List<Provider> providers = dao.getByProviderAccount(accountNo);
		Provider tProviders = new Provider();
		if(!BusinessUtil.isListEmpty(providers)){
			tProviders = providers.get(0);
		}
		return tProviders;
	}
	
	/**
	 * mandiri.span.db.adapter.odbc:selectSParamFTP
	 */
	public static String selectSParamFTP(String iPARAM_ID){
		ParameterDao dao = (ParameterDao) BusinessUtil.getDao("parameterDao");
		Parameter parameter = (Parameter)dao.get(Parameter.class, iPARAM_ID);
		return parameter.getValue();
	}
	
	/**
	 * mandiri.span.db.adapter.odbc:insertSPANDataValidation
	 */
	public static void insertSPANDataValidation(SpanDataValidation spanDataValidation){
		SpanDataValidationDao dao = (SpanDataValidationDao) BusinessUtil.getDao(DaoConstant.SpanDataValidationDao);
		dao.saveSpanDataValidation(spanDataValidation);
	}
	
	public static void insertSpanRecreateRejectedFile(SpanRecreateFileRejected rejectedFile){
		SpanRecreateFileRejectedDao dao = (SpanRecreateFileRejectedDao) BusinessUtil
				.getDao(DaoConstant.SpanRecreateRejectedFile);
		dao.saveSpanRecreateFileRejected(rejectedFile);
	}
	
	public static void insertSpanRejectedFileHis(SpanRejectedFileHis fileHis){
		SpanRecreateFileRejectedDao dao = (SpanRecreateFileRejectedDao) BusinessUtil
				.getDao(DaoConstant.SpanRecreateRejectedFile);
		dao.saveSpanRejectedFileHis(fileHis);
	}
	
	public static List<SpanDataValidation> selectSPANDataValidationStatus(String[] procStatus) {
		// TODO Auto-generated method stub
		// TODO : select * from span_data_validation where proc_status = procStatus[0] or  proc_status = procStatus[1]
		return null;
	}

	/**
	 * mandiri.span.db.adapter:insertSPANDataValidationSP2DNO
	 */
	public static void insertSPANDataValidationSP2DNO(List<SpanDataValidationSP2DNo> outputList) {
		SpanDataValidationSP2DNoDao dao = (SpanDataValidationSP2DNoDao) BusinessUtil.getDao(DaoConstant.SpanDataValidationSP2DNoDao);
		dao.insertBatch(outputList);
	}
	
	/**
	 * mandiri.span.db.adapter.odbc:UpdateFTPStatus
	 */
	public static void UpdateFTPStatus(String ftpStatus, String fileName, String batchId, String trxHeaderid){
		SpanDataValidationDao spanDataValidationDao = (SpanDataValidationDao) BusinessUtil.getDao(DaoConstant.SpanDataValidationDao);
		SpanDataValidation spanDataValidation = spanDataValidationDao.getByCompositeId(fileName, batchId, trxHeaderid);
		spanDataValidation.setFtpStatus(ftpStatus);
		spanDataValidationDao.update(spanDataValidation);
	}
	
	/**
	 * mandiri.span.db.adapter.odbc:selectSPANDataExecuted
	 */
	public static List<SpanDataValidation> selectSPANDataExecuted(String procStatus){
		SpanDataValidationDao spanDataValidationDao = (SpanDataValidationDao) BusinessUtil.getDao(DaoConstant.SpanDataValidationDao);
		return spanDataValidationDao.selectSPANDataExecuted(procStatus);
	}
	
	/**
	 * mandiri.span.db.adapter.odbc:selectSPANDataValidation
	 */
	public static List<SpanDataValidation> selectSpanDataValidation(String fileName){
		SpanDataValidationDao dao = (SpanDataValidationDao) BusinessUtil.getDao(DaoConstant.SpanDataValidationDao);
		return dao.likeByFileName(fileName);
	}
	
	/**
	 * mandiri.span.db.adapter.odbc:updateGajiDataValidation
	 */
	public static void updateGajiDataValidation(String xmlFileName, String xmlVoidFileName, String totalAmount, String totalRecord, String procStatus,
			String procDescription, String fileName){
		
		SpanDataValidationDao dao = (SpanDataValidationDao) BusinessUtil.getDao(DaoConstant.SpanDataValidationDao);
		List<SpanDataValidation> spanDataValidations = dao.getByEqualFileName(fileName);
		if(!BusinessUtil.isListEmpty(spanDataValidations)){
			for(SpanDataValidation spanDataValidation : spanDataValidations){
				spanDataValidation = spanDataValidations.get(0);
				spanDataValidation.setXmlFileName(xmlFileName);
				spanDataValidation.setXmlVoidFileName(xmlVoidFileName);
				spanDataValidation.setTotalAmount(totalAmount);
				spanDataValidation.setTotalRecord(totalRecord);
				spanDataValidation.setProcStatuc(procStatus);
				spanDataValidation.setProcDescription(procDescription);
				spanDataValidation.setUpdateDate(new Date());
				dao.update(spanDataValidation);
			}
		}
	}
	
	/**
	 * mandiri.span.db.adapter.odbc:selectSPANDataValidationStatusByTrxType
	 */
	public static List<SpanDataValidation> selectSPANDataValidationStatusByTrxType(String procStatus, String trxType1, String trxType2){
		SpanDataValidationDao dao = (SpanDataValidationDao) BusinessUtil.getDao(DaoConstant.SpanDataValidationDao);
		return dao.selectSPANDataValidationStatusByTrxType(procStatus, trxType1, trxType2);
	}
	
	/**
	 * mandiri.span.db.adapter.odbc:insertSPANHostResponse
	 */
	public static void insertSPANHostResponse(SpanHostResponse spanResponse){
		SpanHostResponseDao dao = (SpanHostResponseDao) BusinessUtil.getDao(DaoConstant.SpanHostResponseDao);
		dao.saveSpanHostResponse(spanResponse);
	}
	
	/**
	 * mandiri.span.db.adapter.odbc:getExecutedTransactionSummary
	 */
	public static GetExecutedTransactionSummaryResponse getExecutedTransactionSummary(String batchId, String trxHeaderId){
		SpanHostDataDetailsDao dao = (SpanHostDataDetailsDao) BusinessUtil.getDao(DaoConstant.SpanHostDataDetailsDao);
		return dao.getExecutedTransactionSummary(batchId, trxHeaderId);
	}
	
	/**
	 * mandiri.span.db.adapter.odbc:getSPANDataFailedLastStatus
	 */
	public static List<GetSPANDataFailedLastStatusResponse> getSPANDataFailedLastStatus(String iFileName){
		SpanHostDataFailedDao dao = (SpanHostDataFailedDao) BusinessUtil.getDao(DaoConstant.SpanHostDataFailedDao);
		return dao.getSPANDataFailedLastStatus(iFileName);
	}
	
	/**
	 * mandiri.span.db.adapter.odbc:getWaitTransactionSummary
	 */
	public static GetWaitTransactionSummaryResponse getWaitTransactionSummary(String batchId, String trxHeaderId){
		SpanHostDataDetailsDao dao = (SpanHostDataDetailsDao) BusinessUtil.getDao(DaoConstant.SpanHostDataDetailsDao);
		return dao.getWaitTransactionSummary(batchId, trxHeaderId);
	}
	
	/**
	 * mandiri.span.db.adapter.odbc:insertSPANDataFailed
	 */
	public static void insertSPANDataFailed(SpanHostDataFailed spanHostDataFailed){
		SpanHostDataFailedDao dao = (SpanHostDataFailedDao) BusinessUtil.getDao(DaoConstant.SpanHostDataFailedDao);
		dao.saveSpanHostDataFailed(spanHostDataFailed);
	}
	
	/**
	 * mandiri.span.db.adapter.odbc:getTotalRecAmtSendToHost
	 */
	public static GetTotalRecAmtSendToHostResponse getTotalRecAmtSendToHost(String batchId, String trxHeaderId){
		SpanHostDataDetailsDao dao = (SpanHostDataDetailsDao) BusinessUtil.getDao(DaoConstant.SpanHostDataDetailsDao);
		return dao.getTotalRecAmtSendToHost(batchId, trxHeaderId);
	}
	
	/**
	 * mandiri.span.db.adapter.odbc:updateTotalRecAmtSendToHost
	 */
	public static void updateTotalRecAmtSendToHost(String totalSendToHost, String totalAmountSendToHost, String fileName){
		SpanDataValidationDao dao = (SpanDataValidationDao) BusinessUtil.getDao(DaoConstant.SpanDataValidationDao);
		dao.updateTotalRecAmtSendToHost(totalSendToHost, totalAmountSendToHost, fileName);
	}
	
	/**
	 * mandiri.span.db.adapter.odbc:selectCountSPANUserLog
	 */
	public static String selectCountSPANUserLog(String fileName){
		SpanUserLogDao dao = (SpanUserLogDao) BusinessUtil.getDao(DaoConstant.SpanUserLogDao);
		return dao.countSPANUserLog(fileName).toString();
	}
	
	/**
	 * mandiri.span.db.adapter.odbc:getUserID
	 */
	public static String getUserID(String fileName, String rowNumber){
		SpanUserLogDao dao = (SpanUserLogDao) BusinessUtil.getDao(DaoConstant.SpanUserLogDao);
		return dao.getUserID(fileName, rowNumber);
	}
	
	/**
	 * mandiri.span.db.adapter.odbc:insertSPANLogTrxUser
	 */
	public static InsertSPANLogTrxUserResponse insertSPANLogTrxUser(String iFileName, String iUserId){
		SpanLogTrxUserDao dao = (SpanLogTrxUserDao) BusinessUtil.getDao(DaoConstant.SpanLogTrxUserDao);
		return dao.insertSPANLogTrxUser(iFileName, iUserId);
	}
	
	/**
	 * mandiri.span.db.adapter.odbc:selectSPANHostResponse
	 */
	public static List<ACKExecutedDoc> selectSPANHostResponse(String filename){
		/**
		* 	select a.errorcode, a.errormessage, b.doc_date, b.doc_number, b.msg_type_id
			from span_host_response a, span_host_data_details b 
			where (a.response_flag=1 OR a.response_flag=2)
			and A.reserve2 is null
			and a.batchid=b.batchid and a.trxdetailid=b.trxdetailid
			and a.batchid in (select batchid from span_host_data_failed where file_name = '${filename}')
		* 
		*/
		SpanHostDataDetailsDao dao = (SpanHostDataDetailsDao) BusinessUtil.getDao(DaoConstant.SpanHostDataDetailsDao);
		return dao.selectSPANHostResponse(filename);
	}
	
	/**
	 * mandiri.span.db.adapter.odbc:insertSPANHostDetailsBatch
	 */
	public static void insertSPANHostDetailsBatch(List<SpanHostDataDetails> insertSPANHostDetailsBatchInput){
		SpanHostDataDetailsDao dao = (SpanHostDataDetailsDao) BusinessUtil.getDao(DaoConstant.SpanHostDataDetailsDao);
		if(!BusinessUtil.isListEmpty(insertSPANHostDetailsBatchInput)){
			for(SpanHostDataDetails spanHostDataDetails : insertSPANHostDetailsBatchInput){
				dao.saveSpanHostDataDetails(spanHostDataDetails);
			}
		}
	}
	
	/**
	 * mandiri.span.db.adapter.odbc:spanHostRequest
	 */
	public static SpanHostRequestResponse spanHostRequest(String iBatchId, String iTrxHeaderId, String iStatus, String iPriority, String iChannelId, 
			String iFilename, String iDebitAccountSc, String iSettlement){
		SpanHostDataDetailsDao dao = (SpanHostDataDetailsDao) BusinessUtil.getDao(DaoConstant.SpanHostDataDetailsDao);
		return dao.spanHostRequest(iBatchId, iTrxHeaderId, iStatus, iPriority, iChannelId, iFilename, iDebitAccountSc, iSettlement);
	}
	
	/**
	 * mandiri.span.db.adapter.odbc:selectSpanDataValidationSP2DNoByFilename
	 */
	public static List<SpanDataValidationSP2DNo> selectSpanDataValidationSP2DNoByFilename(String iFilename){
		SpanDataValidationSP2DNoDao dao = (SpanDataValidationSP2DNoDao) BusinessUtil.getDao(DaoConstant.SpanDataValidationSP2DNoDao);
		return dao.getByFilename(iFilename);
	}
	
	/**
	 * mandiri.span.db.adapter.odbc:selectGetFileRetry
	 */
	public static SpanGetFiles selectGetFileRetry(String filename){
		SpanGetFilesDao dao = (SpanGetFilesDao) BusinessUtil.getDao(DaoConstant.SpanGetFilesDao);
		return (SpanGetFiles) dao.get(SpanGetFiles.class, filename);
	}
	
	/**
	 * mandiri.span.db.service.odbc:insertGetFileRetry
	 */
	public static void insertGetFileRetry(SpanGetFiles spanGetFiles){
		SpanGetFilesDao dao = (SpanGetFilesDao) BusinessUtil.getDao(DaoConstant.SpanGetFilesDao);
		dao.save(spanGetFiles);
	}
	
	public static String selectSystemParameterByParamName(String paramName){
		SystemParameterDAO dao = (SystemParameterDAO) BusinessUtil.getDao(DaoConstant.SystemParameterDAO);
		return dao.getValueByParamName(paramName);
	}
}
