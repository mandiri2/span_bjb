package mii.span.db;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedProperty;

import org.hibernate.Query;

import com.mii.constant.DaoConstant;
import com.mii.dao.ProviderDao;
import com.mii.dao.SpanDataValidationDao;
import com.mii.dao.SystemParameterDAO;
import com.mii.helpers.InterfaceDAO;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.ApplicationInstance;

import mii.span.constant.SParameterConstant;
import mii.span.db.adapter.Odbc;
import mii.span.doc.DataArea;
import mii.span.doc.DataFileNACK;
import mii.span.doc.SP2DDocument;
import mii.span.model.ClientParams;
import mii.span.model.Parameter;
import mii.span.model.Provider;
import mii.span.model.SpanDataOriginalXML;
import mii.span.model.SpanDataValidation;
import mii.span.model.SpanDataValidationSP2DNo;
import mii.span.model.SpanDocumentNoDetailList;
import mii.span.model.SpanHostDataDetails;
import mii.span.model.SpanHostResponseCode;
import mii.span.model.SpanRecreateFileRejected;
import mii.span.model.SpanRejectedFileHis;
import mii.span.model.SpanVoidDataTrx;
import mii.span.process.model.ACKExecutedDoc;
import mii.span.process.model.DocumentNo;
import mii.span.process.model.FtpParam;
import mii.span.process.model.InsertSPANDataValidationResponse;
import mii.span.process.model.InsertSPANDocumentNoListResponse;
import mii.span.process.model.ProviderInfo;
import mii.span.process.model.SelectSpanDataValidationResponse;
import mii.span.process.model.UpdateForFileRejectedResponse;
import mii.span.process.model.UpdateSpanResponse;
import mii.span.util.BusinessUtil;
import mii.span.util.PubUtil;
import mii.span.util.SpanDocumentUtil;

public class Service implements Serializable{
	private static final long serialVersionUID = 1L;
	@ManagedProperty(value = "#{applicationBean}")
	private static ApplicationBean applicationBean;
	
	private static UpdateSPAN getAdapterBean(){
		applicationBean = ApplicationInstance.getInstance().getApplicationBean();
		return (UpdateSPAN) applicationBean.getFileProcessContext().getBean("spanAdapter");
	}
	
	/**
	 * 
	 * return "ERROR" or "SUCCESS"
	 * 
	 */
	public static String selectFNDataValidation(String fileName){
		String statusFN;
		
		int idxFilename = fileName.indexOf(".");
		fileName = fileName.replace(".jar", "");
		fileName = fileName.substring(0, idxFilename);
		
		List<SpanDataValidation> selectFNDataValidationOutput = Odbc.selectFNDataValidation(fileName);
		SpanDataValidation spanDataValidation = new SpanDataValidation();
		if(!BusinessUtil.isListEmpty(selectFNDataValidationOutput)){
			spanDataValidation = selectFNDataValidationOutput.get(0);
		}
		
		String batchId = spanDataValidation.getBatchId();
		
		if(batchId==null){
			statusFN = "ERROR";
		}
		else{
			statusFN = "SUCCESS";
		}
		
		return statusFN;
	}
	
	/**
	 * mandiri.span.db.service:getProviderInfo
	 * return object[] <br />
	 * object[0] List<T_PROVIDERS> <br />
	 * object[1] T_PROVIDERS
	 * 
	 */
	public static ProviderInfo getProviderInfo(String PROVIDER_CODE_ALIAS, String ACCOUNT_NO){
		ProviderInfo providerInfo = new ProviderInfo();
		
		List<Provider> ResultList = null;
		Provider ResultDoc = null;
		//branch
			//%PROVIDER_CODE_ALIAS%!=null
			if(PROVIDER_CODE_ALIAS!=null){
				ResultList = Adapter.getProviderInfo(PROVIDER_CODE_ALIAS);
			}
			//%ACCOUNT_NO%!=null
			else if(ACCOUNT_NO!=null){
				ResultDoc = Odbc.getProviderInfoDoc(ACCOUNT_NO);
			}
			
		providerInfo.setResultList(ResultList);
		providerInfo.setResultDoc(ResultDoc);
		
		return providerInfo;
	}
	
	/**
	 * mandiri.span.db.service:selectSParamFTP <br />
	 * return String[] <br />
	 * String[0] oValue <br />
	 * String[1] password <br />
	 * String[2] username <br />
	 * String[3] ip_host <br />
	 * String[4] port <br />
	 * String[5] Path <br />
	 * String[6] value <br />
	 * 
	 */
	public static FtpParam selectSParamFTP(String iPARAM_ID){
		
		String oValue = null;
		String password = null;
		String username = null;
		String ip_host = null;
		String port = null;
		String Path = null;
		String value = null;
		
		FtpParam strings = new FtpParam();
		
		oValue = Odbc.selectSParamFTP(iPARAM_ID);
		String known = oValue.substring(0,3);
		
		//branch
			if("ftp".equalsIgnoreCase(known)){
				
				String aaa = oValue.substring(6);
				int usernameInt = aaa.indexOf(":");
				
				//get username
					username = aaa.substring(0,usernameInt);
					String aaa2 = aaa.substring(usernameInt);
					
					aaa2 = aaa2.substring(1);
					
					int passwordInt = aaa2.indexOf("@");
				
				//get password
					password = aaa2.substring(0, passwordInt);
					String aaa3 = aaa2.substring(passwordInt);
					
					aaa3 = aaa3.substring(1);
					
					int ip_hostInt = aaa3.indexOf(":");
					
				//get ip host
					ip_host = aaa3.substring(0, ip_hostInt);
					String aaa4 = aaa3.substring(ip_hostInt);
					
					int portInt = aaa4.indexOf("/");
					
					port = aaa4.substring(0, portInt);
					Path = aaa4.substring(portInt);
					
				//get port
					port = port.substring(1);
			}
			else{
				//default
				value = oValue;
			}
		
		strings.setoValue(oValue);
		strings.setPassword(password); 
		strings.setUsername(username);
		strings.setIp_host(ip_host);
		strings.setPort(port); 
		strings.setPath(Path);
		strings.setValue(value);
		
		return strings;
	}
	
	/**
	 * mandiri.span.db.service:insertSPANDataValidation
	 * 
	 */
	public static InsertSPANDataValidationResponse insertSPANDataValidation(String fileName, String batchId, String trxHeaderId, 
			String spanFnType, String debitAccount, String debitAccountType, String documentDate, String totalAmount,  
			String totalRecord, String procStatuc, String procDescription, String xmlFileName, String responseCode, 
			String ackFileName, String ackStatus, String ackDescription, String trxType, String rejectedFileFlag){
		
		InsertSPANDataValidationResponse insertSPANDataValidationResponse = new InsertSPANDataValidationResponse();
		
		String status = null;
		String errorMessage = null;
		
		Date createDate = new Date();
		try {
			SpanDataValidation spanDataValidation = new SpanDataValidation();
			spanDataValidation.setFileName(fileName);
			spanDataValidation.setBatchId(batchId);
			spanDataValidation.setTrxHeaderId(trxHeaderId);
			spanDataValidation.setSpanFnType(spanFnType);
			spanDataValidation.setDebitAccount(debitAccount);
			spanDataValidation.setDebitAccountType(debitAccountType);
			spanDataValidation.setDocumentDate(documentDate);
			spanDataValidation.setTotalAmount(totalAmount);
			spanDataValidation.setTotalRecord(totalRecord);
			spanDataValidation.setProcStatuc(procStatuc);
			spanDataValidation.setProcDescription(procDescription);
			spanDataValidation.setXmlFileName(xmlFileName);
			spanDataValidation.setResponseCode(responseCode);
			spanDataValidation.setAckFileName(ackFileName);
			spanDataValidation.setAckStatus(ackStatus);
			spanDataValidation.setAckDescription(ackDescription);
			spanDataValidation.setCreateDate(createDate);
			spanDataValidation.setUpdateDate(createDate);
			spanDataValidation.setTrxType(trxType);
			spanDataValidation.setRejectedFileFlag(rejectedFileFlag);
			
			Odbc.insertSPANDataValidation(spanDataValidation);
			InsertSPANDocumentNoListResponse insertSPANDocumentNoListResponse = Service.insertSPANDocumentNOList(fileName, xmlFileName);
			status = "SUCCESS";
			System.out.println("Success inserting to SpanDataValidation : "+fileName);
		} catch (Exception e) {
			e.printStackTrace();
			status = "ERROR";
			errorMessage = e.toString();
		}
		
		insertSPANDataValidationResponse.setStatus(status);
		insertSPANDataValidationResponse.setErrorMessage(errorMessage);
		
		return insertSPANDataValidationResponse;
		
	}
	
	/**
	 * mandiri.span.db.service:insertSPANDocumentNOList
	 */
	public static InsertSPANDocumentNoListResponse insertSPANDocumentNOList(String fileName, String xmlValues){
		
		InsertSPANDocumentNoListResponse insertSPANDocumentNoListResponse = new InsertSPANDocumentNoListResponse();
		
		String status = null;
		String errorMessage = null;
		try {
			SP2DDocument sp2dDocument = (SP2DDocument) PubUtil.xmlValuesToDocument(SP2DDocument.class, xmlValues);
			List<DataArea> dataAreas = sp2dDocument.getDataArea();
			List<DocumentNo> DocumentNOList = new ArrayList<DocumentNo>();
			String statusList = null;
			
			for(DataArea dataArea : dataAreas){
				statusList = "1";
				
				DocumentNo documentNo = new DocumentNo();
				documentNo.setFilename(fileName);
				documentNo.setBeneficiaryName(dataArea.getBeneficiaryName());
				documentNo.setDocumentNumber(dataArea.getDocumentNumber());
				documentNo.setDocumentDate(dataArea.getDocumentDate());
				documentNo.setBeneficiaryAccount(dataArea.getBeneficiaryAccount());
				documentNo.setAmount(dataArea.getAmount());
				documentNo.setAgentBankAccountNumber(dataArea.getAgentBankAccountName());
				
				DocumentNOList.add(documentNo);
			}
			
			List<SpanDocumentNoDetailList> spanDocumentNoDetailLists = new ArrayList<SpanDocumentNoDetailList>();
			if("1".equalsIgnoreCase(statusList)){
				if(DocumentNOList!=null){
					for(DocumentNo documentNo : DocumentNOList){
						SpanDocumentNoDetailList spanDocumentNoDetailList = new SpanDocumentNoDetailList();
						
						spanDocumentNoDetailList.setFileName(documentNo.getFilename());
						spanDocumentNoDetailList.setDocumentNo(documentNo.getDocumentNumber());
						spanDocumentNoDetailList.setDocumentDate(documentNo.getDocumentDate());
						spanDocumentNoDetailList.setCreditAccountName(documentNo.getBeneficiaryName());
						spanDocumentNoDetailList.setCreditAccountNumber(documentNo.getBeneficiaryAccount());
						spanDocumentNoDetailList.setDebitAccountNo(documentNo.getAgentBankAccountNumber());
						spanDocumentNoDetailList.setAmount(documentNo.getAmount());
						
						spanDocumentNoDetailLists.add(spanDocumentNoDetailList);
					}
				}
				
			}
			else{
				if(dataAreas!=null){
					for(DataArea dataArea : dataAreas){
						SpanDocumentNoDetailList spanDocumentNoDetailList = new SpanDocumentNoDetailList();
						
						spanDocumentNoDetailList.setFileName(fileName);
						spanDocumentNoDetailList.setDocumentNo(dataArea.getDocumentNumber());
						spanDocumentNoDetailList.setDocumentDate(dataArea.getDocumentDate());
						spanDocumentNoDetailList.setCreditAccountName(dataArea.getBeneficiaryName());
						spanDocumentNoDetailList.setCreditAccountNumber(dataArea.getBeneficiaryAccount());
						spanDocumentNoDetailList.setDebitAccountNo(dataArea.getAgentBankAccountNumber());
						spanDocumentNoDetailList.setAmount(dataArea.getAmount());
						
						spanDocumentNoDetailLists.add(spanDocumentNoDetailList);
					}
				}
			}
			
			Adapter.insertSPANDocumentNOListBatch(spanDocumentNoDetailLists);
			
			status = "SUCCESS";
		} catch (Exception e) {
			status = "ERROR";
			errorMessage = e.toString();
			e.printStackTrace();
		}
		
		insertSPANDocumentNoListResponse.setStatus(status);
		insertSPANDocumentNoListResponse.setErrorMessage(errorMessage);
		
		return insertSPANDocumentNoListResponse;
	}

	public static ClientParams selectClientParams(String clientKey) {
		ClientParams clientParamsOutput = new ClientParams();
		clientParamsOutput= Adapter.selectClientParameters(clientKey);
		int idx = clientParamsOutput.getBankName().toUpperCase().indexOf("SYARIAH");
		String bankNameType = "";
		if(idx == -1){
			bankNameType = "1";
		}else{
			bankNameType = "2";
		}
		clientParamsOutput.setBankNameType(bankNameType);
		
		String result = Service.checkBenefBankCode(clientKey);
		if (!"1".equalsIgnoreCase(result))
			clientParamsOutput.setCityCD("");
		
		return clientParamsOutput;
	}

	private static String checkBenefBankCode(String benefBankCode) {
		// TODO Auto-generated method stub
		String resultMark = "";
		Parameter paramsObj = Odbc.selectSParameter(SParameterConstant._20150512P01);
		if (paramsObj == null){
			resultMark = "1";
		}else{
			String specialBenefBankCode = paramsObj.getValue();
			if(specialBenefBankCode == null || "".equalsIgnoreCase(specialBenefBankCode)){
				resultMark = "1";
			}else{
				String [] benefList = specialBenefBankCode.split("|");
				for (int i = 0; i < benefList.length; i++) {
					if(benefList[i].equalsIgnoreCase(benefBankCode)){
						resultMark  =  "1";
						break;
					}else{
						resultMark = "0";
					}
				}
			}
		}
		
		return resultMark;
	}

	/**
	 * mandiri.span.db.service:updateSPAN
	 * @param sqlUpdate
	 * @return
	 */
	public static UpdateSpanResponse updateSPAN(Query sqlUpdate) {
		UpdateSpanResponse response = new UpdateSpanResponse();
		System.out.println(PubUtil.concat("update SPAN ",sqlUpdate.getQueryString()));
		
		try {
			getAdapterBean().updateSPAN(sqlUpdate);
			response.setStatus("SUCCESS");
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus("ERROR");
			response.setErrorMessage(e.toString());
		}
		return response;
	}
	
	/**
	 * mandiri.span.db.service:selectExtractFileSPAN
	 * @param procStatus
	 * @param rowNum
	 * @return
	 */
	public static List<SpanDataValidation> selectExtractFileSPAN(String procStatus, String rowNum) {
		return Adapter.selectExtractFileSPAN(procStatus, rowNum);
	}

	public static List<SpanDataValidation> selectSPANDataValidationStatus(String procStatus) {
		return Odbc.selectSPANDataValidationStatus(procStatus, null);
	}

	public static int selectCountSPANFileRejected(String filename) {
		return Odbc.selectCountSPANFileRejected(filename);
	}
	
	/**
	 * mandiri.span.db.service:spanDataValidationSP2DNO
	 */
	public static void insertSPANDataValidationSP2DNO(List<SpanDataValidationSP2DNo> outputList) {
		Odbc.insertSPANDataValidationSP2DNO(outputList);
	}
	
	/**
	 * mandiri.span.db.service:updateForFileRejected
	 */
	public static UpdateForFileRejectedResponse updateForFileRejected(String filename){
		UpdateForFileRejectedResponse response = new UpdateForFileRejectedResponse();
		
		InterfaceDAO dao = (SpanDataValidationDao) BusinessUtil.getDao("spanDataValidationDao");
//		String sql = "update span_data_validation set rejected_file_flag='1' where file_name='"+filename+"' and rejected_file_flag='0'";
		String hql = "UPDATE SpanDataValidation set rejectedFileFlag = '1' WHERE fileName = '"+filename+"' AND rejectedFileFlag='0'";
		Query query = dao.getQuery(hql);
		UpdateSpanResponse updateSpanResponse = updateSPAN(query);
		
		response.setStatus(updateSpanResponse.getStatus());
		response.setErrorMessage(updateSpanResponse.getErrorMessage());
		
		return response;
	}
	
	/**
	 * mandiri.span.db.service:selectSPANVoidDataTransaction
	 */
	public static List<SpanVoidDataTrx> selectSpanVoidDataTransaction(String fileName){
		List<SpanVoidDataTrx> voidList = Adapter.selectSpanVoidDataTransaction(fileName, "0");
		return voidList;
	}
	
	public static void insertSpanDataValidation(SpanDataValidation spanDataValidation) {
		// TODO Auto-generated method stub
		/*
		 * Insert into SPAN_DAT_VALIDATION
		 * VALUES SPAN_DATA_VALIDAITON
		 */
	}
	
	public static void insertSpanRecreateRejectedFile(SpanRecreateFileRejected spanRecreateFile) {
		Odbc.insertSpanRecreateRejectedFile(spanRecreateFile);
	}
	
	public static void insertSpanRejectedFileHis(SpanRejectedFileHis spanHis) {
		Odbc.insertSpanRejectedFileHis(spanHis);
	}
	
	/**
	 * mandiri.span.db.service:selectSPANDataValidation
	 */
	public static SelectSpanDataValidationResponse selectSpanDataValidation(String fileName){
		SelectSpanDataValidationResponse response = null;
		List<SpanDataValidation> spanDataValidations = Odbc.selectSpanDataValidation(fileName);
		if(spanDataValidations!=null){
			for(SpanDataValidation spanDataValidation : spanDataValidations){
				response = new SelectSpanDataValidationResponse();
				response.setFileName(spanDataValidation.getFileName());
				response.setBatchId(spanDataValidation.getBatchId());
				response.setTrxHeaderId(spanDataValidation.getTrxHeaderId());
				response.setSpanFnType(spanDataValidation.getSpanFnType());
				response.setDebitAccount(spanDataValidation.getDebitAccount());
				response.setDebitAccountType(spanDataValidation.getDebitAccountType());
				response.setResponseCode(spanDataValidation.getResponseCode());
				response.setXmlFileName(spanDataValidation.getXmlFileName());
				response.setProcStatus(spanDataValidation.getProcStatuc());
				response.setTrxType(spanDataValidation.getTrxType());
				response.setTotalRecord(spanDataValidation.getTotalRecord());
				response.setXmlVoidFileName(spanDataValidation.getXmlVoidFileName());
				response.setUpdateDate(spanDataValidation.getUpdateDate());
				response.setAckStatus(spanDataValidation.getAckStatus());
				response.setTotalAmount(spanDataValidation.getTotalAmount());
			}
		}
		
		return response;
	}
	
	/**
	 * mandiri.span.db.service:selectSPANHostResponse
	 * @param fileName
	 * @return
	 */
	public static List<ACKExecutedDoc> selectSPANHostResponse(String fileName) {
		return Odbc.selectSPANHostResponse(fileName);
	}
	
	/**
	 * mandiri.span.db.adapter.odbc:getProviderInfoDoc
	 * @param accountNo
	 * @return
	 */
	public static Provider getProviderInfoDoc(String accountNo) {
		//Select * from t_provider where provideraccount = accountno
		ProviderDao dao = (ProviderDao) BusinessUtil.getDao(DaoConstant.ProviderDao);
		List<Provider> providers = dao.getByProviderAccount(accountNo);
		if(!BusinessUtil.isListEmpty(providers)){
			return providers.get(0);
		}
		return null;
	}
	
	/**
	 * mandiri.span.db.service:getReturCode
	 * @param string
	 * @return
	 */
	public static List<SpanHostResponseCode> getReturCode(String string) {
		return Adapter.getReturCode(string);
	}
	
	public static DataFileNACK selectSPANDataValidationNACK(String procStatus,
			String documentDate, String responseCode, String providerCode, String filename) {
		DataFileNACK dataFileNACK = new DataFileNACK();
		
		SpanDataValidationDao dao = (SpanDataValidationDao) BusinessUtil.getDao("spanDataValidationDao");
		dataFileNACK = dao.selectSPANDataValidationNACK(procStatus, documentDate, responseCode, providerCode, filename);
		
		return dataFileNACK;
	}
	
	public static String getTparameter(String paramName){
		SystemParameterDAO dao = (SystemParameterDAO) BusinessUtil.getDao(DaoConstant.SystemParameterDAO);
		return dao.getValueByParamName(paramName);
	}
}
