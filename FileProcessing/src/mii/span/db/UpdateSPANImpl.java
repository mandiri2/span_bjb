package mii.span.db;

import mii.span.process.model.UpdateSpanResponse;

import org.hibernate.Query;

import com.mii.dao.ToBeExecuteDao;
import com.mii.helpers.BasicDaoImplement;

public class UpdateSPANImpl extends BasicDaoImplement implements UpdateSPAN{
	
	/**
	 * Update table based sql string
	 * mandiri.span.db.adapter.odbc:updateSPANStatus
	 */
	public UpdateSpanResponse updateSPAN (Query query){
		
		UpdateSpanResponse output = new UpdateSpanResponse();

		System.out.println(query.getQueryString());
		try {
			getCurrentSession().createQuery(query.getQueryString()).executeUpdate();
			output.setStatus("SUCCESS");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			output.setStatus("ERROR");
			output.setErrorMessage(e.getMessage());
		}

		return output;
	}
}
