package mii.span.constant;

public class SpanDocumentConstant {
	
	//Application Area
	public static final String APPLICATION_AREA = "ApplicationArea";
	public static final String APPLICATION_AREA_SENDER_IDENTIFIER	= "ApplicationAreaSenderIdentifier";
	public static final String APPLICATION_AREA_RECEIVER_IDENTIFIER = "ApplicationAreaReceiverIdentifier" ;
	public static final String APPLICATION_AREA_DETAIL_SENDER_IDENTIFIER = "ApplicationAreaDetailSenderIdentifier";
	public static final String APPLICATION_AREA_DETAIL_RECEIVER_IDENTIFIER = "ApplicationAreaDetailReceiverIdentifier";
	public static final String APPLICATION_AREA_CREATION_DATE_TIME = "ApplicationAreaCreationDateTime";
	public static final String APPLICATION_AREA_MESSAGE_IDENTIFIER = "ApplicationAreaMessageIdentifier";
	public static final String APPLICATION_AREA_MESSAGE_TYPE_INDICATOR = "ApplicationAreaMessageTypeIndicator";
	public static final String APPLICATION_AREA_MESSAGE_VERSION_TEXT = "ApplicationAreaMessageVersionText";
	
	//DataArea
	public static final String DATA_AREA = "DataArea";
	public static final String DOCUMENT_DATE = "DocumentDate";
	public static final String DOCUMENT_NUMBER = "DocumentNumber";
	public static final String BENEFICIARY_NAME = "BeneficiaryName";
	public static final String BENEFICIARY_BANK_CODE = "BeneficiaryBankCode";
	public static final String BENEFICIARY_BANK = "BeneficiaryBank";
	public static final String BENEFICIARY_ACCOUNT = "BeneficiaryAccount";
	public static final String AMOUNT = "Amount";
	public static final String CURRENCY_TARGET = "CurrencyTarget";
	public static final String DESCRIPTION = "Description";
	public static final String AGENT_BANK_CODE = "AgentBankCode";
	public static final String AGENT_BANK_ACCOUNT_NUMBER =  "AgentBankAccountNumber";
	public static final String AGENT_BANK_ACCOUNT_NAME = "AgentBankAccountName";
	public static final String EMAIL_ADDRESS = "EmailAddress";
	public static final String SWIFT_CODE = "SwiftCode";
	public static final String IBAN_CODE = "IBANCode";
	public static final String PAYMENT_METHOD = "PaymentMethod";
	public static final String SP2D_COUNT = "SP2DCount";
	
	//FOOTER
	public static final String FOOTER = "Footer";
	public static final String TOTAL_COUNT = "TotalCount";
	public static final String TOTAL_AMOUNT = "TotalAmount";
	public static final String TOTAL_BATCH_COUNT = "TotalBatchCount";
}
