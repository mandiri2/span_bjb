package mii.span.constant;

public class SParameterConstant {
	public static final String _20130314P03 = "20130314P03";
	public static final String _20130314P02 = "20130314P02";
	public static final String _20141230P01 = "20141230P01";
	public static final String _20160804P02 = "20160804P02";
	public static final String _20160804P03 = "20160804P03";
	public static final String _20160804P04 = "20160804P04";
	public static final String _20160804P05 = "20160804P05";
	public static final String _20140210P02 = "20140210P02";// limit SKN
	public static final String _20140210P01 = "20140210P01";// limit RTGS
	public static final String _20150512P01 = "20150512P01";//Special Beneficiary Bank Code
	public static final String _20130111P01 = "20130111P01";
	public static final String _20140328P01 = "20140328P01";
	public static final String _20140328P02 = "20140328P02";
	public static final String _20140328P04 = "20140328P04";
	public static final String _20160804P01 = "20160804P01";
	
	
}
