package mii.span;

import mii.span.constant.SParameterConstant;
import mii.span.db.service.Odbc;
import mii.span.process.model.ReplaceMvaAccResponse;
import mii.span.util.PubUtil;

public class Service {
	/**
	 * mandiri.span.service:replaceMvaAcc
	 */
	public static ReplaceMvaAccResponse replaceMvaAcc(String MVAAccount, String amount){
		ReplaceMvaAccResponse response = new ReplaceMvaAccResponse();
		
		String username = null;
		String password = null;
		String port = null;
		String trxDetailIDForMVA = null;
		String value = null;
		String status = null;
		String realAccount = null;
		String mvaCompanyCode = null;
		String[] valueList = null;
		//o sequence.init
			//o map
			username = Odbc.selectSParameter(SParameterConstant._20140328P01).getValue();
			password = Odbc.selectSParameter(SParameterConstant._20140328P02).getValue();
			port = Odbc.selectSParameter(SParameterConstant._20140328P04).getValue();
			trxDetailIDForMVA = null;
			//c map
		//c sequence.init
		
		//o sequence.new	
		try {
			//o
			value = mii.span.db.adapter.Odbc.selectMdrCfgMain(MVAAccount).getValue1();
			//c
			//o branch.on.value
				//o sequence.null
				if(value==null){
					//FIXME WS mandiri.span.ws.consumer.spanUBPInquiry_.connectors:spanUBPInquiry_PortType_spanUBPInquiry
				}
				//c sequence.null
				//o sequence.default
				else{
					//o map
					valueList = PubUtil.tokenize(value, "|");
					realAccount = valueList[3];
					status = "00";
					mvaCompanyCode = null;
					//c map
					//o map
					trxDetailIDForMVA = valueList[2];
					mvaCompanyCode = PubUtil.concat(valueList[0],"|",valueList[1],"|",valueList[2]);
					//c map
				}
				//c sequence.default
			//c branch.on.value
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		//c sequence.new	
			
		response.setRealAccount(realAccount);
		response.setStatus(status);
		response.setMvaCompanyCode(mvaCompanyCode);
		response.setTrxDetailIdForMva(trxDetailIDForMVA);
		return response;
	}
}
