package mii.span.model;

import java.util.Date;

public class SpanRecreateFileRejected {
	private String originalFileName,
		newFileName,
		documentNo, //id
		originalDocumentDate,
		beneficiaryName,
		beneficiaryBankCode,
		beneficiaryBank,
		beneficiaryAccount,
		amount,
		description,
		agentBankCode,
		agentBankAccountNo,
		agentBankAccountName,
		paymentMethod,
		transactionFlag;
	
	private Date createDate;
	
	/** GETTER SETTER**/
	public String getOriginalFileName() {
		return originalFileName;
	}

	public void setOriginalFileName(String originalFileName) {
		this.originalFileName = originalFileName;
	}

	public String getNewFileName() {
		return newFileName;
	}

	public void setNewFileName(String newFileName) {
		this.newFileName = newFileName;
	}

	public String getDocumentNo() {
		return documentNo;
	}

	public void setDocumentNo(String documentNo) {
		this.documentNo = documentNo;
	}

	public String getOriginalDocumentDate() {
		return originalDocumentDate;
	}

	public void setOriginalDocumentDate(String originalDocumentDate) {
		this.originalDocumentDate = originalDocumentDate;
	}

	public String getBeneficiaryName() {
		return beneficiaryName;
	}

	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}

	public String getBeneficiaryBankCode() {
		return beneficiaryBankCode;
	}

	public void setBeneficiaryBankCode(String beneficiaryBankCode) {
		this.beneficiaryBankCode = beneficiaryBankCode;
	}

	public String getBeneficiaryBank() {
		return beneficiaryBank;
	}

	public void setBeneficiaryBank(String beneficiaryBank) {
		this.beneficiaryBank = beneficiaryBank;
	}

	public String getBeneficiaryAccount() {
		return beneficiaryAccount;
	}

	public void setBeneficiaryAccount(String beneficiaryAccount) {
		this.beneficiaryAccount = beneficiaryAccount;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAgentBankCode() {
		return agentBankCode;
	}

	public void setAgentBankCode(String agentBankCode) {
		this.agentBankCode = agentBankCode;
	}

	public String getAgentBankAccountNo() {
		return agentBankAccountNo;
	}

	public void setAgentBankAccountNo(String agentBankAccountNo) {
		this.agentBankAccountNo = agentBankAccountNo;
	}

	public String getAgentBankAccountName() {
		return agentBankAccountName;
	}

	public void setAgentBankAccountName(String agentBankAccountName) {
		this.agentBankAccountName = agentBankAccountName;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getTransactionFlag() {
		return transactionFlag;
	}

	public void setTransactionFlag(String transactionFlag) {
		this.transactionFlag = transactionFlag;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
}
