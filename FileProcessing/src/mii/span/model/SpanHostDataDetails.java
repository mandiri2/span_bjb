package mii.span.model;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SpanHostDataDetails implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String BATCHID,
	TRXHEADERID,
	TRXDETAILID,
	SENDER_ID,
	RECEIVER_ID,
	DETAIL_SENDER_ID,
	DETAIL_RECEIVER_ID,
	MESSAGE_ID,
	MSG_TYPE_ID,
	DOC_DATE,
	DOC_NUMBER,
	BEN_NAME,
	BEN_BANK_CODE,
	BEN_BANK,
	BEN_ACCOUNT,
	AMOUNT,
	CURRENCY,
	DESCRIPTION,
	AGENT_BANK_CODE,
	AGENT_BANK_ACCT_NO,
	AGENT_BANK_ACCT_NAME,
	EMAIL_ADDR,
	SWIFT_CODE,
	IBAN_CODE,
	PAYMENT_METHODS,
	SPAN_COUNT,
	TOTAL_ACCOUNT,
	TOTAL_AMOUNT,
	TOT_BATCH_ACCOUNT,
	TRANSACTION_REF,
	PROVIDER_REF,
	DEBITACCNO,
	DEBITACCEXRATE,
	DEBITACCCURRCODE,
	CHARGEINSTRUCTION,
	REMITTERRESFLAG,
	REMITTERRESCODE,
	REMITTERCITIZENFLAG,
	REMITTERCITIZENCODE,
	CREDITACCNO,
	CREDITACCNAME,
	CREDITACCEXRATE,
	CREDITTRFCURR,
	CREDITTRFAMOUNT,
	TRXREMARK1,
	TRXREMARK2,
	TRXREMARK3,
	TRXREMARK4,
	FTSERVICES,
	CHARGECURRCODE1,
	CHARGEAMOUNT1,
	CHARGECURRCODE2,
	CHARGEAMOUNT2,
	CHARGECURRCODE3,
	CHARGEAMOUNT3,
	CHARGECURRCODE4,
	CHARGEAMOUNT4,
	CHARGECURRCODE5,
	CHARGEAMOUNT5,
	BENEFICIARYADDR1,
	BENEFICIARYADDR2,
	BENEFICIARYADDR3,
	BENEFICIARYBANKCODE,
	BENEFICIARYBANKNAME,
	BENEFICIARYBANKBRANCHNAME,
	BENEFICIARYBANKADDR1,
	BENEFICIARYBANKADDR2,
	BENEFICIARYBANKADDR3,
	BENEFICIARYBANKCITYNAME,
	BENEFICIARYBANKCOUNTRYNAME,
	BENEFICIARYRESFLAG,
	BENEFICIARYRESCODE,
	BENEFICIARYCITIZENFLAG,
	BENEFICIARYCITIZENCODE,
	DEBITREFNO,
	FINALIZEPAYMENTFLAG,
	CREDITREFNO,
	BENEFICIARYBANKCITYCODE,
	ORGANIZATIONDIRNAME,
	PURPOSEOFTRX,
	REMITTANCECODE1,
	REMITTANCEINFO1,
	REMITTANCECODE2,
	REMITTANCEINFO2,
	REMITTANCECODE3,
	REMITTANCEINFO3,
	REMITTANCECODE4,
	REMITTANCEINFO4,
	INSTRUCTIONCODE1,
	INSTRUCTIONREMARK1,
	INSTRUCTIONCODE2,
	INSTRUCTIONREMARK2,
	INSTRUCTIONCODE3,
	INSTRUCTIONREMARK3,
	SWIFTMETHOD,
	IBTBUYRATE,
	IBTSELLRATE,
	VALUEDATE,
	DEBITAMOUNT,
	RESERVE1,
	RESERVE2,
	RESERVE3,
	RESERVE4,
	RESERVE5,
	STATUS,
	ERRMESSAGE,
	LEGSTATUS,
	MVA_STATUS,
	MVA_DESC,
	SORBOR_MARK,
	EXEC_STATUS;
	
	private Date CREATE_DT, INSERTDT, PROCESSDT;
	private Long RESPCOUNTCHECKER;
	
	//non db
	private transient String CREATE_DT_STRING;
	
	public String getBATCHID() {
		return BATCHID;
	}

	public void setBATCHID(String bATCHID) {
		BATCHID = bATCHID;
	}

	public String getTRXHEADERID() {
		return TRXHEADERID;
	}

	public void setTRXHEADERID(String tRXHEADERID) {
		TRXHEADERID = tRXHEADERID;
	}

	public String getTRXDETAILID() {
		return TRXDETAILID;
	}

	public void setTRXDETAILID(String tRXDETAILID) {
		TRXDETAILID = tRXDETAILID;
	}

	public String getSENDER_ID() {
		return SENDER_ID;
	}

	public void setSENDER_ID(String sENDER_ID) {
		SENDER_ID = sENDER_ID;
	}

	public String getRECEIVER_ID() {
		return RECEIVER_ID;
	}

	public void setRECEIVER_ID(String rECEIVER_ID) {
		RECEIVER_ID = rECEIVER_ID;
	}

	public String getDETAIL_SENDER_ID() {
		return DETAIL_SENDER_ID;
	}

	public void setDETAIL_SENDER_ID(String dETAIL_SENDER_ID) {
		DETAIL_SENDER_ID = dETAIL_SENDER_ID;
	}

	public String getDETAIL_RECEIVER_ID() {
		return DETAIL_RECEIVER_ID;
	}

	public void setDETAIL_RECEIVER_ID(String dETAIL_RECEIVER_ID) {
		DETAIL_RECEIVER_ID = dETAIL_RECEIVER_ID;
	}

	public String getMESSAGE_ID() {
		return MESSAGE_ID;
	}

	public void setMESSAGE_ID(String mESSAGE_ID) {
		MESSAGE_ID = mESSAGE_ID;
	}

	public String getMSG_TYPE_ID() {
		return MSG_TYPE_ID;
	}

	public void setMSG_TYPE_ID(String mSG_TYPE_ID) {
		MSG_TYPE_ID = mSG_TYPE_ID;
	}

	public String getDOC_DATE() {
		return DOC_DATE;
	}

	public void setDOC_DATE(String dOC_DATE) {
		DOC_DATE = dOC_DATE;
	}

	public String getDOC_NUMBER() {
		return DOC_NUMBER;
	}

	public void setDOC_NUMBER(String dOC_NUMBER) {
		DOC_NUMBER = dOC_NUMBER;
	}

	public String getBEN_NAME() {
		return BEN_NAME;
	}

	public void setBEN_NAME(String bEN_NAME) {
		BEN_NAME = bEN_NAME;
	}

	public String getBEN_BANK_CODE() {
		return BEN_BANK_CODE;
	}

	public void setBEN_BANK_CODE(String bEN_BANK_CODE) {
		BEN_BANK_CODE = bEN_BANK_CODE;
	}

	public String getBEN_BANK() {
		return BEN_BANK;
	}

	public void setBEN_BANK(String bEN_BANK) {
		BEN_BANK = bEN_BANK;
	}

	public String getBEN_ACCOUNT() {
		return BEN_ACCOUNT;
	}

	public void setBEN_ACCOUNT(String bEN_ACCOUNT) {
		BEN_ACCOUNT = bEN_ACCOUNT;
	}

	public String getAMOUNT() {
		return AMOUNT;
	}

	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}

	public String getCURRENCY() {
		return CURRENCY;
	}

	public void setCURRENCY(String cURRENCY) {
		CURRENCY = cURRENCY;
	}

	public String getDESCRIPTION() {
		return DESCRIPTION;
	}

	public void setDESCRIPTION(String dESCRIPTION) {
		DESCRIPTION = dESCRIPTION;
	}

	public String getAGENT_BANK_CODE() {
		return AGENT_BANK_CODE;
	}

	public void setAGENT_BANK_CODE(String aGENT_BANK_CODE) {
		AGENT_BANK_CODE = aGENT_BANK_CODE;
	}

	public String getAGENT_BANK_ACCT_NO() {
		return AGENT_BANK_ACCT_NO;
	}

	public void setAGENT_BANK_ACCT_NO(String aGENT_BANK_ACCT_NO) {
		AGENT_BANK_ACCT_NO = aGENT_BANK_ACCT_NO;
	}

	public String getAGENT_BANK_ACCT_NAME() {
		return AGENT_BANK_ACCT_NAME;
	}

	public void setAGENT_BANK_ACCT_NAME(String aGENT_BANK_ACCT_NAME) {
		AGENT_BANK_ACCT_NAME = aGENT_BANK_ACCT_NAME;
	}

	public String getEMAIL_ADDR() {
		return EMAIL_ADDR;
	}

	public void setEMAIL_ADDR(String eMAIL_ADDR) {
		EMAIL_ADDR = eMAIL_ADDR;
	}

	public String getSWIFT_CODE() {
		return SWIFT_CODE;
	}

	public void setSWIFT_CODE(String sWIFT_CODE) {
		SWIFT_CODE = sWIFT_CODE;
	}

	public String getIBAN_CODE() {
		return IBAN_CODE;
	}

	public void setIBAN_CODE(String iBAN_CODE) {
		IBAN_CODE = iBAN_CODE;
	}

	public String getPAYMENT_METHODS() {
		return PAYMENT_METHODS;
	}

	public void setPAYMENT_METHODS(String pAYMENT_METHODS) {
		PAYMENT_METHODS = pAYMENT_METHODS;
	}

	public String getSPAN_COUNT() {
		return SPAN_COUNT;
	}

	public void setSPAN_COUNT(String sPAN_COUNT) {
		SPAN_COUNT = sPAN_COUNT;
	}

	public String getTOTAL_ACCOUNT() {
		return TOTAL_ACCOUNT;
	}

	public void setTOTAL_ACCOUNT(String tOTAL_ACCOUNT) {
		TOTAL_ACCOUNT = tOTAL_ACCOUNT;
	}

	public String getTOTAL_AMOUNT() {
		return TOTAL_AMOUNT;
	}

	public void setTOTAL_AMOUNT(String tOTAL_AMOUNT) {
		TOTAL_AMOUNT = tOTAL_AMOUNT;
	}

	public String getTOT_BATCH_ACCOUNT() {
		return TOT_BATCH_ACCOUNT;
	}

	public void setTOT_BATCH_ACCOUNT(String tOT_BATCH_ACCOUNT) {
		TOT_BATCH_ACCOUNT = tOT_BATCH_ACCOUNT;
	}

	public String getTRANSACTION_REF() {
		return TRANSACTION_REF;
	}

	public void setTRANSACTION_REF(String tRANSACTION_REF) {
		TRANSACTION_REF = tRANSACTION_REF;
	}

	public String getPROVIDER_REF() {
		return PROVIDER_REF;
	}

	public void setPROVIDER_REF(String pROVIDER_REF) {
		PROVIDER_REF = pROVIDER_REF;
	}

	public String getDEBITACCNO() {
		return DEBITACCNO;
	}

	public void setDEBITACCNO(String dEBITACCNO) {
		DEBITACCNO = dEBITACCNO;
	}

	public String getDEBITACCEXRATE() {
		return DEBITACCEXRATE;
	}

	public void setDEBITACCEXRATE(String dEBITACCEXRATE) {
		DEBITACCEXRATE = dEBITACCEXRATE;
	}

	public String getDEBITACCCURRCODE() {
		return DEBITACCCURRCODE;
	}

	public void setDEBITACCCURRCODE(String dEBITACCCURRCODE) {
		DEBITACCCURRCODE = dEBITACCCURRCODE;
	}

	public String getCHARGEINSTRUCTION() {
		return CHARGEINSTRUCTION;
	}

	public void setCHARGEINSTRUCTION(String cHARGEINSTRUCTION) {
		CHARGEINSTRUCTION = cHARGEINSTRUCTION;
	}

	public String getREMITTERRESFLAG() {
		return REMITTERRESFLAG;
	}

	public void setREMITTERRESFLAG(String rEMITTERRESFLAG) {
		REMITTERRESFLAG = rEMITTERRESFLAG;
	}

	public String getREMITTERRESCODE() {
		return REMITTERRESCODE;
	}

	public void setREMITTERRESCODE(String rEMITTERRESCODE) {
		REMITTERRESCODE = rEMITTERRESCODE;
	}

	public String getREMITTERCITIZENFLAG() {
		return REMITTERCITIZENFLAG;
	}

	public void setREMITTERCITIZENFLAG(String rEMITTERCITIZENFLAG) {
		REMITTERCITIZENFLAG = rEMITTERCITIZENFLAG;
	}

	public String getREMITTERCITIZENCODE() {
		return REMITTERCITIZENCODE;
	}

	public void setREMITTERCITIZENCODE(String rEMITTERCITIZENCODE) {
		REMITTERCITIZENCODE = rEMITTERCITIZENCODE;
	}

	public String getCREDITACCNO() {
		return CREDITACCNO;
	}

	public void setCREDITACCNO(String cREDITACCNO) {
		CREDITACCNO = cREDITACCNO;
	}

	public String getCREDITACCNAME() {
		return CREDITACCNAME;
	}

	public void setCREDITACCNAME(String cREDITACCNAME) {
		CREDITACCNAME = cREDITACCNAME;
	}

	public String getCREDITACCEXRATE() {
		return CREDITACCEXRATE;
	}

	public void setCREDITACCEXRATE(String cREDITACCEXRATE) {
		CREDITACCEXRATE = cREDITACCEXRATE;
	}

	public String getCREDITTRFCURR() {
		return CREDITTRFCURR;
	}

	public void setCREDITTRFCURR(String cREDITTRFCURR) {
		CREDITTRFCURR = cREDITTRFCURR;
	}

	public String getCREDITTRFAMOUNT() {
		return CREDITTRFAMOUNT;
	}

	public void setCREDITTRFAMOUNT(String cREDITTRFAMOUNT) {
		CREDITTRFAMOUNT = cREDITTRFAMOUNT;
	}

	public String getTRXREMARK1() {
		return TRXREMARK1;
	}

	public void setTRXREMARK1(String tRXREMARK1) {
		TRXREMARK1 = tRXREMARK1;
	}

	public String getTRXREMARK2() {
		return TRXREMARK2;
	}

	public void setTRXREMARK2(String tRXREMARK2) {
		TRXREMARK2 = tRXREMARK2;
	}

	public String getTRXREMARK3() {
		return TRXREMARK3;
	}

	public void setTRXREMARK3(String tRXREMARK3) {
		TRXREMARK3 = tRXREMARK3;
	}

	public String getTRXREMARK4() {
		return TRXREMARK4;
	}

	public void setTRXREMARK4(String tRXREMARK4) {
		TRXREMARK4 = tRXREMARK4;
	}

	public String getFTSERVICES() {
		return FTSERVICES;
	}

	public void setFTSERVICES(String fTSERVICES) {
		FTSERVICES = fTSERVICES;
	}

	public String getCHARGECURRCODE1() {
		return CHARGECURRCODE1;
	}

	public void setCHARGECURRCODE1(String cHARGECURRCODE1) {
		CHARGECURRCODE1 = cHARGECURRCODE1;
	}

	public String getCHARGEAMOUNT1() {
		return CHARGEAMOUNT1;
	}

	public void setCHARGEAMOUNT1(String cHARGEAMOUNT1) {
		CHARGEAMOUNT1 = cHARGEAMOUNT1;
	}

	public String getCHARGECURRCODE2() {
		return CHARGECURRCODE2;
	}

	public void setCHARGECURRCODE2(String cHARGECURRCODE2) {
		CHARGECURRCODE2 = cHARGECURRCODE2;
	}

	public String getCHARGEAMOUNT2() {
		return CHARGEAMOUNT2;
	}

	public void setCHARGEAMOUNT2(String cHARGEAMOUNT2) {
		CHARGEAMOUNT2 = cHARGEAMOUNT2;
	}

	public String getCHARGECURRCODE3() {
		return CHARGECURRCODE3;
	}

	public void setCHARGECURRCODE3(String cHARGECURRCODE3) {
		CHARGECURRCODE3 = cHARGECURRCODE3;
	}

	public String getCHARGEAMOUNT3() {
		return CHARGEAMOUNT3;
	}

	public void setCHARGEAMOUNT3(String cHARGEAMOUNT3) {
		CHARGEAMOUNT3 = cHARGEAMOUNT3;
	}

	public String getCHARGECURRCODE4() {
		return CHARGECURRCODE4;
	}

	public void setCHARGECURRCODE4(String cHARGECURRCODE4) {
		CHARGECURRCODE4 = cHARGECURRCODE4;
	}

	public String getCHARGEAMOUNT4() {
		return CHARGEAMOUNT4;
	}

	public void setCHARGEAMOUNT4(String cHARGEAMOUNT4) {
		CHARGEAMOUNT4 = cHARGEAMOUNT4;
	}

	public String getCHARGECURRCODE5() {
		return CHARGECURRCODE5;
	}

	public void setCHARGECURRCODE5(String cHARGECURRCODE5) {
		CHARGECURRCODE5 = cHARGECURRCODE5;
	}

	public String getCHARGEAMOUNT5() {
		return CHARGEAMOUNT5;
	}

	public void setCHARGEAMOUNT5(String cHARGEAMOUNT5) {
		CHARGEAMOUNT5 = cHARGEAMOUNT5;
	}

	public String getBENEFICIARYADDR1() {
		return BENEFICIARYADDR1;
	}

	public void setBENEFICIARYADDR1(String bENEFICIARYADDR1) {
		BENEFICIARYADDR1 = bENEFICIARYADDR1;
	}

	public String getBENEFICIARYADDR2() {
		return BENEFICIARYADDR2;
	}

	public void setBENEFICIARYADDR2(String bENEFICIARYADDR2) {
		BENEFICIARYADDR2 = bENEFICIARYADDR2;
	}

	public String getBENEFICIARYADDR3() {
		return BENEFICIARYADDR3;
	}

	public void setBENEFICIARYADDR3(String bENEFICIARYADDR3) {
		BENEFICIARYADDR3 = bENEFICIARYADDR3;
	}

	public String getBENEFICIARYBANKCODE() {
		return BENEFICIARYBANKCODE;
	}

	public void setBENEFICIARYBANKCODE(String bENEFICIARYBANKCODE) {
		BENEFICIARYBANKCODE = bENEFICIARYBANKCODE;
	}

	public String getBENEFICIARYBANKNAME() {
		return BENEFICIARYBANKNAME;
	}

	public void setBENEFICIARYBANKNAME(String bENEFICIARYBANKNAME) {
		BENEFICIARYBANKNAME = bENEFICIARYBANKNAME;
	}

	public String getBENEFICIARYBANKBRANCHNAME() {
		return BENEFICIARYBANKBRANCHNAME;
	}

	public void setBENEFICIARYBANKBRANCHNAME(String bENEFICIARYBANKBRANCHNAME) {
		BENEFICIARYBANKBRANCHNAME = bENEFICIARYBANKBRANCHNAME;
	}

	public String getBENEFICIARYBANKADDR1() {
		return BENEFICIARYBANKADDR1;
	}

	public void setBENEFICIARYBANKADDR1(String bENEFICIARYBANKADDR1) {
		BENEFICIARYBANKADDR1 = bENEFICIARYBANKADDR1;
	}

	public String getBENEFICIARYBANKADDR2() {
		return BENEFICIARYBANKADDR2;
	}

	public void setBENEFICIARYBANKADDR2(String bENEFICIARYBANKADDR2) {
		BENEFICIARYBANKADDR2 = bENEFICIARYBANKADDR2;
	}

	public String getBENEFICIARYBANKADDR3() {
		return BENEFICIARYBANKADDR3;
	}

	public void setBENEFICIARYBANKADDR3(String bENEFICIARYBANKADDR3) {
		BENEFICIARYBANKADDR3 = bENEFICIARYBANKADDR3;
	}

	public String getBENEFICIARYBANKCITYNAME() {
		return BENEFICIARYBANKCITYNAME;
	}

	public void setBENEFICIARYBANKCITYNAME(String bENEFICIARYBANKCITYNAME) {
		BENEFICIARYBANKCITYNAME = bENEFICIARYBANKCITYNAME;
	}

	public String getBENEFICIARYBANKCOUNTRYNAME() {
		return BENEFICIARYBANKCOUNTRYNAME;
	}

	public void setBENEFICIARYBANKCOUNTRYNAME(String bENEFICIARYBANKCOUNTRYNAME) {
		BENEFICIARYBANKCOUNTRYNAME = bENEFICIARYBANKCOUNTRYNAME;
	}

	public String getBENEFICIARYRESFLAG() {
		return BENEFICIARYRESFLAG;
	}

	public void setBENEFICIARYRESFLAG(String bENEFICIARYRESFLAG) {
		BENEFICIARYRESFLAG = bENEFICIARYRESFLAG;
	}

	public String getBENEFICIARYRESCODE() {
		return BENEFICIARYRESCODE;
	}

	public void setBENEFICIARYRESCODE(String bENEFICIARYRESCODE) {
		BENEFICIARYRESCODE = bENEFICIARYRESCODE;
	}

	public String getBENEFICIARYCITIZENFLAG() {
		return BENEFICIARYCITIZENFLAG;
	}

	public void setBENEFICIARYCITIZENFLAG(String bENEFICIARYCITIZENFLAG) {
		BENEFICIARYCITIZENFLAG = bENEFICIARYCITIZENFLAG;
	}

	public String getBENEFICIARYCITIZENCODE() {
		return BENEFICIARYCITIZENCODE;
	}

	public void setBENEFICIARYCITIZENCODE(String bENEFICIARYCITIZENCODE) {
		BENEFICIARYCITIZENCODE = bENEFICIARYCITIZENCODE;
	}

	public String getDEBITREFNO() {
		return DEBITREFNO;
	}

	public void setDEBITREFNO(String dEBITREFNO) {
		DEBITREFNO = dEBITREFNO;
	}

	public String getFINALIZEPAYMENTFLAG() {
		return FINALIZEPAYMENTFLAG;
	}

	public void setFINALIZEPAYMENTFLAG(String fINALIZEPAYMENTFLAG) {
		FINALIZEPAYMENTFLAG = fINALIZEPAYMENTFLAG;
	}

	public String getCREDITREFNO() {
		return CREDITREFNO;
	}

	public void setCREDITREFNO(String cREDITREFNO) {
		CREDITREFNO = cREDITREFNO;
	}

	public String getBENEFICIARYBANKCITYCODE() {
		return BENEFICIARYBANKCITYCODE;
	}

	public void setBENEFICIARYBANKCITYCODE(String bENEFICIARYBANKCITYCODE) {
		BENEFICIARYBANKCITYCODE = bENEFICIARYBANKCITYCODE;
	}

	public String getORGANIZATIONDIRNAME() {
		return ORGANIZATIONDIRNAME;
	}

	public void setORGANIZATIONDIRNAME(String oRGANIZATIONDIRNAME) {
		ORGANIZATIONDIRNAME = oRGANIZATIONDIRNAME;
	}

	public String getPURPOSEOFTRX() {
		return PURPOSEOFTRX;
	}

	public void setPURPOSEOFTRX(String pURPOSEOFTRX) {
		PURPOSEOFTRX = pURPOSEOFTRX;
	}

	public String getREMITTANCECODE1() {
		return REMITTANCECODE1;
	}

	public void setREMITTANCECODE1(String rEMITTANCECODE1) {
		REMITTANCECODE1 = rEMITTANCECODE1;
	}

	public String getREMITTANCEINFO1() {
		return REMITTANCEINFO1;
	}

	public void setREMITTANCEINFO1(String rEMITTANCEINFO1) {
		REMITTANCEINFO1 = rEMITTANCEINFO1;
	}

	public String getREMITTANCECODE2() {
		return REMITTANCECODE2;
	}

	public void setREMITTANCECODE2(String rEMITTANCECODE2) {
		REMITTANCECODE2 = rEMITTANCECODE2;
	}

	public String getREMITTANCEINFO2() {
		return REMITTANCEINFO2;
	}

	public void setREMITTANCEINFO2(String rEMITTANCEINFO2) {
		REMITTANCEINFO2 = rEMITTANCEINFO2;
	}

	public String getREMITTANCECODE3() {
		return REMITTANCECODE3;
	}

	public void setREMITTANCECODE3(String rEMITTANCECODE3) {
		REMITTANCECODE3 = rEMITTANCECODE3;
	}

	public String getREMITTANCEINFO3() {
		return REMITTANCEINFO3;
	}

	public void setREMITTANCEINFO3(String rEMITTANCEINFO3) {
		REMITTANCEINFO3 = rEMITTANCEINFO3;
	}

	public String getREMITTANCECODE4() {
		return REMITTANCECODE4;
	}

	public void setREMITTANCECODE4(String rEMITTANCECODE4) {
		REMITTANCECODE4 = rEMITTANCECODE4;
	}

	public String getREMITTANCEINFO4() {
		return REMITTANCEINFO4;
	}

	public void setREMITTANCEINFO4(String rEMITTANCEINFO4) {
		REMITTANCEINFO4 = rEMITTANCEINFO4;
	}

	public String getINSTRUCTIONCODE1() {
		return INSTRUCTIONCODE1;
	}

	public void setINSTRUCTIONCODE1(String iNSTRUCTIONCODE1) {
		INSTRUCTIONCODE1 = iNSTRUCTIONCODE1;
	}

	public String getINSTRUCTIONREMARK1() {
		return INSTRUCTIONREMARK1;
	}

	public void setINSTRUCTIONREMARK1(String iNSTRUCTIONREMARK1) {
		INSTRUCTIONREMARK1 = iNSTRUCTIONREMARK1;
	}

	public String getINSTRUCTIONCODE2() {
		return INSTRUCTIONCODE2;
	}

	public void setINSTRUCTIONCODE2(String iNSTRUCTIONCODE2) {
		INSTRUCTIONCODE2 = iNSTRUCTIONCODE2;
	}

	public String getINSTRUCTIONREMARK2() {
		return INSTRUCTIONREMARK2;
	}

	public void setINSTRUCTIONREMARK2(String iNSTRUCTIONREMARK2) {
		INSTRUCTIONREMARK2 = iNSTRUCTIONREMARK2;
	}

	public String getINSTRUCTIONCODE3() {
		return INSTRUCTIONCODE3;
	}

	public void setINSTRUCTIONCODE3(String iNSTRUCTIONCODE3) {
		INSTRUCTIONCODE3 = iNSTRUCTIONCODE3;
	}

	public String getINSTRUCTIONREMARK3() {
		return INSTRUCTIONREMARK3;
	}

	public void setINSTRUCTIONREMARK3(String iNSTRUCTIONREMARK3) {
		INSTRUCTIONREMARK3 = iNSTRUCTIONREMARK3;
	}

	public String getSWIFTMETHOD() {
		return SWIFTMETHOD;
	}

	public void setSWIFTMETHOD(String sWIFTMETHOD) {
		SWIFTMETHOD = sWIFTMETHOD;
	}

	public String getIBTBUYRATE() {
		return IBTBUYRATE;
	}

	public void setIBTBUYRATE(String iBTBUYRATE) {
		IBTBUYRATE = iBTBUYRATE;
	}

	public String getIBTSELLRATE() {
		return IBTSELLRATE;
	}

	public void setIBTSELLRATE(String iBTSELLRATE) {
		IBTSELLRATE = iBTSELLRATE;
	}

	public String getVALUEDATE() {
		return VALUEDATE;
	}

	public void setVALUEDATE(String vALUEDATE) {
		VALUEDATE = vALUEDATE;
	}

	public String getDEBITAMOUNT() {
		return DEBITAMOUNT;
	}

	public void setDEBITAMOUNT(String dEBITAMOUNT) {
		DEBITAMOUNT = dEBITAMOUNT;
	}

	public String getRESERVE1() {
		return RESERVE1;
	}

	public void setRESERVE1(String rESERVE1) {
		RESERVE1 = rESERVE1;
	}

	public String getRESERVE2() {
		return RESERVE2;
	}

	public void setRESERVE2(String rESERVE2) {
		RESERVE2 = rESERVE2;
	}

	public String getRESERVE3() {
		return RESERVE3;
	}

	public void setRESERVE3(String rESERVE3) {
		RESERVE3 = rESERVE3;
	}

	public String getRESERVE4() {
		return RESERVE4;
	}

	public void setRESERVE4(String rESERVE4) {
		RESERVE4 = rESERVE4;
	}

	public String getRESERVE5() {
		return RESERVE5;
	}

	public void setRESERVE5(String rESERVE5) {
		RESERVE5 = rESERVE5;
	}

	public String getSTATUS() {
		return STATUS;
	}

	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}

	public String getLEGSTATUS() {
		return LEGSTATUS;
	}

	public void setLEGSTATUS(String lEGSTATUS) {
		LEGSTATUS = lEGSTATUS;
	}

	public String getMVA_DESC() {
		return MVA_DESC;
	}

	public void setMVA_DESC(String mVA_DESC) {
		MVA_DESC = mVA_DESC;
	}

	public String getMVA_STATUS() {
		return MVA_STATUS;
	}

	public void setMVA_STATUS(String mVA_STATUS) {
		MVA_STATUS = mVA_STATUS;
	}

	public Date getCREATE_DT() {
		return CREATE_DT;
	}

	public void setCREATE_DT(Date cREATE_DT) {
		CREATE_DT = cREATE_DT;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			CREATE_DT_STRING = sdf.format(cREATE_DT);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Date getINSERTDT() {
		return INSERTDT;
	}

	public void setINSERTDT(Date iNSERTDT) {
		INSERTDT = iNSERTDT;
	}

	public Date getPROCESSDT() {
		return PROCESSDT;
	}

	public void setPROCESSDT(Date pROCESSDT) {
		PROCESSDT = pROCESSDT;
	}

	public Long getRESPCOUNTCHECKER() {
		return RESPCOUNTCHECKER;
	}

	public void setRESPCOUNTCHECKER(Long rESPCOUNTCHECKER) {
		RESPCOUNTCHECKER = rESPCOUNTCHECKER;
	}

	public String getERRMESSAGE() {
		return ERRMESSAGE;
	}

	public void setERRMESSAGE(String eRRMESSAGE) {
		ERRMESSAGE = eRRMESSAGE;
	}

	public String getSORBOR_MARK() {
		return SORBOR_MARK;
	}

	public void setSORBOR_MARK(String sORBOR_MARK) {
		SORBOR_MARK = sORBOR_MARK;
	}

	public String getEXEC_STATUS() {
		return EXEC_STATUS;
	}

	public void setEXEC_STATUS(String eXEC_STATUS) {
		EXEC_STATUS = eXEC_STATUS;
	}

	public String getCREATE_DT_STRING() {
		return CREATE_DT_STRING;
	}

	public void setCREATE_DT_STRING(String cREATE_DT_STRING) {
		CREATE_DT_STRING = cREATE_DT_STRING;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			CREATE_DT = sdf.parse(CREATE_DT_STRING);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	
	
}
