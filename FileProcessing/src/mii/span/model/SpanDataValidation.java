package mii.span.model;

import java.io.Serializable;
import java.util.Date;

public class SpanDataValidation implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String fileName,
		batchId,
		trxHeaderId,
		spanFnType,
		debitAccount,
		debitAccountType,
		documentDate,
		totalAmount,
		totalRecord,
		procStatuc,
		procDescription,
		xmlFileName,
		responseCode,
		ackFileName,
		ackStatus,
		ackDescription,
		retryStatus,
		returStatus,
		trxType,
		scFileName,
		scTrxDetailIdStart,
		scTrxDetailIdEnd,
		asynchronous,
		scSettlement,
		payrollType,
		glAccount,
		scDate,
		forcedAck,
		l1Status,
		l2Status,
		ftpStatus,
		changeStatus,
		totalSendToHost,
		lateUpdateFlagging,
		xmlVoidFileName,
		voidFlag,
		rejectedFileFlag,
		extractFileFlag,
		execFileFlag,
		totalAmountSendToHost,
		waitStatus,
		xmlRejectedFileName
		;
	
	private int retryNumberL1,
		retryNumberL2,
		legStatus
		;
	
	private Date createDate;
	private Date updateDate;
	
	/**NON DB**/
	private String createDateString;
	private String updateDateString;
	
	/**
	 * Constructor
	 */
	public SpanDataValidation(String fileName) {
		super();
		this.fileName = fileName;
	}
	
	public SpanDataValidation(String fileName, String xmlFileName) {
		super();
		this.fileName = fileName;
		this.xmlFileName = xmlFileName;
	}
	
	public SpanDataValidation() {
		// TODO Auto-generated constructor stub
	}

	/**GETTER SETTER**/
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getBatchId() {
		return batchId;
	}
	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}
	public String getTrxHeaderId() {
		return trxHeaderId;
	}
	public void setTrxHeaderId(String trxHeaderId) {
		this.trxHeaderId = trxHeaderId;
	}
	public String getSpanFnType() {
		return spanFnType;
	}
	public void setSpanFnType(String spanFnType) {
		this.spanFnType = spanFnType;
	}
	public String getDebitAccount() {
		return debitAccount;
	}
	public void setDebitAccount(String debitAccount) {
		this.debitAccount = debitAccount;
	}
	public String getDebitAccountType() {
		return debitAccountType;
	}
	public void setDebitAccountType(String debitAccountType) {
		this.debitAccountType = debitAccountType;
	}
	public String getDocumentDate() {
		return documentDate;
	}
	public void setDocumentDate(String documentDate) {
		this.documentDate = documentDate;
	}
	public String getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}
	public String getTotalRecord() {
		return totalRecord;
	}
	public void setTotalRecord(String totalRecord) {
		this.totalRecord = totalRecord;
	}
	public String getProcStatuc() {
		return procStatuc;
	}
	public void setProcStatuc(String procStatuc) {
		this.procStatuc = procStatuc;
	}
	public String getProcDescription() {
		return procDescription;
	}
	public void setProcDescription(String procDescription) {
		this.procDescription = procDescription;
	}
	public String getXmlFileName() {
		return xmlFileName;
	}
	public void setXmlFileName(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getAckFileName() {
		return ackFileName;
	}
	public void setAckFileName(String ackFileName) {
		this.ackFileName = ackFileName;
	}
	public String getAckStatus() {
		return ackStatus;
	}
	public void setAckStatus(String ackStatus) {
		this.ackStatus = ackStatus;
	}
	public String getAckDescription() {
		return ackDescription;
	}
	public void setAckDescription(String ackDescription) {
		this.ackDescription = ackDescription;
	}
	public String getRetryStatus() {
		return retryStatus;
	}
	public void setRetryStatus(String retryStatus) {
		this.retryStatus = retryStatus;
	}
	public String getReturStatus() {
		return returStatus;
	}
	public void setReturStatus(String returStatus) {
		this.returStatus = returStatus;
	}
	public String getTrxType() {
		return trxType;
	}
	public void setTrxType(String trxType) {
		this.trxType = trxType;
	}
	public String getScFileName() {
		return scFileName;
	}
	public void setScFileName(String scFileName) {
		this.scFileName = scFileName;
	}
	public String getScTrxDetailIdStart() {
		return scTrxDetailIdStart;
	}
	public void setScTrxDetailIdStart(String scTrxDetailIdStart) {
		this.scTrxDetailIdStart = scTrxDetailIdStart;
	}
	public String getScTrxDetailIdEnd() {
		return scTrxDetailIdEnd;
	}
	public void setScTrxDetailIdEnd(String scTrxDetailIdEnd) {
		this.scTrxDetailIdEnd = scTrxDetailIdEnd;
	}
	public String getAsynchronous() {
		return asynchronous;
	}
	public void setAsynchronous(String asynchronous) {
		this.asynchronous = asynchronous;
	}
	public String getScSettlement() {
		return scSettlement;
	}
	public void setScSettlement(String scSettlement) {
		this.scSettlement = scSettlement;
	}
	public String getPayrollType() {
		return payrollType;
	}
	public void setPayrollType(String payrollType) {
		this.payrollType = payrollType;
	}
	public String getGlAccount() {
		return glAccount;
	}
	public void setGlAccount(String glAccount) {
		this.glAccount = glAccount;
	}
	public String getScDate() {
		return scDate;
	}
	public void setScDate(String scDate) {
		this.scDate = scDate;
	}
	public String getForcedAck() {
		return forcedAck;
	}
	public void setForcedAck(String forcedAck) {
		this.forcedAck = forcedAck;
	}
	public String getL1Status() {
		return l1Status;
	}
	public void setL1Status(String l1Status) {
		this.l1Status = l1Status;
	}
	public String getL2Status() {
		return l2Status;
	}
	public void setL2Status(String l2Status) {
		this.l2Status = l2Status;
	}
	public String getFtpStatus() {
		return ftpStatus;
	}
	public void setFtpStatus(String ftpStatus) {
		this.ftpStatus = ftpStatus;
	}
	public String getChangeStatus() {
		return changeStatus;
	}
	public void setChangeStatus(String changeStatus) {
		this.changeStatus = changeStatus;
	}
	public String getTotalSendToHost() {
		return totalSendToHost;
	}
	public void setTotalSendToHost(String totalSendToHost) {
		this.totalSendToHost = totalSendToHost;
	}
	public String getLateUpdateFlagging() {
		return lateUpdateFlagging;
	}
	public void setLateUpdateFlagging(String lateUpdateFlagging) {
		this.lateUpdateFlagging = lateUpdateFlagging;
	}
	public String getXmlVoidFileName() {
		return xmlVoidFileName;
	}
	public void setXmlVoidFileName(String xmlVoidFileName) {
		this.xmlVoidFileName = xmlVoidFileName;
	}
	public String getVoidFlag() {
		return voidFlag;
	}
	public void setVoidFlag(String voidFlag) {
		this.voidFlag = voidFlag;
	}
	public String getRejectedFileFlag() {
		return rejectedFileFlag;
	}
	public void setRejectedFileFlag(String rejectedFileFlag) {
		this.rejectedFileFlag = rejectedFileFlag;
	}
	public String getExtractFileFlag() {
		return extractFileFlag;
	}
	public void setExtractFileFlag(String extractFileFlag) {
		this.extractFileFlag = extractFileFlag;
	}
	public String getExecFileFlag() {
		return execFileFlag;
	}
	public void setExecFileFlag(String execFileFlag) {
		this.execFileFlag = execFileFlag;
	}
	public String getTotalAmountSendToHost() {
		return totalAmountSendToHost;
	}
	public void setTotalAmountSendToHost(String totalAmountSendToHost) {
		this.totalAmountSendToHost = totalAmountSendToHost;
	}
	public String getWaitStatus() {
		return waitStatus;
	}
	public void setWaitStatus(String waitStatus) {
		this.waitStatus = waitStatus;
	}
	public String getXmlRejectedFileName() {
		return xmlRejectedFileName;
	}
	public void setXmlRejectedFileName(String xmlRejectedFileName) {
		this.xmlRejectedFileName = xmlRejectedFileName;
	}
	public int getRetryNumberL1() {
		return retryNumberL1;
	}
	public void setRetryNumberL1(int retryNumberL1) {
		this.retryNumberL1 = retryNumberL1;
	}
	public int getRetryNumberL2() {
		return retryNumberL2;
	}
	public void setRetryNumberL2(int retryNumberL2) {
		this.retryNumberL2 = retryNumberL2;
	}
	public int getLegStatus() {
		return legStatus;
	}
	public void setLegStatus(int legStatus) {
		this.legStatus = legStatus;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public String getCreateDateString() {
		return createDateString;
	}
	public void setCreateDateString(String createDateString) {
		this.createDateString = createDateString;
	}
	public String getUpdateDateString() {
		return updateDateString;
	}
	public void setUpdateDateString(String updateDateString) {
		this.updateDateString = updateDateString;
	}
}
