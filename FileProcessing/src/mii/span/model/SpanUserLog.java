package mii.span.model;

import java.io.Serializable;
import java.util.Date;

public class SpanUserLog implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long USER_LOG_ID;
	private String USER_ACTION, USERID, DESCRIPTION;
	private Date CREATE_DATE;
	
	public Long getUSER_LOG_ID() {
		return USER_LOG_ID;
	}
	public void setUSER_LOG_ID(Long uSER_LOG_ID) {
		USER_LOG_ID = uSER_LOG_ID;
	}
	public String getUSER_ACTION() {
		return USER_ACTION;
	}
	public void setUSER_ACTION(String uSER_ACTION) {
		USER_ACTION = uSER_ACTION;
	}
	public String getUSERID() {
		return USERID;
	}
	public void setUSERID(String uSERID) {
		USERID = uSERID;
	}
	public String getDESCRIPTION() {
		return DESCRIPTION;
	}
	public void setDESCRIPTION(String dESCRIPTION) {
		DESCRIPTION = dESCRIPTION;
	}
	public Date getCREATE_DATE() {
		return CREATE_DATE;
	}
	public void setCREATE_DATE(Date cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
