package mii.span.model;

import java.io.Serializable;
import java.util.Date;

public class SpanPenihilanLog implements Serializable {
	private static final long serialVersionUID = 1L;

	private String tanggalPenihilan,
		tanggalSp2d,
		referenceNo,
		amount,
		creditAcc,
		sequencePenihilan,
		responseCode,
		procStatus,
		bsStatus,
		narasi;
		
	private Date createDate, updateDate;

	public String getTanggalPenihilan() {
		return tanggalPenihilan;
	}

	public void setTanggalPenihilan(String tanggalPenihilan) {
		this.tanggalPenihilan = tanggalPenihilan;
	}

	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getCreditAcc() {
		return creditAcc;
	}

	public void setCreditAcc(String creditAcc) {
		this.creditAcc = creditAcc;
	}

	public String getSequencePenihilan() {
		return sequencePenihilan;
	}

	public void setSequencePenihilan(String sequencePenihilan) {
		this.sequencePenihilan = sequencePenihilan;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getProcStatus() {
		return procStatus;
	}

	public void setProcStatus(String procStatus) {
		this.procStatus = procStatus;
	}

	public String getBsStatus() {
		return bsStatus;
	}

	public void setBsStatus(String bsStatus) {
		this.bsStatus = bsStatus;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getTanggalSp2d() {
		return tanggalSp2d;
	}

	public void setTanggalSp2d(String tanggalSp2d) {
		this.tanggalSp2d = tanggalSp2d;
	}

	public String getNarasi() {
		return narasi;
	}

	public void setNarasi(String narasi) {
		this.narasi = narasi;
	}
}
