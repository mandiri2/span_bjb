package mii.span.model;

public class FileRejectedInput {
	private String fileName;
	private String documentNo;
	private String errorMessage;
	
	public FileRejectedInput(String fileName, String documentNo) {
		// TODO Auto-generated constructor stub
		this.fileName = fileName;
		this.documentNo = documentNo;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getDocumentNo() {
		return documentNo;
	}

	public void setDocumentNo(String documentNo) {
		this.documentNo = documentNo;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	
}
