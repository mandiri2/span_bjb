package mii.span.model;

public class ClientParams {
	private String clientID = "";
	private String bankCD = "";
	private String sknCD ="";
	private String cityCD = "";
	private String rtgsCD = "";
	private String swiftCD = "";
	private String bicKEY = "";
	private String bankNameType= "";
	private String bankName="";
	private String bicSKN="";
	
	public String getClientID() {
		return clientID;
	}
	public void setClientID(String clientID) {
		this.clientID = clientID;
	}
	public String getBankCD() {
		return bankCD;
	}
	public void setBankCD(String bankCD) {
		this.bankCD = bankCD;
	}
	public String getSknCD() {
		return sknCD;
	}
	public void setSknCD(String sknCD) {
		this.sknCD = sknCD;
	}
	public String getCityCD() {
		return cityCD;
	}
	public void setCityCD(String cityCD) {
		this.cityCD = cityCD;
	}
	public String getRtgsCD() {
		return rtgsCD;
	}
	public void setRtgsCD(String rtgsCD) {
		this.rtgsCD = rtgsCD;
	}
	public String getSwiftCD() {
		return swiftCD;
	}
	public void setSwiftCD(String swiftCD) {
		this.swiftCD = swiftCD;
	}
	public String getBicKEY() {
		return bicKEY;
	}
	public void setBicKEY(String bicKEY) {
		this.bicKEY = bicKEY;
	}
	public String getBankNameType() {
		return bankNameType;
	}
	public void setBankNameType(String bankNameType) {
		this.bankNameType = bankNameType;
	}
	public String getBicSKN() {
		return bicSKN;
	}
	public void setBicSKN(String bicSKN) {
		this.bicSKN = bicSKN;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	
}
