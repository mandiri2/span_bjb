package mii.span.model;

import java.io.Serializable;
import java.util.Date;

public class SpanDataValidationSP2DNo implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String FILE_NAME,
	SP2DNO,
	DEBIT_ACCOUNT_NO,
	DEBIT_ACCOUNT_TYPE,
	DOCUMENT_DATE,
	TOTAL_AMOUNT,
	TOTAL_RECORD,
	PROC_STATUS_FILE,
	PROC_STATUS_SP2DNO,
	DESCRIPTION,
	DESCRIPTION_OF_UPDATE,
	VOID_FLAG;
	
	private Date CREATE_DATE, UPDATE_DATE;
	
	public String getFILE_NAME() {
		return FILE_NAME;
	}

	public void setFILE_NAME(String fILE_NAME) {
		FILE_NAME = fILE_NAME;
	}

	public String getSP2DNO() {
		return SP2DNO;
	}

	public void setSP2DNO(String sP2DNO) {
		SP2DNO = sP2DNO;
	}

	public String getDEBIT_ACCOUNT_NO() {
		return DEBIT_ACCOUNT_NO;
	}

	public void setDEBIT_ACCOUNT_NO(String dEBIT_ACCOUNT_NO) {
		DEBIT_ACCOUNT_NO = dEBIT_ACCOUNT_NO;
	}

	public String getDEBIT_ACCOUNT_TYPE() {
		return DEBIT_ACCOUNT_TYPE;
	}

	public void setDEBIT_ACCOUNT_TYPE(String dEBIT_ACCOUNT_TYPE) {
		DEBIT_ACCOUNT_TYPE = dEBIT_ACCOUNT_TYPE;
	}

	public String getDOCUMENT_DATE() {
		return DOCUMENT_DATE;
	}

	public void setDOCUMENT_DATE(String dOCUMENT_DATE) {
		DOCUMENT_DATE = dOCUMENT_DATE;
	}

	public String getTOTAL_AMOUNT() {
		return TOTAL_AMOUNT;
	}

	public void setTOTAL_AMOUNT(String tOTAL_AMOUNT) {
		TOTAL_AMOUNT = tOTAL_AMOUNT;
	}

	public String getTOTAL_RECORD() {
		return TOTAL_RECORD;
	}

	public void setTOTAL_RECORD(String tOTAL_RECORD) {
		TOTAL_RECORD = tOTAL_RECORD;
	}

	public String getPROC_STATUS_FILE() {
		return PROC_STATUS_FILE;
	}

	public void setPROC_STATUS_FILE(String pROC_STATUS_FILE) {
		PROC_STATUS_FILE = pROC_STATUS_FILE;
	}

	public String getPROC_STATUS_SP2DNO() {
		return PROC_STATUS_SP2DNO;
	}

	public void setPROC_STATUS_SP2DNO(String pROC_STATUS_SP2DNO) {
		PROC_STATUS_SP2DNO = pROC_STATUS_SP2DNO;
	}

	public String getDESCRIPTION() {
		return DESCRIPTION;
	}

	public void setDESCRIPTION(String dESCRIPTION) {
		DESCRIPTION = dESCRIPTION;
	}

	public String getDESCRIPTION_OF_UPDATE() {
		return DESCRIPTION_OF_UPDATE;
	}

	public void setDESCRIPTION_OF_UPDATE(String dESCRIPTION_OF_UPDATE) {
		DESCRIPTION_OF_UPDATE = dESCRIPTION_OF_UPDATE;
	}

	public String getVOID_FLAG() {
		return VOID_FLAG;
	}

	public void setVOID_FLAG(String vOID_FLAG) {
		VOID_FLAG = vOID_FLAG;
	}

	public Date getCREATE_DATE() {
		return CREATE_DATE;
	}

	public void setCREATE_DATE(Date cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}

	public Date getUPDATE_DATE() {
		return UPDATE_DATE;
	}

	public void setUPDATE_DATE(Date uPDATE_DATE) {
		UPDATE_DATE = uPDATE_DATE;
	}
}
