package mii.span.model;

import java.io.Serializable;
import java.util.Date;

public class SpanGetFiles implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String filename, 
		description;
	
	private Long totalRetry;
	private Date createDate, updateDate;
	
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getTotalRetry() {
		return totalRetry;
	}
	public void setTotalRetry(Long totalRetry) {
		this.totalRetry = totalRetry;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
}
