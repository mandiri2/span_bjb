package mii.span.model;

import com.mii.constant.Constants;

public class FASTGapuraAccount {

	private String count;
	private String branch;
	private String isValid;
	private String descriptionDetail;
	
	public FASTGapuraAccount() {}
	
	public FASTGapuraAccount(String count, String branch) {
		this.count = count;
		this.branch = branch;
		if (Integer.parseInt(count) > 0)
			this.isValid = Constants.FAST_FOUND;
		else
			this.isValid = Constants.FAST_NOT_FOUND;
	}

	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getIsValid() {
		return isValid;
	}

	public void setIsValid(String isValid) {
		this.isValid = isValid;
	}

	public String getDescriptionDetail() {
		return descriptionDetail;
	}

	public void setDescriptionDetail(String descriptionDetail) {
		this.descriptionDetail = descriptionDetail;
	}

}
