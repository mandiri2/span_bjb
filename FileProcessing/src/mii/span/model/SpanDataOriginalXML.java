package mii.span.model;

import java.util.Date;

public class SpanDataOriginalXML {
	private String FILE_NAME,
	DOCUMENT_DATE,
	TOTAL_RECORD,
	DEBIT_ACCOUNT_NO,
	DEBIT_ACCOUNT_TYPE,
	XML_FILE_NAME,
	DESCRIPTION;
	
	private Date CREATE_DATE; 
	
	public String getFILE_NAME() {
		return FILE_NAME;
	}

	public void setFILE_NAME(String fILE_NAME) {
		FILE_NAME = fILE_NAME;
	}

	public String getDOCUMENT_DATE() {
		return DOCUMENT_DATE;
	}

	public void setDOCUMENT_DATE(String dOCUMENT_DATE) {
		DOCUMENT_DATE = dOCUMENT_DATE;
	}

	public String getTOTAL_RECORD() {
		return TOTAL_RECORD;
	}

	public void setTOTAL_RECORD(String tOTAL_RECORD) {
		TOTAL_RECORD = tOTAL_RECORD;
	}

	public String getDEBIT_ACCOUNT_NO() {
		return DEBIT_ACCOUNT_NO;
	}

	public void setDEBIT_ACCOUNT_NO(String dEBIT_ACCOUNT_NO) {
		DEBIT_ACCOUNT_NO = dEBIT_ACCOUNT_NO;
	}

	public String getDEBIT_ACCOUNT_TYPE() {
		return DEBIT_ACCOUNT_TYPE;
	}

	public void setDEBIT_ACCOUNT_TYPE(String dEBIT_ACCOUNT_TYPE) {
		DEBIT_ACCOUNT_TYPE = dEBIT_ACCOUNT_TYPE;
	}

	public String getXML_FILE_NAME() {
		return XML_FILE_NAME;
	}

	public void setXML_FILE_NAME(String xML_FILE_NAME) {
		XML_FILE_NAME = xML_FILE_NAME;
	}

	public String getDESCRIPTION() {
		return DESCRIPTION;
	}

	public void setDESCRIPTION(String dESCRIPTION) {
		DESCRIPTION = dESCRIPTION;
	}

	public Date getCREATE_DATE() {
		return CREATE_DATE;
	}

	public void setCREATE_DATE(Date cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}
}
