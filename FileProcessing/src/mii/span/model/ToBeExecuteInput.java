package mii.span.model;

import java.util.List;

import mii.span.model.helper.Files;

public class ToBeExecuteInput {
	private List<Files> files;
	private String trxType;
	private String exeType;
	private String account;
	private String selectedAmount;
	
	public String getTrxType() {
		return trxType;
	}

	public void setTrxType(String trxType) {
		this.trxType = trxType;
	}

	public String getExeType() {
		return exeType;
	}

	public void setExeType(String exeType) {
		this.exeType = exeType;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getSelectedAmount() {
		return selectedAmount;
	}

	public void setSelectedAmount(String selectedAmount) {
		this.selectedAmount = selectedAmount;
	}

	public List<Files> getFiles() {
		return files;
	}

	public void setFiles(List<Files> files) {
		this.files = files;
	}

}
