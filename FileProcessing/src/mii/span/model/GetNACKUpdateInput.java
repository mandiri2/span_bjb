package mii.span.model;

import java.util.List;

import mii.span.model.helper.Files;

public class GetNACKUpdateInput {

	private String providerCode;
	private List<Files> files;

	public String getProviderCode() {
		return providerCode;
	}

	public void setProviderCode(String providerCode) {
		this.providerCode = providerCode;
	}

	public List<Files> getFiles() {
		return files;
	}

	public void setFiles(List<Files> files) {
		this.files = files;
	}

}
