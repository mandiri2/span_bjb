package mii.span.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class THQEQBJBModel implements Serializable{
	private static final long serialVersionUID = 1L;
	/*
	 * Model for table YTHSPNPF
	 * Schema USRBASENEW
	 * EQ-TH-BJB
	 * @author : Frumentius Brian
	 */
	private BigDecimal yID;
	private String yPDATE;
	private String yTCD;
	private String yAMT1;
	private String yAMT2;
	private String yREFF;
	private String yAMT3;
	private String yREK;
	
	public String getyPDATE() {
		return yPDATE;
	}
	public void setyPDATE(String yPDATE) {
		this.yPDATE = yPDATE;
	}
	public String getyTCD() {
		return yTCD;
	}
	public void setyTCD(String yTCD) {
		this.yTCD = yTCD;
	}
	public String getyAMT1() {
		return yAMT1;
	}
	public void setyAMT1(String yAMT1) {
		this.yAMT1 = yAMT1;
	}
	public String getyAMT2() {
		return yAMT2;
	}
	public void setyAMT2(String yAMT2) {
		this.yAMT2 = yAMT2;
	}
	public String getyREFF() {
		return yREFF;
	}
	public void setyREFF(String yREFF) {
		this.yREFF = yREFF;
	}
	public String getyAMT3() {
		return yAMT3;
	}
	public void setyAMT3(String yAMT3) {
		this.yAMT3 = yAMT3;
	}
	public String getyREK() {
		return yREK;
	}
	public void setyREK(String yREK) {
		this.yREK = yREK;
	}
	public BigDecimal getyID() {
		return yID;
	}
	public void setyID(BigDecimal yID) {
		this.yID = yID;
	}
}
