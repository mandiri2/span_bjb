package mii.span.model;

import java.io.Serializable;

public class BulkFTReqPRC implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String batchid;
	private String trxheaderid;
	private String trxdetailid;
	private String debitaccno;
	private String debitacccurrcode;
	private String debitamount;
	private String chargeinstruction;
	private String creditaccno;
	private String creditaccname;
	private String credittrfcurr;
	private String credittrfamount;
	private String trxremark1;
	private String trxremark2;
	private String trxremark3;
	private String trxremark4;
	private String ftservices;
	private String beneficiarybankcode;
	private String beneficiarybankname;
	private String debitrefno;
	private String creditrefno;
	private String organizationdirname;
	private String swiftmethod;
	private String reserve1;
	private String reserve2;
	private String reserve3;
	private String reserve4;
	private String reserve5;
	private String instructioncode1;
	private String legstatus;
	private String insertdt;
	private String prcstatus;
	private String retryCount;
	
	//helper
	private String documentNumber;
	private String documentDate;
	private String traceNumber;
	private String paymentMethod;
	
	public BulkFTReqPRC() {
	}
	
	public BulkFTReqPRC(String batchid, String trxheaderid, String trxdetailid) {
		this.batchid = batchid;
		this.trxheaderid = trxheaderid;
		this.trxdetailid = trxdetailid;
	}
	
	public BulkFTReqPRC(String batchid, String trxheaderid, String trxdetailid,
			String debitaccno, String debitacccurrcode, String debitamount,
			String chargeinstruction, String creditaccno, String creditaccname,
			String credittrfcurr, String credittrfamount, String trxremark1,
			String trxremark2, String trxremark3, String trxremark4,
			String ftservices, String beneficiarybankcode,
			String beneficiarybankname, String debitrefno, String creditrefno,
			String organizationdirname, String swiftmethod, String reserve1,
			String reserve2, String reserve3, String reserve4, String reserve5,
			String instructioncode, String legstatus, String insertdt, String prcstatus,
			String documentNumber, String documentDate, String retryCount, String traceNumber, String paymentMethod) {
		this.batchid = batchid;
		this.trxheaderid = trxheaderid;
		this.trxdetailid = trxdetailid;
		this.debitaccno = debitaccno;
		this.debitacccurrcode = debitacccurrcode;
		this.debitamount = debitamount;
		this.chargeinstruction = chargeinstruction;
		this.creditaccno = creditaccno;
		this.creditaccname = creditaccname;
		this.credittrfcurr = credittrfcurr;
		this.credittrfamount = credittrfamount;
		this.trxremark1 = trxremark1;
		this.trxremark2 = trxremark2;
		this.trxremark3 = trxremark3;
		this.trxremark4 = trxremark4;
		this.ftservices = ftservices;
		this.beneficiarybankcode = beneficiarybankcode;
		this.beneficiarybankname = beneficiarybankname;
		this.debitrefno = debitrefno;
		this.creditrefno = creditrefno;
		this.organizationdirname = organizationdirname;
		this.swiftmethod = swiftmethod;
		this.reserve1 = reserve1;
		this.reserve2 = reserve2;
		this.reserve3 = reserve3;
		this.reserve4 = reserve4;
		this.reserve5 = reserve5;
		this.instructioncode1 = instructioncode;
		this.legstatus = legstatus;
		this.insertdt = insertdt;
		this.prcstatus = prcstatus;
		this.documentNumber = documentNumber;
		this.documentDate = documentDate;
		this.setRetryCount(retryCount);
		this.traceNumber = traceNumber;
		this.paymentMethod = paymentMethod;
	}
	
	public BulkFTReqPRC(String batchid, String trxheaderid, String trxdetailid,
			String debitaccno, String debitacccurrcode, String debitamount,
			String chargeinstruction, String creditaccno, String creditaccname,
			String credittrfcurr, String credittrfamount, String trxremark1,
			String trxremark2, String trxremark3, String trxremark4,
			String ftservices, String beneficiarybankcode,
			String beneficiarybankname, String debitrefno, String creditrefno,
			String organizationdirname, String swiftmethod, String reserve1,
			String reserve2, String reserve3, String reserve4, String reserve5,
			String instructioncode, String legstatus, String insertdt, String prcstatus,
			String documentNumber, String documentDate, String retryCount, String traceNumber) {
		this.batchid = batchid;
		this.trxheaderid = trxheaderid;
		this.trxdetailid = trxdetailid;
		this.debitaccno = debitaccno;
		this.debitacccurrcode = debitacccurrcode;
		this.debitamount = debitamount;
		this.chargeinstruction = chargeinstruction;
		this.creditaccno = creditaccno;
		this.creditaccname = creditaccname;
		this.credittrfcurr = credittrfcurr;
		this.credittrfamount = credittrfamount;
		this.trxremark1 = trxremark1;
		this.trxremark2 = trxremark2;
		this.trxremark3 = trxremark3;
		this.trxremark4 = trxremark4;
		this.ftservices = ftservices;
		this.beneficiarybankcode = beneficiarybankcode;
		this.beneficiarybankname = beneficiarybankname;
		this.debitrefno = debitrefno;
		this.creditrefno = creditrefno;
		this.organizationdirname = organizationdirname;
		this.swiftmethod = swiftmethod;
		this.reserve1 = reserve1;
		this.reserve2 = reserve2;
		this.reserve3 = reserve3;
		this.reserve4 = reserve4;
		this.reserve5 = reserve5;
		this.instructioncode1 = instructioncode;
		this.legstatus = legstatus;
		this.insertdt = insertdt;
		this.prcstatus = prcstatus;
		this.documentNumber = documentNumber;
		this.documentDate = documentDate;
		this.setRetryCount(retryCount);
		this.traceNumber = traceNumber;
	}
	
	public String getBatchid() {
		return batchid;
	}

	public void setBatchid(String batchid) {
		this.batchid = batchid;
	}

	public String getTrxheaderid() {
		return trxheaderid;
	}

	public void setTrxheaderid(String trxheaderid) {
		this.trxheaderid = trxheaderid;
	}

	public String getTrxdetailid() {
		return trxdetailid;
	}

	public void setTrxdetailid(String trxdetailid) {
		this.trxdetailid = trxdetailid;
	}

	public String getDebitaccno() {
		return debitaccno;
	}

	public void setDebitaccno(String debitaccno) {
		this.debitaccno = debitaccno;
	}

	public String getDebitacccurrcode() {
		return debitacccurrcode;
	}

	public void setDebitacccurrcode(String debitacccurrcode) {
		this.debitacccurrcode = debitacccurrcode;
	}

	public String getDebitamount() {
		return debitamount;
	}

	public void setDebitamount(String debitamount) {
		this.debitamount = debitamount;
	}

	public String getChargeinstruction() {
		return chargeinstruction;
	}

	public void setChargeinstruction(String chargeinstruction) {
		this.chargeinstruction = chargeinstruction;
	}

	public String getCreditaccno() {
		return creditaccno;
	}

	public void setCreditaccno(String creditaccno) {
		this.creditaccno = creditaccno;
	}

	public String getCreditaccname() {
		return creditaccname;
	}

	public void setCreditaccname(String creditaccname) {
		this.creditaccname = creditaccname;
	}

	public String getCredittrfcurr() {
		return credittrfcurr;
	}

	public void setCredittrfcurr(String credittrfcurr) {
		this.credittrfcurr = credittrfcurr;
	}

	public String getCredittrfamount() {
		return credittrfamount;
	}

	public void setCredittrfamount(String credittrfamount) {
		this.credittrfamount = credittrfamount;
	}

	public String getTrxremark1() {
		return trxremark1;
	}

	public void setTrxremark1(String trxremark1) {
		this.trxremark1 = trxremark1;
	}

	public String getTrxremark2() {
		return trxremark2;
	}

	public void setTrxremark2(String trxremark2) {
		this.trxremark2 = trxremark2;
	}

	public String getTrxremark3() {
		return trxremark3;
	}

	public void setTrxremark3(String trxremark3) {
		this.trxremark3 = trxremark3;
	}

	public String getTrxremark4() {
		return trxremark4;
	}

	public void setTrxremark4(String trxremark4) {
		this.trxremark4 = trxremark4;
	}

	public String getFtservices() {
		return ftservices;
	}

	public void setFtservices(String ftservices) {
		this.ftservices = ftservices;
	}

	public String getBeneficiarybankcode() {
		return beneficiarybankcode;
	}

	public void setBeneficiarybankcode(String beneficiarybankcode) {
		this.beneficiarybankcode = beneficiarybankcode;
	}

	public String getBeneficiarybankname() {
		return beneficiarybankname;
	}

	public void setBeneficiarybankname(String beneficiarybankname) {
		this.beneficiarybankname = beneficiarybankname;
	}

	public String getDebitrefno() {
		return debitrefno;
	}

	public void setDebitrefno(String debitrefno) {
		this.debitrefno = debitrefno;
	}

	public String getCreditrefno() {
		return creditrefno;
	}

	public void setCreditrefno(String creditrefno) {
		this.creditrefno = creditrefno;
	}

	public String getOrganizationdirname() {
		return organizationdirname;
	}

	public void setOrganizationdirname(String organizationdirname) {
		this.organizationdirname = organizationdirname;
	}

	public String getSwiftmethod() {
		return swiftmethod;
	}

	public void setSwiftmethod(String swiftmethod) {
		this.swiftmethod = swiftmethod;
	}

	public String getReserve1() {
		return reserve1;
	}

	public void setReserve1(String reserve1) {
		this.reserve1 = reserve1;
	}

	public String getReserve2() {
		return reserve2;
	}

	public void setReserve2(String reserve2) {
		this.reserve2 = reserve2;
	}

	public String getReserve3() {
		return reserve3;
	}

	public void setReserve3(String reserve3) {
		this.reserve3 = reserve3;
	}

	public String getReserve4() {
		return reserve4;
	}

	public void setReserve4(String reserve4) {
		this.reserve4 = reserve4;
	}

	public String getReserve5() {
		return reserve5;
	}

	public void setReserve5(String reserve5) {
		this.reserve5 = reserve5;
	}

	public String getInstructioncode() {
		return instructioncode1;
	}

	public void setInstructioncode(String instructioncode) {
		this.instructioncode1 = instructioncode;
	}

	public String getLegstatus() {
		return legstatus;
	}

	public void setLegstatus(String legstatus) {
		this.legstatus = legstatus;
	}

	public String getInsertdt() {
		return insertdt;
	}

	public void setInsertdt(String insertdt) {
		this.insertdt = insertdt;
	}

	public String getPrcstatus() {
		return prcstatus;
	}

	public void setPrcstatus(String prcstatus) {
		this.prcstatus = prcstatus;
	}

	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	public String getDocumentDate() {
		return documentDate;
	}

	public void setDocumentDate(String documentDate) {
		this.documentDate = documentDate;
	}

	public String getRetryCount() {
		return retryCount;
	}

	public void setRetryCount(String retryCount) {
		this.retryCount = retryCount;
	}

	public String getTraceNumber() {
		return traceNumber;
	}

	public void setTraceNumber(String traceNumber) {
		this.traceNumber = traceNumber;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
}
