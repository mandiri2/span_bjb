package mii.span.model;

import java.io.Serializable;
import java.util.Date;

public class SpanHostDataFailed implements Serializable {
	private static final long serialVersionUID = 1L;

	private String fileName,
		batchId,
		trxHeaderId,	//id1
		procStatus,
		procDescription,
		trxType,
		legStatus;
		
	private Long retryStatus,
		returStatus,
		successStatus,
		totalProcess,
		sumStatusRty,
		sumStatusRtu,
		waitStatus,
		sumStatusWat;
	
	private Date createDate;
	
	/**GETTER SETTER**/
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getBatchId() {
		return batchId;
	}

	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}

	public String getTrxHeaderId() {
		return trxHeaderId;
	}

	public void setTrxHeaderId(String trxHeaderId) {
		this.trxHeaderId = trxHeaderId;
	}

	public String getProcStatus() {
		return procStatus;
	}

	public void setProcStatus(String procStatus) {
		this.procStatus = procStatus;
	}

	public String getProcDescription() {
		return procDescription;
	}

	public void setProcDescription(String procDescription) {
		this.procDescription = procDescription;
	}

	public String getTrxType() {
		return trxType;
	}

	public void setTrxType(String trxType) {
		this.trxType = trxType;
	}

	public String getLegStatus() {
		return legStatus;
	}

	public void setLegStatus(String legStatus) {
		this.legStatus = legStatus;
	}

	public Long getRetryStatus() {
		return retryStatus;
	}

	public void setRetryStatus(Long retryStatus) {
		this.retryStatus = retryStatus;
	}

	public Long getReturStatus() {
		return returStatus;
	}

	public void setReturStatus(Long returStatus) {
		this.returStatus = returStatus;
	}

	public Long getSuccessStatus() {
		return successStatus;
	}

	public void setSuccessStatus(Long successStatus) {
		this.successStatus = successStatus;
	}

	public Long getTotalProcess() {
		return totalProcess;
	}

	public void setTotalProcess(Long totalProcess) {
		this.totalProcess = totalProcess;
	}

	public Long getSumStatusRty() {
		return sumStatusRty;
	}

	public void setSumStatusRty(Long sumStatusRty) {
		this.sumStatusRty = sumStatusRty;
	}

	public Long getSumStatusRtu() {
		return sumStatusRtu;
	}

	public void setSumStatusRtu(Long sumStatusRtu) {
		this.sumStatusRtu = sumStatusRtu;
	}

	public Long getWaitStatus() {
		return waitStatus;
	}

	public void setWaitStatus(Long waitStatus) {
		this.waitStatus = waitStatus;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Long getSumStatusWat() {
		return sumStatusWat;
	}

	public void setSumStatusWat(Long sumStatusWat) {
		this.sumStatusWat = sumStatusWat;
	}
	
	
}
