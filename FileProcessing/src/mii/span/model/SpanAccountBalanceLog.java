package mii.span.model;

import java.io.Serializable;
import java.util.Date;

public class SpanAccountBalanceLog implements Serializable {
	private static final long serialVersionUID = 1L;

	private String inquiryDate;
	private String beginBalance,
		accountType;
	
	public String getInquiryDate() {
		return inquiryDate;
	}
	public void setInquiryDate(String inquiryDate) {
		this.inquiryDate = inquiryDate;
	}

	public String getBeginBalance() {
		return beginBalance;
	}
	public void setBeginBalance(String beginBalance) {
		this.beginBalance = beginBalance;
	}
	public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
}
