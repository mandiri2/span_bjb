package mii.span.model;

public class SpanRejectedFileHis {
	public String FILE_NAME, TOTAL_RECORD, TOTAL_AMOUNT, CREATE_DATE;

	public String getFILE_NAME() {
		return FILE_NAME;
	}

	public void setFILE_NAME(String fILE_NAME) {
		FILE_NAME = fILE_NAME;
	}

	public String getTOTAL_RECORD() {
		return TOTAL_RECORD;
	}

	public void setTOTAL_RECORD(String tOTAL_RECORD) {
		TOTAL_RECORD = tOTAL_RECORD;
	}

	public String getTOTAL_AMOUNT() {
		return TOTAL_AMOUNT;
	}

	public void setTOTAL_AMOUNT(String tOTAL_AMOUNT) {
		TOTAL_AMOUNT = tOTAL_AMOUNT;
	}

	public String getCREATE_DATE() {
		return CREATE_DATE;
	}

	public void setCREATE_DATE(String cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}

}
