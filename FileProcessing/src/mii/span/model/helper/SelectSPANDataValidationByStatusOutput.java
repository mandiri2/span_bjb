package mii.span.model.helper;

public class SelectSPANDataValidationByStatusOutput {
	private String FILE_NAME;
	private String BATCHID;
	private String TRXHEADERID;
	private String TOTAL_RECORD;
	private String SC_FILE_NAME;
	private String SC_TRXDETAILID_START;
	private String SC_TRXDETAILID_END;
	private String TOTAL_AMOUNT;
	private String ASYNCHRONOUS;
	private String LEGSTATUS;
	private String TRXTYPE;
	private String SC_SETTLEMENT;
	private String RETRY_STATUS;
	private String RETUR_STATUS;
	private String L1_STATUS;
	private String L2_STATUS;
	private String DOCUMENT_DATE;
	private String CREATE_DATE;
	private String UPDATE_DATE;
	private String WAIT_STATUS;
	private String TOTAL_AMOUNT_SENDTOHOST;
	private String TOTAL_SENDTOHOST;

	public SelectSPANDataValidationByStatusOutput(String fILE_NAME,
			String bATCHID, String tRXHEADERID, String tOTAL_RECORD,
			String sC_FILE_NAME, String sC_TRXDETAILID_START,
			String sC_TRXDETAILID_END, String tOTAL_AMOUNT,
			String aSYNCHRONOUS, String lEGSTATUS, String tRXTYPE,
			String sC_SETTLEMENT, String rETRY_STATUS, String rETUR_STATUS,
			String l1_STATUS, String l2_STATUS, String dOCUMENT_DATE,
			String cREATE_DATE, String uPDATE_DATE, String wAIT_STATUS,
			String tOTAL_AMOUNT_SENDTOHOST, String tOTAL_SENDTOHOST) {
		super();
		FILE_NAME = fILE_NAME;
		BATCHID = bATCHID;
		TRXHEADERID = tRXHEADERID;
		TOTAL_RECORD = tOTAL_RECORD;
		SC_FILE_NAME = sC_FILE_NAME;
		SC_TRXDETAILID_START = sC_TRXDETAILID_START;
		SC_TRXDETAILID_END = sC_TRXDETAILID_END;
		TOTAL_AMOUNT = tOTAL_AMOUNT;
		ASYNCHRONOUS = aSYNCHRONOUS;
		LEGSTATUS = lEGSTATUS;
		TRXTYPE = tRXTYPE;
		SC_SETTLEMENT = sC_SETTLEMENT;
		RETRY_STATUS = rETRY_STATUS;
		RETUR_STATUS = rETUR_STATUS;
		L1_STATUS = l1_STATUS;
		L2_STATUS = l2_STATUS;
		DOCUMENT_DATE = dOCUMENT_DATE;
		CREATE_DATE = cREATE_DATE;
		UPDATE_DATE = uPDATE_DATE;
		WAIT_STATUS = wAIT_STATUS;
		TOTAL_AMOUNT_SENDTOHOST = tOTAL_AMOUNT_SENDTOHOST;
		TOTAL_SENDTOHOST = tOTAL_SENDTOHOST;
	}

	public String getFILE_NAME() {
		return FILE_NAME;
	}

	public void setFILE_NAME(String fILE_NAME) {
		FILE_NAME = fILE_NAME;
	}

	public String getBATCHID() {
		return BATCHID;
	}

	public void setBATCHID(String bATCHID) {
		BATCHID = bATCHID;
	}

	public String getTRXHEADERID() {
		return TRXHEADERID;
	}

	public void setTRXHEADERID(String tRXHEADERID) {
		TRXHEADERID = tRXHEADERID;
	}

	public String getTOTAL_RECORD() {
		return TOTAL_RECORD;
	}

	public void setTOTAL_RECORD(String tOTAL_RECORD) {
		TOTAL_RECORD = tOTAL_RECORD;
	}

	public String getSC_FILE_NAME() {
		return SC_FILE_NAME;
	}

	public void setSC_FILE_NAME(String sC_FILE_NAME) {
		SC_FILE_NAME = sC_FILE_NAME;
	}

	public String getSC_TRXDETAILID_START() {
		return SC_TRXDETAILID_START;
	}

	public void setSC_TRXDETAILID_START(String sC_TRXDETAILID_START) {
		SC_TRXDETAILID_START = sC_TRXDETAILID_START;
	}

	public String getSC_TRXDETAILID_END() {
		return SC_TRXDETAILID_END;
	}

	public void setSC_TRXDETAILID_END(String sC_TRXDETAILID_END) {
		SC_TRXDETAILID_END = sC_TRXDETAILID_END;
	}

	public String getTOTAL_AMOUNT() {
		return TOTAL_AMOUNT;
	}

	public void setTOTAL_AMOUNT(String tOTAL_AMOUNT) {
		TOTAL_AMOUNT = tOTAL_AMOUNT;
	}

	public String getASYNCHRONOUS() {
		return ASYNCHRONOUS;
	}

	public void setASYNCHRONOUS(String aSYNCHRONOUS) {
		ASYNCHRONOUS = aSYNCHRONOUS;
	}

	public String getLEGSTATUS() {
		return LEGSTATUS;
	}

	public void setLEGSTATUS(String lEGSTATUS) {
		LEGSTATUS = lEGSTATUS;
	}

	public String getTRXTYPE() {
		return TRXTYPE;
	}

	public void setTRXTYPE(String tRXTYPE) {
		TRXTYPE = tRXTYPE;
	}

	public String getSC_SETTLEMENT() {
		return SC_SETTLEMENT;
	}

	public void setSC_SETTLEMENT(String sC_SETTLEMENT) {
		SC_SETTLEMENT = sC_SETTLEMENT;
	}

	public String getRETRY_STATUS() {
		return RETRY_STATUS;
	}

	public void setRETRY_STATUS(String rETRY_STATUS) {
		RETRY_STATUS = rETRY_STATUS;
	}

	public String getRETUR_STATUS() {
		return RETUR_STATUS;
	}

	public void setRETUR_STATUS(String rETUR_STATUS) {
		RETUR_STATUS = rETUR_STATUS;
	}

	public String getL1_STATUS() {
		return L1_STATUS;
	}

	public void setL1_STATUS(String l1_STATUS) {
		L1_STATUS = l1_STATUS;
	}

	public String getL2_STATUS() {
		return L2_STATUS;
	}

	public void setL2_STATUS(String l2_STATUS) {
		L2_STATUS = l2_STATUS;
	}

	public String getDOCUMENT_DATE() {
		return DOCUMENT_DATE;
	}

	public void setDOCUMENT_DATE(String dOCUMENT_DATE) {
		DOCUMENT_DATE = dOCUMENT_DATE;
	}

	public String getCREATE_DATE() {
		return CREATE_DATE;
	}

	public void setCREATE_DATE(String cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}

	public String getUPDATE_DATE() {
		return UPDATE_DATE;
	}

	public void setUPDATE_DATE(String uPDATE_DATE) {
		UPDATE_DATE = uPDATE_DATE;
	}

	public String getWAIT_STATUS() {
		return WAIT_STATUS;
	}

	public void setWAIT_STATUS(String wAIT_STATUS) {
		WAIT_STATUS = wAIT_STATUS;
	}

	public String getTOTAL_AMOUNT_SENDTOHOST() {
		return TOTAL_AMOUNT_SENDTOHOST;
	}

	public void setTOTAL_AMOUNT_SENDTOHOST(String tOTAL_AMOUNT_SENDTOHOST) {
		TOTAL_AMOUNT_SENDTOHOST = tOTAL_AMOUNT_SENDTOHOST;
	}

	public String getTOTAL_SENDTOHOST() {
		return TOTAL_SENDTOHOST;
	}

	public void setTOTAL_SENDTOHOST(String tOTAL_SENDTOHOST) {
		TOTAL_SENDTOHOST = tOTAL_SENDTOHOST;
	}

}
