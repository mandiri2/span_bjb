package mii.span.model.helper;

public class SpanHostResponseOutput {
	private String status;
	private String errorMessage;
	private String stat;
	
	public SpanHostResponseOutput(String status, String errorMessage,
			String stat) {
		super();
		this.status = status;
		this.errorMessage = errorMessage;
		this.stat = stat;
	}

	public SpanHostResponseOutput() {
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getStat() {
		return stat;
	}

	public void setStat(String stat) {
		this.stat = stat;
	}

}
