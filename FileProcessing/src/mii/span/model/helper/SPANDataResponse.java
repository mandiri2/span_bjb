package mii.span.model.helper;

public class SPANDataResponse {
	private String FILE_NAME;
	private String BATCHID;
	private String TRXHEADERID;
	private String TOTAL_RECORD;
	private String SC_FILE_NAME;
	private String SC_TRXDETAILID_START;
	private String SC_TRXDETAILID_END;
	private String TOTAL_AMOUNT;
	private String status;

	public String getFILE_NAME() {
		return FILE_NAME;
	}

	public void setFILE_NAME(String fILE_NAME) {
		FILE_NAME = fILE_NAME;
	}

	public String getBATCHID() {
		return BATCHID;
	}

	public void setBATCHID(String bATCHID) {
		BATCHID = bATCHID;
	}

	public String getTRXHEADERID() {
		return TRXHEADERID;
	}

	public void setTRXHEADERID(String tRXHEADERID) {
		TRXHEADERID = tRXHEADERID;
	}

	public String getTOTAL_RECORD() {
		return TOTAL_RECORD;
	}

	public void setTOTAL_RECORD(String tOTAL_RECORD) {
		TOTAL_RECORD = tOTAL_RECORD;
	}

	public String getSC_FILE_NAME() {
		return SC_FILE_NAME;
	}

	public void setSC_FILE_NAME(String sC_FILE_NAME) {
		SC_FILE_NAME = sC_FILE_NAME;
	}

	public String getSC_TRXDETAILID_START() {
		return SC_TRXDETAILID_START;
	}

	public void setSC_TRXDETAILID_START(String sC_TRXDETAILID_START) {
		SC_TRXDETAILID_START = sC_TRXDETAILID_START;
	}

	public String getSC_TRXDETAILID_END() {
		return SC_TRXDETAILID_END;
	}

	public void setSC_TRXDETAILID_END(String sC_TRXDETAILID_END) {
		SC_TRXDETAILID_END = sC_TRXDETAILID_END;
	}

	public String getTOTAL_AMOUNT() {
		return TOTAL_AMOUNT;
	}

	public void setTOTAL_AMOUNT(String tOTAL_AMOUNT) {
		TOTAL_AMOUNT = tOTAL_AMOUNT;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
