package mii.span.model.helper;

public class ACKStringToBytesOutput {

	private byte[] byteNACK;
	private String Status;
	private String ErrorMessage;

	public byte[] getByteNACK() {
		return byteNACK;
	}

	public void setByteNACK(byte[] byteNACK) {
		this.byteNACK = byteNACK;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public String getErrorMessage() {
		return ErrorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		ErrorMessage = errorMessage;
	}

}
