package mii.span.model.helper;

public class CheckDataFailedOutput {
	private String FILE_NAME;
	private String BATCHID;
	private String TRXHEADERID;
	private String RETRY_STATUS;
	private String RETUR_STATUS;
	private String SUCCESS_STATUS;
	private String PROC_STATUS;
	private String PROC_DESCRIPTION;
	private String TOTAL_PROCESS;
	private String CREATE_DATE;
	private String SUM_STATUS_RTY;
	private String SUM_STATUS_RTU;
	private String TRXTYPE;
	private String LEGSTATUS;
	
	public CheckDataFailedOutput(String fILE_NAME, String bATCHID,
			String tRXHEADERID, String rETRY_STATUS, String rETUR_STATUS,
			String sUCCESS_STATUS, String pROC_STATUS, String pROC_DESCRIPTION,
			String tOTAL_PROCESS, String cREATE_DATE, String sUM_STATUS_RTY,
			String sUM_STATUS_RTU, String tRXTYPE, String lEGSTATUS) {
		super();
		FILE_NAME = fILE_NAME;
		BATCHID = bATCHID;
		TRXHEADERID = tRXHEADERID;
		RETRY_STATUS = rETRY_STATUS;
		RETUR_STATUS = rETUR_STATUS;
		SUCCESS_STATUS = sUCCESS_STATUS;
		PROC_STATUS = pROC_STATUS;
		PROC_DESCRIPTION = pROC_DESCRIPTION;
		TOTAL_PROCESS = tOTAL_PROCESS;
		CREATE_DATE = cREATE_DATE;
		SUM_STATUS_RTY = sUM_STATUS_RTY;
		SUM_STATUS_RTU = sUM_STATUS_RTU;
		TRXTYPE = tRXTYPE;
		LEGSTATUS = lEGSTATUS;
	}

	public String getFILE_NAME() {
		return FILE_NAME;
	}

	public void setFILE_NAME(String fILE_NAME) {
		FILE_NAME = fILE_NAME;
	}

	public String getBATCHID() {
		return BATCHID;
	}

	public void setBATCHID(String bATCHID) {
		BATCHID = bATCHID;
	}

	public String getTRXHEADERID() {
		return TRXHEADERID;
	}

	public void setTRXHEADERID(String tRXHEADERID) {
		TRXHEADERID = tRXHEADERID;
	}

	public String getRETRY_STATUS() {
		return RETRY_STATUS;
	}

	public void setRETRY_STATUS(String rETRY_STATUS) {
		RETRY_STATUS = rETRY_STATUS;
	}

	public String getRETUR_STATUS() {
		return RETUR_STATUS;
	}

	public void setRETUR_STATUS(String rETUR_STATUS) {
		RETUR_STATUS = rETUR_STATUS;
	}

	public String getSUCCESS_STATUS() {
		return SUCCESS_STATUS;
	}

	public void setSUCCESS_STATUS(String sUCCESS_STATUS) {
		SUCCESS_STATUS = sUCCESS_STATUS;
	}

	public String getPROC_STATUS() {
		return PROC_STATUS;
	}

	public void setPROC_STATUS(String pROC_STATUS) {
		PROC_STATUS = pROC_STATUS;
	}

	public String getPROC_DESCRIPTION() {
		return PROC_DESCRIPTION;
	}

	public void setPROC_DESCRIPTION(String pROC_DESCRIPTION) {
		PROC_DESCRIPTION = pROC_DESCRIPTION;
	}

	public String getTOTAL_PROCESS() {
		return TOTAL_PROCESS;
	}

	public void setTOTAL_PROCESS(String tOTAL_PROCESS) {
		TOTAL_PROCESS = tOTAL_PROCESS;
	}

	public String getCREATE_DATE() {
		return CREATE_DATE;
	}

	public void setCREATE_DATE(String cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}

	public String getSUM_STATUS_RTY() {
		return SUM_STATUS_RTY;
	}

	public void setSUM_STATUS_RTY(String sUM_STATUS_RTY) {
		SUM_STATUS_RTY = sUM_STATUS_RTY;
	}

	public String getSUM_STATUS_RTU() {
		return SUM_STATUS_RTU;
	}

	public void setSUM_STATUS_RTU(String sUM_STATUS_RTU) {
		SUM_STATUS_RTU = sUM_STATUS_RTU;
	}

	public String getTRXTYPE() {
		return TRXTYPE;
	}

	public void setTRXTYPE(String tRXTYPE) {
		TRXTYPE = tRXTYPE;
	}

	public String getLEGSTATUS() {
		return LEGSTATUS;
	}

	public void setLEGSTATUS(String lEGSTATUS) {
		LEGSTATUS = lEGSTATUS;
	}

}
