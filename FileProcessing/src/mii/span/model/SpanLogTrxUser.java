package mii.span.model;

import java.util.Date;

public class SpanLogTrxUser {
	private Date DATE_TRX;
	private String FILENAME, BRANCH, ACCOUNT_NUMBER, CURRENCY, DESCRIPTION1, DESCRIPTION2, USER_ID, BATCHID, USER_NAME;
	private Long DATA_TRANSACTION, NOMINAL_TRANSACTION, DATA_PENDING, NOMINAL_PENDING, DATA_SUCCESS, NOMINAL_SUCCESS, DATA_RETUR, NOMINAL_RETUR;
	
	
	public Date getDATE_TRX() {
		return DATE_TRX;
	}
	public void setDATE_TRX(Date dATE_TRX) {
		DATE_TRX = dATE_TRX;
	}
	public String getFILENAME() {
		return FILENAME;
	}
	public void setFILENAME(String fILENAME) {
		FILENAME = fILENAME;
	}
	public String getBRANCH() {
		return BRANCH;
	}
	public void setBRANCH(String bRANCH) {
		BRANCH = bRANCH;
	}
	public String getACCOUNT_NUMBER() {
		return ACCOUNT_NUMBER;
	}
	public void setACCOUNT_NUMBER(String aCCOUNT_NUMBER) {
		ACCOUNT_NUMBER = aCCOUNT_NUMBER;
	}
	public String getCURRENCY() {
		return CURRENCY;
	}
	public void setCURRENCY(String cURRENCY) {
		CURRENCY = cURRENCY;
	}
	public String getDESCRIPTION1() {
		return DESCRIPTION1;
	}
	public void setDESCRIPTION1(String dESCRIPTION1) {
		DESCRIPTION1 = dESCRIPTION1;
	}
	public String getDESCRIPTION2() {
		return DESCRIPTION2;
	}
	public void setDESCRIPTION2(String dESCRIPTION2) {
		DESCRIPTION2 = dESCRIPTION2;
	}
	public String getUSER_ID() {
		return USER_ID;
	}
	public void setUSER_ID(String uSER_ID) {
		USER_ID = uSER_ID;
	}
	public String getBATCHID() {
		return BATCHID;
	}
	public void setBATCHID(String bATCHID) {
		BATCHID = bATCHID;
	}
	public String getUSER_NAME() {
		return USER_NAME;
	}
	public void setUSER_NAME(String uSER_NAME) {
		USER_NAME = uSER_NAME;
	}
	public Long getDATA_TRANSACTION() {
		return DATA_TRANSACTION;
	}
	public void setDATA_TRANSACTION(Long dATA_TRANSACTION) {
		DATA_TRANSACTION = dATA_TRANSACTION;
	}
	public Long getNOMINAL_TRANSACTION() {
		return NOMINAL_TRANSACTION;
	}
	public void setNOMINAL_TRANSACTION(Long nOMINAL_TRANSACTION) {
		NOMINAL_TRANSACTION = nOMINAL_TRANSACTION;
	}
	public Long getDATA_PENDING() {
		return DATA_PENDING;
	}
	public void setDATA_PENDING(Long dATA_PENDING) {
		DATA_PENDING = dATA_PENDING;
	}
	public Long getNOMINAL_PENDING() {
		return NOMINAL_PENDING;
	}
	public void setNOMINAL_PENDING(Long nOMINAL_PENDING) {
		NOMINAL_PENDING = nOMINAL_PENDING;
	}
	public Long getDATA_SUCCESS() {
		return DATA_SUCCESS;
	}
	public void setDATA_SUCCESS(Long dATA_SUCCESS) {
		DATA_SUCCESS = dATA_SUCCESS;
	}
	public Long getNOMINAL_SUCCESS() {
		return NOMINAL_SUCCESS;
	}
	public void setNOMINAL_SUCCESS(Long nOMINAL_SUCCESS) {
		NOMINAL_SUCCESS = nOMINAL_SUCCESS;
	}
	public Long getDATA_RETUR() {
		return DATA_RETUR;
	}
	public void setDATA_RETUR(Long dATA_RETUR) {
		DATA_RETUR = dATA_RETUR;
	}
	public Long getNOMINAL_RETUR() {
		return NOMINAL_RETUR;
	}
	public void setNOMINAL_RETUR(Long nOMINAL_RETUR) {
		NOMINAL_RETUR = nOMINAL_RETUR;
	}
	
	
}
