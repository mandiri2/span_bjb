package mii.span.model;

public class BulkFTResp {

	private String batchID;
	private String trxHeaderID;
	private String trxDetailID;
	private String debitAmount;
	private String sourceAccountNumber;
	private String destinationAccountNumber;
	
	public BulkFTResp() {
	}
	
	public BulkFTResp(String batchID, String trxHeaderID, String trxDetailID) {
		this.batchID = batchID;
		this.trxHeaderID = trxHeaderID;
		this.trxDetailID = trxDetailID;
	}

	public String getBatchID() {
		return batchID;
	}

	public void setBatchID(String batchID) {
		this.batchID = batchID;
	}

	public String getTrxHeaderID() {
		return trxHeaderID;
	}

	public void setTrxHeaderID(String trxHeaderID) {
		this.trxHeaderID = trxHeaderID;
	}

	public String getTrxDetailID() {
		return trxDetailID;
	}

	public void setTrxDetailID(String trxDetailID) {
		this.trxDetailID = trxDetailID;
	}

	public String getDebitAmount() {
		return debitAmount;
	}

	public void setDebitAmount(String debitAmount) {
		this.debitAmount = debitAmount;
	}

	public String getSourceAccountNumber() {
		return sourceAccountNumber;
	}

	public void setSourceAccountNumber(String sourceAccountNumber) {
		this.sourceAccountNumber = sourceAccountNumber;
	}

	public String getDestinationAccountNumber() {
		return destinationAccountNumber;
	}

	public void setDestinationAccountNumber(String destinationAccountNumber) {
		this.destinationAccountNumber = destinationAccountNumber;
	}

}
