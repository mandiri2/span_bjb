package mii.span.model;

public class SpanDataToBeExecute {
	private String fileName;
	private String totalAmount;
	private String totalRecord;
	private String documentDate;
	
	public SpanDataToBeExecute(String fileName, String totalAmount,
			String totalRecord, String documentDate) {
		super();
		this.fileName = fileName;
		this.totalAmount = totalAmount;
		this.totalRecord = totalRecord;
		this.documentDate = documentDate;
	}
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}
	public String getTotalRecord() {
		return totalRecord;
	}
	public void setTotalRecord(String totalRecord) {
		this.totalRecord = totalRecord;
	}
	public String getDocumentDate() {
		return documentDate;
	}
	public void setDocumentDate(String documentDate) {
		this.documentDate = documentDate;
	}
	
}
