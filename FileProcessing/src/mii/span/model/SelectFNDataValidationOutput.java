package mii.span.model;

public class SelectFNDataValidationOutput {
	private String batchId;
	private String procStatus;
	private String trxType;
	private String documentDate;
	private String execFileFlag;
	
	public SelectFNDataValidationOutput(String batchId, String procStatus,
			String trxType, String documentDate, String execFileFlag) {
		super();
		this.batchId = batchId;
		this.procStatus = procStatus;
		this.trxType = trxType;
		this.documentDate = documentDate;
		this.execFileFlag = execFileFlag;
	}
	
	public SelectFNDataValidationOutput() {
	}

	public String getBatchId() {
		return batchId;
	}
	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}
	public String getProcStatus() {
		return procStatus;
	}
	public void setProcStatus(String procStatus) {
		this.procStatus = procStatus;
	}
	public String getTrxType() {
		return trxType;
	}
	public void setTrxType(String trxType) {
		this.trxType = trxType;
	}
	public String getDocumentDate() {
		return documentDate;
	}
	public void setDocumentDate(String documentDate) {
		this.documentDate = documentDate;
	}
	public String getExecFileFlag() {
		return execFileFlag;
	}
	public void setExecFileFlag(String execFileFlag) {
		this.execFileFlag = execFileFlag;
	}
	
}
