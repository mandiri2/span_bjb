package mii.span.model;

import java.io.Serializable;
import java.util.Date;

public class Parameter implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String paramId,
		value,
		description,
		createWho,
		changeWho;
	
	private Date createDate,
		changeDate;
	
	
	/** GETTER SETTER **/
	public String getParamId() {
		return paramId;
	}

	public void setParamId(String paramId) {
		this.paramId = paramId;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCreateWho() {
		return createWho;
	}

	public void setCreateWho(String createWho) {
		this.createWho = createWho;
	}

	public String getChangeWho() {
		return changeWho;
	}

	public void setChangeWho(String changeWho) {
		this.changeWho = changeWho;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getChangeDate() {
		return changeDate;
	}

	public void setChangeDate(Date changeDate) {
		this.changeDate = changeDate;
	}
	
	
}
