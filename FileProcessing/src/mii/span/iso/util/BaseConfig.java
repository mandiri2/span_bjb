package mii.span.iso.util;

import com.mii.constant.Constants;
import com.mii.dao.SystemParameterDAO;
import com.mii.models.SystemParameter;

import mii.span.util.BusinessUtil;

public class BaseConfig {
	
	static BaseConfig baseConfig = null;
	// IP server is destination address to connect with
	// this case is Switching BJB
	private String IPServer;
	private int portServer;
		
	// setting timeout for ISO messaging
	private int timeoutISORequest;
	private int echoInterval;
	private int connectInterval;

	public BaseConfig() {
		SystemParameterDAO systemParameterDAO = (SystemParameterDAO) BusinessUtil.getDao("sysParamDAO");
		SystemParameter sysParamIPServer = systemParameterDAO.getValueParameter(Constants.ISO_IP_SERVER).get(0);
		SystemParameter sysParamPortServer = systemParameterDAO.getValueParameter(Constants.ISO_PORT_SERVER).get(0);
		SystemParameter sysParamTimeout = systemParameterDAO.getValueParameter(Constants.ISO_TIMEOUT_LIMIT).get(0);
		SystemParameter sysParamEchoInterval = systemParameterDAO.getValueParameter(Constants.ISO_ECHO_INTERVAL).get(0);
		SystemParameter sysParamConnectInterval = systemParameterDAO.getValueParameter(Constants.ISO_CONNECT_INTERVAL).get(0);
		
		setIPServer(sysParamIPServer.getParam_value());
		setPortServer(Integer.parseInt(sysParamPortServer.getParam_value()));
		setTimeoutISORequest(Integer.parseInt(sysParamTimeout.getParam_value()));
		setEchoInterval(Integer.parseInt(sysParamEchoInterval.getParam_value()));
		setConnectInterval(Integer.parseInt(sysParamConnectInterval.getParam_value()));
		
		System.out.println("[INIT CONFIG] : " + getIPServer() + "|" + getPortServer() + "|" + 
							getTimeoutISORequest() + "|" + getEchoInterval() + "|" + getConnectInterval());
	}
	
	public static BaseConfig getBaseConfig() {
		if (baseConfig == null){
			baseConfig = new BaseConfig();
		}
		return baseConfig;
	}
	
	public String getIPServer() {
		return IPServer;
	}

	public void setIPServer(String iPServer) {
		IPServer = iPServer;
	}

	public int getPortServer() {
		return portServer;
	}

	public void setPortServer(int portServer) {
		this.portServer = portServer;
	}

	public int getTimeoutISORequest() {
		return timeoutISORequest;
	}

	public void setTimeoutISORequest(int timeoutISORequest) {
		this.timeoutISORequest = timeoutISORequest;
	}

	public int getEchoInterval() {
		return echoInterval;
	}

	public void setEchoInterval(int echoInterval) {
		this.echoInterval = echoInterval;
	}

	public int getConnectInterval() {
		return connectInterval;
	}

	public void setConnectInterval(int connectInterval) {
		this.connectInterval = connectInterval;
	}

}
