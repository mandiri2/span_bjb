package mii.span.iso.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;

import mii.span.iso.model.ReinquiryMessage;
import mii.span.util.BusinessUtil;

import com.mii.constant.Constants;
import com.mii.constant.DaoConstant;
import com.mii.dao.BulkFTDao;
import com.mii.dao.SystemParameterDAO;

public class Reinquiry implements Runnable {
	
	static Logger logger = Logger.getLogger(Reinquiry.class);
	static SAMPackager samPackager = new SAMPackager();

	@Override
	public void run() {
		logger.info(Thread.currentThread().getName() + " running...");
		SystemParameterDAO sysParam = (SystemParameterDAO) BusinessUtil.getDao(DaoConstant.SystemParameterDAO);
		
		String limit = sysParam.getValueParameter(Constants.ISO_PRC_LIMIT).get(0).getParam_value();
		String timeout = sysParam.getValueParameter(Constants.ISO_TIMEOUT_LIMIT).get(0).getParam_value();
		
		List<ReinquiryMessage> listReinquiry = new ArrayList<ReinquiryMessage>();
		
		listReinquiry = getYetRespondedRequest(limit, timeout);
		
		// re send transaction_status_inquiry for hanged transaction
		executeYetRespondedRequest(listReinquiry);
		
		logger.info(Thread.currentThread().getName() + " end...");
	}
	
	public static void executeYetRespondedRequest(List<ReinquiryMessage> listRequest) {
		BulkFTDao bulkFTDao = (BulkFTDao) BusinessUtil.getDao("bulkFTDao");
		ISOHelper helper = new ISOHelper();
		for (ReinquiryMessage r : listRequest) {
			ISOMsg msg = new ISOMsg();
			msg.setPackager(samPackager);
			if (r.getRequest().startsWith("0200")) {
				try {
					msg.unpack(r.getRequest().getBytes());
					// manipulate message
					if (msg.getString(3).equals("450000")) {
						msg.set(0, "0200");
						msg.set(47, msg.getString(11));
						msg.set(11, ISOHelper.getStan());
						msg.set(3, "480000");
						msg.set(60, "TRX");
					}
					else if (msg.getString(3).equals("470000")) {
						msg.set(0, "0200");
						msg.set(47, msg.getString(11));
						msg.set(11, ISOHelper.getStan());
						msg.set(3, "480000");
						msg.set(60, "NHL");
					}else if (msg.getString(3).equals("460000")) {
						msg.set(0, "0200");
						msg.set(47, msg.getString(11));
						msg.set(11, ISOHelper.getStan());
						msg.set(3, "480000");
						msg.set(60, "TRX");
					}
					
					r.setRequest(new String(msg.pack()));
					msg.dump(System.out, "[REINQUIRY] ");
				} catch (ISOException e) {
					e.printStackTrace();
				}
			}
			try {
				byte[] msgToBeSend = helper.generateISOMessageByte(msg);
				OpenSocketConnector.getConnector().send(msgToBeSend);
				bulkFTDao.insertReqLog(msg);
				logger.info("Success insert new request after reinquiry ...");
			} catch (ISOException | IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static List<ReinquiryMessage> getYetRespondedRequest(String limit, String timeout) {
		BulkFTDao bulkFTDao = (BulkFTDao) BusinessUtil.getDao("bulkFTDao");
		List<ReinquiryMessage> listReinquiry = new ArrayList<ReinquiryMessage>();
		
		listReinquiry = bulkFTDao.getYetRespondedRequest(limit, timeout);
		
		return listReinquiry;
	}

}
