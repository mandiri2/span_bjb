package mii.span.iso.util;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Base64;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import mii.span.model.BulkFTReqPRC;
import mii.span.scheduller.odbc.PostingScheduller;
import mii.span.util.BusinessUtil;

import org.apache.log4j.Logger;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;

import com.mii.dao.BulkFTDao;

public class ResponseProcessor implements Runnable {
	
	private static Queue<byte[]> responseQueue = new LinkedList<>();
	InputStream in = OpenSocketConnector.in;
	Object mutex = new Object();
	Logger logger = Logger.getLogger(ResponseProcessor.class);
	SAMPackager samPackager = new SAMPackager();

	@Override
	public void run() {
		// initiate
		logger.info(Thread.currentThread().getName() + " running...");
		ISOHelper helper = new ISOHelper();
		while (true) {
			synchronized (mutex) { 
				if (getResponseQueue() != null && getResponseQueue().size() > 0) {
					// processing data byte received from stream into string
					if (getResponseQueue().peek() != null) {
						String isoResponse = helper.byteToString(getResponseQueue().poll());
						logger.info("LOG>>>>>>>> SUCCES GET RESPONSE TO EQ "+ isoResponse );
						 
						logger.info(isoResponse);
						// [PRE PROCESS] checking iso response contain multiple response
						// parsing batch response into list single response
						List<String> listResponse = new ArrayList<String>();
						listResponse.add(isoResponse);
//						listResponse = parseBatchResponse(isoResponse);
						
						// [PRE PROCESS] listResponse (in string) into 
						// listResponseISOMsg (in ISOMsg) into dump response
						List<ISOMsg> listResponseISOMsg = new ArrayList<>();
						listResponseISOMsg = preProcessBatchResponse(listResponse);
						
						// [MAIN] processing all response we got
						processAllResponse(listResponseISOMsg);
					}
				}
			}
		}
	}
	
	/**
	 * List of singleResponse we got before, will be changed into ISOMsg.
	 * With that, we can dump to see the bit of response.
	 * For each element of list we will change it into ISOMsg and return back
	 * into list of singleResponseISOMsg.
	 * @param listResponse - list of singleReponse in string
	 * @return listResponseISOMsg - list of singleResponse in ISOMsg
	 */
	private List<ISOMsg> preProcessBatchResponse(List<String> listResponse) {
		List<ISOMsg> listResponseISOMsg = new ArrayList<>();
		
		for (String s : listResponse) {
//			String subResponse = s.substring(2);
			String subResponse = s;
			ISOMsg response = new ISOMsg();
			response.setPackager(samPackager);
			try {
				response.unpack(subResponse.getBytes());
				response.dump(System.out, "[RESPONSE] ");
				if (!listResponseISOMsg.contains(response)) {
					listResponseISOMsg.add(response);
				} else {
					logger.warn("[DUPLICATE] response contain in listResponseISOMsg...");
				}				
			} catch (ISOException e) {
				e.printStackTrace();
			}
		}
		
		return listResponseISOMsg;
	}
	
	/**
	 * List of singleResponseISOMsg we got, we will process it.
	 * The process are : matching with the request sent from PostingScheduler,
	 * when match insert/update into table cme_bulk_ft_resp, update span_penihilan_log,
	 * update response_log
	 * 
	 * @param listResponseISOMsg - list of singleResponseISOMsg
	 */
	private void processAllResponse(List<ISOMsg> listResponseISOMsg) {
		BulkFTDao bulkFTDao = (BulkFTDao) BusinessUtil.getDao("bulkFTDao");
		for (ISOMsg msg : listResponseISOMsg) {
			//init
			String procStan = getProcStan(msg);
			// check response with sentMessage
			BulkFTReqPRC prc = PostingScheduller.sentMessage.get(msg.getString(7).concat(procStan));
			PostingScheduller.sentMessage.remove(msg.getString(7).concat(procStan));
			if (prc != null) {
				logger.debug("[INFO] We get a response ...");
				
				if (msg.getString(3).equals("480000")){
					logger.info("[INFO] We get a reinquiry response ...");
					bulkFTDao.updateResLogReinquiry(msg);
				}
				else {
					logger.info("[INFO] We get a normal transaction response ...");
					bulkFTDao.updateResLog(msg);
				}
				
				PostingService.insertOrUpdateRSP(prc, msg);
				PostingService.updateLogPenihilan(prc, msg);
				// update span_account_balance_log just if penihilan success
				if (msg.getString(39).equals("00") && prc.getBatchid().startsWith("NHL")){
					PostingService.updateBeginBalance(prc);
				}
				
				logger.info("Success updating cme iso log ... " + msg.getString(7) + msg.getString(11));
			}
			else {
				logger.debug("hello... unexpected response ... probably contains echo or reinquiryResponse");
				msg.dump(System.out, "[UNEXPECTED RESPONSE]");
			}
		}
	}
	
	private String getProcStan(ISOMsg msg) {
		try{
			if(msg.getString(3).equalsIgnoreCase("450000")){
				return msg.getString(11);
			}else if (msg.getString(3).equalsIgnoreCase("460000")){
				return msg.getString(11);
			}else if (msg.getString(3).equalsIgnoreCase("470000")){
				return msg.getString(11);
			}else if (msg.getString(3).equalsIgnoreCase("480000")){
				return msg.getString(47);
			}
			return "";
		}catch(Exception e){
			return "";
		}
	}

	public static Queue<byte[]> getResponseQueue() {
		return responseQueue;
	}
	
	public static void setResponseQueue(Queue<byte[]> responseQueue) {
		ResponseProcessor.responseQueue = responseQueue;
	}

}
