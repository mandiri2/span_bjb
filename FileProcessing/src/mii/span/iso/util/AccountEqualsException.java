package mii.span.iso.util;

public class AccountEqualsException extends Exception {
	private static final long serialVersionUID = -4915712067033530024L;

	public AccountEqualsException() {}
	
    public AccountEqualsException(String message) {
       super(message);
    }

}
