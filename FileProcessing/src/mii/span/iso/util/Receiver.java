package mii.span.iso.util;

import java.io.IOException;
import java.io.InputStream;

import org.apache.log4j.Logger;

public class Receiver implements Runnable {

	InputStream in = OpenSocketConnector.in;
	
	Object mutex = new Object();
	Logger logger = Logger.getLogger(Receiver.class);
	
	@Override
	public void run() {
		// initiate
		logger.info(Thread.currentThread().getName() + " running...");
		int count = 0;
		byte[] data = new byte[1024];
		int len = 2;
		int isoDataLength = 0;
		// main processing
		try {
			while ((count = in.read(data, 0, len)) >= 0) {
				logger.debug("length data read from socket: " + count);
				isoDataLength = PubUtil.getIntegerFrom2ByteArray(data);
				logger.debug("iso data length : " + isoDataLength);
				data = new byte[isoDataLength];
				in.read(data, 0, isoDataLength);
//				logger.debug("response : " + new String(data));
				ResponseProcessor.getResponseQueue().add(data);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		logger.info(Thread.currentThread().getName() + " end...");
	}

}
