package mii.span.iso.util;

public class PubUtil {
	
	public static int getIntegerFrom2ByteArray(byte[] byteArr) {
		return ((byteArr[0] & 0xFF) << 8 | (byteArr[1] & 0xFF));
	}
	
	public static byte[] intTo2ByteArray(int a)
	{
	    byte[] ret = new byte[2];
	    ret[1] = (byte) (a & 0xFF);   
	    ret[0] = (byte) ((a >> 8) & 0xFF);
	    
	    return ret;
	}
	
	public static byte[] intTo4ByteArray(int a)
	{
	    byte[] ret = new byte[4];
	    ret[3] = (byte) (a & 0xFF);   
	    ret[2] = (byte) ((a >> 8) & 0xFF);
	    ret[1] = (byte) ((a >> 16) & 0xFF);
	    ret[0] = (byte) ((a >> 24) & 0xFF);
	    
	    return ret;
	}
	
	public static byte[] concateByteArray(byte[] a, byte[] b){
		byte[] c = new byte[a.length + b.length];
		System.arraycopy(a, 0, c, 0, a.length);
		System.arraycopy(b, 0, c, a.length, b.length);
		return c;
	}

}
