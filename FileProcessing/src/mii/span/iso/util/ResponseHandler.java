package mii.span.iso.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import mii.span.model.BulkFTReqPRC;
import mii.span.scheduller.odbc.PostingScheduller;
import mii.span.util.BusinessUtil;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;

import com.mii.dao.BulkFTDao;
import com.mii.dao.SpanPenihilanLogDao;

public class ResponseHandler implements Runnable {

	InputStream in = OpenSocketConnector.in;
	BulkFTDao bulkFTDao = (BulkFTDao) BusinessUtil.getDao("bulkFTDao");
	SpanPenihilanLogDao spanPenihilanDao = (SpanPenihilanLogDao) BusinessUtil.getDao("spanPenihilanLogDao");
	Object mutex = new Object();
	
	@Override
	public void run() {
		// initiate
		System.out.println(Thread.currentThread().getName() + " running...");
		ISOHelper helper = new ISOHelper();
		int count = 0;
		byte[] data = new byte[3210000]; // FIXME with DB parameter
		
		// main processing
		try {
			while ((count = in.read(data)) >= 0) {
				synchronized (mutex) {
					System.out.println("length data read from socket: " + count);

					// processing data byte received from stream into string
					String isoResponse = helper.byteToString(data);
					isoResponse.substring(0, count);
					
					// [PRE PROCESS] checking iso response contain multiple response
					// parsing batch response into list single response
					List<String> listResponse = new ArrayList<String>();
					listResponse = parseBatchResponse(isoResponse);
					
					// [PRE PROCESS] listResponse (in string) into 
					// listResponseISOMsg (in ISOMsg) into dump response
					List<ISOMsg> listResponseISOMsg = new ArrayList<>();
					listResponseISOMsg = preProcessBatchResponse(listResponse);
					
					// [MAIN] processing all response we got
					processAllResponse(listResponseISOMsg);
				}
				data = new byte[3210000]; // FIXME with DB parameter
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println(Thread.currentThread().getName() + " end...");
	}

	/**
	 * Response we got from the socket may contains multipleResponse.
	 * We need to check it, then parse it into list of singleResponse.
	 * The list will be used in preprocessBatchResponse().
	 * @param isoResponse - String response may contains multipleResponse
	 * @return List<String> listResponse - list of singleResponse
	 */
	private List<String> parseBatchResponse(String isoResponse) {
		List<String> listResponse = new ArrayList<>();
		int firstMsgLenght = PubUtil.getIntegerFrom2ByteArray(isoResponse.getBytes());
		
		while(firstMsgLenght > 0){
			System.out.println("[INFO] Message length read from socket : " + firstMsgLenght);
			if (isoResponse.length() >= firstMsgLenght){
				String singleResponse = isoResponse.substring(0, firstMsgLenght+2);
				String singleResponseTemp = singleResponse.substring(2);
				// echo forget
				if (singleResponseTemp.startsWith("0800") || singleResponseTemp.startsWith("0810")){
					// do nothing
				}
				else {
					if (!listResponse.contains(singleResponse)){
						System.err.println("[SINGLE RESPONSE]" + singleResponse);
						listResponse.add(singleResponse);
					} else {
						System.err.println("[DUPLICATE] response contain in listResponse...");
					}
				}
				isoResponse = isoResponse.substring(firstMsgLenght+2);
				if (isoResponse.length() == 0)
					firstMsgLenght = 0;
				else
					firstMsgLenght = PubUtil.getIntegerFrom2ByteArray(isoResponse.getBytes());
				System.out.println("[INFO] Message length after processing singleresponse : " + firstMsgLenght);
			}
			else {
				System.err.println("[WARNING] IsoResponse length less than msgLength ...");
				break;
			}
		}
		System.err.println("[INFO] End while - end parse batch response");
		
		return listResponse;
	}
	
	/**
	 * List of singleResponse we got before, will be changed into ISOMsg.
	 * With that, we can dump to see the bit of response.
	 * For each element of list we will change it into ISOMsg and return back
	 * into list of singleResponseISOMsg.
	 * @param listResponse - list of singleReponse in string
	 * @return listResponseISOMsg - list of singleResponse in ISOMsg
	 */
	private List<ISOMsg> preProcessBatchResponse(List<String> listResponse) {
		List<ISOMsg> listResponseISOMsg = new ArrayList<>();
		
		for (String s : listResponse) {
			String subResponse = s.substring(2);
			ISOMsg response = new ISOMsg();
			response.setPackager(new SAMPackager());
			try {
				response.unpack(subResponse.getBytes());
				response.dump(System.err, "[RESPONSE] ");
				if (!listResponseISOMsg.contains(response)) {
					listResponseISOMsg.add(response);
				} else {
					System.err.println("[DUPLICATE] response contain in listResponseISOMsg...");
				}				
			} catch (ISOException e) {
				e.printStackTrace();
			}
		}
		
		return listResponseISOMsg;
	}
	
	/**
	 * List of singleResponseISOMsg we got, we will process it.
	 * The process are : matching with the request sent from PostingScheduler,
	 * when match insert/update into table cme_bulk_ft_resp, update span_penihilan_log,
	 * update response_log
	 * 
	 * @param listResponseISOMsg - list of singleResponseISOMsg
	 */
	private void processAllResponse(List<ISOMsg> listResponseISOMsg) {
		for (ISOMsg msg : listResponseISOMsg) {
			// check response with sentMessage
			BulkFTReqPRC prc = PostingScheduller.sentMessage.get(msg.getString(7).concat(msg.getString(11)));
			PostingScheduller.sentMessage.remove(msg.getString(7).concat(msg.getString(11)));
			if (prc != null) {
				System.out.println("[INFO] We get a response ...");
				
				PostingService.insertOrUpdateRSP(prc, msg);
				PostingService.updateLogPenihilan(prc, msg);
				// update span_account_balance_log just if penihilan success
				if (msg.getString(39).equals("00") && prc.getBatchid().startsWith("NHL")){
					PostingService.updateBeginBalance(prc);
				}
				bulkFTDao.updateResLog(msg);
				
				/**
				 * FIXME ASSESSED take out automatically retry by system
				 * 2016-10-25
				 
				if (msg.getString(39).equals("00")) {
					System.out.println("we get success response ...");
				}
				// select retry response code
				else if (msg.getString(39).equals("68")) {
					System.out.println("retrying posting ...");
					// retry -- included checking retry count
					sendRetryMessage(prc, msg, bulkFTDao);
				}*/
			}
		}
	}
	
	private static void sendRetryMessage(BulkFTReqPRC prc, ISOMsg msg, BulkFTDao dao) {
		ISOHelper h = new ISOHelper();
		// generate retry message proccode 460000 -- prev stan in bit47
		try {
			if (prc.getRetryCount().equals("") || prc.getRetryCount().equals("null") || 
					prc.getRetryCount() == null){
				prc.setRetryCount("0");
			}
			// update retrycount on prc
			if (Integer.parseInt(prc.getRetryCount()) < 3){
				// generate iso msg
				System.out.println("[retry][stan] : "+ msg.getString(11));
				msg = h.generateRetryPostingMessage(msg, msg.getString(11));
				// generate iso msg with 2byte length
				byte[] msgToBeSend = h.generateISOMessageByte(msg);
				// send to if400
				try {
					OpenSocketConnector.getConnector().send(msgToBeSend);
				} catch (IOException e) {
					e.printStackTrace();
				}
				PostingScheduller.sentMessage.put(msg.getString(7).concat(msg.getString(11)), prc);
				
				// update retry count query
				int retryCountTemp = Integer.parseInt(prc.getRetryCount());
				prc.setRetryCount(String.valueOf(retryCountTemp+1));
				dao.updateRetryCount(prc);
			}
			else {
				// dont retry
			}
		} catch (ISOException e) {
			e.printStackTrace();
		}
	}
}
