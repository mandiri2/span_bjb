package mii.span.iso.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Base64;

import org.apache.log4j.Logger;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;

public class OpenSocketConnector extends Thread {

	
	private static OpenSocketConnector connector = null;
	private static BaseConfig baseconfig;
	private static Socket socket;

	static String host;
	static int port;
	static int echoInterval;
	static int connectInterval;
	static OutputStream out;
	static InputStream in;

	Logger logger = Logger.getLogger(OpenSocketConnector.class);

	public static OpenSocketConnector getConnector() {
		if (connector == null) {
			connector = new OpenSocketConnector();
		}
		return connector;
	}

	@Override
	public void run() {
		logger.info("starting thread open socket connector ...");
		try {
			host = getBaseconfig().getIPServer();
			port = getBaseconfig().getPortServer();
			echoInterval = getBaseconfig().getEchoInterval();
			connectInterval = getBaseconfig().getConnectInterval();
			connect();
		} catch (StackOverflowError s) {
			logger.warn("cannot connect to server socket, "
					+ "try restart either server ...");
		}
	}
	
	public void connect() {
		try {
			logger.info("trying to connect " + host + " - port " + port);

			socket = new Socket(host, port);
			socket.setKeepAlive(true);
			if (getSocket().isConnected()) {
				logger.info("[CONNECTED] : " + getSocket().isConnected());

				in = socket.getInputStream();
				out = socket.getOutputStream();

				logger.info("InputStream : " + in);
				logger.info("OutputStream : " + out);

				ISOHelper helper = new ISOHelper();
				ISOMsg echoMsg = new ISOMsg();
				echoMsg.setPackager(new SAMPackager());

				// response handler
//				new Thread(new ResponseHandler(), "response_handler").start();
				new Thread(new Receiver(), "receiver").start();
				new Thread(new ResponseProcessor(), "response_processor").start();

				// maintain connection with server socket
				while (socket.isConnected()) {
					echoMsg = helper.generateEchoMessage(echoMsg);
					logger.debug(PostingService.dump(echoMsg, ""));

					byte[] isoToBeSend = generateISOMessageByte(echoMsg);
					out.write(isoToBeSend);
					logger.debug("echo sent");

					Thread.sleep(echoInterval * 1000);
				}

				logger.warn("broken connection");

				// retry new connection
				this.connect();
			}
			logger.debug("just finished connected to "
					+ getSocket().getRemoteSocketAddress());

		} catch (Exception e) {
			logger.warn("exception on connect(); retrying new connection");
			logger.warn(e.getMessage());
			try {
				Thread.sleep(connectInterval * 1000);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
			this.connect();
		}
	}

	public void send(byte[] message) throws IOException {
		out.write(message); 
		logger.info("LOG>>>>>>>> SUCCES POSTING TO EQ "+ Base64.getEncoder().encodeToString(message) );
	}

	public byte[] generateISOMessageByte(ISOMsg message) throws ISOException {
		// make the iso string
		String iso = new String(message.pack());

		// get the length of iso string in byte[]
		byte[] isoLengthByteArray = PubUtil.intTo2ByteArray(iso.length());

		// create iso message to send
		byte[] isoToBeSend = PubUtil.concateByteArray(isoLengthByteArray,
				iso.getBytes());

		return isoToBeSend;
	}

	public static Socket getSocket() {
		return socket;
	}

	public static void setSocket(Socket client) {
		OpenSocketConnector.socket = client;
	}

	public static BaseConfig getBaseconfig() {
		return baseconfig;
	}

	public static void setBaseconfig(BaseConfig baseconfig) {
		OpenSocketConnector.baseconfig = baseconfig;
	}

}
