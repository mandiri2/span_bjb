package mii.span.iso.util;

import org.jpos.iso.ISOMsg;

import com.mii.constant.Constants;
import com.mii.dao.SystemParameterDAO;

import mii.span.util.BusinessUtil;

public class ISOLoggingUtil {

	// iso logging util
	public static void consoleDump(String message) {
		SystemParameterDAO sysParamDao = (SystemParameterDAO) BusinessUtil
				.getDao("sysParamDAO");
		// TODO create parameter ISO_LOGGING --> 1/0

		// logEnabled = 1 / 0
		String logEnabled = sysParamDao
				.getValueParameter(Constants.ISO_LOGGING).get(0)
				.getParam_value();

		if (logEnabled.equals("1")) {
			// TODO dump
		} else {
			// hide console dump
		}

	}

	public static void consoleDump(ISOMsg message) {
		SystemParameterDAO sysParamDao = (SystemParameterDAO) BusinessUtil
				.getDao("sysParamDAO");
		// logEnabled = 1 / 0
		String logEnabled = sysParamDao
				.getValueParameter(Constants.ISO_LOGGING).get(0)
				.getParam_value();

		if (logEnabled.equals("1")) {
			// TODO dump
		} else {
			// hide console dump
		}

	}

}
