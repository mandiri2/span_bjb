package mii.span.iso.util;

import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.faces.bean.ManagedProperty;

import mii.span.model.BulkFTReqPRC;
import mii.span.util.BusinessUtil;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;

import com.mii.constant.Constants;
import com.mii.dao.BulkFTDao;
import com.mii.dao.SystemParameterDAO;
import com.mii.models.SystemParameter;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.ApplicationInstance;
import com.mii.util.CommonUtils;

public class ISOHelper implements Serializable {
	private static final long serialVersionUID = 1L;
	private static String stan;
	static BulkFTDao bulkFTDao = (BulkFTDao) BusinessUtil.getDao("bulkFTDao");
	
	@ManagedProperty(value = "#{applicationBean}")
	private static ApplicationBean applicationBean;
	
	private static SystemParameterDAO getSystemParameterDaoBean(){
		applicationBean = ApplicationInstance.getInstance().getApplicationBean();
		return (SystemParameterDAO) applicationBean.getFileProcessContext().getBean("sysParamDAO");
	}
	
	public String byteToString(byte[] data) {
		String output = new String(data);
		return output;
	}
	
	public ISOMsg generateEchoMessage(ISOMsg msg)
			throws ISOException {
		msg.setMTI("0800");
		msg.set(7, new SimpleDateFormat("MMddhhmmss").format(new Date()));
		msg.set(11, getStan());
		msg.set(70, "301");
		
		return msg;
	}
	
	public ISOMsg generatePostingTransactionMessage(ISOMsg msg)
			throws ISOException {
		
		SystemParameter systemParameter3 = getSystemParameterDaoBean().getValueParameter(Constants.PROCESSING_CODE).get(0);
		SystemParameter systemParameter18 = getSystemParameterDaoBean().getValueParameter(Constants.MERCHANT_TYPE).get(0);
		SystemParameter systemParameter22 = getSystemParameterDaoBean().getValueParameter(Constants.POS_ENTRY_MODE).get(0);
		SystemParameter systemParameter32 = getSystemParameterDaoBean().getValueParameter(Constants.ACQUIRING_CODE).get(0);
		SystemParameter systemParameter33 = getSystemParameterDaoBean().getValueParameter(Constants.FORWARDING_CODE).get(0);
		SystemParameter systemParameter41 = getSystemParameterDaoBean().getValueParameter(Constants.ACCEPTOR_IDENTIFICATION).get(0);
		SystemParameter systemParameter42 = getSystemParameterDaoBean().getValueParameter(Constants.ACCEPTOR_CODE).get(0);
		SystemParameter systemParameter43 = getSystemParameterDaoBean().getValueParameter(Constants.ACCEPTOR_NAME).get(0);
		SystemParameter systemParameter48_1 = getSystemParameterDaoBean().getValueParameter(Constants.NARASI1).get(0);
		SystemParameter systemParameter48_2 = getSystemParameterDaoBean().getValueParameter(Constants.NARASI2).get(0);
		SystemParameter systemParameter48_3 = getSystemParameterDaoBean().getValueParameter(Constants.NARASI3).get(0);
		SystemParameter systemParameter49 = getSystemParameterDaoBean().getValueParameter(Constants.CURRENCY_CODE).get(0);
		SystemParameter systemParameter102 = getSystemParameterDaoBean().getValueParameter(Constants.SOURCE_ACC_NUMBER).get(0);

		String stan = getStan();
		msg.setMTI("0201");
		msg.set(3, systemParameter3.getParam_value());
		msg.set(4, CommonUtils.padleft("450", 12, '0')); // hardcode, just for dummy
		msg.set(7, new SimpleDateFormat("MMddhhmmss").format(new Date()));
		msg.set(11, stan);
		msg.set(12, new SimpleDateFormat("HHmmss").format(new Date()));
		msg.set(13, new SimpleDateFormat("MMdd").format(new Date()));
		msg.set(18, systemParameter18.getParam_value());
		msg.set(22, systemParameter22.getParam_value());
		msg.set(32, systemParameter32.getParam_value());
		msg.set(33, systemParameter33.getParam_value());
		msg.set(37, CommonUtils.padleft(stan, 12, '0'));
		msg.set(41, systemParameter41.getParam_value());
		msg.set(42, systemParameter42.getParam_value());
		msg.set(43, systemParameter43.getParam_value());
		msg.set(48, CommonUtils.padright(systemParameter48_1.getParam_value(), 35, ' ') + 
					CommonUtils.padright(systemParameter48_2.getParam_value(), 35, ' ') +
					CommonUtils.padright(systemParameter48_3.getParam_value(), 35, ' '));
		msg.set(49, systemParameter49.getParam_value());
		msg.set(102, systemParameter102.getParam_value());
		msg.set(103, "0060540365100"); // hardcode, just for dummy

		return msg;
	}
	
	public ISOMsg generatePostingTransactionMessage(BulkFTReqPRC prc, ISOMsg msg)
			throws ISOException {
		
		SystemParameter systemParameter3 = getSystemParameterDaoBean().getValueParameter(Constants.PROCESSING_CODE).get(0);
		SystemParameter systemParameter18 = getSystemParameterDaoBean().getValueParameter(Constants.MERCHANT_TYPE).get(0);
		SystemParameter systemParameter22 = getSystemParameterDaoBean().getValueParameter(Constants.POS_ENTRY_MODE).get(0);
		SystemParameter systemParameter32 = getSystemParameterDaoBean().getValueParameter(Constants.ACQUIRING_CODE).get(0);
		SystemParameter systemParameter33 = getSystemParameterDaoBean().getValueParameter(Constants.FORWARDING_CODE).get(0);
		SystemParameter systemParameter41 = getSystemParameterDaoBean().getValueParameter(Constants.ACCEPTOR_IDENTIFICATION).get(0);
		SystemParameter systemParameter42 = getSystemParameterDaoBean().getValueParameter(Constants.ACCEPTOR_CODE).get(0);
		SystemParameter systemParameter43 = getSystemParameterDaoBean().getValueParameter(Constants.ACCEPTOR_NAME).get(0);
		SystemParameter systemParameter48_1 = getSystemParameterDaoBean().getValueParameter(Constants.NARASI1).get(0);
		SystemParameter systemParameter48_2 = getSystemParameterDaoBean().getValueParameter(Constants.NARASI2).get(0);
		SystemParameter systemParameter48_3 = getSystemParameterDaoBean().getValueParameter(Constants.NARASI3).get(0);
		SystemParameter systemParameter49 = getSystemParameterDaoBean().getValueParameter(Constants.CURRENCY_CODE).get(0);

		String stan = getStan();
		msg.setMTI("0200");
		msg.set(3, systemParameter3.getParam_value());
		msg.set(4, CommonUtils.padleft(prc.getDebitamount(), 12, '0'));
		msg.set(7, new SimpleDateFormat("MMddhhmmss").format(new Date()));
		msg.set(11, stan);
		msg.set(12, new SimpleDateFormat("HHmmss").format(new Date()));
		msg.set(13, new SimpleDateFormat("MMdd").format(new Date()));
		msg.set(18, systemParameter18.getParam_value());
		msg.set(22, systemParameter22.getParam_value());
		msg.set(32, systemParameter32.getParam_value());
		msg.set(33, systemParameter33.getParam_value());
		msg.set(37, CommonUtils.padleft(stan, 12, '0'));
		msg.set(41, systemParameter41.getParam_value());
		msg.set(42, systemParameter42.getParam_value());
		msg.set(43, systemParameter43.getParam_value());
		msg.set(48, CommonUtils.padright(systemParameter48_1.getParam_value()
							.replaceAll("<DocumentNumber>", prc.getDocumentNumber())
							.replaceAll("<DocumentDate>", prc.getDocumentDate())
							.replaceAll("<No. Rek. Sumber>", prc.getDebitaccno()), 35, ' ') + 
					CommonUtils.padright(systemParameter48_2.getParam_value()
							.replaceAll("<DocumentNumber>", prc.getDocumentNumber())
							.replaceAll("<DocumentDate>", prc.getDocumentDate())
							.replaceAll("<No. Rek. Sumber>", prc.getDebitaccno()), 35, ' ') +
					CommonUtils.padright(systemParameter48_3.getParam_value()
							.replaceAll("<DocumentNumber>", prc.getDocumentNumber())
							.replaceAll("<DocumentDate>", prc.getDocumentDate())
							.replaceAll("<No. Rek. Sumber>", prc.getDebitaccno()), 35, ' '));
		msg.set(49, systemParameter49.getParam_value());
		msg.set(102, prc.getDebitaccno());
		msg.set(103, prc.getCreditaccno());

		return msg;
	}
	
	public ISOMsg generateRetryTransactionMessage(BulkFTReqPRC prc, ISOMsg msg)
			throws ISOException {
		
		SystemParameter systemParameter3 = getSystemParameterDaoBean().getValueParameter(Constants.PROCESSING_CODE_RETRY).get(0);
		SystemParameter systemParameter18 = getSystemParameterDaoBean().getValueParameter(Constants.MERCHANT_TYPE).get(0);
		SystemParameter systemParameter22 = getSystemParameterDaoBean().getValueParameter(Constants.POS_ENTRY_MODE).get(0);
		SystemParameter systemParameter32 = getSystemParameterDaoBean().getValueParameter(Constants.ACQUIRING_CODE).get(0);
		SystemParameter systemParameter33 = getSystemParameterDaoBean().getValueParameter(Constants.FORWARDING_CODE).get(0);
		SystemParameter systemParameter41 = getSystemParameterDaoBean().getValueParameter(Constants.ACCEPTOR_IDENTIFICATION).get(0);
		SystemParameter systemParameter42 = getSystemParameterDaoBean().getValueParameter(Constants.ACCEPTOR_CODE).get(0);
		SystemParameter systemParameter43 = getSystemParameterDaoBean().getValueParameter(Constants.ACCEPTOR_NAME).get(0);
		SystemParameter systemParameter48_1 = getSystemParameterDaoBean().getValueParameter(Constants.NARASI1).get(0);
		SystemParameter systemParameter48_2 = getSystemParameterDaoBean().getValueParameter(Constants.NARASI2).get(0);
		SystemParameter systemParameter48_3 = getSystemParameterDaoBean().getValueParameter(Constants.NARASI3).get(0);
		SystemParameter systemParameter49 = getSystemParameterDaoBean().getValueParameter(Constants.CURRENCY_CODE).get(0);

		String stan = getStan();
		String prevStan = "";
		String prevTransmission = "";
		try {
			prevStan = prc.getTraceNumber().substring(10);
			prevTransmission = prc.getTraceNumber().substring(0, 10);
		} catch (Exception e) {
			prevStan = stan;
			prevTransmission = new SimpleDateFormat("MMddhhmmss").format(new Date());
			System.out.println("[ISO] No Previous STAN Available, sending retry without prevStan");
		}
		msg.setMTI("0200");
		msg.set(3, systemParameter3.getParam_value());
		msg.set(4, CommonUtils.padleft(prc.getDebitamount(), 12, '0'));
		msg.set(7, prevTransmission);
		msg.set(11, stan);
		msg.set(12, new SimpleDateFormat("HHmmss").format(new Date()));
		msg.set(13, new SimpleDateFormat("MMdd").format(new Date()));
		msg.set(18, systemParameter18.getParam_value());
		msg.set(22, systemParameter22.getParam_value());
		msg.set(32, systemParameter32.getParam_value());
		msg.set(33, systemParameter33.getParam_value());
		msg.set(37, CommonUtils.padleft(prevStan, 12, '0'));
		msg.set(41, systemParameter41.getParam_value());
		msg.set(42, systemParameter42.getParam_value());
		msg.set(43, systemParameter43.getParam_value());
		msg.set(47, prevStan);
		msg.set(48, CommonUtils.padright(systemParameter48_1.getParam_value()
							.replaceAll("<DocumentNumber>", prc.getDocumentNumber())
							.replaceAll("<DocumentDate>", prc.getDocumentDate())
							.replaceAll("<No. Rek. Sumber>", prc.getDebitaccno()), 35, ' ') + 
					CommonUtils.padright(systemParameter48_2.getParam_value()
							.replaceAll("<DocumentNumber>", prc.getDocumentNumber())
							.replaceAll("<DocumentDate>", prc.getDocumentDate())
							.replaceAll("<No. Rek. Sumber>", prc.getDebitaccno()), 35, ' ') +
					CommonUtils.padright(systemParameter48_3.getParam_value()
							.replaceAll("<DocumentNumber>", prc.getDocumentNumber())
							.replaceAll("<DocumentDate>", prc.getDocumentDate())
							.replaceAll("<No. Rek. Sumber>", prc.getDebitaccno()), 35, ' '));
		msg.set(49, systemParameter49.getParam_value());
		msg.set(102, prc.getDebitaccno());
		msg.set(103, prc.getCreditaccno());

		return msg;
	}
	
	public ISOMsg generateRetryPostingMessage(ISOMsg msg, String stan) throws ISOException{
		
		SystemParameter procode = getSystemParameterDaoBean().getValueParameter(Constants.PROCESSING_CODE_RETRY).get(0);
		msg.setPackager(new SAMPackager());
		msg.unset(39);
		msg.setMTI("0200");
		msg.set(3, procode.getParam_value());
		msg.set(7, msg.getString(7));
		msg.set(11, getStan());
		msg.set(47, stan);
		msg.dump(System.out, "RETRY ");
		return msg;
	}
	
	public byte[] generateISOMessageByte(ISOMsg message) throws ISOException {
		// make the iso string
		String iso = new String(message.pack());
//		String iso = "0201B238440188E180000000000006000000450000000000000001111003553629887015553611106010010050011005001100000002988708883    0000000000088830000                                    105SPAN 201611071544490000003         PEMB. GAJI/TUNJ. 2016-11-07        0072932706001                      360130072932706001130016768316100";
		
		// get the length of iso string in byte[]
		byte[] isoLengthByteArray = PubUtil.intTo2ByteArray(iso.length());

		// create iso message to send
		byte[] isoToBeSend = PubUtil.concateByteArray(isoLengthByteArray,
				iso.getBytes());

		return isoToBeSend;
	}
	
	public byte[] generateISOMessageByte(String message) throws ISOException {
		// get the length of iso string in byte[]
		byte[] isoLengthByteArray = PubUtil.intTo2ByteArray(message.length());

		// create iso message to send
		byte[] isoToBeSend = PubUtil.concateByteArray(isoLengthByteArray,
				message.getBytes());

		return isoToBeSend;
	}
	
	public static String getStan() {
		// query stan sequence
		int stansss = bulkFTDao.getNextStan().intValue();
		stan = CommonUtils.padleft(stansss+"", 6, '0');
		return stan;
	}
	
	public static void createDummyRequestPosting(){
		ISOMsg postReq = new ISOMsg();
		postReq.setPackager(new SAMPackager());
		
		ISOHelper h = new ISOHelper();
		try {
			System.out.println("trying to send ...");
			postReq = h.generatePostingTransactionMessage(postReq);
			postReq.dump(System.out, "TRANSACTION ");
			byte[] msgToBeSend = h.generateISOMessageByte(postReq);
			OpenSocketConnector.getConnector().send(msgToBeSend);
			System.out.println("posting transaction sent ...");
		} catch (ISOException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
