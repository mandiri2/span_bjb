package mii.span.iso.util;

import mii.span.model.BulkFTReqPRC;
import mii.span.scheduller.odbc.PostingScheduller;
import mii.span.util.BusinessUtil;
import mii.span.util.GetCurrentDateTime;

import org.apache.log4j.Logger;
import org.jpos.iso.ISOMsg;

import com.mii.constant.Constants;
import com.mii.dao.BulkFTDao;
import com.mii.dao.SpanAccountBalanceLogDao;
import com.mii.dao.SpanPenihilanLogDao;
import com.mii.dao.SystemParameterDAO;

public class PostingService {
	
	static SystemParameterDAO sysParamDao = (SystemParameterDAO) BusinessUtil.getDao("sysParamDAO");
	static String accountNoGaji = sysParamDao.getValueByParamName(Constants.PARAM_DEBITACCNO+Constants.GAJI);
	static String accountNoGajiR = sysParamDao.getValueByParamName(Constants.PARAM_DEBITACCNO+Constants.GAJIR);
	static Logger logger = Logger.getLogger(PostingService.class);
	
	public static String dump(ISOMsg msg, String identifier) {
		msg.dump(System.out, identifier);
		return "";
	}
	
	public static Boolean sendRequestAndUpdateIfFail(byte[] msgToBeSend, BulkFTReqPRC p) {
		BulkFTDao bulkFTDao = (BulkFTDao) BusinessUtil.getDao("bulkFTDao");
		Boolean status = false;
		try {
			// send to if400
			OpenSocketConnector.getConnector().send(msgToBeSend);
			status = true;
		} catch (Exception e) {
			// update status prc => 0
			bulkFTDao.updatePrcStatus(p);
			e.printStackTrace();
		}
		return status;
	}
	
	public static void putRequestAndLogging(ISOMsg msg, BulkFTReqPRC p) {
		BulkFTDao bulkFTDao = (BulkFTDao) BusinessUtil.getDao("bulkFTDao");
		// put to map - will be used in response handler
		PostingScheduller.sentMessage.put(msg.getString(7).concat(msg.getString(11)), p);
		// insert log request
		bulkFTDao.insertReqLog(msg, "normal");
	}
	
	/**
	 * checking whether response exist in cme_bulk_ft_resp, if exist then update.
	 * @param prc - element to be check in cme_bulk_ft_resp.
	 * @param msg - component will be insert in.
	 */
	public static void insertOrUpdateRSP(BulkFTReqPRC prc, ISOMsg msg) {
		BulkFTDao bulkFTDao = (BulkFTDao) BusinessUtil.getDao("bulkFTDao");
		// checking rsp exist?
		if (bulkFTDao.checkResponseExist(prc).intValue() > 0){
			// update resp response code, tracenumber
			bulkFTDao.updateRSP(prc, msg);
			logger.info("Success update PRC ..." + prc.getTrxdetailid());
		}
		else {
			// insert response to cme_bulk_ft_resp
			bulkFTDao.insertRSP(prc, msg);
			logger.info("Success insert PRC ..." + prc.getTrxdetailid());
		}
	}
	
	public static void updateLogPenihilan(BulkFTReqPRC prc, ISOMsg msg) {
		SpanPenihilanLogDao spanPenihilanDao = (SpanPenihilanLogDao) BusinessUtil.getDao("spanPenihilanLogDao");
		if (prc.getBatchid().startsWith("NHL")) {
			// update span_penihilan log
			logger.info("[INFO] Updating span penihilan log ...");
			spanPenihilanDao.updatePenihilanLog(prc, msg);
		}
	}
	
	public static void updateBeginBalance(BulkFTReqPRC prc) {
		SpanAccountBalanceLogDao spanAccountBalanceLogDao = (SpanAccountBalanceLogDao) BusinessUtil.getDao("spanAccountBalanceLogDao");
		String accountType = getAccountType(prc);
		String inquiryDate = GetCurrentDateTime.getCurrentDateString("yyyy-MM-dd");
		spanAccountBalanceLogDao.updateBeginBalance("0", accountType, inquiryDate);
		logger.info("Success updating begining balance ...");
	}
	
	public static String getAccountType(BulkFTReqPRC prc) {
		String accountType = "";
		if (prc.getDebitaccno().equals(accountNoGaji))
			accountType = "GAJI";
		if (prc.getDebitaccno().equals(accountNoGajiR))
			accountType = "GAJIR";
		
		return accountType;
	}

}
