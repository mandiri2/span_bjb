package mii.span.iso.util;

import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

@SuppressWarnings("serial")
public class InitConnector extends HttpServlet {

	Logger logger = Logger.getAnonymousLogger();

	@SuppressWarnings("static-access")
	@Override
	public void init() throws ServletException {
		super.init();

		logger.info("Try to connect to switching ... ");
		// try to connect to switching
		System.out.println("==================================");
		System.out.println();

		BaseConfig cfg = BaseConfig.getBaseConfig();
		System.out.println(cfg);
		OpenSocketConnector connector = OpenSocketConnector.getConnector();
		connector.setBaseconfig(cfg);
		connector.start();
		System.out.println("Connecting to switching running on background");

		System.out.println();
		System.out.println("==================================");

	}
	
	@Override
	public void destroy() {
		super.destroy();
		System.out.println("Destroying SocketConnector ...");
		OpenSocketConnector.getConnector().interrupt();
		System.out.println("Destroying SocketConnector done...");
	}

}
