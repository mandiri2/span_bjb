package mii.span.iso.model;

public class ReinquiryMessage {

	private String stan;
	private String transmissionDate;
	private String request;
	
	public ReinquiryMessage() {}
	
	public ReinquiryMessage(String stan, String request, String transmissionDate) {
		this.stan = stan;
		this.transmissionDate = transmissionDate;
		this.request = request;
	}

	public String getStan() {
		return stan;
	}

	public void setStan(String stan) {
		this.stan = stan;
	}

	public String getTransmissionDate() {
		return transmissionDate;
	}

	public void setTransmissionDate(String transmissionDate) {
		this.transmissionDate = transmissionDate;
	}

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

}
