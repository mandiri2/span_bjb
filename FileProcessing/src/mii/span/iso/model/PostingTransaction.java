package mii.span.iso.model;

public class PostingTransaction {

	private String processingCode;
	private String amountTransaction;
	private String transmissionDateTime;
	private String systemTraceAuditNumber;
	private String timeLocalTransaction;
	private String dateLocalTransaction;
	private String merchantType;
	private String posEntryModeCode;
	private String acquiringInstitutionCode;
	private String forwardingInstitutionCode;
	private String retrievalReferenceNumber;
	private String responseCode;
	private String terminalIdentificationNumber;
	private String terminalName;
	private String cardAcceptorName;
	private String additionalData;
	private String currencyCode;
	private String sourceAccountNumber;
	private String destinationAccountNumber;

	public String getProcessingCode() {
		return processingCode;
	}

	public void setProcessingCode(String processingCode) {
		this.processingCode = processingCode;
	}

	public String getAmountTransaction() {
		return amountTransaction;
	}

	public void setAmountTransaction(String amountTransaction) {
		this.amountTransaction = amountTransaction;
	}

	public String getTransmissionDateTime() {
		return transmissionDateTime;
	}

	public void setTransmissionDateTime(String transmissionDateTime) {
		this.transmissionDateTime = transmissionDateTime;
	}

	public String getSystemTraceAuditNumber() {
		return systemTraceAuditNumber;
	}

	public void setSystemTraceAuditNumber(String systemTraceAuditNumber) {
		this.systemTraceAuditNumber = systemTraceAuditNumber;
	}

	public String getTimeLocalTransaction() {
		return timeLocalTransaction;
	}

	public void setTimeLocalTransaction(String timeLocalTransaction) {
		this.timeLocalTransaction = timeLocalTransaction;
	}

	public String getDateLocalTransaction() {
		return dateLocalTransaction;
	}

	public void setDateLocalTransaction(String dateLocalTransaction) {
		this.dateLocalTransaction = dateLocalTransaction;
	}

	public String getMerchantType() {
		return merchantType;
	}

	public void setMerchantType(String merchantType) {
		this.merchantType = merchantType;
	}

	public String getPosEntryModeCode() {
		return posEntryModeCode;
	}

	public void setPosEntryModeCode(String posEntryModeCode) {
		this.posEntryModeCode = posEntryModeCode;
	}

	public String getAcquiringInstitutionCode() {
		return acquiringInstitutionCode;
	}

	public void setAcquiringInstitutionCode(String acquiringInstitutionCode) {
		this.acquiringInstitutionCode = acquiringInstitutionCode;
	}

	public String getForwardingInstitutionCode() {
		return forwardingInstitutionCode;
	}

	public void setForwardingInstitutionCode(String forwardingInstitutionCode) {
		this.forwardingInstitutionCode = forwardingInstitutionCode;
	}

	public String getRetrievalReferenceNumber() {
		return retrievalReferenceNumber;
	}

	public void setRetrievalReferenceNumber(String retrievalReferenceNumber) {
		this.retrievalReferenceNumber = retrievalReferenceNumber;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getTerminalIdentificationNumber() {
		return terminalIdentificationNumber;
	}

	public void setTerminalIdentificationNumber(
			String terminalIdentificationNumber) {
		this.terminalIdentificationNumber = terminalIdentificationNumber;
	}

	public String getTerminalName() {
		return terminalName;
	}

	public void setTerminalName(String terminalName) {
		this.terminalName = terminalName;
	}

	public String getCardAcceptorName() {
		return cardAcceptorName;
	}

	public void setCardAcceptorName(String cardAcceptorName) {
		this.cardAcceptorName = cardAcceptorName;
	}

	public String getAdditionalData() {
		return additionalData;
	}

	public void setAdditionalData(String additionalData) {
		this.additionalData = additionalData;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getSourceAccountNumber() {
		return sourceAccountNumber;
	}

	public void setSourceAccountNumber(String sourceAccountNumber) {
		this.sourceAccountNumber = sourceAccountNumber;
	}

	public String getDestinationAccountNumber() {
		return destinationAccountNumber;
	}

	public void setDestinationAccountNumber(String destinationAccountNumber) {
		this.destinationAccountNumber = destinationAccountNumber;
	}

}
