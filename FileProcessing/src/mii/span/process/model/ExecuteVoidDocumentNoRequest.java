package mii.span.process.model;

public class ExecuteVoidDocumentNoRequest {
	private String fileName,
		xmlFileName;
	
	/**GETTER SETTER**/
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getXmlFileName() {
		return xmlFileName;
	}

	public void setXmlFileName(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}
	
	
}
