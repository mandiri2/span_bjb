package mii.span.process.model;

import java.util.ArrayList;
import java.util.List;

import mii.span.doc.FileSFTP;

public class DataFiles {
	/**
	 * [0]List List<FileSFTP>	<br />
	 * [1]String Status <br />
	 * [2]String ErrorMessage <br />
	 * [3]String FTP_STATUS <br />
	 * @return
	 */
	private List<FileSFTP> files = new ArrayList<FileSFTP>();
	private String status;
	private String errorMessage;
	private String ftpStatus;
	public DataFiles(String status, String errorMessage, List<FileSFTP> files) {
		this.status=status;
		this.errorMessage=errorMessage;
		this.files=files;
	}
	public DataFiles(){}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public String getFtpStatus() {
		return ftpStatus;
	}
	public void setFtpStatus(String ftpStatus) {
		this.ftpStatus = ftpStatus;
	}
	public List<FileSFTP> getFiles() {
		return files;
	}
	public void setFiles(List<FileSFTP> files) {
		this.files = files;
	}
	public boolean isSuccess() {
		return "SUCCESS".equalsIgnoreCase(status);
	}
	public boolean isValid() {
		return "valid".equalsIgnoreCase(status);
	}
}
