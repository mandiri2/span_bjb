package mii.span.process.model;

import java.util.List;

public class MappingAckExecutedRequest {

	public List<ACKExecutedDoc> ackExecutedDoc;
	public String filename;
	public String forcedACK;
	
	public List<ACKExecutedDoc> getAckExecutedDoc() {
		return ackExecutedDoc;
	}
	public void setAckExecutedDoc(List<ACKExecutedDoc> ackExecutedDoc) {
		this.ackExecutedDoc = ackExecutedDoc;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getForcedACK() {
		return forcedACK;
	}
	public void setForcedACK(String forcedACK) {
		this.forcedACK = forcedACK;
	}
	
}
