package mii.span.process.model;

public class GetTotalRecAmtSendToHostResponse {
	private String totalRecSendToHost,
		totalAmtSendToHost;
	
	/**GETTER SETTER**/
	public String getTotalRecSendToHost() {
		return totalRecSendToHost;
	}

	public void setTotalRecSendToHost(String totalRecSendToHost) {
		this.totalRecSendToHost = totalRecSendToHost;
	}

	public String getTotalAmtSendToHost() {
		return totalAmtSendToHost;
	}

	public void setTotalAmtSendToHost(String totalAmtSendToHost) {
		this.totalAmtSendToHost = totalAmtSendToHost;
	}
	
}
