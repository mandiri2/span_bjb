package mii.span.process.model;

public class ValidateSpanDataResponse {
	private String status,
		errorMessage,
		spanProviderName,
		spanProviderCode,
		spanProviderAcct,
		documentDate;
	
	/**getter setter**/
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getSpanProviderName() {
		return spanProviderName;
	}

	public void setSpanProviderName(String spanProviderName) {
		this.spanProviderName = spanProviderName;
	}

	public String getSpanProviderCode() {
		return spanProviderCode;
	}

	public void setSpanProviderCode(String spanProviderCode) {
		this.spanProviderCode = spanProviderCode;
	}

	public String getSpanProviderAcct() {
		return spanProviderAcct;
	}

	public void setSpanProviderAcct(String spanProviderAcct) {
		this.spanProviderAcct = spanProviderAcct;
	}

	public String getDocumentDate() {
		return documentDate;
	}

	public void setDocumentDate(String documentDate) {
		this.documentDate = documentDate;
	}
	
	
}
