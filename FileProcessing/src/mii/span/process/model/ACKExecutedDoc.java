package mii.span.process.model;

import java.io.Serializable;

public class ACKExecutedDoc implements Serializable {
	private static final long serialVersionUID = 1L;
	public String ERROR_CODE,
	ERROR_MESSAGE,
	DOC_DATE,
	DOC_NUMBER,
	DOC_TYPE;

	public String getERROR_CODE() {
		return ERROR_CODE;
	}

	public void setERROR_CODE(String eRROR_CODE) {
		ERROR_CODE = eRROR_CODE;
	}

	public String getERROR_MESSAGE() {
		return ERROR_MESSAGE;
	}

	public void setERROR_MESSAGE(String eRROR_MESSAGE) {
		ERROR_MESSAGE = eRROR_MESSAGE;
	}

	public String getDOC_DATE() {
		return DOC_DATE;
	}

	public void setDOC_DATE(String dOC_DATE) {
		DOC_DATE = dOC_DATE;
	}

	public String getDOC_NUMBER() {
		return DOC_NUMBER;
	}

	public void setDOC_NUMBER(String dOC_NUMBER) {
		DOC_NUMBER = dOC_NUMBER;
	}

	public String getDOC_TYPE() {
		return DOC_TYPE;
	}

	public void setDOC_TYPE(String dOC_TYPE) {
		DOC_TYPE = dOC_TYPE;
	}
}
