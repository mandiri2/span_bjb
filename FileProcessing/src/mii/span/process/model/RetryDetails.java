package mii.span.process.model;

public class RetryDetails {
	private String totalRetry,
		addRetryStatement,
		totalRetryAmount;
	
	public String getTotalRetry() {
		return totalRetry;
	}

	public void setTotalRetry(String totalRetry) {
		this.totalRetry = totalRetry;
	}

	public String getAddRetryStatement() {
		return addRetryStatement;
	}

	public void setAddRetryStatement(String addRetryStatement) {
		this.addRetryStatement = addRetryStatement;
	}

	public String getTotalRetryAmount() {
		return totalRetryAmount;
	}

	public void setTotalRetryAmount(String totalRetryAmount) {
		this.totalRetryAmount = totalRetryAmount;
	}
	
	
}
