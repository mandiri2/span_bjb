package mii.span.process.model;

public class PopulateSP2DNoDetailsForVoidOutput {
	private  String sp2dNO
	,totalAmount
	,totalRecord
	,debitAccount
	,filename
	,documentDate
	,debitAccountType
	,procStatusFile
	,procStatusSP2DNO
	,voidFlag;

	public String getSp2dNO() {
		return sp2dNO;
	}

	public void setSp2dNO(String sp2dNO) {
		this.sp2dNO = sp2dNO;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getTotalRecord() {
		return totalRecord;
	}

	public void setTotalRecord(String totalRecord) {
		this.totalRecord = totalRecord;
	}

	public String getDebitAccount() {
		return debitAccount;
	}

	public void setDebitAccount(String debitAccount) {
		this.debitAccount = debitAccount;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getDocumentDate() {
		return documentDate;
	}

	public void setDocumentDate(String documentDate) {
		this.documentDate = documentDate;
	}

	public String getDebitAccountType() {
		return debitAccountType;
	}

	public void setDebitAccountType(String debitAccountType) {
		this.debitAccountType = debitAccountType;
	}

	public String getProcStatusFile() {
		return procStatusFile;
	}

	public void setProcStatusFile(String procStatusFile) {
		this.procStatusFile = procStatusFile;
	}

	public String getProcStatusSP2DNO() {
		return procStatusSP2DNO;
	}

	public void setProcStatusSP2DNO(String procStatusSP2DNO) {
		this.procStatusSP2DNO = procStatusSP2DNO;
	}

	public String getVoidFlag() {
		return voidFlag;
	}

	public void setVoidFlag(String voidFlag) {
		this.voidFlag = voidFlag;
	}
}
