package mii.span.process.model;

public class ExecuteVoidDocumentNoResponse {
	private String status,
		errorMessage;
	
	/**GETTER SETTER**/
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
}
