package mii.span.process.model;

public class SuccessDetail {
	private String totalSuccess;

	public String getTotalSuccess() {
		return totalSuccess;
	}

	public void setTotalSuccess(String totalSuccess) {
		this.totalSuccess = totalSuccess;
	}
}
