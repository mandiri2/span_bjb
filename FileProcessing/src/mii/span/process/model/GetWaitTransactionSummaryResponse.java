package mii.span.process.model;

public class GetWaitTransactionSummaryResponse {
	private String totalWaitRec,
		totalWaitAmount;
	
	/**GETTER SETTER**/
	public String getTotalWaitRec() {
		return totalWaitRec;
	}

	public void setTotalWaitRec(String totalWaitRec) {
		this.totalWaitRec = totalWaitRec;
	}

	public String getTotalWaitAmount() {
		return totalWaitAmount;
	}

	public void setTotalWaitAmount(String totalWaitAmount) {
		this.totalWaitAmount = totalWaitAmount;
	}
	
	
}
