package mii.span.process.model;

public class InsertSPANLogTrxUserResponse {
	private String oSTATUS,
		oERRORMESSAGE;
	
	public InsertSPANLogTrxUserResponse(){
		
	}
	
	public InsertSPANLogTrxUserResponse(String oSTATUS, String oERRORMESSAGE){
		super();
		this.oSTATUS = oSTATUS;
		this.oERRORMESSAGE = oERRORMESSAGE;
	}
	
	public String getoSTATUS() {
		return oSTATUS;
	}

	public void setoSTATUS(String oSTATUS) {
		this.oSTATUS = oSTATUS;
	}

	public String getoERRORMESSAGE() {
		return oERRORMESSAGE;
	}

	public void setoERRORMESSAGE(String oERRORMESSAGE) {
		this.oERRORMESSAGE = oERRORMESSAGE;
	}
	
	
}
