package mii.span.process.model;

public class SpanAckAcceptedResponse {
	private String status,
		errorMessage,
		ackDataString,
		documentType;
	
	/**getter setter**/
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getAckDataString() {
		return ackDataString;
	}

	public void setAckDataString(String ackDataString) {
		this.ackDataString = ackDataString;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}
	
	
}
