package mii.span.process.model;

public class MappingDataHostResponseDoc {
	private String Status,ErrorMessage,statusChange,sknBankCode,sknSandiBank;

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public String getErrorMessage() {
		return ErrorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		ErrorMessage = errorMessage;
	}

	public String getStatusChange() {
		return statusChange;
	}

	public void setStatusChange(String statusChange) {
		this.statusChange = statusChange;
	}

	public String getSknSandiBank() {
		return sknSandiBank;
	}

	public void setSknSandiBank(String sknSandiBank) {
		this.sknSandiBank = sknSandiBank;
	}

	public String getSknBankCode() {
		return sknBankCode;
	}

	public void setSknBankCode(String sknBankCode) {
		this.sknBankCode = sknBankCode;
	}
	
}
