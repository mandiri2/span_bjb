package mii.span.process.model;

import mii.span.doc.ACKDataDocument;

public class MappingDataACKSpanRequest {
	public ACKDataDocument  ackDataDocument;

	public ACKDataDocument getAckDataDocument() {
		return ackDataDocument;
	}

	public void setAckDataDocument(ACKDataDocument ackDataDocument) {
		this.ackDataDocument = ackDataDocument;
	}
	
}
