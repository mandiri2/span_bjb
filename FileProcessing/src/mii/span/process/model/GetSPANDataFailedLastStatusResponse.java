package mii.span.process.model;

public class GetSPANDataFailedLastStatusResponse {
	private String totalSuccess,
		totalRetry,
		totalRetur,
		totalWait,
		sumStatusWat,
		sumStatusRty,
		sumStatusRtu,
		totalProcess,
		legstatus;
	
	/**GETTER SETTER**/
	public String getTotalSuccess() {
		return totalSuccess;
	}

	public void setTotalSuccess(String totalSuccess) {
		this.totalSuccess = totalSuccess;
	}

	public String getTotalRetry() {
		return totalRetry;
	}

	public void setTotalRetry(String totalRetry) {
		this.totalRetry = totalRetry;
	}

	public String getTotalRetur() {
		return totalRetur;
	}

	public void setTotalRetur(String totalRetur) {
		this.totalRetur = totalRetur;
	}

	public String getTotalWait() {
		return totalWait;
	}

	public void setTotalWait(String totalWait) {
		this.totalWait = totalWait;
	}

	public String getSumStatusWat() {
		return sumStatusWat;
	}

	public void setSumStatusWat(String sumStatusWat) {
		this.sumStatusWat = sumStatusWat;
	}

	public String getSumStatusRty() {
		return sumStatusRty;
	}

	public void setSumStatusRty(String sumStatusRty) {
		this.sumStatusRty = sumStatusRty;
	}

	public String getSumStatusRtu() {
		return sumStatusRtu;
	}

	public void setSumStatusRtu(String sumStatusRtu) {
		this.sumStatusRtu = sumStatusRtu;
	}

	public String getTotalProcess() {
		return totalProcess;
	}

	public void setTotalProcess(String totalProcess) {
		this.totalProcess = totalProcess;
	}

	public String getLegstatus() {
		return legstatus;
	}

	public void setLegstatus(String legstatus) {
		this.legstatus = legstatus;
	}
	
	
}
