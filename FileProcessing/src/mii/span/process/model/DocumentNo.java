package mii.span.process.model;

public class DocumentNo {
	private String filename,
		beneficiaryName,
		documentNumber,
		documentDate,
		beneficiaryAccount,
		amount,
		agentBankAccountNumber;

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getBeneficiaryName() {
		return beneficiaryName;
	}

	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}

	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	public String getDocumentDate() {
		return documentDate;
	}

	public void setDocumentDate(String documentDate) {
		this.documentDate = documentDate;
	}

	public String getBeneficiaryAccount() {
		return beneficiaryAccount;
	}

	public void setBeneficiaryAccount(String beneficiaryAccount) {
		this.beneficiaryAccount = beneficiaryAccount;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getAgentBankAccountNumber() {
		return agentBankAccountNumber;
	}

	public void setAgentBankAccountNumber(String agentBankAccountNumber) {
		this.agentBankAccountNumber = agentBankAccountNumber;
	}
	
	
}
