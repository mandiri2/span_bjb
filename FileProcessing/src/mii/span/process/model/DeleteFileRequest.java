package mii.span.process.model;

public class DeleteFileRequest {
	private String nameOfFileToDelete,
		serverName,
		spanPath,
		password,
		username;
	
	public String getNameOfFileToDelete() {
		return nameOfFileToDelete;
	}

	public void setNameOfFileToDelete(String nameOfFileToDelete) {
		this.nameOfFileToDelete = nameOfFileToDelete;
	}

	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	public String getSpanPath() {
		return spanPath;
	}

	public void setSpanPath(String spanPath) {
		this.spanPath = spanPath;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
}
