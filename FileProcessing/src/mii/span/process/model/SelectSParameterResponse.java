package mii.span.process.model;

public class SelectSParameterResponse {
	private String value,
		description;
	
	/**GETTER SETTER**/
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
