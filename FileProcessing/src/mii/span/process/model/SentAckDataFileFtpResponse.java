package mii.span.process.model;

public class SentAckDataFileFtpResponse {
	private String status,
		errorMessage,
		ackFileName;
	
	/**getter setter**/
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getAckFileName() {
		return ackFileName;
	}

	public void setAckFileName(String ackFileName) {
		this.ackFileName = ackFileName;
	}
	
}
