package mii.span.process.model;

import java.util.List;

import mii.span.doc.SP2DDocument;
import mii.span.model.Provider;

public class ValidateSpanDataRequest {
	SP2DDocument spanDocument;
	List<Provider> spanBankAcct;
	String lateFileHour;
	
	/*getter setter*/
	public SP2DDocument getSpanDocument() {
		return spanDocument;
	}
	public void setSpanDocument(SP2DDocument spanDocument) {
		this.spanDocument = spanDocument;
	}
	public List<Provider> getSpanBankAcct() {
		return spanBankAcct;
	}
	public void setSpanBankAcct(List<Provider> spanBankAcct) {
		this.spanBankAcct = spanBankAcct;
	}
	public String getLateFileHour() {
		return lateFileHour;
	}
	public void setLateFileHour(String lateFileHour) {
		this.lateFileHour = lateFileHour;
	}
}
