package mii.span.process.model;

import java.util.List;

import mii.span.doc.FileSFTP;

public class GetDataFilesResponse {
	private List<FileSFTP> files;
	private String status,
		errorMessage,
		ftpStatus;
	
	/**getter setter**/
	public List<FileSFTP> getFiles() {
		return files;
	}
	public void setFiles(List<FileSFTP> files) {
		this.files = files;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public String getFtpStatus() {
		return ftpStatus;
	}
	public void setFtpStatus(String ftpStatus) {
		this.ftpStatus = ftpStatus;
	}
	
	
}
