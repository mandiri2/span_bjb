package mii.span.process.model;

public class SentAckDataFileFtpRequest {
	private String filename,
		documentType,
		errorMessage,
		errorCode,
		ackDataString;
	
	/**getter setter**/
	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getAckDataString() {
		return ackDataString;
	}

	public void setAckDataString(String ackDataString) {
		this.ackDataString = ackDataString;
	}
	
	
}
