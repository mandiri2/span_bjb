package mii.span.process.model;

import java.util.List;

import mii.span.model.Provider;

public class ProviderInfo {

	private List<Provider> resultList;
	private Provider resultDoc;
	
	public List<Provider> getResultList() {
		return resultList;
	}
	public void setResultList(List<Provider> resultList) {
		this.resultList = resultList;
	}
	public Provider getResultDoc() {
		return resultDoc;
	}
	public void setResultDoc(Provider resultDoc) {
		this.resultDoc = resultDoc;
	}
	
}
