package mii.span.process.model;

import mii.span.doc.SP2DDocument;

public class SpanAckAcceptedRequest {
	private String errorMessage,
		errorCode,
		fileName;
	
	private SP2DDocument spanDocument;
	
	/**getter setter**/
	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public SP2DDocument getSpanDocument() {
		return spanDocument;
	}

	public void setSpanDocument(SP2DDocument spanDocument) {
		this.spanDocument = spanDocument;
	}

}
