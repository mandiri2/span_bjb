package mii.span.process.model;

public class GetSCFirstDataResponse {
	private String BATCHID,
		TRXHEADERID,
		RETRY_STATUS,
		RETUR_STATUS,
		SUCCESS_STATUS,
		CREATE_DATE,
		SUM_STATUS_RTY,
		SUM_STATUS_RTU,
		TRXTYPE;

	public String getBATCHID() {
		return BATCHID;
	}

	public void setBATCHID(String bATCHID) {
		BATCHID = bATCHID;
	}

	public String getTRXHEADERID() {
		return TRXHEADERID;
	}

	public void setTRXHEADERID(String tRXHEADERID) {
		TRXHEADERID = tRXHEADERID;
	}

	public String getRETRY_STATUS() {
		return RETRY_STATUS;
	}

	public void setRETRY_STATUS(String rETRY_STATUS) {
		RETRY_STATUS = rETRY_STATUS;
	}

	public String getRETUR_STATUS() {
		return RETUR_STATUS;
	}

	public void setRETUR_STATUS(String rETUR_STATUS) {
		RETUR_STATUS = rETUR_STATUS;
	}

	public String getSUCCESS_STATUS() {
		return SUCCESS_STATUS;
	}

	public void setSUCCESS_STATUS(String sUCCESS_STATUS) {
		SUCCESS_STATUS = sUCCESS_STATUS;
	}

	public String getCREATE_DATE() {
		return CREATE_DATE;
	}

	public void setCREATE_DATE(String cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}

	public String getSUM_STATUS_RTY() {
		return SUM_STATUS_RTY;
	}

	public void setSUM_STATUS_RTY(String sUM_STATUS_RTY) {
		SUM_STATUS_RTY = sUM_STATUS_RTY;
	}

	public String getSUM_STATUS_RTU() {
		return SUM_STATUS_RTU;
	}

	public void setSUM_STATUS_RTU(String sUM_STATUS_RTU) {
		SUM_STATUS_RTU = sUM_STATUS_RTU;
	}

	public String getTRXTYPE() {
		return TRXTYPE;
	}

	public void setTRXTYPE(String tRXTYPE) {
		TRXTYPE = tRXTYPE;
	}
}
