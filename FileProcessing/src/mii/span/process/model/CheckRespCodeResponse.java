package mii.span.process.model;

public class CheckRespCodeResponse {

	public String errorCode,errorMessage,returMark;

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getReturMark() {
		return returMark;
	}

	public void setReturMark(String returMark) {
		this.returMark = returMark;
	}
}
