package mii.span.process.model;

public class PopulateSP2DNoDetailsForVoidInput {
	private String documentNO
	,amount
	,debitAccount
	,filename
	,documentDate
	,debitAccountType
	,procStatusFile
	,procStatusSP2DNO;

	public String getDocumentNO() {
		return documentNO;
	}

	public void setDocumentNO(String documentNO) {
		this.documentNO = documentNO;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getDebitAccount() {
		return debitAccount;
	}

	public void setDebitAccount(String debitAccount) {
		this.debitAccount = debitAccount;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getDocumentDate() {
		return documentDate;
	}

	public void setDocumentDate(String documentDate) {
		this.documentDate = documentDate;
	}

	public String getDebitAccountType() {
		return debitAccountType;
	}

	public void setDebitAccountType(String debitAccountType) {
		this.debitAccountType = debitAccountType;
	}

	public String getProcStatusFile() {
		return procStatusFile;
	}

	public void setProcStatusFile(String procStatusFile) {
		this.procStatusFile = procStatusFile;
	}

	public String getProcStatusSP2DNO() {
		return procStatusSP2DNO;
	}

	public void setProcStatusSP2DNO(String procStatusSP2DNO) {
		this.procStatusSP2DNO = procStatusSP2DNO;
	}
}
