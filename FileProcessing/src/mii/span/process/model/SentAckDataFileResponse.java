package mii.span.process.model;

public class SentAckDataFileResponse {
	private String status,
		errorMessage,
		ackFileName,
		statusSend;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getAckFileName() {
		return ackFileName;
	}

	public void setAckFileName(String ackFileName) {
		this.ackFileName = ackFileName;
	}

	public String getStatusSend() {
		return statusSend;
	}

	public void setStatusSend(String statusSend) {
		this.statusSend = statusSend;
	}
	
}
