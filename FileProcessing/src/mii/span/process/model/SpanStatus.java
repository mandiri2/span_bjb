package mii.span.process.model;

public class SpanStatus {
	private String waitStatus,
		procStatus,
		procDescription,
		responseCode;
	
	/**GETTER SETTER**/
	public String getWaitStatus() {
		return waitStatus;
	}

	public void setWaitStatus(String waitStatus) {
		this.waitStatus = waitStatus;
	}

	public String getProcStatus() {
		return procStatus;
	}

	public void setProcStatus(String procStatus) {
		this.procStatus = procStatus;
	}

	public String getProcDescription() {
		return procDescription;
	}

	public void setProcDescription(String procDescription) {
		this.procDescription = procDescription;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	
	
}
