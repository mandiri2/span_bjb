package mii.span.process.model;

public class ReplaceMvaAccResponse {
	private String realAccount,
		status,
		mvaCompanyCode,
		trxDetailIdForMva;
	
	/**GETTER SETTER**/
	public String getRealAccount() {
		return realAccount;
	}

	public void setRealAccount(String realAccount) {
		this.realAccount = realAccount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMvaCompanyCode() {
		return mvaCompanyCode;
	}

	public void setMvaCompanyCode(String mvaCompanyCode) {
		this.mvaCompanyCode = mvaCompanyCode;
	}

	public String getTrxDetailIdForMva() {
		return trxDetailIdForMva;
	}

	public void setTrxDetailIdForMva(String trxDetailIdForMva) {
		this.trxDetailIdForMva = trxDetailIdForMva;
	}
	
	
}
