package mii.span.process.model;

public class GetExecutedTransactionSummaryResponse {
	private String totalExecutedRec,
		totalExecutedAmount;
	
	/**GETTER SETTER**/
	public String getTotalExecutedRec() {
		return totalExecutedRec;
	}

	public void setTotalExecutedRec(String totalExecutedRec) {
		this.totalExecutedRec = totalExecutedRec;
	}

	public String getTotalExecutedAmount() {
		return totalExecutedAmount;
	}

	public void setTotalExecutedAmount(String totalExecutedAmount) {
		this.totalExecutedAmount = totalExecutedAmount;
	}
	
	
}
