package mii.span.process.model;

public class CheckingAmountTrxRequest {
	private String BatchID,
		TrxHeaderID,
		fileName,
		trxType,
		totalRecord,
		totalAmount,
		totalRecSendToHost,
		totalAmtSendToHost,
		waitStatus,
		retryStatus,
		returStatus,
		legStatus;
	
	/**GETTER SETTER**/
	public String getBatchID() {
		return BatchID;
	}

	public void setBatchID(String batchID) {
		BatchID = batchID;
	}

	public String getTrxHeaderID() {
		return TrxHeaderID;
	}

	public void setTrxHeaderID(String trxHeaderID) {
		TrxHeaderID = trxHeaderID;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getTrxType() {
		return trxType;
	}

	public void setTrxType(String trxType) {
		this.trxType = trxType;
	}

	public String getTotalRecord() {
		return totalRecord;
	}

	public void setTotalRecord(String totalRecord) {
		this.totalRecord = totalRecord;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getTotalRecSendToHost() {
		return totalRecSendToHost;
	}

	public void setTotalRecSendToHost(String totalRecSendToHost) {
		this.totalRecSendToHost = totalRecSendToHost;
	}

	public String getTotalAmtSendToHost() {
		return totalAmtSendToHost;
	}

	public void setTotalAmtSendToHost(String totalAmtSendToHost) {
		this.totalAmtSendToHost = totalAmtSendToHost;
	}

	public String getWaitStatus() {
		return waitStatus;
	}

	public void setWaitStatus(String waitStatus) {
		this.waitStatus = waitStatus;
	}

	public String getRetryStatus() {
		return retryStatus;
	}

	public void setRetryStatus(String retryStatus) {
		this.retryStatus = retryStatus;
	}

	public String getReturStatus() {
		return returStatus;
	}

	public void setReturStatus(String returStatus) {
		this.returStatus = returStatus;
	}

	public String getLegStatus() {
		return legStatus;
	}

	public void setLegStatus(String legStatus) {
		this.legStatus = legStatus;
	}
	
	
}
