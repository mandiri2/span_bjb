package mii.span.atomic.response.odbc.ack;

import java.util.List;

import mii.span.doc.ACKDataDocument;
import mii.span.doc.ApplicationArea;
import mii.span.doc.DataArea;
import mii.span.doc.SP2DDocument;
import mii.span.process.model.SpanAckAcceptedRequest;
import mii.span.process.model.SpanAckAcceptedResponse;
import mii.span.util.Atomic;
import mii.span.util.BusinessUtil;

public class Accepted {
	
	/**
	 * mandiri.span.atomic.response.odbc.ack.accepted:spanACKAccepted <br />
	 * return String[] <br />
	 * String[0] Status <br />
	 * String[1] ErrorMessage <br />
	 * String[2] ACKDataString <br />
	 * String[3] DocumentType <br />
	 * @throws Exception 
	 * 
	 */
	public static SpanAckAcceptedResponse spanACKAccepted(SpanAckAcceptedRequest spanAckAcceptedRequest) throws Exception{
		SpanAckAcceptedResponse response = new SpanAckAcceptedResponse();
		
		String Status = null;
		String ACKDataString = null;
		String ErrorMessage = spanAckAcceptedRequest.getErrorMessage();
		String DocumentType = null;
		String ErrorCode = spanAckAcceptedRequest.getErrorCode();
		SP2DDocument spanDocument = spanAckAcceptedRequest.getSpanDocument();
		String fileName = spanAckAcceptedRequest.getFileName();
		
		String[] mappingACKAcceptedResult = mappingACKAccepted(ErrorCode, ErrorMessage, spanDocument, fileName);
		
		ACKDataString = BusinessUtil.stringIfNotNull(mappingACKAcceptedResult[0]);
		Status = BusinessUtil.stringIfNotNull(mappingACKAcceptedResult[1]);
		ErrorMessage = BusinessUtil.stringIfNotNull(mappingACKAcceptedResult[2]);
		DocumentType = BusinessUtil.stringIfNotNull(mappingACKAcceptedResult[3]);
		
		response.setStatus(Status);
		response.setErrorMessage(ErrorMessage);
		response.setAckDataString(ACKDataString);
		response.setDocumentType(DocumentType);
		
		return response;
	}
	
	/**
	 * mandiri.span.atomic.response.odbc.ack.accepted:mappingACKAccepted <br />
	 * return String[] <br />
	 * String[0] ACKDataString <br />
	 * String[1] Status <br />
	 * String[2] ErrorMessage <br />
	 * String[3] DocumentType <br />
	 * @throws Exception 
	 */
	public static String[] mappingACKAccepted(String ErrorCode, String ErrorMessage, SP2DDocument spanDocument, String fileName) throws Exception{
		String[] strings = new String[4];
		String ACKDataString = null;
		String Status = null;
		String DocumentType = null;
		
		ACKDataString = "";
		String MessageTypeIndicator = "";
		
		StringBuffer sb = new StringBuffer();
		
		int idxSPT = fileName.indexOf("SPT");
		int idxSP2D = fileName.indexOf("SP2D");
		
		if(idxSPT>0) MessageTypeIndicator = "SPT";
		if(idxSP2D>0) MessageTypeIndicator = "SP2D";
		
		
		Status = "SUCCESS";         	
		if ("A003".equals(ErrorCode)){
			ACKDataDocument insertDataACKSpan = new ACKDataDocument();
			insertDataACKSpan.setDocumentDate("XXXXXXXXXXXXXXX");
			insertDataACKSpan.setDocumentType(MessageTypeIndicator);
			insertDataACKSpan.setDocumentNo("XXXXXXXXXXXXXXX");
			insertDataACKSpan.setFileName(fileName);
			insertDataACKSpan.setReturnCode(ErrorCode);
			insertDataACKSpan.setDescription(ErrorMessage);
			
			ACKDataString = Atomic.mappingDataACKSpan(insertDataACKSpan);
			sb.append(ACKDataString+"\n");
		
		}else{
			try{
				if (spanDocument != null){
		
					System.out.println("ACK-1");
					ApplicationArea spanDocumentAppArea = spanDocument.getApplicationArea(); 
					MessageTypeIndicator = spanDocumentAppArea.getApplicationAreaMessageTypeIndicator(); 
					
					List<DataArea> spanDocumentDataArea = spanDocument.getDataArea();
					if (spanDocumentDataArea != null){	
						System.out.println("ACK-2");		
						for (DataArea dataArea : spanDocumentDataArea){
							String DocumentDate = dataArea.getDocumentDate(); 
							String DocumentNumber = dataArea.getDocumentNumber(); 
							
							ACKDataDocument insertDataACKSpan = new ACKDataDocument();
							insertDataACKSpan.setDocumentDate(DocumentDate);
							insertDataACKSpan.setDocumentType(MessageTypeIndicator);
							insertDataACKSpan.setDocumentNo(DocumentNumber);
							insertDataACKSpan.setFileName(fileName);
							insertDataACKSpan.setReturnCode(ErrorCode);
							insertDataACKSpan.setDescription(ErrorMessage);
							
							ACKDataString = Atomic.mappingDataACKSpan(insertDataACKSpan);
							
							if(spanDocumentDataArea.size()>1){
								sb.append(ACKDataString+"\n");
							}
							else{
								sb.append(ACKDataString);
							}
						}
						ErrorMessage = "";
					} 
				}else {
					System.out.println("ACK-4");
					ACKDataDocument insertDataACKSpan = new ACKDataDocument();
					insertDataACKSpan.setDocumentDate("XXXXXXXXXXXXXXX");
					insertDataACKSpan.setDocumentType(MessageTypeIndicator);
					insertDataACKSpan.setDocumentNo("XXXXXXXXXXXXXXX");
					insertDataACKSpan.setFileName(fileName);
					insertDataACKSpan.setReturnCode(ErrorCode);
					insertDataACKSpan.setDescription(ErrorMessage);
					
					ACKDataString = Atomic.mappingDataACKSpan(insertDataACKSpan);
					
					sb.append(ACKDataString+"\n");
				}
			}catch(Exception ex){
				Status = "ERROR";
				//ErrorMessage = ex.getMessage();
				ErrorMessage = "Error Exception";
			}
		}
		
		ACKDataString = sb.toString();
		DocumentType = MessageTypeIndicator;
		
		//init strings
		strings[0] = ACKDataString;
		strings[1] = Status;
		strings[2] = ErrorMessage;
		strings[3] = DocumentType;
		
		return strings;
	}
	
}
