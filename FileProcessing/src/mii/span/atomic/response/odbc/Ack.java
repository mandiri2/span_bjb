package mii.span.atomic.response.odbc;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedProperty;

import com.mii.dao.SystemParameterDAO;
import com.mii.models.SystemParameter;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.ApplicationInstance;
import com.zehon.ftp.FTP;
import com.zehon.ftp.FTPClient;

import mii.span.Log;
import mii.span.constant.MdrCfgMainConstant;
import mii.span.db.adapter.Odbc;
import mii.span.doc.MdrCfgMain;
import mii.span.process.model.SentAckDataFileFtpRequest;
import mii.span.process.model.SentAckDataFileFtpResponse;
import mii.span.process.model.SentAckDataFileRequest;
import mii.span.process.model.SentAckDataFileResponse;
import mii.span.sftp.Services;
import mii.span.util.Atomic;
import mii.span.util.BusinessUtil;
import mii.span.util.PubUtil;

public class Ack {
	
	@ManagedProperty(value = "#{applicationBean}")
	private static ApplicationBean applicationBean;
	
	private static SystemParameterDAO getSystemParameterDaoBean(){
		applicationBean = ApplicationInstance.getInstance().getApplicationBean();
		return (SystemParameterDAO) applicationBean.getFileProcessContext().getBean("sysParamDAO");
	}
	
	/**
	 * mandiri.span.atomic.response.odbc.ack:sentACKDataFile <br />
	 * return String[] <br />
	 * String[0] Status <br />
	 * String[1] ErrorMessage <br />
	 * String[2] ACKFilename <br />
	 * String[3] statussend <br />
	 */
	
	public static SentAckDataFileResponse sentACKDataFile(SentAckDataFileRequest request){
		SentAckDataFileResponse response = new SentAckDataFileResponse();
		String Status = null;
		String ErrorMessage = request.getErrorMessage();
		String ACKFilename = null;
		String statussend = null;
		String DocumentType = request.getDocumentType();
		try {
			if(DocumentType.equals("")||DocumentType==null||DocumentType.length()<=0){
				DocumentType = "SP2D";
			}
		} catch (Exception e) {
			DocumentType = "SP2D";
		}
		String filename = request.getFilename();
		String ACKDataString = request.getAckDataString();
		try {
			if(ACKDataString.equals("")||ACKDataString==null||ACKDataString.length()<=0){
				ACKDataString = PubUtil.padRight("XXXXXXXXXXXXXXX", 10)+"|"+DocumentType+"|"+PubUtil.padRight("XXXXXXXXXXXXXXX", 21)+"|"+PubUtil.padRight(filename, 100)+'|'+PubUtil.padRight(ErrorMessage,200);
			}
		} catch (Exception e) {
			ACKDataString = PubUtil.padRight("XXXXXXXXXXXXXXX", 10)+"|"+DocumentType+"|"+PubUtil.padRight("XXXXXXXXXXXXXXX", 21)+"|"+PubUtil.padRight(filename, 100)+'|'+PubUtil.padRight(ErrorMessage,200);
		}	
		//Sequence	preparation for send ack to span
		try {
			
			//rename file, send file to backup directory - enhancment for throw exception
				/*String LOCAL_DIR = Odbc.selectMdrCfgMain(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_PUT_LOCAL_DIR).getValue1();
				String spanHost = Odbc.selectMdrCfgMain(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_IPADDRESS).getValue1();
				String spanPort = Odbc.selectMdrCfgMain(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_PORT).getValue1();
				String spanUser = Odbc.selectMdrCfgMain(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_USERNAME).getValue1();
				String spanPass = Odbc.selectMdrCfgMain(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_PASSWORD).getValue1();
				String spanACKDir = Odbc.selectMdrCfgMain(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_PUT_REMOTE_DIR).getValue1();
				String BACKUP_DIR = Odbc.selectMdrCfgMain(MdrCfgMainConstant.SPAN_DEPKEU_BJB_BACKUP_FTP_PUT_DIR).getValue1();
				String mdrHost = Odbc.selectMdrCfgMain(MdrCfgMainConstant.SPAN_DEPKEU_BJB_BACKUP_FTP_IPADDRESS).getValue1();
				String mdrPort = Odbc.selectMdrCfgMain(MdrCfgMainConstant.SPAN_DEPKEU_BJB_BACKUP_FTP_PORT).getValue1();
				String mdrUser = Odbc.selectMdrCfgMain(MdrCfgMainConstant.SPAN_DEPKEU_BJB_BACKUP_FTP_USERNAME).getValue1();
				String mdrPass = Odbc.selectMdrCfgMain(MdrCfgMainConstant.SPAN_DEPKEU_BJB_BACKUP_FTP_PASSWORD).getValue1();
				String spanOutboundDir = Odbc.selectMdrCfgMain(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_GET_REMOTE_DIR).getValue1();*/
			
			//TODO Error Handling
			SystemParameter systemParameterspanHost =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_IPADDRESS).get(0);
			String spanHost = systemParameterspanHost.getParam_value();
			SystemParameter systemParameterspanPort =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_PORT).get(0);
			String spanPort = systemParameterspanPort.getParam_value();
			SystemParameter systemParameterLOCAL_DIR =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_PUT_LOCAL_DIR).get(0);
			String LOCAL_DIR = systemParameterLOCAL_DIR.getParam_value();
			SystemParameter systemParameterspanUser =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_USERNAME).get(0);
			String spanUser = systemParameterspanUser.getParam_value();
			SystemParameter systemParameterspanPass =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_PASSWORD).get(0);
			String spanPass = systemParameterspanPass.getParam_value();
			SystemParameter systemParameterspanACKDir =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_PUT_REMOTE_DIR).get(0);
			String spanACKDir = systemParameterspanACKDir.getParam_value();
			SystemParameter systemParameterBACKUP_DIR =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_BJB_BACKUP_FTP_PUT_DIR).get(0);
			String BACKUP_DIR = systemParameterBACKUP_DIR.getParam_value();
			SystemParameter systemParametermdrHost =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_BJB_BACKUP_FTP_PUT_DIR).get(0);
			String mdrHost = systemParametermdrHost.getParam_value();
			SystemParameter systemParametermdrPort =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_BJB_BACKUP_FTP_PORT).get(0);
			String mdrPort = systemParametermdrPort.getParam_value();
			SystemParameter systemParametermdrUser =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_BJB_BACKUP_FTP_USERNAME).get(0);
			String mdrUser = systemParametermdrUser.getParam_value();
			SystemParameter systemParametermdrPass =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_BJB_BACKUP_FTP_PASSWORD).get(0);
			String mdrPass = systemParametermdrPass.getParam_value();
			SystemParameter systemParameterspanOutboundDir =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_GET_REMOTE_DIR).get(0);
			String spanOutboundDir = systemParameterspanOutboundDir.getParam_value();
			
			List<String> spanFolderNames = new ArrayList<String>();
			int indexValue = 0;
			//sequence for outbound folder
				spanFolderNames = BusinessUtil.getFolderName(spanOutboundDir);
				
				//loop spanFolderNames
					for(String spanFolderNames2 : spanFolderNames){
						indexValue = spanFolderNames2.indexOf(DocumentType);
						
						//branch	
							//%indexValue%>0
							if(indexValue>0){
								spanOutboundDir = spanFolderNames2;
							}
					}
			
			//sequence for inbound folder
					spanFolderNames = BusinessUtil.getFolderName(spanACKDir);
					
				//loop spanFolderNames
					for(String spanFolderNames2 : spanFolderNames){
						indexValue = spanFolderNames2.indexOf(DocumentType);
						
						//branch
							//%indexValue%>0
							if(indexValue>0){
								spanACKDir = PubUtil.concat(spanFolderNames2, "_ACK");
							}
					}
			
					
			//(FA)
				String newFileNameFA = BusinessUtil.renameFileName(filename, PubUtil.concat(DocumentType,"_FA"));
			
			//jar --> txt
				newFileNameFA = newFileNameFA.replace("jar", "txt");
				
			//xml --> txt
				newFileNameFA = newFileNameFA.replace("xml", "txt");
				
			//mandiri.span.util.atomic:ACKStringToBytes
				Object[] ACKStringToBytesResult = Atomic.ACKStringToBytes(ACKDataString);
				byte[] ACKbytes = BusinessUtil.bytesIfNotNull(ACKStringToBytesResult[0]);
				Status = BusinessUtil.stringIfNotNull(ACKStringToBytesResult[1]);
				ErrorMessage = BusinessUtil.stringIfNotNull(ACKStringToBytesResult[2]);
				
			//create file FA in /tmp
				PubUtil.bytesToFile(PubUtil.concat(LOCAL_DIR,newFileNameFA), ACKbytes, null);
				
			System.out.println("success create file");
			
			//sequence SFTP - ACK 
				//sequence sent file ack to inbound
					//mandiri.span.util.atomic:createAndExtractJAR
						String[] createAndExtractJARResult = Atomic.createAndExtractJAR(newFileNameFA, LOCAL_DIR, "C");
						Status = BusinessUtil.stringIfNotNull(createAndExtractJARResult[0]);
						ErrorMessage = BusinessUtil.stringIfNotNull(createAndExtractJARResult[1]);
						
					//create jar file name
						String jarFileName = newFileNameFA.replace(".txt", ".jar");
					
					//send functional ack
					//mandiri.span.sftp.services:uploadFile
					statussend = Services.uploadFile(spanACKDir, LOCAL_DIR, jarFileName, spanHost, spanUser, spanPass);
					Status = statussend;
					
					//branch on Status\
						String StatusJAR = null;
						if("SUCCESS".equalsIgnoreCase(Status)){
							StatusJAR = "SUCCESS";
						}
						else{
							//do nothing
						}
						
				//debug log
			
			//delete F-ACK
				String status = PubUtil.deleteFile(PubUtil.concat(LOCAL_DIR,newFileNameFA));
			
			//branch on /ErrorMessage
				
				//debug log
				
				if(ErrorMessage==null){
					Log.debugLog("SPAN", "sendACKDataFile", "INFO", 
							PubUtil.concat(Status," to send ACK and delete file in SPAN folder : ",status)
							);
				}
				else{
					//default
					Log.debugLog("SPAN", "sendACKDataFile", "INFO", 
							PubUtil.concat(Status," to send ACK and delete file in SPAN folder : ",status,"-",ErrorMessage)
							);
				}
			
			ACKFilename = jarFileName;
			ErrorMessage = "";
			Status = "SUCCESS";
					
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
		response.setStatus(Status);
		response.setErrorMessage(ErrorMessage);
		response.setAckFileName(ACKFilename);
		response.setStatusSend(statussend);
		
		return response;
	}
	
	/**
	 * mandiri.span.atomic.response.odbc.ack:sentACKDataFileFTP <br />
	 * return String[] <br />
	 * String[0] Status <br />
	 * String[1] ErrorMessage <br />
	 * String[2] ACKFilename <br />
	 * @throws IOException 
	 */
	public static SentAckDataFileFtpResponse sentACKDataFileFTP(SentAckDataFileFtpRequest request) throws IOException{
		SentAckDataFileFtpResponse response = new SentAckDataFileFtpResponse();
		String Status = null;
		String ACKFilename = null;
		String filename = request.getFilename();
		String DocumentType = request.getDocumentType();
		String ErrorMessage = request.getErrorMessage();
		String ErrorCode = request.getErrorCode();
		String ACKDataString = request.getAckDataString();
		
		//SEQUENCE preparation for send ack to span
			//rename file, send file to backup directory - enhancment for throw exception
				SystemParameter systemParameterspanlocalPath =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_PUT_LOCAL_DIR).get(0);
				String LOCAL_DIR = systemParameterspanlocalPath.getParam_value();
				SystemParameter systemParameterLOCAL_DIR =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_PUT_LOCAL_DIR).get(0);
				String BACKUP_DIR = systemParameterLOCAL_DIR.getParam_value();
				SystemParameter systemParametermdrUser =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_BJB_BACKUP_FTP_USERNAME).get(0);
				String mdrUser = systemParametermdrUser.getParam_value();
				SystemParameter systemParametermdrPass =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_BJB_BACKUP_FTP_PASSWORD).get(0);
				String mdrPass = systemParametermdrPass.getParam_value();
				SystemParameter systemParametermdrHost =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_BJB_BACKUP_FTP_IPADDRESS).get(0);
				String mdrHost = systemParametermdrHost.getParam_value();
				SystemParameter systemParametermdrPort =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_BJB_BACKUP_FTP_PORT).get(0);
				String mdrPort = systemParametermdrPort.getParam_value();
				
				//Comments	for outbound folder	disabled
				//Comments	for inbound folder disabled
				
				//Service	mandiri.span.util:renameFileName
					String newFileNameFA = BusinessUtil.renameFileName(filename, PubUtil.concat(DocumentType,"_FA"));
					
				newFileNameFA = newFileNameFA.replace("jar", "txt");
				newFileNameFA = newFileNameFA.replace("xml", "txt");
				
				//string to bytes disabled
				
				//mandiri.span.util.atomic:ACKStringToBytes
					Object[] ACKStringToBytesResult = Atomic.ACKStringToBytes(ACKDataString);
					byte[] ACKbytes = BusinessUtil.bytesIfNotNull(ACKStringToBytesResult[0]);
					Status = BusinessUtil.stringIfNotNull(ACKStringToBytesResult[1]);
					ErrorMessage = BusinessUtil.stringIfNotNull(ACKStringToBytesResult[2]);
					
				//create file FA in /tmp (skip)
				
				//mandiri.span.util.atomic:createAndExtractJAR
					String[] createAndExtractJARResult = Atomic.createAndExtractJAR(newFileNameFA, LOCAL_DIR, "C");
					Status = createAndExtractJARResult[0];
					ErrorMessage = createAndExtractJARResult[1];
					
				//Sequence	create jar file name
					String jarFileName = newFileNameFA.replace(".txt", ".jar");
					
				//Sequence	FTP - for mandiri backup files
					try {
						PubUtil.sendFileFTP(ACKbytes, mdrHost, mdrPort, mdrUser, mdrPass, PubUtil.concat(BACKUP_DIR,"/"), jarFileName);
					} catch (Exception e) {
						e.printStackTrace();
					}
				
				//Comments	SFTP - ACK  disabled sequence
					
				//delete F-ACK 
					String status = PubUtil.deleteFile(PubUtil.concat(LOCAL_DIR,"/",newFileNameFA));
				
				//branch on /ErrorMessage
					if(ErrorMessage == null){
						Log.debugLog("SPAN", "sendACKDataFile", "INFO", 
								PubUtil.concat(Status," to send ACK and delete file in SPAN folder : ", status));
					}
					else{
						//default
						Log.debugLog("SPAN", "sendACKDataFile", "INFO", 
								PubUtil.concat(Status," to send ACK and delete file in SPAN folder : ",status," - ",ErrorMessage));
					}
					
				ErrorMessage = "";
				Status = "SUCCESS";
				ACKFilename = jarFileName;
			
			response.setStatus(Status);
			response.setErrorMessage(ErrorMessage);
			response.setAckFileName(ACKFilename);
			
			return response;
	}
}
