package mii.span.atomic;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.hibernate.Query;

import com.mii.dao.SpanDataValidationDao;
import com.mii.helpers.InterfaceDAO;

import mii.span.Log;
import mii.span.db.Service;
import mii.span.db.adapter.Odbc;
import mii.span.doc.DataArea;
import mii.span.doc.SP2DDocument;
import mii.span.model.SpanVoidDataTrx;
import mii.span.process.model.ExecuteVoidDocumentNoRequest;
import mii.span.process.model.ExecuteVoidDocumentNoResponse;
import mii.span.process.model.SelectSpanDataValidationResponse;
import mii.span.process.model.UpdateSpanResponse;
import mii.span.util.BusinessUtil;
import mii.span.util.PubUtil;

public class Request {
	
	/**
	 * mandiri.span.atomic.request:executeVoidDocumentNO
	 * @throws JAXBException 
	 */
	public static ExecuteVoidDocumentNoResponse executeVoidDocumentNO(ExecuteVoidDocumentNoRequest request) throws JAXBException{
		InterfaceDAO dao = (SpanDataValidationDao) BusinessUtil.getDao("spanDataValidationDao");
		
		ExecuteVoidDocumentNoResponse response = new ExecuteVoidDocumentNoResponse();
		String filename = request.getFileName();
		String xmlFilename = request.getXmlFileName();
		
		String ErrorMessage = null;
		String xmlvalues = null;
		String processingStatus = null;
		//o mandiri.span.db.service:selectSPANVoidDataTransaction
		List<SpanVoidDataTrx> VOIDList = Service.selectSpanVoidDataTransaction(filename);
		//c mandiri.span.db.service:selectSPANVoidDataTransaction
		//o mandiri.span.db.service:selectSPANDataValidation
		SelectSpanDataValidationResponse selectSpanDataValidationResponse = Service.selectSpanDataValidation(filename);
		String TOTAL_RECORD = selectSpanDataValidationResponse.getTotalRecord();
		String XML_VOID_FILE_NAME = selectSpanDataValidationResponse.getXmlVoidFileName();
		Date updateDate = selectSpanDataValidationResponse.getUpdateDate();
		String ACK_STATUS = selectSpanDataValidationResponse.getAckStatus();
		String ACK_FILE_NAME = selectSpanDataValidationResponse.getAckFileName();
		String TOTAL_AMOUNT = selectSpanDataValidationResponse.getTotalAmount();
		//c mandiri.span.db.service:selectSPANDataValidation
		//o map
		Integer voidListSize = VOIDList.size();
		String Status = "SUCCESS";
		String totalRecord = "0";
		String totalAmount = "0";
		//c map
		//o branch on.voidlistsize
		if(voidListSize!=null
				&& voidListSize>0){
			//o branch.on.xml_void_file_name
				Integer totalVOIDList = null;
				//o null.map
				if(XML_VOID_FILE_NAME==null){
					totalVOIDList = VOIDList.size();
				}	
				//c null.map
				//o map.default
				else{
					totalVOIDList = VOIDList.size();
					xmlFilename = XML_VOID_FILE_NAME;
				}
				//c map.default
			//c branch.on.xml_void_file_name
		}
		else{
			return null;
		}
		//c branch on.voidlistsize
		//o xmlValuesToDocument
		SP2DDocument document = (SP2DDocument) PubUtil.xmlValuesToDocument(SP2DDocument.class, xmlFilename);
		//c xmlValuesToDocument
		//o sequence
		try {
			//o map
			SP2DDocument newDocument = (SP2DDocument)BusinessUtil.cloningObject(SP2DDocument.class, document);
			//c map
			//o loop.over.voidList
			List<DataArea> newDataArea = new ArrayList<DataArea>();
			for(SpanVoidDataTrx VOIDList2 : VOIDList){
				//o loop.over.document.dataArea
				for(DataArea DataArea : document.getDataArea()){
					//o branch
					processingStatus = null;
						//o map
						if(DataArea.getDocumentNumber().equals(VOIDList2.getDocumentNo())){
							processingStatus = "X";
						}
						//c map
						//o map.default
						else{
							newDataArea.add(DataArea);
						}
						//c map.default
					//c branch
					
				
				}
				//c loop.over.document.dataArea
				//o map
				//do nothing
				//c map
				//o map
				document.setDataArea(newDataArea);
				//c map
			}
			//c loop.over.voidList
			//o map.loop.over.document.dataArea
			String dataAreaList=null;
			for(DataArea DataArea : document.getDataArea()){
				//o map
				dataAreaList = "1";
				totalRecord = PubUtil.addInts(totalRecord, "1");
				totalAmount = PubUtil.addInts(totalAmount, DataArea.getAmount());
				//c map
			}
			//c map.loop.over.document.dataArea
			//o loop.over.voidlList
			
			for(SpanVoidDataTrx VOIDList2 : VOIDList){
				//o map
				String sp2dno = VOIDList2.getDocumentNo().substring(0, 15);
				//c map
				//o mandiri.span.db.service:updateSPAN
				
//				String sql = "update SPAN_DATA_VALIDATION_SP2DNO set VOID_FLAG='2' where file_name='"+filename+"' and VOID_FLAG='0' and sp2dno='"+sp2dno+"'";
				String hql = "UPDATE SpanDataValidationSP2DNo SET VOID_FLAG='2' WHERE FILE_NAME= '"+filename+"' AND VOID_FLAG='0' AND SP2DNO= '"+sp2dno+"'";
				Query query = dao.getQuery(hql);
//				query.setString("filename", filename);
//				query.setString("sp2dno", sp2dno);
				
				UpdateSpanResponse updateSpanResponse = Service.updateSPAN(query);
				Status = updateSpanResponse.getStatus();
				ErrorMessage = updateSpanResponse.getErrorMessage();
				//c mandiri.span.db.service:updateSPAN
			}
			//c loop.over.voidlList
			//o branch.on.dataAreaList
				//o 1.sequence
				if("1".equalsIgnoreCase(dataAreaList)){
					//o map
					newDocument = (SP2DDocument) BusinessUtil.cloningObject(SP2DDocument.class, document);
					newDocument.getFooter().setTotalCount(totalRecord);
					newDocument.getFooter().setTotalAmount(totalAmount);
					newDocument.getFooter().setTotalBatchCount(totalRecord);
					//c map
					//o documentToXmlValues
					xmlvalues = PubUtil.documentToXmlValues(SP2DDocument.class, newDocument);
					//c documentToXmlValues
					//o map
					TOTAL_RECORD = totalRecord;
					TOTAL_AMOUNT = totalAmount;
					//c map
				}
				//c 1.sequence
				//o map.default
				else{
					xmlvalues = xmlFilename;
					processingStatus = "V";
					totalRecord = TOTAL_RECORD;
					totalAmount = TOTAL_AMOUNT;
				}
				//c map.default
			//c branch.on.dataAreaList
			//o mandiri.span.db.adapter.odbc:updateGajiDataValidation
			Odbc.updateGajiDataValidation(xmlvalues, xmlFilename, TOTAL_AMOUNT, TOTAL_RECORD, processingStatus, "XML File has changed", filename);
			//c mandiri.span.db.adapter.odbc:updateGajiDataValidation
			//o loop.over.VOIDList
			for(SpanVoidDataTrx VOIDList2 : VOIDList){
				//o mandiri.span.db.service:updateSPAN(with.document.no)
//				String sql = "update SPAN_VOID_DATA_TRX set STATUS='1' where file_name='"+filename+"' and STATUS='0' and document_no='"+VOIDList2.getDocumentNo()+"'";
				String hql = "UPDATE SpanVoidDataTrx SET status='1' WHERE fileName = '"+filename+"' AND status = '0' AND documentNo = '"+VOIDList2.getDocumentNo()+"' ";
				Query query = dao.getQuery(hql);
//				query.setString("filename", filename);
//				query.setString("documentNo", VOIDList2.getDocumentNo());
				UpdateSpanResponse updateSpanResponse = Service.updateSPAN(query);
				Status = updateSpanResponse.getStatus();
				ErrorMessage = updateSpanResponse.getErrorMessage();
				//c mandiri.span.db.service:updateSPAN(with.document.no)
				//o mandiri.span.db.service:updateSPAN(remove_docNo_from_span_host_data_details)
				
//				sql="delete from SPAN_HOST_DATA_DETAILS where DOC_NUMBER='"+VOIDList2.getDocumentNo()+"' and status = 'V'";
				hql="DELETE FROM SpanHostDataDetails WHERE DOC_NUMBER = '"+VOIDList2.getDocumentNo()+"' AND STATUS='V'";
				query = dao.getQuery(hql);
//				query.setString("documentNo", VOIDList2.getDocumentNo());
				
				updateSpanResponse = Service.updateSPAN(query);
				Status = updateSpanResponse.getStatus();
				ErrorMessage = updateSpanResponse.getErrorMessage();
				//c mandiri.span.db.service:updateSPAN(remove_docNo_from_span_host_data_details)
			}
			//c loop.over.VOIDList
		} 
		catch (Exception e) {
			e.printStackTrace();
			Log.debugLog("SPAN", "executeVoidDocumentNO", "INFO", e.toString());
		}
		//c sequence
		
		response.setStatus(Status);
		response.setErrorMessage(ErrorMessage);
		return response;
	}
}
