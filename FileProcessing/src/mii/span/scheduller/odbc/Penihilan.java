package mii.span.scheduller.odbc;

import java.math.BigDecimal;
import java.util.Date;

import mii.span.model.BulkFTReqPRC;
import mii.span.model.SpanPenihilanLog;
import mii.span.util.BusinessUtil;
import mii.span.util.PubUtil;
import mii.utils.CommonDate;

import com.mii.constant.Constants;
import com.mii.constant.DaoConstant;
import com.mii.dao.SpanDataValidationDao;
import com.mii.dao.SpanPenihilanLogDao;
import com.mii.dao.SystemParameterDAO;

public class Penihilan {
	
	public void run(){
		processPenihilan();
	}
	
	public static void processPenihilan(){
		System.out.println("Start Penihilan");
		try {
			SystemParameterDAO dao = (SystemParameterDAO) BusinessUtil.getDao(DaoConstant.SystemParameterDAO);
			String accountNo = dao.getValueByParamName(Constants.PARAM_DEBITACCNO+Constants.GAJI);
			String saldoString = ToBeExecute.getSaldo(accountNo);
//			String saldoString = "1000"; //bypass
			BigDecimal saldo = new BigDecimal(saldoString);
			String documentDate = CommonDate.getCurrentDateString("yyyy-MM-dd");
			if(saldo.compareTo(BigDecimal.ZERO)>0){
				if(isSp2dExist(documentDate)){
					if(!isStillProcessing(documentDate)){	
						if(!isStillProsesPenihilan()){ 
							process(documentDate, saldoString, accountNo);
							System.out.println("SUCCESS SEND TO PRC");
						}
					}
				}
			}
		} catch (Exception e) {
			System.out.println("FAILED SEND TO PRC");
			e.printStackTrace();
		}
		System.out.println("END PENIHILAN");
	}
	
	private static boolean isSp2dExist(String documentDate) {
		SpanDataValidationDao dao = (SpanDataValidationDao) BusinessUtil.getDao(DaoConstant.SpanDataValidationDao);
		return dao.isSp2dExist(documentDate);
	}

	private static void process(String documentDate, String amount, String debitAccount) {
		SpanPenihilanLogDao dao = (SpanPenihilanLogDao) BusinessUtil.getDao(DaoConstant.SpanPenihilanLogDao);
		SpanPenihilanLog spanPenihilanLog = constructSpanPenihilanLog(documentDate, amount);
		dao.save(spanPenihilanLog);
		BulkFTReqPRC bulkFTReqPRC = cosntructPrc(spanPenihilanLog, debitAccount);
		dao.save(bulkFTReqPRC);
	}

	private static BulkFTReqPRC cosntructPrc(SpanPenihilanLog spanPenihilanLog, String debitAccount) {
		BulkFTReqPRC prc = new BulkFTReqPRC();
		prc.setBatchid(spanPenihilanLog.getReferenceNo());
		prc.setDebitamount(spanPenihilanLog.getAmount());
		prc.setDebitaccno(debitAccount);
		prc.setCreditaccno(spanPenihilanLog.getCreditAcc());
		prc.setPrcstatus("0");
		prc.setCreditaccname(getCreditaccname());
		
		//other
		prc.setTrxheaderid(spanPenihilanLog.getReferenceNo());
		prc.setTrxdetailid(spanPenihilanLog.getReferenceNo());
		return prc;
	}

	private static String getCreditaccname(){
		SystemParameterDAO dao = (SystemParameterDAO) BusinessUtil.getDao(DaoConstant.SystemParameterDAO);
		return dao.getValueByParamName(Constants.SPAN_NAMA_REKENING_PENAMPUNG_RTGS);
	}

	private static SpanPenihilanLog constructSpanPenihilanLog(String documentDate, String amount) {
		Date date = new Date();
		SpanPenihilanLog spanPenihilanLog = new SpanPenihilanLog();
		spanPenihilanLog.setTanggalPenihilan(CommonDate.dateToString("yyyy-MM-dd", date));
		spanPenihilanLog.setCreateDate(date);
		spanPenihilanLog.setTanggalSp2d(documentDate);
		spanPenihilanLog.setReferenceNo(generateReferenceNo());
		spanPenihilanLog.setAmount(amount);
		spanPenihilanLog.setCreditAcc(getCreditAcct());
		spanPenihilanLog.setBsStatus("0");
		spanPenihilanLog.setProcStatus("1");
		
		String sequence = getSequencePenihilan(documentDate);
		spanPenihilanLog.setSequencePenihilan(sequence);
		spanPenihilanLog.setNarasi(constructNarasi(date, sequence));
		
		return spanPenihilanLog;
	}

	private static String constructNarasi(Date date, String sequence) {
		return PubUtil.concat("NHL ",CommonDate.dateToString("ddMMyyyy", date), " ke ", sequence);
	}

	private static String getSequencePenihilan(String documentDate) {
		SpanPenihilanLogDao dao = (SpanPenihilanLogDao) BusinessUtil.getDao(DaoConstant.SpanPenihilanLogDao);
		return String.valueOf(dao.getSequencePenihilan(documentDate) + 1);
	}

	private static String getCreditAcct() {
		SystemParameterDAO dao = (SystemParameterDAO) BusinessUtil.getDao(DaoConstant.SystemParameterDAO);
		return dao.getValueByParamName(Constants.SPAN_REKENING_PENAMPUNG_RTGS);
	}

	private static String generateReferenceNo() {
		return  PubUtil.concat("NHL-", CommonDate.dateToString("yyyyMMddHHmmss", new Date()));
	}

	/**
	 * check apakah masih ada proses penihilan
	 * @return
	 */
	private static boolean isStillProsesPenihilan() {
		SpanPenihilanLogDao dao = (SpanPenihilanLogDao) BusinessUtil.getDao(DaoConstant.SpanPenihilanLogDao);
		return dao.isStillProsesPenihilan();
	}
	
	/**
	 * Check di span data validation apakah masih ada yang procstatusnya X,1,2,3,4
	 * @return
	 */
	private static boolean isStillProcessing(String documentDate) {
		SpanDataValidationDao dao = (SpanDataValidationDao) BusinessUtil.getDao(DaoConstant.SpanDataValidationDao);
		return dao.isStillProcessing(documentDate);
	}
}
