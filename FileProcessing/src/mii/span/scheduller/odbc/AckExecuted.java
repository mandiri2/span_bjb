package mii.span.scheduller.odbc;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;

import com.mii.constant.DaoConstant;
import com.mii.dao.SpanDataValidationDao;
import com.mii.helpers.InterfaceDAO;

import mii.span.atomic.response.odbc.Ack;
import mii.span.db.Adapter;
import mii.span.db.Service;
import mii.span.doc.ACKDataDocument;
import mii.span.model.MappingDataACKSpanRequest;
import mii.span.model.Provider;
import mii.span.model.SpanDataValidation;
import mii.span.model.SpanHostResponseCode;
import mii.span.process.model.ACKExecutedDoc;
import mii.span.process.model.CheckRespCodeRequest;
import mii.span.process.model.CheckRespCodeResponse;
import mii.span.process.model.MappingACKExecutedResponse;
import mii.span.process.model.MappingAckExecutedRequest;
import mii.span.process.model.SentAckDataFileRequest;
import mii.span.process.model.SentAckDataFileResponse;
import mii.span.process.model.UpdateSpanResponse;
import mii.span.util.BusinessUtil;
import mii.span.util.PubUtil;

public class AckExecuted {
	
	public void run(){
		System.out.println("Start Run ACK Executed");
		ackExecuted();
		System.out.println("End Run ACK Executed");
	}

	public static void ackExecuted(){

		String ErrorMessage = "";
		String Status = "";
		//Get Data Span Data Validatioin with status 6
		String procStatus = "6";
		List<SpanDataValidation> Files = Service.selectSPANDataValidationStatus(procStatus);
		for (SpanDataValidation file : Files) {
			String sqlUpdate = "";
			try {
				String respCode = file.getResponseCode();
				if("".equalsIgnoreCase(respCode)||respCode == null){
					ErrorMessage = "Still have an error transaction in this file";
					Status = "ERROR";
				}else{
					ErrorMessage = "All records are success to be executed";
					Status = "SUCCESS";
				}
				List<ACKExecutedDoc> ackDocList = new ArrayList<ACKExecutedDoc>();
				
				if ("SUCCESS".equalsIgnoreCase(Status)||"1".equalsIgnoreCase(file.getForcedAck())) {
					//Sleep 1000ms
					Thread.sleep(1000); //Sleep 1second
					
					String accountNo = file.getDebitAccount();
					Provider providerInfo = Service.getProviderInfoDoc(accountNo);
//					if ("SPN02".equalsIgnoreCase(providerInfo.getProviderCode())) {
						ackDocList = Service.selectSPANHostResponse(file.getFileName());	
//					}
					
					String oriFN = file.getFileName();
					int rf = file.getFileName().indexOf("RF");
					String convertedFN = "";
					if (rf >= 0) {
						convertedFN = Adapter.selectOriginalFileName(oriFN);
						if (!"".equalsIgnoreCase(convertedFN) && convertedFN != null) {
							file.setFileName(convertedFN);
						} else {
							// do nothing
						}
					}
					
					// call mapping ackExecuted
					//Accepted.mappingACKAccepted(", ErrorMessage, spanDocument, fileName)
					MappingAckExecutedRequest mappingRequest = new MappingAckExecutedRequest();
					mappingRequest.setAckExecutedDoc(ackDocList);
					mappingRequest.setFilename(file.getFileName());
					mappingRequest.setForcedACK("0");
					
					MappingACKExecutedResponse response = mappingACKExecuted(mappingRequest);
					
					String responseStatus = response.getStatus();
					String checkpoint = "SENDING";
					String statussend = "";
					if("SUCCESS".equalsIgnoreCase(responseStatus)){
						
						SentAckDataFileRequest sendAckRequest = new SentAckDataFileRequest();
						sendAckRequest.setAckDataString(response.getACKDataString());
						sendAckRequest.setDocumentType(response.getDocumentType());
						sendAckRequest.setErrorMessage(response.getErrorMessage());
						sendAckRequest.setFilename(file.getFileName());
						/*
						 * CALL SERVICE SENDACKDATAFILE -> same service di spanDataValidation
						 */
						SentAckDataFileResponse sendAckResponse = new SentAckDataFileResponse();
						sendAckResponse = Ack.sentACKDataFile(sendAckRequest);
						
						statussend = sendAckResponse.getStatusSend();
						//Bug Fixing Unit Test
						if("SUCCESS".equalsIgnoreCase(statussend)){
							file.setProcStatuc("5");
							file.setAckStatus("1");
							file.setProcDescription("ACK Executed data file has been SUCCESS send to SPAN");
							file.setAckDescription("");
							file.setAckFileName(sendAckResponse.getAckFileName());
							checkpoint = "";
						}else{
							file.setProcStatuc("4");
							file.setAckFileName(sendAckResponse.getAckFileName());
							file.setProcDescription("Failed to send ACK : "+sendAckResponse.getErrorMessage());
							file.setAckStatus("0");
						}
					}
					
					sqlUpdate = "update SpanDataValidation set "
							+ "PROC_STATUS = '"+file.getProcStatuc()+"', "
							+ "PROC_DESCRIPTION = '"+file.getProcDescription()+"', "
							+ "ACK_FILE_NAME = '"+file.getAckFileName()+"', "
							+ "ACK_STATUS = '"+file.getAckStatus()+"', "
							+ "ACK_DESCRIPTION = '"+file.getProcDescription()+" "+checkpoint+" "+statussend+"', "
							+ "UPDATE_DATE = current_timestamp "
							+ "where FILE_NAME = '"+oriFN+"'";
				} else {
					sqlUpdate = "update SpanDataValidation "
							+ "set PROC_STATUS = '4', "
							+ "PROC_DESCRIPTION = '"+ErrorMessage+"',"
							+ " UPDATE_DATE = current_timestamp "
							+ "where FILE_NAME = '"+file.getFileName()+"'";
				}
				
				
			} catch (Exception e) {
				ErrorMessage = e.getMessage();
				sqlUpdate = "update SpanDataValidation"
						+ "set PROC_STATUS = '4', "
						+ "PROC_DESCRIPTION = '"+ErrorMessage+"',"
						+ " UPDATE_DATE = current_timestamp "
						+ "where FILE_NAME = '"+file.getFileName()+"'";
			}
			// updateSPAN
			InterfaceDAO dao = (SpanDataValidationDao) BusinessUtil.getDao(DaoConstant.SpanDataValidationDao);
			Query query = dao.getQuery(sqlUpdate);
			
			UpdateSpanResponse updateSpanResponse = Service.updateSPAN(query);
			ErrorMessage = updateSpanResponse.getErrorMessage();
			Status = updateSpanResponse.getStatus();
		}
 	}
	
	private static CheckRespCodeResponse checkRespCode(CheckRespCodeRequest checkRequest){
		CheckRespCodeResponse response = new CheckRespCodeResponse();
		
		try {
			// call DB
		List<SpanHostResponseCode> spanHostRC = new ArrayList<SpanHostResponseCode>();
		String RC_ID = "SPAN02";
		spanHostRC = Service.getReturCode("SPAN02");
		String responseCode = checkRequest.getResponseCode();
		String errorCode = "";
		String errorMessage = "";
		String returMark = "";
			for (SpanHostResponseCode spanResponse : spanHostRC) {
				String returCode = spanResponse.getRESPONSE_CODE();

				if (returCode.equalsIgnoreCase(responseCode)) {
					errorCode = returCode;
					errorMessage = spanResponse.getERROR_MESSAGE();
					returMark = "1";
					if ("999".equalsIgnoreCase(returCode)) {
						errorMessage = checkRequest.getResponseMessage();

					}
					response.setErrorCode(errorCode);
					response.setErrorMessage(errorMessage);
					response.setReturMark(returMark);
					break;
				} else if ("77".equalsIgnoreCase(responseCode)){
					errorCode = "77";
					errorMessage = "Reject";
					returMark = "0";
					response.setErrorCode(errorCode);
					response.setErrorMessage(errorMessage);
					response.setReturMark(returMark);
					break;
				} else {
					errorCode = "0";
					errorMessage = "SUKSES";
					returMark = "0";
					response.setErrorCode(errorCode);
					response.setErrorMessage(errorMessage);
					response.setReturMark(returMark);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;

	}
	
	
	public static final MappingACKExecutedResponse mappingACKExecuted(MappingAckExecutedRequest mappingRequest) {
//		IDataCursor pipelineCursor = pipeline.getCursor();
		String	filename = mappingRequest.getFilename();
		String	forcedACK = mappingRequest.getForcedACK();
		String Status = "SUCCESS";         	
		String ErrorMessage = "";      
		String DocumentType = "";   
		String ACKDataString = "";	
		StringBuffer sb = new StringBuffer();
		String returCode = "";
//		Map<String, IData> returCodeMap = new HashMap<String, IData>();
//		IData output = null;	
//		IDataCursor outputCursor = null;
//		
		try{
			// ACKExecutedDoc
			List<ACKExecutedDoc> ackExecutedDocList = mappingRequest.getAckExecutedDoc();
			
//			IData[] returCodes = IDataUtil.getIDataArray (pipelineCursor, "returCodes");
			if ( ackExecutedDocList != null)
			{
				
				for ( ACKExecutedDoc ackExecutedDoc : ackExecutedDocList)
				{
					ACKDataDocument ackDoc = new ACKDataDocument();
//					IDataCursor ACKExecutedDocCursor = ACKExecutedDoc[i].getCursor();
						DocumentType = ackExecutedDoc.getDOC_TYPE();
//						IData	insertDataACKSpan = IDataFactory.create();
//						IDataCursor insertDataACKSpanCursor = insertDataACKSpan.getCursor();
						String ErrorCode = ackExecutedDoc.getERROR_CODE();
						String Error_Message = ackExecutedDoc.getERROR_MESSAGE();
//						IDataUtil.put( insertDataACKSpanCursor,  "DocumentDate", IDataUtil.getString( ACKExecutedDocCursor, "DOC_DATE" ));                         
//						IDataUtil.put( insertDataACKSpanCursor,  "DocumentType", DocumentType );
//						IDataUtil.put( insertDataACKSpanCursor,  "DocumentNo", IDataUtil.getString( ACKExecutedDocCursor, "DOC_NUMBER" ));
//						IDataUtil.put( insertDataACKSpanCursor,  "FileName", filename.replace("jar","xml"));
						ackDoc.setDocumentDate(ackExecutedDoc.getDOC_DATE());
						ackDoc.setDocumentType(DocumentType);
						ackDoc.setDocumentNo(ackExecutedDoc.getDOC_NUMBER());
						ackDoc.setFileName(filename.replace("jar", "xml"));
						if("1".equals(forcedACK)) {
							ErrorCode = "00";
							Error_Message = PubUtil.padRight("SUKSES",100);
						} else {
							if ("0".equals(ErrorCode)){
								ErrorCode = "0";
								Error_Message = PubUtil.padRight("SUKSES",100);
							} else if ("00".equals(ErrorCode)){
								ErrorCode = "00";
								Error_Message = PubUtil.padRight("SUKSES",100);
							} else {
								/*
								if(returCodeMap.containsKey(ErrorCode)){
									output = returCodeMap.get(ErrorCode);
								}else{
					   				//get Data for checking error code					
					   				IData responseCode = IDataFactory.create();
					   				IDataCursor responseCodeCursor = responseCode.getCursor();
					   				IDataUtil.put(responseCodeCursor, "responseCode", ErrorCode);
					   				IDataUtil.put(responseCodeCursor, "responseMessage", Error_Message);
					   				responseCodeCursor.destroy();	
		          	        	        				
					   				output = wmDoInvoke("mandiri.span.util", "checkRespCode", responseCode);
					   				returCodeMap.put(ErrorCode,output);
								}
								*/
								// Check response code directly to DB
								//get Data for checking error code					
//				   				IData responseCode = IDataFactory.create();
//				   				IDataCursor responseCodeCursor = responseCode.getCursor();
//				   				IDataUtil.put(responseCodeCursor, "responseCode", ErrorCode);
//				   				IDataUtil.put(responseCodeCursor, "responseMessage", Error_Message);
//				   				responseCodeCursor.destroy();	
								CheckRespCodeRequest request = new CheckRespCodeRequest();	
								request.setResponseCode(ErrorCode);
								request.setResponseMessage(Error_Message);
								CheckRespCodeResponse response = checkRespCode(request);
								
								ErrorCode = response.getErrorCode();
								Error_Message = response.getErrorMessage();
								
//				   				output = wmDoInvoke("mandiri.span.util", "checkRespCode", responseCode);
//				   				
//								outputCursor = output.getCursor();
//								if(outputCursor.first("errorCode"))
//									ErrorCode = IDataUtil.getString(outputCursor, "errorCode");
//								if(outputCursor.first("errorMessage"))
//									Error_Message = IDataUtil.getString(outputCursor, "errorMessage");
//								outputCursor.destroy();
							}
						}
						
//						IDataUtil.put( insertDataACKSpanCursor,  "ReturnCode", ErrorCode );
//						IDataUtil.put( insertDataACKSpanCursor,  "Description", Error_Message);
//							
//						insertDataACKSpanCursor.destroy();
						ackDoc.setReturnCode(ErrorCode);
						ackDoc.setDescription(Error_Message);
						
						MappingDataACKSpanRequest req = new MappingDataACKSpanRequest();
						req.setAckDataDocument(ackDoc);
						String ackDataString = mappingDataACKSpan(req);
						
//						
//						if(outputDataACKSpanDocCursor.first("Status"))
//							Status = IDataUtil.getString(outputDataACKSpanDocCursor, "Status");
//		
//						if(outputDataACKSpanDocCursor.first("ErrorMessage"))
//							ErrorMessage = IDataUtil.getString(outputDataACKSpanDocCursor, "ErrorMessage");
//		
//						if(outputDataACKSpanDocCursor.first("ACKDataString"))
//							ACKDataString = IDataUtil.getString(outputDataACKSpanDocCursor, "ACKDataString");
//		
//						outputDataACKSpanDocCursor.destroy();
						sb.append(ackDataString+"\n");
						
//					ACKExecutedDocCursor.destroy();
				}
			}
		}catch(Exception ex){
				Status = "ERROR";
				ErrorMessage = ex.getMessage();
		}
		// pipeline
//		IDataUtil.put( pipelineCursor, "ACKDataString", sb.toString() );
//		IDataUtil.put( pipelineCursor, "Status", Status );
//		IDataUtil.put( pipelineCursor, "ErrorMessage", ErrorMessage );
//		IDataUtil.put( pipelineCursor, "DocumentType", DocumentType );
//		pipelineCursor.destroy();
		MappingACKExecutedResponse response = new MappingACKExecutedResponse();
		response.setACKDataString(sb.toString());
		response.setStatus(Status);
		response.setErrorMessage(ErrorMessage);
		response.setDocumentType(DocumentType);
		return response;
		
}
	
	public static final String mappingDataACKSpan(MappingDataACKSpanRequest request) throws Exception {
//		IDataCursor pipelineCursor = pipeline.getCursor();
//		IData pipelineCursorData = IDataUtil.getIData(pipelineCursor,"ACKDataDocument");
//		IDataCursor pipelineCursorDoc = pipelineCursorData.getCursor();	
//		
//	
		ACKDataDocument dataDoc = request.getAckDataDocument();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");            
		Date dateTime = null; 
		String newDateTime = null;
		
		String DocumentDate = dataDoc.getDocumentDate();
		
		if("XXXXXXXXXXXXXXX".equals(DocumentDate)){
		    	newDateTime = DocumentDate;
		}else{
			if(DocumentDate!=null && DocumentDate.length()>0){
				try{
					dateTime = sdf.parse(DocumentDate);	
					newDateTime = sdf.format(dateTime);
				} catch (ParseException ex) {
		            		throw new Exception(ex.getMessage());
			        }
			}
		}
			
		String DocumentType = dataDoc.getDocumentType();
		if(DocumentType==null || DocumentType.length()<=0)DocumentType="";
		
		String DocumentNo   = dataDoc.getDocumentNo();
		if(DocumentNo==null || DocumentNo.length()<=0)DocumentNo="";
		
		String FileName	    = dataDoc.getFileName();
		if(FileName==null || FileName.length()<=0)FileName="";
		
		String ReturnCode   = dataDoc.getReturnCode();
		if(ReturnCode==null || ReturnCode.length()<=0)ReturnCode="";
		
		String Description  = dataDoc.getDescription();
		if(Description==null || Description.trim().length()<=0)Description="SUKSES";
		
		String ACKDataString = newDateTime.concat("|").concat(PubUtil.padRight(DocumentType,4).concat("|")).concat(
					PubUtil.padRight(DocumentNo,21).concat("|")).concat(
					PubUtil.padRight(FileName,100).concat("|")).concat(
					PubUtil.padLeft(ReturnCode,"0",4).concat("|")).concat(
					PubUtil.padRight(Description,  100));
		
		return ACKDataString;
	}
}
