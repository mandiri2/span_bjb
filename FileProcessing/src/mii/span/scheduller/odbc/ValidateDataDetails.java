package mii.span.scheduller.odbc;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import javax.faces.bean.ManagedProperty;

import mii.span.Log;
import mii.span.atomic.response.odbc.Ack;
import mii.span.atomic.response.odbc.ack.Accepted;
import mii.span.constant.BusinessConstant;
import mii.span.constant.MdrCfgMainConstant;
import mii.span.constant.SParameterConstant;
import mii.span.db.Service;
import mii.span.db.service.Odbc;
import mii.span.doc.ApplicationArea;
import mii.span.doc.DataArea;
import mii.span.doc.FileSFTP;
import mii.span.doc.Footer;
import mii.span.doc.MdrCfgMain;
import mii.span.doc.SP2DDocument;
import mii.span.model.Provider;
import mii.span.process.model.DataFiles;
import mii.span.process.model.DeleteFileRequest;
import mii.span.process.model.FtpParam;
import mii.span.process.model.GetDataFilesResponse;
import mii.span.process.model.InsertSPANDataValidationResponse;
import mii.span.process.model.ProviderInfo;
import mii.span.process.model.SentAckDataFileFtpRequest;
import mii.span.process.model.SentAckDataFileFtpResponse;
import mii.span.process.model.SentAckDataFileRequest;
import mii.span.process.model.SentAckDataFileResponse;
import mii.span.process.model.SpanAckAcceptedRequest;
import mii.span.process.model.SpanAckAcceptedResponse;
import mii.span.process.model.ValidateSpanDataRequest;
import mii.span.process.model.ValidateSpanDataResponse;
import mii.span.sftp.Atomic;
import mii.span.sftp.Services;
import mii.span.util.BusinessUtil;
import mii.span.util.PubUtil;
import mii.span.util.SpanDocumentUtil;

import org.apache.commons.lang.StringUtils;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;

import com.mii.bjb.core.FastBJBConnector;
import com.mii.constant.CommonConstant;
import com.mii.constant.Constants;
import com.mii.dao.SystemParameterDAO;
import com.mii.models.SystemParameter;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.ApplicationInstance;

public class ValidateDataDetails {
	
	@ManagedProperty(value = "#{applicationBean}")
	private static ApplicationBean applicationBean;
	
	private static SystemParameterDAO getSystemParameterDaoBean(){
		applicationBean = ApplicationInstance.getInstance().getApplicationBean();
		return (SystemParameterDAO) applicationBean.getFileProcessContext().getBean("sysParamDAO");
	}
	
	/**
	 * Wrapper
	 */
	public void runSpanDataValidation(){
		spanDataValidation();
	}
	
	
	/**
	 * Code
	 */
	public static void spanDataValidation(){
		System.out.println("START SPAN DATA VALIDATION");
		
		//o mandiri.span.scheduller.odbc.ValidateDataDetails:getDataFiles
		GetDataFilesResponse getDataFilesResponse = getDataFiles();
		List<FileSFTP> Files = getDataFilesResponse.getFiles();
		String Status = getDataFilesResponse.getStatus();
		String ErrorMessage = getDataFilesResponse.getErrorMessage();
		String FTP_STATUS = getDataFilesResponse.getFtpStatus();
		//c mandiri.span.scheduller.odbc.ValidateDataDetails:getDataFiles
		
		//o loop process 5 files first
		List<FileSFTP> newFiles = new ArrayList<FileSFTP>();
		if(Files!=null){
			for(FileSFTP Files2 : Files){
				//branch Manage cannot extract jar
					if("SUCCESS".equalsIgnoreCase(Files2.getStatus())){
						newFiles.add(Files2);
					}
					else{
						//default sequence error code and error message
						String[] ErrorDetails = PubUtil.tokenize(Files2.getErrorMessage(), "|");
						String ErrorCode = ErrorDetails[0];
						ErrorMessage = ErrorDetails[1];
						String dateTime = BusinessUtil.getDateByPattern("yyyy-MM-dd HH:mm:ss");
						
						String createFile = replaceFileNameExtentionToTxt(Files2);
							
						//branch on errorCode
							if("A003".equalsIgnoreCase(ErrorCode)){
//								String TOTAL_RETRY = Odbc.selectGetFileRetry(Files2.getFileName());
								
								//branch on TOTAL_RETRY
//								if(StringUtils.isEmpty(TOTAL_RETRY)){
//									Odbc.insertGetFileRetry(Files2.getFileName(), "1", Files2.getErrorMessage(), dateTime, dateTime);
//								}
//								else if("3".equalsIgnoreCase(TOTAL_RETRY)){
//									Odbc.updateGetFileRetry(TOTAL_RETRY, Files2.getFileName());
									newFiles.add(Files2);
//								}
//								else{
									//default
//									TOTAL_RETRY = PubUtil.addInts(TOTAL_RETRY, "1");
//									Odbc.updateGetFileRetry(TOTAL_RETRY, Files2.getFileName());
//								}
							}
							else if("A009".equalsIgnoreCase(ErrorCode)){
								//do nothing
							}
							else if("A005".equalsIgnoreCase(ErrorCode)){
								newFiles.add(Files2);
							}
							else{
								//default
								newFiles.add(Files2);
							}
					}
					
					int sizeFileList = newFiles.size();
					
					//branch
						if(sizeFileList<5){
							//do nothing
						}
						else{
							break;	//EXIT LOOP
						}
			}
		}
		
		//c loop process 5 files first
		
		//o map
		Files = newFiles;
		//c map
		
		//o loop.over.files
		if(Files!=null){
			for(FileSFTP Files2 : Files){
				try { 
					//o new.file
						String batchID = BusinessUtil.batchID();
						String trxHeaderID = BusinessUtil.trxHeaderID();
						SP2DDocument spanDocument = null;
						String ErrorCode = null;
						String SPANProviderName = null;
						String SPANProviderCode = null;
						String SPANProviderAcct = null;
						String xmlvalues = null;
						//o branch.on/Files/Status
							//o SUCCESS
							if("SUCCESS".equalsIgnoreCase(Files2.getStatus())){
								
								//o mandiri.span.scheduller.odbc.ValidateDataDetails:getSpanDocument
								Object[] objects2 = getSpanDocument(Files2.getBytes());
								spanDocument = (SP2DDocument)objects2[0];
								String docType = (String)objects2[1];
								//c mandiri.span.scheduller.odbc.ValidateDataDetails:getSpanDocument
								
								//o mandiri.span.db.service:getProviderInfo
								ProviderInfo providerInfo = Service.getProviderInfo(BusinessConstant.SPAN, null);
								List<Provider> ResultList = providerInfo.getResultList();
								Provider ResultDoc = providerInfo.getResultDoc();
								//c mandiri.span.db.service:getProviderInfo

								//o map.get.late.file.hour
//								FtpParam ftpParam = Service.selectSParamFTP(SParameterConstant._20141230P01);
//								String lateFileHour = ftpParam.getValue();
								String lateFileHour = Service.getTparameter(Constants.SPAN_CUT_OFF_TIME);
								//c map.get.late.file.hour
								
								//o mandiri.span.scheduller.odbc.ValidateDataDetails:validateSpanData
								ValidateSpanDataRequest validateSpanDataRequest = new ValidateSpanDataRequest();
								validateSpanDataRequest.setSpanDocument(spanDocument);
								validateSpanDataRequest.setSpanBankAcct(ResultList);
								validateSpanDataRequest.setLateFileHour(lateFileHour);
								
								ValidateSpanDataResponse validateSpanDataResponse = validateSpanData(validateSpanDataRequest);
								
								Status = validateSpanDataResponse.getStatus();
								ErrorMessage = validateSpanDataResponse.getErrorMessage();
								SPANProviderName = validateSpanDataResponse.getSpanProviderName();
								SPANProviderCode = validateSpanDataResponse.getSpanProviderCode();
								SPANProviderAcct = validateSpanDataResponse.getSpanProviderAcct();
								String DocumentDate = validateSpanDataResponse.getDocumentDate();
								//c mandiri.span.scheduller.odbc.ValidateDataDetails:validateSpanData
								
								//o branch.on.Status
									String DocumentType = null;
									String procStatus = null;
									String procDesc = null;
									String responseCode = null;
									String ackDescription = null;
									String ackStatus = null;
									String ackFileName = null;
									//o sequence.SUCCESS.insert.into.db.SPAN_DATA_VALIDATION
									if("SUCCESS".equalsIgnoreCase(Status)){
										//o branch.ErrorMessage
											String ErrorMessageValidation = null;
											//o map.null
											if(StringUtils.isEmpty(ErrorMessage)){
												ErrorCode = "0000";
												ErrorMessageValidation = "Success to validating data";
												System.out.println("Success to validating data");
											}//c map.null
											//o map.default
											else{ 
												//o map
												System.out.println("***** ERROR MESSAGE "+ErrorMessage);
												String[] ErrorDetails = PubUtil.tokenize(ErrorMessage, "|");
												//c map
												for(String a : ErrorDetails){
													System.out.println("ERROR DETAILS " + a);
												}
												//o map
												ErrorCode = ErrorDetails[0];
												ErrorMessageValidation = ErrorDetails[1];
												//c map
											}
											//c map.default
										//c branch.ErrorMessage
										
										//o map
										procStatus = "1";
										procDesc = ErrorMessageValidation;
										responseCode = ErrorCode;
										ackDescription = "";
										DocumentType = docType;
										
										//c map
											
										//o branch.on.responseCode
											//o 005.sent.ack.for.lately.file
											if("005".equalsIgnoreCase(responseCode)){
												//o map
												String[] ErrorDetails = PubUtil.tokenize(ErrorMessage, "|");
												//c map
													
												//o map
												ErrorCode = ErrorDetails[0];
												ErrorMessageValidation = ErrorDetails[1];
												String filenameACK = Files2.getFileName().replace("jar", "xml");
												//c map
												
												//o mandiri.span.atomic.response.odbc.ack.accepted:spanACKAccepted
												SpanAckAcceptedRequest spanAckAcceptedRequest = new SpanAckAcceptedRequest();
												spanAckAcceptedRequest.setErrorMessage(ErrorMessageValidation);
												spanAckAcceptedRequest.setErrorCode(ErrorCode);
												spanAckAcceptedRequest.setSpanDocument(spanDocument);
												spanAckAcceptedRequest.setFileName(filenameACK);
												
												SpanAckAcceptedResponse spanAckAcceptedResponse = Accepted.spanACKAccepted(spanAckAcceptedRequest);
												Status = spanAckAcceptedResponse.getStatus();
												ErrorMessage = spanAckAcceptedResponse.getErrorMessage();
												String ACKDataString = spanAckAcceptedResponse.getAckDataString();
												DocumentType = spanAckAcceptedResponse.getDocumentType();
												//c mandiri.span.atomic.response.odbc.ack.accepted:spanACKAccepted
													
												//o mandiri.span.atomic.response.odbc.ack:sentACKDataFile
												SentAckDataFileRequest sentAckDataFileRequest = new SentAckDataFileRequest();
												sentAckDataFileRequest.setFilename(Files2.getFileName());
												sentAckDataFileRequest.setDocumentType(DocumentType);
												sentAckDataFileRequest.setErrorMessage(ErrorMessageValidation);
												sentAckDataFileRequest.setErrorCode(ErrorCode);
												sentAckDataFileRequest.setAckDataString(ACKDataString);
												
												SentAckDataFileResponse sentAckDataFileResponse = Ack.sentACKDataFile(sentAckDataFileRequest);
												
												Status = sentAckDataFileResponse.getStatus();
												ErrorMessage = sentAckDataFileResponse.getErrorMessage();
												String ACKFilename = sentAckDataFileResponse.getAckFileName();
												String statussend = sentAckDataFileResponse.getStatusSend();
												//c mandiri.span.atomic.response.odbc.ack:sentACKDataFile
														
												//o branch.on.Status
													//o map.SUCCESS
													ErrorMessage = ErrorMessageValidation;
													procStatus = "1";
													procDesc = PubUtil.concat("File failed to validation : ",ErrorMessageValidation);
													responseCode = ErrorCode;
													ackFileName = ACKFilename;
													if("SUCCESS".equalsIgnoreCase(Status)){
														ackStatus = "1";
														ackDescription = "";
													}//c map.SUCCESS
													//o map.default
													else{
														ackStatus = "0";
														ackDescription = ErrorMessage;
													}
													//c map.default
													
												//c branch.on.Status
												
												//o branch.Put.ACK.to.FTP
													//o ackstatus0
													if("0".equalsIgnoreCase(ackStatus) 
															&& "1".equalsIgnoreCase(FTP_STATUS)){
														//mandiri.span.atomic.response.odbc.ack:sentACKDataFileFTP
															SentAckDataFileFtpRequest sentAckDataFileFtpRequest = new SentAckDataFileFtpRequest();
															sentAckDataFileFtpRequest.setFilename(Files2.getFileName());
															sentAckDataFileFtpRequest.setDocumentType(DocumentType);
															sentAckDataFileFtpRequest.setErrorMessage(ErrorMessage);
															sentAckDataFileFtpRequest.setErrorCode(ErrorCode);
															sentAckDataFileFtpRequest.setAckDataString(ACKDataString);
															
															SentAckDataFileFtpResponse sentAckDataFileFtpResponse = Ack.sentACKDataFileFTP(sentAckDataFileFtpRequest);
															Status = sentAckDataFileFtpResponse.getStatus();
															ErrorMessage = sentAckDataFileFtpResponse.getErrorMessage();
															ACKFilename = sentAckDataFileFtpResponse.getAckFileName();
													}
													//c ackstatus0
													//o default
													else{
														//default
														//do nothing
													}
													//c default
												//c branch.Put.ACK.to.FTP
														
												}
											//c 005.sent.ack.for.lately.file
											//o 006
											//else if("006".equalsIgnoreCase(responseCode)){ //for backdate file
											else if("A004".equalsIgnoreCase(responseCode)){ //for backdate file
												//o map
												String[] ErrorDetails = PubUtil.tokenize(ErrorMessage, "|");
												//c map
												//o map
												ErrorCode = ErrorDetails[0];
												ErrorMessageValidation = ErrorDetails[1];
												procStatus = "V";
												responseCode = "0000";
												//c map
												
												//Addtional ACK request Perbend//
												String filenameACK = Files2.getFileName().replace("jar", "xml");
												SpanAckAcceptedRequest spanAckAcceptedRequest = new SpanAckAcceptedRequest();
												spanAckAcceptedRequest.setErrorMessage(ErrorMessageValidation);
												spanAckAcceptedRequest.setErrorCode(ErrorCode);
												spanAckAcceptedRequest.setSpanDocument(spanDocument);
												spanAckAcceptedRequest.setFileName(filenameACK);
												SpanAckAcceptedResponse spanAckAcceptedResponse = Accepted.spanACKAccepted(spanAckAcceptedRequest);
												Status = spanAckAcceptedResponse.getStatus();
												ErrorMessage = spanAckAcceptedResponse.getErrorMessage();
												String ACKDataString = spanAckAcceptedResponse.getAckDataString();
												DocumentType = spanAckAcceptedResponse.getDocumentType();
												SentAckDataFileRequest sentAckDataFileRequest = new SentAckDataFileRequest();
												sentAckDataFileRequest.setFilename(Files2.getFileName());
												sentAckDataFileRequest.setDocumentType(DocumentType);
												sentAckDataFileRequest.setErrorMessage(ErrorMessageValidation);
												sentAckDataFileRequest.setErrorCode(ErrorCode);
												sentAckDataFileRequest.setAckDataString(ACKDataString);
												
												SentAckDataFileResponse sentAckDataFileResponse = Ack.sentACKDataFile(sentAckDataFileRequest);
												
												Status = sentAckDataFileResponse.getStatus();
												ErrorMessage = sentAckDataFileResponse.getErrorMessage();
												String ACKFilename = sentAckDataFileResponse.getAckFileName();
												String statussend = sentAckDataFileResponse.getStatusSend();
												
												ErrorMessage = ErrorMessageValidation;
												procStatus = "1";
												procDesc = PubUtil.concat("File failed to validation : ",ErrorMessageValidation);
												responseCode = ErrorCode;
												ackFileName = ACKFilename;
												if("SUCCESS".equalsIgnoreCase(Status)){
													ackStatus = "1";
													ackDescription = "";
												}//c map.SUCCESS
												//o map.default
												else{
													ackStatus = "0";
													ackDescription = ErrorMessage;
												}
												
												if("0".equalsIgnoreCase(ackStatus) 
														&& "1".equalsIgnoreCase(FTP_STATUS)){
													//mandiri.span.atomic.response.odbc.ack:sentACKDataFileFTP
														SentAckDataFileFtpRequest sentAckDataFileFtpRequest = new SentAckDataFileFtpRequest();
														sentAckDataFileFtpRequest.setFilename(Files2.getFileName());
														sentAckDataFileFtpRequest.setDocumentType(DocumentType);
														sentAckDataFileFtpRequest.setErrorMessage(ErrorMessage);
														sentAckDataFileFtpRequest.setErrorCode(ErrorCode);
														sentAckDataFileFtpRequest.setAckDataString(ACKDataString);
														
														SentAckDataFileFtpResponse sentAckDataFileFtpResponse = Ack.sentACKDataFileFTP(sentAckDataFileFtpRequest);
														Status = sentAckDataFileFtpResponse.getStatus();
														ErrorMessage = sentAckDataFileFtpResponse.getErrorMessage();
														ACKFilename = sentAckDataFileFtpResponse.getAckFileName();
												}
											}
											//c 006
											//o default
											else{
												//default
												//do nothing
											}
											//c default
										//c branch.on.responseCode
											
									}
									//c sequence.SUCCESS.insert.into.db.SPAN_DATA_VALIDATION
									//o default.sent.ack.accepted
									else{
										//o sequence error.code.and.error.message
											//o map
											String[] ErrorDetails = PubUtil.tokenize(ErrorMessage, "|");
											//c map
											//o map
											ErrorCode = ErrorDetails[0];
											String ErrorMessageValidation = ErrorDetails[1];
											//c map
										//c sequence error.code.and.error.message
										//o mandiri.span.atomic.response.odbc.ack.accepted:spanACKAccepted
										SpanAckAcceptedRequest spanAckAcceptedRequest = new SpanAckAcceptedRequest();
										spanAckAcceptedRequest.setErrorMessage(ErrorMessageValidation);
										spanAckAcceptedRequest.setErrorCode(ErrorCode);
										spanAckAcceptedRequest.setSpanDocument(spanDocument);
										spanAckAcceptedRequest.setFileName(Files2.getFileName());
										
										SpanAckAcceptedResponse spanAckAcceptedResponse = Accepted.spanACKAccepted(spanAckAcceptedRequest);
										Status = spanAckAcceptedResponse.getStatus();
										ErrorMessage = spanAckAcceptedResponse.getErrorMessage();
										String ACKDataString = spanAckAcceptedResponse.getAckDataString();
										DocumentType = spanAckAcceptedResponse.getDocumentType();
										//c mandiri.span.atomic.response.odbc.ack.accepted:spanACKAccepted
										//o mandiri.span.atomic.response.odbc.ack:sentACKDataFile
										SentAckDataFileRequest sentAckDataFileRequest = new SentAckDataFileRequest();
										sentAckDataFileRequest.setFilename(Files2.getFileName());
										sentAckDataFileRequest.setDocumentType(DocumentType);
										sentAckDataFileRequest.setErrorMessage(ErrorMessageValidation);
										sentAckDataFileRequest.setErrorCode(ErrorCode);
										sentAckDataFileRequest.setAckDataString(ACKDataString);
										
										SentAckDataFileResponse sentAckDataFileResponse = Ack.sentACKDataFile(sentAckDataFileRequest);
										
										Status = sentAckDataFileResponse.getStatus();
										ErrorMessage = sentAckDataFileResponse.getErrorMessage();
										String ACKFilename = sentAckDataFileResponse.getAckFileName();
										String statussend = sentAckDataFileResponse.getStatusSend();
										//c mandiri.span.atomic.response.odbc.ack:sentACKDataFile
										
										//o branch.on.Status
										procStatus = null;
										procDesc = null;
										responseCode = null;
										ackDescription = null;
											//o map.success
											if("SUCCESS".equalsIgnoreCase(Status)){
												ErrorMessage = ErrorMessageValidation;
												procStatus = "0";
												procDesc = PubUtil.concat("File failed to validation : ",ErrorMessageValidation);
												ackStatus = "1";
												responseCode = ErrorCode;
												ackDescription = "";
												ackFileName = ACKFilename;
											}
											//c map.success
											//o map default
											else{
												ErrorMessage = ErrorMessageValidation;
												procStatus = "0";
												procDesc = PubUtil.concat("File failed to validation : ",ErrorMessageValidation);
												ackStatus = "0";
												responseCode = ErrorCode;
												ackDescription = ErrorMessage;
												ackFileName = ACKFilename;
											}
											//c map default
										//c branch.on.Status
										//o branch.put.ack.to.ftp
											//o mandiri.span.atomic.response.odbc.ack:sentACKDataFileFTP
											if("0".equalsIgnoreCase(ackStatus)
													&& "1".equalsIgnoreCase(FTP_STATUS)){
												SentAckDataFileFtpRequest sentAckDataFileFtpRequest = new SentAckDataFileFtpRequest();
												sentAckDataFileFtpRequest.setFilename(Files2.getFileName());
												sentAckDataFileFtpRequest.setDocumentType(DocumentType);
												sentAckDataFileFtpRequest.setErrorMessage(ErrorMessage);
												sentAckDataFileFtpRequest.setErrorCode(ErrorCode);
												sentAckDataFileFtpRequest.setAckDataString(ACKDataString);
												
												SentAckDataFileFtpResponse sentAckDataFileFtpResponse = Ack.sentACKDataFileFTP(sentAckDataFileFtpRequest);
												
												Status = sentAckDataFileFtpResponse.getStatus();
												ErrorMessage = sentAckDataFileFtpResponse.getErrorMessage();
												ACKFilename = sentAckDataFileFtpResponse.getAckFileName();
											}
											//c mandiri.span.atomic.response.odbc.ack:sentACKDataFileFTP
											//o map.default
											else{
												//do nothing
											}
											//c map.default
										//c branch.put.ack.to.ftp
										
									}
									//c default.sent.ack.accepted
									
								//c branch.on.Status
								
								//o sequence.delete.file.in.span's.folder
									//o map
									/*String spanHost = mii.span.db.adapter.Odbc.selectMdrCfgMain(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_IPADDRESS).getValue1();
									String spanPort = mii.span.db.adapter.Odbc.selectMdrCfgMain(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_PORT).getValue1();
									String LOCAL_DIR = mii.span.db.adapter.Odbc.selectMdrCfgMain(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_PUT_LOCAL_DIR).getValue1();
									String spanUser = mii.span.db.adapter.Odbc.selectMdrCfgMain(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_USERNAME).getValue1();
									String spanPass = mii.span.db.adapter.Odbc.selectMdrCfgMain(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_PASSWORD).getValue1();
									String spanACKDir = mii.span.db.adapter.Odbc.selectMdrCfgMain(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_PUT_REMOTE_DIR).getValue1();
									String BACKUP_DIR = mii.span.db.adapter.Odbc.selectMdrCfgMain(MdrCfgMainConstant.SPAN_DEPKEU_MDR_BACKUP_FTP_PUT_DIR).getValue1();
									String mdrHost = mii.span.db.adapter.Odbc.selectMdrCfgMain(MdrCfgMainConstant.SPAN_DEPKEU_MDR_BACKUP_FTP_IPADDRESS).getValue1();
									String mdrPort = mii.span.db.adapter.Odbc.selectMdrCfgMain(MdrCfgMainConstant.SPAN_DEPKEU_MDR_BACKUP_FTP_PORT).getValue1();
									String mdrUser = mii.span.db.adapter.Odbc.selectMdrCfgMain(MdrCfgMainConstant.SPAN_DEPKEU_MDR_BACKUP_FTP_USERNAME).getValue1();
									String mdrPass = mii.span.db.adapter.Odbc.selectMdrCfgMain(MdrCfgMainConstant.SPAN_DEPKEU_MDR_BACKUP_FTP_PASSWORD).getValue1();
									String spanOutboundDir = mii.span.db.adapter.Odbc.selectMdrCfgMain(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_GET_REMOTE_DIR).getValue1();*/
									
									//TODO Error Handling
									SystemParameter systemParameterspanHost =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_IPADDRESS).get(0);
									String spanHost = systemParameterspanHost.getParam_value();
									SystemParameter systemParameterspanPort =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_PORT).get(0);
									String spanPort = systemParameterspanPort.getParam_value();
									SystemParameter systemParameterLOCAL_DIR =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_PUT_LOCAL_DIR).get(0);
									String LOCAL_DIR = systemParameterLOCAL_DIR.getParam_value();
									SystemParameter systemParameterspanUser =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_USERNAME).get(0);
									String spanUser = systemParameterspanUser.getParam_value();
									SystemParameter systemParameterspanPass =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_PASSWORD).get(0);
									String spanPass = systemParameterspanPass.getParam_value();
									SystemParameter systemParameterspanACKDir =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_PUT_REMOTE_DIR).get(0);
									String spanACKDir = systemParameterspanACKDir.getParam_value();
									SystemParameter systemParameterBACKUP_DIR =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_BJB_BACKUP_FTP_PUT_DIR).get(0);
									String BACKUP_DIR = systemParameterBACKUP_DIR.getParam_value();
									SystemParameter systemParametermdrHost =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_BJB_BACKUP_FTP_PUT_DIR).get(0);
									String mdrHost = systemParametermdrHost.getParam_value();
									SystemParameter systemParametermdrPort =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_BJB_BACKUP_FTP_PORT).get(0);
									String mdrPort = systemParametermdrPort.getParam_value();
									SystemParameter systemParametermdrUser =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_BJB_BACKUP_FTP_USERNAME).get(0);
									String mdrUser = systemParametermdrUser.getParam_value();
									SystemParameter systemParametermdrPass =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_BJB_BACKUP_FTP_PASSWORD).get(0);
									String mdrPass = systemParametermdrPass.getParam_value();
									SystemParameter systemParameterspanOutboundDir =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_GET_REMOTE_DIR).get(0);
									String spanOutboundDir = systemParameterspanOutboundDir.getParam_value();
									SystemParameter systemParameterspanLocalPathManual =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_MANUAL_PUT_LOCAL_DIR).get(0);
									String localPathManual = systemParameterspanLocalPathManual.getParam_value();
									SystemParameter systemParameterspanLocalPathManualBackup =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_MANUAL_PUT_LOCAL_BACKUP_DIR).get(0);
									String localPathManualBackup = systemParameterspanLocalPathManualBackup.getParam_value();
									
									//c map
									//o sequence.for.outbound.folder
										//o mandiri.span.util:getFolderName
										List<String> spanFolderNames = BusinessUtil.getFolderName(spanOutboundDir);
										//c mandiri.span.util:getFolderName
										//o loop.over.spanFolderNames
										for(String spanFolderNames2 : spanFolderNames){
											//o map
											int indexValue = spanFolderNames2.indexOf(DocumentType);
											//c map
											//o branch
												//o map
												if(indexValue>0){
													spanOutboundDir = spanFolderNames2;
												}
												//c map
											//c branch
										}
										//c loop.over.spanFolderNames
										//o map
										
										//c map
										//o mandiri.span.sftp.atomic:deleteFile
										DeleteFileRequest deleteFileRequest = new DeleteFileRequest();
										deleteFileRequest.setNameOfFileToDelete(Files2.getFileNameOri());
										deleteFileRequest.setServerName(spanHost);
										deleteFileRequest.setSpanPath(spanOutboundDir);
										deleteFileRequest.setPassword(spanPass);
										deleteFileRequest.setUsername(spanUser);
										
										Status = Atomic.deleteFile(deleteFileRequest);
//										if(String.valueOf(false).equals(Status)){
										try{
											Status = Atomic.deleteFileLocalManual(localPathManual, Files2.getFileNameOri(), localPathManualBackup);
										}catch(Exception e){
											System.out.println("File Allready Deleted or Not Found in local Upload Directory.");
										}
//										}
										//c mandiri.span.sftp.atomic:delete
										//o mandiri.span.log:debugLog
										Log.debugLog("SPAN", "SpanDataValidation", "INFO", "Deleting file : " + Files2.getFileNameOri());
										//c mandiri.span.log:debugLog
										
								//c sequence.for.outbound.folder
									
								//c sequence.delete.file.in.span's.folder
								
								//o branch
									String trxType = null;
									//o map spanprovidercode==SPN02
									if("SPN02".equalsIgnoreCase(SPANProviderCode)){
										trxType = "2";
									}
									//c map spanprovidercode==SPN02
									//o map default
									else{
										trxType = "0";
									}
									//c map default
								//c branch
								
								//o documentToXmlValues
								xmlvalues = PubUtil.documentToXmlValues(SP2DDocument.class, spanDocument);
								//c documentToXmlValues
								
								//o mandiri.span.db.service:insertSPANDataValidation
								InsertSPANDataValidationResponse insertSPANDataValidationResponse = Service.insertSPANDataValidation(Files2.getFileNameOri(), 
										batchID, trxHeaderID, docType, SPANProviderAcct, SPANProviderName, DocumentDate, spanDocument.getFooter().getTotalAmount(), 
										spanDocument.getFooter().getTotalCount(), procStatus, procDesc, xmlvalues, responseCode, ackFileName, ackStatus, ackDescription, trxType, 
										null);
								Thread.sleep(1000);
								Status = insertSPANDataValidationResponse.getStatus();
								ErrorMessage = insertSPANDataValidationResponse.getErrorMessage();
								//c mandiri.span.db.service:insertSPANDataValidation
							
								//o branch.on.ftp_status
									//o 1.sequence(flagging_for_ftp_file)
										//o mandiri.span.db.adapter.odbc:UpdateFTPStatus
										if("1".equalsIgnoreCase(FTP_STATUS)){
											mii.span.db.adapter.Odbc.UpdateFTPStatus(FTP_STATUS, Files2.getFileNameOri(), batchID, trxHeaderID);
										}
										//c mandiri.span.db.adapter.odbc:UpdateFTPStatus
										//o map default.do.nothing.for.sftpfile
										else{
											//do nothing
										}
										//c map default.do.nothing.for.sftpfile
									//c 1.sequence(flagging_for_ftp_file)
								//c branch.on.ftp_status
								
								
							}
							//c SUCCESS
							//o default.sequence.A003
							else{
								
								//o sequence.error_code_and_error_message
									//o map
									String[] ErrorDetails = PubUtil.tokenize(Files2.getErrorMessage(), "|");
									//c map
								
									//o map
									ErrorCode = ErrorDetails[0];
									ErrorMessage = ErrorDetails[1];
									//c map
									
									//o branch.on.errorcode
									String ACKDataString = null;
									String DocumentType  = null;
										//o sequence.A003
										if("A003".equalsIgnoreCase(ErrorCode)){
											SpanAckAcceptedRequest spanAckAcceptedRequest = new SpanAckAcceptedRequest();
											spanAckAcceptedRequest.setErrorMessage(Files2.getErrorMessage());
											spanAckAcceptedRequest.setErrorCode(ErrorCode);
											spanAckAcceptedRequest.setSpanDocument(spanDocument);
											spanAckAcceptedRequest.setFileName(Files2.getFileName());
											
											SpanAckAcceptedResponse spanAcceptedResponse = Accepted.spanACKAccepted(spanAckAcceptedRequest);
											
											Status = spanAcceptedResponse.getStatus();
											ErrorMessage = spanAcceptedResponse.getErrorMessage();
											ACKDataString = spanAcceptedResponse.getAckDataString();
											DocumentType = spanAcceptedResponse.getDocumentType();
										}
										//c sequence.A003
										//o sequence.B001
										else if("B001".equalsIgnoreCase(ErrorCode)){
											SpanAckAcceptedRequest spanAckAcceptedRequest = new SpanAckAcceptedRequest();
											spanAckAcceptedRequest.setErrorMessage(Files2.getErrorMessage());
											spanAckAcceptedRequest.setErrorCode(ErrorCode);
											spanAckAcceptedRequest.setSpanDocument(spanDocument);
											spanAckAcceptedRequest.setFileName(Files2.getFileName());
											
											SpanAckAcceptedResponse spanAcceptedResponse = Accepted.spanACKAccepted(spanAckAcceptedRequest);
											
											Status = spanAcceptedResponse.getStatus();
											ErrorMessage = spanAcceptedResponse.getErrorMessage();
											ACKDataString = spanAcceptedResponse.getAckDataString();
											DocumentType = spanAcceptedResponse.getDocumentType();
										}
										//c sequence.B001
										//o sequence.B002
										else if("B002".equalsIgnoreCase(ErrorCode)){
											SpanAckAcceptedRequest spanAckAcceptedRequest = new SpanAckAcceptedRequest();
											spanAckAcceptedRequest.setErrorMessage(Files2.getErrorMessage());
											spanAckAcceptedRequest.setErrorCode(ErrorCode);
											spanAckAcceptedRequest.setSpanDocument(spanDocument);
											spanAckAcceptedRequest.setFileName(Files2.getFileName());
											
											SpanAckAcceptedResponse spanAcceptedResponse = Accepted.spanACKAccepted(spanAckAcceptedRequest);
											
											Status = spanAcceptedResponse.getStatus();
											ErrorMessage = spanAcceptedResponse.getErrorMessage();
											ACKDataString = spanAcceptedResponse.getAckDataString();
											DocumentType = spanAcceptedResponse.getDocumentType();
										}
										//c sequence.B002
										//o sequence.B003
										else if("B003".equalsIgnoreCase(ErrorCode)){
											SpanAckAcceptedRequest spanAckAcceptedRequest = new SpanAckAcceptedRequest();
											spanAckAcceptedRequest.setErrorMessage(Files2.getErrorMessage());
											spanAckAcceptedRequest.setErrorCode(ErrorCode);
											spanAckAcceptedRequest.setSpanDocument(spanDocument);
											spanAckAcceptedRequest.setFileName(Files2.getFileName());
											
											SpanAckAcceptedResponse spanAcceptedResponse = Accepted.spanACKAccepted(spanAckAcceptedRequest);
											
											Status = spanAcceptedResponse.getStatus();
											ErrorMessage = spanAcceptedResponse.getErrorMessage();
											ACKDataString = spanAcceptedResponse.getAckDataString();
											DocumentType = spanAcceptedResponse.getDocumentType();
										}
										//c sequence.B003
										//o sequence.B004
										else if("B004".equalsIgnoreCase(ErrorCode)){
											SpanAckAcceptedRequest spanAckAcceptedRequest = new SpanAckAcceptedRequest();
											spanAckAcceptedRequest.setErrorMessage(Files2.getErrorMessage());
											spanAckAcceptedRequest.setErrorCode(ErrorCode);
											spanAckAcceptedRequest.setSpanDocument(spanDocument);
											spanAckAcceptedRequest.setFileName(Files2.getFileName());
											
											SpanAckAcceptedResponse spanAcceptedResponse = Accepted.spanACKAccepted(spanAckAcceptedRequest);
											
											Status = spanAcceptedResponse.getStatus();
											ErrorMessage = spanAcceptedResponse.getErrorMessage();
											ACKDataString = spanAcceptedResponse.getAckDataString();
											DocumentType = spanAcceptedResponse.getDocumentType();
										}
										//c sequence.B004
										//o sequence.A009
										else if("A009".equalsIgnoreCase(ErrorCode)){
											continue;
										}
										//c sequence.A009
									//c branch.on.errorcode
									
									//o mandiri.span.atomic.response.odbc.ack:sentACKDataFile
									System.out.println("******************************************** document type "+DocumentType);
									System.out.println("******************************************** Error Message "+ErrorMessage);
									System.out.println("******************************************** Error Code "+ErrorCode);
									SentAckDataFileRequest sentAckDataFileRequest = new SentAckDataFileRequest();
									sentAckDataFileRequest.setFilename(Files2.getFileName());
									sentAckDataFileRequest.setDocumentType(DocumentType);
									sentAckDataFileRequest.setErrorMessage(Files2.getErrorMessage());
									sentAckDataFileRequest.setErrorCode(ErrorCode);
									sentAckDataFileRequest.setAckDataString(ACKDataString);
									
									SentAckDataFileResponse sentAckDataFileResponse = Ack.sentACKDataFile(sentAckDataFileRequest);
									
									Status = sentAckDataFileResponse.getStatus();
									ErrorMessage = sentAckDataFileResponse.getErrorMessage();
									String ACKFilename = sentAckDataFileResponse.getAckFileName();
									String statussend = sentAckDataFileResponse.getStatusSend();
									//c mandiri.span.atomic.response.odbc.ack:sentACKDataFile	
									
									//o branch.on.status
									String procStatus = null;
									String procDesc = null;
									String ackStatus = null;
									String ackDescription = null;
										//o success.sequence
										if("SUCCESS".equalsIgnoreCase(Status)){
											//o map
											procStatus = "0";
											procDesc = PubUtil.concat("Failed to validating Data : ", Files2.getErrorMessage());
											ackStatus = "1";
											ackDescription = ErrorMessage;
											//String ackFileName = filenamFA; ?? filenamFA dari mana??
											
											//c map
											//o sequence.delete.file.in.spans.folder
												//o map
												
												/*String spanHost = mii.span.db.adapter.Odbc.selectMdrCfgMain(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_IPADDRESS).getValue1();
												String spanPort = mii.span.db.adapter.Odbc.selectMdrCfgMain(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_PORT).getValue1();
												String LOCAL_DIR = mii.span.db.adapter.Odbc.selectMdrCfgMain(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_PUT_LOCAL_DIR).getValue1();
												String spanUser = mii.span.db.adapter.Odbc.selectMdrCfgMain(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_USERNAME).getValue1();
												String spanPass = mii.span.db.adapter.Odbc.selectMdrCfgMain(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_PASSWORD).getValue1();
												String spanACKDir = mii.span.db.adapter.Odbc.selectMdrCfgMain(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_PUT_REMOTE_DIR).getValue1();
												String BACKUP_DIR = mii.span.db.adapter.Odbc.selectMdrCfgMain(MdrCfgMainConstant.SPAN_DEPKEU_MDR_BACKUP_FTP_PUT_DIR).getValue1();
												String mdrHost = mii.span.db.adapter.Odbc.selectMdrCfgMain(MdrCfgMainConstant.SPAN_DEPKEU_MDR_BACKUP_FTP_IPADDRESS).getValue1();
												String mdrPort = mii.span.db.adapter.Odbc.selectMdrCfgMain(MdrCfgMainConstant.SPAN_DEPKEU_MDR_BACKUP_FTP_PORT).getValue1();
												String mdrUser = mii.span.db.adapter.Odbc.selectMdrCfgMain(MdrCfgMainConstant.SPAN_DEPKEU_MDR_BACKUP_FTP_USERNAME).getValue1();
												String mdrPass = mii.span.db.adapter.Odbc.selectMdrCfgMain(MdrCfgMainConstant.SPAN_DEPKEU_MDR_BACKUP_FTP_PASSWORD).getValue1();
												String spanOutboundDir = mii.span.db.adapter.Odbc.selectMdrCfgMain(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_GET_REMOTE_DIR).getValue1();*/
													
												SystemParameter systemParameterspanHost =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_IPADDRESS).get(0);
												String spanHost = systemParameterspanHost.getParam_value();
												SystemParameter systemParameterspanPort =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_PORT).get(0);
												String spanPort = systemParameterspanPort.getParam_value();
												SystemParameter systemParameterLOCAL_DIR =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_PUT_LOCAL_DIR).get(0);
												String LOCAL_DIR = systemParameterLOCAL_DIR.getParam_value();
												SystemParameter systemParameterspanUser =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_USERNAME).get(0);
												String spanUser = systemParameterspanUser.getParam_value();
												SystemParameter systemParameterspanPass =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_PASSWORD).get(0);
												String spanPass = systemParameterspanPass.getParam_value();
												SystemParameter systemParameterspanACKDir =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_PUT_REMOTE_DIR).get(0);
												String spanACKDir = systemParameterspanACKDir.getParam_value();
												SystemParameter systemParameterBACKUP_DIR =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_PUT_REMOTE_DIR).get(0);
												String BACKUP_DIR = systemParameterBACKUP_DIR.getParam_value();
												SystemParameter systemParametermdrHost =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_PUT_REMOTE_DIR).get(0);
												String mdrHost = systemParametermdrHost.getParam_value();
												SystemParameter systemParametermdrPort =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_BJB_BACKUP_FTP_PORT).get(0);
												String mdrPort = systemParametermdrPort.getParam_value();
												SystemParameter systemParametermdrUser =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_BJB_BACKUP_FTP_USERNAME).get(0);
												String mdrUser = systemParametermdrUser.getParam_value();
												SystemParameter systemParametermdrPass =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_BJB_BACKUP_FTP_PASSWORD).get(0);
												String mdrPass = systemParametermdrPass.getParam_value();
												SystemParameter systemParameterspanOutboundDir =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_GET_REMOTE_DIR).get(0);
												String spanOutboundDir = systemParameterspanOutboundDir.getParam_value();
												SystemParameter systemParameterspanLocalPathManual =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_MANUAL_PUT_LOCAL_DIR).get(0);
												String localPathManual = systemParameterspanLocalPathManual.getParam_value();
												SystemParameter systemParameterspanLocalPathManualBackup =  getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_MANUAL_PUT_LOCAL_BACKUP_DIR).get(0);
												String localPathManualBackup = systemParameterspanLocalPathManualBackup.getParam_value();
												//c map
												//o sequence.for.outbound.folder
													//o mandiri.span.util:getFolderName
													List<String> spanFolderNames = BusinessUtil.getFolderName(spanOutboundDir);
													//c mandiri.span.util:getFolderName
													//o loop.over.spanFolderNames
														for(String spanFolderNames2 : spanFolderNames){
															//o map
															int indexValue = spanFolderNames2.indexOf(DocumentType);
															//c map
															//o branch
																//o map indexvalue>0
																	if(indexValue>0){
																		spanOutboundDir = spanFolderNames2;
																	}
																//c map indexvalue>0
															//c branch
														}
													//c loop.over.spanFolderNames
													//o map
													
													//c map
														
												//c sequence.for.outbound.folder
												//o mandiri.span.sftp.atomic:deleteFile
												DeleteFileRequest deleteFileRequest = new DeleteFileRequest();
												deleteFileRequest.setNameOfFileToDelete(Files2.getFileName());
												deleteFileRequest.setServerName(spanHost);
												deleteFileRequest.setSpanPath(spanOutboundDir);
												deleteFileRequest.setPassword(spanPort);
												deleteFileRequest.setUsername(spanUser);
												
												Status = Atomic.deleteFile(deleteFileRequest);
//												if(String.valueOf(false).equals(Status)){
												try{
													Status = Atomic.deleteFileLocalManual(localPathManual, Files2.getFileNameOri(), localPathManualBackup);
												}catch(Exception e){
													System.out.println("File Allready Deleted or Not Found in local Upload Directory.");
												}
//												}
												//c mandiri.span.sftp.atomic:deleteFile		
												
												
											//c sequence.delete.file.in.spans.folder
										}
											
										//c success.sequence
										//o map.default
										procStatus = "0";
										procDesc = PubUtil.concat("Failed to validating Data : ", Files2.getErrorMessage());
										ackStatus = "0";
										ackDescription = ErrorMessage;
										DocumentType = Files2.getFileName().substring(13,17);
										//FIXME filenamFA dari mana?
										//String ackFileName = filenamFA;
										//c map.default
									//c branch.on.status
									
									//o branch.put.ack.to.ftp
										//o sequence
										if("0".equalsIgnoreCase(ackStatus)
												&& "1".equalsIgnoreCase(FTP_STATUS)){
											//o mandiri.span.atomic.response.odbc.ack:sentACKDataFileFTP
											SentAckDataFileFtpRequest sentAckDataFileFtpRequest = new SentAckDataFileFtpRequest();
											sentAckDataFileFtpRequest.setFilename(Files2.getFileName());
											sentAckDataFileFtpRequest.setDocumentType(DocumentType);
											sentAckDataFileFtpRequest.setErrorMessage(Files2.getErrorMessage());
											sentAckDataFileFtpRequest.setErrorCode(ErrorCode);
											sentAckDataFileFtpRequest.setAckDataString(ACKDataString);
											
											SentAckDataFileFtpResponse fileFtpResponse = Ack.sentACKDataFileFTP(sentAckDataFileFtpRequest);
											
											Status = fileFtpResponse.getStatus();
											ErrorMessage = fileFtpResponse.getErrorMessage();
											ACKFilename = fileFtpResponse.getAckFileName();
											//c mandiri.span.atomic.response.odbc.ack:sentACKDataFileFTP
										}
										//c sequence
										//o map.default.donothing
										else{
											//do nothing
										}
										//c map.default.donothing
									//c branch.put.ack.to.ftp	
									
									//o mandiri.span.db.service:insertSPANDataValidation
									String TRXTYPE = null;
									Service.insertSPANDataValidation(Files2.getFileNameOri(), batchID, trxHeaderID, DocumentType, SPANProviderAcct, 
											SPANProviderName, spanDocument.getDataArea().get(0).getDocumentDate(), spanDocument.getFooter().getTotalAmount(), 
											spanDocument.getFooter().getTotalCount(), "0", Files2.getErrorMessage(), xmlvalues, ErrorCode, 
											ACKFilename, ackStatus, ackDescription, TRXTYPE, null);
									Thread.sleep(1000);
									//c mandiri.span.db.service:insertSPANDataValidation
									
									//o branch.on.ftp_status
											//o sequence.flagging.for.ftp.file
											mii.span.db.adapter.Odbc.UpdateFTPStatus(FTP_STATUS, Files2.getFileNameOri(), batchID, trxHeaderID);
											//c sequence.flagging.for.ftp.file
											//o map.donothing
												//donothing
											//c map.donothing
									//c branch.on.ftp_status
								//c sequence.error_code_and_error_message
							}
							//c default.sequence.A003
						//c branch.on/Files/Status
					
					//c new.file
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
		}
		//c loop.over.files
		System.out.println(Status);
		System.out.println("STOP SPAN DATA VALIDATION");
	}

	private static String replaceFileNameExtentionToTxt(FileSFTP Files2) {
		String createFile = null;
		
		int createFileJar = Files2.getFileName().indexOf("jar");
		int createFileXml = Files2.getFileName().indexOf("xml");
		
		if(createFileXml>0){
			createFile = Files2.getFileName().replace("xml", "txt");
		}
		else if(createFileJar>0){
			createFile = Files2.getFileName().replace("jar", "txt");
		}
		return createFile;
	}
	
	/**
	 * [0]List List<FileSFTP>	<br />
	 * [1]String Status <br />
	 * [2]String ErrorMessage <br />
	 * [3]String FTP_STATUS <br />
	 * mandiri.span.sftp.services:getFiles
	 * @return
	 */
	public static GetDataFilesResponse getDataFiles(){
		GetDataFilesResponse getDataFilesResponse = new GetDataFilesResponse();
		DataFiles dataFiles = Services.getFiles();
		
		String Status = dataFiles.getStatus();
		String ErrorMessage = dataFiles.getErrorMessage();
		List<FileSFTP> fileSFTP = dataFiles.getFiles();
		
		//branch on status
			if(!"SUCCESS".equalsIgnoreCase(Status)){ //default
				//branch on errorMessage
					if(ErrorMessage==null){
						ErrorMessage = "File(s) not found";
					}
					else{
						ErrorMessage = "ERROR";
					}
			}
			else{
				Status = "SUCCESS";
			}
		
		getDataFilesResponse.setStatus(Status);
		getDataFilesResponse.setErrorMessage(ErrorMessage);
		getDataFilesResponse.setFiles(fileSFTP);
		
		return getDataFilesResponse;
	}
	
	/**
	 * mandiri.span.scheduller.odbc.ValidateDataDetails:getSpanDocument <br />
	 * [0]SpanDocument spanDocument
	 * [1]String docType
	 */
	public static Object[] getSpanDocument (byte[] bytes) throws JDOMException, IOException{
		Object[] objects = new Object[2];
		
		String string = new String(bytes);
		
		//SEQUENCE BEFORE
		Document document = PubUtil.xmlNodeToDocument(string);
		
		//custom
		Element sp2dOrSpt = SpanDocumentUtil.getSp2dOrSpt(document);
		String ApplicationAreaMessageTypeIndicatorValue = SpanDocumentUtil.getApplicationAreaMessageTypeIndicatorValue(sp2dOrSpt);
		
		SP2DDocument spanDocument = null;
		String docType = null;
		//branch on /document/soapenv:Envelope/soapenv:Body/SP2D/ApplicationArea/ApplicationAreaMessageTypeIndicator
			if("SP2D".equalsIgnoreCase(ApplicationAreaMessageTypeIndicatorValue)){
				spanDocument = SpanDocumentUtil.jDomToSpanDocument(sp2dOrSpt);
				docType = "SP2D";
			}
			else{
				//default	
				//do nothing ignoring spt
			}
		
		objects[0] = spanDocument;
		objects[1] = docType;
		
		return objects;
	}
	
	/**
	 * mandiri.span.scheduller.odbc.ValidateDataDetails:validateSpanData <br />
	 * output String[] <br />
	 * String[0] Status <br />
	 * String[1] ErrorMessage <br />
	 * String[2] SPANProviderName <br />
	 * String[3] SPANProviderCode <br />
	 * String[4] SPANProviderAcct <br />
	 * String[5] DocumentDate <br />
	 */
	public static ValidateSpanDataResponse validateSpanData(ValidateSpanDataRequest request){
		ValidateSpanDataResponse response = new ValidateSpanDataResponse();
		
		SP2DDocument spanDocument = request.getSpanDocument();
		List<Provider> spanBankAcct = request.getSpanBankAcct();
		String lateFileHour = request.getLateFileHour();
		
		String Status = "";
		double spanAmount = 0;
		String ErrorMessage = "";
		String spanProvAcct = "";	
		String spanProvCode = "";
		String spanProvName = "";
		String spanDocDate = "";
		String spanDocNumber = "";
		int lateFileHr = 0;
		boolean spanValidationStatus = false;
		SimpleDateFormat sdf = null;
		String DocumentDate = "";
		int creationDTInt = 0;
		
		int amountLength = 0;
		int creditAccountLengthOverbooking = 0;
		int creditAccountLengthSKN =0;
		int creditAccountLengthRTGS=0;
		String amountFormat = "";
				
		try{
			// Get valid format
			String[] getValidFormatResult = BusinessUtil.getValidFormat();
			
											      
				amountLength =  Integer.parseInt(getValidFormatResult[0]);
				creditAccountLengthOverbooking = Integer.parseInt(getValidFormatResult[2]);
				creditAccountLengthSKN = Integer.parseInt(getValidFormatResult[3]);
				creditAccountLengthRTGS = Integer.parseInt(getValidFormatResult[4]);
				amountFormat = getValidFormatResult[1];
			
			// spanDocument
			if (spanDocument != null){
				
				ApplicationArea applicationArea = spanDocument.getApplicationArea();
				String SenderIdentifier = applicationArea.getApplicationAreaSenderIdentifier(); 
				String ReceiverIdentifier = applicationArea.getApplicationAreaReceiverIdentifier();
				String DetailSenderIdentifier = applicationArea.getApplicationAreaDetailSenderIdentifier();
				String DetailReceiverIdentifier = applicationArea.getApplicationAreaDetailReceiverIdentifier();
				String CreationDateTime = applicationArea.getApplicationAreaCreationDateTime();
		
				String MessageIdentifier = applicationArea.getApplicationAreaMessageIdentifier(); 
				String MessageTypeIndicator = applicationArea.getApplicationAreaMessageTypeIndicator(); 
				
				sdf = new SimpleDateFormat("yyyy-MM-dd");
			
				creationDTInt = Integer.parseInt(CreationDateTime.substring(11,19).replace(":",""));
				
				Footer footer = spanDocument.getFooter();
				String TotalCount = footer.getTotalCount(); 
				String TotalAmount = footer.getTotalAmount(); 
				String TotalBatchCount = footer.getTotalBatchCount();
				
				
				List<DataArea> spanDocumentListDataArea = spanDocument.getDataArea();
		
				if (spanDocumentListDataArea != null){	
					if (!TotalCount.equals(String.valueOf(spanDocumentListDataArea.size()))) throw new Exception("A007|Total count tidak sama dengan  total line count");
					
					sdf = new SimpleDateFormat("yyyyMMddHHmmss");
					
					for (DataArea dataArea : spanDocumentListDataArea){
						
						DocumentDate = dataArea.getDocumentDate();
						String DocumentNumber = dataArea.getDocumentNumber();
						String BeneficiaryName = dataArea.getBeneficiaryName(); 
						String BeneficiaryBankCode = dataArea.getBeneficiaryBankCode();
						String BeneficiaryBank = dataArea.getBeneficiaryBank();
						String BeneficiaryAccount = dataArea.getBeneficiaryAccount();
						String Amount = dataArea.getAmount();
						String CurrencyTarget = dataArea.getCurrencyTarget();
						String Description = dataArea.getDescription();
						String AgentBankCode = dataArea.getAgentBankCode();
						String AgentBankAccountNumber = dataArea.getAgentBankAccountNumber();
						String AgentBankAccountName = dataArea.getAgentBankAccountName();
						String EmailAddress = dataArea.getEmailAddress();
						String SwiftCode = dataArea.getSwiftCode();
						String IBANCode = dataArea.getIBANCode();
						String PaymentMethod = dataArea.getPaymentMethod();
						String SP2DCount = dataArea.getSP2DCount();
							// spanBankAcct
							if(!spanValidationStatus){
								if ( spanBankAcct != null){
									for (Provider tProvider : spanBankAcct){
										if(AgentBankAccountNumber.equals(tProvider.getProviderAccount())){
											spanProvCode = tProvider.getProviderCode();
											spanProvName = tProvider.getProviderName(); 
											spanProvAcct = tProvider.getProviderAccount(); 
											spanValidationStatus = true;
											break;
										}
			
									}
								}
							}
		
							//check span's debit account double
							if("".equals(spanProvAcct))throw new Exception("B001|Account Debit Not Found "+AgentBankAccountNumber);
							else if(!spanProvAcct.equals(AgentBankAccountNumber)) throw new Exception("B002|Duplicate Account Debit Number");
						
							if("".equals(spanDocDate))spanDocDate = DocumentDate;
							else if(!spanDocDate.equals(DocumentDate)) throw new Exception("B003|Duplicate Document Date");
		
							if("".equals(spanDocNumber))spanDocNumber = DocumentNumber;
							else if(spanDocNumber.equals(DocumentNumber)) throw new Exception("B004|Duplicate Document Number");
							
							//Add validation Credit Account, Amount, and Payment Method
							
							//Validate Payment Method
							if("".equals(PaymentMethod) || (!"0".equals(PaymentMethod) && !"1".equals(PaymentMethod) && !"2".equals(PaymentMethod)))
								throw new Exception("C001|Invalid Payment Method " + PaymentMethod);
							
//									System.out.println("SPAN_DEBUG PM VALIDATION : " + i + "VALID");
							
							//Validate BeneficiaryAccount overbooking -- Di takeout by BJB
							/*if("0".equals(PaymentMethod)){
								if(!validateNumeric(BeneficiaryAccount)) throw new Exception("C002|Invalid Beneficiary Account Format " + BeneficiaryAccount);
								
								if(!validateLength(BeneficiaryAccount, creditAccountLengthOverbooking))
									throw new Exception("C002|Invalid Beneficiary Account Length " + BeneficiaryAccount);
							}
							
							//Validate BeneficiaryAccount RTGS
							if("1".equals(PaymentMethod)){
								if(!validateNumeric(BeneficiaryAccount))
									throw new Exception("C002|Invalid Beneficiary Account Format " + BeneficiaryAccount);
								
								if(!validateLength(BeneficiaryAccount, creditAccountLengthRTGS))
									throw new Exception("C002|Invalid Beneficiary Account Length " + BeneficiaryAccount);
							}
							
							//Validate BeneficiaryAccount SKN
							if("2".equals(PaymentMethod)){
								if(!validateNumeric(BeneficiaryAccount))
									throw new Exception("C002|Invalid Beneficiary Account Format " + BeneficiaryAccount);
								
								if(!validateLength(BeneficiaryAccount, creditAccountLengthSKN))
									throw new Exception("C002|Invalid Beneficiary Account Length " + BeneficiaryAccount);
							}*/
							
//									System.out.println("SPAN_DEBUG CREDITACCT VALIDATION : " + i + "VALID");
		
							//Validate Amount				
							if(!validateAmount(Amount, amountFormat))
								throw new Exception("C003|Invalid Amount Format " + Amount);
									
							//if(!validateLength(Amount, amountLength))
								//throw new ServiceException("C003|Invalid Amount Length " + Amount);
							
							//if overbooking
							/*if("0".equalsIgnoreCase(PaymentMethod)){
//								System.out.println("**************************ENABLED FAST FILTER PLEASE**************************");
								if(CommonConstant.FAST_NOT_FOUND.equalsIgnoreCase(checkBA(BeneficiaryName, BeneficiaryAccount))){
									throw new Exception(PubUtil.concat("3003|",CommonConstant.FAST_NOT_FOUND," ",BeneficiaryName," ", BeneficiaryAccount));
								}	
							}*/

							
							spanAmount = Double.parseDouble(Amount) + spanAmount;
							
//									System.out.println("SPAN_DEBUG AMOUNT VALIDATION : " + i + "VALID");
		
					}
				}
				//note win : tidak menggunakan else seperti di is, karena method nya sama, hanya ada pengecekan apakah arraydocumentlist atau document
				
				System.out.println("SPAN_DEBUG AMOUNT VALID");
				
				if(spanAmount!=Double.parseDouble(TotalAmount))
						throw new Exception("A006|Total amount tidak sama dengan  total line amount");
				
				System.out.println("SPAN_DEBUG AMOUNT 2 VALID");
				Status = "SUCCESS";
			}
		
			sdf = new SimpleDateFormat("yyyy-MM-dd");
			if (StringUtils.isEmpty(lateFileHour)){
				lateFileHour = "200000";
			}
			System.out.println("SPAN_DEBUG DATE LATE 2 " +lateFileHour);
			lateFileHr = Integer.parseInt(lateFileHour);
			System.out.println("SPAN_DEBUG DATE LATE 2 " +lateFileHour);
			
			SimpleDateFormat timeonly = new SimpleDateFormat("HHmmss");
			
			//TODO Kalau perbend minta creation date tidak dilihat, code if ini di comment
//			if(creationDTInt>lateFileHr && DocumentDate.equals(sdf.format(new Date()))){
			//TODO trus ini di uncomment
			if(Integer.parseInt(timeonly.format(new Date()))>lateFileHr && DocumentDate.equals(sdf.format(new Date()))){
				Status = "SUCCESS";
				ErrorMessage = "005|Late File";
			}
			System.out.println("SPAN_DEBUG SALDO VALID");
			
			int documentDateInt = Integer.parseInt(DocumentDate.replace("-",""));
		
			sdf = new SimpleDateFormat("yyyyMMdd");
			int currentDateInt = Integer.parseInt(sdf.format(new Date()));
			
			
			if(documentDateInt<currentDateInt){
				Status = "SUCCESS";
//				ErrorMessage = "006|Backdate data, Void by system";
				ErrorMessage = "A004|Backdate data, Void by system";
			}
			System.out.println("SPAN_DEBUG Backdate VALID");
		}catch(Exception ex){
			Status = "ERROR";
			ErrorMessage = ex.getMessage();
		}
		
		response.setStatus(Status);
		response.setErrorMessage(ErrorMessage);
		response.setSpanProviderName(spanProvName);
		response.setSpanProviderCode(spanProvCode);
		response.setSpanProviderAcct(spanProvAcct);
		response.setDocumentDate(DocumentDate);
		
		return response;
	}
	
	/**
	 * String response
	 * booelan
	 * @param beneficiaryAccount
	 * @return
	 */
	private static String checkBA(String beneficiaryName, String beneficiaryAccount) {
		return FastBJBConnector.CheckAccountBJB(beneficiaryName, beneficiaryAccount, getSystemParameterDaoBean());
	}

	/**SHARE UTIL**/
	 
	public static boolean validateNumeric(String inString){
		
		// Regular expression in Java to check if String is number or not
	    Pattern pattern = Pattern.compile(".*[^0-9].*");
	    //Pattern pattern = Pattern.compile(".*\\D.*");	
		
		return !pattern.matcher(inString).matches();
	}
	
	public static boolean validateLength(String inString, int length){
		boolean status = false;
		int stringLength = inString.length();
		if(stringLength > 0 && stringLength <= length){
			status = true;
		}		
		return status;
	}
	
	public static boolean validateAmount(String amount, String amountFormat){
		boolean status = false;
		try{
			//Double.parseDouble(Amount);
			status = amount.matches(amountFormat);
			//status = true;
		}catch(NumberFormatException nfe){
		}
		return status;
	}
}
