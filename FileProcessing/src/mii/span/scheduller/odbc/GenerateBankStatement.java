package mii.span.scheduller.odbc;

import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.security.PrivilegedExceptionAction;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedProperty;
import javax.xml.soap.SOAPMessage;

import mii.span.model.SpanRekeningKoran;
import mii.span.service.BJBRekeningSamaHandler;
import mii.span.util.BusinessUtil;
import mii.span.util.PubUtil;

import org.apache.commons.lang.StringUtils;
import org.apache.xml.custom.security.utils.XMLUtils;
import org.w3c.dom.Document;

import com.mii.bjb.core.GSSBJBConnector;
import com.mii.constant.Constants;
import com.mii.constant.DaoConstant;
import com.mii.dao.DataMT940DAO;
import com.mii.dao.SpanAccountBalanceLogDao;
import com.mii.dao.SpanHostResponseDao;
import com.mii.dao.SystemParameterDAO;
import com.mii.logger.Log;
import com.mii.models.DataDetailsMT940;
import com.mii.models.SystemParameter;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.ApplicationInstance;
import com.mii.util.ConstructXML;
import com.mii.util.CreateJarFile;
import com.mii.util.DigitalSignature;
import com.sun.org.apache.xml.internal.security.Init;

public class GenerateBankStatement implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private static Log log = Log.getInstance(GenerateBankStatement.class);
	
	@ManagedProperty(value = "#{applicationBean}")
	private static ApplicationBean applicationBean;
	
	public void run(){
		SystemParameter sysParamFlag = getSystemParameterDaoBean().getValueParameter(Constants.BS_HLP_RUN_FLAG).get(0);
		String BS_SCH_FLAG = sysParamFlag.getParam_value();
		SystemParameter sysParamStart = getSystemParameterDaoBean().getValueParameter(Constants.BS_HLP_RUN_START).get(0);
		String BS_SCH_RUN_START = sysParamStart.getParam_value();
		SystemParameter sysParamEnd = getSystemParameterDaoBean().getValueParameter(Constants.BS_HLP_RUN_END).get(0);
		String BS_SCH_RUN_END = sysParamEnd.getParam_value();
		SimpleDateFormat sdf = new SimpleDateFormat(Constants.BS_HLP_HOUR_FORMAT);
		String hour = sdf.format(new Date());
		
		if((Integer.parseInt(BS_SCH_RUN_START) <= Integer.parseInt(hour) && 
				Integer.parseInt(BS_SCH_RUN_END) >= Integer.parseInt(hour))&&
				BS_SCH_FLAG.equals("0")){
			getSystemParameterDaoBean().updateParameter(Constants.BS_HLP_RUN_FLAG, "1");
			System.out.println("Start Generate Bank Statement");
				
				for(int i=0;i<2;i++){
					if(i==0){
						go(Constants.GAJI);
					}
					else{
						go(Constants.GAJIR);
					}
				}
				
			System.out.println("End Generate Bank Statement");
		}else{
			if(Integer.parseInt(BS_SCH_RUN_START) <= Integer.parseInt(hour) && 
					Integer.parseInt(BS_SCH_RUN_END) >= Integer.parseInt(hour)){
				//DO NOTHING
				System.out.println("Not yet.");
			}else{
				if(BS_SCH_FLAG.equals("0")){
					//DO NOTHING
					System.out.println("Not yet.");
				}else{
					sysParamEnd.setParam_value("0");
					getSystemParameterDaoBean().updateParameter(Constants.BS_HLP_RUN_FLAG, "0");
					System.out.println("Ok here we go...");
				}
			}
		}
	}
	
	public static void go(String accountType){
		try {
			SystemParameterDAO dao = (SystemParameterDAO) BusinessUtil.getDao(DaoConstant.SystemParameterDAO);
			SpanAccountBalanceLogDao spanAccountBalanceLogDao = (SpanAccountBalanceLogDao) BusinessUtil.getDao(DaoConstant.SpanAccountBalanceLogDao);
			
			//get variabel digital signature
				String xmlPath = dao.getValueParameter(Constants.XML_BANK_STATEMENT_PATH).get(0).getParam_value();
				String keyFile = dao.getValueParameter(Constants.KEYSTORE_FILE_PATH_BS).get(0).getParam_value(); //	C:/bjbKey.jks
				String keyUser = dao.getValueParameter(Constants.PRIVATE_KEY_USER_BS).get(0).getParam_value(); // bank.jawa.barat123
				String jarPath = dao.getValueParameter(Constants.JAR_BANK_STATEMENT_PATH).get(0).getParam_value(); //grk.getParameter("JAR_BANK_STATEMENT_PATH");
				String bankCode = dao.getValueParameter(Constants.KODE_BANK_12_DIGIT).get(0).getParam_value(); //ini perlu ditanyakan ke user BJB, berapa kode bank BJB yg 12 digit
			//Add Decrypt 25 Jul 2016 -- Brian
				String keystorePass = dao.getValueParameter(Constants.KEYSTORE_PASS_BS).get(0).getParam_value();//bjb123
				String keyPass = dao.getValueParameter(Constants.PRIVATE_KEY_PASS_BS).get(0).getParam_value(); //bjb123
			//start and ending balance
				String bankAcctNo = getBankAcctNo(accountType, dao);
				String beginBalance = spanAccountBalanceLogDao.getLastBeginBalance(accountType, getDocDateBeginingBal());
				//String beginBalance = "100000.0"; //kalo mau di by pass
				String closeBalance = spanAccountBalanceLogDao.getLastBeginBalance(accountType, getDocDateYesterday());
//				String closeBalance = ToBeExecute.getSaldo(bankAcctNo);
//				String closeBalance = byPassCloseBalance(); //kalo mau di by pass
			
			//query untuk count cek record masih ada apa tidak yg belum tergenerate BS
			String lastDocDate = getDocDateYesterday(); //get yesterday doc date	pattern yyyy-MM-dd
			String successErrorCode = "00";
			String rowNum = getMaxPerPage(dao);
			
			List<SpanRekeningKoran> credit= filterCreditFromTh(dao, lastDocDate, bankAcctNo);
			List<SpanRekeningKoran> creditRekSama = BJBRekeningSamaHandler.getTransaksiRekeningSama(getSpanHostResponseDaoBean(), bankAcctNo);
			credit.addAll(creditRekSama);
			
//			List<SpanRekeningKoran> credit = TransactionHistory.byPassRekeningKoranCreditOnly();	//bypass
			
			String totalDebit = checkStatusDataDetailMT940(lastDocDate, "0", successErrorCode, bankAcctNo);
			double totalCredit = BusinessUtil.sizeListToDouble(credit);
			String totalPenihilan = getTotalPenihilan(lastDocDate);
			
			double totalRowAll = totalCredit + Double.valueOf(totalDebit) + Double.valueOf(totalPenihilan);
			int maxPerPage = Integer.valueOf(rowNum);
			int totalPage = (int)Math.ceil(totalRowAll/maxPerPage);
			
			//for line area sequence
			Map<String, Object> mapLineArea = new HashMap<String, Object>();
			mapLineArea.put(Constants.TOTAL_AMOUNT_CREDIT, 0l);
			mapLineArea.put(Constants.LINE_AREA_ROW, 0);
			
			//Create Empty BS
			if(totalPage==0){
				constructGenerateBankStatement(credit, totalPenihilan, maxPerPage, mapLineArea, 
						accountType, bankAcctNo, lastDocDate, totalDebit, bankCode, beginBalance, closeBalance, xmlPath, 
						jarPath, keyFile, keyUser, keystorePass, keyPass);
				Thread.sleep(1000); //preventing overwrite file BS
			}
			
			for(int i=0;i<totalPage;i++){
				constructGenerateBankStatement(credit, totalPenihilan, maxPerPage, mapLineArea, 
						accountType, bankAcctNo, lastDocDate, totalDebit, bankCode, beginBalance, closeBalance, xmlPath, 
						jarPath, keyFile, keyUser, keystorePass, keyPass);
				Thread.sleep(1000); //preventing overwrite file BS
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
	}

	private static String byPassCloseBalance() {
		System.out.println("***********CLOSE BALANCE DI BY PASS****************8");
		return "10000";
	}

	private static String getTotalPenihilan(String lastDocDate) {
		DataMT940DAO dao = (DataMT940DAO) BusinessUtil.getDao(DaoConstant.DataMT940DAO);
		return String.valueOf(dao.getTotalPenihilan(lastDocDate));
	}

	public static List<DataDetailsMT940> getPenihilan(String docDate, String max) {
		DataMT940DAO dao = (DataMT940DAO) BusinessUtil.getDao(DaoConstant.DataMT940DAO);
		List<DataDetailsMT940> penihilan = dao.getPenihilan(docDate, max);
		if(!BusinessUtil.isListEmpty(penihilan)){
			dao.updatePenihilan(docDate, max);
		}
		return penihilan;
	}

	/*private static List<DataDetailsMT940> bypassPenihilan() {
		System.out.println("***********************PENIHILAN DI BY PASSED***********************************");
		List<DataDetailsMT940> list = new ArrayList<DataDetailsMT940>();
		DataDetailsMT940 detailsMT940 = new DataDetailsMT940();
		detailsMT940.setBankReffNo("0");
		detailsMT940.setBankTrxCode("0");
		detailsMT940.setDebitCredit("P");
		detailsMT940.setOriginalAmount("20000");
		list.add(detailsMT940);
		return list;
	}*/

	private static List<SpanRekeningKoran> filterCreditFromTh(SystemParameterDAO systemParameterDao, String lastDocDate, String bankAcctNo) {
		/*Date startDateTh = PubUtil.stringToDate(systemParameterDao.getValueByParamName(Constants.START_DATE_TH_BS), 
				systemParameterDao.getValueByParamName(Constants.START_DATE_TH_BS_PATTERN));*/
		Date startDateTh = PubUtil.stringToDate(lastDocDate, "yyyy-MM-dd");
		Date endDateTh = PubUtil.stringToDate(lastDocDate, "yyyy-MM-dd");
//		return TransactionHistory.getTransactionHistoryCreditOnly(systemParameterDao, bankAcctNo, startDateTh, endDateTh);
		return GSSBJBConnector.getGSSCreditOnly(systemParameterDao, bankAcctNo, startDateTh, endDateTh);
	}

	private static String getMaxPerPage(SystemParameterDAO dao) {
		String rowNum = dao.getValueParameter(Constants.LIMIT_ROW_BANK_STATEMEMT).get(0).getParam_value();//default 10000	
		if(StringUtils.isEmpty(rowNum)){
			rowNum = "10000"; //default value
		}
		return rowNum;
	}

	private static String getBankAcctNo(String accountType, SystemParameterDAO dao) {
		String bankAcctNo;
		if(Constants.GAJI.equalsIgnoreCase(accountType)){
			bankAcctNo = dao.getValueParameter(Constants.PARAM_DEBITACCNO+Constants.GAJI).get(0).getParam_value();
		}
		else{
			bankAcctNo = dao.getValueParameter(Constants.PARAM_DEBITACCNO+Constants.GAJIR).get(0).getParam_value();
		}
		return bankAcctNo;
	}
	
	private static void constructGenerateBankStatement(List<SpanRekeningKoran> credit, String totalPenihilan, 
			int maxPerPage,  Map<String, Object> mapLineArea, String accountType, String bankAcctNo, String lastDocDate, String totalDebit,
			String bankCode, String beginBalance, String closeBalance, String xmlPath, String jarPath, String keyFile, String keyUser,
			String keystorePass, String keyPass) throws IOException{
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		
		//data-data yg diperlukan untuk header BS
		DateFormat dtf = new SimpleDateFormat("yyyyMMddHHmmss");
		DateFormat dtFormat = new SimpleDateFormat("yyyy-MM-dd");
		String mtFilename = "MTBJB"+dtf.format(date); //ini nanti di hardcode saja
//		String bankStatementDt = dtFormat.format(date); //ini tanggal BS di generate dengan format seperti spec BJB //FIXME ini diganti docDate transaksi
		String bankStatementDt = getDocDateYesterday();
		String currency = "IDR"; //defaultnya IDR
		
		System.out.println("Total record transaction = "+totalDebit);
		BusinessUtil.logWithSysout(log, PubUtil.concat("Generate BS from File ", "mtFilename", "start time =", dateFormat.format(date)));
		
		//call construct XML Bank Statement
		ConstructXML cx = new ConstructXML();
		BusinessUtil.logWithSysout(log, "Start time construct XML = "+dateFormat.format(new Date()));
		String status = cx.doConstruct(credit, Integer.valueOf(totalPenihilan), mtFilename, bankCode, bankAcctNo, 
				bankStatementDt, currency, beginBalance, closeBalance, xmlPath, mapLineArea, maxPerPage, lastDocDate, Integer.valueOf(totalDebit), accountType);
		String statusConstruct = status.substring(0, 7);
		String bsName = status.substring(8);
		
		//sign bs
		BusinessUtil.logWithSysout(log, PubUtil.concat("Finish time construct XML = ", dateFormat.format(new Date())));
		signDsAndCreateJar(xmlPath, jarPath, statusConstruct, bsName,keyFile, keyUser, keystorePass, keyPass);
		BusinessUtil.logWithSysout(log, PubUtil.concat("Generate BS from File ",mtFilename," finish time = "+dateFormat.format(new Date())));
		
	}
	
	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		String xmlPath = "D:/MII/BJB/SPAN/_doc/TestSIGN/jar/";
		String jarPath = "D:/MII/BJB/SPAN/_doc/TestSIGN/jar/";
		String statusConstruct = "SUCCESS";
		String bsName = "520008000990_BS_I_1040004062266_20170330_040100.xml";
		String keyFile = "D:/MII/BJB/SPAN/_doc/TestSIGN/bjbmpn.jks";
		String keyUser = "bjbmpn";
		String keystorePass = "security";
		String keyPass = "security";
		signDsAndCreateJar(xmlPath, jarPath, statusConstruct, bsName, keyFile, keyUser, keystorePass, keyPass);
	}

	private static void signDsAndCreateJar(String xmlPath, String jarPath,
			String statusConstruct, String bsName,
			String keyFile, String keyUser, String keystorePass, String keyPass) {
		String bsFile;
		String bsSignedFile;
		String statusSign;
		String statusJAR;
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		if("SUCCESS".equalsIgnoreCase(statusConstruct)){
			//Sign XML BS
			bsFile = xmlPath.concat(bsName);
			bsSignedFile = xmlPath.concat(bsName);
			
			BusinessUtil.logWithSysout(log, PubUtil.concat("Start time sign BS XML = ", dateFormat.format(new Date())));
			statusSign = signBSFile(bsFile, keyFile, keystorePass, keyUser, keyPass, bsSignedFile);
			BusinessUtil.logWithSysout(log, PubUtil.concat("Finish time sign BS XML = ",dateFormat.format(new Date())));
			
			if("OK".equalsIgnoreCase(statusSign)){
				//Archiving xml BS
				String fileInput = bsSignedFile;
				String fileTarget = jarPath.concat(bsName.replace("xml", "jar"));
				
				BusinessUtil.logWithSysout(log, PubUtil.concat("Start time archiving BS XML = ",dateFormat.format(new Date())));
				statusJAR = doArchiveFile(fileInput, fileTarget);
				BusinessUtil.logWithSysout(log, PubUtil.concat("Start time archiving BS XML = ",dateFormat.format(new Date())));
				BusinessUtil.logWithSysout(log, PubUtil.concat("Archiving file ",bsName+" is ",statusJAR));
			}else{
				BusinessUtil.logWithSysout(log, "Failed to sign BS");
			}
		}else{
			BusinessUtil.logWithSysout(log, "Failed to sign BS");
		}
	}

	/*private static String byPassCloseBalance(){
		System.out.println("******Close Balance is by passed, please comment me****************");
		return "1000.00";
	}*/
	
	private static String signBSFile(String filename, String keystore, String keystorePass, String keyUser, String keyPass,
			String signedFile){
		String status = "";
		try{
			DigitalSignature ds = new DigitalSignature();
			@SuppressWarnings("rawtypes")
			Class iClass = ds.getClass();
			
			//load file and wrap xml with SOAPEnvelope
			@SuppressWarnings("static-access")
			SOAPMessage msg  = ds.loadDocument(filename);
			
			//Sign soap using keystore  
	        Document signedDoc = ds.signSOAPMessage(msg, keystore, keystorePass, keyUser, keyPass, iClass);
	        
	      //persist document to file    
	     	ds.persistDocument(signedDoc, signedFile);
	     	
	     	status = "OK";
		}catch(Exception e){
			status = "FAILED";
			e.printStackTrace();
			log.error(e.getMessage());
		}
		return status;
	}
	
	/*public static void main(String[] args) {
		signBSFile("D:/MII/BJB/SPAN/_promote/test/ftp/bs/xml/520008000990_BS_I_0072932706001_20161018_122204.xml", 
				"D:/MII/BJB/SPAN/_promote/test/ssl/bjbmpn.jks", "security", "bjbmpn", "security", "D:/MII/BJB/SPAN/_promote/test/ftp/bs/xml/520008000990_BS_I_0072932706001_20161018_122204_SIGN.xml");
	}*/
	
	private static String doArchiveFile(String fileInput, String fileTarget){
		String statusJar = "";
		try{
			CreateJarFile.runCreateJar(fileInput, fileTarget);
			statusJar = "SUCCESS";
		}catch(Exception e){
			statusJar = "FAILED";
			log.error(e.getMessage());
		}
		return statusJar;
	}
	
	public static List<DataDetailsMT940> getDataDetailMT940List(String batchid, String status, String rowNum, String norek){
		DataMT940DAO dmt940Dao = (DataMT940DAO) BusinessUtil.getDao(DaoConstant.DataMT940DAO);
		dmt940Dao.getDataDetailMT940(batchid, status, rowNum, norek);
		List<DataDetailsMT940> detailList = new ArrayList<DataDetailsMT940>(dmt940Dao.getDataProcessed(rowNum, norek));
		if(!BusinessUtil.isListEmpty(detailList)){
			dmt940Dao.updateExecuteBs(norek);
		}
		return detailList;
	}
	
	private static String checkStatusDataDetailMT940(String docDate, String status, String errorCode, String norek){
		DataMT940DAO dmt = (DataMT940DAO) BusinessUtil.getDao(DaoConstant.DataMT940DAO);
		int totalRow = dmt.checkStatusDetailMT940(docDate, status, errorCode, norek);
		return String.valueOf(totalRow);
	}
	
	private static String getDocDateYesterday(){
		String dateParam = getSystemParameterDaoBean().getValueByParamName(Constants.DYNAMIC_BS_DATE);
		DateFormat dtf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		if(dateParam.equals("AUTO")){
			return processGetDocDateYesterday(dtf, cal);
		}else{
			try{
				cal.setTime(dtf.parse(dateParam)); //kalau bukan formatnya harusnya kelempar ke catch
				return processGetDocDateYesterday(dtf, cal);
			}catch(Exception e){
				return processGetDocDateYesterday(dtf, cal);
			}
		}
	}

	private static String processGetDocDateYesterday(DateFormat dtf, Calendar cal) {
		cal.add(Calendar.DATE, -1); //FIXME uncoment
		return dtf.format(cal.getTime());
	}
	
	private static String getDocDateBeginingBal(){
		String dateParam = getSystemParameterDaoBean().getValueByParamName(Constants.DYNAMIC_BS_DATE);
		DateFormat dtf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		if(dateParam.equals("AUTO")){
			return processGetDocDateBeginingBal(dtf, cal);
		}else{
			try{
				cal.setTime(dtf.parse(dateParam)); //kalau bukan formatnya harusnya kelempar ke catch
				return processGetDocDateBeginingBal(dtf, cal);
			}catch(Exception e){
				return processGetDocDateBeginingBal(dtf, cal);
			}
		}
	}

	private static String processGetDocDateBeginingBal(DateFormat dtf, Calendar cal) {
		cal.add(Calendar.DATE, -2);
		return dtf.format(cal.getTime());
	}
	
	private static SystemParameterDAO getSystemParameterDaoBean(){
		applicationBean = ApplicationInstance.getInstance().getApplicationBean();
		return (SystemParameterDAO) applicationBean.getFileProcessContext().getBean("sysParamDAO");
	}
	
	private static SpanHostResponseDao getSpanHostResponseDaoBean(){
		applicationBean = ApplicationInstance.getInstance().getApplicationBean();
		return (SpanHostResponseDao) applicationBean.getFileProcessContext().getBean("spanHostResponseDao");
	}
	
}
