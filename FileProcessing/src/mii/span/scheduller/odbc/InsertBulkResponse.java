package mii.span.scheduller.odbc;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.faces.bean.ManagedProperty;

import mii.span.Log;
import mii.span.db.UpdateSPAN;
import mii.span.model.helper.CheckDataFailedOutput;
import mii.span.model.helper.SPANDataResponse;
import mii.span.model.helper.SelectSPANDataValidationByStatusOutput;
import mii.span.model.helper.SpanHostResponseOutput;

import com.mii.dao.InsertBulkResponseDao;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.ApplicationInstance;

public class InsertBulkResponse implements Serializable{
	private static final long serialVersionUID = 1L;
	@ManagedProperty(value = "#{applicationBean}")
	private static ApplicationBean applicationBean;
	
	private InsertBulkResponseDao getInsertBulkResponseDaoBean(){
		applicationBean = ApplicationInstance.getInstance().getApplicationBean();
		return (InsertBulkResponseDao) applicationBean.getFileProcessContext().getBean("InsertBulkResponseDao");
	}
	
	public void run(){
		System.out.println("Start Run Insert Bulk Response");
		/**
		 * Querying (selectSPANDataValidationStatusByTrxType) with input
		 * [TRXTYPE2 = 1, TRXTYPE1 = 0, PROC_STATUS = 3] mapping the output to
		 * variable selectSPANDataValidationByStatusOutput
		 */
		List<SelectSPANDataValidationByStatusOutput> selectSPANDataValidationByStatusOutput = new ArrayList<SelectSPANDataValidationByStatusOutput>();
		try{
			selectSPANDataValidationByStatusOutput = getInsertBulkResponseDaoBean().selectSPANDataValidationStatusByTrxType("3");
			System.out.println(selectSPANDataValidationByStatusOutput.size());
			for (SelectSPANDataValidationByStatusOutput out : selectSPANDataValidationByStatusOutput) {
				System.out.println(out.getFILE_NAME());
			}
		}catch(Exception e){
			System.out.println("Error");
			e.printStackTrace();
		}
		
		// map
		String toDay = new SimpleDateFormat("yyyy-MM-dd").format(
				Calendar.getInstance().getTime()).toString();
		String legStatus = "";
		String channelID = "20";
		String retryLeg = "0";
		String nextDay = "";
		
		// loop
		List<SPANDataResponse> SPANDataResponse = new ArrayList<>();
		
		for (SelectSPANDataValidationByStatusOutput s : selectSPANDataValidationByStatusOutput) {
			System.out.println("ASYNCHRONOUS : "+s.getASYNCHRONOUS());
			System.out.println("LEG STATUS : "+s.getLEGSTATUS());
			System.out.println("TRX Type : "+s.getTRXTYPE());
			// map
			s.setCREATE_DATE(s.getCREATE_DATE().substring(0, 10));
			String updateDateSubstring = s.getUPDATE_DATE().substring(0, 10);
			// L3
			if (s.getASYNCHRONOUS().equals(null)) {
				s.setLEGSTATUS("L1");
			} else if (s.getASYNCHRONOUS().equals("0")) {
				s.setLEGSTATUS("L1");
			} else {
				if (s.getLEGSTATUS().equals(null)) {
					s.setLEGSTATUS("L1");
					retryLeg = "1";
				} else if (s.getLEGSTATUS().equals("0")) {
					if (s.getTRXTYPE().equals("0")) {
						if (s.getL1_STATUS().equals("L1")) {
							s.setLEGSTATUS("L1");
							retryLeg = "0";
						} else if (s.getL1_STATUS().equals("L2")) {
							s.setLEGSTATUS("L2");
							retryLeg = "0";
						} else {
							s.setLEGSTATUS("L1");
							retryLeg = "1";
						}
					} else if (s.getTRXTYPE().equals("1")) {
						s.setLEGSTATUS("SC");
						retryLeg = "0";
					}
				} else if (s.getLEGSTATUS().equals("1")) {
					s.setLEGSTATUS("L2");
					retryLeg = "0";
				} else if (s.getLEGSTATUS().equals("2")) {
					s.setLEGSTATUS("L1");
					retryLeg = "0";
				} else {
					// do nothing
				}
			}
			
			// branch on trxtype
			if (s.getTRXTYPE().equals("1")) {
				if (s.getSC_SETTLEMENT().equals("1")) {
					s.setLEGSTATUS("L2");
					retryLeg = "0";
					System.out.println("Masuk 1");
				} else {
					if (s.getDOCUMENT_DATE().equals(toDay)) {
						if (updateDateSubstring.equals(s.getDOCUMENT_DATE())) {
							/**
							 * querying (checkDataFailed) with input
							 * filename mapping the output to variable
							 * checkDataFailedOutput
							 */
							System.out.println("checkDataFailedOutput");
							List<CheckDataFailedOutput> checkDataFailedOutput = getInsertBulkResponseDaoBean().checkDataFailed(s.getFILE_NAME());
							System.out.println(checkDataFailedOutput.size());
							for(CheckDataFailedOutput out : checkDataFailedOutput){
								System.out.println(out.getBATCHID());
							}
							
							// map
							int sizeFailed = checkDataFailedOutput.size();
	
							// branch
							if (sizeFailed > 0
									&& checkDataFailedOutput.get(0)
											.getLEGSTATUS().equals("SC")) {
								s.setLEGSTATUS("SC");
								retryLeg = "0";
								System.out.println("Masuk 2");
							} else {
								s.setLEGSTATUS("L1");
								retryLeg = "0";
								System.out.println("Masuk 3");
							}
						}
					} else {
						s.setLEGSTATUS("SC");
						retryLeg = "0";
						System.out.println("Masuk 4");
					}
				}
			} else {
				// do nothing
			}
			
			/**
			 * Querying (getTotalRecSendToHost) with input batchID,
			 * trxHeaderID mapping the output to variable totalReqSendToHost
			 */
			String totalReqSendToHost = Integer.toString(getInsertBulkResponseDaoBean().getTotalRecSendToHost(s.getBATCHID(), s.getTRXHEADERID())); 
			System.out.println("totalReqSendToHost : "+totalReqSendToHost);
			// branch on
			if (totalReqSendToHost.equals("0")) {
				// do nothing
			} else {
				s.setTOTAL_RECORD(totalReqSendToHost);
			}
			
			// STAT
			System.out.println("retryLeg : "+retryLeg);
			SpanHostResponseOutput spanHostResponseOutput = new SpanHostResponseOutput();
			for (int i = 0; i <= Integer.parseInt(retryLeg); i++) {
				Log.debugLog("SPAN", "insertBulkResponse", "INFO",
						"1. For file name " + s.getFILE_NAME()
								+ " with LegStatus : " + s.getLEGSTATUS()
								+ ", BATCHID : " + s.getBATCHID()
								+ ", TRXHEADERID : " + s.getTRXHEADERID()
								+ " and TOTALRECORD : " + s.getTOTAL_RECORD());
				/*
				 * Querying (spanHostResponse) with input [filename,
				 * batchID, trxHeaderID, totalRecord, legStatus] mapping the
				 * output to variable spanHostResponseOutput
				 */
				spanHostResponseOutput = getInsertBulkResponseDaoBean().spanHostResponse(s.getFILE_NAME(), 
						s.getBATCHID(), s.getTRXHEADERID(), s.getTOTAL_RECORD(), s.getLEGSTATUS());
				System.out.println(spanHostResponseOutput.getErrorMessage());
				System.out.println(spanHostResponseOutput.getStatus());
				
				
				// branch
				if (spanHostResponseOutput.getStat().equals("3")) {
					break;
				} else {
					s.setLEGSTATUS("L2");
				}
				Log.debugLog(
						"SPAN",
						"insertBulkResponse",
						"INFO",
						"2. For file name " + s.getFILE_NAME() + "Status : "
								+ spanHostResponseOutput.getStatus() + " and "
								+ "ErrorMessage : "
								+ spanHostResponseOutput.getErrorMessage());

			}

			// branch on
			if (spanHostResponseOutput.equals("SUCCESS")) {
				SPANDataResponse spanDataResponse = new SPANDataResponse();
				spanDataResponse.setFILE_NAME(s.getFILE_NAME());
				spanDataResponse.setBATCHID(s.getBATCHID());
				spanDataResponse.setTRXHEADERID(s.getTRXHEADERID());
				spanDataResponse.setTOTAL_RECORD(s.getTOTAL_RECORD());
				spanDataResponse.setSC_FILE_NAME(s.getSC_FILE_NAME());
				spanDataResponse.setSC_TRXDETAILID_START(s
						.getSC_TRXDETAILID_START());
				spanDataResponse.setSC_TRXDETAILID_END(s
						.getSC_TRXDETAILID_END());
				spanDataResponse.setTOTAL_AMOUNT(s.getTOTAL_AMOUNT());
				spanDataResponse.setStatus(spanHostResponseOutput.getStatus());
				SPANDataResponse.add(spanDataResponse);
			} else {
				// do nothing
			}
		}
		// loop
		for (SPANDataResponse sdr : SPANDataResponse) {
			Log.debugLog("SPAN", "insertBulkResponse", "INFO",
					"Status Response from " + "HOST/ODBC : " + sdr.getStatus()
							+ ", FILE NAME : " + sdr.getFILE_NAME());

			// branch
			if (sdr.getStatus().equals("SUCCESS")) {
				
				//TODO querying (insertCMEHeaderLog) with input filename
				 

				// spanReport
//				ISService.spanReport(sdr.getFILE_NAME());

				// updateSPAN
				String sql = "update span_data_validation_sp2dno set void_flag='2' "
						+ "where file_name='" + sdr.getFILE_NAME() + "' "
						+ "and sp2dno not in (select substr(document_no, 0, 15) "
						+ "from span_void_data_trx where file_name='"
						+ sdr.getFILE_NAME() + "')";
				getAdapterBean().updateSPAN(getAdapterBean().getQuery(sql));
			} else {
				// do nothing
			}
		}
		
		System.out.println("End Run Insert Bulk Response");
	}

	private static UpdateSPAN getAdapterBean(){
		applicationBean = ApplicationInstance.getInstance().getApplicationBean();
		return (UpdateSPAN) applicationBean.getFileProcessContext().getBean("spanAdapter");
	}
}
