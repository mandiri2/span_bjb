package mii.span.scheduller.odbc;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedProperty;

import mii.span.atomic.response.odbc.Ack;
import mii.span.atomic.response.odbc.ack.Accepted;
import mii.span.constant.MdrCfgMainConstant;
import mii.span.db.Service;
import mii.span.doc.DataFileNACK;
import mii.span.doc.SP2DDocument;
import mii.span.model.GetNACKUpdateInput;
import mii.span.model.GetNACKUpdateOutput;
import mii.span.model.SpanAccountBalanceLog;
import mii.span.model.SpanDataValidation;
import mii.span.model.helper.ACKStringToBytesOutput;
import mii.span.model.helper.Files;
import mii.span.process.model.SentAckDataFileRequest;
import mii.span.process.model.SentAckDataFileResponse;
import mii.span.process.model.SpanAckAcceptedRequest;
import mii.span.process.model.SpanAckAcceptedResponse;
import mii.span.process.model.UpdateSpanResponse;
import mii.span.sftp.Services;
import mii.span.util.BusinessUtil;
import mii.span.util.GetCurrentDateTime;
import mii.span.util.PubUtil;

import org.hibernate.Query;

import com.mii.constant.Constants;
import com.mii.constant.DaoConstant;
import com.mii.dao.SpanDataValidationDao;
import com.mii.dao.SystemParameterDAO;
import com.mii.dao.ToBeExecuteDao;
import com.mii.helpers.InterfaceDAO;
import com.mii.json.AccountBalance;
import com.mii.models.SystemParameter;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.ApplicationInstance;

public class GetNACKUpdate implements Serializable{
	private static final long serialVersionUID = 1L;
	@ManagedProperty(value = "#{applicationBean}")
	private static ApplicationBean applicationBean;
	
	private static SpanDataValidationDao getSpanDataValidationDaoBean(){
		applicationBean = ApplicationInstance.getInstance().getApplicationBean();
		return (SpanDataValidationDao) applicationBean.getFileProcessContext().getBean("spanDataValidationDao");
	}
	
	public void run() {
		System.out.println("Start Run GetNACKUpdate");
		
		//TODO Ini di uncomment untuk log balance nya ketika cut off time
		goLogAB();
		
		for(int i=0;i<2;i++){
			GetNACKUpdateInput input = new GetNACKUpdateInput();
			List<Files> files = new ArrayList<Files>();
			Files file = new Files();
			if(i==0){
				input.setProviderCode(Constants.GAJI);	
			}
			else{
				input.setProviderCode(Constants.GAJIR);
			}
			
			List<SpanDataValidation> resultList = new ArrayList<SpanDataValidation>();
			resultList = getSpanDataValidationDaoBean().getNACKUpdateList(input.getProviderCode());
			for(SpanDataValidation s : resultList){
				file.setFileName(s.getFileName());
				files.add(file);
			}
			input.setFiles(files);
			
			GetNACKUpdateOutput output = new GetNACKUpdateOutput();
			output = getNACKUpdate(input);
			System.out.println("getNACKUpdate : "+output.getStatus()+" - "+output.getErrorMessage());
		}
		System.out.println("End Run GetNACKUpdate");
	}

	public static GetNACKUpdateOutput getNACKUpdate(GetNACKUpdateInput input) {
		GetNACKUpdateOutput output = new GetNACKUpdateOutput();
		InterfaceDAO dao = (SpanDataValidationDao) BusinessUtil.getDao(DaoConstant.SpanDataValidationDao);
		try {
			// map
			String documentDate = GetCurrentDateTime.getCurrentDateString("yyyy-MM-dd");
			
			// loop file
			for (Files f : input.getFiles()) {
				// sequence update span data validation
				try {
					String hql = "update SpanDataValidation "
							+ "set PROC_STATUS='1', RESPONSE_CODE='005', FORCED_ACK='2', UPDATE_DATE=current_timestamp "
							+ "where DOCUMENT_DATE='"+documentDate+"' and PROC_STATUS='1' and FILE_NAME = '"+f.getFileName()+"'";
					Query query = dao.getQuery(hql);
					
					UpdateSpanResponse updateSpanResponse = Service.updateSPAN(query);
					output.setStatus(updateSpanResponse.getStatus());
					output.setErrorMessage(updateSpanResponse.getErrorMessage());
				} catch (Exception e) {
					output.setStatus("ERROR");
					output.setErrorMessage(e.getMessage());
				}
				
				// branch on status
				try {
					if (output.getStatus().equals("SUCCESS")) {
						// snegativeACK
						negativeACK(input.getProviderCode(), f.getFileName());
					}
				} catch (Exception e) {
					System.out.println("[SPAN negativeACK] Error Message " + e.getMessage());
					e.printStackTrace();
				}
				
				// sleep
				Thread.sleep(1000);
			}
			
		} catch (Exception e) {
			System.out.println("error NACK " + e.getMessage());
			e.printStackTrace();
		}
		
		return output;
	}
	
	public static void negativeACK(String providerCode, String filename) throws Exception {
		// mandiri.span.db.service.odbc:selectSPANDataValidationStatusParamsNACK
		DataFileNACK dataFileNACK = selectSPANDataValidationStatusParamsNACK(providerCode, filename);
		
		// mandiri.span.util.atomic:ACKStringToBytes
		ACKStringToBytesOutput byteOutput = new ACKStringToBytesOutput();
		byteOutput = ACKStringToBytes(dataFileNACK.getXmlfilename());
//		byte[] byteNACK = byteOutput.getByteNACK();
		String Status = byteOutput.getStatus();
		String ErrorMessage = byteOutput.getErrorMessage();
		
		// mandiri.span.scheduller.odbc.ValidateDataDetails:getSpanDocument
//		Object[] objects2 = ValidateDataDetails.getSpanDocument(byteNACK);
//		SP2DDocument spanDocument = (SP2DDocument) objects2[0];
//		String docType = (String) objects2[1];
		SP2DDocument spanDocument = (SP2DDocument) PubUtil.xmlValuesToDocument(SP2DDocument.class, dataFileNACK.getXmlfilename());
		
		// map
		String contentName = dataFileNACK.getFilename().replaceAll("jar", "xml");
		String ErrorCode = "005";
		// mandiri.span.atomic.response.odbc.ack.accepted:spanACKAccepted
		SpanAckAcceptedRequest spanAckAcceptedRequest = new SpanAckAcceptedRequest();
		spanAckAcceptedRequest.setErrorCode(ErrorCode);
		spanAckAcceptedRequest.setErrorMessage("Saldo Tidak Cukup");
		spanAckAcceptedRequest.setSpanDocument(spanDocument);
		spanAckAcceptedRequest.setFileName(dataFileNACK.getFilename());
		SpanAckAcceptedResponse spanAckAcceptedResponse = new SpanAckAcceptedResponse();
		
		spanAckAcceptedResponse = Accepted.spanACKAccepted(spanAckAcceptedRequest);
		
		String ErrMsg = spanAckAcceptedResponse.getErrorMessage();
		Status = spanAckAcceptedResponse.getStatus();
		ErrorMessage = spanAckAcceptedResponse.getErrorMessage();
		String ACKDataString = spanAckAcceptedResponse.getAckDataString();
		String DocumentType = spanAckAcceptedResponse.getDocumentType();
		
		// mandiri.span.atomic.response.odbc.ack:sentACKDataFile
		SentAckDataFileRequest request = new SentAckDataFileRequest();
		request.setFilename(dataFileNACK.getFilename());
		request.setDocumentType(DocumentType);
		request.setErrorMessage(ErrorMessage);
		request.setErrorCode(ErrorCode);
		request.setAckDataString(ACKDataString);
		SentAckDataFileResponse sentAckDataFileResponse = Ack.sentACKDataFile(request);
		
		Status = sentAckDataFileResponse.getStatus();
		ErrorMessage = sentAckDataFileResponse.getErrorMessage();
		String ACKFilename = sentAckDataFileResponse.getAckFileName();
		String statussend = sentAckDataFileResponse.getStatusSend();
		
		// branch on status
		if (Status.equals("SUCCESS")) {
			// init config
			SystemParameterDAO sysParamDao = (SystemParameterDAO) BusinessUtil.getDao("sysParamDAO");
			SystemParameter systemParameterspanHost =  sysParamDao.getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_IPADDRESS).get(0);
			SystemParameter systemParameterspanUser =  sysParamDao.getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_USERNAME).get(0);
			SystemParameter systemParameterspanPass =  sysParamDao.getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_PASSWORD).get(0);
			SystemParameter systemParameterspanACKDir =  sysParamDao.getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_PUT_REMOTE_DIR).get(0);
			SystemParameter systemParameterLOCAL_DIR =  sysParamDao.getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_PUT_LOCAL_DIR).get(0);
			String LOCAL_DIR = systemParameterLOCAL_DIR.getParam_value();
			String spanACKDir = systemParameterspanACKDir.getParam_value();
			String ip = systemParameterspanHost.getParam_value();
			String username = systemParameterspanUser.getParam_value();
			String password = systemParameterspanPass.getParam_value();
			// map
			String ACKDesc = "Negative ACK data file has been SUCCESS send to SPAN";
			
			// updateSPAN
			String hql = "update SpanDataValidation "
					+ "set ACK_FILE_NAME='"+ACKFilename+"', ACK_DESCRIPTION='"+ACKDesc+"' "
					+ "where RESPONSE_CODE='005' and FORCED_ACK='2' and FILE_NAME = '"+filename+"'";
			InterfaceDAO dao = (SpanDataValidationDao) BusinessUtil.getDao("spanDataValidationDao");
			Query query = dao.getQuery(hql);
			UpdateSpanResponse rspUpdate= Service.updateSPAN(query);
			Status = rspUpdate.getStatus();
			ErrorMessage = rspUpdate.getErrorMessage();
			
			// putFTPFile
			statussend = Services.uploadFile(spanACKDir, LOCAL_DIR, ACKFilename, ip, username, password);
		}
		else {
			// do nothing
		}
		 
	}
	
	public static DataFileNACK selectSPANDataValidationStatusParamsNACK(String providerCode, String filename) {
		DataFileNACK dataFileNACK = new DataFileNACK();
		
		// map
		String procStatus = "1";
		String responseCode = "005";
		String documentDate = GetCurrentDateTime.getCurrentDateString("yyyy-MM-dd");
		
		// mandiri.span.db.adapter.odbc:selectSPANDataValidationNACK
		dataFileNACK = Service.selectSPANDataValidationNACK(procStatus, documentDate, responseCode, providerCode, filename);
		
		return dataFileNACK;
	}
	
	public static ACKStringToBytesOutput ACKStringToBytes(String input) {
		ACKStringToBytesOutput output = new ACKStringToBytesOutput();
		byte[] bytes = null;
		String Status = "";
		String ErrorMessage = "";
		try{
			bytes = input.getBytes();
			Status = "SUCCESS";
		}catch(Exception e){
			e.printStackTrace();
			Status = "ERROR";
			ErrorMessage = e.getMessage();
		}
		
		output.setByteNACK(bytes);
		output.setStatus(Status);
		output.setErrorMessage(ErrorMessage);
		
		return output;
	}
	
	//Persiapan Pindah Untuk log Begining Balance,, Karena Gapura mati tengah malam, di log ketika jam cut off
	private void goLogAB(){
		for(int i=0;i<2;i++){
			if(i==0){
				try {
					accountBalanceLog(Constants.GAJI);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			else{
				try {
					accountBalanceLog(Constants.GAJIR);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	private void accountBalanceLog(String accountType) throws Exception {
		
		SystemParameter systemParameter =  getSystemParameterDaoBean().getValueParameter(Constants.PARAM_DEBITACCNO+accountType).get(0);
		String availableBalance2 = getSaldo(systemParameter.getParam_value());
		try{
			if (availableBalance2.equalsIgnoreCase("ERROR")) {
				throw new Exception("Error While Inquiry Saldo.");
			} else {
				insertToAccountBalLogs(accountType, availableBalance2);
			}
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception("Exit To Be Execute");
		}
	}
	
	private static void insertToAccountBalLogs(String accountType, String balance) {
		SpanAccountBalanceLog spanAccountBalanceLog = new SpanAccountBalanceLog();

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		spanAccountBalanceLog.setInquiryDate(sdf.format(new Date()));
		spanAccountBalanceLog.setBeginBalance(balance);
		spanAccountBalanceLog.setAccountType(accountType);
		
		boolean checked = false;
		int count = getToBeExecuteDaoBean().countFirstAvailableBalance(accountType);
		if(count>0){
			checked = true;
		}
		if(!checked){
			try{
				getToBeExecuteDaoBean().save(spanAccountBalanceLog);
			}catch(Exception e){
				System.out.println("Available Balance Log Allready Exist.");
			}
		}else{
			System.out.println("Available Balance Log Allready Exist.");
		}
	}
	
	public static String getSaldo(String accountNo){
		String balance = AccountBalance.getAccountBalance(getSystemParameterDaoBean(), accountNo);
		return balance;
	}
	
	private static ToBeExecuteDao getToBeExecuteDaoBean(){
		applicationBean = ApplicationInstance.getInstance().getApplicationBean();
		return (ToBeExecuteDao) applicationBean.getFileProcessContext().getBean("toBeExecuteDao");
	}
	
	private static SystemParameterDAO getSystemParameterDaoBean(){
		applicationBean = ApplicationInstance.getInstance().getApplicationBean();
		return (SystemParameterDAO) applicationBean.getFileProcessContext().getBean("sysParamDAO");
	}
}
