package mii.span.scheduller.odbc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedProperty;

import mii.span.iso.util.AccountEqualsException;
import mii.span.iso.util.ISOHelper;
import mii.span.iso.util.PostingService;
import mii.span.iso.util.Reinquiry;
import mii.span.iso.util.SAMPackager;
import mii.span.model.BulkFTReqPRC;
import mii.span.model.FASTGapuraAccount;
import mii.span.util.BusinessUtil;

import org.apache.log4j.Logger;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;

import com.mii.bjb.core.FastBJBConnector;
import com.mii.constant.Constants;
import com.mii.constant.DaoConstant;
import com.mii.dao.BulkFTDao;
import com.mii.dao.SpanVoidDataTrxDao;
import com.mii.dao.SystemParameterDAO;
import com.mii.scoped.ApplicationBean;
import com.mii.span.handler.MonitoringVoidDepkeuHandler;
import com.mii.span.model.SpanSp2dVoidLog;
import com.mii.span.process.VoidDocumentNo;

public class PostingScheduller {
	
	public static Map<String, BulkFTReqPRC> sentMessage = new HashMap<>(0);
	
	Logger logger = Logger.getLogger(PostingScheduller.class);
	SAMPackager samPackager = new SAMPackager();
	
	@ManagedProperty(value = "#{applicationBean}")
	private static ApplicationBean applicationBean;
	
	public void run() {
		BulkFTDao bulkFTDao = (BulkFTDao) BusinessUtil.getDao("bulkFTDao");
		SystemParameterDAO sysParam = (SystemParameterDAO) BusinessUtil.getDao(DaoConstant.SystemParameterDAO);
		// initiate
		List<BulkFTReqPRC> prcRetry = new ArrayList<>();
		List<BulkFTReqPRC> prcPosting = new ArrayList<>();
		List<BulkFTReqPRC> prcPenihilan = new ArrayList<>();
		String limit = sysParam.getValueParameter(Constants.ISO_PRC_LIMIT).get(0).getParam_value();
		String timeout = sysParam.getValueParameter(Constants.ISO_TIMEOUT_LIMIT).get(0).getParam_value();
		
		// handling hanging transaction after posting (because this is scheduler)
		int hangedTransaction = bulkFTDao.getHangedTransaction(Double.parseDouble(timeout));
		executeHangedTransaction(hangedTransaction);
		
		// query penihilan, prc retry and prc posting
		prcPenihilan = bulkFTDao.getAllUnprocessPenihilan(limit);
		prcRetry = bulkFTDao.getAllUnprocessRetryOrPosting(limit, "retry");
		prcPosting = bulkFTDao.getAllUnprocessRetryOrPosting(limit, "posting");
		
		// PREPROCESSING list penihilan
		prcPosting.addAll(prcPenihilan);
		
		// PREPROCESSING list Retry | update prc_status where trxdetailid in trxDetailIDList
		List<BulkFTReqPRC> prcTemp = new ArrayList<>();
		List<String> trxDetailIDList = new ArrayList<>();
		prcTemp.addAll(prcRetry);		
		preProcessingRetryData(prcTemp, prcRetry, prcPosting, trxDetailIDList);
		
		// EXECUTE list Retry
		executeRetryTransaction(prcRetry);
		
		// PREPROCESSING list Posting, Retur, and Penihilan
		// update prc_status where trxdetailid in trxDetailIDList
		trxDetailIDList = new ArrayList<>();
		preProcessingPostingData(prcPosting, trxDetailIDList);
		
		// EXECUTE list Posting, Retur, and Penihilan
		executePostingTransaction(prcPosting);
	}
	
	private void executeHangedTransaction(int hangedTransaction) {
		if (hangedTransaction > 0) {
			// open thread to send yet responded request
			new Thread(new Reinquiry(), "reinquiry_transaction").start();
		}
	}

	private void preProcessingRetryData(List<BulkFTReqPRC> prcTemp,
			List<BulkFTReqPRC> prcRetry, List<BulkFTReqPRC> prcPosting,
			List<String> trxDetailIDList) {
		BulkFTDao bulkFTDao = (BulkFTDao) BusinessUtil.getDao("bulkFTDao");
		for (BulkFTReqPRC p : prcTemp){
			// may contains retur data, check it !
			if (bulkFTDao.getCountRetryOrRetur(p) > 1){
				// retry ~ let it in retry list // do nothing
			}
			else {
				// retur ~ dont let it in retry list
				// move p to prcPosting for retur process
				prcPosting.add(p);
				prcRetry.remove(p);
			}
			trxDetailIDList.add(p.getTrxdetailid());
		}
		if (trxDetailIDList.size() > 0)
			bulkFTDao.updatePrcStatusBatch(trxDetailIDList);
	}
	
	private void preProcessingPostingData(List<BulkFTReqPRC> prcPosting, List<String> trxDetailIDList) {
		BulkFTDao bulkFTDao = (BulkFTDao) BusinessUtil.getDao("bulkFTDao");
		for (BulkFTReqPRC p : prcPosting){
			trxDetailIDList.add(p.getTrxdetailid());
		}
		if (trxDetailIDList.size() > 0)
			bulkFTDao.updatePrcStatusBatch(trxDetailIDList);
	}
	
	private void executeRetryTransaction(List<BulkFTReqPRC> prcRetry) {
		BulkFTDao bulkFTDao = (BulkFTDao) BusinessUtil.getDao("bulkFTDao");
		// generate iso message from retry list
		for (BulkFTReqPRC p : prcRetry) {
			ISOHelper helper = new ISOHelper();
			ISOMsg msg = new ISOMsg();
			msg.setPackager(samPackager);
			try {
				// generate message for retry
				msg = helper.generateRetryTransactionMessage(p, msg);
				msg.dump(System.out, "[SCHEDULER][RETRY] ");
				byte[] msgToBeSend = helper.generateISOMessageByte(msg);

				// validate name and account number to BJB Fast, if found
				// then send request to if400
				validateAccountAndSend(p, msg, msgToBeSend);
				
			/*} catch (Exception e) {
				// when failed generate request message, force insert to prc
 				try {
 					logger.info("force insert when exception ...");
 					forceInsertRSP(p, msg, bulkFTDao);
				} catch (ISOException e1) {
					e1.printStackTrace();
				}
				e.printStackTrace();
			}*/
			} catch (ISOException e) {
				// when failed generate request message, force insert to prc
				try {
					logger.info("force insert when iso exception ...");
					forceInsertRSP(p, msg, bulkFTDao);
				} catch (ISOException e1) { 
					e1.printStackTrace(); 
				}
				e.printStackTrace();
			}catch (Exception e){
				logger.info("force insert when other exception ...");
				try {
					forceInsertRSPGapuraException(p, msg, bulkFTDao);
				} catch (ISOException e1) {
					e1.printStackTrace();
				}
				e.printStackTrace();
			}
		}
	}
	
	private void executePostingTransaction(List<BulkFTReqPRC> prcPosting) {
		BulkFTDao bulkFTDao = (BulkFTDao) BusinessUtil.getDao("bulkFTDao");
		// generate iso message from posting list
		for (BulkFTReqPRC p : prcPosting) {
			ISOHelper helper = new ISOHelper();
			ISOMsg msg = new ISOMsg();
			msg.setPackager(samPackager);
			try {
				// checking account no before posting
				if (p.getDebitaccno().equals(p.getCreditaccno()))
					throw new AccountEqualsException();
				// generate message
				if (p.getBatchid().startsWith("NHL")) {
					msg = helper.generatePostingTransactionMessage(p, msg);
					// override procode
					msg.set(3, "470000");
				}
				else {
					msg = helper.generatePostingTransactionMessage(p, msg);
				}
				
				msg.dump(System.out, "[SCHEDULER] ");
				byte[] msgToBeSend = helper.generateISOMessageByte(msg);

				// validate name and account number to BJB Fast, if found
				// then send request to if400
				validateAccountAndSend(p, msg, msgToBeSend);
			} 
			catch (AccountEqualsException aee) {
				try {
					logger.info("force insert success when account equals exception ...");
					forceInsertSuccessRSP(p, msg, bulkFTDao);
				} catch (ISOException e1) { e1.printStackTrace(); }
			}
			catch (ISOException e) {
				// when failed generate request message, force insert to prc
				try {
					logger.info("force insert when iso exception ...");
					forceInsertRSP(p, msg, bulkFTDao);
				} catch (ISOException e1) { e1.printStackTrace(); }
			}catch (Exception e){
				logger.info("force insert when other exception ...");
				try {
					forceInsertRSPGapuraException(p, msg, bulkFTDao);
				} catch (ISOException e1) {
					e1.printStackTrace();
				}
			}
		}
	}
	
	public void validateAccountAndSend(BulkFTReqPRC p, ISOMsg msg,
			byte[] msgToBeSend)
			throws ISOException {
		System.out.println("Validate Account and Send Trigered");
		BulkFTDao bulkFTDao = (BulkFTDao) BusinessUtil.getDao("bulkFTDao");
		SystemParameterDAO sysParam = (SystemParameterDAO) BusinessUtil.getDao(DaoConstant.SystemParameterDAO);
		FASTGapuraAccount bjbAccount = new FASTGapuraAccount();
		//* for penihilan - (penihilan doesnt has payment method) (if null set default 0)
		if (p.getPaymentMethod() == null)
			p.setPaymentMethod("0");
		// overbooking checking to FAST else direct hit to IF400
		if (p.getPaymentMethod().equals("0")) {
			bjbAccount = FastBJBConnector.CheckAccountFASTBJB(p.getCreditaccname(), p.getCreditaccno(), sysParam);
			if (bjbAccount.getIsValid().equals(Constants.FAST_FOUND)) {

				// update account validation status ==> 1 (valid), and also branch
				bulkFTDao.updateAccountValidationStatus(p, "1", bjbAccount.getBranch());
				logger.info("Update Success Validate Account Status and Branch.");
				Boolean result = PostingService.sendRequestAndUpdateIfFail(msgToBeSend, p);
				if(result)PostingService.putRequestAndLogging(msg, p);
			} else {
				// update account validation status ==> 2 (not valid), and also branch
				try{
					bulkFTDao.updateAccountValidationStatus(p, "2", bjbAccount.getBranch());
				}catch(Exception e){
					bulkFTDao.updateAccountValidationStatus(p, "2", "");
				}
				logger.info("force insert when account no and name not in FAST...");
				// insert to RESP
				msg = createISOMsgBasedOnPaymentMethodCheckNoRekNama(p, msg, bjbAccount);
				PostingService.insertOrUpdateRSP(p, msg);
				PostingService.updateLogPenihilan(p, msg);
			}
		}
		else {
			Boolean result = PostingService.sendRequestAndUpdateIfFail(msgToBeSend, p);
			if(result)PostingService.putRequestAndLogging(msg, p);
		}
		
	}
	
	public static void forceInsertSuccessRSP(BulkFTReqPRC p, ISOMsg msg, BulkFTDao bulkFTDao) throws ISOException {
		msg = constructErrorResponseMessage("00");
		bulkFTDao.insertRSP(p, msg);
	}
	
	public static void forceInsertRSP(BulkFTReqPRC p, ISOMsg msg, BulkFTDao bulkFTDao) throws ISOException {
		msg = createISOMsgBasedOnPaymentMethod(p, msg);
		bulkFTDao.insertRSP(p, msg);
	}
	
	public static ISOMsg createISOMsgBasedOnPaymentMethod(BulkFTReqPRC p, ISOMsg msg) throws ISOException {		
		if (p.getPaymentMethod().equals("0")) {
			msg = constructErrorResponseMessage(Constants.SALAH_NO_REKENING); // retur
		}
		else {
			msg = constructErrorResponseMessage("98"); // retry
		}
		return msg;
	}
	
	public static void forceInsertRSPGapuraException(BulkFTReqPRC p, ISOMsg msg, BulkFTDao bulkFTDao) throws ISOException {
		msg = createISOMsgBasedOnPaymentMethodGapuraException(p, msg);
		bulkFTDao.insertRSP(p, msg);
	}
	
	public static ISOMsg createISOMsgBasedOnPaymentMethodGapuraException(BulkFTReqPRC p, ISOMsg msg) throws ISOException {		
		if (p.getPaymentMethod().equals("0")) {
			msg = constructErrorResponseMessage(Constants.GAPURA_TIMEOUT); // retur
		}
		else {
			msg = constructErrorResponseMessage("98"); // retry
		}
		return msg;
	}
	
	public static ISOMsg createISOMsgBasedOnPaymentMethodCheckNoRekNama(BulkFTReqPRC p, ISOMsg msg, FASTGapuraAccount account) throws ISOException {
		if (p.getPaymentMethod().equals("0")) {
			if(Constants.SALAH_NAMA.equalsIgnoreCase(account.getDescriptionDetail())){
				msg = constructErrorResponseMessage(Constants.SALAH_NAMA); // retur
			}
			else if(Constants.SALAH_NO_REKENING.equalsIgnoreCase(account.getDescriptionDetail())){
				msg = constructErrorResponseMessage(Constants.SALAH_NO_REKENING); // retur
			}else{
				msg = constructErrorResponseMessage(Constants.GAPURA_TIMEOUT); //Retry 
			}
		}
		else {
			msg = constructErrorResponseMessage("98"); // retry
		}
		return msg;
	}
	
	public static ISOMsg constructErrorResponseMessage(String responseCode) throws ISOException{
		ISOMsg msg = new ISOMsg();
		msg.setPackager(new SAMPackager());
		
		msg.set(7, "");
		msg.set(11, "");
		msg.set(39, responseCode);
		
		return msg;
	}
	
}
