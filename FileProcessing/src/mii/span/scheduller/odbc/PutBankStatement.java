package mii.span.scheduller.odbc;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;

import javax.faces.bean.ManagedProperty;

import mii.span.constant.MdrCfgMainConstant;
import mii.span.sftp.Atomic;

import com.mii.constant.Constants;
import com.mii.dao.SystemParameterDAO;
import com.mii.models.SystemParameter;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.ApplicationInstance;

public class PutBankStatement implements Serializable{
	private static final long serialVersionUID = 1L;
	@ManagedProperty(value = "#{applicationBean}")
	private static ApplicationBean applicationBean;
	
	public void run(){
		System.out.println("Start Run Put Bank Statement");
		sendBankStatement();
		System.out.println("End Run Put Bank Statement");
	}

	private static void sendBankStatement() {
		
		String localBSPath = getParamValue(Constants.JAR_BANK_STATEMENT_PATH);
		String localBSBackupPath = getParamValue(Constants.JAR_BANK_STATEMENT_PATH_BACKUP);
		String remoteBSHost = getParamValue(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_IPADDRESS);
		String remoteBSUsername = getParamValue(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_USERNAME);
		String remoteBSPassword = getParamValue(MdrCfgMainConstant.SPAN_DEPKEU_SFTP_PASSWORD);
		String remoteBSPath = getParamValue(Constants.SPAN_DEPKEU_SFTP_PUT_BS_REMOTE_DIR);
		File[] files = getLocalFileList(localBSPath);
	    for (int i = 0; i < files.length; i++) {
	      if (files[i].isFile()) {
	        System.out.println("File " + files[i].getName());
	        String status = Atomic.uploadFile(remoteBSPath, localBSPath, files[i].getName(), remoteBSHost, remoteBSUsername, remoteBSPassword);
	        if(status.equals("true")){
	        	copyAndDeleteLocalFile(localBSPath, localBSBackupPath, files, i);
	        }
	      }
	    }
	}

	private static void copyAndDeleteLocalFile(String localBSPath, String localBSBackupPath,
			File[] files, int i) {
		InputStream inStream = null;
		OutputStream outStream = null;
		try{
			File afile =new File(localBSPath.concat(files[i].getName()));
		    File bfile =new File(localBSBackupPath.concat(files[i].getName()));
		    inStream = new FileInputStream(afile);
		    outStream = new FileOutputStream(bfile);

		    byte[] buffer = new byte[1024];

		    int length;
		    //copy the file content in bytes
		    while ((length = inStream.read(buffer)) > 0){
		    	outStream.write(buffer, 0, length);
		    }

		    inStream.close();
		    outStream.close();

		    //delete the original file
		    afile.delete();

		    System.out.println("File is backup successful!");
		}catch(Exception e){
		    e.printStackTrace();
		}
	}

	private static File[] getLocalFileList(String localBSPath) {
		File folder = new File(localBSPath);
		File [] files = folder.listFiles(new FilenameFilter() {
		    @Override
		    public boolean accept(File dir, String name) {
		        return name.endsWith(".jar");
		    }
		});
		return files;
	}
	
	private static SystemParameterDAO getSystemParameterDaoBean(){
		applicationBean = ApplicationInstance.getInstance().getApplicationBean();
		return (SystemParameterDAO) applicationBean.getFileProcessContext().getBean("sysParamDAO");
	}
	
	private static String getParamValue(String paramName){
		SystemParameter systemParameter = getSystemParameterDaoBean().getValueParameter(paramName).get(0);
		return systemParameter.getParam_value();
	}
}
