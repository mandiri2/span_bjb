package mii.span.scheduller.odbc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import mii.span.Log;
import mii.span.db.Service;
import mii.span.db.adapter.Odbc;
import mii.span.doc.DataArea;
import mii.span.doc.SP2DDocument;
import mii.span.model.SpanDataOriginalXML;
import mii.span.model.SpanDataValidation;
import mii.span.model.SpanDataValidationSP2DNo;
import mii.span.process.model.PopulateSP2DNoDetailsForVoidInput;
import mii.span.util.BusinessUtil;
import mii.span.util.GetCurrentDateTime;
import mii.span.util.PubUtil;

public class SpanDataValidationSP2DNoScheduller {
	
	public void run(){
		System.out.println("Start Run SpanDataValidationSP2DNo");
		spanDataValidationSP2DNo();
		System.out.println("End Run SpanDataValidationSP2DNo");
	}

	public static void spanDataValidationSP2DNo(){
		//o mandiri.span.db.adapter.odbc:selectSPANDataValidationStatus
		List<SpanDataValidation> spanDataList = Odbc.selectSPANDataValidationStatus("1", "V");
		//c mandiri.span.db.adapter.odbc:selectSPANDataValidationStatus
		//o loop
		for (Iterator iterator = spanDataList.iterator(); iterator.hasNext();) {
			SpanDataValidation spanDataValidation = (SpanDataValidation) iterator.next();
			String isExist = "";
			//o mandiri.span.db.service.odbc:checkFileOnSpanDataValidationSP2DNo
			isExist = checkFileOnSpanDataValidationSP2DNo(spanDataValidation.getFileName());
			//c mandiri.span.db.service.odbc:checkFileOnSpanDataValidationSP2DNo
			if("No".equalsIgnoreCase(isExist)){
				try {
					int indexRF = spanDataValidation.getFileName().indexOf("RF");
					String fileName = spanDataValidation.getFileName();
					String debitAccountType = spanDataValidation.getDebitAccountType();
					String currentDate = GetCurrentDateTime.getCurrentDateString("yyyyMMdd");
					String documentDate = spanDataValidation.getDocumentDate().replace("-", "");
					if(indexRF<=0 
							&&  "0000".equalsIgnoreCase(spanDataValidation.getResponseCode())){
						//o mandiri.span.log:debugLog
						Log.debugLog("SPAN", "SpanDataValidationSP2DNo", "INFO", 
								PubUtil.concat("Start Extracting SP2DNo for file ", spanDataValidation.getFileName()));
						//c mandiri.span.log:debugLog
						try {
							//o mandiri.span.db.service.odbc:insertSPANDataOriginalXML
							SpanDataOriginalXML spanOriData = new SpanDataOriginalXML();
							spanOriData.setFILE_NAME(spanDataValidation.getFileName());
							spanOriData.setDOCUMENT_DATE(spanDataValidation.getDocumentDate());
							spanOriData.setTOTAL_RECORD(spanDataValidation.getTotalRecord());
							spanOriData.setDEBIT_ACCOUNT_NO(spanDataValidation.getDebitAccount());
							spanOriData.setDEBIT_ACCOUNT_TYPE(spanDataValidation.getDebitAccountType());
							spanOriData.setXML_FILE_NAME(spanDataValidation.getXmlFileName());
							spanOriData.setDESCRIPTION("");
							String result = mii.span.db.service.Odbc.insertSPANDataOriginalXML(spanOriData);
							//c mandiri.span.db.service.odbc:insertSPANDataOriginalXML
						}catch (Exception e) {
								e.printStackTrace();
								Log.debugLog("SPAN", "SpanDataValidationSP2DNo", "INFO", e.getMessage());
						}
						SP2DDocument spanDocument = PubUtil.createSPANDocument(spanDataValidation.getXmlFileName());
						List<PopulateSP2DNoDetailsForVoidInput> inputList = new ArrayList<PopulateSP2DNoDetailsForVoidInput>();
						
						for (Iterator iterator2 = spanDocument.getDataArea().iterator(); iterator2.hasNext();) {
							DataArea dataArea= (DataArea) iterator2.next();
							PopulateSP2DNoDetailsForVoidInput input = new PopulateSP2DNoDetailsForVoidInput();
								input.setAmount(dataArea.getAmount());
								input.setDebitAccount(dataArea.getAgentBankAccountNumber());
								input.setDebitAccountType(debitAccountType);
								input.setDocumentDate(dataArea.getDocumentDate());
								input.setDocumentNO(dataArea.getDocumentNumber().substring(0, 15));//substring to get SP2DNO
								input.setFilename(fileName);
								input.setProcStatusFile("1");
								input.setProcStatusSP2DNO("1");
								inputList.add(input);
						}
							
						//o mandiri.span.util:populateSP2DNODetailsForVOID
						List<SpanDataValidationSP2DNo> outputList = new ArrayList<SpanDataValidationSP2DNo>();
						outputList = populateSP2DNODetailsForVoid(inputList);
						//c mandiri.span.util:populateSP2DNODetailsForVOID
						
						try {
							//o mandiri.span.db.service:spanDataValidationSP2DNO
							Service.insertSPANDataValidationSP2DNO(outputList);
							//c mandiri.span.db.service:spanDataValidationSP2DNO
							Log.debugLog("SPAN", "SpanDataValidationSP2DNo", "INFO", 
									PubUtil.concat("Finish Extracting SP2DNo for file ",spanDataValidation.getFileName()));
						} catch (Exception e) {
							e.printStackTrace();
							Log.debugLog("SPAN", "SpanDataValidationSP2DNo", "INFO", 
									PubUtil.concat("Finish Extracting SP2DNo for file ",e.getMessage()));
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
					Log.debugLog("SPAN", "SpanDataValidationSP2DNo", "ERROR", PubUtil.concat("File ",spanDataValidation.getFileName(),
							" error ",e.toString()));
				}   
			}
			else{
				Log.debugLog("SPAN", "SpanDataValidationSP2DNo", "INFO", 
						PubUtil.concat("LOG >>>>>>>> SUCCES Validation TO EQ ", spanDataValidation.getFileName()," is already extracted"));
			
//				Log.debugLog("SPAN", "SpanDataValidationSP2DNo", "INFO", 
//						PubUtil.concat("File ", spanDataValidation.getFileName()," is already extracted"));
			}
		}
		//c loop
	}

	private static List<SpanDataValidationSP2DNo> populateSP2DNODetailsForVoid(
			List<PopulateSP2DNoDetailsForVoidInput> Inputs) {
		// pipeline
//		IDataCursor pipelineCursor = pipeline.getCursor();
		HashMap hm = new HashMap();
		int totalAmount = 0;
		int totalRecord = 0;
		String debitAccount = "";
		String filename = "";
		String documentDate = "";
		String debitAccountType = "";
		String procStatusFile = "";
		String procStatusSP2DNO = "";
		BigDecimal amountBD = null;
		BigDecimal totalAmountBD = null;
		
		int totalMap = 0;
		System.out.println("0");
			// Inputs
//			IData[]	Inputs = IDataUtil.getIDataArray( pipelineCursor, "Inputs" );
			if ( Inputs != null)
			{
				for ( int i = 0; i < Inputs.size(); i++ )
				{
					
//					IDataCursor InputsCursor = Inputs[i].getCursor();
					PopulateSP2DNoDetailsForVoidInput input = Inputs.get(i);
					String	sp2dNO = input.getDocumentNO().substring(0,15);
					String	amount = input.getAmount();
					debitAccount = input.getDebitAccount();
					filename = input.getFilename();
					documentDate = input.getDocumentDate();
					debitAccountType = input.getDebitAccountType();
					procStatusFile = input.getProcStatusFile();
					procStatusSP2DNO = input.getProcStatusSP2DNO();
					System.out.println(totalMap+"|"+sp2dNO);
					
//					InputsCursor.destroy();
					if(!hm.containsValue(sp2dNO)){
						//System.out.println("2 : "+sp2dNO+" | "+amount);
						//System.out.println("2 : "+amount+"|"+documentDate+"|"+debitAccount+"|"+debitAccount+"|"+debitAccountType+"|"+filename+"|"+procStatusFile+"|"+procStatusSP2DNO);				
						hm.put(totalMap, sp2dNO);
						totalMap++;
						hm.put(sp2dNO.concat("TA"), amount);
						hm.put(sp2dNO.concat("TR"), 1);	
						hm.put(sp2dNO.concat("DD"), documentDate);
						hm.put(sp2dNO.concat("DA"), debitAccount);
						hm.put(sp2dNO.concat("AT"), debitAccountType);
						hm.put(sp2dNO.concat("FN"), filename);
						hm.put(sp2dNO.concat("P1"), procStatusFile);
						hm.put(sp2dNO.concat("P2"), procStatusSP2DNO);
						//System.out.println("3");
					}else{
						//System.out.println("There is a Map");
						//totalAmount=Integer.parseInt(hm.get(sp2dNO.concat("TA")).toString())+Integer.parseInt(amount);
						
						amountBD = new BigDecimal(amount);
						totalAmountBD = new BigDecimal(hm.get(sp2dNO.concat("TA")).toString());
						totalAmountBD = totalAmountBD.add(amountBD);
						String totalAmountStr = String.valueOf(totalAmountBD);
		
						totalRecord=Integer.parseInt(hm.get(sp2dNO.concat("TR")).toString())+1;
						//System.out.println("Total Amount : "+totalAmount+" | "+sp2dNO+" | "+amount);
						//System.out.println("Total Record : "+totalRecord+" | "+sp2dNO+" | "+amount);
						//hm.put(sp2dNO.concat("TA"), totalAmount);
						hm.put(sp2dNO.concat("TA"), totalAmountStr);
						hm.put(sp2dNO.concat("TR"), totalRecord);
					}
					System.out.println("HM Size : "+totalMap);
				}
			}
		
		// Outputs
//		IData[]	Outputs = new IData[totalMap];
//		List<PopulateSP2DNoDetailsForVoidOutput> outputList = new ArrayList<PopulateSP2DNoDetailsForVoidOutput>();
		List<SpanDataValidationSP2DNo> spanSP2DNoList = new ArrayList<SpanDataValidationSP2DNo>();
		for(int x=0; x<totalMap; x++){
			//System.out.println("Output : "+totalMap);
//			Outputs[x] = IDataFactory.create();	
//			PopulateSP2DNoDetailsForVoidOutput output = new PopulateSP2DNoDetailsForVoidOutput();
			SpanDataValidationSP2DNo spanOutput = new SpanDataValidationSP2DNo();
//			IDataCursor OutputsCursor = Outputs[x].getCursor();
			String sp2dNO = hm.get(x).toString();
			//System.out.println("Output A");
//			output.setSp2dNO(sp2dNO);
			spanOutput.setSP2DNO(sp2dNO);
			//System.out.println("Output B");
//			output.setTotalAmount(hm.get(sp2dNO.concat("TA")).toString());
			spanOutput.setTOTAL_AMOUNT(hm.get(sp2dNO.concat("TA")).toString());
			//System.out.println("Output C "+hm.get(sp2dNO.concat("TA")).toString()+" | "+sp2dNO);
			spanOutput.setTOTAL_RECORD( hm.get(sp2dNO.concat("TR")).toString() );
			//System.out.println("Output D"+hm.get(sp2dNO.concat("TR")).toString()+" | "+sp2dNO);
			spanOutput.setDOCUMENT_DATE(hm.get(sp2dNO.concat("DD")).toString() );
			spanOutput.setDEBIT_ACCOUNT_NO(hm.get(sp2dNO.concat("DA")).toString() );
			spanOutput.setFILE_NAME(hm.get(sp2dNO.concat("FN")).toString() );
			spanOutput.setDEBIT_ACCOUNT_TYPE(hm.get(sp2dNO.concat("AT")).toString() );	
			//System.out.println("Output E");
			spanOutput.setPROC_STATUS_FILE(hm.get(sp2dNO.concat("P1")).toString() );
			//System.out.println("Output F");
			spanOutput.setPROC_STATUS_SP2DNO(hm.get(sp2dNO.concat("P2")).toString() );
			//System.out.println("Output G");
			spanOutput.setVOID_FLAG("0" );
//			OutputsCursor.destroy();
			//add to list
			spanSP2DNoList.add(spanOutput);
		}
	
		hm.clear();
		
		return spanSP2DNoList;
	}

	private static String checkFileOnSpanDataValidationSP2DNo(String fileName) {
		String isExist = "No";
		try {
			//o mandiri.span.db.adapter.odbc:selectSpanDataValidationSP2DNoByFilename
			List<SpanDataValidationSP2DNo> list = Odbc.selectSpanDataValidationSP2DNoByFilename(fileName);
			//c mandiri.span.db.adapter.odbc:selectSpanDataValidationSP2DNoByFilename
			if(!BusinessUtil.isListEmpty(list)){
				isExist = "Yes";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isExist;
	}
}
