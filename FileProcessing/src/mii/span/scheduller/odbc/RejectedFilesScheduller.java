package mii.span.scheduller.odbc;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.hibernate.Query;

import mii.span.Log;
import mii.span.db.Service;
import mii.span.db.adapter.Odbc;
import mii.span.doc.ApplicationArea;
import mii.span.doc.DataArea;
import mii.span.doc.Footer;
import mii.span.doc.SP2DDocument;
import mii.span.model.FileRejectedInput;
import mii.span.model.SpanDataValidation;
import mii.span.model.SpanRecreateFileRejected;
import mii.span.model.SpanRejectedFileHis;
import mii.span.model.helper.CreateRFOutput;
import mii.span.util.BusinessUtil;
import mii.span.util.GetCurrentDateTime;
import mii.span.util.PubUtil;

import com.mii.constant.Constants;
import com.mii.dao.SpanDataValidationDao;
import com.mii.dao.SystemParameterDAO;
import com.mii.helpers.InterfaceDAO;

import edu.emory.mathcs.backport.java.util.Arrays;

public class RejectedFilesScheduller {
	
	public void run(){
		spanDataValidationForRejectedFile();
	}

	public static void spanDataValidationForRejectedFile() {
		// debug Log
		Log.debugLog("SPAN", "SpanDataValidationForRejectFile", "INFO",
				"Searching data for Reject File");

		insertSPANDataValidationForRejectedFile();

		// debug log
		Log.debugLog("SPAN", "SpanDataValidationForRejectFile", "INFO",
				"Searching data for Reject File END");

	}
	
	public static  void insertSPANDataValidationForRejectedFile(){
		String procStatus = "1";
		List<SpanDataValidation> spanDataValidationList = Service
				.selectSPANDataValidationStatus(procStatus);
		String pattern = "yyyy-MM-dd";
		String currentDate = GetCurrentDateTime.getCurrentDateString(pattern);
		
		// for loop advance
		System.out.println("#3 for advance");
		for (SpanDataValidation spanDataValidation : spanDataValidationList) {
			String filename = spanDataValidation.getFileName();
			if (filename.indexOf("RF") <= 0){
				int totalData = Service.selectCountSPANFileRejected(filename);
				if ("005".equalsIgnoreCase(spanDataValidation.getResponseCode())
						&& totalData == 0) {
					FileRejectedInput input = new FileRejectedInput(filename,"");
					List<FileRejectedInput> inputList = new ArrayList<FileRejectedInput>();
					inputList.add(input);
					String schedulerFlag = "1";
					createSPANFileRejected(inputList, schedulerFlag);
				} else {
					// do nothing
				}
			}
		}
	}
	
	private static void createSPANFileRejected(List<FileRejectedInput> rejectedList, String schedulerFlag) {
		for (Iterator iterator = rejectedList.iterator(); iterator.hasNext();) {
			FileRejectedInput fileRejectedInput = (FileRejectedInput) iterator.next();
			String fileName = fileRejectedInput.getFileName();
			SpanDataValidation spanData = (SpanDataValidation) Odbc.selectSpanDataValidation(fileName).get(0);
			String oriTotalRecord = spanData.getTotalRecord();
			String oriTotalAmount = spanData.getTotalAmount();
			if("1".equalsIgnoreCase(spanData.getProcStatuc())){
				try {
					SP2DDocument spanDoc = PubUtil.createSPANDocument(spanData.getXmlFileName());
					String suffix  = "0";
					String originalFilename = "";
					String newFilename = "";
	
					String documentDateCurrent = GetCurrentDateTime.getCurrentDateString("yyyy-MM-dd");
					List<DataArea> dataAreaList = new ArrayList<DataArea>(spanDoc.getDataArea());
					
					for (DataArea dataArea : spanDoc.getDataArea() ) {
						
						CreateRFOutput rf = createRF(suffix, fileRejectedInput.getFileName());
						suffix = rf.getSuffix();
						newFilename = rf.getNewFileName();
						
						//new object SP2D Document
						ApplicationArea appArea = new ApplicationArea();
						appArea.setApplicationAreaCreationDateTime(spanDoc.getApplicationArea().getApplicationAreaCreationDateTime());
						appArea.setApplicationAreaDetailReceiverIdentifier(spanDoc.getApplicationArea().getApplicationAreaDetailReceiverIdentifier());
						appArea.setApplicationAreaDetailSenderIdentifier(spanDoc.getApplicationArea().getApplicationAreaDetailSenderIdentifier());
						appArea.setApplicationAreaMessageIdentifier(spanDoc.getApplicationArea().getApplicationAreaMessageIdentifier());
						appArea.setApplicationAreaMessageTypeIndicator(spanDoc.getApplicationArea().getApplicationAreaMessageTypeIndicator());
						appArea.setApplicationAreaMessageVersionText(spanDoc.getApplicationArea().getApplicationAreaMessageVersionText());
						appArea.setApplicationAreaReceiverIdentifier(spanDoc.getApplicationArea().getApplicationAreaReceiverIdentifier());
						appArea.setApplicationAreaSenderIdentifier(spanDoc.getApplicationArea().getApplicationAreaSenderIdentifier());
			
						List<DataArea> dataAreaRejectList= new ArrayList<DataArea>();
						DataArea dataArea2 = new DataArea();
						dataArea2.setAgentBankAccountName(dataArea.getAgentBankAccountName());
						dataArea2.setAgentBankAccountNumber(dataArea.getAgentBankAccountNumber());
						dataArea2.setAgentBankCode(dataArea.getAgentBankCode());
						dataArea2.setAmount(dataArea.getAmount());
						dataArea2.setBeneficiaryAccount(dataArea.getBeneficiaryAccount());
						dataArea2.setBeneficiaryBank(dataArea.getBeneficiaryBank());
						dataArea2.setBeneficiaryBankCode(dataArea.getBeneficiaryBankCode());
						dataArea2.setBeneficiaryName(dataArea.getBeneficiaryName());
						dataArea2.setCurrencyTarget(dataArea.getCurrencyTarget());
						dataArea2.setDescription(dataArea.getDescription());
						dataArea2.setDocumentDate(dataArea.getDocumentDate());
						dataArea2.setDocumentNumber(dataArea.getDocumentNumber());
						dataArea2.setEmailAddress(dataArea.getEmailAddress());
						dataArea2.setIBANCode(dataArea.getIBANCode());
						dataArea2.setPaymentMethod(dataArea.getPaymentMethod());
						dataArea2.setSP2DCount(dataArea.getSP2DCount());
						dataArea2.setSwiftCode(dataArea.getSwiftCode());
						dataAreaRejectList.add(dataArea2);
						
						Footer footer = new Footer();
						footer.setTotalCount("1");
						footer.setTotalBatchCount("1");
						String totalAmount = dataArea.getAmount();
						footer.setTotalAmount(totalAmount);
						
						SP2DDocument sp2dReject = new SP2DDocument();
						sp2dReject.setApplicationArea(appArea);
						sp2dReject.setDataArea(dataAreaRejectList);
						sp2dReject.setFooter(footer);

						String DocumentDate = dataArea.getDocumentDate();
						String TOTAL_AMOUNT = dataArea.getAmount();

						//Convert to XML
						String xmlvalues = PubUtil.createXMLSpanDocument(sp2dReject);
						String batchId = BusinessUtil.batchID();
						String trxHeaderId  = BusinessUtil.trxHeaderID();
						
						SpanDataValidation spanDataValidation = new SpanDataValidation();
						spanDataValidation.setFileName(newFilename);
						spanDataValidation.setBatchId(batchId);
						spanDataValidation.setTrxHeaderId(trxHeaderId);
						spanDataValidation.setSpanFnType(spanData.getSpanFnType());
						spanDataValidation.setDebitAccount(spanData.getDebitAccount());
						spanDataValidation.setDebitAccountType(spanData.getDebitAccountType());
						spanDataValidation.setDocumentDate(documentDateCurrent);
						spanDataValidation.setTotalAmount(dataArea.getAmount());
						spanDataValidation.setTotalRecord("1");
						spanDataValidation.setProcStatuc("1");
						spanDataValidation.setProcDescription("Rejected File, request from COP/DEPKEU");
						spanDataValidation.setXmlFileName(xmlvalues);
						spanDataValidation.setResponseCode("005");
						spanDataValidation.setTrxType(spanData.getTrxType());
						spanDataValidation.setRejectedFileFlag("0");
						spanDataValidation.setCreateDate(new Date());
						Odbc.insertSPANDataValidation(spanDataValidation);
						
						SpanRecreateFileRejected spanRecreateFile = new SpanRecreateFileRejected();
						spanRecreateFile.setOriginalFileName(spanData.getFileName());
						spanRecreateFile.setNewFileName(newFilename);
						spanRecreateFile.setDocumentNo(dataArea.getDocumentNumber());
						spanRecreateFile.setOriginalDocumentDate(dataArea.getDocumentDate());
						spanRecreateFile.setBeneficiaryName(dataArea.getBeneficiaryName());
						spanRecreateFile.setBeneficiaryBankCode(dataArea.getBeneficiaryBankCode());
						spanRecreateFile.setBeneficiaryBank(dataArea.getBeneficiaryBank());
						spanRecreateFile.setBeneficiaryAccount(dataArea.getBeneficiaryAccount());
						spanRecreateFile.setAmount(dataArea.getAmount());
						spanRecreateFile.setDescription(dataArea.getDescription());
						spanRecreateFile.setAgentBankCode(dataArea.getAgentBankCode());
						spanRecreateFile.setAgentBankAccountNo(dataArea.getAgentBankAccountNumber());
						spanRecreateFile.setAgentBankAccountName(dataArea.getAgentBankAccountName());
						spanRecreateFile.setPaymentMethod(dataArea.getPaymentMethod());
						spanRecreateFile.setTransactionFlag("0");
						spanRecreateFile.setCreateDate(new Date());
						Service.insertSpanRecreateRejectedFile(spanRecreateFile);
						
						
						String status = "SUCCESS";
						
						if("SUCCESS".equalsIgnoreCase(status)){
							SpanRejectedFileHis spanHis = new SpanRejectedFileHis();
							spanHis.setFILE_NAME(spanData.getFileName());
							spanHis.setTOTAL_RECORD(oriTotalRecord);
							spanHis.setTOTAL_AMOUNT(oriTotalAmount);
							spanHis.setCREATE_DATE(GetCurrentDateTime.getCurrentDateString("dd-MMM-yy").toUpperCase());
							
							Service.insertSpanRejectedFileHis(spanHis);
							
							InterfaceDAO dao = (SpanDataValidationDao) BusinessUtil.getDao("spanDataValidationDao");
							String hql = "UPDATE SpanDataValidation SET proc_status='R' "
									+ "WHERE file_name= '"+spanData.getFileName()+"' and response_code='005'";
							Query query = dao.getQuery(hql);
							Service.updateSPAN(query);
						}
								
					}
				} catch (JAXBException e) {
					e.printStackTrace();
				}
				
			}
			
		}
		
	}
	
	public static CreateRFOutput createRF(String suffix, String originalFileName){
		CreateRFOutput output = new CreateRFOutput();
		
		String prefix = "RF";
		String[] fileNameDetails = PubUtil.tokenize(originalFileName, "_");
		
		suffix = PubUtil.padLeft(suffix, "0", 4);
		
		String rejectFileName = prefix+suffix;
		
		fileNameDetails[4] = rejectFileName;
		
		suffix = Integer.parseInt(suffix)+1+"";
		
		String newFileName = PubUtil.makeString(fileNameDetails, "_");
		
		output.setSuffix(suffix);
		output.setNewFileName(newFileName);
		return output;
	}
	
}
