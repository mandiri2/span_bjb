package mii.span.scheduller.odbc;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import mii.span.atomic.response.odbc.Ack;
import mii.span.process.model.SentAckDataFileRequest;
import mii.span.process.model.SentAckDataFileResponse;
import mii.span.util.PubUtil;

import com.mii.constant.Constants;
import com.mii.dao.SpanHostDataDetailsDao;
import com.mii.dao.SystemParameterDAO;
import com.mii.models.SystemParameter;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.ApplicationInstance;
import com.mii.span.model.SPANDataValidation;

@ManagedBean
@ViewScoped
public class VoidLister implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3322224070304917646L;
	private List<SPANDataValidation> todayVoidList;

	@ManagedProperty(value = "#{applicationBean}")
	private static ApplicationBean applicationBean;

	private SpanHostDataDetailsDao getSpanHostDataDetailsDao(){
		applicationBean = ApplicationInstance.getInstance().getApplicationBean();
		return (SpanHostDataDetailsDao) applicationBean.getFileProcessContext().getBean("spanHostDataDetailsDao");
	}

	public void fillVoidList(){
		todayVoidList = getSpanHostDataDetailsDao().getVoidDataToday();
	}
	
	private static SystemParameterDAO getSystemParameterDaoBean(){
		applicationBean = ApplicationInstance.getInstance().getApplicationBean();
		return (SystemParameterDAO) applicationBean.getFileProcessContext().getBean("sysParamDAO");
	}
	
	public void run(){
		SystemParameter sysParamFlag = getSystemParameterDaoBean().getValueParameter(Constants.ACK_VOID_HLP_RUN_FLAG).get(0);
		String ACK_VOID_HLP_RUN_FLAG = sysParamFlag.getParam_value();
		SystemParameter sysParamStart = getSystemParameterDaoBean().getValueParameter(Constants.ACK_VOID_HLP_RUN_START).get(0);
		String ACK_VOID_HLP_RUN_START = sysParamStart.getParam_value();
		SystemParameter sysParamEnd = getSystemParameterDaoBean().getValueParameter(Constants.ACK_VOID_HLP_RUN_END).get(0);
		String ACK_VOID_HLP_RUN_END = sysParamEnd.getParam_value();
		SimpleDateFormat sdf = new SimpleDateFormat(Constants.ACK_VOID_HLP_HOUR_FORMAT);
		String hour = sdf.format(new Date());
		if((Integer.parseInt(ACK_VOID_HLP_RUN_START) <= Integer.parseInt(hour) && 
				Integer.parseInt(ACK_VOID_HLP_RUN_END) >= Integer.parseInt(hour))&&
				ACK_VOID_HLP_RUN_FLAG.equals("0")){
			getSystemParameterDaoBean().updateParameter(Constants.ACK_VOID_HLP_RUN_FLAG, "1");
			System.out.println("Start ACK VOID");

			autoSendAck();
				
			System.out.println("End ACK VOID");
		}else{
			if(Integer.parseInt(ACK_VOID_HLP_RUN_START) <= Integer.parseInt(hour) && 
					Integer.parseInt(ACK_VOID_HLP_RUN_END) >= Integer.parseInt(hour)){
				//DO NOTHING
				System.out.println("ACK VOID Not yet.");
			}else{
				if(ACK_VOID_HLP_RUN_FLAG.equals("0")){
					//DO NOTHING
					System.out.println("ACK VOID Not yet.");
				}else{
					sysParamEnd.setParam_value("0");
					getSystemParameterDaoBean().updateParameter(Constants.ACK_VOID_HLP_RUN_FLAG, "0");
					System.out.println("ACK VOID Ok here we go...");
				}
			}
		}
	}

	public void autoSendAck(){
		todayVoidList = new ArrayList<SPANDataValidation>();
		fillVoidList();
		//o mandiri.span.atomic.response.odbc.ack:sentACKDataFile
		String ackDataFile = "";
		for (Iterator iterator = todayVoidList.iterator(); iterator.hasNext();) {
			SPANDataValidation type = (SPANDataValidation) iterator.next();
			ackDataFile = ackDataFile.concat(constructAckString(type.getDocumentDate(), type.getDocNumber(), type.getFileName())).concat("\n");
		}
		if(todayVoidList.size()>0){
			String documentType = "SP2D";
			SentAckDataFileRequest sentAckDataFileRequest = new SentAckDataFileRequest();
			sentAckDataFileRequest.setFilename(constructFilename());
			sentAckDataFileRequest.setDocumentType(documentType);
			sentAckDataFileRequest.setErrorMessage(Constants.MSG_VOID);
			sentAckDataFileRequest.setErrorCode(Constants.RC_VOID);
			sentAckDataFileRequest.setAckDataString(ackDataFile);

			SentAckDataFileResponse sentAckDataFileResponse = Ack.sentACKDataFile(sentAckDataFileRequest);
			System.out.println(sentAckDataFileResponse.toString());
		}
	}

	private String constructFilename() {
		Timestamp now = new Timestamp(Calendar.getInstance().getTime().getTime());
		String bankCode = getSystemParameterDaoBean().getValueByParamName(Constants.KODE_BANK_12_DIGIT);
		String sp2d = "SP2D";
		String fa = "FA";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
		String dateTime = sdf.format(now);
		String ext = ".jar";
		return bankCode+"_"+sp2d+"_"+fa+"_"+dateTime+ext;
	}

	private String constructAckString(String docDate,String docNumber,String fileName){
		String documentDate = PubUtil.padRight(docDate, 10);
		String documentType = "SP2D";
		String documentNumber = PubUtil.padRight(docNumber, 21);
		String filename = PubUtil.padRight(fileName,100);
		String returnCode = PubUtil.padLeft(Constants.RC_VOID, "0", 4) ;
		String description = PubUtil.padRight(Constants.MSG_VOID, 200);
		return documentDate+"|"+documentType+"|"+documentNumber+"|"+filename+"|"+returnCode+"|"+description;
	}
}
