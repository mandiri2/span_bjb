package mii.span.scheduller.odbc;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedProperty;

import org.hibernate.Query;

import com.mii.constant.DaoConstant;
import com.mii.dao.SpanDataValidationDao;
import com.mii.dao.SystemParameterDAO;
import com.mii.helpers.InterfaceDAO;
import com.mii.models.SystemParameter;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.ApplicationInstance;

import mii.span.Log;
import mii.span.constant.MdrCfgMainConstant;
import mii.span.constant.SParameterConstant;
import mii.span.db.Adapter;
import mii.span.db.Service;
import mii.span.db.adapter.Odbc;
import mii.span.model.SpanDataValidation;
import mii.span.model.SpanRecreateFileRejected;
import mii.span.process.model.CheckingAmountTrxRequest;
import mii.span.process.model.ExecuteDataReqResponse;
import mii.span.process.model.ExecuteVoidDocumentNoRequest;
import mii.span.process.model.ExecuteVoidDocumentNoResponse;
import mii.span.process.model.GetSCFirstDataResponse;
import mii.span.process.model.InsertBulkFtResponse;
import mii.span.process.model.SpanHostRequestResponse;
import mii.span.process.model.SpanInputRequest;
import mii.span.process.model.SpanReportResponse;
import mii.span.process.model.UpdateForFileRejectedResponse;
import mii.span.process.model.UpdateSpanResponse;
import mii.span.service.Report;
import mii.span.util.BusinessUtil;
import mii.span.util.PubUtil;
import mii.utils.CommonDate;
//lanjutsini
public class Request {
	
	@ManagedProperty(value = "#{applicationBean}")
	private static ApplicationBean applicationBean;
	
	private static SystemParameterDAO getSystemParameterDaoBean(){
		applicationBean = ApplicationInstance.getInstance().getApplicationBean();
		return (SystemParameterDAO) applicationBean.getFileProcessContext().getBean("sysParamDAO");
	}
	
	public void runExecuteDataReq(){
		System.out.println("Test runExecuteDataReq");
		executeDataReq();
	}
	
	public void runInsertBulkFT(){
		System.out.println("Test runInsertBulkFT");
		insertBulkFt();
	}
	
	/**
	 * mandiri.span.scheduller.odbc.request:executeDataReq
	 */
	public static ExecuteDataReqResponse executeDataReq(){
		System.out.println("Start Run ExecuteDataReq");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		InterfaceDAO dao = (SpanDataValidationDao) BusinessUtil.getDao(DaoConstant.SpanDataValidationDao);
		ExecuteDataReqResponse response = new ExecuteDataReqResponse();
		String statusVoid=null;
		String ErrMsgVoid=null;
		String oriDocDate = null;
		String nextDay = null;
		String Status = null;
		String ErrorMessage = null;
		//o mandiri.span.db.adapter.odbc:selectSPANDataExecuted
		List<SpanDataValidation> SpanDataValidationList = Odbc.selectSPANDataExecuted("X");
		System.out.println("SpanDataValidationList : "+SpanDataValidationList.size());
		//c mandiri.span.db.adapter.odbc:selectSPANDataExecuted
		
		//o map
		String SCDate = CommonDate.getCurrentDateString("yyyyMMdd");
		String sourceBranch = Odbc.selectSParameter(SParameterConstant._20130111P01).getValue();
		String currentDate = CommonDate.getCurrentDateString("yyyy-MM-dd HH:mm:ss");
		SystemParameter systemParameterspanLocalPath = getSystemParameterDaoBean().getValueParameter(MdrCfgMainConstant.SPAN_DEPKEU_FILE_PATH).get(0);
		String spanLocalPath = systemParameterspanLocalPath.getParam_value();
		
		String overLimitCapacity = "N";
		String hashStatus = "N";
		String sqlUpdateSCFN = "";
		
		//c map
		
		//o sequence
			try {
				
				//o map
				//do nothing
				//c map
				
				//o loop.over.files
					if(SpanDataValidationList!=null){
						for(SpanDataValidation Files2 : SpanDataValidationList){
							
							//o map
							//do nothing
							//c map
							//o branch.rejected.on.files.rejected_file_flag
								REJECTED:
								//o 0.sequence
								if("0".equalsIgnoreCase(Files2.getRejectedFileFlag())){
									//o mandiri.span.db.adapter:selectSPANFileRejected
									List<SpanRecreateFileRejected> spanRecreateFileRejecteds = Adapter.selectSpanFileRejected(Files2.getFileName());
									System.out.println("spanRecreateFileRejecteds : "+spanRecreateFileRejecteds.size()+" - "+Files2.getFileName());
									String TRANSACTION_FLAG = spanRecreateFileRejecteds.get(0).getTransactionFlag();
									
									//c mandiri.span.db.adapter:selectSPANFileRejected
									//o branch.on.transaction_flag
									String StatusUpdateReject = null;
									String ErrMsgReject = null;
										//o mandiri.span.db.service:updateForFileRejected
										if("1".equalsIgnoreCase(TRANSACTION_FLAG)){
											UpdateForFileRejectedResponse updateForFileRejectedResponse = Service.updateForFileRejected(Files2.getFileName());
											StatusUpdateReject = updateForFileRejectedResponse.getStatus();
											ErrMsgReject = updateForFileRejectedResponse.getErrorMessage();
										}
										//c mandiri.span.db.service:updateForFileRejected
										//o sequence.0
										else if("0".equalsIgnoreCase(TRANSACTION_FLAG)){
											//o map.drop.files
											Files2 = null;
											//c map.drop.files
											//o exit.rejected
											break REJECTED;
											//c exit.rejected
										}
										//c sequence.0
										//o default
										else{
											//do nothing
										}
										//c default
									//c branch.on.transaction_flag
									//o mandiri.span.log:debugLog
									Log.debugLog("SPAN", "request:executeDataReq", "INFO", PubUtil.concat("Status Rejected File = ",Files2.getFileName(), 
											"|| Status = ", StatusUpdateReject, "|| Error Message = ",ErrMsgReject));
									//c mandiri.span.log:debugLog	
									//o branch.on.statusupdatereject
										//o error.sequence
										if("ERROR".equalsIgnoreCase(StatusUpdateReject)){
											//o map.dropfiles
											Files2 = null;
											//c map.dropfiles
											//o exit.rejected
											break REJECTED;
											//c exit.rejected
										}
										//c error.sequence
										//o default
										else{
											//do nothing
										}
										//c default
									//c branch.on.statusupdatereject
								}
								//c 0.sequence
								//o default
								else{
									//do nothing
								}
								//c default
							//c branch.rejected.on.files.rejected_file_flag
							//o branch.on.files.void_flag
								//o 1.sequence
								if("1".equalsIgnoreCase(Files2.getVoidFlag())){
									//o mandiri.span.atomic.request:executeVoidDocumentNO
									ExecuteVoidDocumentNoRequest executeVoidDocumentNoRequest = new ExecuteVoidDocumentNoRequest();
									executeVoidDocumentNoRequest.setFileName(Files2.getFileName());
									executeVoidDocumentNoRequest.setXmlFileName(Files2.getXmlFileName());
									ExecuteVoidDocumentNoResponse executeVoidDocumentNoResponse = mii.span.atomic.Request.executeVoidDocumentNO(executeVoidDocumentNoRequest);
									statusVoid = executeVoidDocumentNoResponse.getStatus();
									ErrMsgVoid = executeVoidDocumentNoResponse.getErrorMessage();
									//c mandiri.span.atomic.request:executeVoidDocumentNO
									//o mandiri.span.log:debugLog
									Log.debugLog("SPAN", "executeDataReq", "INFO", PubUtil.concat("Status VOID File = ", 
											Files2.getFileName(), "|| Status = ", statusVoid, "|| Error Message = ", ErrMsgVoid));
									//c mandiri.span.log:debugLog
									//o branch.on.statusVoid
										//o map.success
										if("SUCCESS".equalsIgnoreCase(statusVoid)){
											//do nothing
										}
										//c map.success
										//o map.default
										else{
											//do nothing
										}
										//c map.default
									//c branch.on.statusVoid
								}
								//c 1.sequence
								//o default
								else{
									//do nothing
								}
								//c default
							//c branch.on.files.void_flag
							//o branch.on.files.trxtype
								//o sequence.handling.for.SPAN.GAJI
								if("2".equalsIgnoreCase(Files2.getTrxType())){
									//o map
									Files2.setCreateDateString(PubUtil.dateTimeFormat(Files2.getCreateDate(), "yyyy-MM-dd HH:mm:ss", "yyyyMMdd"));
									oriDocDate = Files2.getDocumentDate();
									Files2.setDocumentDate(PubUtil.dateTimeFormat(Files2.getDocumentDate(), "yyyy-MM-dd", "yyyyMMdd"));
									nextDay = BusinessUtil.addDate("yyyy-MM-dd HH:mm:ss", currentDate, "1", null, null, null, null, null, "yyyyMMdd");
									
									//c map
									//o branch
										//o branch
										if("1".equalsIgnoreCase(Files2.getPayrollType())
												|| "3".equalsIgnoreCase(Files2.getPayrollType())){
											//o sequence
											if(nextDay.equalsIgnoreCase(Files2.getDocumentDate())){
												//o mandiri.span.db.service:updateSPAN
//												String sql = "update span_data_validation set proc_status = 'S' where proc_status = 'X' and debit_account_type = 'GAJI' and payroll_type in ('1','3') and document_date = '"+oriDocDate+"'";
												List<String> param = Arrays.asList("1", "3");
//												SimpleDateFormat sdfSCdocDate = new SimpleDateFormat("yyyy-MM-dd");
												String hql = "UPDATE SpanDataValidation SET procStatuc = 'S' WHERE procStatuc = 'X' AND debitAccountType = 'GAJI' AND payrollType IN(:param) AND documentDate = :oriDocDate " ;
												Query query = dao.getQuery(hql);
												query.setParameter("param", param);
												query.setString("oriDocDate", oriDocDate);
												UpdateSpanResponse updateSpanResponse = Service.updateSPAN(query);
												Status = updateSpanResponse.getStatus();
												ErrorMessage = updateSpanResponse.getErrorMessage();
												//c mandiri.span.db.service:updateSPAN
												//o debug
												System.out.println(PubUtil.concat("[Execute Req] Update proc status S is ", Status, "|| ", ErrorMessage));
												//c debug
												//o exit.flow
												response.setStatus(Status);
												response.setErrorMessage(ErrorMessage);
												return response;
												//c exit.flow
												
											}
											//c sequence
											//o default.map
											else{
												//do nothing
											}
											//c default.map
										}
										//c branch
										//o default.map
										else{
											//do nothing
										}	
										//c default.map
									//c branch
								}
								//c sequence.handling.for.SPAN.GAJI
								//o default.map
								else{
									//do nothing
								}
								//c default.map
							//c branch.on.files.trxtype	
							//o map
								//do nothing
							//c map	
							//o sequence.update.status.file
								//o branch
									//o sequence.update.proc.status
									if("1".equalsIgnoreCase(Files2.getExtractFileFlag())
											&& "1".equalsIgnoreCase(Files2.getExecFileFlag())){
										//o branch.on.files.trxtype
										String sqlUpdate = null;
											//o 2.sequence.handilng.for.span.gaji
											
											if("2".equalsIgnoreCase(Files2.getTrxType())){
												//o branch
													//o map
													if(SCDate.equalsIgnoreCase(Files2.getDocumentDate())){
//														sqlUpdate = "UPDATE SPAN_DATA_VALIDATION SET PROC_STATUS = '2', PROC_DESCRIPTION = 'Data send to host', UPDATE_DATE = sysdate, TRXTYPE='1' WHERE EXTRACT_FILE_FLAG = '1' AND EXEC_FILE_FLAG = '1' AND FILE_NAME = '"+Files2.getFileName()+"'";
														sqlUpdate = "UPDATE SpanDataValidation SET procStatuc = '2', procDescription = 'Data send to host', updateDate = '"+sdf.format(new Date())+"', trxType='1' WHERE extractFileFlag = '1' AND execFileFlag = '1' AND fileName = '"+Files2.getFileName()+"' ";
													}
													//c map
													//o map
													else if(nextDay==Files2.getDocumentDate()){
														sqlUpdate = null;
													}
													//c map
													
												//c branch
											}
											//c 2.sequence.handilng.for.span.gaji
											//o default.map
											else{
												sqlUpdate = "UPDATE SpanDataValidation SET procStatuc = '2', procDescription = 'Data send to host', updateDate = '"+sdf.format(new Date())+"' WHERE extractFileFlag = '1' AND execFileFlag = '1' AND fileName = '"+Files2.getFileName()+"'";
												System.out.println("Proc Status Update : 2");
											}
											//c default.map		
													
										//c branch.on.files.trxtype
										//o branch.on.sqlupdate
											//o mandiri.span.db.service:updateSPAN
											if(sqlUpdate!=null){
												Query query = dao.getQuery(sqlUpdate);
//												query.setDate("updateDate", new Date());
//												query.setString("fileName", Files2.getFileName());
												
												UpdateSpanResponse updateSpanResponse = Service.updateSPAN(query);
												response.setStatus(updateSpanResponse.getStatus());
												response.setErrorMessage(updateSpanResponse.getErrorMessage());
											}
											//c mandiri.span.db.service:updateSPAN
											//o null.map
											else if(sqlUpdate==null){
												//do nothing
											}
											//c null.map
										//c branch.on.sqlupdate	
									}
									//c sequence
									//o default.map
									else{
										//do nothing
									}
									//c default.map
								//c branch
							//c sequence.update.status.file	
						}
					}
				//c loop.over.files
				
			} catch (Exception e) {
				Log.debugLog("SPAN", "executeDataReq", "INFO", "Error Scheduller ="+e.toString());
				e.printStackTrace();
			}
		//c sequence
		System.out.println("End Run ExecuteDataReq");
		return response;
	}
	
	/**
	 * mandiri.span.scheduller.odbc.request:insertBulkFT
	 */
	public static InsertBulkFtResponse insertBulkFt(){
		InsertBulkFtResponse response = new InsertBulkFtResponse();
		String Status = null;
		String ErrorMessage = null;
		
		String totalRec = null;
		String totalAmount = null;
		String BATCHID =null;
		String TRXHEADERID = null;
		String RETRY_STATUS = null;
		String RETUR_STATUS = null;
		String SUCCESS_STATUS = null;
		String CREATE_DATE = null;
		String SUM_STATUS_RTY = null;
		String SUM_STATUS_RTU = null;
		String TRXTYPE = null;
		//o map
		String channelID = "18";
		String debitAcctSC = Odbc.selectMdrCfgMain(MdrCfgMainConstant.SPAN_DEPKEU_DEBIT_ACCOUNT_PARAM_ID).getValue1();
		//c map
		
		//o mandiri.span.db.adapter.odbc:selectSPANDataValidationStatusByTrxType
		List<SpanDataValidation> results = Odbc.selectSPANDataValidationStatusByTrxType("2", "0", "1");
		//c mandiri.span.db.adapter.odbc:selectSPANDataValidationStatusByTrxType

		//o loop.over.results
		if(results!=null){
			for(SpanDataValidation results2 : results){
				//o map.disabled
				/*totalRec = results2.getTotalRecord();
				totalAmount = results2.getTotalAmount();*/
				//c map.disabled
				
				//o mandiri.span.util:checkingMVATransaction
//				BusinessUtil.checkingMVATransaction(results2.getBatchId());
				//c mandiri.span.util:checkingMVATransaction
				//o mandiri.span.util:checkingAmountTrx
				CheckingAmountTrxRequest checkingAmountTrxRequest = new CheckingAmountTrxRequest();
				checkingAmountTrxRequest.setBatchID(results2.getBatchId());
				checkingAmountTrxRequest.setTrxHeaderID(results2.getTrxHeaderId());
				checkingAmountTrxRequest.setFileName(results2.getFileName());
				checkingAmountTrxRequest.setTrxType(results2.getTrxType());
				checkingAmountTrxRequest.setTotalRecord(results2.getTotalRecord());
				checkingAmountTrxRequest.setTotalAmount(results2.getTotalAmount());
				checkingAmountTrxRequest.setTotalRecSendToHost(results2.getTotalSendToHost());
				checkingAmountTrxRequest.setTotalAmtSendToHost(results2.getTotalAmountSendToHost());
				checkingAmountTrxRequest.setWaitStatus(results2.getWaitStatus());
				checkingAmountTrxRequest.setRetryStatus(results2.getRetryStatus());
				checkingAmountTrxRequest.setReturStatus(results2.getReturStatus());
				checkingAmountTrxRequest.setLegStatus(String.valueOf(results2.getLegStatus()));
				
				totalAmount = BusinessUtil.checkingAmountTrx(checkingAmountTrxRequest);
				//c mandiri.span.util:checkingAmountTrx
				//o branch.on.totalAmountOut
					//o 0.sequence
					if("0".equalsIgnoreCase(totalAmount)){
						SpanInputRequest request = new SpanInputRequest();
						request.setFileName(results2.getFileName());
						List<SpanInputRequest> spanInputRequests = new ArrayList<SpanInputRequest>();
						spanInputRequests.add(request);
						SpanReportResponse spanReportResponse = Report.spanReport(spanInputRequests);
						Status = spanReportResponse.getStatus();
						ErrorMessage = spanReportResponse.getErrorMessage();
					}
					//c 0.sequence
					//o default
					else{
						try {
							//o branch
								//o 1.sequence
								if("1".equalsIgnoreCase(results2.getScSettlement())){
									//o mandiri.span.db.service.sc:getSCFirstData
									GetSCFirstDataResponse getSCFirstDataResponse = mii.span.db.service.SC.getSCFirstData(results2.getFileName(), 
											"1", "SC");
									BATCHID = getSCFirstDataResponse.getBATCHID();
									TRXHEADERID = getSCFirstDataResponse.getTRXHEADERID();
									RETRY_STATUS = getSCFirstDataResponse.getRETRY_STATUS();
									RETUR_STATUS = getSCFirstDataResponse.getRETUR_STATUS();
									SUCCESS_STATUS = getSCFirstDataResponse.getSUCCESS_STATUS();
									CREATE_DATE = getSCFirstDataResponse.getCREATE_DATE();
									SUM_STATUS_RTY = getSCFirstDataResponse.getSUM_STATUS_RTY();
									SUM_STATUS_RTU = getSCFirstDataResponse.getSUM_STATUS_RTU();
									TRXTYPE = getSCFirstDataResponse.getTRXTYPE();
									//c mandiri.span.db.service.sc:getSCFirstData
									//o map
									results2.setBatchId(BATCHID);
									results2.setTrxHeaderId(TRXHEADERID);
									//c map
								}
								//c 1.sequence
								//o mandiri.span.db.adapter.odbc:spanHostRequest
								SpanHostRequestResponse spanHostRequestResponse = Odbc.spanHostRequest(results2.getBatchId(), 
										results2.getTrxHeaderId(), "1", "1", channelID, results2.getFileName(), debitAcctSC, results2.getScSettlement());
								Status = spanHostRequestResponse.getStatus();
								ErrorMessage = spanHostRequestResponse.getErrorMessage();
								//c mandiri.span.db.adapter.odbc:spanHostRequest
								//o mandiri.span.log:debugLog
								Log.debugLog("SPAN", "insertBulkFT", "INFO", PubUtil.concat("Insert into host with Status : ",Status," ErrorMessage : ",ErrorMessage," for file name : ",results2.getFileName()));
								//c mandiri.span.log:debugLog
							//c branch
						} 
						catch (Exception e) {
							e.printStackTrace();
						}
					}
					//c default
				
				//c branch.on.totalAmountOut
			}
		}
		//c loop.over.results
		
		response.setStatus(Status);
		response.setErrorMessage(ErrorMessage);
		return response;
	}
	
	public static void main(String[] args) {
		String a = "0";
		rejected:
		if(true){
			System.out.println(a);
			if(true){
				a="1";
				break rejected;
			}
			System.out.println("asa");
		} //rejected hanya untuk if pertama
		if(true){
			System.out.println("break from rejected");
			System.out.println(a);
		}
		
	}
}
