package mii.span.scheduller.odbc;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import mii.span.atomic.response.odbc.Ack;
import mii.span.process.model.SentAckDataFileRequest;
import mii.span.process.model.SentAckDataFileResponse;
import mii.span.util.PubUtil;

import com.mii.constant.Constants;
import com.mii.dao.SpanHostDataDetailsDao;
import com.mii.dao.SystemParameterDAO;
import com.mii.models.SystemParameter;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.ApplicationInstance;
import com.mii.span.model.SPANDataValidation;

@ManagedBean
@ViewScoped
public class ACKLister implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3322224070304917646L;
	private List<SPANDataValidation> tempNowACKList;

	@ManagedProperty(value = "#{applicationBean}")
	private static ApplicationBean applicationBean;

	private SpanHostDataDetailsDao getSpanHostDataDetailsDao(){
		applicationBean = ApplicationInstance.getInstance().getApplicationBean();
		return (SpanHostDataDetailsDao) applicationBean.getFileProcessContext().getBean("spanHostDataDetailsDao");
	}

	public void fillACKTempList(){
		tempNowACKList = getSpanHostDataDetailsDao().getACKDataTempNow();
	}
	
	private static SystemParameterDAO getSystemParameterDaoBean(){
		applicationBean = ApplicationInstance.getInstance().getApplicationBean();
		return (SystemParameterDAO) applicationBean.getFileProcessContext().getBean("sysParamDAO");
	}

	public void autoSendAck(){
		System.out.println("Send ACK For Normal Transaction");
		tempNowACKList = new ArrayList<SPANDataValidation>();
		List<String> inputList = new ArrayList<String>();
		fillACKTempList();
		//o mandiri.span.atomic.response.odbc.ack:sentACKDataFile
		String ackDataFile = "";
		for (Iterator iterator = tempNowACKList.iterator(); iterator.hasNext();) {
			SPANDataValidation type = (SPANDataValidation) iterator.next();
			ackDataFile = ackDataFile.concat(constructAckString(type.getDocumentDate(), type.getDocNumber(), type.getFileName())).concat("\n");
			inputList.add(type.getDocNumber());
		}
		System.out.println("Total Record on ACK : "+inputList.size());
		if(tempNowACKList.size()>0){
			String documentType = "SP2D";
			SentAckDataFileRequest sentAckDataFileRequest = new SentAckDataFileRequest();
			sentAckDataFileRequest.setFilename(constructFilename());
			sentAckDataFileRequest.setDocumentType(documentType);
			sentAckDataFileRequest.setErrorMessage("SUCCESS");
			sentAckDataFileRequest.setErrorCode("00");
			sentAckDataFileRequest.setAckDataString(ackDataFile);

			SentAckDataFileResponse sentAckDataFileResponse = Ack.sentACKDataFile(sentAckDataFileRequest);
			System.out.println(sentAckDataFileResponse.toString());
			
			//Update ACK Flag
			System.out.println("Start Update ACK Flag");
			getSpanHostDataDetailsDao().updateACKFlag(inputList);
			System.out.println("End Update ACK Flag");
		}
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		//Send ACK Retur Response
		System.out.println("Send ACK For Retur Transaction");
		List<SPANDataValidation> ackReturList = new ArrayList<SPANDataValidation>();
		ackReturList = getSpanHostDataDetailsDao().getACKReturDataNow();
		String ackDataFileRetur = "";
		List<String> inputListRetur = new ArrayList<String>();
		for(SPANDataValidation spdv : ackReturList){
			ackDataFileRetur = ackDataFileRetur.concat(constructAckStringForRetur(
					spdv.getDocumentDate(), spdv.getDocNumber(), spdv.getFileName(), spdv.getResponseCode(), spdv.getErrorMessage())).concat("\n");
			inputListRetur.add(spdv.getDocNumber());
		}
		if(ackReturList.size()>0){
			String documentType = "SP2D";
			SentAckDataFileRequest sentAckDataFileRequest = new SentAckDataFileRequest();
			sentAckDataFileRequest.setFilename(constructFilename());
			sentAckDataFileRequest.setDocumentType(documentType);
			/*sentAckDataFileRequest.setErrorMessage("SUCCESS");
			sentAckDataFileRequest.setErrorCode("00");*/
			sentAckDataFileRequest.setAckDataString(ackDataFileRetur);

			SentAckDataFileResponse sentAckDataFileResponse = Ack.sentACKDataFile(sentAckDataFileRequest);
			System.out.println(sentAckDataFileResponse.toString());
			
			//Update ACK Flag
			System.out.println("Start Update ACK Flag for Retur");
			getSpanHostDataDetailsDao().updateACKFlag(inputListRetur);
			System.out.println("End Update ACK Flag for Retur");
		}
		
	}

	private String constructFilename() {
		Timestamp now = new Timestamp(Calendar.getInstance().getTime().getTime());
		String bankCode = getSystemParameterDaoBean().getValueByParamName(Constants.KODE_BANK_12_DIGIT);
		String sp2d = "SP2D";
		String fa = "FA";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
		String dateTime = sdf.format(now);
		String ext = ".jar";
		return bankCode+"_"+sp2d+"_"+fa+"_"+dateTime+ext;
	}

	private String constructAckString(String docDate,String docNumber,String fileName){
		String documentDate = PubUtil.padRight(docDate, 10);
		String documentType = "SP2D";
		String documentNumber = PubUtil.padRight(docNumber, 21);
		String filename = PubUtil.padRight(fileName,100);
		String returnCode = PubUtil.padLeft("00", "0", 4) ;
		String description = PubUtil.padRight("SUCCESS", 200);
		return documentDate+"|"+documentType+"|"+documentNumber+"|"+filename+"|"+returnCode+"|"+description;
	}
	
	private String constructAckStringForRetur(String docDate,String docNumber,String fileName,String responseCode, String errorMessage){
		String documentDate = PubUtil.padRight(docDate, 10);
		String documentType = "SP2D";
		String documentNumber = PubUtil.padRight(docNumber, 21);
		String filename = PubUtil.padRight(fileName,100);
		String returnCode = PubUtil.padLeft(responseCode, "0", 4) ;
		String description = PubUtil.padRight(errorMessage, 200);
		return documentDate+"|"+documentType+"|"+documentNumber+"|"+filename+"|"+returnCode+"|"+description;
	}
}
