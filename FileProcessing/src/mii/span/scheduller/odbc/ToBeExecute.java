package mii.span.scheduller.odbc;

import java.io.Serializable;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedProperty;

import mii.span.model.SelectFNDataValidationOutput;
import mii.span.model.SpanAccountBalanceLog;
import mii.span.model.SpanDataToBeExecute;
import mii.span.model.ToBeExecuteInput;
import mii.span.model.ToBeExecuteOutput;
import mii.span.model.helper.Files;

import com.mii.constant.Constants;
import com.mii.dao.SpanVoidDataTrxDao;
import com.mii.dao.SystemParameterDAO;
import com.mii.dao.ToBeExecuteDao;
import com.mii.json.AccountBalance;
import com.mii.models.SystemParameter;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.ApplicationInstance;
import com.mii.span.dao.SPANDataValidationDao;
import com.mii.span.handler.MonitoringVoidDepkeuHandler;
import com.mii.span.model.SpanSp2dVoidLog;
import com.mii.span.process.VoidDocumentNo;

public class ToBeExecute implements Serializable{
	private static final long serialVersionUID = 1L;
	@ManagedProperty(value = "#{applicationBean}")
	private static ApplicationBean applicationBean;
	
	public void run(String accountType){
		System.out.println("Start Run To Be Execute");
		
		//Generate Input
		SystemParameter systemParameter =  getSystemParameterDaoBean().getValueParameter(Constants.PARAM_DEBITACCNO+accountType).get(0);
		List<SpanDataToBeExecute> sdtbeList = getToBeExecuteDaoBean().selectAllDataToBeExecuted(systemParameter.getParam_value());
		
		BigInteger totalAmount = new BigInteger("0");
		List<Files> fileList = new ArrayList<Files>();
		for(SpanDataToBeExecute sdtbe : sdtbeList){
			totalAmount = totalAmount.add(new BigInteger(sdtbe.getTotalAmount()));
			Files files = new Files();
			String fileName = sdtbe.getFileName();
			files.setFileName(fileName);
			files.setRetryNumber("3");
			files.setPayRollType("N");
			files.setAsynchronous("N");
			files.setGlAccount("0");
			files.setScdate("");
			files.setSettlement("N");
			fileList.add(files);
			
			//=================================scheduller auto void
			List<SpanSp2dVoidLog> spanSp2dVoidLogs = VoidDocumentNo.getAllSpanSp2dVoidLogs(getSpanVoidDataTrxDAOBean());
			doVoid(fileName, spanSp2dVoidLogs, getSPANDataValidationDao());
			//=====================================================
			
		}
		ToBeExecuteInput input = new ToBeExecuteInput();
		input.setExeType("0");
		input.setTrxType("0");
		input.setAccount(systemParameter.getParam_value());
		input.setFiles(fileList);
		input.setSelectedAmount(totalAmount.toString());
		
		//To Be Execute
		toBeExecute(input, accountType);
		
		System.out.println("End Run To Be Execute");
	}
	
	private static SpanVoidDataTrxDao getSpanVoidDataTrxDAOBean(){
		return (SpanVoidDataTrxDao) applicationBean.getCorpGWContext().getBean("SPANVoidDataTrxDao");
	}
	
	private SPANDataValidationDao getSPANDataValidationDao(){
		return (SPANDataValidationDao) applicationBean.getCorpGWContext().getBean("SPANDataValidationDao");
	}

	private static SystemParameterDAO getSystemParameterDaoBean(){
		applicationBean = ApplicationInstance.getInstance().getApplicationBean();
		return (SystemParameterDAO) applicationBean.getFileProcessContext().getBean("sysParamDAO");
	}
	
	private static ToBeExecuteDao getToBeExecuteDaoBean(){
		applicationBean = ApplicationInstance.getInstance().getApplicationBean();
		return (ToBeExecuteDao) applicationBean.getFileProcessContext().getBean("toBeExecuteDao");
	}

	public static ToBeExecuteOutput toBeExecute(ToBeExecuteInput input, String accountType) {

		// branch on exeType
		switch (input.getExeType()) {
		case "1":
			// do nothing
			break;

		default:
			input.setExeType("0");
			break;
		}

		// sequence
		try {
			/**
			 * Service odbc inquirySaldo with input account mapping output
			 * to variable [availableBalance, L2TotalAmount, availableBalance2,
			 * Status, ErrorMessage]
			 */
			
			//Prepare bypass validation,, comment this.....
//			String availableBalance2 = getSaldo(input.getAccount());
//			System.out.println("Available Balance :" + availableBalance2+" vs "+input.getSelectedAmount());
			String Status = "";
//			// branch
//			try{
//				if (Integer.parseInt(availableBalance2) < Integer.parseInt(input
//						.getSelectedAmount())) {
//					throw new Exception("Not Enough Balance for Posting Transaction.");
//				} else if (availableBalance2.equalsIgnoreCase("ERROR")) {
//					throw new Exception("Error While Inquiry Saldo.");
//				} else if (Integer.parseInt(availableBalance2) >= Integer
//						.parseInt(input.getSelectedAmount())) {
					Status = "SUCCESS"; //But not this
//					insertToAccountBalLogs(accountType, availableBalance2);
//				}
//			}catch(Exception e){
//				e.printStackTrace();
//				throw new Exception("Exit To Be Execute");
//			}
			//.....Until this

			// branch on status
			if (Status.equals("SUCCESS")) {
				// execute by fileName
				if (input.getExeType().equals("0")) {
					/**
					 * Method odbc executeByFilename
					 */
					System.out.println("Execute By Filename");
					executeByFileName(input.getFiles());
				}
				// execute by docNumbers
				else if (input.getExeType().equals("1")) {
					/**
					 * TODO method odbc executeByDocNumber
					 */
					System.out.println("Execute By Doc Number");
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		ToBeExecuteOutput output = new ToBeExecuteOutput();

		return output;
	}
	
	private void doVoid(String fileName, List<SpanSp2dVoidLog> spanSp2dVoidLogs, SPANDataValidationDao spanDataValidationDao) {
		List<String> allDocumentNumberDataDetails = VoidDocumentNo.getAllDocumentNumberDataDetailsByFilename(fileName, spanDataValidationDao);

		List<String> alreadyVoids = new ArrayList<String>();
		for (SpanSp2dVoidLog spanSp2dVoidLog : spanSp2dVoidLogs) {
			String sp2dNo = spanSp2dVoidLog.getSp2dNo();
			alreadyVoids.add(sp2dNo);
		}
		
		MonitoringVoidDepkeuHandler monitoringVoidDepkeuHandler = new MonitoringVoidDepkeuHandler();
		
		for (String allDocumentNumberDataDetail : allDocumentNumberDataDetails) {
			if(alreadyVoids.contains(allDocumentNumberDataDetail.substring(0, 15))){
				monitoringVoidDepkeuHandler.doVoidDocumentNumberByScheduller(allDocumentNumberDataDetail);
			}
		}
	}

	private static void insertToAccountBalLogs(String accountType, String balance) {
		SpanAccountBalanceLog spanAccountBalanceLog = new SpanAccountBalanceLog();

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		spanAccountBalanceLog.setInquiryDate(sdf.format(new Date()));
		spanAccountBalanceLog.setBeginBalance(balance);
		spanAccountBalanceLog.setAccountType(accountType);
		
		boolean checked = false;
		int count = getToBeExecuteDaoBean().countFirstAvailableBalance(accountType);
		if(count>0){
			checked = true;
		}
		if(!checked){
			try{
				getToBeExecuteDaoBean().save(spanAccountBalanceLog);
			}catch(Exception e){
				System.out.println("Available Balance Log Allready Exist.");
			}
		}else{
			System.out.println("Available Balance Log Allready Exist.");
		}
	}

	public static ToBeExecuteOutput executeByFileName(List<Files> files) {

		ToBeExecuteOutput output = new ToBeExecuteOutput();
		try {
			// map -- get today date
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String todayDate = sdf.format(java.util.Calendar.getInstance().getTime());

			// map -- get next day -- Update Using Calendar
			Calendar cal = Calendar.getInstance();
			Date date = sdf.parse(todayDate);
			cal.setTime(date);
			cal.add(Calendar.DATE, 1);
			String nextDayDate = sdf.format(cal.getTime());
			System.out.println("Size : "+files.size());
			// loop
			for (Files f : files) {
				try {
					// sequence checking parameter
					// branch on Files/payrolltype
					if (f.getPayRollType().equals(null)) {
						f.setPayRollType("N");
					} else {
						f.setPayRollType("N");
					}

					// branch on Files/asynchronous
					if (f.getAsynchronous().equals(null)) {
						f.setAsynchronous("N");
					} else {
						f.setAsynchronous("N");
					}

					// branch on Files/glaccount
					if (f.getGlAccount().equals(null)) {
						f.setGlAccount("0");
					} else {
						f.setGlAccount("0");
					}

					// branch on Files/scdate
					if (f.getScdate().equals(null)) {
						f.setScdate("");
					} else {
						f.setScdate("");
					}

					// branch on Files/settlement
					if (f.getSettlement().equals(null)) {
						f.setSettlement("N");
					} else {
						f.setSettlement("N");
					}

					// branch on Files/retrynumber
					if (f.getRetryNumber().equals(null)) {
						f.setRetryNumber("3");
					} else {
						f.setRetryNumber("3");
					}

				} catch (Exception e) {
					e.printStackTrace();
				}

				/**
				 * Querying (selectFNDataValidation) with input filename
				 * mapping the output to variable [batchId, procStatus, trxType,
				 * docDate]
				 */
				SelectFNDataValidationOutput selectFNDataValidationOutput = getToBeExecuteDaoBean().selectFNDataValidation(f.getFileName());
				f.setBatchId(selectFNDataValidationOutput.getBatchId());
				f.setProcStatus(selectFNDataValidationOutput.getProcStatus());
				f.setTrxType(selectFNDataValidationOutput.getTrxType());
				f.setDocdate(selectFNDataValidationOutput.getDocumentDate());
				
				String SCFlag = "";
				
				if (f.getTrxType().equals("2")) {
					if (f.getDocdate().equals(nextDayDate)){
						SCFlag = "1";
					}
					else if (f.getDocdate().equals(todayDate)) {
						SCFlag = "0";
					}
					else {
						break;
					}
				}
				else {
					if (f.getDocdate().equals(todayDate)) {
						SCFlag = "0";
					}
					else {
						break;
					}
				}
				
				/**
				 * Querying SP (spanExecByFilename) with input
				 * [fileName, scFlag, glAccount, asynchronous, payrolltype
				 * retryNo, settlement, scDate]
				 * mapping the output to variable Status, ErrorMessage
				 */
				ToBeExecuteOutput toBeExecuteOutput = getToBeExecuteDaoBean().spanExecByFilename(f.getFileName(), SCFlag, f.getGlAccount(), f.getAsynchronous()
						, f.getPayRollType(), f.getRetryNumber(), f.getSettlement(), f.getScdate());
				output = toBeExecuteOutput;
				System.out.println("OK : "+output.getStatus()+output.getErrorMessage());
			}

		} catch (Exception e) {
			e.printStackTrace();
			output.setStatus("SUCCESS");
			output.setErrorMessage(e.getMessage());
		}

		return output;
	}
	
	public static ToBeExecuteOutput executeByDocNumbers(List<Files> files) {

		ToBeExecuteOutput output = new ToBeExecuteOutput();
		String documentNoList = "";
		try {
			// map -- get today date
			String todayDate = new SimpleDateFormat("yyyy-MM-dd")
					.format(java.util.Calendar.getInstance().getTime());

			// map -- get next day
			Date date = new SimpleDateFormat().parse(todayDate);
			int day = date.getDay() + 1;
			date.setDate(day);
			String nextDayDate = new SimpleDateFormat("yyyy-MM-dd")
					.format(date);

			// loop
			for (Files f : files) {
				try {
					// sequence checking parameter
					// branch on Files/payrolltype
					if (f.getPayRollType().equals(null)) {
						f.setPayRollType("N");
					} else {
						f.setPayRollType("N");
					}

					// branch on Files/asynchronous
					if (f.getAsynchronous().equals(null)) {
						f.setAsynchronous("N");
					} else {
						f.setAsynchronous("N");
					}

					// branch on Files/glaccount
					if (f.getGlAccount().equals(null)) {
						f.setGlAccount("0");
					} else {
						f.setGlAccount("0");
					}

					// branch on Files/scdate
					if (f.getScdate().equals(null)) {
						f.setScdate("");
					} else {
						f.setScdate("");
					}

					// branch on Files/settlement
					if (f.getSettlement().equals(null)) {
						f.setSettlement("N");
					} else {
						f.setSettlement("N");
					}

					// branch on Files/retrynumber
					if (f.getRetryNumber().equals(null)) {
						f.setRetryNumber("3");
					} else {
						f.setRetryNumber("3");
					}

				} catch (Exception e) {
					e.printStackTrace();
				}

				/**
				 * TODO querying (selectFNDataValidation) with input filename
				 * mapping the output to variable [batchId, procStatus, trxType,
				 * docDate, execFileFlag]
				 */
				f.setBatchId(""); // FIXME with query result
				f.setProcStatus(""); // FIXME with query result
				f.setTrxType(""); // FIXME with query result
				f.setDocdate(""); // FIXME with query result
				f.setExecFileFlag(""); // FIXME with query result

				if (f.getTrxType().equals("1")) {
					f.setTrxType("2");
				}
				
				if (f.getTrxType().equals("2")) {
					if (f.getDocdate().equals(nextDayDate)){
						String SCFlag = "1";
					}
					else if (f.getDocdate().equals(todayDate)) {
						String SCFlag = "0";
					}
					else {
						break;
					}
					
					// branch on exedFileFlag
					if (f.getExecFileFlag().equals("1")){
						// loop
						for(int i = 1; i<=f.getDocumentNumbers().size(); i++){
							if (i == 1){
								documentNoList = "'"+ f.getDocumentNumbers().get(i) + "'";
							}
							else {
								documentNoList += documentNoList + ",'"+f.getDocumentNumbers().get(i)+"'";
							}
							documentNoList = "("+documentNoList+")";
						}
					}
					else {
						// loop
						for(int i = 1; i<=f.getDocumentNumbers().size(); i++){
							if (i == 1){
								documentNoList = f.getDocumentNumbers().get(i);
							}
							else {
								documentNoList += documentNoList + "|"+f.getDocumentNumbers().get(i);
							}
						}
					}
				}
				else {
					if (f.getDocdate().equals(todayDate)) {
						String SCFlag = "0";
					}
					else {
						break;
					}
					// loop
					for(int i = 1; i<=f.getDocumentNumbers().size(); i++){
						if (i == 1){
							documentNoList = f.getDocumentNumbers().get(i);
						}
						else {
							documentNoList += documentNoList + "|"+f.getDocumentNumbers().get(i);
						}
					}
				}
				
				/**
				 * TODO querying (spanExecByDocNumbers) with input
				 * [fileName, documentNumbersList, scFlag, glAccount, asynchronous, payrolltype
				 * retryNo, settlement, scDate]
				 * mapping the output to variable Status, ErrorMessage
				 */

			}

		} catch (Exception e) {
			output.setStatus("SUCCESS");
			output.setErrorMessage(e.getMessage());
		}

		return output;
	}
	
	public static String getSaldo(String accountNo){
		String balance = AccountBalance.getAccountBalance(getSystemParameterDaoBean(), accountNo);
		return balance;
	}
}
