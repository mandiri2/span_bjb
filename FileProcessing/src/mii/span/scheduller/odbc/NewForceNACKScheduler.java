package mii.span.scheduller.odbc;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedProperty;

import com.mii.constant.Constants;
import com.mii.dao.SpanHostDataDetailsDao;
import com.mii.dao.SpanHostDataFailedDao;
import com.mii.dao.SpanHostResponseDao;
import com.mii.dao.SpanRecreateFileRejectedDao;
import com.mii.dao.SystemParameterDAO;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.ApplicationInstance;
import com.mii.span.dao.SPANDataValidationDao;
import com.mii.span.handler.ResultToBeProcessHandler;
import com.mii.span.model.SpanDataSummary;
import com.mii.span.process.GetSPANSummariesDetail;
import com.mii.span.process.io.GetSPANSummariesDetailRequest;
import com.mii.span.process.io.GetSPANSummariesDetailResponse;
import com.mii.span.process.io.helper.DetailsOutputList;

import mii.span.db.Service;
import mii.span.model.SpanDataValidation;
import mii.span.model.SpanHostDataDetails;
import mii.span.model.SpanHostDataFailed;
import mii.span.model.SpanHostResponse;
import mii.span.model.SpanRecreateFileRejected;
import mii.span.model.SpanRejectedFileHis;
import mii.span.model.helper.CreateRFOutput;
import mii.span.util.BusinessUtil;
import mii.span.util.GetCurrentDateTime;
import mii.span.util.PubUtil;

public class NewForceNACKScheduler {
	
	@ManagedProperty(value = "#{applicationBean}")
	private static ApplicationBean applicationBean;
	
	private static SystemParameterDAO getSystemParameterDaoBean(){
		applicationBean = ApplicationInstance.getInstance().getApplicationBean();
		return (SystemParameterDAO) applicationBean.getFileProcessContext().getBean("sysParamDAO");
	}
	
	private static SPANDataValidationDao getSpanDataValidationDaoBean(){
		applicationBean = ApplicationInstance.getInstance().getApplicationBean();
		return (SPANDataValidationDao) applicationBean.getFileProcessContext().getBean("SPANDataValidationDao");
	}
	
	private static SpanRecreateFileRejectedDao getRecreateFileRejectedDaoBean(){
		applicationBean = ApplicationInstance.getInstance().getApplicationBean();
		return (SpanRecreateFileRejectedDao) applicationBean.getFileProcessContext().getBean("spanRecreateFileRejectedDao");
	}
	
	private static SpanHostDataDetailsDao getSpanHostDataDetailsDaoBean(){
		applicationBean = ApplicationInstance.getInstance().getApplicationBean();
		return (SpanHostDataDetailsDao) applicationBean.getFileProcessContext().getBean("spanHostDataDetailsDao");
	}
	
	private static SpanHostResponseDao getSpanHostResponseDaoBean(){
		applicationBean = ApplicationInstance.getInstance().getApplicationBean();
		return (SpanHostResponseDao) applicationBean.getFileProcessContext().getBean("spanHostResponseDao");
	}
	
	private static SpanHostDataFailedDao getSpanHostDataFailedDaoBean(){
		applicationBean = ApplicationInstance.getInstance().getApplicationBean();
		return (SpanHostDataFailedDao) applicationBean.getFileProcessContext().getBean("spanHostDataFailedDao");
	}
	
	
	public void run(){
		System.out.println("Start NewForceNACKScheduler");
		go(Constants.GAJI);
		go(Constants.GAJIR);
		System.out.println("End NewForceNACKScheduler");
	}

	private void go(String tabSelected) {
		//Init load Data
		List<DetailsOutputList> detailList = loadData(tabSelected);
		String suffix  = "0";
		
		for (DetailsOutputList output : detailList) {
			SpanHostDataDetails spanHostDataDetails = getSpanHostDataDetailsDaoBean().getLastSpanHostDataDetailsByDocNumber(output.getDocNumber());
			SpanDataValidation spdv = getSpanDataValidationDaoBean().getSpanDataValidationBySpanHostDataDetails(
					spanHostDataDetails.getMESSAGE_ID().replace(".xml", ".jar"));
			forceRejectRetryTransaction(spanHostDataDetails, spdv);
			String newFileName = "";
			CreateRFOutput rf = createRF(suffix, spdv.getFileName());
			suffix = rf.getSuffix();
			newFileName = rf.getNewFileName();
			
			forceInsertRFToSpanDataValidation(spdv, newFileName, spanHostDataDetails);
			forceInsertRFToSpanRecreateFileRejected(spdv, newFileName, spanHostDataDetails);
			forceInsertRFToSpanRejectedFileHis(spdv, output);
		}
	}

	private void forceInsertRFToSpanRejectedFileHis(SpanDataValidation spdv, DetailsOutputList output) {
		SpanRejectedFileHis spanRejectedFileHis = getRecreateFileRejectedDaoBean().selectRejectedFileHisByFilename(spdv.getFileName());
		try{
			spanRejectedFileHis.setFILE_NAME(spdv.getFileName());
			BigDecimal amount = new BigDecimal(spanRejectedFileHis.getTOTAL_AMOUNT());
			amount = amount.add(new BigDecimal(output.getDebitAmount()));
			spanRejectedFileHis.setTOTAL_RECORD(Integer.toString(Integer.parseInt(spanRejectedFileHis.getTOTAL_RECORD())+1));
			spanRejectedFileHis.setTOTAL_AMOUNT(amount.toString());
			spanRejectedFileHis.setCREATE_DATE(GetCurrentDateTime.getCurrentDateString("dd-MMM-yy").toUpperCase());
		}catch(Exception e){
			System.out.println("Reject his not found, insert new Record.");
			spanRejectedFileHis = new SpanRejectedFileHis();
			spanRejectedFileHis.setFILE_NAME(spdv.getFileName());
			spanRejectedFileHis.setTOTAL_RECORD("1");
			spanRejectedFileHis.setTOTAL_AMOUNT(output.getDebitAmount());
			spanRejectedFileHis.setCREATE_DATE(GetCurrentDateTime.getCurrentDateString("dd-MMM-yy").toUpperCase());
		}
		Service.insertSpanRejectedFileHis(spanRejectedFileHis);	
	}

	private List<DetailsOutputList> loadData(String tabSelected) {
		List<SpanDataSummary> fileList = new ArrayList<SpanDataSummary>();
		ResultToBeProcessHandler rtbph = new ResultToBeProcessHandler(); 
		rtbph.setTabSelected(tabSelected);
		rtbph.doClickRetry();
		fileList = rtbph.getSummaryList();
		List<DetailsOutputList> resultListFinal = new ArrayList<DetailsOutputList>();
		for(SpanDataSummary s : fileList){
			List<DetailsOutputList> resultList = new ArrayList<DetailsOutputList>();
			resultList = loadDocNo(s.getFileName());
			resultListFinal.addAll(resultList);
		}
		return resultListFinal;
	}

	private List<DetailsOutputList> loadDocNo(String fileName) {
		GetSPANSummariesDetailRequest request = new GetSPANSummariesDetailRequest();
		request.setFilename(fileName);
		request.setClickOn("1"); //For Retry Only
		GetSPANSummariesDetailResponse getSPANSummariesDetailResponse = new GetSPANSummariesDetailResponse();
		List<DetailsOutputList> detailsOutputList = new ArrayList<DetailsOutputList>();
		try {
			getSPANSummariesDetailResponse = GetSPANSummariesDetail.getSPANSummariesDetail(request, getSpanDataValidationDaoBean(), 
						getSystemParameterDaoBean(), Constants.GAJI);
			detailsOutputList = getSPANSummariesDetailResponse.getDetailsOutputList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return detailsOutputList;
	}

	private static CreateRFOutput createRF(String suffix, String fileName) {
		CreateRFOutput output = new CreateRFOutput();
		String prefix = "RF";
		String[] fileNameDetails = PubUtil.tokenize(fileName, "_");
		suffix = PubUtil.padLeft(suffix, "0", 4);
		String rejectFileName = prefix+suffix;
		fileNameDetails[4] = rejectFileName;
		suffix = Integer.parseInt(suffix)+1+"";
		String newFileName = PubUtil.makeString(fileNameDetails, "_");
		output.setSuffix(suffix);
		output.setNewFileName(newFileName);
		return output;
	}

	private void forceRejectRetryTransaction(SpanHostDataDetails spanHostDataDetails, SpanDataValidation spdv) {
		updateResponseToRejectRC(spanHostDataDetails); //Update RC --> 77 on SpanHostResponse
		updateSpanDataFailed(spanHostDataDetails); //Update SpanDataFailed
		updateSpanDataValidation(spdv); //Update SpanDataValidation
	}

	private void updateSpanDataValidation(SpanDataValidation spdv) {
		spdv.setRetryStatus("1");
		spdv.setL1Status("SUCCESS");
		spdv.setUpdateDate(new Date());
		getSpanDataValidationDaoBean().save(spdv);
	}

	private void updateSpanDataFailed(SpanHostDataDetails spanHostDataDetails) {
		SpanHostDataFailed spanHostDataFailed = getSpanHostDataFailedDaoBean().getSpanHostDataFailed(spanHostDataDetails.getMESSAGE_ID().replace(".xml", ".jar"));
		spanHostDataFailed.setRetryStatus(spanHostDataFailed.getRetryStatus()-1);
		spanHostDataFailed.setSuccessStatus(spanHostDataFailed.getSuccessStatus()+1);
		spanHostDataFailed.setSumStatusRty((long) 1);
		getSpanHostDataFailedDaoBean().save(spanHostDataFailed);
	}

	private void updateResponseToRejectRC(SpanHostDataDetails spanHostDataDetails) {
		SpanHostResponse spanHostResponse = getSpanHostResponseDaoBean().getResponseByHostDataDetails(spanHostDataDetails.getBATCHID(),
				spanHostDataDetails.getTRXHEADERID(), spanHostDataDetails.getTRXDETAILID());
		spanHostResponse.setErrorCode("77");
		spanHostResponse.setResponseCode("77");
		spanHostResponse.setErrorMessage("Reject");
		spanHostResponse.setInstructionCode1("1");
		getSpanHostResponseDaoBean().save(spanHostResponse);
	}

	private void forceInsertRFToSpanDataValidation(SpanDataValidation spdv, String newFilename, SpanHostDataDetails spanHostDataDetails) {
		SpanDataValidation spanDataValidation = new SpanDataValidation();
		spanDataValidation.setFileName(newFilename); //Old filename di replace RF
		spanDataValidation.setBatchId(BusinessUtil.batchID()); //New Batch Id
		spanDataValidation.setTrxHeaderId(BusinessUtil.trxHeaderID()); //New Trx Header Id
		spanDataValidation.setSpanFnType(spdv.getSpanFnType()); //Old
		spanDataValidation.setDebitAccount(spdv.getDebitAccount()); //Old
		spanDataValidation.setDebitAccountType(spdv.getDebitAccountType()); //Old
		spanDataValidation.setDocumentDate(spdv.getDocumentDate()); //Old
		spanDataValidation.setTotalAmount(spanHostDataDetails.getDEBITAMOUNT()); // new
		spanDataValidation.setTotalRecord("1"); //new
		spanDataValidation.setProcStatuc("1"); //new
		spanDataValidation.setProcDescription("Rejected File, request from COP/DEPKEU"); // new
		spanDataValidation.setXmlFileName(generateNewXML(spanDataValidation, spanHostDataDetails, newFilename)); //new
		spanDataValidation.setResponseCode("005"); //new
		spanDataValidation.setAckFileName(null);
		spanDataValidation.setAckStatus(null);
		spanDataValidation.setAckDescription(null);
		spanDataValidation.setCreateDate(new Date());
		spanDataValidation.setUpdateDate(null);
		spanDataValidation.setRetryStatus("0");
		spanDataValidation.setReturStatus("0");
		spanDataValidation.setTrxType("0");
		spanDataValidation.setScFileName(null);
		spanDataValidation.setScTrxDetailIdStart(null);
		spanDataValidation.setScTrxDetailIdEnd(null);
		spanDataValidation.setAsynchronous(null);
		spanDataValidation.setScSettlement(null);
		spanDataValidation.setPayrollType(null);
		spanDataValidation.setGlAccount(null);
		spanDataValidation.setRetryNumberL1(0);
		spanDataValidation.setScDate(null);
		spanDataValidation.setForcedAck("0");
		spanDataValidation.setRetryNumberL2(0);
		spanDataValidation.setLegStatus(0);
		spanDataValidation.setL1Status(null);
		spanDataValidation.setL2Status(null);
		spanDataValidation.setFtpStatus("0");
		spanDataValidation.setChangeStatus(null);
		spanDataValidation.setTotalSendToHost(null);
		spanDataValidation.setLateUpdateFlagging("0");
		spanDataValidation.setXmlVoidFileName(null);
		spanDataValidation.setVoidFlag(null);
		spanDataValidation.setRejectedFileFlag("0");
		spanDataValidation.setExtractFileFlag(null);
		spanDataValidation.setExecFileFlag(null);
		spanDataValidation.setTotalAmountSendToHost(null);
		spanDataValidation.setWaitStatus(null);
		spanDataValidation.setXmlRejectedFileName(null);
		
		getSpanDataValidationDaoBean().save(spanDataValidation);
	}

	private void forceInsertRFToSpanRecreateFileRejected(SpanDataValidation spdv, String newFileName, SpanHostDataDetails spanHostDataDetails) {
		SpanRecreateFileRejected spanRecreateFileRejected = new SpanRecreateFileRejected();
		
		spanRecreateFileRejected.setOriginalFileName(spdv.getFileName());
		spanRecreateFileRejected.setNewFileName(newFileName);
		spanRecreateFileRejected.setDocumentNo(spanHostDataDetails.getDOC_NUMBER());
		spanRecreateFileRejected.setOriginalDocumentDate(spanHostDataDetails.getDOC_DATE());
		spanRecreateFileRejected.setBeneficiaryName(spanHostDataDetails.getCREDITACCNAME());
		spanRecreateFileRejected.setBeneficiaryBankCode(spanHostDataDetails.getBENEFICIARYBANKCODE());
		spanRecreateFileRejected.setBeneficiaryBank(spanHostDataDetails.getBENEFICIARYBANKNAME());
		spanRecreateFileRejected.setBeneficiaryAccount(spanHostDataDetails.getCREDITACCNO());
		spanRecreateFileRejected.setAmount(spanHostDataDetails.getAMOUNT());
		spanRecreateFileRejected.setDescription(spanHostDataDetails.getDESCRIPTION());
		spanRecreateFileRejected.setAgentBankCode(spanHostDataDetails.getAGENT_BANK_CODE());
		spanRecreateFileRejected.setAgentBankAccountNo(spanHostDataDetails.getAGENT_BANK_ACCT_NO());
		spanRecreateFileRejected.setAgentBankAccountName(spanHostDataDetails.getAGENT_BANK_ACCT_NAME());
		spanRecreateFileRejected.setPaymentMethod(spanHostDataDetails.getPAYMENT_METHODS());
		spanRecreateFileRejected.setCreateDate(new Date());
		spanRecreateFileRejected.setTransactionFlag("0");
		
		getSpanDataValidationDaoBean().save(spanRecreateFileRejected);
	}
	

	private String generateNewXML(SpanDataValidation spanDataValidation, SpanHostDataDetails spanHostDataDetails, String newFilename) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String xml = ""
				+ "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
				+ "<SP2D>"
				+ "<ApplicationArea>"
				+ "<ApplicationAreaCreationDateTime>"+sdf.format(new Date())+"</ApplicationAreaCreationDateTime>"
				+ "<ApplicationAreaDetailReceiverIdentifier></ApplicationAreaDetailReceiverIdentifier>"
				+ "<ApplicationAreaDetailSenderIdentifier></ApplicationAreaDetailSenderIdentifier>"
				+ "<ApplicationAreaMessageIdentifier>"+spanDataValidation.getFileName().replace(".jar", ".xml")+"</ApplicationAreaMessageIdentifier>"
				+ "<ApplicationAreaMessageTypeIndicator>SP2D</ApplicationAreaMessageTypeIndicator>"
				+ "<ApplicationAreaMessageVersionText>1.0</ApplicationAreaMessageVersionText>"
				+ "<ApplicationAreaReceiverIdentifier>BJB</ApplicationAreaReceiverIdentifier>"
				+ "<ApplicationAreaSenderIdentifier>SPAN</ApplicationAreaSenderIdentifier>"
				+ "</ApplicationArea>"
				+ "<DataArea>"
				+ "<AgentBankAccountName>"+spanHostDataDetails.getAGENT_BANK_ACCT_NAME()+"</AgentBankAccountName>"
				+ "<AgentBankAccountNumber>"+spanHostDataDetails.getAGENT_BANK_ACCT_NO()+"</AgentBankAccountNumber>"
				+ "<AgentBankCode>"+spanHostDataDetails.getAGENT_BANK_CODE()+"</AgentBankCode>"
				+ "<Amount>"+spanHostDataDetails.getAMOUNT()+"</Amount>"
				+ "<BeneficiaryAccount>"+spanHostDataDetails.getCREDITACCNO()+"</BeneficiaryAccount>"
				+ "<BeneficiaryBank>"+spanHostDataDetails.getBENEFICIARYBANKNAME()+"</BeneficiaryBank>"
				+ "<BeneficiaryBankCode>"+spanHostDataDetails.getBENEFICIARYBANKCODE()+"</BeneficiaryBankCode>"
				+ "<BeneficiaryName>"+spanHostDataDetails.getCREDITACCNAME()+"</BeneficiaryName>"
				+ "<CurrencyTarget>"+spanHostDataDetails.getCURRENCY()+"</CurrencyTarget>"
				+ "<Description>"+spanHostDataDetails.getDESCRIPTION()+"</Description>"
				+ "<DocumentDate>"+spanHostDataDetails.getDOC_DATE()+"</DocumentDate>"
				+ "<DocumentNumber>"+spanHostDataDetails.getDOC_NUMBER()+"</DocumentNumber>"
				+ "<EmailAddress></EmailAddress>"
				+ "<IBANCode>-</IBANCode>"
				+ "<PaymentMethod>"+spanHostDataDetails.getPAYMENT_METHODS()+"</PaymentMethod>"
				+ "<SP2DCount>1</SP2DCount>"
				+ "<SwiftCode>-</SwiftCode>"
				+ "</DataArea>"
				+ "<Footer>"
				+ "<TotalAmount>"+spanHostDataDetails.getAMOUNT()+"</TotalAmount>"
				+ "<TotalBatchCount>1</TotalBatchCount>"
				+ "<TotalCount>1</TotalCount>"
				+ "</Footer>"
				+ "</SP2D>";
		return xml;
	}
	
}
