package mii.span.scheduller.odbc;

import java.io.File;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedProperty;
import javax.xml.bind.JAXBException;

import mii.span.Log;
import mii.span.db.Adapter;
import mii.span.db.Service;
import mii.span.db.adapter.Odbc;
import mii.span.doc.DataArea;
import mii.span.doc.SP2DDocument;
import mii.span.iso.util.OpenSocketConnector;
import mii.span.model.ClientParams;
import mii.span.model.SpanDataValidation;
import mii.span.model.SpanHostDataDetails;
import mii.span.process.model.MappingDataHostResponseDoc;
import mii.span.util.BusinessUtil;
import mii.span.util.PubUtil;
import mii.utils.CommonDate;

import org.apache.log4j.Logger;
import org.hibernate.Query;

import com.mii.constant.Constants;
import com.mii.constant.DaoConstant;
import com.mii.dao.SpanDataValidationDao;
import com.mii.dao.SpanHostDataDetailsDao;
import com.mii.dao.SpanVoidDataTrxDao;
import com.mii.helpers.InterfaceDAO;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.ApplicationInstance;
import com.mii.span.dao.SPANMonitoringVoidDepKeuDao;
import com.mii.span.handler.MonitoringVoidDepkeuHandler;
import com.mii.span.model.SpanSp2dVoidLog;
import com.mii.span.process.VoidDocumentNo;

public class ExtractSP2DFile implements Serializable{
	private static final long serialVersionUID = 1L;
	@ManagedProperty(value = "#{applicationBean}")
	private static ApplicationBean applicationBean;
	
	static Logger logger = Logger.getLogger(ExtractSP2DFile.class);
	
	public void run(){
		System.out.println("Start Run Extract SP2D File");
		extractSP2D();
		System.out.println("End Run Extract SP2D File");
	}
	
	private static SpanDataValidationDao getSpanDataValidationDaoBean(){
		applicationBean = ApplicationInstance.getInstance().getApplicationBean();
		return (SpanDataValidationDao) applicationBean.getFileProcessContext().getBean("spanDataValidationDao");
	}
	
	public static void extractSP2D(){
		//o mandiri.span.db.service:selectExtractFileSPAN
		
		List<SpanDataValidation> spanDataValidationList = new ArrayList<SpanDataValidation>();
		logger.info("LOG>>>>>>>> SUCCES GET_FILES "+ spanDataValidationList.get(0).toString() );
		String procStat="1";
		String rowNum="5";
		spanDataValidationList = Service.selectExtractFileSPAN(procStat, rowNum);
		System.out.println("spanDataValidationList : "+spanDataValidationList.size());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		//c mandiri.span.db.service:selectExtractFileSPAN
		InterfaceDAO dao = (SpanDataValidationDao) BusinessUtil.getDao(DaoConstant.SpanDataValidationDao);
		//o loop.over.spanFiles
		for (Iterator iterator = spanDataValidationList.iterator(); iterator.hasNext();) {
			SpanDataValidation spanDataValidation = (SpanDataValidation) iterator.next();
			
			//o mandiri.span.db.service:updateSPAN
//			String sql = "update SPAN_DATA_VALIDATION set PROC_STATUS = 'K',  PROC_DESCRIPTION = 'Extracting file process', UPDATE_DATE = sysdate where FILE_NAME='%SPANFiles/FILE_NAME%'";
			String sql = "UPDATE SpanDataValidation SET procStatuc = 'K',  procDescription = 'Extracting file process', updateDate = '"+sdf.format(new Date())+"' WHERE fileName = '"+spanDataValidation.getFileName()+"'";
			Query query = dao.getQuery(sql);
			Service.updateSPAN(query);
			//c mandiri.span.db.service:updateSPAN
			
			
			
			//TODO : Need help to convert below logic to asynchronous method to call method extractProcesSP2D
			//o mandiri.span.service.extractor:extractSCProcessSP2D
			//below 
			String fileName = spanDataValidation.getFileName();
			String batchID = spanDataValidation.getBatchId();
			String trxHeaderID = spanDataValidation.getTrxHeaderId();
			String debitAccount = spanDataValidation.getDebitAccount();
			String debitAccountType = spanDataValidation.getDebitAccountType();
			String documentDate = spanDataValidation.getDocumentDate();
			String totalAmount = spanDataValidation.getTotalAmount();
			String totalRecord = spanDataValidation.getTotalRecord();
			String procStatus = spanDataValidation.getProcStatuc();
			String xmlFileName = spanDataValidation.getXmlFileName();
			String responseCode = spanDataValidation.getResponseCode();
			String trxType = spanDataValidation.getTrxType();
			Date createDate = spanDataValidation.getCreateDate();
			String fileRejectedFlag  = spanDataValidation.getRejectedFileFlag();
			String voidFlag = spanDataValidation.getVoidFlag();
			String extractFileFlag = spanDataValidation.getExtractFileFlag();
			String execFileFlag = spanDataValidation.getExecFileFlag();
			String payrollType = spanDataValidation.getPayrollType();
			
			try {
				extractProcesSP2D(  fileName,
						  batchID,
						  trxHeaderID,
						  debitAccount,
						  debitAccountType,
						  documentDate,
						  totalAmount,
						  totalRecord,
						  procStatus,
						  xmlFileName,
						  responseCode,
						  trxType,
						  createDate.toString(),
						  fileRejectedFlag,
						  voidFlag,
						  extractFileFlag,
						  execFileFlag,
						  payrollType);
			} catch (JAXBException e) {
				e.printStackTrace();
			}
			
			//c mandiri.span.service.extractor:extractSCProcessSP2D
		}
		//c loop.over.spanFiles
		
		//Finally
		String sqlUpdate = "";
		Query query = null;
		/*if(!checkTransaction()){
			sqlUpdate = "UPDATE SpanDataValidation SET procStatuc = '1', procDescription = 'Data ready to be executed', UPDATE_DATE = '"+sdf.format(new Date())+"' WHERE procStatuc = 'K' and extractFileFlag = '1'";
			query = dao.getQuery(sqlUpdate);
			if(query != null){
				Service.updateSPAN(query);
			}
		}*/
	}

	private static void extractProcesSP2D(String fileName,
			String batchID,
			String trxHeaderID,
			String debitAccount,
			String debitAccountType,
			String documentDate,
			String totalAmount,
			String totalRecord,
			String procStatus,
			String xmlFileName,
			String responseCode,
			String trxType,
			String createDate,
			String fileRejectedFlag,
			String voidFlag,
			String extractFileFlag,
			String execFileFlag,
			String payrollType) throws JAXBException{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		InterfaceDAO dao = (SpanHostDataDetailsDao) BusinessUtil.getDao(DaoConstant.SpanHostDataDetailsDao);
		//Sequence EXTRACT
		
		//o Convert XML to Object
		SP2DDocument spanDocument = PubUtil.createSPANDocument(xmlFileName);
		//c Convert XML to Object
		
		
		//o read parameter rtgs & skn
		String limitRTGS = Odbc.selectSystemParameterByParamName(Constants.SPAN_LIMIT_RTGS);
		String limitSKN = Odbc.selectSystemParameterByParamName(Constants.SPAN_LIMIT_SKN);
		String asynchronous = "0";
		//c read parameter rtgs & skn
		
		//o mandiri.span.util.atomic:getSleep
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		//c mandiri.span.util.atomic:getSleep
		
		//o mandiri.span.atomic.request.odbc:completeDataHost
		//prepare to invoke completeDataHost
		String messageType = "SOA";	
		String forwardBranch = "99102";
		String rtgsTRNCode = "IFT00000";
		String sknCityCode = "0391";
		String residentFlag = "Y";
		String citizenship = "Y";
		String batchTime = "200";
		String glAccount = "";
		String scSettlement = "";
		MappingDataHostResponseDoc results = completeDataHost(spanDocument,
				batchID,
				trxHeaderID,
				messageType,
				forwardBranch ,
				rtgsTRNCode ,
				sknCityCode,
				residentFlag,
				citizenship,
				batchTime,
				asynchronous,
				trxType,
				glAccount,
				scSettlement,
				limitSKN,
				limitRTGS);
		//c mandiri.span.atomic.request.odbc:completeDataHost
		
		//o mandiri.span.log:debugLog
		Log.debugLog("SPAN", "extractProcessSP2D", "INFO", "Complete Extract File "+ fileName + "| Status = " + results.getStatus());
		//c mandiri.span.log:debugLog
		
		String status = results.getStatus();
		String sqlUpdate = "";
		Query query = null;
		if("SUCCESS".equalsIgnoreCase(status)){
			sqlUpdate = "update SpanDataValidation set procStatuc = '1', procDescription = 'Data ready to be executed', UPDATE_DATE = '"+sdf.format(new Date())+"', extractFileFlag = '1' where FILE_NAME='"+fileName+"'";
//			sqlUpdate = "UPDATE SpanDataValidation SET procDescription = 'Waiting ongoing transaction', UPDATE_DATE = '"+sdf.format(new Date())+"', extractFileFlag = '1' WHERE fileName= '"+fileName+"'";
			query = dao.getQuery(sqlUpdate);
		}else if("ERROR".equalsIgnoreCase(status)){
//			sqlUpdate = "update SPAN_DATA_VALIDATION set PROC_STATUS = '1',  PROC_DESCRIPTION = 'Failed to extract data, please try again',  UPDATE_DATE = sysdate, EXTRACT_FILE_FLAG = '0' where FILE_NAME='%mandiri.span.doc.publish:spanDataExtract/input/fileName%'";
			sqlUpdate = "UPDATE SpanDataValidation SET procStatuc = '1', procDescription = 'Failed to extract data, please try again', UPDATE_DATE = '"+sdf.format(new Date())+"', extractFileFlag = '0' WHERE FILE_NAME= '"+fileName+"'";
			query = dao.getQuery(sqlUpdate);
		}else{
			// do nothing
		}
		if(query != null){
			Service.updateSPAN(query);
		}
		
		//scheduller auto void
		MonitoringVoidDepkeuHandler monitoringVoidDepkeuHandler = new MonitoringVoidDepkeuHandler();
		//monitoringVoidDepkeuHandler.init();
		
		List<String> sp2dAlreadyVoids = checkAlreadyVoid(xmlFileName);
		
		if(null != sp2dAlreadyVoids && sp2dAlreadyVoids.size() > 0){
			List<DataArea> dataAreas = spanDocument.getDataArea();
			Map<String, DataArea> dataAreaalreadyVoids = new LinkedHashMap<String, DataArea>();
			for (DataArea dataArea : dataAreas) {
				 if(sp2dAlreadyVoids.contains(dataArea.getDocumentNumber().substring(0, 15))){
					 //document number already void
					 monitoringVoidDepkeuHandler.doVoidDocumentNumberByScheduller(dataArea.getDocumentNumber());
				 }
			}
		}
	}

	private static List<String> checkAlreadyVoid(String xmlFileName) {
		// TODO Auto-generated method stub
		List<String> sp2dAlreadyVoids = new ArrayList<String>();
		
		List<SpanSp2dVoidLog> spanSp2dVoidLogs = VoidDocumentNo.getAllSpanSp2dVoidLogs(getSpanVoidDataTrxDAOBean());
		for (SpanSp2dVoidLog spanSp2dVoidLog : spanSp2dVoidLogs) {
			String sp2dNo = spanSp2dVoidLog.getSp2dNo();
			if(xmlFileName.contains(sp2dNo)){
				sp2dAlreadyVoids.add(sp2dNo);
			}
		}
		return new ArrayList<String>(new LinkedHashSet<String>(sp2dAlreadyVoids));
	}

	private static boolean checkVoidData(DataArea dataArea) {
		List<SpanSp2dVoidLog> result = new ArrayList<>();
		 String sp2dNo = dataArea.getDocumentNumber().substring(0, 15);
		 result = VoidDocumentNo.checkSpanSp2dVoidLogsBySp2dNo(sp2dNo, getSpanVoidDataTrxDAOBean());
		
		if(null != result || result.size()>0){
			return true;
		}
		return false;
	}
	
	private static SpanVoidDataTrxDao getSpanVoidDataTrxDAOBean(){
		applicationBean = ApplicationInstance.getInstance().getApplicationBean();
		return (SpanVoidDataTrxDao) applicationBean.getCorpGWContext().getBean("SPANVoidDataTrxDao");
	}

	private static SPANMonitoringVoidDepKeuDao getSpanVoidDepKeuDAOBean(){
		return (SPANMonitoringVoidDepKeuDao) applicationBean.getCorpGWContext().getBean("SPANMonitoringVoidDepKeuDao");
	}

	public static boolean checkTransaction(){
		/*boolean status = false;
		int count = getSpanDataValidationDaoBean().countOngoingTransaction();
		
		System.out.println("Ongoing transaction : "+count);
		if(count > 0){
			status = true;
		}else{
			status = false;
		}*/
		String documentDate = CommonDate.getCurrentDateString("yyyy-MM-dd");
		boolean status = getSpanDataValidationDaoBean().isStillProcessing(documentDate);
		return status;
	}

	private static MappingDataHostResponseDoc completeDataHost(SP2DDocument spanDocument, String batchID, String trxHeaderID, String messageType,
			String forwardBranch, String rtgsTRNCode, String sknCityCode, String residentFlag, String citizenship,
			String batchTime, String asynchronous, String trxType, String glAccount, String scSettlement,
			String limitSKN, String limitRTGS) {
		
		//o mandiri.span.atomic.request.odbc:mappingDataHost
		String bookRate = "0.0000000";
		String ttSellRate = "1.0000000";
		String ttBuyRate = "1.0000000";
		String ibtSellRate = "1.0000000";
		String ibtBuyRate = "1.0000000";
		String reqStatus = "0";
		String changeInstruction = "BEN";
		MappingDataHostResponseDoc results = mappingDataHost(spanDocument,
				batchID,
				trxHeaderID,
				bookRate,
				ttSellRate,
				ttBuyRate,
				ibtSellRate,
				ibtBuyRate,
				reqStatus,
				changeInstruction,
				batchTime,
				asynchronous,
				trxType,
				glAccount,
				scSettlement,
				limitSKN,
				limitRTGS);
		//c mandiri.span.atomic.request.odbc:mappingDataHost
		
		//o mandiri.span.db.adapter:updateChangeStatus
		String Status =  results.getStatus();
		String ErrorMessage =  results.getErrorMessage();
		String statusChange =  results.getStatusChange();
		String sknBankCode =  results.getSknBankCode();
		String sknSandiBank =  results.getSknSandiBank();
		
		 Adapter.updateChangeStatus(batchID,statusChange);
		//c mandiri.span.db.adapter:updateChangeStatus
		
		
		return results;
	}
	

	private static MappingDataHostResponseDoc mappingDataHost(SP2DDocument spanDocument, String batchID, String trxHeaderID, String bookRate,
			String ttSellRate, String ttBuyRate, String ibtSellRate, String ibtBuyRate, String reqStatus,
			String changeInstruction, String BatchTime, String asynchronous, String trxType, String glAccount,
			String scSettlement, String limitSKN, String limitRTGS) {
		String charges = "";
//		String asynchronous = "";
//		String scSettlement = "";
//		String trxType = "";
//		String glAccount = "";
		String trxDetailIDForMVA = "";
		String mvaErrorFlag = "0";
		
		String reserve1 ="";
		String bicSKN = "";
//		
//		if(pipelineCursor.first("asynchronous"))
//			asynchronous = IDataUtil.getString(pipelineCursor, "asynchronous");
//		if(pipelineCursor.first("trxType"))
//			trxType = IDataUtil.getString(pipelineCursor, "trxType");
//		
//		if(pipelineCursor.first("glAccount"))
//			glAccount = IDataUtil.getString(pipelineCursor, "glAccount");
//		
//		if(pipelineCursor.first("scSettlement")) 
//			scSettlement = IDataUtil.getString(pipelineCursor, "scSettlement");
		
		int batchTime = Integer.parseInt(BatchTime);
		int sendPerList = 0;
		
		Date date = null;
		Date date2 = new Date();
		SimpleDateFormat sdf = null;
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat sdf2 = new SimpleDateFormat("yyMMdd");
		SimpleDateFormat sdf3 = new SimpleDateFormat("yyyyDDD");
		
		String Status = "SUCCESS";   
		String ErrorMessage = "";             	
		String blank = "";
		String zero = "0";
		String trackIDReference = batchID;
		String postingStatus = "";
		String messageType = "";
		String messageNumber = "";
		String messageSequence = "";
		String applicationCode = "";
		String transferAmount = "";
		String transferCurrency = "";
		String totalIDRAmount = "";
		String currency = "";
//		String ibtBuyRate = IDataUtil.getString(pipelineCursor, "ibtBuyRate");
//		String ibtSellRate = IDataUtil.getString(pipelineCursor, "ibtSellRate");
//		String ttBuyRate = IDataUtil.getString(pipelineCursor, "ttBuyRate");
//		String ttSellRate = IDataUtil.getString(pipelineCursor, "ttSellRate");
//		String bookRate = IDataUtil.getString(pipelineCursor, "bookRate");
		String source = "";
//		String forwardBranch = IDataUtil.getString(pipelineCursor, "forwardBranch");
		String chargeCode = "";
		String chargesCurr = "";
		String valueDate = "";
		String valueDateYYYYDDD = "";
		String applicantName = "";
		String applicantaddress1 = "";
		String applicantaddress2 = "";
		String applicantaddress3 = "";
		String applicantpostcode = "";
		String beneficiaryName = "";
		String beneficiaryAdd1 = "";
		String beneficiaryAdd2 = "";
		String beneficiaryAdd3 = "";
		String beneficiaryPostcode = "";
		String beneficiaryRemark1 = "";
		String beneficiaryRemark2 = "";
		String beneficiaryRemark3 = "";
		String beneficiaryRemark4 = "";
		String beneficiaryRemark5 = "";
		String beneficiaryRemark6 = "";
		String receiveDate = "";
		String receiveDatejulian = "";
		String receiveTime = "";
		String processingDate = "";
		String processingDatejul = "";
		String processingTime = "";
		String creditAmount = "";
		String creditCurrency = "";
		String fromAccountDr = "";
		String fromAccountCurr = "";
		String toAccountCr = "";
		String toAccountCurr = "";
		String beneficiaryAC = "";
		String senderAC = "";
		String miscSenderRefNo = "";
		String miscBenRefNo = "";
		String processingErrorNo = "";
		String processingSequence = "";
		String interfaceStatus = "";
		String senderCorr = "";
		String receiverCorr = "";
		String trnReferenceNo = "";
		String refOwner = "";
		String refServer = "";
		String miscRef1 = "";
		String ecOriginalSeqNo = "";
		String errorCorrect = "";
		String inwardRemittanceNo = "";
		String outwardRemittanceNo = "";
		String beneficiaryBankName = "";
		String treasuryCode = "";
		String agentCode = "";
		String specialDebitGL = "";
		String specialCreditGL = "";
		String swiftMTType = "";
		String swiftType = "";
		String customerType = "";
		String payInFullCharge = "";
		String provisionCharge = "";
		String bicCode = "";
		String senderCorrBIC = "";
		String swiftTFileACT = "";
		String rtgsBankCode = "";
		String rtgsTRNCode = "";
		rtgsTRNCode = padRight(rtgsTRNCode, " ", 10);
		String rtgsBankAcct = "";
		String sknBankCode = "";
		String sknCityCode = "";
		sknCityCode = padRight(sknCityCode, " ", 4);
		String sknSandiBank = "";
		String sknTransactionCode = "";
//		String residentFlag = IDataUtil.getString(pipelineCursor, "residentFlag");
		String citizenship = "";
		String debitExchangeRate = "";
		String debitTreasuryRate = "";
		String creditExchangeRate = "";
		String creditTreasuryRate = "";
		String skneChannel = "";
		String miscCode1 = "";
		String miscCode2 = "";
		String miscCode3 = "";
		String miscCode4 = "";
		String purgeDate = "";
		String purgeJULDate = "";
		String purgeTime = "";
		String urgeUserOrProgram = "";
		
		String BankCode = "";
		String CityCode = "";
		String SKNCode  = "";
		String SKNCode_="";
		String BICBank = "";
		String BenBankType = ""; 
		String transferMethod = "";
		String dbPaymentMethod = "";
		
		String hostSequenceNoAS1 = "";
		String hostSequenceNoAS2 = "";
		
		BigDecimal limitSKN_ = new BigDecimal(limitSKN);
		BigDecimal limitRTGS_ = new BigDecimal(limitRTGS);
		BigDecimal Amount_ = new BigDecimal("0.00");
		
		String status_change="";
		String change_flagSKN="";
		String change_flagRTGS="";
		String prefixAcct = "";
		String realAcct = "";
		String hostSequenceNo= "";
		String mvaHostSequenceNo= "";
		String statusMVA = "";
		String descMVA = "";
		String detailIDMVA = "";
		String remark3MVA = "";
		String voidTrx = "";
		String MVAAcct = "";
		
		ClientParams output = new ClientParams();
		Map<String, ClientParams> clientParamsMap = new HashMap<String, ClientParams>();
		
		String legStatus4SCResponse = "";
		
		try{
		if ( spanDocument != null){
			String SenderIdentifier = spanDocument.getApplicationArea().getApplicationAreaSenderIdentifier();
			String ReceiverIdentifier = spanDocument.getApplicationArea().getApplicationAreaReceiverIdentifier();	
			String DetailSenderIdentifier = "";
				if(spanDocument.getApplicationArea().getApplicationAreaDetailSenderIdentifier() != null)
					DetailSenderIdentifier = spanDocument.getApplicationArea().getApplicationAreaDetailSenderIdentifier();	
			
			String DetailReceiverIdentifier = "";
				if(spanDocument.getApplicationArea().getApplicationAreaDetailReceiverIdentifier() !=null)
					DetailReceiverIdentifier = spanDocument.getApplicationArea().getApplicationAreaDetailReceiverIdentifier();
		
			String CreationDateTime = spanDocument.getApplicationArea().getApplicationAreaCreationDateTime();		
			String MessageIdentifier = spanDocument.getApplicationArea().getApplicationAreaMessageIdentifier();
			String MessageTypeIndicator = spanDocument.getApplicationArea().getApplicationAreaMessageTypeIndicator();
			
				
			String TotalCount = spanDocument.getFooter().getTotalCount();
			String TotalAmount = spanDocument.getFooter().getTotalAmount();
			String TotalBatchCount = spanDocument.getFooter().getTotalBatchCount();

		
			List<DataArea> spanDocumentDataAreaList =  new ArrayList<DataArea>();
			spanDocumentDataAreaList = spanDocument.getDataArea();
			
			DataArea spanDocDataAreaCursor = null;
			
			List<SpanHostDataDetails> insertSPANHostDetailsList = null;
			List<SpanHostDataDetails> insertSPANHostDetailsListAS1 = null;
			List<SpanHostDataDetails>	insertSPANHostDetailsListAS2 = null;
			SpanHostDataDetails insertSPANHostDetailsCursor = null;
			SpanHostDataDetails insertSPANHostDetailsCursorAS1 = null;
			SpanHostDataDetails insertSPANHostDetailsCursorAS2 = null;
			
			Thread.sleep(1000);
			int seqNo = 0;
			
			if (!BusinessUtil.isListEmpty(spanDocumentDataAreaList)){
				insertSPANHostDetailsList = new ArrayList<SpanHostDataDetails>();
				insertSPANHostDetailsListAS1 = new ArrayList<SpanHostDataDetails>();
				insertSPANHostDetailsListAS2 = new ArrayList<SpanHostDataDetails>();
				long x = System.currentTimeMillis();
				for ( int i = 0; i < spanDocumentDataAreaList.size(); i++ ){
					spanDocDataAreaCursor = spanDocumentDataAreaList.get(i);//per document list
		
					String DocumentDate = spanDocDataAreaCursor.getDocumentDate();
					String DocumentNumber = spanDocDataAreaCursor.getDocumentNumber();
					String BeneficiaryName = spanDocDataAreaCursor.getBeneficiaryName();
					String BeneficiaryBankCode = spanDocDataAreaCursor.getBeneficiaryBankCode();
					String BeneficiaryBank = spanDocDataAreaCursor.getBeneficiaryBank();
					String BeneficiaryAccount = spanDocDataAreaCursor.getBeneficiaryAccount();
					String Amount = spanDocDataAreaCursor.getAmount();
					String CurrencyTarget = spanDocDataAreaCursor.getCurrencyTarget();
					String Description = spanDocDataAreaCursor.getDescription();
					String AgentBankCode = spanDocDataAreaCursor.getAgentBankCode();		
					String AgentBankAccountNumber = spanDocDataAreaCursor.getAgentBankAccountNumber();
					String AgentBankAccountName = spanDocDataAreaCursor.getAgentBankAccountName();
					String EmailAddress = spanDocDataAreaCursor.getEmailAddress();
					String SwiftCode = spanDocDataAreaCursor.getSwiftCode();		
					String IBANCode = spanDocDataAreaCursor.getIBANCode();
					String PaymentMethod = spanDocDataAreaCursor.getPaymentMethod();	
					String SP2DCount = spanDocDataAreaCursor.getSP2DCount();
						
//					spanDocDataAreaCursor.destroy();
		
					if(Amount.equalsIgnoreCase("VOID")){
						Amount="0";
						voidTrx="Y";
					}else{
						voidTrx="N";
					}
		/**
					//CHECKING FOR MVA ACCOUNT
					prefixAcct = BeneficiaryAccount.substring(0,1);
					String status = "";
					//System.out.println("Check MVA");
					if ("8".equals(prefixAcct.trim()) && "0".equals(PaymentMethod.trim())){
					    try{
						MVAAcct = BeneficiaryAccount;
						//Create IData MVA Account
//						IData MVAAccount = IDataFactory.create();
//						IDataCursor MVAAccountCursor = MVAAccount.getCursor();
						
						IDataUtil.put(MVAAccountCursor, "MVAAccount", BeneficiaryAccount);
						IDataUtil.put(MVAAccountCursor, "amount", Amount);
//						MVAAccountCursor.destroy();
						System.out.println("Ready to invoke mandiri.span.service:replaceMvaAcc : "+BeneficiaryAccount+" - "+Amount);
		
						MVADetails
						MVAoutput = wmDoInvoke("mandiri.span.service","replaceMvaAcc",MVAAccount);
						MVAoutputCursor = MVAoutput.getCursor();
						
						if(MVAoutputCursor.first("realAccount")){
							realAcct = IDataUtil.getString(MVAoutputCursor, "realAccount");
							System.out.println("CAL"+realAcct);
						}
						if (MVAoutputCursor.first("status")){
							statusMVA = IDataUtil.getString(MVAoutputCursor, "status");
							System.out.println("status MVA = "+statusMVA);
						}
						if (MVAoutputCursor.first("mvaCompanyCode")){
							descMVA = IDataUtil.getString(MVAoutputCursor, "mvaCompanyCode");							
							System.out.println("MVAcompanyCode = "+descMVA);							
						}
						if (MVAoutputCursor.first("trxDetailIDForMVA")){
							trxDetailIDForMVA = IDataUtil.getString(MVAoutputCursor, "trxDetailIDForMVA");
							if("".equals(trxDetailIDForMVA)) 
								trxDetailIDForMVA = "UBP".concat(sdf.format(new Date())).concat(padLeft(String.valueOf(seqNo),"0",3));			
							remark3MVA = trxDetailIDForMVA;
							mvaHostSequenceNo = trxDetailIDForMVA.concat(padLeft(String.valueOf(seqNo),"0",3));
						}
		
						MVAoutputCursor.destroy();
						BeneficiaryAccount = realAcct;
		
						System.out.println("CAL"+"benefacct"+BeneficiaryAccount);
						//Checking for status MVA, B8 = Already paid, B5 = Bill Key Not Found, 01 = General Error
						if("B8".equals(statusMVA.trim()) || "B5".equals(statusMVA.trim()) || "01".equals(statusMVA.trim())){
							//Amount = "0";
							mvaErrorFlag = "1";
						}
				
					    }catch(Exception e){
						e.printStackTrace();
					    }
					}
					**/
		
					//MAPPING PROCESS
					messageType = padRight(messageType, " ", 5);
					messageNumber = padLeft("0", "0", 11);	
					messageSequence = "0";
					transferAmount = padLeft(setDecimal(Amount),"0",20);//19+1 dot
					transferCurrency = CurrencyTarget;
					totalIDRAmount = padLeft(setDecimal(zero), "0", 20);//19+1dot
					currency = CurrencyTarget;
					source = padRight(zero, " ", 2);
					
					//0=inhouse(ACCT), 1=rtgs(RTGS), 2=skn(CN), 3=swift			
					if("0".equals(PaymentMethod)) {
						PaymentMethod = padRight("ACCT"," ",5);
						dbPaymentMethod = "0";
						transferMethod = "IBU";					
					}else if("1".equals(PaymentMethod)) {
						PaymentMethod = padRight("RTGS"," ",5);
						dbPaymentMethod = "1";
						transferMethod = "RBU";
					}else if("2".equals(PaymentMethod)) {
						PaymentMethod = padRight("CN"," ",5);
						dbPaymentMethod = "2";
						transferMethod = "LBU";
					}
					charges = padLeft(setDecimal("0"),"0",16);
					chargesCurr = padRight("IDR"," ", 4);
		
					//sdf1 = new SimpleDateFormat("yyyy-MM-dd");
					try {
						date = sdf1.parse(DocumentDate);
						//sdf2 = new SimpleDateFormat("yyMMdd");
						valueDate = sdf2.format(date);
		
						//sdf3 = new SimpleDateFormat("yyyyDDD");
						valueDateYYYYDDD = sdf3.format(date);
					} catch (ParseException e) {
						e.printStackTrace();
					}
		
					applicantName = padRight("SPAN"," ",40);
					beneficiaryName = BeneficiaryName;
		//			beneficiaryRemark3 = padRight(referenceNo.concat(padLeft(String.valueOf(i+1),"0",20))," ",40);
					beneficiaryRemark2 = padRight(SP2DCount," ",40);
		
					if(Description.length() >=100) {
						beneficiaryRemark1 = padRight(Description.substring(0,40)," ",40);
						beneficiaryRemark4 = padRight(Description.substring(41,80)," ",40);
						beneficiaryRemark5 = padRight(Description.substring(81,100)," ",40);
					}else if(Description.length()>=81 && Description.length()<100) {
						beneficiaryRemark1 = padRight(Description.substring(0,40)," ",40);
						beneficiaryRemark4 = padRight(Description.substring(41,80)," ",40);
						beneficiaryRemark5 = padRight(Description.substring(81)," ",40);
					}else if(Description.length()>=41 && Description.length()<81) {
						beneficiaryRemark1 = padRight(Description.substring(0,40)," ",40);
						beneficiaryRemark4 = padRight(Description.substring(41)," ",40);
						beneficiaryRemark5 = padRight(blank," ",40);
					}else if(Description.length()<41) {
						beneficiaryRemark1 = padRight(Description," ",40);
						beneficiaryRemark4 = padRight(blank," ",40);
						beneficiaryRemark5 = padRight(blank," ",40);
					}
		
					//For Tunning Invoke
//					if(clientParamsMap.containsKey(BeneficiaryBankCode)){
//						output = clientParamsMap.get(BeneficiaryBankCode);
//					}else{
					   //get Data for Client Key (BIC,SKN, Bank Code, City Code)					
//					   IData clientKey = IDataFactory.create();
//					   IDataCursor clientKeyCursor = clientKey.getCursor();
					   	
//					   IDataUtil.put(clientKeyCursor, "clientKey", BeneficiaryBankCode);
//					   clientKeyCursor.destroy();	
//					   String clientKey = BeneficiaryBankCode;
//					   output = Service.selectClientParams(clientKey);
		          	        	        				
//					   output = wmDoInvoke("mandiri.span.db.service", "selectClientParams", clientKey);
//					   clientParamsMap.put(BeneficiaryBankCode,output);
//					}
					
//					outputCursor = output.getCursor();
					//System.out.println("berhasil invoke select client params");									      
		                	
//					if(outputCursor.first("rtgsCD"))
//						BICBank = output.getRtgsCD();
//					if(outputCursor.first("bankCD"))
//						BankCode = output.getBankCD();
//					if(outputCursor.first("cityCD"))	
//						CityCode = output.getCityCD();
//					if(outputCursor.first("clientID"))	
//						SKNCode = output.getClientID();
//					if(outputCursor.first("sknCD"))
//						SKNCode_ = output.getSknCD();
//					if(outputCursor.first("bankNameType"))	
//						BenBankType = output.getBankNameType();
//					if(outputCursor.first("bicSKN"))
//						bicSKN =  output.getBicSKN();
		                	
//					outputCursor.destroy();						      
		
					if("CN".equals(PaymentMethod.trim())||"SKN".equals(PaymentMethod.trim())) {
						creditAmount = padLeft(setDecimal(Amount),"0",20);
						creditCurrency = padLeft(setDecimal(CurrencyTarget),"0",20);
					}else {
						creditAmount = padLeft(setDecimal(zero), "0", 20);
						creditCurrency = padLeft(setDecimal(zero), "0", 20);
					}
		
					fromAccountDr = padLeft(AgentBankAccountNumber,"0",19);
					toAccountCr = padLeft(BeneficiaryAccount,"0",19);
					beneficiaryAC = padRight(BeneficiaryAccount," ", 35);
					senderAC = padRight(AgentBankAccountNumber," ", 35);
					beneficiaryBankName = padRight(BeneficiaryName, " ",40);
					
					String BeneficiaryBankCode_ = BeneficiaryBankCode;
					if("3".equals(PaymentMethod)){					
						PaymentMethod = padRight("ACCT"," ",5);
						dbPaymentMethod = "3";
						transferMethod = "INU";
					}else{
						bicCode = padRight(BICBank, " ", 12);
		              }
		
					if("RTGS".equals(PaymentMethod.trim())) {
						rtgsBankCode = padRight(BeneficiaryBankCode," ",12);
						BeneficiaryBankCode = padRight(BICBank," ",12);
						BeneficiaryBankCode_ = BeneficiaryBankCode;
					}
					else rtgsBankCode = padRight(BICBank, " ", 12);
					
					if("CN".equals(PaymentMethod.trim())) {
						sknBankCode = padRight((BeneficiaryBankCode).substring(4,6)," ",3);
						BeneficiaryBankCode = SKNCode_;
						BeneficiaryBankCode_ = BeneficiaryBankCode;
					}
					else sknBankCode = padRight(SKNCode, " ", 3);
		            
					Amount_ = new BigDecimal(Amount);
					
					if("CN".equals(PaymentMethod.trim())&&(Amount_.compareTo(limitSKN_)>0)){
						dbPaymentMethod = "1";
						transferMethod = "RBU";
						BeneficiaryBankCode = padRight(BICBank," ",12);
						BeneficiaryBankCode_ = BeneficiaryBankCode;
						status_change="SKN to RTGS";
						change_flagSKN="1";
					}else if("RTGS".equals(PaymentMethod.trim())&&Amount_.compareTo(limitSKN_)<=0){					
						dbPaymentMethod = "2";
						transferMethod = "LBU";
						BeneficiaryBankCode = SKNCode_;
						BeneficiaryBankCode_ = BeneficiaryBankCode;
						status_change="RTGS to SKN";
						change_flagRTGS="2";
					}else{
						status_change="0";
					}
		
					if ("1".equals(change_flagSKN) && "2".equals(change_flagRTGS)){
						status_change="SKN to RTGS and RTGS to SKN";
						System.out.println(PubUtil.concat("****************STATUS CHANGE***********", status_change));
					}
		
					if ("3".equals(dbPaymentMethod)){
						if (Amount_.compareTo(BigDecimal.ZERO)>0){
							reqStatus="3";
						}else{
							reqStatus="2";
						}
					}else{
						if(Amount_.compareTo(BigDecimal.ZERO)==0 && "Y".equals(voidTrx)){
							reqStatus="2";
						}else if("1".equals(mvaErrorFlag)){
							reqStatus="1";
						}else if (Amount_.compareTo(BigDecimal.ZERO)==0 && "N".equals(voidTrx)){
							reqStatus="2";
						}else{
							reqStatus="0";
						}
					}
					
					// map reserve1 -> bicSKN for SKN
					if("2".equals(dbPaymentMethod)){
						reserve1 = bicSKN;
					}
					
//					spanDocDataAreaCursor.destroy();							      
		                	
					if (seqNo>999){
					   seqNo=0;
					   try{
						Thread.sleep(2000);
					   }catch(InterruptedException e){
						e.printStackTrace();
						Status = "ERROR";
					   }
					}
		
					sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		
		
					if("8".equals(prefixAcct.trim()) && "0".equals(dbPaymentMethod.trim())){
						hostSequenceNo = "UBP".concat(sdf.format(new Date())).concat(
								padLeft(String.valueOf(seqNo),"0",3));	
					}else{ 
						hostSequenceNo = "SPN".concat(sdf.format(new Date())).concat(
								padLeft(String.valueOf(seqNo),"0",3));
						
					}			
					seqNo++;
		
					if(asynchronous.equals("1")){
						hostSequenceNoAS1 = hostSequenceNo.replace("SPN", "SP1");
						hostSequenceNoAS2 = hostSequenceNo.replace("SPN", "SP2");
					}
		
					if("ACCT".equals(PaymentMethod.trim())){
					 	if("8".equals(prefixAcct.trim()) && "0".equals(dbPaymentMethod.trim())){
							beneficiaryRemark3 = padRight(remark3MVA, " ", 40);
						}else{
							//beneficiaryRemark3 = padRight(hostSequenceNo, " ", 40);
		
							//Change remark trxremark3 overbooking for SKN Gen 2
							beneficiaryRemark3 = padRight("SPAN ".concat(DocumentNumber), " ", 40);
		
						}
					}else{
						beneficiaryRemark3 = beneficiaryRemark1;
						//beneficiaryRemark1 = padRight(hostSequenceNo, " ", 40);
		
						//Change remark trxremark1 skn or rtgs for SKN Gen 2
						beneficiaryRemark1 = padRight("SPAN ".concat(DocumentNumber), " ", 40);
					}
						
					//per document list			
					if(asynchronous.equals("1")) {
						if(!trxType.equals("2")){
							String debetAcctTemp = AgentBankAccountNumber;
							AgentBankAccountNumber = glAccount;
//							insertSPANHostDetailsListAS1[i] = IDataFactory.create();
//							insertSPANHostDetailsCursorAS1 = insertSPANHostDetailsListAS1.get(i);
							insertSPANHostDetailsCursorAS1 = putData2Pipeline( insertSPANHostDetailsCursorAS1,  batchID,  trxHeaderID,  hostSequenceNoAS1, 
											SenderIdentifier, DetailSenderIdentifier, DetailReceiverIdentifier,  
											CreationDateTime,  MessageIdentifier, MessageTypeIndicator,  
											DocumentDate,  DocumentNumber, BeneficiaryName, BeneficiaryBankCode,
											BeneficiaryBank,  BeneficiaryAccount, Amount, currency, 
											Description,  AgentBankCode, AgentBankAccountNumber, AgentBankAccountName,  
											EmailAddress,  SwiftCode,  IBANCode,  dbPaymentMethod,
											SP2DCount,  TotalCount,  TotalAmount,  ibtBuyRate,  
											changeInstruction,  citizenship, beneficiaryRemark1,  
											beneficiaryRemark2,  beneficiaryRemark3,  beneficiaryRemark4,  
											BeneficiaryBankCode_, ibtSellRate,  reqStatus, ReceiverIdentifier, 
											TotalBatchCount, transferMethod, "L1", descMVA, statusMVA,CityCode, reserve1);
//							insertSPANHostDetailsCursorAS1.destroy();
							legStatus4SCResponse = "L2";
							AgentBankAccountNumber = debetAcctTemp;
							BeneficiaryAccount = glAccount;
							insertSPANHostDetailsListAS1.add(insertSPANHostDetailsCursorAS1);	//by win
						} else {//20130813 For SC response
							if ("SC".equals(glAccount)) {
								legStatus4SCResponse = "L1";
								hostSequenceNoAS2 = hostSequenceNoAS2.replace("SP2", "SPN");
							} else{
								legStatus4SCResponse = "L2";
							}
						}
//						insertSPANHostDetailsListAS2[i] = IDataFactory.create();
//						insertSPANHostDetailsCursorAS2 = insertSPANHostDetailsListAS2.get(i);
						insertSPANHostDetailsCursorAS2 = putData2Pipeline( insertSPANHostDetailsCursorAS2,  batchID,  trxHeaderID,  hostSequenceNoAS2, 
											SenderIdentifier, DetailSenderIdentifier, DetailReceiverIdentifier,  
											CreationDateTime,  MessageIdentifier, MessageTypeIndicator,  
											DocumentDate,  DocumentNumber, BeneficiaryName, BeneficiaryBankCode,
											BeneficiaryBank,  BeneficiaryAccount, Amount, currency, 
											Description,  AgentBankCode, AgentBankAccountNumber, AgentBankAccountName,  
											EmailAddress,  SwiftCode,  IBANCode,  dbPaymentMethod,
											SP2DCount,  TotalCount,  TotalAmount,  ibtBuyRate,  
											changeInstruction,  citizenship, beneficiaryRemark1,  
											beneficiaryRemark2,  beneficiaryRemark3,  beneficiaryRemark4,  
											BeneficiaryBankCode_, ibtSellRate,  reqStatus, ReceiverIdentifier, 
											TotalBatchCount, transferMethod, legStatus4SCResponse, descMVA, statusMVA,CityCode, reserve1);
//						insertSPANHostDetailsCursorAS2.destroy();
						
						insertSPANHostDetailsListAS2.add(insertSPANHostDetailsCursorAS2); //by win
						
					} else {
//						insertSPANHostDetailsList[i] = IDataFactory.create();
//						insertSPANHostDetailsCursor = insertSPANHostDetailsList.get(i);
						
						insertSPANHostDetailsCursor = putData2Pipeline( insertSPANHostDetailsCursor,  batchID,  trxHeaderID,  hostSequenceNo, 
											SenderIdentifier, DetailSenderIdentifier, DetailReceiverIdentifier,  
											CreationDateTime,  MessageIdentifier, MessageTypeIndicator,  
											DocumentDate,  DocumentNumber, BeneficiaryName, BeneficiaryBankCode,
											BeneficiaryBank,  BeneficiaryAccount, Amount, currency, 
											Description,  AgentBankCode, AgentBankAccountNumber, AgentBankAccountName,  
											EmailAddress,  SwiftCode,  IBANCode,  dbPaymentMethod,
											SP2DCount,  TotalCount,  TotalAmount,  ibtBuyRate,  
											changeInstruction,  citizenship, beneficiaryRemark1,  
											beneficiaryRemark2,  beneficiaryRemark3,  beneficiaryRemark4,  
											BeneficiaryBankCode_, ibtSellRate,  reqStatus, ReceiverIdentifier, 
											TotalBatchCount, transferMethod, "L1", descMVA, statusMVA,CityCode, reserve1);
						
//						insertSPANHostDetailsCursor.destroy();
						insertSPANHostDetailsList.add(insertSPANHostDetailsCursor); //by win
					}
					//Clear value variable
					descMVA = "";
					statusMVA = "";			
				}			
											
			}

			if(asynchronous.equals("1")){
				if(!trxType.equals("2")){
//					IDataUtil.put(insertSPANHostDetailsBatchCursor,"SPANHostBatch", insertSPANHostDetailsListAS1);
//					insertSPANHostDetailsBatchCursor.destroy();	
					mii.span.db.service.Odbc.insertSPANHostDetailsBatch(insertSPANHostDetailsListAS1);
//					wmDoInvoke("mandiri.span.db.service.odbc", "insertSPANHostDetailsBatch", insertSPANHostDetailsBatch);
				}
//				IDataUtil.put(insertSPANHostDetailsBatchCursor,"SPANHostBatch", insertSPANHostDetailsListAS2);
//				insertSPANHostDetailsBatchCursor.destroy();	
					mii.span.db.service.Odbc.insertSPANHostDetailsBatch(insertSPANHostDetailsListAS2);
//				wmDoInvoke("mandiri.span.db.service.odbc", "insertSPANHostDetailsBatch", insertSPANHostDetailsListAS2);
			}else{
//				IDataUtil.put(insertSPANHostDetailsBatchCursor,"SPANHostBatch", insertSPANHostDetailsList);
//				insertSPANHostDetailsBatchCursor.destroy();	
				mii.span.db.service.Odbc.insertSPANHostDetailsBatch(insertSPANHostDetailsList);
//				wmDoInvoke("mandiri.span.db.service.odbc", "insertSPANHostDetailsBatch", insertSPANHostDetailsList);	
				}
		
			}
		}catch(Exception ex){
			Status = "ERROR";
			ErrorMessage = ex.getMessage();
			ex.printStackTrace();
		}
		
		MappingDataHostResponseDoc response = new MappingDataHostResponseDoc();
		response.setStatus(Status);
		response.setErrorMessage(ErrorMessage );
		response.setStatusChange(status_change);
		response.setSknBankCode(sknBankCode);
		response.setSknSandiBank(sknSandiBank);
		return response;
	}

	
	
	public static String padLeft(String inString, String padString, int length){
	    if(inString==null || inString.length()<1) {
			inString = "0";
		}
	    
	    while(true){           
	        if(inString.length()<length){                    
	            inString = padString.concat(inString);
	        } else break;
	    }
	    return inString;
	}
	
	public static String padRight(String inString, String padString, int length){
		if(inString==null || inString.length()<1) {
			inString = " ";		
	    }
	    while(true){    
	        if(inString.length()<length){                
	            inString = inString.concat(padString);
	        } else break;
	    }
	    
	    return inString;
	}
	
	public static String setDecimal(String inString){
	    String transferAmount = inString;
	    
	    int decimalMark  = transferAmount.indexOf(".");
	    int lengthTransferAmount = transferAmount.length();	
		int totDecDigit = 0;
		if(decimalMark>0) {totDecDigit = lengthTransferAmount - decimalMark;}
	    if(decimalMark>0){
	        if(totDecDigit==1){	
	            transferAmount = transferAmount.concat("00");
	            transferAmount = transferAmount.replace(".", "");                
	        }
	        else if(totDecDigit==2){
				transferAmount = transferAmount.concat("0");
	            transferAmount = transferAmount.replace(".", "");                
	        }
	        else if(totDecDigit==3){
	            transferAmount = transferAmount.replace(".", "");                
	        }
	    }else {
	        transferAmount = transferAmount.concat("00");            
	    }
	
	    return transferAmount;
	} 
	
	public static String setDecimalDot(String inString){
	    String transferAmount = inString;
	    
	    int decimalMark  = transferAmount.indexOf(".");
	    int lengthTransferAmount = transferAmount.length();	
		int totDecDigit = 0;
		if(decimalMark>0) {totDecDigit = lengthTransferAmount - decimalMark;}
	    if(decimalMark>0){
	        if(totDecDigit==1){	
	            transferAmount = transferAmount.concat(".00");
	//            transferAmount = transferAmount.replace(".", "");                
	        }
	        else if(totDecDigit==2){
				transferAmount = transferAmount.concat("0");
	//            transferAmount = transferAmount.replace(".", "");                
	        }
	        else if(totDecDigit==3){
	//            transferAmount = transferAmount.replace(".", "");                
	        }
	    }else {
	        transferAmount = transferAmount.concat(".00");            
	    }
	
	    return transferAmount;
	} 
	
	/**
	public static IData wmDoInvoke(String ns, String sn, IData valueData){
		IData output = null;
	
		try {
				output = Service.doInvoke(ns, sn, valueData);
		} catch (Exception e) {
				throw new RuntimeException(e);
		}		
	
		return output;
	
	}
	**/
	
	public static SpanHostDataDetails putData2Pipeline(SpanHostDataDetails insertSPANHostDetailsCursor, String batchID, String trxHeaderID, String hostSequenceNo,
											   	String SenderIdentifier, String DetailSenderIdentifier, 
												String DetailReceiverIdentifier,String CreationDateTime, String MessageIdentifier, 
												String MessageTypeIndicator, String DocumentDate, String DocumentNumber, 
												String BeneficiaryName, String BeneficiaryBankCode, String BeneficiaryBank, 
												String BeneficiaryAccount, String Amount, String currency, String Description, 
												String AgentBankCode, String AgentBankAccountNumber, String AgentBankAccountName, 
												String EmailAddress, String SwiftCode, String IBANCode, String dbPaymentMethod, 
												String SP2DCount, String TotalCount, String TotalAmount, String ibtBuyRate, 
												String changeInstruction, String citizenship, String beneficiaryRemark1, 
												String beneficiaryRemark2, String beneficiaryRemark3, String beneficiaryRemark4, 
												String BeneficiaryBankCode_, String ibtSellRate, String reqStatus, 
												String ReceiverIdentifier, String TotalBatchCount, String trancferMethod, String LegStatus, 
												String descMVA, String statusMVA, String CityCode, String reserve1) {
			
			
			insertSPANHostDetailsCursor = new SpanHostDataDetails();//win
			
			insertSPANHostDetailsCursor.setBATCHID(batchID);
			insertSPANHostDetailsCursor.setTRXHEADERID(trxHeaderID);
			insertSPANHostDetailsCursor.setTRXDETAILID(hostSequenceNo);
			insertSPANHostDetailsCursor.setSENDER_ID(SenderIdentifier);		
			insertSPANHostDetailsCursor.setRECEIVER_ID(ReceiverIdentifier);
			insertSPANHostDetailsCursor.setDETAIL_SENDER_ID(DetailSenderIdentifier);
			insertSPANHostDetailsCursor.setDETAIL_RECEIVER_ID(DetailReceiverIdentifier);
			insertSPANHostDetailsCursor.setCREATE_DT_STRING(CreationDateTime);	
			insertSPANHostDetailsCursor.setMESSAGE_ID(MessageIdentifier);
			insertSPANHostDetailsCursor.setMSG_TYPE_ID(MessageTypeIndicator);
			insertSPANHostDetailsCursor.setDOC_DATE(DocumentDate);
			insertSPANHostDetailsCursor.setDOC_NUMBER(DocumentNumber);
			insertSPANHostDetailsCursor.setBEN_NAME(BeneficiaryName);
			insertSPANHostDetailsCursor.setBEN_BANK_CODE(BeneficiaryBankCode);
			insertSPANHostDetailsCursor.setBEN_BANK(BeneficiaryBank);
			insertSPANHostDetailsCursor.setBEN_ACCOUNT(BeneficiaryAccount);
//			insertSPANHostDetailsCursor.setAMOUNT(setDecimalDot(Amount)); comment by win, karena di bjb tidak perlu menggunakan precision
			insertSPANHostDetailsCursor.setAMOUNT(Amount);
			insertSPANHostDetailsCursor.setCURRENCY(currency);
			insertSPANHostDetailsCursor.setDESCRIPTION(Description);
			insertSPANHostDetailsCursor.setAGENT_BANK_CODE(AgentBankCode);
			insertSPANHostDetailsCursor.setAGENT_BANK_ACCT_NO(AgentBankAccountNumber);
			insertSPANHostDetailsCursor.setAGENT_BANK_ACCT_NAME(AgentBankAccountName);
			insertSPANHostDetailsCursor.setEMAIL_ADDR(EmailAddress);
			insertSPANHostDetailsCursor.setSWIFT_CODE(SwiftCode);
			insertSPANHostDetailsCursor.setIBAN_CODE(IBANCode);
			insertSPANHostDetailsCursor.setPAYMENT_METHODS(dbPaymentMethod);
			insertSPANHostDetailsCursor.setSPAN_COUNT(SP2DCount);
			insertSPANHostDetailsCursor.setTOTAL_ACCOUNT(TotalCount);
//			insertSPANHostDetailsCursor.setTOTAL_AMOUNT(setDecimalDot(TotalAmount));
			insertSPANHostDetailsCursor.setTOTAL_AMOUNT(TotalAmount);
			insertSPANHostDetailsCursor.setTOT_BATCH_ACCOUNT(TotalBatchCount);
			insertSPANHostDetailsCursor.setTRANSACTION_REF(hostSequenceNo);
			insertSPANHostDetailsCursor.setPROVIDER_REF(hostSequenceNo);
			insertSPANHostDetailsCursor.setDEBITACCNO(AgentBankAccountNumber);
			insertSPANHostDetailsCursor.setDEBITACCEXRATE(ibtBuyRate);
			insertSPANHostDetailsCursor.setDEBITACCCURRCODE(currency);
			insertSPANHostDetailsCursor.setCHARGEINSTRUCTION(changeInstruction);
			insertSPANHostDetailsCursor.setREMITTERRESFLAG(citizenship);
			insertSPANHostDetailsCursor.setREMITTERRESCODE("");
			insertSPANHostDetailsCursor.setREMITTERCITIZENFLAG(citizenship);
			insertSPANHostDetailsCursor.setREMITTERCITIZENCODE("");
			insertSPANHostDetailsCursor.setCREDITACCNO(BeneficiaryAccount);
			if(BeneficiaryName.length()>50) BeneficiaryName=BeneficiaryName.substring(0,50);
			insertSPANHostDetailsCursor.setCREDITACCNAME(BeneficiaryName);
			insertSPANHostDetailsCursor.setCREDITACCEXRATE(ibtBuyRate);
			insertSPANHostDetailsCursor.setCREDITTRFCURR(currency);
			//insertSPANHostDetailsCursor.setCREDITTRFAMOUNT(setDecimalDot(Amount)); comment by win, karena di bjb tidak perlu ada precision
			insertSPANHostDetailsCursor.setCREDITTRFAMOUNT(Amount);
			insertSPANHostDetailsCursor.setTRXREMARK1(beneficiaryRemark1);
			insertSPANHostDetailsCursor.setTRXREMARK2(beneficiaryRemark2);
			insertSPANHostDetailsCursor.setTRXREMARK3(beneficiaryRemark3);
			insertSPANHostDetailsCursor.setTRXREMARK4(beneficiaryRemark4);
			insertSPANHostDetailsCursor.setFTSERVICES(trancferMethod);
			insertSPANHostDetailsCursor.setCHARGECURRCODE1(currency);
			insertSPANHostDetailsCursor.setCHARGEAMOUNT1("0.00");
			insertSPANHostDetailsCursor.setCHARGECURRCODE2("");	
			insertSPANHostDetailsCursor.setCHARGEAMOUNT2("0.00");
			insertSPANHostDetailsCursor.setCHARGECURRCODE3("");
			insertSPANHostDetailsCursor.setCHARGEAMOUNT3("0.00");
			insertSPANHostDetailsCursor.setCHARGECURRCODE4("");
			insertSPANHostDetailsCursor.setCHARGEAMOUNT4("");
			insertSPANHostDetailsCursor.setCHARGECURRCODE5("");
			insertSPANHostDetailsCursor.setCHARGEAMOUNT5("0.00");
			insertSPANHostDetailsCursor.setBENEFICIARYADDR1("");
			insertSPANHostDetailsCursor.setBENEFICIARYADDR2("");
			insertSPANHostDetailsCursor.setBENEFICIARYADDR3("");
			insertSPANHostDetailsCursor.setBENEFICIARYBANKCODE(BeneficiaryBankCode_);
			insertSPANHostDetailsCursor.setBENEFICIARYBANKNAME(BeneficiaryBank);
			insertSPANHostDetailsCursor.setBENEFICIARYBANKBRANCHNAME("");
			insertSPANHostDetailsCursor.setBENEFICIARYBANKADDR1("");
			insertSPANHostDetailsCursor.setBENEFICIARYBANKADDR2("");
			insertSPANHostDetailsCursor.setBENEFICIARYBANKADDR3("");
			insertSPANHostDetailsCursor.setBENEFICIARYBANKCITYNAME("");
			insertSPANHostDetailsCursor.setBENEFICIARYBANKCOUNTRYNAME("");
			insertSPANHostDetailsCursor.setBENEFICIARYRESFLAG("");
			insertSPANHostDetailsCursor.setBENEFICIARYRESCODE("");
			insertSPANHostDetailsCursor.setBENEFICIARYCITIZENFLAG(citizenship);
			insertSPANHostDetailsCursor.setBENEFICIARYCITIZENCODE("");
			insertSPANHostDetailsCursor.setDEBITREFNO("");
			insertSPANHostDetailsCursor.setFINALIZEPAYMENTFLAG("");
			insertSPANHostDetailsCursor.setCREDITREFNO("");
			insertSPANHostDetailsCursor.setBENEFICIARYBANKCITYCODE(CityCode);
			insertSPANHostDetailsCursor.setORGANIZATIONDIRNAME("");
			insertSPANHostDetailsCursor.setPURPOSEOFTRX("");
			insertSPANHostDetailsCursor.setREMITTANCECODE1("");
			insertSPANHostDetailsCursor.setREMITTANCEINFO1("");
			insertSPANHostDetailsCursor.setREMITTANCECODE2("");
			insertSPANHostDetailsCursor.setREMITTANCEINFO2("");
			insertSPANHostDetailsCursor.setREMITTANCECODE3("");
			insertSPANHostDetailsCursor.setREMITTANCEINFO3("");
			insertSPANHostDetailsCursor.setREMITTANCECODE4("");
			insertSPANHostDetailsCursor.setREMITTANCEINFO4("");
			insertSPANHostDetailsCursor.setINSTRUCTIONCODE1("1");
			insertSPANHostDetailsCursor.setINSTRUCTIONREMARK1("");
			insertSPANHostDetailsCursor.setINSTRUCTIONCODE2("");
			insertSPANHostDetailsCursor.setINSTRUCTIONREMARK2("");
			insertSPANHostDetailsCursor.setINSTRUCTIONCODE3("");
			insertSPANHostDetailsCursor.setINSTRUCTIONREMARK3("");
			insertSPANHostDetailsCursor.setSWIFTMETHOD("");
			insertSPANHostDetailsCursor.setIBTBUYRATE(ibtBuyRate);
			insertSPANHostDetailsCursor.setIBTSELLRATE(ibtSellRate);
			insertSPANHostDetailsCursor.setVALUEDATE(new SimpleDateFormat("yyyyMMdd").format(new Date()));
			//insertSPANHostDetailsCursor.setDEBITAMOUNT(setDecimalDot(Amount)); by win
			insertSPANHostDetailsCursor.setDEBITAMOUNT(Amount);
			/** 
			 * reserve1 = bicSKN
			 * reserve2 = Document No
			 * reserve3 = Debit Account Name 	
			 * insertSPANHostDetailsCursor.setRESERVE1("");
			 * insertSPANHostDetailsCursor.setRESERVE2("");
			 * insertSPANHostDetailsCursor.setRESERVE3("");
			 **/
			insertSPANHostDetailsCursor.setRESERVE1(reserve1);
			insertSPANHostDetailsCursor.setRESERVE2(DocumentNumber);
			insertSPANHostDetailsCursor.setRESERVE3(AgentBankAccountName);
			insertSPANHostDetailsCursor.setRESERVE4("");
			insertSPANHostDetailsCursor.setRESERVE5("");
			insertSPANHostDetailsCursor.setSTATUS(reqStatus);
			insertSPANHostDetailsCursor.setINSERTDT(new Date());
			insertSPANHostDetailsCursor.setLEGSTATUS(LegStatus);
			insertSPANHostDetailsCursor.setMVA_DESC(descMVA);
			insertSPANHostDetailsCursor.setMVA_STATUS(statusMVA);
				
		return insertSPANHostDetailsCursor;
	
	}
	
	public static String write2File(String dirPath, String filename, String strToWrite){
		
		String ErrorMessage = "";
		try{
	
			dirPath = dirPath.replace('\\', '/');
			File checkDirPath = new File (dirPath);
	
			if (!checkDirPath.isDirectory()) 
				checkDirPath.mkdirs();
			
			String absFileName = dirPath + "/" + filename;
			FileOutputStream fos = new FileOutputStream(absFileName);
	
			if (strToWrite != null)	
			{
				int len = strToWrite.length();
				for(int i = 0; i < len; i++) 
				{
	                byte b = (byte) strToWrite.charAt(i);
					fos.write(b);
				}
			} 
	
			fos.flush();
			fos.close();
			
		} 
		catch (Exception exc) {
			ErrorMessage = exc.toString();
		}
	
	//	System.out.println("ErrorMessage = "+ErrorMessage);
	
		return ErrorMessage;
	}
}
