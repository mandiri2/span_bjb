package mii.span.scheduller.odbc;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import mii.span.model.SpanRekeningKoran;
import mii.span.model.THQEQBJBModel;
import mii.span.util.BusinessUtil;

import com.mii.constant.Constants;
import com.mii.dao.SpanHostDataDetailsDao;
import com.mii.dao.SpanRekeningKoranDao;
import com.mii.dao.SystemParameterDAO;
import com.mii.dao.THEQBJBDao;
import com.mii.models.SystemParameter;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.ApplicationInstance;

@ManagedBean
@ViewScoped
public class GetTransactionHistory implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ManagedProperty(value = "#{applicationBean}")
	private static ApplicationBean applicationBean;
	
	private THEQBJBDao getTHEQBJBDao(){
		applicationBean = ApplicationInstance.getInstance().getApplicationBean();
		return (THEQBJBDao) applicationBean.getFileProcessContext().getBean("thEQBJBDao");
	}
	
	private SpanRekeningKoranDao getSpanRekeningKoranDao(){
		applicationBean = ApplicationInstance.getInstance().getApplicationBean();
		return (SpanRekeningKoranDao) applicationBean.getFileProcessContext().getBean("SpanRekeningKoranDao");
	}
	
	private SystemParameterDAO getSystemParameterDao(){
		return (SystemParameterDAO) BusinessUtil.getDao("sysParamDAO");
	}
	
	private int getLimitTH(){
		SystemParameter param = getSystemParameterDao().getValueParameter(Constants.LIMIT_GET_TH).get(0);
		return Integer.parseInt(param.getParam_value());
	}
	
	public void run(){
		System.out.println("Start Run GetTransactionHistory");
		List<THQEQBJBModel> result = new ArrayList<THQEQBJBModel>();
		result = getTHEQBJBDao().getTHList(getLimitTH());
		System.out.println("Size List from EQ: "+result.size());
		List<BigDecimal> listId = new ArrayList<BigDecimal>();
		for(THQEQBJBModel thEQBJB : result){
			SpanRekeningKoran srk = new SpanRekeningKoran();
			srk = mappingTHEQData(srk, thEQBJB);
			getSpanRekeningKoranDao().save(srk);
			listId.add(thEQBJB.getyID());
		}
		getTHEQBJBDao().deleteAquiredData(listId);
		System.out.println("End Run GetTransactionHistory");
	}

	private SpanRekeningKoran mappingTHEQData(SpanRekeningKoran srk, THQEQBJBModel thEQBJB) {
		srk.setyId(thEQBJB.getyID());
		srk.setTransactionDate(thEQBJB.getyPDATE());
		srk.setBankTransactionCode(thEQBJB.getyTCD());
		srk.setDebit(thEQBJB.getyAMT1().trim());
		srk.setCredit(thEQBJB.getyAMT2().trim());
		srk.setBankReferenceNumber(thEQBJB.getyREFF());
		srk.setTotalAmount(thEQBJB.getyAMT3().trim());
		srk.setBankAccountNumber(thEQBJB.getyREK().trim());
		return srk;
	}
}
