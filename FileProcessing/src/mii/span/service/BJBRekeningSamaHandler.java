package mii.span.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.mii.dao.SpanHostResponseDao;

import mii.span.model.SpanHostResponse;
import mii.span.model.SpanRekeningKoran;

public class BJBRekeningSamaHandler {
	
	public static List<SpanRekeningKoran> getTransaksiRekeningSama(SpanHostResponseDao spanHostResponseDao, String accountNo){
		List<SpanRekeningKoran> listRekKoran = new ArrayList<SpanRekeningKoran>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat sdf2 = new SimpleDateFormat("ddMMMyy");
		List<SpanHostResponse> spanHostResponseList = spanHostResponseDao.getTransaksiRekeningSama(accountNo);
		
		for(SpanHostResponse response : spanHostResponseList){
			String ZHAMDR = "";
			String ZHAMCR = response.getDebitAmount();
			String ZHPOD;
			try {
				ZHPOD = sdf2.format(sdf.parse(response.getDocumentDate()));
			} catch (ParseException e) {
				ZHPOD = "";
				e.printStackTrace();
			}
			String ZHRBAL = "0";
			String ZHTCD = "519";
			String ZHNR = response.getDocumentNo();
			SpanRekeningKoran s = setRekeningKoran("0", ZHAMDR, ZHAMCR, ZHPOD, ZHRBAL, ZHTCD, ZHNR);
			listRekKoran.add(s);
		}
		return listRekKoran;
	}
	
	private static SpanRekeningKoran setRekeningKoran(String key,
			String ZHAMDR, String ZHAMCR, String ZHPOD, String ZHRBAL,
			String ZHTCD, String ZHNR) {
		SpanRekeningKoran rek = new SpanRekeningKoran();
		rek.setKey(Integer.parseInt(key));
		rek.setTransactionDate(ZHPOD);
		rek.setBankTransactionCode(ZHTCD);
		rek.setDebit(ZHAMDR);
		rek.setCredit(ZHAMCR);
		rek.setBankReferenceNumber(ZHNR);
		rek.setTotalAmount(ZHRBAL);
		return rek;
	}
	
}
