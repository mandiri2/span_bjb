package mii.span.service;

import java.util.List;

import mii.span.db.service.Odbc;
import mii.span.process.model.InsertSPANLogTrxUserResponse;
import mii.span.process.model.SpanInputRequest;
import mii.span.process.model.SpanReportResponse;

public class Report {
	
	/**
	 * mandiri.span.service.report:spanReport
	 */
	public static SpanReportResponse spanReport(List<SpanInputRequest> filesInput){
		SpanReportResponse spanReportResponse = new SpanReportResponse();
		String countSPANUserLog = null;
		String USERID = null;
		String Status = null;
		String ErrorMessage = null;
		for(SpanInputRequest FilesInput2 : filesInput){
			//o mandiri.span.db.service.odbc:selectCountSPANUserLog
			countSPANUserLog = Odbc.selectCountSPANUserLog(FilesInput2.getFileName());
			//c mandiri.span.db.service.odbc:selectCountSPANUserLog
			//o mandiri.span.db.service.odbc:getUserID
			USERID = Odbc.getUserID(FilesInput2.getFileName(), countSPANUserLog);
			//c mandiri.span.db.service.odbc:getUserID
			//o mandiri.span.db.service.odbc:insertSPANLogTrxUser
			InsertSPANLogTrxUserResponse inserSpanLogTrxUserResponse = Odbc.insertSPANLogTrxUser(FilesInput2.getFileName(), USERID);
			Status = inserSpanLogTrxUserResponse.getoSTATUS();
			ErrorMessage = inserSpanLogTrxUserResponse.getoERRORMESSAGE();
			//c mandiri.span.db.service.odbc:insertSPANLogTrxUser
		}
		
		spanReportResponse.setStatus(Status);
		spanReportResponse.setErrorMessage(ErrorMessage);
		return spanReportResponse;
	}
}
