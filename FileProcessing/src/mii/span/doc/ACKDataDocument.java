package mii.span.doc;

public class ACKDataDocument {
	private String DocumentDate,
		DocumentType,
		DocumentNo,
		FileName,
		ReturnCode,
		Description;
	
	/**Constructor**/
	public ACKDataDocument() {
	}
	
	public ACKDataDocument(String fileName, String documentType,
			String documentDate) {
		super();
		DocumentDate = documentDate;
		DocumentType = documentType;
		FileName = fileName;
	}

	/**GETTER SETTER**/
	public String getDocumentDate() {
		return DocumentDate;
	}

	public void setDocumentDate(String documentDate) {
		DocumentDate = documentDate;
	}

	public String getDocumentType() {
		return DocumentType;
	}

	public void setDocumentType(String documentType) {
		DocumentType = documentType;
	}

	public String getDocumentNo() {
		return DocumentNo;
	}

	public void setDocumentNo(String documentNo) {
		DocumentNo = documentNo;
	}

	public String getFileName() {
		return FileName;
	}

	public void setFileName(String fileName) {
		FileName = fileName;
	}

	public String getReturnCode() {
		return ReturnCode;
	}

	public void setReturnCode(String returnCode) {
		ReturnCode = returnCode;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}
	
	
	
}
