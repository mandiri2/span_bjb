package mii.span.doc;

public class FileSFTP {
	private byte[] bytes;
	private String fileName;
	private String status;
	private String errorMessage;
	private String fileNameOri;
	
	/**GETTER SETTER**/
	public byte[] getBytes() {
		return bytes;
	}
	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public String getFileNameOri() {
		return fileNameOri;
	}
	public void setFileNameOri(String fileNameOri) {
		this.fileNameOri = fileNameOri;
	}
}
