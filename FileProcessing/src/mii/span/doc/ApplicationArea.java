package mii.span.doc;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement (name =  "ApplicationArea")
public class ApplicationArea {
	private String ApplicationAreaSenderIdentifier,
	ApplicationAreaReceiverIdentifier,
	ApplicationAreaDetailSenderIdentifier,
	ApplicationAreaDetailReceiverIdentifier,
	ApplicationAreaCreationDateTime,
	ApplicationAreaMessageIdentifier,
	ApplicationAreaMessageTypeIndicator,
	ApplicationAreaMessageVersionText;

	public String getApplicationAreaSenderIdentifier() {
		return ApplicationAreaSenderIdentifier;
	}
	
	@XmlElement (name = "ApplicationAreaSenderIdentifier")
	public void setApplicationAreaSenderIdentifier(
			String applicationAreaSenderIdentifier) {
		ApplicationAreaSenderIdentifier = applicationAreaSenderIdentifier;
	}

	public String getApplicationAreaReceiverIdentifier() {
		return ApplicationAreaReceiverIdentifier;
	}

	@XmlElement (name = "ApplicationAreaReceiverIdentifier")
	public void setApplicationAreaReceiverIdentifier(
			String applicationAreaReceiverIdentifier) {
		ApplicationAreaReceiverIdentifier = applicationAreaReceiverIdentifier;
	}

	public String getApplicationAreaDetailSenderIdentifier() {
		return ApplicationAreaDetailSenderIdentifier;
	}

	@XmlElement (name = "ApplicationAreaDetailSenderIdentifier")
	public void setApplicationAreaDetailSenderIdentifier(
			String applicationAreaDetailSenderIdentifier) {
		ApplicationAreaDetailSenderIdentifier = applicationAreaDetailSenderIdentifier;
	}

	public String getApplicationAreaDetailReceiverIdentifier() {
		return ApplicationAreaDetailReceiverIdentifier;
	}

	@XmlElement (name = "ApplicationAreaDetailReceiverIdentifier")
	public void setApplicationAreaDetailReceiverIdentifier(
			String applicationAreaDetailReceiverIdentifier) {
		ApplicationAreaDetailReceiverIdentifier = applicationAreaDetailReceiverIdentifier;
	}

	public String getApplicationAreaCreationDateTime() {
		return ApplicationAreaCreationDateTime;
	}

	@XmlElement (name = "ApplicationAreaCreationDateTime")
	public void setApplicationAreaCreationDateTime(
			String applicationAreaCreationDateTime) {
		ApplicationAreaCreationDateTime = applicationAreaCreationDateTime;
	}

	public String getApplicationAreaMessageIdentifier() {
		return ApplicationAreaMessageIdentifier;
	}

	@XmlElement (name = "ApplicationAreaMessageIdentifier")
	public void setApplicationAreaMessageIdentifier(
			String applicationAreaMessageIdentifier) {
		ApplicationAreaMessageIdentifier = applicationAreaMessageIdentifier;
	}

	public String getApplicationAreaMessageTypeIndicator() {
		return ApplicationAreaMessageTypeIndicator;
	}

	@XmlElement (name = "ApplicationAreaMessageTypeIndicator")
	public void setApplicationAreaMessageTypeIndicator(
			String applicationAreaMessageTypeIndicator) {
		ApplicationAreaMessageTypeIndicator = applicationAreaMessageTypeIndicator;
	}

	public String getApplicationAreaMessageVersionText() {
		return ApplicationAreaMessageVersionText;
	}

	@XmlElement (name = "ApplicationAreaMessageVersionText")
	public void setApplicationAreaMessageVersionText(
			String applicationAreaMessageVersionText) {
		ApplicationAreaMessageVersionText = applicationAreaMessageVersionText;
	}
}
