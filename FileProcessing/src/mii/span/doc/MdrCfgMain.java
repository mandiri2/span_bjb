package mii.span.doc;

public class MdrCfgMain {
	private String keygroup;
	private String keyname;
	private String value1;
	private String value2;
	private String description;
	private String objhashname;
	private String enabled;
	
	/**GETTER SETTER**/
	public String getKeyname() {
		return keyname;
	}
	public void setKeyname(String keyname) {
		this.keyname = keyname;
	}
	public String getValue1() {
		return value1;
	}
	public void setValue1(String value1) {
		this.value1 = value1;
	}
	public String getObjhashname() {
		return objhashname;
	}
	public void setObjhashname(String objhashname) {
		this.objhashname = objhashname;
	}
	public String getKeygroup() {
		return keygroup;
	}
	public void setKeygroup(String keygroup) {
		this.keygroup = keygroup;
	}
	public String getValue2() {
		return value2;
	}
	public void setValue2(String value2) {
		this.value2 = value2;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getEnabled() {
		return enabled;
	}
	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}
}
