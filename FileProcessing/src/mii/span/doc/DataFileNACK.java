package mii.span.doc;

public class DataFileNACK {

	private String filename;
	private String xmlfilename;

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getXmlfilename() {
		return xmlfilename;
	}

	public void setXmlfilename(String xmlfilename) {
		this.xmlfilename = xmlfilename;
	}

}
