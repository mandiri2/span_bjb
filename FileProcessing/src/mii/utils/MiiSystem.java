package mii.utils;

public class MiiSystem {
	public static long getTimeMillis(Long startTime){
		long timeMillis = System.currentTimeMillis();

		if (startTime!=null){
		  timeMillis -= startTime;
		}

		return timeMillis;
	}
}
