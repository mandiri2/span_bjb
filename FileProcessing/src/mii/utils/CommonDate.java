package mii.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

//import java.util.Locale;
import org.apache.commons.lang.StringUtils;

import com.mii.logger.Log;
import com.mii.logger.LoggerEvent;

public class CommonDate {
	
	private static final Log log = Log.getInstance(CommonDate.class);

	public static String getCurrentDateString(String dateFormat){
		return new SimpleDateFormat(dateFormat).format(new Date( ));
	}
	
	public static Date getCurrentDate(String dateFormat){
		try {
			return new SimpleDateFormat(dateFormat).parse(getCurrentDateString(dateFormat));
		} catch (ParseException e) {
			e.printStackTrace();
			LoggerEvent.errorProcess(log, CommonDate.class.getName()+"/"+CommonDate.class.getMethods().toString(), e.getMessage());
			return null;
		}
	}
	
	public static Date stringToDate(String dateFormat, String dateTime){
		try {
			Date date;
			if("".equalsIgnoreCase(dateTime) || dateTime.isEmpty()){
				date = getCurrentDate(dateFormat);
			}else{
				if(StringUtils.isNumeric(dateTime)){
					date = new SimpleDateFormat(dateFormat).parse(dateTime);
				}else{
					date = getCurrentDate(dateFormat);
				}
			}
			
			return date;
		} catch (Exception e) {
			e.printStackTrace();
			LoggerEvent.errorProcess(log, CommonDate.class.getName()+"/"+CommonDate.class.getMethods().toString(), e.getMessage());
			return getCurrentDate(dateFormat);
		}
	}
	
	/**
	 * convert date ke string sesuai dengan patter
	 * @return
	 */
	public static String dateToString(String pattern, Date date){
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(pattern);
			return sdf.format(date);
		} catch (Exception e) {
			System.out.println("gagal parsing data");
		}
		return null;
	}
	
	public static String getNextDateString(String dateFormat){
	    Calendar calendar = Calendar.getInstance();
	    calendar.add(Calendar.DAY_OF_YEAR, 1);
	    
		return new SimpleDateFormat(dateFormat).format(calendar.getTime());
	}
	
	/**
	 * Mengambil Date Sekarang dengan time 0
	 * @param date
	 * @return
	 */
	public static Date getDateWithoutTime(Date date) {
	    Calendar cal = Calendar.getInstance();
	    cal.setTime(date);
	    cal.set(Calendar.HOUR_OF_DAY, 0);
	    cal.set(Calendar.MINUTE, 0);
	    cal.set(Calendar.SECOND, 0);
	    cal.set(Calendar.MILLISECOND, 0);
	    return cal.getTime();
	}
	
}
