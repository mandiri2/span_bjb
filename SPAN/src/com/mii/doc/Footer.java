package com.mii.doc;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement (name = "Footer")
public class Footer {
	private String TotalCount,
	TotalAmount,
	TotalBatchCount;

	public String getTotalCount() {
		return TotalCount;
	}

	@XmlElement (name = "TotalCount")
	public void setTotalCount(String totalCount) {
		TotalCount = totalCount;
	}

	public String getTotalAmount() {
		return TotalAmount;
	}

	@XmlElement	 (name = "TotalAmount")
	public void setTotalAmount(String totalAmount) {
		TotalAmount = totalAmount;
	}

	public String getTotalBatchCount() {
		return TotalBatchCount;
	}

	@XmlElement  (name = "TotalBatchCount")
	public void setTotalBatchCount(String totalBatchCount) {
		TotalBatchCount = totalBatchCount;
	}
}
