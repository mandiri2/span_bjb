package com.mii.doc;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement (name = "DataArea")
public class DataArea {
	private String DocumentDate,
	DocumentNumber,
	BeneficiaryName,
	BeneficiaryBankCode,
	BeneficiaryBank,
	BeneficiaryAccount,
	Amount,
	CurrencyTarget,
	Description,
	AgentBankCode,
	AgentBankAccountNumber,
	AgentBankAccountName,
	EmailAddress,
	SwiftCode,
	IBANCode,
	PaymentMethod,
	SP2DCount;
	
	public String getDocumentDate() {
		return DocumentDate;
	}

	@XmlElement (name = "DocumentDate")
	public void setDocumentDate(String documentDate) {
		DocumentDate = documentDate;
	}

	public String getDocumentNumber() {
		return DocumentNumber;
	}

	@XmlElement (name = "DocumentNumber")
	public void setDocumentNumber(String documentNumber) {
		DocumentNumber = documentNumber;
	}

	public String getBeneficiaryName() {
		return BeneficiaryName;
	}

	@XmlElement (name = "BeneficiaryName")
	public void setBeneficiaryName(String beneficiaryName) {
		BeneficiaryName = beneficiaryName;
	}

	public String getBeneficiaryBankCode() {
		return BeneficiaryBankCode;
	}

	@XmlElement (name = "BeneficiaryBankCode")
	public void setBeneficiaryBankCode(String beneficiaryBankCode) {
		BeneficiaryBankCode = beneficiaryBankCode;
	}

	public String getBeneficiaryBank() {
		return BeneficiaryBank;
	}

	@XmlElement (name = "BeneficiaryBank")
	public void setBeneficiaryBank(String beneficiaryBank) {
		BeneficiaryBank = beneficiaryBank;
	}

	public String getBeneficiaryAccount() {
		return BeneficiaryAccount;
	}

	@XmlElement (name = "BeneficiaryAccount")
	public void setBeneficiaryAccount(String beneficiaryAccount) {
		BeneficiaryAccount = beneficiaryAccount;
	}

	public String getAmount() {
		return Amount;
	}

	@XmlElement (name = "Amount")
	public void setAmount(String amount) {
		Amount = amount;
	}

	public String getCurrencyTarget() {
		return CurrencyTarget;
	}

	@XmlElement (name = "CurrencyTarget")
	public void setCurrencyTarget(String currencyTarget) {
		CurrencyTarget = currencyTarget;
	}

	public String getDescription() {
		return Description;
	}

	@XmlElement(name = "Description")
	public void setDescription(String description) {
		Description = description;
	}

	public String getAgentBankCode() {
		return AgentBankCode;
	}

	@XmlElement (name = "AgentBankCode")
	public void setAgentBankCode(String agentBankCode) {
		AgentBankCode = agentBankCode;
	}

	public String getAgentBankAccountNumber() {
		return AgentBankAccountNumber;
	}
	
	@XmlElement(name = "AgentBankAccountNumber")
	public void setAgentBankAccountNumber(String agentBankAccountNumber) {
		AgentBankAccountNumber = agentBankAccountNumber;
	}

	public String getAgentBankAccountName() {
		return AgentBankAccountName;
	}

	@XmlElement (name="AgentBankAccountName")
	public void setAgentBankAccountName(String agentBankAccountName) {
		AgentBankAccountName = agentBankAccountName;
	}

	public String getEmailAddress() {
		return EmailAddress;
	}

	@XmlElement (name = "EmailAddress")
	public void setEmailAddress(String emailAddress) {
		EmailAddress = emailAddress;
	}

	public String getSwiftCode() {
		return SwiftCode;
	}

	@XmlElement (name = "SwiftCode")
	public void setSwiftCode(String swiftCode) {
		SwiftCode = swiftCode;
	}

	public String getIBANCode() {
		return IBANCode;
	}

	@XmlElement (name = "IBANCode")
	public void setIBANCode(String iBANCode) {
		IBANCode = iBANCode;
	}

	public String getPaymentMethod() {
		return PaymentMethod;
	}

	@XmlElement (name="PaymentMethod")
	public void setPaymentMethod(String paymentMethod) {
		PaymentMethod = paymentMethod;
	}

	public String getSP2DCount() {
		return SP2DCount;
	}

	@XmlElement (name = "SP2DCount")
	public void setSP2DCount(String sP2DCount) {
		SP2DCount = sP2DCount;
	}
	
}
