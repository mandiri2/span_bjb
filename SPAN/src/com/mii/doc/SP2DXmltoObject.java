package com.mii.doc;

import java.io.StringReader;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import com.mii.doc.DataArea;
import com.mii.doc.SP2DDocument;

public class SP2DXmltoObject {
	public static void main(String[] args) {
		List<DataArea> dataAreaList = ConvertSP2DXMLToObject("<?xml version=\"1.0\" encoding=\"UTF-8\"?> <SP2D> <ApplicationArea> <ApplicationAreaSenderIdentifier>SPAN</ApplicationAreaSenderIdentifier> <ApplicationAreaReceiverIdentifier>BTPN</ApplicationAreaReceiverIdentifier> <ApplicationAreaDetailSenderIdentifier/> <ApplicationAreaDetailReceiverIdentifier/> <ApplicationAreaCreationDateTime>2016-06-06 13:08:36</ApplicationAreaCreationDateTime> <ApplicationAreaMessageIdentifier>523213000990_SP2D_O_20160606_113210_790.xml</ApplicationAreaMessageIdentifier> <ApplicationAreaMessageTypeIndicator>SP2D</ApplicationAreaMessageTypeIndicator> <ApplicationAreaMessageVersionText>1.0</ApplicationAreaMessageVersionText> </ApplicationArea> <DataArea> <DocumentDate>2016-06-06</DocumentDate> <DocumentNumber>160191506000014000001</DocumentNumber> <BeneficiaryName>RINA PURWANINGTYAS</BeneficiaryName> <BeneficiaryBankCode>523213000990</BeneficiaryBankCode> <BeneficiaryBank>BANK TABUNGAN PENSIUNAN NASIONAL</BeneficiaryBank> <BeneficiaryAccount>00102003381</BeneficiaryAccount> <Amount>17</Amount> <CurrencyTarget>IDR</CurrencyTarget> <Description>Pembayaran Gaji bulan Mei 2016 untuk 1 pegawai / 1 jiwa - SDE</Description> <AgentBankCode>523213000990</AgentBankCode> <AgentBankAccountNumber>00013000026</AgentBankAccountNumber> <AgentBankAccountName>RPKBUNP GAJI BTPN</AgentBankAccountName> <EmailAddress/> <SwiftCode>-</SwiftCode> <IBANCode>-</IBANCode> <PaymentMethod>0</PaymentMethod> <SP2DCount>1</SP2DCount> </DataArea> <Footer> <TotalCount>1</TotalCount> <TotalAmount>17</TotalAmount> <TotalBatchCount>1</TotalBatchCount> </Footer> </SP2D> ");
		
		System.out.println("size " + dataAreaList.size());
		int i = 0;
		for (Iterator<DataArea> iterator = dataAreaList.iterator(); iterator.hasNext();) {
			DataArea dataArea = (DataArea) iterator.next();
			
			System.out.println(i);
			System.out.println(dataArea.getAgentBankAccountName());
			System.out.println(dataArea.getAgentBankAccountNumber());
			System.out.println(dataArea.getAmount());
			System.out.println(dataArea.getBeneficiaryName());
			System.out.println("");
			i++;	
		}
	}
	
	public static List<DataArea> ConvertSP2DXMLToObject(String SP2DXML){
		try{
			JAXBContext jaxbContext = JAXBContext.newInstance(SP2DDocument.class);
	
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			
			StringReader reader = new StringReader(SP2DXML);
			SP2DDocument spandoc = (SP2DDocument) jaxbUnmarshaller.unmarshal(reader);
			
			return spandoc.getDataArea();
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
}
