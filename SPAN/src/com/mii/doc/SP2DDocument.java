package com.mii.doc;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement (name="SP2D")
public class SP2DDocument {
	private ApplicationArea ApplicationArea;
	private List<DataArea> DataArea;
	private Footer Footer;
	
	
	public ApplicationArea getApplicationArea() {
		return ApplicationArea;
	}
	@XmlElement(name="ApplicationArea")
	public void setApplicationArea(ApplicationArea applicationArea) {
		ApplicationArea = applicationArea;
	}
	public List<DataArea> getDataArea() {
		return DataArea;
	}
	//@XmlElementWrapper (name="DataAreaList")
	@XmlElement(name = "DataArea")
	public void setDataArea(List<DataArea> dataArea) {
		DataArea = dataArea;
	}
	public Footer getFooter() {
		return Footer;
	}
	@XmlElement (name="Footer")
	public void setFooter(Footer footer) {
		Footer = footer;
	}
}
