package com.mii.is;

import java.math.BigInteger;


import com.mii.dao.SpanVoidDataTrxDao;
import com.mii.helpers.FacesUtil;
import com.mii.is.io.DeleteSPANVoidDataTransactionRequest;

public class DeleteSPANVoidDataTransaction {
	
	public static void unvoidDocumentNumber(DeleteSPANVoidDataTransactionRequest deleteSPANVoidDataTransactionRequest,
			SpanVoidDataTrxDao spanVoidDataTrxDao){
		String amount;
		try {
			amount = selectSpanVoidDataTrxByDocNumber(deleteSPANVoidDataTransactionRequest.getDocumentNo(), spanVoidDataTrxDao);
			deleteSPANVoidDataTransaction(deleteSPANVoidDataTransactionRequest.getFileName(), deleteSPANVoidDataTransactionRequest.getDocumentNo(), spanVoidDataTrxDao);
			String sql = "UPDATE SPAN_HOST_DATA_DETAILS SET STATUS= '0' "
					+ " WHERE DOC_NUMBER = '"+deleteSPANVoidDataTransactionRequest.getDocumentNo()+"'";
			System.out.println("SQL : "+sql);
			spanVoidDataTrxDao.updateDao(sql); //execute update
			BigInteger totalAmount = new BigInteger("0");
			totalAmount = totalAmount.add(new BigInteger(amount));
			String sql2 = "UPDATE SPAN_DATA_VALIDATION SET PROC_STATUS = '1', "
					+ " TOTAL_AMOUNT = cast(cast(TOTAL_AMOUNT as numeric) + "+totalAmount.toString()+" as character varying), "
					+ " TOTAL_RECORD = cast(cast(TOTAL_RECORD as numeric) + 1  as character varying)"
					+ " WHERE FILE_NAME = '"+deleteSPANVoidDataTransactionRequest.getFileName()+"'";
			System.out.println("SQL2 : "+sql2);
			spanVoidDataTrxDao.updateDao(sql2); //execute update
			System.out.println("Unvoid Success.");
			String headerMessage = "Success";
			String bodyMessage = "Unvoid Success. ";
			FacesUtil.setInfoMessageDialog(headerMessage, bodyMessage);
		} catch (Exception e) {
			System.out.println("Failed to Unvoid.");
			String headerMessage = "Error";
			String bodyMessage = "Failed to Unvoid. "+e.getMessage();
			FacesUtil.setErrorMessageDialog(headerMessage, bodyMessage);
			e.printStackTrace();
		}
		
	}

	private static void deleteSPANVoidDataTransaction(String filename, String documentNumber, SpanVoidDataTrxDao spanVoidDataTrxDao) throws Exception{
		spanVoidDataTrxDao.deleteSPANVoidDataTransaction(filename, documentNumber);
	}

	private static String selectSpanVoidDataTrxByDocNumber(String documentNumber, SpanVoidDataTrxDao spanVoidDataTrxDao) throws Exception{
		String amount = spanVoidDataTrxDao.selectSpanVoidDataTrxAmountByDocNumber(documentNumber);
		return amount;
	}
}
