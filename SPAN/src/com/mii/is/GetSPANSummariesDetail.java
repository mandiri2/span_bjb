package com.mii.is;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.mii.dao.SPANDataValidationDao;
import com.mii.dao.SystemParameterDao;
import com.mii.dao.ToBeProcessDao;
import com.mii.doc.DataArea;
import com.mii.doc.SP2DDocument;
import com.mii.is.io.GetSPANSummariesDetailRequest;
import com.mii.is.io.GetSPANSummariesDetailResponse;
import com.mii.is.io.GetSPANSummariesRequest;
import com.mii.is.io.GetSPANSummariesResponse;
import com.mii.is.io.SPANDocument;
import com.mii.is.io.ToGetDetailsOutputResponse;
import com.mii.is.io.helper.DetailsOutputList;
import com.mii.is.io.helper.ReturCodes;
import com.mii.is.io.helper.SPANSummaries;
import com.mii.is.io.helper.SelectFNDataValidationOutput;
import com.mii.is.io.helper.StatusDetailOutput;
import com.mii.is.util.PubUtil;

public class GetSPANSummariesDetail {
	private static List<StatusDetailOutput> statusDetailOutputs;
	private static List<SPANSummaries>  searchByList;

	public static GetSPANSummariesDetailResponse getSPANSummariesDetail(
			GetSPANSummariesDetailRequest request,
			SPANDataValidationDao spanDataValidationDao,
			SystemParameterDao systemParameterDao, 
			String accountType) throws Exception {

		GetSPANSummariesDetailResponse response = new GetSPANSummariesDetailResponse();
		DetailsOutputList detailOutputList = null;

		// map
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		Date date = new Date();
		String dateTime = dateFormat.format(date);
		String legStatus = "";
		int successACKStatus = 0;
		int returACKStatus = 0;
		int grandTotalAmount = 0;
		int grandTotalRecord = 0;

		// branch on legStatus
		if (request.getLegStatus() == null) {
			legStatus = "L1";
		} else {
			legStatus = request.getLegStatus();
		}

		// branch on filename
		String totalAmountTemp = "";
		
		List<DetailsOutputList> detailsOutput = new ArrayList<>();
		List<DetailsOutputList> detailsOutputList = new ArrayList<>();
		
		if (request.getFilename() == null) {
			/**
			 * (getSPANSummaries) with input [sumType] mapping the output to
			 * variable SPANSummaries
			 */
			GetSPANSummariesRequest requestSummaries = new GetSPANSummariesRequest();
			requestSummaries.setSumType("2");
			// FIXME remove hardcode
			requestSummaries.setStatus("1");

			GetSPANSummariesResponse responseSummaries = new GetSPANSummariesResponse();
			responseSummaries = GetSPANSummaries
					.getSPANSummaries(requestSummaries, spanDataValidationDao,
							systemParameterDao, accountType);
			System.out.println(responseSummaries);
			// mapping
			successACKStatus = 0;
			returACKStatus = 0;

			List<SPANSummaries> SPANSummaries = responseSummaries
					.getSPANSummaries();
			searchByList = new ArrayList<>();
			List<SPANSummaries> newSPANSummaries = new ArrayList<>();
			// switch on searchBy
			switch (request.getSearchBy()) {
			case "0": // open
				// loop
				for (SPANSummaries s : SPANSummaries) {
					if (request.getParams().equalsIgnoreCase(s.getFileName())) {
						// map
						searchByList.add(s);
						returACKStatus = 0;
						successACKStatus = 0;

						// loop
						for (SPANSummaries ss : searchByList) {
							if (ss.getProcStatus().equalsIgnoreCase("5")) {
								/**
								 * querying (getReturDataDetails) with input
								 * [fileName] mapping the output to variable
								 * returACKStatus
								 */
								returACKStatus = Integer
										.parseInt(spanDataValidationDao
												.getReturDataDetails(
														ss.getFileName())
												.toString());
								successACKStatus = Integer.parseInt(ss
										.getTotalRecord()) - returACKStatus;
							} else {
								returACKStatus = 0;
								successACKStatus = 0;
							}
							ss.setSuccessACKStatus(Integer
									.toString(successACKStatus));
							ss.setReturACKStatus(Integer
									.toString(returACKStatus));

							// Add searchByList to new newSPANSummaries list
							newSPANSummaries.add(ss);

							grandTotalAmount = grandTotalAmount
									+ Integer.parseInt(ss.getTotalAmount());
							grandTotalRecord = grandTotalRecord
									+ Integer.parseInt(ss.getTotalRecord());
						}
						// clear search by list array
						searchByList.clear();
					}
				}
				// close
				break;

			case "1": // open
				// loop
				for (SPANSummaries s : SPANSummaries) {
					// prevent null pointer
					if (request.getParams() == null)
						request.setParams("0");
					if (s.getTotalRecord() == null)
						s.setTotalRecord("0");

					if (Integer.parseInt(request.getParams()) <= Integer
							.parseInt(s.getTotalRecord())) {
						s.setSuccessACKStatus(Integer
								.toString(successACKStatus));
						s.setReturACKStatus(Integer.toString(returACKStatus));
						searchByList.add(s);

						for (SPANSummaries ssm : searchByList) {
							if ("5".equalsIgnoreCase(ssm.getProcStatus())) {
								/**
								 * querying (getReturDataDetails) with input
								 * [fileName] mapping the output to variable
								 * returACKStatus
								 */
								returACKStatus = Integer
										.parseInt(spanDataValidationDao
												.getReturDataDetails(
														ssm.getFileName())
												.toString());

								successACKStatus = Integer.parseInt(ssm
										.getTotalRecord()) - returACKStatus;
							} else {
								// do nothing
							}
							ssm.setSuccessACKStatus(Integer
									.toString(successACKStatus));
							ssm.setReturACKStatus(Integer
									.toString(returACKStatus));

							// Add searchByList to new newSPANSummaries list
							newSPANSummaries.add(ssm);

							grandTotalAmount = grandTotalAmount
									+ Integer.parseInt(ssm.getTotalAmount());
							grandTotalRecord = grandTotalRecord
									+ Integer.parseInt(ssm.getTotalRecord());
						}
						// clear search by list array
						searchByList.clear();

					} else if ("0".equalsIgnoreCase(request.getParams())) {
						if ("0".equalsIgnoreCase(s.getTotalRecord())) {
							s.setSuccessACKStatus("0");
							s.setReturACKStatus("0");
							searchByList.add(s);
						}

						for (SPANSummaries sbl : searchByList) {
							if ("5".equalsIgnoreCase(sbl.getProcStatus())) {
								/**
								 * querying (getReturDataDetails) with input
								 * [fileName] mapping the output to variable
								 * returACKStatus
								 */
								returACKStatus = Integer
										.parseInt(spanDataValidationDao
												.getReturDataDetails(
														sbl.getFileName())
												.toString());
								successACKStatus = Integer.parseInt(sbl
										.getTotalRecord()) - returACKStatus;
							} else {
								// do nothing
							}

							// Add searchByList to new newSPANSummaries list
							newSPANSummaries.add(sbl);

							grandTotalAmount = grandTotalAmount
									+ Integer.parseInt(sbl.getTotalAmount());
							grandTotalRecord = grandTotalRecord
									+ Integer.parseInt(sbl.getTotalRecord());
						}

						// clear search by list array
						searchByList.clear();
					} else {
						// do nothing
					}
				}
				// close
				break;

			case "2": // open
				// loop
				for (SPANSummaries s : SPANSummaries) {
					if ("0".equalsIgnoreCase(request.getParams())) {
						if ("0".equalsIgnoreCase(s.getRetryStatusL1())) {
							// Append Span Summaries to Search By List
							searchByList.add(s);
						}

						for (SPANSummaries ssm : searchByList) {
							if ("5".equalsIgnoreCase(ssm.getProcStatus())) {
								/**
								 * querying (getReturDataDetails) with input
								 * [fileName] mapping the output to variable
								 * returACKStatus
								 */
								returACKStatus = Integer
										.parseInt(spanDataValidationDao
												.getReturDataDetails(
														ssm.getFileName())
												.toString());

								// mapping substract total record and
								// returACKStatus and mapping to
								// successACKStatus
								successACKStatus = Integer.parseInt(ssm
										.getTotalRecord()) - returACKStatus;
							} else {
								// do nothing
							}

							// Add searchByList to new newSPANSummaries list
							newSPANSummaries.add(ssm);

							grandTotalAmount = grandTotalAmount
									+ Integer.parseInt(ssm.getTotalAmount());
							grandTotalRecord = grandTotalRecord
									+ Integer.parseInt(ssm.getTotalRecord());
						}

						// clear search by list array
						searchByList.clear();
					} else {
						if ("0".equalsIgnoreCase(s.getRetryStatusL1())) {
							// Append Span Summaries to Search By List
							searchByList.add(s);
						}

						for (SPANSummaries ssm : searchByList) {
							if ("5".equalsIgnoreCase(ssm.getProcStatus())) {
								/**
								 * querying (getReturDataDetails) with input
								 * [fileName] mapping the output to variable
								 * returACKStatus
								 */
								returACKStatus = Integer
										.parseInt(spanDataValidationDao
												.getReturDataDetails(
														ssm.getFileName())
												.toString());

								// mapping substract total record and
								// returACKStatus and mapping to
								// successACKStatus
								successACKStatus = Integer.parseInt(ssm
										.getTotalRecord()) - returACKStatus;
							} else {
								// do nothing
							}

							// Add searchByList to new newSPANSummaries list
							newSPANSummaries.add(ssm);

							grandTotalAmount = grandTotalAmount
									+ Integer.parseInt(ssm.getTotalAmount());
							grandTotalRecord = grandTotalRecord
									+ Integer.parseInt(ssm.getTotalRecord());
						}

						// clear search by list array
						searchByList.clear();
					}
				}
				// close
				break;

			case "3": // open
				// loop
				for (SPANSummaries s : SPANSummaries) {
					if ("0".equalsIgnoreCase(request.getParams())) {
						if ("0".equalsIgnoreCase(s.getReturStatus())) {
							searchByList.add(s);
						}
					} else {
						if (Integer.parseInt(request.getParams()) <= Integer
								.parseInt(s.getReturStatus())) {
							searchByList.add(s);
						}
					}

					for (SPANSummaries ssm : searchByList) {
						if ("5".equalsIgnoreCase(ssm.getProcStatus())) {
							/**
							 * querying (getReturDataDetails) with input
							 * [fileName] mapping the output to variable
							 * returACKStatus
							 */
							returACKStatus = Integer
									.parseInt(spanDataValidationDao
											.getReturDataDetails(
													ssm.getFileName())
											.toString());

							// mapping substract total record and returACKStatus
							// and mapping to successACKStatus
							successACKStatus = Integer.parseInt(ssm
									.getTotalRecord()) - returACKStatus;
						} else {
							// do nothing
						}

						// Add searchByList to new newSPANSummaries list
						newSPANSummaries.add(ssm);

						grandTotalAmount = grandTotalAmount
								+ Integer.parseInt(ssm.getTotalAmount());
						grandTotalRecord = grandTotalRecord
								+ Integer.parseInt(ssm.getTotalRecord());
					}

					// clear search by list array
					searchByList.clear();
				}
				// close
				break;

			case "4": // open
				// loop
				for (SPANSummaries s : SPANSummaries) {
					if ("0".equalsIgnoreCase(request.getParams())) {
						if ("0".equalsIgnoreCase(s.getSuccessStatusL1())) {
							searchByList.add(s);
						}
					} else {
						if (Integer.parseInt(request.getParams()) <= Integer
								.parseInt(s.getSuccessStatusL1())) {
							searchByList.add(s);
						}
					}

					for (SPANSummaries ssm : searchByList) {
						if ("5".equalsIgnoreCase(ssm.getProcStatus())) {
							/**
							 * querying (getReturDataDetails) with input
							 * [fileName] mapping the output to variable
							 * returACKStatus
							 */
							returACKStatus = Integer
									.parseInt(spanDataValidationDao
											.getReturDataDetails(
													ssm.getFileName())
											.toString());

							// mapping substract total record and returACKStatus
							// and mapping to successACKStatus
							successACKStatus = Integer.parseInt(ssm
									.getTotalRecord()) - returACKStatus;
						} else {
							// do nothing
						}

						// Add searchByList to new newSPANSummaries list
						newSPANSummaries.add(ssm);

						grandTotalAmount = grandTotalAmount
								+ Integer.parseInt(ssm.getTotalAmount());
						grandTotalRecord = grandTotalRecord
								+ Integer.parseInt(ssm.getTotalRecord());
					}

					// clear search by list array
					searchByList.clear();
				}
				// close
				break;

			default:

				break;
			}

		} 
		else {
			/**
			 * querying (selectFNDataValidation) with input [fileName] mapping
			 * the output to variable PROC_STATUS, TRX_TYPE
			 */
			SelectFNDataValidationOutput sfnOutput = spanDataValidationDao
					.selectFNDataValidation(request.getFilename());
			String PROC_STATUS = sfnOutput.getProcStatus();
			String TRX_TYPE = sfnOutput.getTrxType();

			
			// branching
			if (PROC_STATUS.equalsIgnoreCase("4") || PROC_STATUS.equalsIgnoreCase("5")) {
				String additionalCondition = null;
				String additionalCondtitionSC = null;
				String retryCondition = null;
				// switch click on
				switch (request.getClickOn()) {
				case "0":
					additionalCondition = "";
					additionalCondtitionSC = "";
					break;
				case "1": // open
					additionalCondition = "A.ERRORCODE<>'0' and A.ERRORCODE not "
							+ "in (select response_code from span_host_response_code where rc_id='SPAN02') "
							+ "and a.batchid=b.batchid and a.response_flag in ('1', '3') and";
					additionalCondtitionSC = "A.ERRORCODE<>'00' and A.ERRORCODE not in"
							+ " (select response_code from span_host_response_code where rc_id='SPAN02') "
							+ "and a.scflaging IN ('0','1') and ";
					retryCondition = "and ((retur_status>0 and retry_status<1) or sum_status_rty='2')";
					break; // close
				case "2": // open
					additionalCondition = "a.errorcode<>'0' and a.errorcode in (select response_code from "
							+ "span_host_response_code where rc_id='SPAN02') and (a.RESPONSE_FLAG = '1' OR"
							+ " a.RESPONSE_FLAG = '3') and";
					additionalCondtitionSC = "a.errorcode<>'00' and a.errorcode in (select response_code from"
							+ " span_host_response_code where rc_id='SPAN02') and a.scflaging = '1' and";
					break; // close
				case "3": // open
					additionalCondition = "(a.ERRORCODE='0' or a.ERRORCODE='77') and a.RESERVE2 IS NULL and";
					additionalCondtitionSC = "(a.ERRORCODE='00' or a.errorcode='0' a.errorcode='77') and";
					break; // close
				case "4": // open
					if (PROC_STATUS.equalsIgnoreCase("5")) {
						additionalCondition = "a.RESPONSE_FLAG = '2' and a.ERRORCODE in (select response_code from"
								+ " span_host_response_code where rc_id='SPAN02') and";
						additionalCondtitionSC = "a.RESPONSE_FLAG = '1' and a.ERRORCODE in (select response_code"
								+ " from span_host_response_code where rc_id='SPAN02') and";
					} else {
						// do nothing
					}
					break; // close
				case "5": // open
					if (PROC_STATUS.equalsIgnoreCase("5")) {
						additionalCondition = "(a.ERRORCODE='0' or a.ERRORCODE='77') and a.response_flag IN ('1','3') and";
						additionalCondtitionSC = "(a.ERRORCODE='00' or a.ERRORCODE='77') and a.SCFLAGING IN ('0','1') and";
					} else {
						// do nothing
					}
					break; // close
				case "6": // open
					if (PROC_STATUS.equalsIgnoreCase("5")) {
						additionalCondition = "a.RESPONSE_FLAG = '1' and a.errorcode<>'0' and a.errorcode "
								+ "not in (select response_code from span_host_response_code where "
								+ "rc_id='SPAN02') and";
					} else {
						additionalCondition = "a.RESPONSE_FLAG = '1' and a.errorcode<>'0' and a.errorcode "
								+ "not in (select response_code from span_host_response_code where "
								+ "rc_id='SPAN02') and";
					}
					break; // close
				case "7": // open
					if (PROC_STATUS.equalsIgnoreCase("5")) {
						additionalCondition = "(a.ERRORCODE='0' or a.ERRORCODE='77') and";
					} else {
						additionalCondition = "(a.ERRORCODE='0' or a.ERRORCODE='77') and";
					}
					break; // close
				case "8": // open
					// do nothing
					break; // close
				case "9": // open
					additionalCondition = "Hold";
					break; // close
				default: // open
					additionalCondition = "";
					break; // close
				}

				// switch on additionalContition
				if (additionalCondition == null){
					// null do nothing
				}
				else {
					// switch on TRX_TYPE
					switch (TRX_TYPE) {
					case "0":
						/**
						 * querying (getODBCTotalProcess) with input [fileName,
						 * legStatus] mapping the output to variable total
						 * process
						 */
						int totalProcess = getODBCTotalProcess(
								spanDataValidationDao, request.getFilename(),
								legStatus);

						// String totalProcess = null;
						// mapping
						String ODBCTotalProcess = String.valueOf(totalProcess);
						String processCondition = "";
						// switch clickOn
						switch (request.getClickOn()) {
						/**
						 * case 0 adalah tambahan sendiri ketika development SPAN BJB,
						 * existing SPAN Mandiri (IS) tidak ada case 0
						 */
						case "0":
							processCondition = "";
							break;
							
						case "1":
							processCondition = "AND SUM_STATUS_RTY='2'";
							break;

						case "2":
							processCondition = "AND SUM_STATUS_RTU='2' and a.RESPONSE_FLAG = '1'";
							break;

						case "4":
							processCondition = "";
							break;

						case "9":
							ODBCTotalProcess = "2";
							break;

						default:
							processCondition = "and total_process='"+totalProcess+"'";
							break;
						}

						// branching
						// ODBCTotalProcess = "";
						if (Integer.parseInt(ODBCTotalProcess) > 1) {
							String returStatus;
							// switch on clickOn
							switch (request.getClickOn()) {
							case "0": // open
								additionalCondition = "a.response_flag IN ('1','3') and";
								/**
								 * querying (getRetryOrReturStatus) with input
								 * [fileName, ODBCTotalProcess] mapping the
								 * output to variable detailsOutput
								 */

								detailsOutput = getSPANSummaryDetails(
										request.getFilename(),
										additionalCondition, legStatus,
										retryCondition, processCondition,
										spanDataValidationDao);
								break; // close
							case "1": // open
								/**
								 * querying (getRetryOrReturStatus) with
								 * input [fileName, ODBCTotalProcess] mapping
								 * the output to variable RETUR_STATUS,
								 * RETRY_STATUS
								 */

								statusDetailOutputs = getRetryOrReturStatus(
										request.getFilename(), totalProcess,
										spanDataValidationDao);
								String RETRY_STATUS = statusDetailOutputs
										.get(0).getRETRYSTATS();
								// mapping
								String retryStatus = RETRY_STATUS;
								// branching
								if (Integer.parseInt(retryStatus) > 0) {
									/**
									 * querying (getSPANSummariesDetails) with
									 * input [fileName, legStatus,
									 * retryCondition, additionalCondition,
									 * processCondition] mapping the output to
									 * variable detailsOutput
									 */
									detailsOutput = getSPANSummaryDetails(
											request.getFilename(),
											additionalCondition, legStatus,
											retryCondition, processCondition,
											spanDataValidationDao);
									// result
									// mapping checkdata
									String ODBCRetryFound = detailsOutput
											.size() + "";

									// branching
									if (ODBCRetryFound.equalsIgnoreCase("0")) {
										/**
										 * querying (getSPANSummariesDetails)
										 * with input [fileName, legStatus,
										 * retryCondition, processCondition]
										 * mapping the output to variable
										 * detailsOutput
										 */
										detailsOutput = getSPANSummaryDetails(
												request.getFilename(),
												additionalCondition, legStatus,
												retryCondition, "",
												spanDataValidationDao);
									} else {
										// do nothing
									}
								} else {
									// do nothing
								}
								break; // close
							case "2": // open
								/**
								 * querying (getRetryOrReturStatus) with
								 * input [fileName, ODBCTotalProcess] mapping
								 * the output to variable detailsOutput
								 */
								List<StatusDetailOutput> statusDetailOutputs = getRetryOrReturStatus(
										request.getFilename(), totalProcess,
										spanDataValidationDao);
								// mapping
								String RETUR_STATUS = statusDetailOutputs
										.get(0).getRETURSTATS();
								returStatus = RETUR_STATUS;
								// branching
								if (Integer.parseInt(returStatus) > 0) {
									/**
									 * querying (getSPANSummariesDetails) with
									 * input [fileName, legStatus,
									 * retryCondition, additionalCondition,
									 * processCondition] mapping the output to
									 * variable detailsOutput
									 */
									detailsOutput = getSPANSummaryDetails(
											request.getFilename(),
											additionalCondition, legStatus,
											retryCondition, processCondition,
											spanDataValidationDao);
									// mapping checkdata
									String ODBCReturFound = detailsOutput
											.size() + "";
									// branching
									if (ODBCReturFound.equalsIgnoreCase("0")) {
										/**
										 * querying (getSPANSummariesDetails)
										 * with input [fileName, legStatus,
										 * retryCondition, processCondition]
										 * mapping the output to variable
										 * detailsOutput
										 */
										detailsOutput = getSPANSummaryDetails(
												request.getFilename(),
												additionalCondition, legStatus,
												retryCondition, "",
												spanDataValidationDao);
									} else {
										// do nothing
									}
								} else {
									// do nothing
								}
								break; // close
							case "3": // open
								/**
								 * querying (getRetryOrReturStatus) with input
								 * [fileName, ODBCTotalProcess] mapping the
								 * output to variable detailsOutput
								 */
								detailsOutput = getSPANSummaryDetails(
										request.getFilename(),
										additionalCondition, legStatus,
										retryCondition, "",
										spanDataValidationDao);
								break; // close
							case "4": // open
								/**
								 * querying (getRetryOrReturStatus) with input
								 * [fileName, ODBCTotalProcess] mapping the
								 * output to variable detailsOutput
								 */
								detailsOutput = getSPANSummaryDetails(
										request.getFilename(),
										additionalCondition, legStatus,
										retryCondition, "",
										spanDataValidationDao);
								break; // close
							case "5": // open
								/**
								 * querying (getRetryOrReturStatus) with input
								 * [fileName, ODBCTotalProcess] mapping the
								 * output to variable detailsOutput
								 */
								detailsOutput = getSPANSummaryDetails(
										request.getFilename(),
										additionalCondition, legStatus,
										retryCondition, "",
										spanDataValidationDao);
								break; // close
							case "6": // open
								detailsOutput = getSPANSummaryDetails(
										request.getFilename(),
										additionalCondition, legStatus,
										retryCondition, processCondition,
										spanDataValidationDao);
								break; // close
							case "7": // open
								detailsOutput = getSPANSummaryDetails(
										request.getFilename(),
										additionalCondition, legStatus,
										retryCondition, processCondition,
										spanDataValidationDao);
								break; // close
							case "9": // open
								String message_id = request.getFilename()
										.replaceAll("jar", "xml");
								/**
								 * querying (getHoldRecord) with input
								 * [message_id] mapping the output to variable
								 * detailsOutput
								 */
								detailsOutput = getHoldRecord(message_id,
										spanDataValidationDao); 
								break; // close
							}
						} 
						else {
							/**
							 * querying (getSPANSummariesDetails) with
							 * input [fileName, legStatus, additionalCondition,
							 * processCondition] mapping the output to variable
							 * detailsOutput
							 */
							detailsOutput = getSPANSummaryDetails(
									request.getFilename(),
									additionalCondition, legStatus,
									retryCondition, processCondition,
									spanDataValidationDao); //nah ini aneh,, additionalCondition itu gak ada
							break; 
						}

						break;

					default:
						/**
						 * querying (getLegStatus) with input [fileName]
						 * mapping the output to variable LEGSTATUS
						 */
						String LEGSTATUS = getLegStatus(request.getFilename(), spanDataValidationDao);
						// switch on LEGSTATUS
						switch (LEGSTATUS) {
						default:
							if (request.getClickOn().equalsIgnoreCase("9")) {
								// mapping replace string --> message_id
								String message_id = request.getFilename()
										.replaceAll("jar", "xml");
								/**
								 * querying (getLegStatus) with input
								 * [message_id] mapping the output to
								 * detailsOutput
								 */
								detailsOutput = getHoldRecord(message_id,
										spanDataValidationDao);
							} else {
								/**
								 * querying (getLegStatus) with input
								 * [message_id] mapping the output to
								 * detailsOutput
								 */
								detailsOutput = getSPANSummaryDetails(
										request.getFilename(),
										additionalCondition, legStatus,
										retryCondition, null,
										spanDataValidationDao);
							}
							break;
						}
						break;
					}

					// getReturCodeList
					String returErrorCodes = getReturCodeList(spanDataValidationDao);

					// toGetDetailsOutput
					ToGetDetailsOutputResponse toGetDetailsOutput = toGetDetailsOutput(
							detailsOutput, returErrorCodes);

					// switch on clickOn
					switch (request.getClickOn()) {
					case "0":
						detailsOutputList = detailsOutput;
						totalAmountTemp = toGetDetailsOutput
								.getTotalAmountSuccess()
								+ toGetDetailsOutput.getTotalAmountRetur()
								+ toGetDetailsOutput.getTotalAmountRetry();
						break;

					case "1":
						totalAmountTemp = toGetDetailsOutput
								.getTotalAmountRetry() + "";
						detailsOutputList = toGetDetailsOutput
								.getRetryOutputList();
						break;

					case "2":
						if (!PROC_STATUS.equalsIgnoreCase("5")) {
							detailsOutputList = toGetDetailsOutput
									.getReturOutputList();
						} else {
							// do nothing
						}
						break;

					case "3":
						if (!PROC_STATUS.equalsIgnoreCase("5")) {
							totalAmountTemp = toGetDetailsOutput
									.getTotalAmountSuccess();
							detailsOutputList = toGetDetailsOutput
									.getSuccessOutputList();
						} else {
							// do nothing
						}
						break;

					case "4":
						totalAmountTemp = toGetDetailsOutput
								.getTotalAmountRetur();
						detailsOutputList = toGetDetailsOutput
								.getReturOutputList();
						break;

					case "5":
						totalAmountTemp = toGetDetailsOutput
								.getTotalAmountSuccess();
						detailsOutputList = toGetDetailsOutput
								.getSuccessOutputList();
						break;

					case "6":
						totalAmountTemp = toGetDetailsOutput
								.getTotalAmountRetry();
						detailsOutputList = toGetDetailsOutput
								.getRetryOutputList();
						break;

					case "7":
						totalAmountTemp = toGetDetailsOutput
								.getTotalAmountSuccess();
						detailsOutputList = toGetDetailsOutput
								.getSuccessOutputList();
						break;

					case "9":
						detailsOutputList = detailsOutput;
						break;

					default:
						break;
					}
				}
			} 
			else if (PROC_STATUS.equalsIgnoreCase("1") || PROC_STATUS.equalsIgnoreCase("2") || PROC_STATUS.equalsIgnoreCase("3")) {
				// branching clickon <> 1 && clickon <> 2 && clickon <> 3 &&
				// clickon <> 4 && clickon <> 5
				if (!request.getClickOn().equals("1")
						&& !request.getClickOn().equals("2")
						&& !request.getClickOn().equals("3")
						&& !request.getClickOn().equals("4")
						&& !request.getClickOn().equals("5")) {
					/**
					 * querying (SelectSPANDataValidation) with input
					 * [filename], mapping the output to variable XML_FILE_NAME
					 */
					String xmlFileName = selectSPANDataValidation(request.getFilename(), spanDataValidationDao).get(0).getXmlFileName();

					/**
					 * parsing XML_FILE_NAME to document
					 * mapping the output to SPANDocument. SPANDocument contains
					 * [DataArea, ApplicationArea]
					 */
					
					SP2DDocument document = new SP2DDocument();
					document = (SP2DDocument) PubUtil.xmlValuesToDocument(SP2DDocument.class, xmlFileName);
					
					String listDataArea = "";
					try {
						// mapping size of list from parsed document
						listDataArea = document.getDataArea().size() + "";
					} catch (Exception e) {
						// set size of list = 0
						listDataArea = "";
					}

					if (Integer.parseInt(listDataArea) > 0) {
						// translate java service
						detailsOutputList = mappingDetailsOutputList(document);
						// mapping totalAmountTemp
						totalAmountTemp = document.getFooter()
								.getTotalAmount();
					} else {
						// mapping append document
						detailOutputList = new DetailsOutputList();
						detailOutputList.setErrorCode("");
						detailOutputList.setErrorMessage("");
						detailOutputList.setCreditAcctNo(document
								.getDataArea().get(0).getBeneficiaryAccount());
						detailOutputList.setCreditAcctName(document
								.getDataArea().get(0).getBeneficiaryName());
						detailOutputList.setBenefBankName(document
								.getDataArea().get(0).getBeneficiaryBank());
						detailOutputList.setDebitAccount(document
								.getDataArea().get(0)
								.getAgentBankAccountNumber());
						detailOutputList
								.setDebitAccountType(document.getDataArea()
										.get(0).getAgentBankAccountName());
						detailOutputList.setDocDate(document.getDataArea()
								.get(0).getDocumentDate());
						detailOutputList.setDebitAmount(document
								.getDataArea().get(0).getAmount());
						detailOutputList.setSpanType(document
								.getApplicationArea()
								.getApplicationAreaMessageTypeIndicator());
						// append
						detailsOutput.add(detailOutputList);

						totalAmountTemp = document.getDataArea().get(0)
								.getAmount();

						detailsOutputList = detailsOutput;
					}
				}
			}
		}

		// mapping total amount
		String TotalAmount = totalAmountTemp;

		// switch on detailsOutputList
		if (detailsOutputList == null) {
			detailOutputList = new DetailsOutputList();
			detailOutputList.setErrorCode("");
			detailOutputList.setErrorMessage("");
			detailOutputList.setCreditAcctNo("");
			detailOutputList.setCreditAcctName("");
			detailOutputList.setBenefBankName("");
			detailOutputList.setDebitAccount("");
			detailOutputList.setDebitAccountType("");
			detailOutputList.setDocDate("");
			detailOutputList.setDebitAmount("0");
			detailOutputList.setSpanType("");

			response.setTotalAmount("0");
			response.setDetailsOutputList(detailsOutputList); 
			response.setSearchByList(searchByList);
		}else{
			response.setDetailsOutputList(detailsOutputList);
			response.setTotalAmount(TotalAmount);
			response.setSearchByList(searchByList);
		}
		return response;
	}

	private static List<SPANSummaries> selectSPANDataValidation(String filename, SPANDataValidationDao spanDataValidationDao) {
		
		List<SPANSummaries> spanSummaries = new ArrayList<>();
		spanSummaries = spanDataValidationDao.selectSPANDataValidation(filename);
		return spanSummaries;
	}

	private static String getLegStatus(String filename, SPANDataValidationDao spanDataValidationDao) {
		
		String result = "";
		result = spanDataValidationDao.getLegStatus(filename);
		return null;
	}

	private static List<DetailsOutputList> getHoldRecord(String messageId, SPANDataValidationDao spanDataValidationDao) {
		
		List<DetailsOutputList> result = spanDataValidationDao
				.getHoldRecord(messageId);

		return result;
	}

	private static List<StatusDetailOutput> getRetryOrReturStatus(
			String filename, int totalProcess,
			SPANDataValidationDao spanDataValidationDao) {
		List<StatusDetailOutput> result = spanDataValidationDao
				.getRetryOrReturStatus(filename, totalProcess);
		return result;
	}

	private static List<DetailsOutputList> getSPANSummaryDetails(
			String filename, String additionalCondition, String legStatus,
			String retryCondition, String processCondition,
			SPANDataValidationDao spanDataValidationDao) {
		
		List<DetailsOutputList> result = spanDataValidationDao
				.getSPANSummaryDetails(filename, additionalCondition,
						legStatus, retryCondition, processCondition);

		return result;
	}

	private static int getODBCTotalProcess(
			SPANDataValidationDao spanDataValidationDao, String fileName,
			String legStatus) {
		int result = 0;
		result = spanDataValidationDao.getODBCTotalProcess(fileName, legStatus);

		return result;
	}

	public static List<DetailsOutputList> mappingDetailsOutputList(
			SP2DDocument SPANDocument) {

		List<DetailsOutputList> detailsOutputList = new ArrayList<>();

		for (DataArea da : SPANDocument.getDataArea()) {
			String DocumentDate = da.getDocumentDate();
			String BeneficiaryName = da.getBeneficiaryName();
			String BeneficiaryBank = da.getBeneficiaryBank();
			String BeneficiaryAccount = da.getBeneficiaryAccount();
			String Amount = da.getAmount();
			String AgentBankAccountNumber = da.getAgentBankAccountNumber();
			String AgentBankAccountName = da.getAgentBankAccountName();

			DetailsOutputList detailOutput = new DetailsOutputList();
			detailOutput.setErrorCode("");
			detailOutput.setErrorMessage("");
			detailOutput.setCreditAcctNo(BeneficiaryAccount);
			detailOutput.setCreditAcctName(BeneficiaryName);
			detailOutput.setBenefBankName(BeneficiaryBank);
			detailOutput.setDebitAccount(AgentBankAccountNumber);
			detailOutput.setDebitAccountType(AgentBankAccountName);
			detailOutput.setDocDate(DocumentDate);
			detailOutput.setDebitAmount(Amount);
			detailOutput.setSpanType(SPANDocument.getApplicationArea()
					.getApplicationAreaMessageTypeIndicator());
			detailOutput.setDocNumber(da.getDocumentNumber());

			detailsOutputList.add(detailOutput);
		}

		return detailsOutputList;
	}

	public static String getReturCodeList(SPANDataValidationDao spanDataValidationDao) {
		/**
		 * querying (getReturCode) with input RC_ID = "SPAN02", mapping the
		 * output to variable list[returCode, errorMessage]
		 */
		List<ReturCodes> returCodes = new ArrayList<>();
		String rcId = "SPAN02";
		returCodes = getReturCode(rcId, spanDataValidationDao);
		
		int i = 1;
		String returCodeList = "";
		for (ReturCodes r : returCodes) {
			if (i == 1) {
				returCodeList = r.getReturCode();
			}else{
				returCodeList = returCodeList.concat("|").concat(r.getReturCode());
			}
			i++;
		}

		return returCodeList;
	}

	private static List<ReturCodes> getReturCode(String rcId, SPANDataValidationDao spanDataValidationDao) {
		List<ReturCodes> result = spanDataValidationDao
				.getReturCode(rcId);

		return result;
	}

	public static ToGetDetailsOutputResponse toGetDetailsOutput(
			List<DetailsOutputList> detailsOutput, String returErrorCodes) {

		ToGetDetailsOutputResponse response = new ToGetDetailsOutputResponse();
		List<DetailsOutputList> successOutputList = new ArrayList<DetailsOutputList>();
		List<DetailsOutputList> returOutputList = new ArrayList<DetailsOutputList>();
		List<DetailsOutputList> retryOutputList = new ArrayList<DetailsOutputList>();
		response.setSuccessOutputList(successOutputList);
		response.setReturOutputList(returOutputList);
		response.setRetryOutputList(retryOutputList);
		returErrorCodes = returErrorCodes.replace("|", ":");
		String[] returErrorCodeList = null;
		String errorForRetry = "";
		String status = "SUCCESS";
		String errorMessage = "";
		BigDecimal totalAmountSuccess = new BigDecimal(0);
		BigDecimal totalAmountRetur = new BigDecimal(0);
		BigDecimal totalAmountRetry = new BigDecimal(0);

		try {
			for (DetailsOutputList dol : detailsOutput) {
				errorForRetry = "0";
				returErrorCodeList = returErrorCodes.split(":");
				String errorCode = dol.getErrorCode();
				String debitAmount = dol.getDebitAmount();
				for (int x = 0; x < returErrorCodeList.length; x++) {
					if ("0".equalsIgnoreCase(errorCode)
							|| "00".equalsIgnoreCase(errorCode)
							|| "77".equalsIgnoreCase(errorCode)) {
						// successOutputList.add(dol);
						response.getSuccessOutputList().add(dol);
						errorForRetry = "1";
						totalAmountSuccess = totalAmountSuccess
								.add(new BigDecimal(debitAmount.replace(".00",
										"")));
						break;
					} else if (returErrorCodeList[x]
							.equalsIgnoreCase(errorCode)) {
						// returOutputList.add(dol);
						response.getReturOutputList().add(dol);
						errorForRetry = "1";
						totalAmountRetur = totalAmountRetur.add(new BigDecimal(
								debitAmount.replace(".00", "")));
						break;
					}
				}
				if ("0".equalsIgnoreCase(errorForRetry)) {
					// retryOutputList.add(dol);
					response.getRetryOutputList().add(dol);
					totalAmountRetry = totalAmountRetry.add(new BigDecimal(
							debitAmount.replace(".00", "")));
				}
			}
			// set amount
			response.setTotalAmountSuccess(totalAmountSuccess.toString());
			response.setTotalAmountRetur(totalAmountRetur.toString());
			response.setTotalAmountRetry(totalAmountRetry.toString());

		} catch (Exception e) {
			status = "ERROR";
			errorMessage = e.getMessage();
			e.printStackTrace();
		}

		response.setStatus(status);
		response.setErrorMessage(errorMessage);
		return response;
	}
}
