package com.mii.is.io;

import java.util.List;

import com.mii.is.io.helper.DetailsOutputList;

public class ToGetDetailsOutputResponse {
	private List<DetailsOutputList> successOutputList;
	private List<DetailsOutputList> returOutputList;
	private List<DetailsOutputList> retryOutputList;
	private String status;
	private String errorMessage;
	private String totalAmountSuccess;
	private String totalAmountRetur;
	private String totalAmountRetry;
	
	
	public List<DetailsOutputList> getSuccessOutputList() {
		return successOutputList;
	}
	public List<DetailsOutputList> getReturOutputList() {
		return returOutputList;
	}
	public List<DetailsOutputList> getRetryOutputList() {
		return retryOutputList;
	}
	public void setRetryOutputList(List<DetailsOutputList> retryOutputList) {
		this.retryOutputList = retryOutputList;
	}
	public void setReturOutputList(List<DetailsOutputList> returOutputList) {
		this.returOutputList = returOutputList;
	}
	public void setSuccessOutputList(List<DetailsOutputList> successOutputList) {
		this.successOutputList = successOutputList;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public String getTotalAmountSuccess() {
		return totalAmountSuccess;
	}
	public void setTotalAmountSuccess(String totalAmountSuccess) {
		this.totalAmountSuccess = totalAmountSuccess;
	}
	public String getTotalAmountRetur() {
		return totalAmountRetur;
	}
	public void setTotalAmountRetur(String totalAmountRetur) {
		this.totalAmountRetur = totalAmountRetur;
	}
	public String getTotalAmountRetry() {
		return totalAmountRetry;
	}
	public void setTotalAmountRetry(String totalAmountRetry) {
		this.totalAmountRetry = totalAmountRetry;
	}
}
