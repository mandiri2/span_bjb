package com.mii.is.io.helper;

import java.util.List;

public class Files {
	private String batchId;
	private String fileName;
	private List<String> documentNumbers;
	private String procStatus;
	private String glAccount;
	private String asynchronous;
	private String payRollType;
	private String retryNumber;
	private String settlement;
	private String scdate;
	private String trxType;
	private String docdate;
	private String execFileFlag;

	public String getBatchId() {
		return batchId;
	}

	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public List<String> getDocumentNumbers() {
		return documentNumbers;
	}

	public void setDocumentNumbers(List<String> documentNumbers) {
		this.documentNumbers = documentNumbers;
	}

	public String getProcStatus() {
		return procStatus;
	}

	public void setProcStatus(String procStatus) {
		this.procStatus = procStatus;
	}

	public String getGlAccount() {
		return glAccount;
	}

	public void setGlAccount(String glAccount) {
		this.glAccount = glAccount;
	}

	public String getAsynchronous() {
		return asynchronous;
	}

	public void setAsynchronous(String asynchronous) {
		this.asynchronous = asynchronous;
	}

	public String getPayRollType() {
		return payRollType;
	}

	public void setPayRollType(String payRollType) {
		this.payRollType = payRollType;
	}

	public String getRetryNumber() {
		return retryNumber;
	}

	public void setRetryNumber(String retryNumber) {
		this.retryNumber = retryNumber;
	}

	public String getSettlement() {
		return settlement;
	}

	public void setSettlement(String settlement) {
		this.settlement = settlement;
	}

	public String getScdate() {
		return scdate;
	}

	public void setScdate(String scdate) {
		this.scdate = scdate;
	}

	public String getTrxType() {
		return trxType;
	}

	public void setTrxType(String trxType) {
		this.trxType = trxType;
	}

	public String getDocdate() {
		return docdate;
	}

	public void setDocdate(String docdate) {
		this.docdate = docdate;
	}

	public String getExecFileFlag() {
		return execFileFlag;
	}

	public void setExecFileFlag(String execFileFlag) {
		this.execFileFlag = execFileFlag;
	}

}
