package com.mii.is.io.helper;

public class OutputDetails {
	private String fileName;
	private String totalAmount;
	private String totalRecord;
	private String remarkPM3;
	
	public OutputDetails() {
	}
	
	public OutputDetails(String totalAmount, String totalRecord) {
		this.totalAmount = totalAmount;
		this.totalRecord = totalRecord;
	}
	
	public OutputDetails(String fileName, String totalAmount, String totalRecord) {
		this.fileName = fileName;
		this.totalAmount = totalAmount;
		this.totalRecord = totalRecord;
	}
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}
	public String getTotalRecord() {
		return totalRecord;
	}
	public void setTotalRecord(String totalRecord) {
		this.totalRecord = totalRecord;
	}
	public String getRemarkPM3() {
		return remarkPM3;
	}
	public void setRemarkPM3(String remarkPM3) {
		this.remarkPM3 = remarkPM3;
	}
}
