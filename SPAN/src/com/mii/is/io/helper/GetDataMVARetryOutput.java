package com.mii.is.io.helper;

public class GetDataMVARetryOutput {
	private String MVAAccount;
	private String Amount;
	private String Batchid;
	private String Trxdetailid;

	public String getMVAAccount() {
		return MVAAccount;
	}

	public void setMVAAccount(String mVAAccount) {
		MVAAccount = mVAAccount;
	}

	public String getAmount() {
		return Amount;
	}

	public void setAmount(String amount) {
		Amount = amount;
	}

	public String getBatchid() {
		return Batchid;
	}

	public void setBatchid(String batchid) {
		Batchid = batchid;
	}

	public String getTrxdetailid() {
		return Trxdetailid;
	}

	public void setTrxdetailid(String trxdetailid) {
		Trxdetailid = trxdetailid;
	}

}
