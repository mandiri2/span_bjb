package com.mii.is.io.helper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.mii.is.util.PubUtil;

public class MappingErrorData_SVC {
	
	public static final MappingErrorDataOutput mappingErrorData(String legStatus, String dirPath, String fileType, List<DownloadErrorResponse> downloadErrorResponses){
	
		StringBuffer overbookingSB = new StringBuffer();
		StringBuffer sknSB = new StringBuffer();
		StringBuffer rtgsSB = new StringBuffer();
		StringBuffer sb = new StringBuffer();
		
		String overbookingStr = "";
		String sknStr = "";
		String rtgsStr = "";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		
		String Status = "SUCCESS";
		String ErrorMessage = "";
		
		String filename = "";
		try{
			//System.out.println("AAAA");
		
		// pipeline
			// odbcDataError
			if(PubUtil.isListNotEmpty(downloadErrorResponses)){
				for(DownloadErrorResponse downloadErrorResponse : downloadErrorResponses){
					String	Amount = downloadErrorResponse.getAmount();
					String	CreditAccount = downloadErrorResponse.getCreditAccNo();
					String	CreditName = downloadErrorResponse.getCreditAccName();
					String	DebitAccount = downloadErrorResponse.getDebitAccount();
					String	DebitAccountType = downloadErrorResponse.getDebitAccountType();
					String	PaymentMethods = downloadErrorResponse.getPaymentMethod();
					//System.out.println("PaymentMethods = "+PaymentMethods);
					String	BenefBankCode = downloadErrorResponse.getBenBankCode();
					String	DocNumber = downloadErrorResponse.getDocNumber();
					String	BenefBankName = downloadErrorResponse.getBenBank();
					String	Description = downloadErrorResponse.getDescription();
					
					DebitAccount = DebitAccount.length()>13 ? DebitAccount.substring(0,13) : DebitAccount;
					CreditAccount = CreditAccount.length()>13 ? CreditAccount.substring(0,13) : CreditAccount;
					CreditName = CreditName.length()>40 ? CreditName.substring(0,40) : CreditName;
					DocNumber = DocNumber.length()>20 ? DocNumber.substring(0,20) : CreditName;
					
					if(PaymentMethods.equals("0") && fileType.equals("OB")){//overbooking
						overbookingStr= "99099SPAN000000".concat(DebitAccount.concat(
								"000000".concat(CreditAccount.concat(
								padLeft(setDecimal(Amount),"0",17).concat(" ".concat(
								CreditName.concat(DocNumber)))))));
					
	//					overbookingSB.append(overbookingStr.concat("\n"));
						sb.append(overbookingStr.concat("\n"));
						filename = "OB".concat(sdf.format(new Date()).concat(".txt"));
	//					filename = "OB".concat(sdf.format(new Date()));
	
					}
					else if(PaymentMethods.equals("2") && fileType.equals("SKN")){ //SKN
						if(legStatus.equals("L1")){						
							sknStr= (CreditAccount+"|".concat(CreditName+"|".concat(
								padLeft(setDecimal(Amount),"0",17)+"|".concat(BenefBankCode+"|".concat(
								BenefBankName+"|".concat(Description+"|".concat(DocNumber+"|")))))));
	
						}else{
							sknStr= "99099SPAN000000"+"|".concat(DebitAccount+"|".concat(
								"000000"+"|".concat(CreditAccount+"|".concat(
								padLeft(setDecimal(Amount),"0",17)+"|".concat(" "+"|".concat(
								CreditName+"|".concat(DocNumber+"|")))))));
										
						}
	
	//					sknSB.append(sknStr.concat("\n"));
						sb.append(sknStr.concat("\n"));
	//					filename = "SKN".concat(sdf.format(new Date()).concat(".txt"));
						filename = "SKN".concat(sdf.format(new Date()));
	
					}
					else if(PaymentMethods.equals("1") && fileType.equals("RTGS")){ //RTGS
						if(legStatus.equals("L1")){
							rtgsStr=(CreditAccount+"|".concat(CreditName+"|".concat(
								padLeft(setDecimal(Amount),"0",17)+"|".concat(BenefBankCode+"|".concat(
								BenefBankName+"|".concat(Description+"|".concat(DocNumber+"|")))))));
						}else{
							rtgsStr="99099SPAN000000".concat(DebitAccount+"|".concat(
								"000000"+"|".concat(CreditAccount+"|".concat(
								padLeft(setDecimal(Amount),"0",17)+"|".concat(" "+"|".concat(
								CreditName+"|".concat(DocNumber+"|")))))));
						}
	
	//					rtgsSB.append(rtgsStr.concat("\n"));
						sb.append(rtgsStr.concat("\n"));
	//					filename = "RTGS".concat(sdf.format(new Date()).concat(".txt"));
						filename = "RTGS".concat(sdf.format(new Date()));
					}
					else if(PaymentMethods.equals("3") && fileType.equals("OB")){
						overbookingStr= "99099SPAN000000".concat(DebitAccount.concat(
								"000000".concat(CreditAccount.concat(
								padLeft(setDecimal(Amount),"0",17).concat(" ".concat(
								CreditName.concat(DocNumber)))))));			
						sb.append(overbookingStr.concat("\n"));
						filename = "OB".concat(sdf.format(new Date()).concat(".txt"));
					
					}
					else{
						filename = "ERROR".concat(sdf.format(new Date()));
						sb.append("No data file was created".concat("\n"));
					}
					
				}
			}else{
				filename = "ERROR".concat(sdf.format(new Date()));
				sb.append("No data file was created".concat("\n"));
			}
		
		
			if("".equals(filename)){
				filename = "ERROR".concat(sdf.format(new Date()));
				sb.append("No data file was created".concat("\n"));
				ErrorMessage = "No data file was created";
			}
			
			String createFileErr = write2File(dirPath,filename,sb.toString());
			if(createFileErr.length()>0) {
				Status="ERROR";
				ErrorMessage=createFileErr;
			}
		
		}catch(Exception ex){
			ex.printStackTrace();
			ErrorMessage=ex.getMessage();	
		}
		//System.out.println(ErrorMessage);
		MappingErrorDataOutput output = new MappingErrorDataOutput();
		if("SUCCESS".equalsIgnoreCase(Status)){
			output.setPathfile(dirPath.concat(filename));
			output.setFilename(filename);
			output.setByteFiles(sb.toString().getBytes());
		}
		
		return output;
		
	}
	
	public static String setDecimal(String inString){
	    String transferAmount = inString;
	    
	    int decimalMark  = transferAmount.indexOf(".");
	    int lengthTransferAmount = transferAmount.length();	
		int totDecDigit = 0;
		if(decimalMark>0) {totDecDigit = lengthTransferAmount - decimalMark;}
	    if(decimalMark>0){
	        if(totDecDigit==1){	
	            transferAmount = transferAmount.concat("00");
	            transferAmount = transferAmount.replace(".", "");                
	        }
	        else if(totDecDigit==2){
				transferAmount = transferAmount.concat("0");
	            transferAmount = transferAmount.replace(".", "");                
	        }
	        else if(totDecDigit==3){
	            transferAmount = transferAmount.replace(".", "");                
	        }
	    }else {
	        transferAmount = transferAmount.concat("00");            
	    }
	
	    return transferAmount;
	} 
	
	public static String padLeft(String inString, String padString, int length){
	    if(inString==null || inString.length()<1) {
			inString = "0";
		}
	    
	    while(true){           
	        if(inString.length()<length){                    
	            inString = padString.concat(inString);
	        } else break;
	    }
	    return inString;
	}
	
public static String write2File(String dirPath, String filename, String strToWrite) throws IOException{
		FileOutputStream fos =null;
		String ErrorMessage = "";
		try{
	
			dirPath = dirPath.replace('\\', '/');
			File checkDirPath = new File (dirPath);
	
			if (!checkDirPath.isDirectory()) 
				checkDirPath.mkdirs();
			
			String absFileName = dirPath + "/" + filename;
			fos = new FileOutputStream(absFileName);
	
			if (strToWrite != null)	
			{
				int len = strToWrite.length();
				for(int i = 0; i < len; i++) 
				{
	                byte b = (byte) strToWrite.charAt(i);
					fos.write(b);
				}
			} 
	
			fos.flush();
			fos.close();
			
		} 
		catch (Exception exc) {
			ErrorMessage = exc.toString();
		}finally{
			if(fos!=null)fos.close();
		}
	
	//	System.out.println("ErrorMessage = "+ErrorMessage);
	
		return ErrorMessage;
	}
}
