package com.mii.is.io.helper;

import java.io.Serializable;

public class SPANSummaries implements Serializable {
	private static final long serialVersionUID = 1L;
	private String fileName;
	private String retryStatusL1;
	private String retryStatusL2;
	private String returStatus;
	private String retryStatus;
	private String successStatus;
	private String totalAmount;
	private String totalRecord;
	private String totalProcess;
	private String successStatusL1;
	private String successStatusL2;
	private String procStatus;
	private String returACKStatus;
	private String successACKStatus;
	private String createDate;
	private String updateDate;
	private String totalWait;
	private String waitStatus;
	private String forcedACK;
	private String documentDate;
	private String batchID;
	private String legStatus;
	private String asynchronous;
	
	private String trxHeaderId;
	private String spanFnType;
	private String debitAccount;
	private String debitAccountType;
	private String xmlFileName;
	private String trxType;
	private String xmlVoidFileName;
	
	public SPANSummaries() {

	}

	public SPANSummaries(String filename, String total_record,
			String proc_status, String total_amount, String update_date,
			String create_date) {
		super();
		fileName = filename;
		totalRecord = total_record;
		procStatus = proc_status;
		totalAmount = total_amount;
		updateDate = update_date;
		createDate = create_date;
	}

	public SPANSummaries(String file_name, String batchid, String trxheaderid,
			String span_fn_type, String debit_account,
			String debit_account_type, String response_code,
			String proc_status, String xml_file_name, String trxtype,
			String total_amount, String total_record, String xml_void_file_name) {
		this.fileName = file_name;
		this.batchID = batchid;
		this.trxHeaderId = trxheaderid;
		this.spanFnType = span_fn_type;
		this.debitAccount = debit_account;
		this.debitAccountType = debit_account_type;
		this.procStatus = proc_status;
		this.xmlFileName = xml_file_name;
		this.trxType = trxtype;
		this.totalAmount = total_amount;
		this.totalRecord = total_record;
		this.xmlVoidFileName = xml_void_file_name;
	}

	public SPANSummaries(String filename, String retry_status,
			String retur_status, String wait_status, String success_status,
			String total_amount, String total_record, String total_process,
			String update_date, String create_date, String document_date,
			String proc_status, String batch_id, String forced_ack,
			String leg_status, String asynchronous, String total_wait) {
		super();
		fileName = filename;
		retryStatus = retry_status;
		returStatus = retur_status;
		waitStatus = wait_status;
		successStatus = success_status;
		totalAmount = total_amount;
		totalRecord = total_record;
		totalProcess = total_process;
		updateDate = update_date;
		createDate = create_date;
		documentDate = document_date;
		procStatus = proc_status;
		batchID = batch_id;
		forcedACK = forced_ack;
		setLegStatus(leg_status);
		this.setAsynchronous(asynchronous);
		totalWait = total_wait;
	}

	public String getReturACKStatus() {
		return returACKStatus;
	}

	public void setReturACKStatus(String returACKStatus) {
		this.returACKStatus = returACKStatus;
	}

	public String getWaitStatus() {
		return waitStatus;
	}

	public void setWaitStatus(String waitStatus) {
		this.waitStatus = waitStatus;
	}

	public String getForcedACK() {
		return forcedACK;
	}

	public void setForcedACK(String forcedACK) {
		this.forcedACK = forcedACK;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getRetryStatusL1() {
		return retryStatusL1;
	}

	public void setRetryStatusL1(String retryStatusL1) {
		this.retryStatusL1 = retryStatusL1;
	}

	public String getRetryStatusL2() {
		return retryStatusL2;
	}

	public void setRetryStatusL2(String retryStatusL2) {
		this.retryStatusL2 = retryStatusL2;
	}

	public String getReturStatus() {
		return returStatus;
	}

	public void setReturStatus(String returStatus) {
		this.returStatus = returStatus;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getTotalRecord() {
		return totalRecord;
	}

	public void setTotalRecord(String totalRecord) {
		this.totalRecord = totalRecord;
	}

	public String getSuccessStatusL1() {
		return successStatusL1;
	}

	public void setSuccessStatusL1(String successStatusL1) {
		this.successStatusL1 = successStatusL1;
	}

	public String getSuccessStatusL2() {
		return successStatusL2;
	}

	public void setSuccessStatusL2(String successStatusL2) {
		this.successStatusL2 = successStatusL2;
	}

	public String getProcStatus() {
		return procStatus;
	}

	public void setProcStatus(String procStatus) {
		this.procStatus = procStatus;
	}

	public String getSuccessACKStatus() {
		return successACKStatus;
	}

	public void setSuccessACKStatus(String successAckStatus) {
		this.successACKStatus = successAckStatus;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getTotalWait() {
		return totalWait;
	}

	public void setTotalWait(String totalWait) {
		this.totalWait = totalWait;
	}

	public String getRetryStatus() {
		return retryStatus;
	}

	public void setRetryStatus(String retryStatus) {
		this.retryStatus = retryStatus;
	}

	public String getSuccessStatus() {
		return successStatus;
	}

	public void setSuccessStatus(String successStatus) {
		this.successStatus = successStatus;
	}

	public String getTotalProcess() {
		return totalProcess;
	}

	public void setTotalProcess(String totalProcess) {
		this.totalProcess = totalProcess;
	}

	public String getDocumentDate() {
		return documentDate;
	}

	public void setDocumentDate(String documentDate) {
		this.documentDate = documentDate;
	}

	public String getBatchID() {
		return batchID;
	}

	public void setBatchID(String batchID) {
		this.batchID = batchID;
	}

	public String getLegStatus() {
		return legStatus;
	}

	public void setLegStatus(String legStatus) {
		this.legStatus = legStatus;
	}

	public String getAsynchronous() {
		return asynchronous;
	}

	public void setAsynchronous(String asynchronous) {
		this.asynchronous = asynchronous;
	}

	public String getTrxHeaderId() {
		return trxHeaderId;
	}

	public void setTrxHeaderId(String trxHeaderId) {
		this.trxHeaderId = trxHeaderId;
	}

	public String getSpanFnType() {
		return spanFnType;
	}

	public void setSpanFnType(String spanFnType) {
		this.spanFnType = spanFnType;
	}

	public String getDebitAccount() {
		return debitAccount;
	}

	public void setDebitAccount(String debitAccount) {
		this.debitAccount = debitAccount;
	}

	public String getXmlFileName() {
		return xmlFileName;
	}

	public void setXmlFileName(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public String getTrxType() {
		return trxType;
	}

	public void setTrxType(String trxType) {
		this.trxType = trxType;
	}
}
