package com.mii.is.io.helper;

public class MappingErrorDataOutput {
	private String status,errorMessage,filename,pathfile;
	private byte[] byteFiles;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getPathfile() {
		return pathfile;
	}
	public void setPathfile(String pathfile) {
		this.pathfile = pathfile;
	}
	public byte[] getByteFiles() {
		return byteFiles;
	}
	public void setByteFiles(byte[] byteFiles) {
		this.byteFiles = byteFiles;
	}
}
