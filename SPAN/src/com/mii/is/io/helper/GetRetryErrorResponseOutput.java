package com.mii.is.io.helper;

public class GetRetryErrorResponseOutput {
	private String fileName;
	private String batchID;
	private String trxDetailID;
	private String amount;
	private String creditAccNo;
	private String creditAccName;
	private String errorCode;
	private String errorMessage;
	private String paymentMethods;
	private String debitAccount;
	private String debitAccountType;
	private String BENBankCode;
	private String description;
	private String backend;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getBatchID() {
		return batchID;
	}

	public void setBatchID(String batchID) {
		this.batchID = batchID;
	}

	public String getTrxDetailID() {
		return trxDetailID;
	}

	public void setTrxDetailID(String trxDetailID) {
		this.trxDetailID = trxDetailID;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getCreditAccNo() {
		return creditAccNo;
	}

	public void setCreditAccNo(String creditAccNo) {
		this.creditAccNo = creditAccNo;
	}

	public String getCreditAccName() {
		return creditAccName;
	}

	public void setCreditAccName(String creditAccName) {
		this.creditAccName = creditAccName;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getPaymentMethods() {
		return paymentMethods;
	}

	public void setPaymentMethods(String paymentMethods) {
		this.paymentMethods = paymentMethods;
	}

	public String getDebitAccount() {
		return debitAccount;
	}

	public void setDebitAccount(String debitAccount) {
		this.debitAccount = debitAccount;
	}

	public String getDebitAccountType() {
		return debitAccountType;
	}

	public void setDebitAccountType(String debitAccountType) {
		this.debitAccountType = debitAccountType;
	}

	public String getBENBankCode() {
		return BENBankCode;
	}

	public void setBENBankCode(String bENBankCode) {
		BENBankCode = bENBankCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getBackend() {
		return backend;
	}

	public void setBackend(String backend) {
		this.backend = backend;
	}

}
