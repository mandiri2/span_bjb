package com.mii.is.io.helper;

public class StatusDetailOutput {
	private String RETRYSTATS;
	private String RETURSTATS;
	
	public StatusDetailOutput() {
		
	}
	
	public StatusDetailOutput(String RETRYSTATS, String RETURSTATS) {
		this.RETRYSTATS = RETRYSTATS;
		this.RETURSTATS = RETURSTATS;
	}

	public String getRETRYSTATS() {
		return RETRYSTATS;
	}

	public void setRETRYSTATS(String rETRYSTATS) {
		RETRYSTATS = rETRYSTATS;
	}

	public String getRETURSTATS() {
		return RETURSTATS;
	}

	public void setRETURSTATS(String rETURSTATS) {
		RETURSTATS = rETURSTATS;
	}

}
