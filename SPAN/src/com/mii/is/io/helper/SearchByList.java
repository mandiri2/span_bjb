package com.mii.is.io.helper;

public class SearchByList {
	private String fileName;
	private String retryStatusL1;
	private String retryStatusL2;
	private String returStatus;
	private String totalAmount;
	private String totalRecord;
	private String successStatusL1;
	private String successStatusL2;
	private String procStatus;
	private String returACKStatus;
	private String successACKStatus;
	private String createDate;
	private String updateDate;
	private String totalWait;
	private String waitStatus;
	private String forcedACK;
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getRetryStatusL1() {
		return retryStatusL1;
	}
	public void setRetryStatusL1(String retryStatusL1) {
		this.retryStatusL1 = retryStatusL1;
	}
	public String getRetryStatusL2() {
		return retryStatusL2;
	}
	public void setRetryStatusL2(String retryStatusL2) {
		this.retryStatusL2 = retryStatusL2;
	}
	public String getReturStatus() {
		return returStatus;
	}
	public void setReturStatus(String returStatus) {
		this.returStatus = returStatus;
	}
	public String getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}
	public String getTotalRecord() {
		return totalRecord;
	}
	public void setTotalRecord(String totalRecord) {
		this.totalRecord = totalRecord;
	}
	public String getSuccessStatusL1() {
		return successStatusL1;
	}
	public void setSuccessStatusL1(String successStatusL1) {
		this.successStatusL1 = successStatusL1;
	}
	public String getSuccessStatusL2() {
		return successStatusL2;
	}
	public void setSuccessStatusL2(String successStatusL2) {
		this.successStatusL2 = successStatusL2;
	}
	public String getProcStatus() {
		return procStatus;
	}
	public void setProcStatus(String procStatus) {
		this.procStatus = procStatus;
	}
	public String getReturACKStatus() {
		return returACKStatus;
	}
	public void setReturACKStatus(String returACKStatus) {
		this.returACKStatus = returACKStatus;
	}
	public String getSuccessACKStatus() {
		return successACKStatus;
	}
	public void setSuccessACKStatus(String successACKStatus) {
		this.successACKStatus = successACKStatus;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	public String getTotalWait() {
		return totalWait;
	}
	public void setTotalWait(String totalWait) {
		this.totalWait = totalWait;
	}
	public String getWaitStatus() {
		return waitStatus;
	}
	public void setWaitStatus(String waitStatus) {
		this.waitStatus = waitStatus;
	}
	public String getForcedACK() {
		return forcedACK;
	}
	public void setForcedACK(String forcedACK) {
		this.forcedACK = forcedACK;
	}
}
