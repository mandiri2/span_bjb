package com.mii.is.io.helper;

public class ResultList {
	private String providerName;
	private String providerAccount;
	private String providerCurrency;
	private String providerCode;

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	public String getProviderAccount() {
		return providerAccount;
	}

	public void setProviderAccount(String providerAccount) {
		this.providerAccount = providerAccount;
	}

	public String getProviderCurrency() {
		return providerCurrency;
	}

	public void setProviderCurrency(String providerCurrency) {
		this.providerCurrency = providerCurrency;
	}

	public String getProviderCode() {
		return providerCode;
	}

	public void setProviderCode(String providerCode) {
		this.providerCode = providerCode;
	}

}
