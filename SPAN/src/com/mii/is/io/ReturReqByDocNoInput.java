package com.mii.is.io;

import java.util.List;

import com.mii.is.io.helper.Files;

public class ReturReqByDocNoInput {
	private List<Files> files;
	private String accountRetur;
	private String legStatus;

	public List<Files> getFiles() {
		return files;
	}

	public void setFiles(List<Files> files) {
		this.files = files;
	}

	public String getAccountRetur() {
		return accountRetur;
	}

	public void setAccountRetur(String accountRetur) {
		this.accountRetur = accountRetur;
	}

	public String getLegStatus() {
		return legStatus;
	}

	public void setLegStatus(String legStatus) {
		this.legStatus = legStatus;
	}

}
