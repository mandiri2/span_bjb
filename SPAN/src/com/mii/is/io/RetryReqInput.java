package com.mii.is.io;

import java.util.List;

import com.mii.is.io.helper.Files;

public class RetryReqInput {

	private List<Files> files;
	private String legStatus;

	public List<Files> getFiles() {
		return files;
	}

	public void setFiles(List<Files> files) {
		this.files = files;
	}

	public String getLegStatus() {
		return legStatus;
	}

	public void setLegStatus(String legStatus) {
		this.legStatus = legStatus;
	}

}
