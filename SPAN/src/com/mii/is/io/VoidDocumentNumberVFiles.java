package com.mii.is.io;

import java.util.List;

public class VoidDocumentNumberVFiles {
	private String fileName;
	private List<String> documentNoList;
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public List<String> getDocumentNoList() {
		return documentNoList;
	}
	public void setDocumentNoList(List<String> documentNoList) {
		this.documentNoList = documentNoList;
	}
}
