package com.mii.is.io;

public class GetSPANSummariesVSPANSummaries {
	private String fileName;
	private String  retryStatusL1;
	private String  retryStatusL2;
	private String  returStatus;
	private String  totalAmount;
	private String  totalRecord;
	private String  successStatusL1;
	private String  successStatusL2;
	private String  procStatus;
	private String  returAckStatus;
	private String  successAckStatus;
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getRetryStatusL1() {
		return retryStatusL1;
	}
	public void setRetryStatusL1(String retryStatusL1) {
		this.retryStatusL1 = retryStatusL1;
	}
	public String getRetryStatusL2() {
		return retryStatusL2;
	}
	public void setRetryStatusL2(String retryStatusL2) {
		this.retryStatusL2 = retryStatusL2;
	}
	public String getReturStatus() {
		return returStatus;
	}
	public void setReturStatus(String returStatus) {
		this.returStatus = returStatus;
	}
	public String getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}
	public String getTotalRecord() {
		return totalRecord;
	}
	public void setTotalRecord(String totalRecord) {
		this.totalRecord = totalRecord;
	}
	public String getSuccessStatusL1() {
		return successStatusL1;
	}
	public void setSuccessStatusL1(String successStatusL1) {
		this.successStatusL1 = successStatusL1;
	}
	public String getSuccessStatusL2() {
		return successStatusL2;
	}
	public void setSuccessStatusL2(String successStatusL2) {
		this.successStatusL2 = successStatusL2;
	}
	public String getProcStatus() {
		return procStatus;
	}
	public void setProcStatus(String procStatus) {
		this.procStatus = procStatus;
	}
	public String getReturAckStatus() {
		return returAckStatus;
	}
	public void setReturAckStatus(String returAckStatus) {
		this.returAckStatus = returAckStatus;
	}
	public String getSuccessAckStatus() {
		return successAckStatus;
	}
	public void setSuccessAckStatus(String successAckStatus) {
		this.successAckStatus = successAckStatus;
	}
}
