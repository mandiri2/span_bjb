package com.mii.is.io;

public class SpanRetryOrReturInput {
	private String iBatchId;
	private String iTrxHeaderId;
	private String iFileName;
	private String iRetry;
	private String iChannelId;
	private String iCreditAcct;
	private String iCreditAccType;
	private String iStatus;
	private String iPriority;
	private String iDebitAccountSC;
	private String iLegStatus;
	
	public String getiBatchId() {
		return iBatchId;
	}
	public void setiBatchId(String iBatchId) {
		this.iBatchId = iBatchId;
	}
	public String getiTrxHeaderId() {
		return iTrxHeaderId;
	}
	public void setiTrxHeaderId(String iTrxHeaderId) {
		this.iTrxHeaderId = iTrxHeaderId;
	}
	public String getiFileName() {
		return iFileName;
	}
	public void setiFileName(String iFileName) {
		this.iFileName = iFileName;
	}
	public String getiRetry() {
		return iRetry;
	}
	public void setiRetry(String iRetry) {
		this.iRetry = iRetry;
	}
	public String getiChannelId() {
		return iChannelId;
	}
	public void setiChannelId(String iChannelId) {
		this.iChannelId = iChannelId;
	}
	public String getiCreditAcct() {
		return iCreditAcct;
	}
	public void setiCreditAcct(String iCreditAcct) {
		this.iCreditAcct = iCreditAcct;
	}
	public String getiCreditAccType() {
		return iCreditAccType;
	}
	public void setiCreditAccType(String iCreditAccType) {
		this.iCreditAccType = iCreditAccType;
	}
	public String getiStatus() {
		return iStatus;
	}
	public void setiStatus(String iStatus) {
		this.iStatus = iStatus;
	}
	public String getiPriority() {
		return iPriority;
	}
	public void setiPriority(String iPriority) {
		this.iPriority = iPriority;
	}
	public String getiDebitAccountSC() {
		return iDebitAccountSC;
	}
	public void setiDebitAccountSC(String iDebitAccountSC) {
		this.iDebitAccountSC = iDebitAccountSC;
	}
	public String getiLegStatus() {
		return iLegStatus;
	}
	public void setiLegStatus(String iLegStatus) {
		this.iLegStatus = iLegStatus;
	}
	
}
