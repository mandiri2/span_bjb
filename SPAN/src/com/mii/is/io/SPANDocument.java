package com.mii.is.io;

import java.util.List;

import com.mii.doc.ApplicationArea;
import com.mii.doc.DataArea;
import com.mii.doc.Footer;

public class SPANDocument {
	ApplicationArea applicationArea;
	List<DataArea> dataArea;
	Footer footer;

	public ApplicationArea getApplicationArea() {
		return applicationArea;
	}

	public void setApplicationArea(ApplicationArea applicationArea) {
		this.applicationArea = applicationArea;
	}

	public List<DataArea> getDataArea() {
		return dataArea;
	}

	public void setDataArea(List<DataArea> dataArea) {
		this.dataArea = dataArea;
	}

	public Footer getFooter() {
		return footer;
	}

	public void setFooter(Footer footer) {
		this.footer = footer;
	}

}
