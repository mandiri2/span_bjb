package com.mii.is.io;

import java.util.List;

import com.mii.is.io.helper.OutputDetails;
import com.mii.is.io.helper.SPANSummaries;

public class GetSPANSummariesResponse {
	private List<SPANSummaries> SPANSummaries;
	private List<OutputDetails> outputDetails;
	private String grandTotalAmount;
	private String grandTotalRecord;

	public List<SPANSummaries> getSPANSummaries() {
		return SPANSummaries;
	}

	public void setSPANSummaries(List<SPANSummaries> sPANSummaries) {
		SPANSummaries = sPANSummaries;
	}

	public List<OutputDetails> getOutputDetails() {
		return outputDetails;
	}

	public void setOutputDetails(List<OutputDetails> outputDetails) {
		this.outputDetails = outputDetails;
	}

	public String getGrandTotalAmount() {
		return grandTotalAmount;
	}

	public void setGrandTotalAmount(String grandTotalAmount) {
		this.grandTotalAmount = grandTotalAmount;
	}

	public String getGrandTotalRecord() {
		return grandTotalRecord;
	}

	public void setGrandTotalRecord(String grandTotalRecord) {
		this.grandTotalRecord = grandTotalRecord;
	}
}
