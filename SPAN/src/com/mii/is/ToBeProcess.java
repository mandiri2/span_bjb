package com.mii.is;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.mii.constant.Constants;
import com.mii.dao.SPANDataValidationDao;
import com.mii.dao.SystemParameterDao;
import com.mii.dao.ToBeProcessDao;
import com.mii.is.io.CheckingMVAAcountOutput;
import com.mii.is.io.ForceReturInput;
import com.mii.is.io.ForceReturOutput;
import com.mii.is.io.RetryReqByDocNoInput;
import com.mii.is.io.RetryReqByDocNoOutput;
import com.mii.is.io.RetryReqInput;
import com.mii.is.io.RetryReqOutput;
import com.mii.is.io.ReturReqByDocNoInput;
import com.mii.is.io.ReturReqByDocNoOutput;
import com.mii.is.io.ReturReqInput;
import com.mii.is.io.ReturReqOutput;
import com.mii.is.io.SpanRetryOrReturInput;
import com.mii.is.io.SpanRetryOrReturOutput;
import com.mii.is.io.ToBeProcessInput;
import com.mii.is.io.ToBeProcessOutput;
import com.mii.is.io.UpdateSPANNoTrxOutput;
import com.mii.is.io.WaitReqByDocNoInput;
import com.mii.is.io.WaitReqByDocNoOutput;
import com.mii.is.io.WaitReqInput;
import com.mii.is.io.WaitReqOutput;
import com.mii.is.io.helper.Files;
import com.mii.is.io.helper.GetDataMVARetryOutput;
import com.mii.is.io.helper.GetRetryErrorResponseOutput;
import com.mii.is.io.helper.ResultDoc;
import com.mii.is.io.helper.ResultList;
import com.mii.is.util.PubUtil;
import com.mii.models.SPANDataValidation;
import com.mii.models.SPANDataValidationErrorStatusModel;
import com.mii.models.SystemParameter;

public class ToBeProcess {

	public static ToBeProcessOutput toBeProcess(ToBeProcessInput input, SPANDataValidationDao spanDataValidationDao, ToBeProcessDao toBeProcessDao,
			SystemParameterDao systemParameterDao) {
		ToBeProcessOutput output = new ToBeProcessOutput();

		// branch one exeType
		if (input.getExeType().equals("1")) {
			// do nothing
		} else {
			input.setExeType("0");
		}

		// sequence try catch
		try {
			// branch on exeType
			// execute by filename
			if (input.getExeType().equals("0")) {
				switch (input.getTypeProcess()) {
				case "0":
					// service waitReq
					// mapping input
					WaitReqInput waitReqInput = new WaitReqInput();
					waitReqInput.setFiles(input.getFiles());
					waitReqInput.setLegStatus(input.getLegStatus());
					
					WaitReqOutput waitReqOutput = new WaitReqOutput();
					
					// invoke service
					waitReqOutput = waitReq(waitReqInput, toBeProcessDao);
					
					output.setStatus(waitReqOutput.getStatus());
					output.setErrorMessage(waitReqOutput.getErrorMessage());
					break;

				case "1":
					// service returReq
					ReturReqInput returReqInput = new ReturReqInput();
					returReqInput.setFiles(input.getFiles());
					returReqInput.setAccountRetur(input.getAccountRetur());
					returReqInput.setLegStatus(input.getLegStatus());
					
					ReturReqOutput returReqOutput = new ReturReqOutput();
					
					returReqOutput = returReq(returReqInput, toBeProcessDao, systemParameterDao);
					
					output.setStatus(returReqOutput.getStatus());
					output.setErrorMessage(returReqOutput.getErrorMessage());
					break;

				case "2":
					// service retryReq
					RetryReqInput retryReqInput = new RetryReqInput();
					retryReqInput.setFiles(input.getFiles());
					retryReqInput.setLegStatus(input.getLegStatus());
					
					RetryReqOutput retryReqOutput = new RetryReqOutput();
					retryReqOutput = retryReq(retryReqInput, toBeProcessDao);
					
					output.setStatus(retryReqOutput.getStatus());
					output.setErrorMessage(retryReqOutput.getErrorMessage());
					break;

				case "3":
					// service spanACKExecuted
					spanACKExecuted(input.getFiles(), spanDataValidationDao);
					output.setStatus("Success");
					break;

				case "4":
					// service forceRetur
					ForceReturInput forceReturInput = new ForceReturInput();
					forceReturInput.setFiles(input.getFiles());
					forceReturInput.setLegStatus(input.getLegStatus());
					forceReturInput.setProviderCode(input.getProviderCode());
					
					ForceReturOutput forceReturOutput = new ForceReturOutput();
					forceReturOutput = forceRetur(forceReturInput);
					
					output.setStatus(forceReturOutput.getStatus());
					output.setErrorMessage(forceReturOutput.getErrorMessage());
					break;

				default:
					break;
				}

			}
			// execute by doc numbers
			else if (input.getExeType().equals("1")) {
				switch (input.getTypeProcess()) {
				case "0":
					// mapping input
					WaitReqByDocNoInput waitReqByDocNoInput = new WaitReqByDocNoInput();
					waitReqByDocNoInput.setFiles(input.getFiles());
					waitReqByDocNoInput.setLegStatus(input.getLegStatus());

					// mapping output
					WaitReqByDocNoOutput waitReqByDocNoOutput = new WaitReqByDocNoOutput();
					waitReqByDocNoOutput = waitReqByDocNo(waitReqByDocNoInput, toBeProcessDao);
					
					output.setStatus(waitReqByDocNoOutput.getStatus());
					output.setErrorMessage(waitReqByDocNoOutput.getErrorMessage());
					break;

				case "1":
					// service returReqByDocNo
					// mapping input
					ReturReqByDocNoInput returReqByDocNoInput = new ReturReqByDocNoInput();
					returReqByDocNoInput.setFiles(input.getFiles());
					returReqByDocNoInput.setAccountRetur(input.getAccountRetur());
					returReqByDocNoInput.setLegStatus(input.getLegStatus());
					
					// mapping output
					ReturReqByDocNoOutput returReqByDocNoOutput = new ReturReqByDocNoOutput();
					returReqByDocNoOutput = returReqByDocNo(returReqByDocNoInput, toBeProcessDao);
					
					output.setStatus(returReqByDocNoOutput.getStatus());
					output.setErrorMessage(returReqByDocNoOutput.getErrorMessage());
					break;

				case "2":
					// service retryReqByDocNo
					// mapping input
					RetryReqByDocNoInput retryReqByDocNoInput = new RetryReqByDocNoInput();
					retryReqByDocNoInput.setFiles(input.getFiles());
					retryReqByDocNoInput.setLegStatus(input.getLegStatus());
					
					// mapping output
					RetryReqByDocNoOutput retryReqByDocNoOutput = new RetryReqByDocNoOutput();
					retryReqByDocNoOutput = retryReqByDocNo(retryReqByDocNoInput, toBeProcessDao);
					
					output.setStatus(retryReqByDocNoOutput.getStatus());
					output.setErrorMessage(retryReqByDocNoOutput.getErrorMessage());
					break;

				case "4":
					// service forceRetur
					ForceReturInput forceReturInput = new ForceReturInput();
					forceReturInput.setFiles(input.getFiles());
					forceReturInput.setLegStatus(input.getLegStatus());
					forceReturInput.setProviderCode(input.getProviderCode());
					
					ForceReturOutput forceReturOutput = new ForceReturOutput();
					forceReturOutput = forceRetur(forceReturInput);
					
					output.setStatus(forceReturOutput.getStatus());
					output.setErrorMessage(forceReturOutput.getErrorMessage());
					break;

				default:
					break;
				}
			}
		} catch (Exception e) {
			output.setStatus("ERROR");
			output.setErrorMessage(e.getMessage());
			e.printStackTrace();
		}

		return output;
	}
	
	private static RetryReqByDocNoOutput retryReqByDocNo(
			RetryReqByDocNoInput input, ToBeProcessDao toBeProcessDao) {
		
		RetryReqByDocNoOutput output = new RetryReqByDocNoOutput();
		
		// map
		String channelID = "18";
		input.setLegStatus(input.getLegStatus().replaceAll("L", ""));
		
		// branch on legStatus
		if (input.getLegStatus().equals("1")){
			input.setLegStatus("L1");
		}
		else if (input.getLegStatus().equals("2")){
			input.setLegStatus("L2");
		}
		
		// service selectSPANDataValidation
		String ErrorStatus = selectSPANDataValidationErrorStatus(
				input.getFiles().get(0).getFileName(), toBeProcessDao);
		
		// branch on ErrorStatus
		if (ErrorStatus.equals("RYU")){
			debugLog("SPAN", "selectSPANDataValidationErrorStatus", 
					"INFO", "For this file ("+input.getFiles().get(0).
						getFileName()+"),please do RETUR first !!!");
		}
		else {
			// branch on legStatus
			if (input.getLegStatus().equals(null)){
				/**
				 * TODO querying getLegStatus with input filename
				 * mapping the output to variable legStatus
				 */
				input.setLegStatus(""); // FIXME with query result
			}
			else {
				// do nothing
			}
			
			// map
			String status = "0";
			
			// branch on status
			if (status.equals("0")){
				// loop
				String documentNoList = null;
				for (int i = 1; i < input.getFiles().get(0).getDocumentNumbers().size(); i++) {
					if (i == 1) {
						documentNoList = input.getFiles().get(0).getDocumentNumbers()
								.get(i);
					} else {
						documentNoList = PubUtil.concatString(documentNoList, "|",
								input.getFiles().get(0).getDocumentNumbers().get(i));
					}
				}
				
				// sleep 1000
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				// batchID
				String batchID = batchID(toBeProcessDao);

				// trxHeaderID
				String trxHeaderID = trxHeaderID(toBeProcessDao);
				
				/**
				 * TODO querying storeProcedure spanRetryByDocNumber
				 * with input [filename, legstatus, channelid, 
				 * documentNoList, batchid, trxheaderid,
				 *  status = 1, priority = 2]
				 * mapping the output to variable
				 * Error Message, Status
				 */
				output.setStatus(""); // FIXME with query result
				output.setErrorMessage(""); // FIXME with query result
			}
			else {
				// do nothing
			}
		}
		
		return output;
	}

	private static ReturReqByDocNoOutput returReqByDocNo(
			ReturReqByDocNoInput input, ToBeProcessDao toBeProcessDao) {
		
		ReturReqByDocNoOutput output = new ReturReqByDocNoOutput();
		
		/**
		 * TODO querying (getProviderInfo) with input accountRetur
		 * mapping the output to variable [ResultList, ResultDoc]
		 */
		ResultList rl = new ResultList(); // FIXME with query result
		ResultDoc rd = new ResultDoc(); // FIXME with query result
		
		// map
		String channelID = "18";
		String debitAcctSC = ""; // FIXME with true data
		
		// branch on legStatus
		if (input.getLegStatus().equals("1")){
			input.setLegStatus("L1");
		}
		else if (input.getLegStatus().equals("2")){
			/**
			 * TODO querying getLegStatus with input filename
			 * mapping the output to variable legStatus
			 */
			input.setLegStatus(""); // FIXME with query result
		}
		else {
			input.setLegStatus("L1");
		}
		
		// loop
		String documentNoList = null;
		for (int i = 1; i < input.getFiles().get(0).getDocumentNumbers().size(); i++) {
			if (i == 1) {
				documentNoList = input.getFiles().get(0).getDocumentNumbers()
						.get(i);
			} else {
				documentNoList = PubUtil.concatString(documentNoList, "|",
						input.getFiles().get(0).getDocumentNumbers().get(i));
			}
		}
		
		// batchID
		String batchID = batchID(toBeProcessDao);

		// trxHeaderID
		String trxHeaderID = trxHeaderID(toBeProcessDao);
		
		/**
		 * TODO querying storeProcedure spanReturByDocNumber
		 * with input [filename, legstatus, channelid, 
		 * documentNoList, batchid, trxheaderid, debitAcctSC, 
		 * accountRetur, providerName, status = 1, priority = 3]
		 * mapping the output to variable
		 * Error Message, Status
		 */
		output.setStatus(""); // FIXME with query result
		output.setErrorMessage(""); // FIXME with query result
		
		return output;
	}

	private static WaitReqByDocNoOutput waitReqByDocNo(
			WaitReqByDocNoInput input, ToBeProcessDao toBeProcessDao) {
		
		WaitReqByDocNoOutput output = new WaitReqByDocNoOutput();
		
		String channelID = "";
		String documentNoList = "";
		try {
			// map initiate
			channelID = "18";
			input.setLegStatus(input.getLegStatus().replaceAll("L", ""));
			
			// loop
			for (int i = 1; i < input.getFiles().get(0).getDocumentNumbers().size(); i++){
				if (i == 1){
					documentNoList = input.getFiles().get(0).getDocumentNumbers().get(i);
				}
				else {
					documentNoList = PubUtil.concatString(documentNoList, "|", 
							input.getFiles().get(0).getDocumentNumbers().get(i));
				}
			}
			
			// sequence main process
			// branch on legStatus
			if (input.getLegStatus().equals("1")){
				input.setLegStatus("L1");
			}
			else if (input.getLegStatus().equals("2")){
				/**
				 * TODO querying getLegStatus with input filename
				 * mapping the output to variable legStatus
				 */
				input.setLegStatus(""); // FIXME with query result
			}
			
			// batchID
			String batchID = batchID(toBeProcessDao);
			
			// trxHeaderID
			String trxHeaderID = trxHeaderID(toBeProcessDao);
			
			/**
			 * TODO querying storeProcedure spanWaitByDocNo
			 * with input [filename, legstatus, channelid, 
			 * documentNoList, batchid, trxheaderid, priority = 2]
			 * mapping the output to variable
			 * Error Message
			 */
			output.setErrorMessage(""); // FIXME with query result
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		output.setStatus("");
		output.setErrorMessage("");
		
		return output;
	}

	public static ForceReturOutput forceRetur(ForceReturInput input){
		ForceReturOutput output = new ForceReturOutput();
		
		try {
			String documentNoList = "";
			String addClause = "";
			// loop
			for (Files f : input.getFiles()){
				// map
				String RCForceRetur = "999";
				String totalForceRetur = "0";
				String sizeDocNumList = f.getDocumentNumbers().size()+"";
				
				// branch
				if (sizeDocNumList.equals("0")){
					// map
					addClause = "";
				}
				else {
					// loop
					for (int i = 1; i <= f.getDocumentNumbers().size(); i++){
						if (i == 1) {
							documentNoList = f.getDocumentNumbers().get(i);
						}
						else {
							documentNoList = documentNoList + "'"+f.getDocumentNumbers().get(i)+"'";
						}
					}
					// map
					documentNoList = "("+documentNoList+")";
					
					//map
					addClause = "AND b.DOC_NUMBER in %documentNoList%";
					
				}
				
				// branch
				switch (input.getLegStatus()) {
				case "1":
					input.setLegStatus("L1");
					break;
				case "L1":
					// do nothing
					break;
				case "": // null
					input.setLegStatus("L1");
					break;
					
				default:
					input.setLegStatus("SC");
					break;
				}
				
				// branch
				List<GetRetryErrorResponseOutput> errorHostResponse = null;
				if (input.getProviderCode().equals("SPN02")){
					/**
					 * TODO querying (checkGAJIType) with input filename
					 * mapping the output to variable legStatus
					 */
					input.setLegStatus(""); // FIXME with query result
					
					// branch on legStatus
					if (input.getLegStatus().equals("L1")){
						/**
						 * TODO querying (getRetryErrorResponse) with input
						 * [filename, addClause, providerCode, legStatus]
						 * mapping the output to variable
						 * [errorHostResponse]
						 */
						errorHostResponse = new ArrayList<>();
					}
				}
				else {
					/**
					 * TODO querying (getRetryErrorResponse) with input
					 * [filename, addClause, providerCode, legStatus]
					 * mapping the output to variable
					 * [errorHostResponse]
					 */
					errorHostResponse = new ArrayList<>();
				}
				
				int totalRetur = 0; 
				int totalRetry = 0; 
				int sumStatusRty = 0;
				int sumStatusRtu = 0;
				int totalProcess = 0;
				int returStatus = 0;
				int retryStatus = 0;
				// loop
				for (GetRetryErrorResponseOutput o : errorHostResponse){
					// branch
					if (o.getBackend().equals("SC")){
						// do nothing
					}
					else {
						/**
						 * TODO querying (updateSPANHostResponse) with input
						 * [RCForceRetur, batchID, trxDetailID]
						 */
					}
					
					// branch
					if (o.getErrorCode().equals("024")){
						// mapping
						String sqlUpdate = "update SPAN_HOST_DATA_DETAILS set "
								+ "ftservices = 'IBU', status='0', payment_methods='0' "
								+ "where batchid = '"+o.getBatchID()+"'  and "
								+ " trxdetailid = '"+o.getTrxDetailID()+"'";
						
						// updateSPANNoTrx
						updateSPANNoTrx(sqlUpdate);
					}
					
					// map
					totalForceRetur = Integer.parseInt(totalForceRetur)+1+"";
					
					// map
					totalForceRetur = "1";
					
					/**
					 * TODO querying getSpanHostDataFailedByBatchId with input
					 * [Files/filename, errorHostResponse/batchID]
					 * mapping the output to variable
					 * [totalRetur, totalRetry, sumStatusRty, sumStatusRtu, totalProcess]
					 */
					totalRetur = 0; // FIXME with query result
					totalRetry = 0; // FIXME with query result
					sumStatusRty = 0; // FIXME with query result
					sumStatusRtu = 0; // FIXME with query result
					totalProcess = 0; // FIXME with query result
					
					/**
					 * TODO querying (countSpanHostDataFailed) with input
					 * [filename]
					 * mapping the output to variable maxProcess
					 */
					int maxProcess = 0;
					
					// branch
					if (maxProcess == totalProcess){
						// map
						totalRetur = totalRetur + Integer.parseInt(totalForceRetur);
						totalRetry = totalRetry - Integer.parseInt(totalForceRetur);
						sumStatusRtu = 2;
						returStatus = 0;
						
						// branch
						if (totalRetry > 0){
							sumStatusRty = 2;
							retryStatus = 0;
						}
						else {
							sumStatusRty = 1;
							retryStatus = 0;
						}

						// service updateSPANNoTrx
						UpdateSPANNoTrxOutput updateSPANNoTrxOutput = new UpdateSPANNoTrxOutput();
						updateSPANNoTrxOutput = updateSPANNoTrx("UPDATE SPAN_HOST_DATA_FAILED SET RETUR_STATUS "
								+ "= '"+totalRetur+"', RETRY_STATUS = '"+totalRetry+"',"
								+ " SUM_STATUS_RTY = '"+sumStatusRty+"',SUM_STATUS_RTU"
								+ " = '"+sumStatusRtu+"' WHERE FILE_NAME = '"+input.getFiles().get(0).getFileName()+"' "
								+ "AND BATCHID = '%errorHostResponse/BATCHID%'");
						output.setStatus(updateSPANNoTrxOutput.getStatus());
						output.setErrorMessage(updateSPANNoTrxOutput.getErrorMessage());
					}
					else {
						// map
						totalRetur = totalRetur + Integer.parseInt(totalForceRetur);
						totalRetry = totalRetry - Integer.parseInt(totalForceRetur);
						sumStatusRtu = 2;
						returStatus = 0;
						
						// branch
						if (totalRetry > 0){
							sumStatusRty = 2;
						}
						else {
							sumStatusRty = 1;
						}
						
						// service updateSPANNoTrx
						UpdateSPANNoTrxOutput updateSPANNoTrxOutput = new UpdateSPANNoTrxOutput();
						updateSPANNoTrxOutput = updateSPANNoTrx("UPDATE SPAN_HOST_DATA_FAILED SET RETUR_STATUS = "
								+ "'"+totalRetur+"', RETRY_STATUS = '"+totalRetry+"', SUM_STATUS_RTY = "
								+ "'"+sumStatusRty+"', SUM_STATUS_RTU = '"+sumStatusRtu+"' WHERE FILE_NAME"
								+ " = '"+input.getFiles().get(0).getFileName()+"' AND BATCHID = "
								+ "'"+o.getBatchID()+"'");
						output.setStatus(updateSPANNoTrxOutput.getStatus());
						output.setErrorMessage(updateSPANNoTrxOutput.getErrorMessage());
						
						// sequence update the last host_data_failed
						/**
						 * TODO querying (getSpanHostDataFailedByTotalProcess)
						 * with input [filename, total_process]
						 * mapping the output to variable [totalRetur, totalRetry, sumStatusRty, sumStatusRtu]
						 */
						
						// map
						totalRetur = totalRetur + Integer.parseInt(totalForceRetur);
						totalRetry = totalRetry - Integer.parseInt(totalForceRetur);
						sumStatusRtu = 2;
						
						// branch
						if (totalRetry > 0){
							sumStatusRty = 2;
						}
						else {
							sumStatusRty = 1;
							retryStatus = 1;
						}
						
						// service updateSPANNoTrx
						updateSPANNoTrxOutput = new UpdateSPANNoTrxOutput();
						updateSPANNoTrxOutput = updateSPANNoTrx("UPDATE SPAN_HOST_DATA_FAILED "
								+ "SET RETUR_STATUS = '"+totalRetur+"', RETRY_STATUS = '"+totalRetry+"', "
								+ "SUM_STATUS_RTY = '"+sumStatusRty+"', SUM_STATUS_RTU = '"+sumStatusRtu+"' WHERE "
								+ "FILE_NAME = '"+input.getFiles().get(0).getFileName()+"' AND TOTAL_PROCESS = "
								+ "'"+maxProcess+"'");
						output.setStatus(updateSPANNoTrxOutput.getStatus());
						output.setErrorMessage(updateSPANNoTrxOutput.getErrorMessage());
					}
				}
				
				// service updateSPANNoTrx
				UpdateSPANNoTrxOutput updateSPANNoTrxOutput = new UpdateSPANNoTrxOutput();
				updateSPANNoTrxOutput = updateSPANNoTrx("UPDATE SPAN_DATA_VALIDATION "
						+ "SET RETUR_STATUS = '"+returStatus+"', RETRY_STATUS = '"+retryStatus+"' "
						+ "WHERE FILE_NAME = '"+input.getFiles().get(0).getFileName()+"'");
				output.setStatus(updateSPANNoTrxOutput.getStatus());
				output.setErrorMessage(updateSPANNoTrxOutput.getErrorMessage());
				
			}
			
		} catch (Exception e) {
			 e.printStackTrace();
			 output.setStatus("ERROR");
			 output.setErrorMessage(e.getMessage());
		}
		
		return output;
	}
	
	public static UpdateSPANNoTrxOutput updateSPANNoTrx(String sql){
		UpdateSPANNoTrxOutput output = new UpdateSPANNoTrxOutput();
		try {
			/**
			 * TODO querying (updateSPANStatusNoTrx)
			 * with input sql
			 */
			
			// map
			output.setStatus("SUCCESS");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("update SPAN "+e.getMessage());
			output.setStatus("ERROR");
			output.setErrorMessage(e.getMessage());
		}
		
		return output;
	}
	
	public static void spanACKExecuted(List<Files> files, SPANDataValidationDao spanDataValidationDao){
		String sql = "";
		// loop
		for (Files f : files){
			/**
			 * Querying (selectSPANDataValidation) with input filename
			 * mapping the output to variable 
			 * [PROC_STATUS, RESPONSE_CODE]
			 */
			SPANDataValidation sdv = spanDataValidationDao.getSPANDataValidationByFilename(f.getFileName());
			String PROC_STATUS = sdv.getProcStatus();
			String RESPONSE_CODE = sdv.getResponseCode();
			
			// branch on proc status
			if (PROC_STATUS.equals("4")){
				if (RESPONSE_CODE.equals("0000")) {
					// map
					spanDataValidationDao.updateProcStatusAndDescriptionByFilename("6", "Send ACK to DEPKEU still on progress", f.getFileName());
					/*sql = "update SPAN_DATA_VALIDATION set PROC_STATUS = '6', PROC_DESCRIPTION "
							+ "= 'Send ACK to DEPKEU still on progress', UPDATE_DATE = current_timestamp where "
							+ "FILE_NAME = "+f.getFileName()+"";*/
				}
				else {
					debugLog("SPAN", "spanACKExecuted", "INFO","Data has an error status, soa system will send the FORCED ACK's");
					spanDataValidationDao.updateProcStatusAndDescriptionByFilenameForceACK("6", "Send ACK to DEPKEU still on progress", f.getFileName());
					/*sql = "update SPAN_DATA_VALIDATION set PROC_STATUS = '6', PROC_DESCRIPTION = "
							+ "'Send ACK to DEPKEU still on progress', UPDATE_DATE = current_timestamp, "
							+ "FORCED_ACK='1' where FILE_NAME = "+f.getFileName()+"";*/
				}
				
				// service updateSPAN
//				updateSPAN(sql);
			} 
			else {
				debugLog("SPAN", "spanACKExecuted", "INFO",
						"Data has been sent to span before check the logs please...");
			}
		}
			
	}
	
	/*public static UpdateSPANOutput updateSPAN(String sql){
		UpdateSPANOutput output = new UpdateSPANOutput();
		
		System.out.println(sql);
		try {
			*//**
			 * TODO querying (updateSPANStatus) with input sql
			 *//*
			output.setStatus("SUCCESS");
			
		} catch (Exception e) {
			 e.printStackTrace();
			 System.out.println(e.getMessage());
			 output.setStatus("ERROR");
			 output.setErrorMessage(e.getMessage());
		}
		
		return output;
	}*/
	
	public static RetryReqOutput retryReq(RetryReqInput input, ToBeProcessDao toBeProcessDao){
		RetryReqOutput output = new RetryReqOutput();
		
		// map
		String debitAcctSC = ""; // FIXME with true data
		input.setLegStatus(input.getLegStatus().replaceAll("L", ""));
		String channelID = "18";
		
		// branch on legStatus
		if (input.getLegStatus().equals("1")){
			input.setLegStatus("L1");
		}
		else if (input.getLegStatus().equals("2")){
			input.setLegStatus("L2");
		}
		
		// loop files
		for (Files f : input.getFiles()){
			// service selectSPANDataValidationErrorStatus
			String errorStatus = selectSPANDataValidationErrorStatus(f.getFileName(), toBeProcessDao);
			
			// branch error status
			if (errorStatus.equals("RYU")) {
				debugLog("SPAN", "selectSPANDataValidationErrorStatus", "INFO",
						"For this file ("+f.getFileName()+"), please do RETUR first !!!");
				output.setErrorMessage("For this file ("+f.getFileName()+"), please do RETUR first !!!");
				output.setStatus("FAILED");
			}
			else {
				// branch on legstatus
				if (input.getLegStatus().equals(null)){
					/**
					 * TODO querying (getLegStatus) with input filename
					 * mapping the output to varible legStatus
					 */
					input.setLegStatus(""); // FIXME with query result
				}
				else {
					// do nothing
				}
								
				// map
				output.setStatus("0");
				
				// branch on status
				if (output.getStatus().equals("0")){
					// sleep 1000
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					
					String batchID = batchID(toBeProcessDao);
					String trxHeaderID = trxHeaderID(toBeProcessDao);
					
					/**
					 * Querying (spanRetryOrRetur)
					 * with input [channelid, debitAcctSC, filename,
					 * retry = 1, status = 1, priority = 2, legStatus]
					 * mapping the output to variable 
					 * [Status, ErrorStatus]
					 */
					SpanRetryOrReturInput spanRetryOrReturInput = new SpanRetryOrReturInput();
					spanRetryOrReturInput.setiBatchId(batchID);
					spanRetryOrReturInput.setiTrxHeaderId(trxHeaderID);
					spanRetryOrReturInput.setiFileName(f.getFileName());
					spanRetryOrReturInput.setiRetry("1");
					spanRetryOrReturInput.setiChannelId(channelID);
					spanRetryOrReturInput.setiStatus("1");
					spanRetryOrReturInput.setiPriority("2");
					spanRetryOrReturInput.setiDebitAccountSC(debitAcctSC);
					spanRetryOrReturInput.setiLegStatus(input.getLegStatus());
					SpanRetryOrReturOutput spanRetryOrReturOutput = toBeProcessDao.spanRetryOrRetur(spanRetryOrReturInput);
					output.setStatus(spanRetryOrReturOutput.getoStatus());
					output.setErrorMessage(spanRetryOrReturOutput.getoErrorMessage());
					System.out.println(spanRetryOrReturOutput.getoStatus());
					System.out.println(spanRetryOrReturOutput.getoErrorMessage());
				}
				else {
					// do nothing
				}
			}
		}
		
		return output;
	}
	
	

	public static void debugLog(String sysID, String svcName, String type,
			String message) {
		String concatMessage = PubUtil.concatString("[", sysID, "]", "[",
				svcName, "]", "[", type, "]", message);
		System.out.println(concatMessage);
	}

	public static String selectSPANDataValidationErrorStatus(String fileName, ToBeProcessDao toBeProcessDao){
		String errorStatus = "";
		/**
		 * Querying (selectSPANDataValidationErrorStatus)
		 * with input [fileName], mapping the output to variable
		 * [RETRY_STATUS, RETUR_STATUS]
		 */
		SPANDataValidationErrorStatusModel status = toBeProcessDao.selectSPANDataValidationErrorStatus(fileName);
		String RETRY_STATUS = status.getRetryStatus();
		String RETUR_STATUS = status.getReturStatus();
		
		// branch
		if (RETRY_STATUS.equals("0") && RETUR_STATUS.equals("1")){
			errorStatus = "RY";
		}
		else if (RETRY_STATUS.equals("1") && RETUR_STATUS.equals("0")){
			errorStatus = "RU";
		}
		else if (RETRY_STATUS.equals("1") && RETUR_STATUS.equals("1")){
			errorStatus = "RYU";
		}
		else {
			errorStatus = "1";
		}
		
		return errorStatus;
	}
	
	public static ReturReqOutput returReq(ReturReqInput input, ToBeProcessDao toBeProcessDao, SystemParameterDao systemParameterDao){
		ReturReqOutput output = new ReturReqOutput();
		
		/**
		 * TODO querying (getProviderInfo) with input accountRetur
		 * mapping the output to variable [ResultList, ResultDoc]
		 */
		ResultList rl = new ResultList(); // FIXME with query result
		ResultDoc rd = new ResultDoc(); // FIXME with query result
		
		// map
		String channelID = "18";
		String debitAcctSC = ""; // FIXME with true data
		
		// loop
		for (Files f : input.getFiles()){
			if (input.getLegStatus().equals("1")){
				input.setLegStatus("L1");
			}
			else if (input.getLegStatus().equals("2")){
				/**
				 * TODO querying (getLegStatus) with input filename
				 * mapping the output to variable legStatus
				 */
				input.setLegStatus(""); // FIXME with query result
			}
			else {
				input.setLegStatus("L1");
			}
			
			// batchID
			String batchID = batchID(toBeProcessDao);
			
			// trxHeaderID
			String trxHeaderID = trxHeaderID(toBeProcessDao);
			
			// getSleep
//			Thread.sleep(1000);
			
			/**
			 * Querying (spanRetryOrRetur) with input
			 * [channelID, debitAcctSC, filename, iRetry, accountRetur,
			 * legStatus, providerName, batchID, trxHeaderID, iStatus,
			 * iPriority]
			 * mapping the output to variable Status, ErrorStatus
			 */
			SpanRetryOrReturInput spanRetryOrReturInput = new SpanRetryOrReturInput();
			spanRetryOrReturInput.setiBatchId(batchID);
			spanRetryOrReturInput.setiTrxHeaderId(trxHeaderID);
			spanRetryOrReturInput.setiFileName(f.getFileName());
			spanRetryOrReturInput.setiRetry("0");
			spanRetryOrReturInput.setiChannelId(channelID);
			spanRetryOrReturInput.setiCreditAcct(input.getAccountRetur());
			SystemParameter systemParameterCreditAccType = systemParameterDao.getDetailedParameterByParamName(Constants.DEBIT_ACCOUNT_NO_GAJIR_NAME);
//			spanRetryOrReturInput.setiCreditAccType("RR RPKBUN P GAJI BANK BJB"); //FIXME Parameterized
			spanRetryOrReturInput.setiCreditAccType(systemParameterCreditAccType.getParamValue());
			spanRetryOrReturInput.setiStatus("1");
			spanRetryOrReturInput.setiPriority("3");
			spanRetryOrReturInput.setiDebitAccountSC(debitAcctSC);
			spanRetryOrReturInput.setiLegStatus(input.getLegStatus());
			SpanRetryOrReturOutput spanRetryOrReturOutput = toBeProcessDao.spanRetryOrRetur(spanRetryOrReturInput);
			output.setStatus(spanRetryOrReturOutput.getoStatus());
			output.setErrorMessage(spanRetryOrReturOutput.getoErrorMessage());
			System.out.println(spanRetryOrReturOutput.getoStatus());
			System.out.println(spanRetryOrReturOutput.getoErrorMessage());
		}
		
		return output;
	}

	public static WaitReqOutput waitReq(WaitReqInput input, ToBeProcessDao toBeProcessDao) {
		WaitReqOutput output = new WaitReqOutput();

		String exeFile = "";
		String channelID = "";
		try {
			exeFile = "";
			channelID = "18";
			input.setLegStatus(input.getLegStatus().replaceAll("L", ""));

			// main process
			// loop
			for (Files f : input.getFiles()) {
				if (input.getLegStatus().equals("1")) {
					input.setLegStatus("L1");
				} else if (input.getLegStatus().equals("2")) {
					/**
					 * TODO querying (getLegStatus) with input filename mapping
					 * the output to variable legStatus
					 */
					input.setLegStatus(""); // FIXME set with query result
				}

				// service batchID
				String batchID = batchID(toBeProcessDao);

				// service trxHeaderID
				String trxHeaderID = trxHeaderID(toBeProcessDao);

				// service getSleep
//				Thread.sleep(1000);

				/**
				 *  TODO service store procedure spanWaitExecute
				 *  with input [filename, legStatus, channelID, batchID, trxHeaderID, iPriority = 2]
				 *  mapping the output to variable [status, errorMessage]
				 */
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return output;
	}

	public static String trxHeaderID(ToBeProcessDao toBeProcessDao) {
		String trxHeaderID = "";

		// map
		String dateTime = new SimpleDateFormat("yyyyMMddHHmmss").format(Calendar
				.getInstance().getTime());
		/**
		 * Querying (getSequenceNo) with no input
		 * mapping the output to variable sequenceNo
		 */
		String sequenceNo = spanSequenceNo(toBeProcessDao);
		
		// map
		sequenceNo = sequenceNo.substring(6);

		// map
		trxHeaderID = dateTime+sequenceNo;

		return trxHeaderID;
	}
	
	private static String spanSequenceNo(ToBeProcessDao toBeProcessDao) {
		String spanSequenceNo = toBeProcessDao.getSequenceNo();
		spanSequenceNo = PubUtil.padLeft(spanSequenceNo, "0", 10);
		return spanSequenceNo;
	}

	public static String batchID(ToBeProcessDao toBeProcessDao) {
		String batchID = "";

		// map
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

		/**
		 * Querying (getBatchIDSeq) with no input mapping the output to
		 * variable batchIDSeq
		 */
		String batchIDSeq = getBatchId(toBeProcessDao);

		// map
		batchID = StringUtils.leftPad(batchIDSeq, 10, "0");

		// map
		batchID = "SP-"+sdf.format(new Date())+"-" + batchID;

		return batchID;
	}
	
	private static String getBatchId(ToBeProcessDao toBeProcessDao) {
		String batchId = toBeProcessDao.getBatchIdSeq();
		return batchId;
	}
	
	public static CheckingMVAAcountOutput checkingMVAAccount(String fileName){
		CheckingMVAAcountOutput output = new CheckingMVAAcountOutput();
		
		try {
			/**
			 * TODO querying (getDataMVARetry) with input filename
			 * mapping the output to variable list getDataMVARetryOutput
			 */
			List<GetDataMVARetryOutput> getDataMVARetryOutput = new ArrayList<>(); // FIXME with query result
			
			// map
			String sizeOfList = getDataMVARetryOutput.size()+"";
			
			// branch sizeOfList
			if (sizeOfList.equals("0")){
				output.setStatus("0");
				output.setErrorMessage("Trx in progress");
			}
			else {
				// do nothing map
				
				// loop getDataMVARetryOutput
				for (GetDataMVARetryOutput o : getDataMVARetryOutput){
					// service replaceMvaAcc
					
					// branch
				}
				
				// branch on status
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("[Retry MVA Account] Error = "+e.getMessage());
		}
		
		return null;
	}

}
