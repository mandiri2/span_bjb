package com.mii.is;

import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.mii.constant.Constants;
import com.mii.dao.SPANDataValidationDao;
import com.mii.dao.SystemParameterDao;
import com.mii.is.io.GetSPANSummariesRequest;
import com.mii.is.io.GetSPANSummariesResponse;
import com.mii.is.io.SpanSummariesType0Input;
import com.mii.is.io.SpanSummariesType0Output;
import com.mii.is.io.helper.OutputDetails;
import com.mii.is.io.helper.SPANSummaries;
import com.mii.is.io.helper.StatusDetailOutput;
import com.mii.models.SystemParameter;

public class GetSPANSummaries {
	
	public static GetSPANSummariesResponse getSPANSummaries(GetSPANSummariesRequest request,
			SPANDataValidationDao spanDataValidationDao,
			SystemParameterDao systemParameterDao, String accountType) throws Exception{
		GetSPANSummariesResponse response = new GetSPANSummariesResponse();
		String providerCode = request.getProviderCode();
		String sumType = request.getSumType();
		String dateTimeStart = request.getDateTimeStart();
		String dateTimeEnd = request.getDateTimeEnd();
		String notification = request.getNotification();
		String legStatusInput = request.getLegStatusInput();
		String status = request.getStatus();
		
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		Date date = new Date();
		String dateTimeDefault = dateFormat.format(date);
		response.setGrandTotalAmount("0");
		response.setGrandTotalRecord("0");
		
		/*if("1".equalsIgnoreCase(notification)){
			if(!legStatusInput.isEmpty()){
				String dateNotif = dateFormat.format(date);
				String dateNotifComplete = Constants.DATESTART_PART1.concat(
						dateNotif.concat(Constants.DATESTART_PART2));
				dateNotifComplete = dateNotifComplete.concat(Constants.DATEEND_PART1.concat(
						dateNotif.concat(Constants.DATEEND_PART2)));
				
				// invoke query getSPANSummaries and mapping result to Array SPANSummaries
			}else{
				throw new Exception("Please Input Leg Status");
			}
		}else{*/
			String trxType = "0";
			String dateTime = "";
			
			// branch on dateTimeStart
			if (dateTimeStart == null) {
				dateTimeStart = dateTimeDefault;
			} else {
//				dateTimeStart = dateTimeStart;
			}
	
			// branch on dateTimeEnd
			if (dateTimeEnd == null) {
				dateTimeEnd = dateTimeDefault;
			} else {
//				dateTimeEnd = dateTimeEnd;
			}
			
			// branch on trxType
			SpanSummariesType0Input input = new SpanSummariesType0Input();
			input.setDateStart(dateTimeStart);
			input.setDateEnd(dateTimeEnd);
			input.setSumType(sumType);
			input.setProviderCode(providerCode);
			SpanSummariesType0Output output = new SpanSummariesType0Output();
			if (trxType.equals("0")) {
				output = spanSummariesType0(input, status,
						systemParameterDao, accountType,
						spanDataValidationDao);
			}
			
			// mapping output
			response.setSPANSummaries(output.getSpanSummaries());
			response.setOutputDetails(output.getOutputDetails());
			response.setGrandTotalAmount(output.getGrandTotalAmount());
			response.setGrandTotalRecord(output.getGrandTotalRecord());
			
			// branch on SPANSummaries
			/*if (response.getSPANSummaries().isEmpty()){
				// do nothing
			}*/
			
			// branch on outputDetails[0]
			/*if (response.getOutputDetails().size() > 0){
				if (response.getOutputDetails().get(0) == null){
					// do nothing
				}
			}*/
			
//		}
		
		return response;
	}
	
	public static SpanSummariesType0Output spanSummariesType0(
			SpanSummariesType0Input input, String status,
			SystemParameterDao systemParameterDao, String accountType,
			SPANDataValidationDao spanDataValidationDao) {

		long startTimeMilis = System.currentTimeMillis();
		String trxType = "0";

//		input.setProviderCode(input.getProviderCode().toUpperCase());

		String errorCodes = "1|7|48";
		BigInteger grandTotalAmount = new BigInteger("0");
		BigInteger grandTotalRecord = new BigInteger("0");
		String procStatus = input.getProcStatus();

		List<SPANSummaries> SPANSummariesTemp = null;
		List<SPANSummaries> SPANSummaries = null;
		List<OutputDetails> OutputDetailsList = null;
		// branch on sumType
		switch (input.getSumType()) {
		case "0":
			try{
				if (input.getProviderCode()==null) {
					break; // null -> exit
				} else {
					input.setProcStatus("4");
				}
			}catch(Exception e){
				input.setProcStatus("4");
			}
			break;
		default:
//			input.setProcStatus("1|4|5");
			input.setProcStatus("4|5");
			input.setProviderCd("");
			String dateStart = input.getDateStart();
			String dateEnd = input.getDateEnd();
			
			List<String> procStatusList = new ArrayList<>();
//			String a = "2|3";
			String a = "1|2|3";
			String[] b = a.split("\\|");
			for (int i = 0; i < b.length; i++) {
				procStatusList.add(b[i]);
			}
			SystemParameter norekeningParam = systemParameterDao.getDetailedParameterByParamName(
					Constants.PARAM_DEBITACCNO + accountType);
			/**
			 * querying (selectSPANDataValidationStatusParams) with input
			 * [procStatus = ('1', '2', '3'), dateStart, dateEnd] mapping the
			 * output to variable SPANSummariesTemp (documentList)
			 */
			/*
			 * new dev 20161028, proc status 1 taken out. query add where clause debit_account
			 */
			SPANSummariesTemp = spanDataValidationDao
					.selectSPANDataValidationStatusParams(procStatusList, dateStart, dateEnd, norekeningParam.getParamValue());

			break;
		}

		List<String> procStatusList = new ArrayList<>();
		String[] b = input.getProcStatus().split("\\|");
		for (int i = 0; i < b.length; i++) {
			procStatusList.add(b[i]);
		}
		/**
		 * querying (getSPANSummaries) with input [procStatus, providerCd,
		 * legStatus = L1, dateTime, tableResponse = SPAN_HOST_RESPONSE] mapping
		 * the output to variable SPANSummaries (documentList)
		 */
		SystemParameter norekeningParam = systemParameterDao.getDetailedParameterByParamName(
				Constants.PARAM_DEBITACCNO + accountType);
		/**
		 * actual format date thrown from UI yyyyMMdd, to getSPANSummaries 
		 * we need yyyy-MM-dd (docDate)
		 */
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		String dateStart = "";
		String dateEnd = "";
		try {
			Date date1 = dateFormat.parse(input.getDateStart());
			System.out.println(date1);
			Date date2 = dateFormat.parse(input.getDateEnd());
			System.out.println(date2);
			dateStart = new SimpleDateFormat("yyyy-MM-dd").format(date1);
			System.out.println(dateStart);
			dateEnd = new SimpleDateFormat("yyyy-MM-dd").format(date2);
			System.out.println(dateEnd);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		SPANSummaries = spanDataValidationDao.getSPANSummaries(
				procStatusList, "L1", "SPAN_HOST_RESPONSE", norekeningParam.getParamValue(),
				dateStart, dateEnd);
//		if(SPANSummaries.size()>1){
//			SPANSummariesTemp = new ArrayList<SPANSummaries>();
//		}
		String sumType01 = input.getSumType();

		// appendSPANSummaries
		if(SPANSummariesTemp!=null){
			SPANSummaries = appendSPANSummaries(SPANSummariesTemp, SPANSummaries);
		}

		List<SPANSummaries> newSPANSummaries = new ArrayList<SPANSummaries>();
		switch (input.getSumType()) {
		case "1":
			// loop over SPANSummaries
			for (SPANSummaries s : SPANSummaries) {
				String returACKStatus, successACKStatus;
				switch (s.getProcStatus()) {
				case "5":
					/**
					 * querying (getReturDataDetails) with input [filename]
					 * mapping output to variable RETUR ACK STATUS
					 */
					returACKStatus = spanDataValidationDao
							.getReturDataDetailsACK(s.getFileName());
//					System.err.println(returACKStatus);
//					System.err.println(s.getTotalRecord());
					s.setReturACKStatus(returACKStatus);
					s.setSuccessACKStatus((String.valueOf((Integer.parseInt(s
							.getTotalRecord()) - Integer
							.parseInt(returACKStatus)))));
					break;

				default:
					returACKStatus = "";
					successACKStatus = "";
					break;
				}
				// append to documentList new span summaries
				newSPANSummaries.add(s);
				// add int grand total amount
				grandTotalAmount = grandTotalAmount.add(new BigInteger(s.getTotalAmount()));
				// add int grand total record
				grandTotalRecord = grandTotalRecord.add(new BigInteger(s.getTotalRecord()));
			}
			SPANSummaries = newSPANSummaries;
			break;

		default:
			switch (status) {
			/*case "0":
				for(SPANSummaries s : SPANSummaries){
					if (Integer.parseInt(s.getWaitStatus()) > 0) {
						*//**
						 * to do querying (selectWaitDetails) with input
						 * [providerCode, legStatus = L1, trxType]
						 * mapping the output to variable OutputDetails
						 *//*
						List<OutputDetails> od = null; // replace null here with query result
						break;
					}
					else {
						String emptyString = "True";
						s.setReturStatus("0");
					}
				}
				break;*/
			
			case "1":
				OutputDetailsList = new ArrayList<OutputDetails>();
				for(SPANSummaries s : SPANSummaries){
					if (Integer.parseInt(s.getReturStatus().trim()) > 0) {
						/**
						 * querying (selectReturDetails) with input
						 * [providerCode, legStatus = L1, tableResponse =
						 * SPAN_HOST_RESPONSE, procStatus = 4, trxType]
						 * mapping the output to variable OutputDetails
						 */
						SystemParameter systemParameter = systemParameterDao.getDetailedParameterByParamName
								(Constants.PARAM_DEBITACCNO+accountType);
						System.out.println("trxType : "+trxType);
						System.out.println("account : "+systemParameter.getParamValue());
						OutputDetailsList = spanDataValidationDao.selectReturDetails("L1",
								"SPAN_HOST_RESPONSE", "4", trxType, systemParameter.getParamValue());
						System.out.println("Size : "+OutputDetailsList.size());
						break;
					}
					else {
						String emptyString = "True";
						s.setReturStatus("0");
					}
				}
				break;
				
			case "2":
				OutputDetailsList = new ArrayList<OutputDetails>();
				for(SPANSummaries s : SPANSummaries){
					if (s.getRetryStatus() == null
							|| s.getRetryStatus().equals("")
							|| s.getRetryStatus().equals("null")) {
						s.setRetryStatus("0");
					}
					if (Integer.parseInt(s.getRetryStatus().trim()) > 0) {
						String addConditional = "";
						/**
						 * querying (selectRetryDetails) with input
						 * [providerCode, legStatus = L1, tableResponse =
						 * SPAN_HOST_RESPONSE, procStatus = 4, trxType, addingCondition]
						 * mapping the output to variable OutputDetails
						 */
						SystemParameter systemParameter = systemParameterDao.getDetailedParameterByParamName
									(Constants.PARAM_DEBITACCNO+accountType);
						System.out.println("trxType : "+trxType);
						System.out.println("account : "+systemParameter.getParamValue());
						OutputDetailsList = spanDataValidationDao.selectRetryDetails(
								"L1", "SPAN_HOST_RESPONSE", "4", trxType, systemParameter.getParamValue()
								, addConditional);
						System.out.println("Size : "+OutputDetailsList.size());
						// NOTE : checkSummariesPM3 skipped, not used in BJB		
						break;
					}
					else {
						String emptyString = "True";
						s.setRetryStatusL1("0");
					}
				}
				break;
				
			case "3":
				OutputDetailsList = new ArrayList<OutputDetails>();
				for(SPANSummaries s : SPANSummaries){
					String TOTAL_AMOUNT = "0";
					String TOTAL_RECORD = "0";
					String RETRYSTATS = "0";
					String RETURSTATS = "0";
					OutputDetails outputDetailsTemp = new OutputDetails();
					if (Integer.parseInt(s.getProcStatus().trim()) == 4) {
						/**
						 * querying (selectSuccessDetails) with input
						 * [fileName, legStatus = L1]
						 * mapping the output to variable TOTAL_AMOUNT, TOTAL_RECORD
						 */
						outputDetailsTemp = spanDataValidationDao.selectSuccessDetails(s.getFileName(), "L1");
						TOTAL_AMOUNT = outputDetailsTemp.getTotalAmount();
						TOTAL_RECORD = outputDetailsTemp.getTotalRecord();
						if (TOTAL_AMOUNT.equals("null") || TOTAL_AMOUNT.equals(null)){
							TOTAL_AMOUNT = "";
						}
						if (TOTAL_RECORD.equals("null") || TOTAL_RECORD.equals(null)){
							TOTAL_RECORD = "";
						}
						/**
						 * querying (checkSuccessStatus) with input
						 * [fileName]
						 * mapping the output to variable RETRYSTATS, RETURSTATS
						 */
						StatusDetailOutput sdo = new StatusDetailOutput();
						sdo = spanDataValidationDao.checkSuccessStatus(s.getFileName());
						RETRYSTATS = sdo.getRETRYSTATS();
						RETURSTATS = sdo.getRETURSTATS();
						
						if (!("0").equalsIgnoreCase(RETRYSTATS) || !("0").equalsIgnoreCase(RETURSTATS)){
							TOTAL_AMOUNT = "";
							TOTAL_RECORD = "0";
						}
						switch (TOTAL_RECORD) {
						case "0":
							if (s.getForcedACK() != null && s.getForcedACK().equalsIgnoreCase("1")){
								TOTAL_AMOUNT = s.getTotalAmount();
								TOTAL_RECORD = s.getTotalRecord();
							}
//							break;

						default:
							// do nothing
//							break;
						}
//						break;
					}
					else {
						String emptyString = "True";
					}
					// append to document list
					outputDetailsTemp.setFileName(s.getFileName());
					outputDetailsTemp.setTotalAmount(TOTAL_AMOUNT);
					outputDetailsTemp.setTotalRecord(TOTAL_RECORD);
					OutputDetailsList.add(outputDetailsTemp);
				}
				break;

			default:
				break;
			}
			break;
		}
		
		long endTimeMilis = System.currentTimeMillis();
		
		long result = endTimeMilis - startTimeMilis;

		SpanSummariesType0Output output = new SpanSummariesType0Output();
		output.setSPANSummaries(SPANSummaries);
		output.setOutputDetails(OutputDetailsList);
		output.setGrandTotalAmount(grandTotalAmount.toString());
		output.setGrandTotalRecord(grandTotalRecord.toString());
		return output;
	}

	/**
	 * Tested appendSPANSummaries(from, to)
	 * Goal : append list SPANSummaries
	 * @param from
	 * @param to
	 * @return
	 */
	public static List<SPANSummaries> appendSPANSummaries(
			List<SPANSummaries> from, List<SPANSummaries> to) {

		int n = 0;
		int m = 0;

		List<SPANSummaries> SPANSummariesTemp = from;
		List<SPANSummaries> SPANSummaries = to;

//		SPANSummariesTemp = from;
		try{
			n = from.size();
		}catch(Exception e){
			SPANSummaries = new ArrayList<SPANSummaries>();
		}

//		SPANSummaries = to;
		try{
			m = to.size();
		}catch(Exception e){
			SPANSummariesTemp = new ArrayList<SPANSummaries>();
		}

		List<SPANSummaries> SPANSummariesFinal = new ArrayList<>();

		// lengkapi SPANSummariesTemp;
		for (SPANSummaries s : SPANSummariesTemp) {
			s.setReturStatus("0");
			s.setRetryStatusL1("0");
			s.setRetryStatusL2("0");
			s.setSuccessStatusL1("0");
			s.setSuccessStatusL2("0");
			s.setReturACKStatus("0");
			s.setSuccessACKStatus("0");
			s.setTotalWait("0");
			SPANSummariesFinal.add(s);
		}

		// mapping ke SPANSummariesFinal
//		System.out.println(SPANSummariesFinal.size());
		SPANSummariesFinal.addAll(SPANSummaries);
//		System.out.println(SPANSummariesFinal.size());
//		System.out.println(to.size());
		boolean successAddAll = to.addAll(from);
//		System.out.println(to.size());
		if (successAddAll) {
			return to;
		}
		else {
			return to;
		}
	}
	
}
