package com.mii.is.util;

public class GetTimeMillis {
	public static void main(String[] args) {
		System.out.println(getTimeMillis(""));
	}
	
	public static String getTimeMillis(String startTime){
		long timeMillis = System.currentTimeMillis();
		try{
			if(!startTime.equals("")||startTime!=null){
				long startTimeMillis = Long.parseLong(startTime);
				timeMillis -= startTimeMillis;
			}
		}catch(Exception e){
			//DO NOTHING
		}
		return String.valueOf(timeMillis);
	}
}
