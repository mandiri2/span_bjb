package com.mii.is.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.jar.Attributes;
import java.util.jar.JarEntry;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.primefaces.context.RequestContext;

import com.mii.helpers.FacesUtil;
import com.mii.util.jasper.JasperExporter;
import com.mii.util.jasper.JasperExporterFactory;

public class PubUtil {
	
	public static String concatString(String... strings){
		StringBuilder builder = new StringBuilder();
		
		if (strings != null){
			for (String s : strings){
				builder.append(s);
			}
		}
		
		return builder.toString();
	}
	
	public static String[] tokenize(String inString, String delim){
		return inString.split(Pattern.quote(delim));
	}

	public static Object xmlValuesToDocument(Class yourClass, String xmlValues) throws JAXBException{
		JAXBContext jaxbContext = JAXBContext.newInstance(yourClass);
		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
		
		StringReader reader = new StringReader(xmlValues);
		return unmarshaller.unmarshal(reader);
	}
	
	/**
	 * pub.string:padLeft
	 */
	public static String padLeft(String inString, String padString, int size){
		
		if(inString==null || padString==null || inString.length()>=size)return inString;
		
		StringBuilder appended = new StringBuilder();
		
		for(int i=0; i<(size-inString.length()); i++){
			appended.append(padString);
		}
		appended.append(inString);
		
		return appended.toString();
	}
	
	public static String padRight(String s, int n) {
		return String.format("%1$-" + n + "s", s);
	}
	
	public static String getStringDate(Date date,String format){
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		String dateAsString = "";
		try{
			dateAsString=sdf.format(date);
		}catch(Exception e){
			e.printStackTrace();
		}
		return dateAsString;
	}
	
	public static void moveFile(String filename, String sourcePath, String backupPath) {
		try{
			File file = new File(concatString(sourcePath, filename));
			if(file.renameTo(new File(concatString(backupPath,file.getName())))){
				System.out.println("File is moved successful!");
			}else{
				System.out.println("File is failed to move!");
			}
		    
		}catch(Exception e){
			System.out.println("Exception occured when moving file");
		    e.printStackTrace();
		}
		
	}
	
	public static void runCreateJar(String fileInput, String fileTarget) throws IOException{
	  Manifest manifest = new Manifest();
	  manifest.getMainAttributes().put(Attributes.Name.MANIFEST_VERSION, "1.0");
	  JarOutputStream target = new JarOutputStream(new FileOutputStream(fileTarget), manifest);
	  add(new File(fileInput), target);
	  target.close();
	}
	
	private static void add(File source, JarOutputStream target) throws IOException
	{
	  BufferedInputStream in = null;
	  try
	  {
	    if (source.isDirectory())
	    {
	      //not optimize for directory
	      String name = source.getPath().replace("\\", "/");
	      if (!name.isEmpty())
	      {
	        if (!name.endsWith("/"))
	          name += "/";
	      }
	      for (File nestedFile: source.listFiles())
	        add(nestedFile, target);
	      return;
	    }
	    source.getPath().replace("\\", "/");
	    JarEntry entry = new JarEntry(source.getName());
	    entry.setTime(source.lastModified());
	    target.putNextEntry(entry);
	    in = new BufferedInputStream(new FileInputStream(source));

	    byte[] buffer = new byte[1024];
	    while (true)
	    {
	      int count = in.read(buffer);
	      if (count == -1)
	        break;
	      target.write(buffer, 0, count);
	    }
	    target.closeEntry();
	  }
	  finally
	  {
	    if (in != null)
	      in.close();
	  }
	}
	
	/**
	 * Return String jika object adalah instance of String dan tidak null
	 */
	public static String stringIfNotNull(Object object){
		if(object!=null
				&& object instanceof String){
			return (String)object;
		}
		return null;
	}
	
	/**
	 * check if big integer 0 or null
	 * @param value
	 * @return
	 */
	public static boolean isBigIntegerEmpty(BigInteger value){
		if(value==null
				|| value.compareTo(BigInteger.ZERO)==0){
			return true;
		}
		return false;
	}
	
	public static String byPassString(String value, String comment){
		System.out.println(comment);
		return value;
	}
	
	/**
	 * true if list not null and list.size != 0
	 */
	public static boolean isListEmpty(List list){
		if(list!=null 
				&& list.size()>0){
			return false;
		}
		return true;
	}
	
	public static boolean isListNotEmpty(List list){
		return !isListEmpty(list);
	}
	
	public static List mapToList(Map map){
		List list = null;
		try {
			list = new ArrayList(map.values());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	/**
	 * convert date ke string sesuai dengan pattern
	 * @return
	 */
	public static String dateToString(String pattern, Date date){
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(pattern);
			return sdf.format(date);
		} catch (Exception e) {
			System.out.println("gagal parsing data");
		}
		return null;
	}
	
	public static String dateToStringNewDateIfNull(String pattern, Date date){
		String value = dateToString(pattern, date);
		if(value==null){
			dateToString(pattern, new Date());
		}
		return value;
	}
	
	/**
	 * @throws IOException 
	 * 
	 */
	public static String bytesToFile(String fileName, byte[] bytes, String append) throws IOException{
		FileOutputStream fos = null;
		String length = null;
		try {
			fos = new FileOutputStream(fileName);
			fos.write(bytes);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(fos!=null){
				fos.close();
			}
		}
		return length;
	}
	
	public static String deleteFile(String fileName){
		String status = null;
		
		try {
			File  file = new File(fileName);
			if(file.delete()){
				status = "true";
			}
			else{
				status = "false";
			}
		} catch (Exception e) {
			status = "false";
			e.printStackTrace();
		}
		return status;
	}
	
	
	
	//CREATE REPORT
	public static void generateReport(String fileName, String jasperPath, String type, HashMap<String,Object> parameter, String contentType,List listData) {
		RequestContext.getCurrentInstance().execute("loadingDlg.show();");
		JasperExporter jasperExporter = JasperExporterFactory.getJasperExporter(type, fileName, jasperPath, parameter);

		try {
			byte[] report = jasperExporter.ecportReportFromBean(listData);
			FacesUtil.generateDownloadReport(report, fileName, contentType);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void runJarWithoutManifest(String sorborFile,String fileName){
		byte[] buffer = new byte[1024];

    	try{
    		String outputName=sorborFile.replace(".SBR", ".jar");
    		FileOutputStream fos = new FileOutputStream(outputName);
    		ZipOutputStream zos = new ZipOutputStream(fos);
    		ZipEntry ze= new ZipEntry(fileName);
    		zos.putNextEntry(ze);
    		FileInputStream in = new FileInputStream(sorborFile);

    		int len;
    		while ((len = in.read(buffer)) > 0) {
    			zos.write(buffer, 0, len);
    		}

    		in.close();
    		zos.closeEntry();

    		//remember close it
    		zos.close();

    		System.out.println("Done");

    	}catch(IOException ex){
    	   ex.printStackTrace();
    	}
	}
	
}
