package com.mii.json;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import com.mii.bjb.core.GapuraConnector;
import com.mii.constant.Constants;
import com.mii.dao.SystemParameterDao;
import com.mii.json.model.BaseMessage;
import com.mii.json.model.TransactionHistoryModel;
import com.mii.models.SpanRekeningKoran;
import com.mii.models.SystemParameter;

public class TransactionHistory {

	public static List<SpanRekeningKoran> getTransactionHistory(SystemParameterDao systemParameterDao, String accountNumber, Date start, Date end){
		SystemParameter systemParameterIP = systemParameterDao.getDetailedParameterByParamName(Constants.SPAN_LOCAL_IP);
		SystemParameter systemParameterCID = systemParameterDao.getDetailedParameterByParamName(Constants.GAPURA_CID);
		// pack request
		String request = packTransactionHistory(systemParameterIP.getParamValue(), systemParameterCID.getParamValue(), accountNumber, start, end);
		
		// get history gapura connector
		String response = GapuraConnector.connect(request, systemParameterDao);
		
		// unpack response
		List<SpanRekeningKoran> list = unpackTransactionHistory(response);
		
		return list;
	}
	
	public static String packTransactionHistory(String remoteIp, String CID, String accountNo, Date start, Date end){
		// fixing request message
		BaseMessage base = new BaseMessage();
		TransactionHistoryModel th = new TransactionHistoryModel();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		SimpleDateFormat thDate = new SimpleDateFormat("ddMMYY");
		
		base.setPCC("5");
		base.setCC("0001");
		base.setDT(sdf.format(new Date()));
		base.setST("22147483647");
		base.setPC("001001");
		base.setMC("90023");
		base.setMT("9200");
		base.setFC("TH");
		base.setREMOTEIP(remoteIp);
		base.setSID("singleUserIDWebTeller");
		base.setCID(CID);
		
		th.setZLEAN(accountNo);
		th.setZLVFRZ(thDate.format(start));
		th.setZLVTOZ(thDate.format(end));
		
		base.setMPI(th);
		
		Gson gson = new Gson();
		String request = gson.toJson(base);
		System.out.println("TransactionHistory request message : "+request);
		return request;
	}
	
	@SuppressWarnings("unchecked")
	public static List<SpanRekeningKoran> unpackTransactionHistory(String response){
		Gson gson = new Gson();
		BaseMessage base = gson.fromJson(response, BaseMessage.class);
		List<SpanRekeningKoran> listRekKoran = new ArrayList<SpanRekeningKoran>();
		try{
			Map<String, Object> jsonMapList = (Map<String, Object>) base.getMPO();
//			System.out.println("json map list size : " + jsonMapList.size());
			Set<String> keys = jsonMapList.keySet();
			for (Iterator<String> it = keys.iterator(); it.hasNext();) {
				String key = (String) it.next();
				// get what needed
				LinkedTreeMap<String, Object> map = (LinkedTreeMap<String, Object>) jsonMapList.get(key);
				String ZHAMDR = map.get("ZHAMDR").toString();
				String ZHAMCR = map.get("ZHAMCR").toString();
				String ZHPOD = map.get("ZHPOD").toString();
				String ZHRBAL = map.get("ZHRBAL").toString();
				String ZHTCD = "";
				try{
					ZHTCD = map.get("ZHTCD").toString();
				}catch(Exception e){}
				String ZHNR = map.get("ZHNR").toString();

				SpanRekeningKoran rek = new SpanRekeningKoran();
				rek.setKey(Integer.parseInt(key));
				rek.setTransactionDate(ZHPOD);
				rek.setBankTransactionCode(ZHTCD);
				rek.setDebit(ZHAMDR);
				rek.setCredit(ZHAMCR);
				rek.setBankReferenceNumber(ZHNR.replaceAll("SPAN ", ""));
				rek.setTotalAmount(ZHRBAL);
				listRekKoran.add(rek);
			}
			
			// sorting by key
			Collections.sort(listRekKoran, new RekKoranComparator()); //JAVA 7
//			listRekKoran.sort(new RekKoranComparator()); //FIXME JAVA8
			for(SpanRekeningKoran s : listRekKoran){
				System.out.println("key " + s.getKey() + " " + s.getTransactionDate());
			}
		}catch(Exception e){
			System.out.println("Error Parsing JSON Response Transaction History");
			e.printStackTrace();
		}
		return listRekKoran;
	}
}
