package com.mii.json;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.mii.bjb.core.GapuraConnector;
import com.mii.constant.Constants;
import com.mii.dao.SystemParameterDao;
import com.mii.json.model.AccountBalanceModel;
import com.mii.json.model.BaseMessage;
import com.mii.models.SystemParameter;

public class AccountBalance {

	static Logger logger = Logger.getAnonymousLogger();

	public static String STA() {
		SimpleDateFormat sdf = new SimpleDateFormat("ddHHmmssSSS");
		return sdf.format(new Date()).substring(1, 10);
	}

	public static String packAccountBalance(String remoteIp, String CID, String accountNo) {
		BaseMessage base = new BaseMessage();
		AccountBalanceModel ab = new AccountBalanceModel();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

		base.setPCC("5");
		base.setCC("0001");
		base.setDT(sdf.format(new Date()));
		base.setST(STA());
		base.setPC("001001");
		base.setMC("90023");
		base.setMT("9200");
		base.setFC("AB");
		base.setREMOTEIP(remoteIp);
		base.setSID("singleUserIDWebTeller");
		base.setCID(CID);

		//Internal Account//
		/*ab.setZLAB("0001");
		ab.setZLAS("360");
		ab.setZLAN("800459");*/

		//External Account
		ab.setZLEAN(accountNo);
		//		ab.setZLEAN("0023269392100");

		base.setMPI(ab);

		Gson gson = new Gson();
		String request = gson.toJson(base);
		System.out.println("AccountBalance request message : "+request);
		return request;

	}

	public static String getAccountBalance(SystemParameterDao systemParameterDao, String accountNo){
		SystemParameter systemParameterIP = systemParameterDao.getDetailedParameterByParamName(Constants.SPAN_LOCAL_IP);
		SystemParameter systemParameterCID = systemParameterDao.getDetailedParameterByParamName(Constants.GAPURA_CID);
		String request = packAccountBalance(systemParameterIP.getParamValue(), systemParameterCID.getParamValue(), accountNo);
		String response = GapuraConnector.connect(request, systemParameterDao);
		AccountBalanceModel abm = unpackAccountBalance(response);
		String balance = abm.getZLBAL();
		return balance;
	}

	public static String checkAccountAndName(SystemParameterDao systemParameterDao, String accountNo, String accountName){
		SystemParameter systemParameterIP = systemParameterDao.getDetailedParameterByParamName(Constants.SPAN_LOCAL_IP);
		SystemParameter systemParameterCID = systemParameterDao.getDetailedParameterByParamName(Constants.GAPURA_CID);
		String request = packAccountBalance(systemParameterIP.getParamValue(), systemParameterCID.getParamValue(), accountNo);
		try{
			String response = GapuraConnector.connect(request, systemParameterDao);
			AccountBalanceModel abm = unpackAccountBalance(response);
			System.out.println(abm.getZLRC());
			if(abm.getZLRC().equalsIgnoreCase(Constants.GAPURA_RC_SUCCESS) || abm.getZLRC().equalsIgnoreCase(Constants.GAPURA_RC_NOT_FOUND)){
				String oAccountName = abm.getZLCUN();
				String status = Constants.FAST_NOT_FOUND;
				if(Constants.ERROR.equals(oAccountName)){
					status=Constants.FAST_SALAH_NOMOR;
				}else{
					if(compareAccountNo(oAccountName, accountName, Constants.START_INDEX_NAME_VALIDATION, Constants.END_INDEX_NAME_VALIDATION)){
						if(abm.getZLSTS().equals(Constants.GAPURA_AB_ZLSTS_TUTUP)||abm.getZLSTS().equals(Constants.GAPURA_AB_ZLSTS_BLOKIR)||abm.getZLSTS().equals("Error")){
							status = Constants.FAST_REK_DORMANT;
						}else{
							status = Constants.FAST_FOUND;
						}
					}else{
						status=Constants.FAST_SALAH_NAMA;
					}
				}

				return status;
			}else return Constants.GAPURA_CONNECTION_TIMEOUT;
		}catch(Exception e){
			return Constants.GAPURA_CONNECTION_TIMEOUT;
		}
	}
	
	private static boolean compareAccountNo(String string1, String string2, int startIndex, int endIndex){
		if(substringIfFit(string1, startIndex, endIndex).equals(substringIfFit(string2, startIndex, endIndex))){
			return true;
		}
		return false;
	}
	
	private static String substringIfFit(String input, int startIndex, int endIndex){
		if(input.length()>endIndex){
			return input.substring(startIndex, endIndex);
		}
		return input;
	}

	@SuppressWarnings("unchecked")
	public static AccountBalanceModel unpackAccountBalance(String response) {
		Gson gson = new Gson();
		BaseMessage base = gson.fromJson(response, BaseMessage.class);

		AccountBalanceModel abm = new AccountBalanceModel();
		String ZLBAL = "Error";
		String ZLCUN = "Error";
		String ZLSTS = "Error";
		abm.setZLBAL(ZLBAL);
		abm.setZLCUN(ZLCUN);
		abm.setZLSTS(ZLSTS);
		try{
			abm.setZLRC(base.getRC());
		}catch(Exception e){
			e.printStackTrace();
		}
		try{
			Map<String, Object> jsonMapList = (Map<String, Object>) base.getMPO();
			for(int i=0; i<jsonMapList.size(); i++){
				Map<String, Object> jsonABList = (Map<String, Object>) jsonMapList.get(Integer.toString(i));
				ZLBAL = jsonABList.get("ZLBAL").toString();
				ZLBAL = ZLBAL.trim();
				ZLBAL = ZLBAL.replace(",", "");
				//				ZLSHN = jsonABList.get("ZLSHN").toString();
				ZLCUN = jsonABList.get("ZLCUN").toString();
				ZLSTS = jsonABList.get("ZLSTS").toString();
				abm.setZLBAL(ZLBAL);
				abm.setZLCUN(ZLCUN);
				abm.setZLSTS(ZLSTS);
			}
		}catch(Exception e){
			System.out.println("Error Parsing JSON Response Account Balance");
			e.printStackTrace();
		}
		return abm;
	}

}
