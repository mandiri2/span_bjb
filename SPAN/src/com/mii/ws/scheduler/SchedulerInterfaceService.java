/**
 * SchedulerInterfaceService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.mii.ws.scheduler;

public interface SchedulerInterfaceService extends javax.xml.rpc.Service {
    public java.lang.String getSchedulerInterfaceAddress();

    public com.mii.ws.scheduler.SchedulerInterface getSchedulerInterface() throws javax.xml.rpc.ServiceException;

    public com.mii.ws.scheduler.SchedulerInterface getSchedulerInterface(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
