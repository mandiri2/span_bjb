package com.mii.ws;

import com.mii.constant.Constants;
import com.mii.dao.SystemParameterDao;
import com.mii.helpers.FacesUtil;
import com.mii.models.SystemParameter;
import com.mii.scoped.ApplicationBean;


public class WebServiceConfigurator {

	private String username; 
	
	private String password; 
	
	private String addressInquiry;
	
	private String addressPayment;
	
	private String addressReInquiry;
	
	private String addressScheduler;
	
	private String addressRTGS;
	
	public WebServiceConfigurator(){
		
		SystemParameter systemParameter;
		
		/*systemParameter = getSystemParameterDAOBean().getDetailedParameterByParamName(Constants.WEB_SERVICE_IP);
		String ip		= systemParameter.getParamValue();

		systemParameter = getSystemParameterDAOBean().getDetailedParameterByParamName(Constants.WEB_SERVICE_PORT);
		String port		= systemParameter.getParamValue();*/
		
		systemParameter 	= getSystemParameterDAOBean().getDetailedParameterByParamName(Constants.WEB_SERVICE_INTERNAL_IP);
		String ipInternal 	= systemParameter.getParamValue();

		systemParameter 	= getSystemParameterDAOBean().getDetailedParameterByParamName(Constants.WEB_SERVICE_INTERNAL_PORT);
		String portInternal	= systemParameter.getParamValue();

		/*systemParameter = getSystemParameterDAOBean().getDetailedParameterByParamName(Constants.WEB_SERVICE_USERNAME);
		username = systemParameter.getParamValue();

		systemParameter = getSystemParameterDAOBean().getDetailedParameterByParamName(Constants.WEB_SERVICE_PASSWORD);
		password = systemParameter.getParamValue();*/
		
		/*addressInquiry 		= "http://" +ip+ ":" +port+ "/ws/com.btpn.mpn.ws.Inquiry/com_btpn_mpn_ws_Inquiry_Port";
		addressPayment 		= "http://" +ip+ ":" +port+ "/ws/com.btpn.mpn.ws.Payment/com_btpn_mpn_ws_Payment_Port";
		addressReInquiry 	= "http://" +ip+ ":" +port+ "/ws/com.btpn.mpn.ws.Reinquiry/com_btpn_mpn_ws_Reinquiry_Port";*/
		addressScheduler	= "http://" +ipInternal+ ":" +portInternal+ "/FileProcessing/services/SchedulerInterface";
//		addressRTGS 		= "http://" +ipInternal+ ":" +portInternal+ "/FileProcessing/services/RTGSInterface";
		
	}
	

	private SystemParameterDao getSystemParameterDAOBean(){
		ApplicationBean applicationBean = (ApplicationBean) FacesUtil.getManagedBean("applicationBean", null);
		return (SystemParameterDao) applicationBean.getCorpGWContext().getBean("systemParameterDAO");
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAddressInquiry() {
		return addressInquiry;
	}

	public void setAddressInquiry(String addressInquiry) {
		this.addressInquiry = addressInquiry;
	}

	public String getAddressPayment() {
		return addressPayment;
	}

	public void setAddressPayment(String addressPayment) {
		this.addressPayment = addressPayment;
	}

	public String getAddressReInquiry() {
		return addressReInquiry;
	}

	public void setAddressReInquiry(String addressReInquiry) {
		this.addressReInquiry = addressReInquiry;
	}

	public String getAddressScheduler() {
		return addressScheduler;
	}

	public void setAddressScheduler(String addressScheduler) {
		this.addressScheduler = addressScheduler;
	}

	public String getAddressRTGS() {
		return addressRTGS;
	}

	public void setAddressRTGS(String addressRTGS) {
		this.addressRTGS = addressRTGS;
	}
	
	
	
	
}
