/**
 * RTGSInterfaceServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.mii.ws.rtgs;

import com.mii.ws.WebServiceConfigurator;

public class RTGSInterfaceServiceLocator extends org.apache.axis.client.Service implements com.mii.ws.rtgs.RTGSInterfaceService {

	WebServiceConfigurator conf = new WebServiceConfigurator();
	
    public RTGSInterfaceServiceLocator() {
    }
    
    public RTGSInterfaceServiceLocator(WebServiceConfigurator conf) {
    	this.conf = conf;
    }


    public RTGSInterfaceServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public RTGSInterfaceServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for RTGSInterface
    private java.lang.String RTGSInterface_address = conf.getAddressRTGS();

    public java.lang.String getRTGSInterfaceAddress() {
        return RTGSInterface_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String RTGSInterfaceWSDDServiceName = "RTGSInterface";

    public java.lang.String getRTGSInterfaceWSDDServiceName() {
        return RTGSInterfaceWSDDServiceName;
    }

    public void setRTGSInterfaceWSDDServiceName(java.lang.String name) {
        RTGSInterfaceWSDDServiceName = name;
    }

    public com.mii.ws.rtgs.RTGSInterface getRTGSInterface() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
        	RTGSInterface_address = conf.getAddressRTGS();
            endpoint = new java.net.URL(RTGSInterface_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getRTGSInterface(endpoint);
    }

    public com.mii.ws.rtgs.RTGSInterface getRTGSInterface(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.mii.ws.rtgs.RTGSInterfaceSoapBindingStub _stub = new com.mii.ws.rtgs.RTGSInterfaceSoapBindingStub(portAddress, this);
            _stub.setPortName(getRTGSInterfaceWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setRTGSInterfaceEndpointAddress(java.lang.String address) {
        RTGSInterface_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.mii.ws.rtgs.RTGSInterface.class.isAssignableFrom(serviceEndpointInterface)) {
                com.mii.ws.rtgs.RTGSInterfaceSoapBindingStub _stub = new com.mii.ws.rtgs.RTGSInterfaceSoapBindingStub(new java.net.URL(RTGSInterface_address), this);
                _stub.setPortName(getRTGSInterfaceWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("RTGSInterface".equals(inputPortName)) {
            return getRTGSInterface();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://gateway.mii.com", "RTGSInterfaceService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://gateway.mii.com", "RTGSInterface"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("RTGSInterface".equals(portName)) {
            setRTGSInterfaceEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
