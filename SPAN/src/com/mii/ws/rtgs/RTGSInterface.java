/**
 * RTGSInterface.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.mii.ws.rtgs;

public interface RTGSInterface extends java.rmi.Remote {
    public java.lang.String execute(java.lang.String executeDate) throws java.rmi.RemoteException;
}
