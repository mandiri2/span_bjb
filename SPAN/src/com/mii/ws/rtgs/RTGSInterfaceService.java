/**
 * RTGSInterfaceService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.mii.ws.rtgs;

public interface RTGSInterfaceService extends javax.xml.rpc.Service {
    public java.lang.String getRTGSInterfaceAddress();

    public com.mii.ws.rtgs.RTGSInterface getRTGSInterface() throws javax.xml.rpc.ServiceException;

    public com.mii.ws.rtgs.RTGSInterface getRTGSInterface(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
