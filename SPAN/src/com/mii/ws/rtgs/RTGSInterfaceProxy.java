package com.mii.ws.rtgs;

public class RTGSInterfaceProxy implements com.mii.ws.rtgs.RTGSInterface {
  private String _endpoint = null;
  private com.mii.ws.rtgs.RTGSInterface rTGSInterface = null;
  
  public RTGSInterfaceProxy() {
    _initRTGSInterfaceProxy();
  }
  
  public RTGSInterfaceProxy(String endpoint) {
    _endpoint = endpoint;
    _initRTGSInterfaceProxy();
  }
  
  private void _initRTGSInterfaceProxy() {
    try {
      rTGSInterface = (new com.mii.ws.rtgs.RTGSInterfaceServiceLocator()).getRTGSInterface();
      if (rTGSInterface != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)rTGSInterface)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)rTGSInterface)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (rTGSInterface != null)
      ((javax.xml.rpc.Stub)rTGSInterface)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.mii.ws.rtgs.RTGSInterface getRTGSInterface() {
    if (rTGSInterface == null)
      _initRTGSInterfaceProxy();
    return rTGSInterface;
  }
  
  public java.lang.String execute(java.lang.String executeDate) throws java.rmi.RemoteException{
    if (rTGSInterface == null)
      _initRTGSInterfaceProxy();
    return rTGSInterface.execute(executeDate);
  }
  
  
}