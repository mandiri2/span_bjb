package com.mii.ws;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import com.mii.ws.rtgs.RTGSInterfaceServiceLocator;

public class WebServiceRtgsExecutor {
	public static String executeRTGS(String transactionDate) throws RemoteException, ServiceException{
		RTGSInterfaceServiceLocator rtgs = new RTGSInterfaceServiceLocator();
		String status = rtgs.getRTGSInterface().execute(transactionDate);
		return status;
	}
}
