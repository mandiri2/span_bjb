package com.mii.util.jasper;

import java.sql.Connection;
import java.util.HashMap;
import java.util.List;

public abstract class JasperExporter {
	private String filename;
	private String jasperPath;
	private HashMap parameter;
	
	public JasperExporter(String filename, String jasperPath, HashMap parameter){
		this.filename = filename;
		this.jasperPath = jasperPath;
		this.parameter = parameter;
	}
	
	/**
	 * FillReport Ke Jasper <br />
	 * Connection akan di close disini
	 * @param connection
	 * @return
	 */
	public abstract byte[] exportReportFromDb(Connection connection);
	public abstract byte[] ecportReportFromBean(List listData);
	
	/**GETTER SETTER**/
	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getJasperPath() {
		return jasperPath;
	}

	public void setJasperPath(String jasperPath) {
		this.jasperPath = jasperPath;
	}

	public HashMap getParameter() {
		return parameter;
	}

	public void setParameter(HashMap parameter) {
		this.parameter = parameter;
	}

}
