package com.mii.util.jasper;

import java.sql.Connection;
import java.util.HashMap;
import java.util.List;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

public class JasperExporterPdf extends JasperExporter {

	public JasperExporterPdf(String filename, String jasperPath, HashMap hashMap) {
		super(filename, jasperPath, hashMap);
	}

	@Override
	public byte[] exportReportFromDb(Connection connection) {
		try {
			return JasperRunManager.runReportToPdf(getJasperPath(), getParameter(), connection);
		} catch (JRException e) {
			e.printStackTrace();
		}finally{
			try {
				if(connection!=null)connection.close();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
			 
		}
		return null;
	}

	@Override
	public byte[] ecportReportFromBean(List listData) {
		JRDataSource dataSource=new JRBeanCollectionDataSource(listData);
		try {
			return JasperRunManager.runReportToPdf(getJasperPath(), getParameter(),dataSource);
		} catch (JRException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
}
