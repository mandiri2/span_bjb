package com.mii.util.jasper;

import java.util.HashMap;

public class JasperExporterFactory {
	
	/**
	 * Mengambil instance jasperExporter berdasarkan type
	 * @param type
	 * @param filename
	 * @param jasperPath
	 * @param parameter
	 * @return
	 */
	
	public static final String PDF = "PDF";
	public static final String EXCEL = "EXCEL";
	
	public static JasperExporter getJasperExporter(String type, String filename, String jasperPath, HashMap parameter){
		if(PDF.equalsIgnoreCase(type)){
			return new JasperExporterPdf(filename, jasperPath, parameter);
		}
		else if(EXCEL.equalsIgnoreCase(type)){
			return new JasperExporterExcel(filename, jasperPath, parameter);
		}
		return null;
	}
}
