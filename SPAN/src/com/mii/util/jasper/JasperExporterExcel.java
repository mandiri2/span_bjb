package com.mii.util.jasper;

import java.io.ByteArrayOutputStream;
import java.sql.Connection;
import java.util.HashMap;
import java.util.List;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JExcelApiExporterParameter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;

public class JasperExporterExcel extends JasperExporter {

	public JasperExporterExcel(String filename, String jasperPath,HashMap parameter) {
		super(filename, jasperPath, parameter);
	}

	@Override
	public byte[] exportReportFromDb(Connection connection){
		ByteArrayOutputStream baos = null;
		byte[] bytes = null;
		try {
			JasperPrint jasperPrint = JasperFillManager.fillReport(getJasperPath(), getParameter(), connection);
			
			JRXlsExporter exporter = new JRXlsExporter();
			baos = new ByteArrayOutputStream();
			
			exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			exporter.setParameter(JExcelApiExporterParameter.MAXIMUM_ROWS_PER_SHEET, 50000);
			exporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE,Boolean.TRUE);
			exporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND,Boolean.FALSE);
			exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS,Boolean.TRUE);
			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);
			exporter.exportReport(); 
			
			return bytes = baos.toByteArray();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				if(baos!=null)baos.close();
				if(connection!=null)connection.close();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
			
		}
		return bytes;
	}

	@Override
	public byte[] ecportReportFromBean(List listData) {
		ByteArrayOutputStream baos = null;
		byte[] bytes = null;
		JRDataSource dataSource = new JRBeanCollectionDataSource(listData);
		try {
			JasperPrint jasperPrint = JasperFillManager.fillReport(getJasperPath(), getParameter(), dataSource);
			return getBytes(jasperPrint);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				if(baos!=null)baos.close();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
			
		}
		return bytes;
	}
	
	
	private byte[] getBytes(JasperPrint jasperPrint){
		JRXlsExporter exporter = new JRXlsExporter();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
		exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
		exporter.setParameter(JExcelApiExporterParameter.MAXIMUM_ROWS_PER_SHEET, 50000);
		exporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE,Boolean.TRUE);
		exporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND,Boolean.FALSE);
		exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS,Boolean.TRUE);
		exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);
		try{
			exporter.exportReport(); 
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return baos.toByteArray();
	}
	
	

}
