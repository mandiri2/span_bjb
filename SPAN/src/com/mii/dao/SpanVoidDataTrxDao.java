package com.mii.dao;

import java.util.List;

import com.mii.helpers.BasicDao;
import com.mii.models.SpanSp2dVoidLog;
import com.mii.models.SpanVoidDataTrx;

public interface SpanVoidDataTrxDao extends BasicDao{
	public void InsertSPANDataVoidTransaction(SpanVoidDataTrx spanVoidDataTrx);
	public String selectSpanVoidDataTrxAmountByDocNumber(String documentNumber);
	public void deleteSPANVoidDataTransaction(String filename, String documentNumber);
	
	public int countVoid(String status, String filename, String sp2dNumber);
	
	public void updateDao(String sql);
	
	public void insertSpanSp2dVoidLogs(List<SpanSp2dVoidLog> spanSp2dVoidLogs);
	public List<SpanSp2dVoidLog> viewSpanSp2dVoidLogs(String filenameFilter, String sp2dNoFilter, int min, int max);
	public int countSpanSp2dVoidLogs(String filenameFilter, String sp2dNoFilter);
	public List<SpanVoidDataTrx> getAutoSendAckVoidListByDocumentList(
			List<String> documentNoList);
	
	List<String> getDocumentNoBySp2dNo(String sp2dNo);
}
