package com.mii.dao;

import java.util.List;

import com.mii.helpers.BasicDao;
import com.mii.models.ReportTransaksiM;


public interface ReportTransaksiDao extends BasicDao {
	public List<ReportTransaksiM> getReportTransaksi(String startDateS,String endDateS,String startDateBukuS,String endDateBukuS,String typePembayaran,String reversalStatus,String ntpn, String ntb,String kodeBilling,String status,String uidOpr,String kodeCabang,String namaCabang);
	public List<ReportTransaksiM> getReportTransaksiNtpn(String startDateS,String endDateS,String startDateBukuS,String endDateBukuS,String typePembayaran,String reversalStatus,String ntpn,String ntb,String kodeBilling,String status,String uidOpr,String kodeCabang,String namaCabang);
	public List<ReportTransaksiM> getReportTransaksiNoNtpn(String startDateS,String endDateS,String startDateBukuS,String endDateBukuS,String typePembayaran,String reversalStatus,String ntb,String kodeBilling,String status,String uidOpr,String kodeCabang,String namaCabang);
	public String getBranchByUserId(String userIdS); 
	public List<ReportTransaksiM> getSelectItem();
	public String getKantorPusat(String descKP);
	public String getCreateDate(String paymentCode);
	
}
