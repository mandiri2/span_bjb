package com.mii.dao;

import java.util.List;

import com.mii.helpers.BasicDao;
import com.mii.models.MonitoringManualUploadSp2d;

public interface SpanUploadSp2dDao extends BasicDao{
	public int countFileName(String fileName);
	public List<MonitoringManualUploadSp2d> getUploadFile(int maxRow, int minRow, String startDate, String endDate);
	public int countGetUploadFile(String startDate, String endDate);
	int countSpanValidationFileName(String fileName);
}
