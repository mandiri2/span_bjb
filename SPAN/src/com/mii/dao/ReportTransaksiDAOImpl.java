package com.mii.dao;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.transform.ResultTransformer;
import com.mii.helpers.BasicDaoImpl;
import com.mii.models.ReportTransaksiM;

public class ReportTransaksiDAOImpl extends BasicDaoImpl implements ReportTransaksiDao {
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ReportTransaksiM> getReportTransaksi(String startDateS,String endDateS, String startDateBukuS,String endDateBukuS, String typePembayaran,String reversalStatus,  String ntpn, String ntb,String kodeBilling,String status,String uidOpr,String kodeCabang,String namaCabang) {
		
		List<ReportTransaksiM> list = new ArrayList<ReportTransaksiM>();
		
		try {
			if(typePembayaran.equalsIgnoreCase("GIRAL")){
				if(reversalStatus.equalsIgnoreCase("Yes")){
					list = (List<ReportTransaksiM>) getCurrentSession().getNamedQuery("Report#getTransaksiGiralRev")
							.setString("startDate", startDateS)
							.setString("endDate", endDateS)
							.setString("startDateBuku", startDateBukuS)
							.setString("endDateBuku", endDateBukuS)
							.setString("ntpn", "%"+ntpn+"%")
							.setString("ntb", "%"+ntb+"%")
							.setString("kodeBilling", "%"+kodeBilling+"%")
							.setString("uidOpr", "%"+uidOpr+"%")
							.setString("kodeCabang", "%"+kodeCabang+"%")
							.setString("namaCabang", "%"+namaCabang+"%")
							.list();
				}else if(reversalStatus.equalsIgnoreCase("No")){
					list = (List<ReportTransaksiM>) getCurrentSession().getNamedQuery("Report#getTransaksiGiralNoRev")
							.setString("startDate", startDateS)
							.setString("endDate", endDateS)
							.setString("startDateBuku", startDateBukuS)
							.setString("endDateBuku", endDateBukuS)
							.setString("ntpn", "%"+ntpn+"%")
							.setString("ntb", "%"+ntb+"%")
							.setString("kodeBilling", "%"+kodeBilling+"%")
							.setString("uidOpr", "%"+uidOpr+"%")
							.setString("kodeCabang", "%"+kodeCabang+"%")
							.setString("namaCabang", "%"+namaCabang+"%")
							.list();
				}else{
					list = (List<ReportTransaksiM>) getCurrentSession().getNamedQuery("Report#getTransaksiGiral")
							.setString("startDate", startDateS)
							.setString("endDate", endDateS)
							.setString("startDateBuku", startDateBukuS)
							.setString("endDateBuku", endDateBukuS)
							.setString("ntpn", "%"+ntpn+"%")
							.setString("ntb", "%"+ntb+"%")
							.setString("kodeBilling", "%"+kodeBilling+"%")
							.setString("uidOpr", "%"+uidOpr+"%")
							.setString("kodeCabang", "%"+kodeCabang+"%")
							.setString("namaCabang", "%"+namaCabang+"%")
							.list();
				}
				
			}else if(typePembayaran.equalsIgnoreCase("TUNAI")){
				if(reversalStatus.equalsIgnoreCase("Yes")){
					list = (List<ReportTransaksiM>) getCurrentSession().getNamedQuery("Report#getTransaksiTunaiRev")
							.setString("startDate", startDateS)
							.setString("endDate", endDateS)
							.setString("startDateBuku", startDateBukuS)
							.setString("endDateBuku", endDateBukuS)
							.setString("ntpn", "%"+ntpn+"%")
							.setString("ntb", "%"+ntb+"%")
							.setString("kodeBilling", "%"+kodeBilling+"%")
							.setString("uidOpr", "%"+uidOpr+"%")
							.setString("kodeCabang", "%"+kodeCabang+"%")
							.setString("namaCabang", "%"+namaCabang+"%")
							.list();
				}else if(reversalStatus.equalsIgnoreCase("No")){
					list = (List<ReportTransaksiM>) getCurrentSession().getNamedQuery("Report#getTransaksiTunaiNoRev")
							.setString("startDate", startDateS)
							.setString("endDate", endDateS)
							.setString("startDateBuku", startDateBukuS)
							.setString("endDateBuku", endDateBukuS)
							.setString("ntpn", "%"+ntpn+"%")
							.setString("ntb", "%"+ntb+"%")
							.setString("kodeBilling", "%"+kodeBilling+"%")
							.setString("uidOpr", "%"+uidOpr+"%")
							.setString("kodeCabang", "%"+kodeCabang+"%")
							.setString("namaCabang", "%"+namaCabang+"%")
							.list();
				}else{
					list = (List<ReportTransaksiM>) getCurrentSession().getNamedQuery("Report#getTransaksiTunai")
							.setString("startDate", startDateS)
							.setString("endDate", endDateS)
							.setString("startDateBuku", startDateBukuS)
							.setString("endDateBuku", endDateBukuS)
							.setString("ntpn", "%"+ntpn+"%")
							.setString("ntb", "%"+ntb+"%")
							.setString("kodeBilling", "%"+kodeBilling+"%")
							.setString("uidOpr", "%"+uidOpr+"%")
							.setString("kodeCabang", "%"+kodeCabang+"%")
							.setString("namaCabang", "%"+namaCabang+"%")
							.list();
				}
				
			}else{
				if(reversalStatus.equalsIgnoreCase("Yes")){
					list = (List<ReportTransaksiM>) getCurrentSession().getNamedQuery("Report#getTransaksiRev")
							.setString("startDate", startDateS)
							.setString("endDate", endDateS)
							.setString("startDateBuku", startDateBukuS)
							.setString("endDateBuku", endDateBukuS)
							.setString("ntpn", "%"+ntpn+"%")
							.setString("ntb", "%"+ntb+"%")
							.setString("kodeBilling", "%"+kodeBilling+"%")
							.setString("uidOpr", "%"+uidOpr+"%")
							.setString("kodeCabang", "%"+kodeCabang+"%")
							.setString("namaCabang", "%"+namaCabang+"%")
							.list();
				}else if(reversalStatus.equalsIgnoreCase("No")){
					list = (List<ReportTransaksiM>) getCurrentSession().getNamedQuery("Report#getTransaksiNoRev")
							.setString("startDate", startDateS)
							.setString("endDate", endDateS)
							.setString("startDateBuku", startDateBukuS)
							.setString("endDateBuku", endDateBukuS)
							.setString("ntpn", "%"+ntpn+"%")
							.setString("ntb", "%"+ntb+"%")
							.setString("kodeBilling", "%"+kodeBilling+"%")
							.setString("uidOpr", "%"+uidOpr+"%")
							.setString("kodeCabang", "%"+kodeCabang+"%")
							.setString("namaCabang", "%"+namaCabang+"%")
							.list();
				}else{
					list = (List<ReportTransaksiM>) getCurrentSession().getNamedQuery("Report#getTransaksi")
							.setString("startDate", startDateS)
							.setString("endDate", endDateS)
							.setString("startDateBuku", startDateBukuS)
							.setString("endDateBuku", endDateBukuS)
							.setString("ntpn", "%"+ntpn+"%")
							.setString("ntb", "%"+ntb+"%")
							.setString("kodeBilling", "%"+kodeBilling+"%")
							.setString("uidOpr", "%"+uidOpr+"%")
							.setString("kodeCabang", "%"+kodeCabang+"%")
							.setString("namaCabang", "%"+namaCabang+"%")
							.list();
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ReportTransaksiM> getReportTransaksiNtpn(String startDateS,String endDateS, String startDateBukuS,String endDateBukuS, String typePembayaran,String reversalStatus, String ntpn,String ntb,String kodeBilling,String status,String uidOpr,String kodeCabang,String namaCabang) {
		
		List<ReportTransaksiM> list = new ArrayList<ReportTransaksiM>();
		
		try {
			if(typePembayaran.equalsIgnoreCase("GIRAL")){
				if(reversalStatus.equalsIgnoreCase("Yes")){
					list = (List<ReportTransaksiM>) getCurrentSession().getNamedQuery("Report#getTransaksiNtpnGiralRev")
							.setString("startDate", startDateS)
							.setString("endDate", endDateS)
							.setString("startDateBuku", startDateBukuS)
							.setString("endDateBuku", endDateBukuS)
							.setString("ntpn", "%"+ntpn+"%")
							.setString("ntb", "%"+ntb+"%")
							.setString("kodeBilling", "%"+kodeBilling+"%")
							.setString("uidOpr", "%"+uidOpr+"%")
							.setString("kodeCabang", "%"+kodeCabang+"%")
							.setString("namaCabang", "%"+namaCabang+"%")
							.list();
				}else if(reversalStatus.equalsIgnoreCase("No")){
					list = (List<ReportTransaksiM>) getCurrentSession().getNamedQuery("Report#getTransaksiNtpnGiralNoRev")
							.setString("startDate", startDateS)
							.setString("endDate", endDateS)
							.setString("startDateBuku", startDateBukuS)
							.setString("endDateBuku", endDateBukuS)
							.setString("ntpn", "%"+ntpn+"%")
							.setString("ntb", "%"+ntb+"%")
							.setString("kodeBilling", "%"+kodeBilling+"%")
							.setString("uidOpr", "%"+uidOpr+"%")
							.setString("kodeCabang", "%"+kodeCabang+"%")
							.setString("namaCabang", "%"+namaCabang+"%")
							.list();
				}else{
					list = (List<ReportTransaksiM>) getCurrentSession().getNamedQuery("Report#getTransaksiNtpnGiral")
							.setString("startDate", startDateS)
							.setString("endDate", endDateS)
							.setString("startDateBuku", startDateBukuS)
							.setString("endDateBuku", endDateBukuS)
							.setString("ntpn", "%"+ntpn+"%")
							.setString("ntb", "%"+ntb+"%")
							.setString("kodeBilling", "%"+kodeBilling+"%")
							.setString("uidOpr", "%"+uidOpr+"%")
							.setString("kodeCabang", "%"+kodeCabang+"%")
							.setString("namaCabang", "%"+namaCabang+"%")
							.list();
				}
				
			}else if(typePembayaran.equalsIgnoreCase("TUNAI")){
				if(reversalStatus.equalsIgnoreCase("Yes")){
					list = (List<ReportTransaksiM>) getCurrentSession().getNamedQuery("Report#getTransaksiNtpnTunaiRev")
							.setString("startDate", startDateS)
							.setString("endDate", endDateS)
							.setString("startDateBuku", startDateBukuS)
							.setString("endDateBuku", endDateBukuS)
							.setString("ntpn", "%"+ntpn+"%")
							.setString("ntb", "%"+ntb+"%")
							.setString("kodeBilling", "%"+kodeBilling+"%")
							.setString("uidOpr", "%"+uidOpr+"%")
							.setString("kodeCabang", "%"+kodeCabang+"%")
							.setString("namaCabang", "%"+namaCabang+"%")
							.list();
				}else if(reversalStatus.equalsIgnoreCase("No")){
					list = (List<ReportTransaksiM>) getCurrentSession().getNamedQuery("Report#getTransaksiNtpnTunaiNoRev")
							.setString("startDate", startDateS)
							.setString("endDate", endDateS)
							.setString("startDateBuku", startDateBukuS)
							.setString("endDateBuku", endDateBukuS)
							.setString("ntpn", "%"+ntpn+"%")
							.setString("ntb", "%"+ntb+"%")
							.setString("kodeBilling", "%"+kodeBilling+"%")
							.setString("uidOpr", "%"+uidOpr+"%")
							.setString("kodeCabang", "%"+kodeCabang+"%")
							.setString("namaCabang", "%"+namaCabang+"%")
							.list();
				}else{
					list = (List<ReportTransaksiM>) getCurrentSession().getNamedQuery("Report#getTransaksiNtpnTunai")
							.setString("startDate", startDateS)
							.setString("endDate", endDateS)
							.setString("startDateBuku", startDateBukuS)
							.setString("endDateBuku", endDateBukuS)
							.setString("ntpn", "%"+ntpn+"%")
							.setString("ntb", "%"+ntb+"%")
							.setString("kodeBilling", "%"+kodeBilling+"%")
							.setString("uidOpr", "%"+uidOpr+"%")
							.setString("kodeCabang", "%"+kodeCabang+"%")
							.setString("namaCabang", "%"+namaCabang+"%")
							.list();
				}
				
			}else{
				if(reversalStatus.equalsIgnoreCase("Yes")){
					list = (List<ReportTransaksiM>) getCurrentSession().getNamedQuery("Report#getTransaksiNtpnRev")
							.setString("startDate", startDateS)
							.setString("endDate", endDateS)
							.setString("startDateBuku", startDateBukuS)
							.setString("endDateBuku", endDateBukuS)
							.setString("ntpn", "%"+ntpn+"%")
							.setString("ntb", "%"+ntb+"%")
							.setString("kodeBilling", "%"+kodeBilling+"%")
							.setString("uidOpr", "%"+uidOpr+"%")
							.setString("kodeCabang", "%"+kodeCabang+"%")
							.setString("namaCabang", "%"+namaCabang+"%")
							.list();
				}else if(reversalStatus.equalsIgnoreCase("No")){
					list = (List<ReportTransaksiM>) getCurrentSession().getNamedQuery("Report#getTransaksiNtpnNoRev")
							.setString("startDate", startDateS)
							.setString("endDate", endDateS)
							.setString("startDateBuku", startDateBukuS)
							.setString("endDateBuku", endDateBukuS)
							.setString("ntpn", "%"+ntpn+"%")
							.setString("ntb", "%"+ntb+"%")
							.setString("kodeBilling", "%"+kodeBilling+"%")
							.setString("uidOpr", "%"+uidOpr+"%")
							.setString("kodeCabang", "%"+kodeCabang+"%")
							.setString("namaCabang", "%"+namaCabang+"%")
							.list();
				}else{
					list = (List<ReportTransaksiM>) getCurrentSession().getNamedQuery("Report#getTransaksiNtpn")
							.setString("startDate", startDateS)
							.setString("endDate", endDateS)
							.setString("startDateBuku", startDateBukuS)
							.setString("endDateBuku", endDateBukuS)
							.setString("ntpn", "%"+ntpn+"%")
							.setString("ntb", "%"+ntb+"%")
							.setString("kodeBilling", "%"+kodeBilling+"%")
							.setString("uidOpr", "%"+uidOpr+"%")
							.setString("kodeCabang", "%"+kodeCabang+"%")
							.setString("namaCabang", "%"+namaCabang+"%")
							.list();
				}
			
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ReportTransaksiM> getReportTransaksiNoNtpn(String startDateS,String endDateS, String startDateBukuS,String endDateBukuS, String typePembayaran,String reversalStatus, String ntb,String kodeBilling,String status,String uidOpr,String kodeCabang,String namaCabang) {
		
		List<ReportTransaksiM> list = new ArrayList<ReportTransaksiM>();
		
		try {
			if(typePembayaran.equalsIgnoreCase("GIRAL")){
				if(reversalStatus.equalsIgnoreCase("Yes")){
					list = (List<ReportTransaksiM>) getCurrentSession().getNamedQuery("Report#getTransaksiNoNtpnGiralRev")
							.setString("startDate", startDateS)
							.setString("endDate", endDateS)
							.setString("startDateBuku", startDateBukuS)
							.setString("endDateBuku", endDateBukuS)
							.setString("ntb", "%"+ntb+"%")
							.setString("kodeBilling", "%"+kodeBilling+"%")
							.setString("uidOpr", "%"+uidOpr+"%")
							.setString("kodeCabang", "%"+kodeCabang+"%")
							.setString("namaCabang", "%"+namaCabang+"%")
							.list();
				}else if(reversalStatus.equalsIgnoreCase("No")){
					list = (List<ReportTransaksiM>) getCurrentSession().getNamedQuery("Report#getTransaksiNoNtpnGiralNoRev")
							.setString("startDate", startDateS)
							.setString("endDate", endDateS)
							.setString("startDateBuku", startDateBukuS)
							.setString("endDateBuku", endDateBukuS)
							.setString("ntb", "%"+ntb+"%")
							.setString("kodeBilling", "%"+kodeBilling+"%")
							.setString("uidOpr", "%"+uidOpr+"%")
							.setString("kodeCabang", "%"+kodeCabang+"%")
							.setString("namaCabang", "%"+namaCabang+"%")
							.list();
				}else{
					list = (List<ReportTransaksiM>) getCurrentSession().getNamedQuery("Report#getTransaksiNoNtpnGiral")
							.setString("startDate", startDateS)
							.setString("endDate", endDateS)
							.setString("startDateBuku", startDateBukuS)
							.setString("endDateBuku", endDateBukuS)
							.setString("ntb", "%"+ntb+"%")
							.setString("kodeBilling", "%"+kodeBilling+"%")
							.setString("uidOpr", "%"+uidOpr+"%")
							.setString("kodeCabang", "%"+kodeCabang+"%")
							.setString("namaCabang", "%"+namaCabang+"%")
							.list();
				}
				
			}else if(typePembayaran.equalsIgnoreCase("TUNAI")){
				if(reversalStatus.equalsIgnoreCase("Yes")){
					list = (List<ReportTransaksiM>) getCurrentSession().getNamedQuery("Report#getTransaksiNoNtpnTunaiRev")
							.setString("startDate", startDateS)
							.setString("endDate", endDateS)
							.setString("startDateBuku", startDateBukuS)
							.setString("endDateBuku", endDateBukuS)
							.setString("ntb", "%"+ntb+"%")
							.setString("kodeBilling", "%"+kodeBilling+"%")
							.setString("uidOpr", "%"+uidOpr+"%")
							.setString("kodeCabang", "%"+kodeCabang+"%")
							.setString("namaCabang", "%"+namaCabang+"%")
							.list();
				}else if(reversalStatus.equalsIgnoreCase("No")){
					list = (List<ReportTransaksiM>) getCurrentSession().getNamedQuery("Report#getTransaksiNoNtpnTunaiNoRev")
							.setString("startDate", startDateS)
							.setString("endDate", endDateS)
							.setString("startDateBuku", startDateBukuS)
							.setString("endDateBuku", endDateBukuS)
							.setString("ntb", "%"+ntb+"%")
							.setString("kodeBilling", "%"+kodeBilling+"%")
							.setString("uidOpr", "%"+uidOpr+"%")
							.setString("kodeCabang", "%"+kodeCabang+"%")
							.setString("namaCabang", "%"+namaCabang+"%")
							.list();
				}else{
					list = (List<ReportTransaksiM>) getCurrentSession().getNamedQuery("Report#getTransaksiNoNtpnTunai")
							.setString("startDate", startDateS)
							.setString("endDate", endDateS)
							.setString("startDateBuku", startDateBukuS)
							.setString("endDateBuku", endDateBukuS)
							.setString("ntb", "%"+ntb+"%")
							.setString("kodeBilling", "%"+kodeBilling+"%")
							.setString("uidOpr", "%"+uidOpr+"%")
							.setString("kodeCabang", "%"+kodeCabang+"%")
							.setString("namaCabang", "%"+namaCabang+"%")
							.list();
				}
				
			}else{
				if(reversalStatus.equalsIgnoreCase("Yes")){
					list = (List<ReportTransaksiM>) getCurrentSession().getNamedQuery("Report#getTransaksiNoNtpnRev")
							.setString("startDate", startDateS)
							.setString("endDate", endDateS)
							.setString("startDateBuku", startDateBukuS)
							.setString("endDateBuku", endDateBukuS)
							.setString("ntb", "%"+ntb+"%")
							.setString("kodeBilling", "%"+kodeBilling+"%")
							.setString("uidOpr", "%"+uidOpr+"%")
							.setString("kodeCabang", "%"+kodeCabang+"%")
							.setString("namaCabang", "%"+namaCabang+"%")
							.list();
				}else if(reversalStatus.equalsIgnoreCase("No")){
					list = (List<ReportTransaksiM>) getCurrentSession().getNamedQuery("Report#getTransaksiNoNtpnNoRev")
							.setString("startDate", startDateS)
							.setString("endDate", endDateS)
							.setString("startDateBuku", startDateBukuS)
							.setString("endDateBuku", endDateBukuS)
							.setString("ntb", "%"+ntb+"%")
							.setString("kodeBilling", "%"+kodeBilling+"%")
							.setString("uidOpr", "%"+uidOpr+"%")
							.setString("kodeCabang", "%"+kodeCabang+"%")
							.setString("namaCabang", "%"+namaCabang+"%")
							.list();
				}else{
					list = (List<ReportTransaksiM>) getCurrentSession().getNamedQuery("Report#getTransaksiNoNtpn")
							.setString("startDate", startDateS)
							.setString("endDate", endDateS)
							.setString("startDateBuku", startDateBukuS)
							.setString("endDateBuku", endDateBukuS)
							.setString("ntb", "%"+ntb+"%")
							.setString("kodeBilling", "%"+kodeBilling+"%")
							.setString("uidOpr", "%"+uidOpr+"%")
							.setString("kodeCabang", "%"+kodeCabang+"%")
							.setString("namaCabang", "%"+namaCabang+"%")
							.list();
				}
			
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	
	@Override
	public String getBranchByUserId(String userIdS) {
		
		String branch = null;
		try{
			branch =  (String) getCurrentSession().getNamedQuery("Report#selectBranch")
					.setString("userid", userIdS)
					.uniqueResult();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return branch;
	}
	
			
	@SuppressWarnings("unchecked")
	@Override
	public List<ReportTransaksiM> getSelectItem() {
		List<ReportTransaksiM> list = new ArrayList<ReportTransaksiM>();
		try {
			list = (List<ReportTransaksiM>) getCurrentSession().getNamedQuery("Report#selectItem").
			setResultTransformer(new ResultTransformer() {						
				private static final long serialVersionUID = 1L;

				@Override
				public Object transformTuple(Object[] row, String[] arg1) {
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					ReportTransaksiM reportTransaksiM;
											
						reportTransaksiM = new ReportTransaksiM(String.valueOf(row[0]), 
								String.valueOf(row[1]));
						return reportTransaksiM;
					
				}
				
				@Override
				public List transformList(List arg0) {
					return arg0;
				}
			}).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	
	@Override
	public String getKantorPusat(String descKP) {
		
		String branch = null;
		try{
			branch =  (String) getCurrentSession().getNamedQuery("Report#getKantorPusat")
					.setString("kp", descKP)
					.uniqueResult();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return branch;
	}
	
	@Override
	public String getCreateDate(String paymentCode) {
		String createDate = null;
		try{
			createDate = (String) getCurrentSession().getNamedQuery("Report#getCreateDate")
					.setString("paymentCode", paymentCode).uniqueResult();
		}catch(Exception e){
			e.printStackTrace();
		}
		return createDate;
	}
}
