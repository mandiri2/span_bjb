package com.mii.dao;

import java.util.List;

import com.mii.helpers.BasicDaoImpl;
import com.mii.models.SpanHostResponse;

public class SpanHostResponseDaoImpl extends BasicDaoImpl implements SpanHostResponseDao {
	@Override
	public SpanHostResponse getResponseByHostDataDetailsRetryOnly(String batchId,
			String trxHeaderId, String trxDetailId) {
		SpanHostResponse result = (SpanHostResponse) getCurrentSession().getNamedQuery("SpanHostResponse#getResponseByHostDataDetailsRetryOnly")
				.setString("batchId", batchId)
				.setString("trxHeaderId", trxHeaderId)
				.setString("trxDetailId", trxDetailId)
				.uniqueResult();
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SpanHostResponse> getResponseByResponsecodeRetryOnly(String sp2dNo, String fileName) {
		 List<SpanHostResponse>  result = getCurrentSession().getNamedQuery("SpanHostResponse#getResponseByResponsecodeRetryOnly")
				.setString("sp2dNo", sp2dNo)
				.setString("fileName", fileName).list();
		return result;
	}

	@Override
	public void updateResponseToVOIDId(String trxHeaderId, String batchId,
			String trxDetailId) {
		getCurrentSession().getNamedQuery("SpanHostResponse#updateResponseToVOIDId").
			setString("oTrxHeaderId", trxHeaderId.concat("VOID")).
			setString("oBatchId", batchId.concat("VOID")).
			setString("oTrxDetailId", trxDetailId.concat("VOID")).
			setString("iTrxHeaderId", trxHeaderId).
			setString("iBatchId", batchId).
			setString("iTrxDetailId", trxDetailId).
			executeUpdate();
	}
}
