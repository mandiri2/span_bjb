package com.mii.dao;

import java.util.List;

import com.mii.helpers.BasicDao;
import com.mii.models.Role;
import com.mii.models.UserRole;

public interface RoleDao extends BasicDao {
	public List<Role> getAllRole();	
	public List<Role> getAllValidRole();
	public Role getAllValidRoleByRoleName(String roleName);
	public List<Role> getLazyRole(int maxRow, int minRow);
	public List<Role> getLazyRoleByCategory(int maxRow, int minRow, String roleId, String roleName, List<String> status);
	public int countAllRole();
	public int countAllRoleByCategory(String roleId, String roleName, List<String> status);
	public int countFilteredRole(String roleName);
	public List<Role> getFilteredRole(String roleName, int maxRow, int minRow);
	public Role getDetailedRole(Role role);
	public Role getDetailedRoleByRoleName(String roleName) ;
	public void saveOrUpdateRole(Role role);
	public void deleteRole(Role role);
	public List<Role> getRolesByIds(List<UserRole> userRoles);
	public Role getRoleByUIMRoleId(String uimRoleId);
}
