package com.mii.dao;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.ResultTransformer;

import com.mii.helpers.BasicDaoImpl;
import com.mii.models.InternalReportBJB;
import com.mii.models.SPANHostDataDetails;
import com.mii.models.SpanHostResponseCode;

public class SPANHostDataDetailsDaoImpl extends BasicDaoImpl implements SPANHostDataDetailsDao{

	@SuppressWarnings("unchecked")
	@Override
	public List<SPANHostDataDetails> getAll() {
		List<SPANHostDataDetails> details = new ArrayList<SPANHostDataDetails>();
		try{
			details = (List<SPANHostDataDetails>) getCurrentSession().
					getNamedQuery("SPANDetails#selectAllSPANHostDataDetails").list();
		}catch(Exception e){
			e.printStackTrace();
		}
		return details;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SPANHostDataDetails> getAllReturLazyLoading(int maxRow, int minRow, String debitAccount) {
		List<SPANHostDataDetails> details = new ArrayList<SPANHostDataDetails>();
		try{
			details = (List<SPANHostDataDetails>) getCurrentSession().
					getNamedQuery("SPANDetails#selectAllSPANHostDataDetailsReturLazyLoading").
					setString("MAX_ROW", String.valueOf(maxRow)).
					setString("MIN_ROW", String.valueOf(minRow)).
					setString("DEBIT_ACCOUNT",debitAccount).
					setResultTransformer(new ResultTransformer() {
						private static final long serialVersionUID = 1L;
						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							SPANHostDataDetails spanHostDataDetails;
							spanHostDataDetails = new SPANHostDataDetails(
									(BigDecimal) row[0], 
									String.valueOf(row[1]),
									String.valueOf(row[2]), 
									String.valueOf(row[3]), 
									String.valueOf(row[4]), 
									String.valueOf(row[5]),
									String.valueOf(row[6]),
									String.valueOf(row[7]),
									String.valueOf(row[8]),
									String.valueOf(row[9]),
									String.valueOf(row[10]),
									String.valueOf(row[11]),
									String.valueOf(row[12]));
							return spanHostDataDetails;
						}
						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).list();
		}catch(Exception e){
			e.printStackTrace();
		}
		return details;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SPANHostDataDetails> getAllRetryLazyLoading(int maxRow, int minRow, String debitAccount) {
		List<SPANHostDataDetails> details = new ArrayList<SPANHostDataDetails>();
		try{
			details = (List<SPANHostDataDetails>) getCurrentSession().
					getNamedQuery("SPANDetails#selectAllSPANHostDataDetailsRetryLazyLoading").
					setString("MAX_ROW", String.valueOf(maxRow)).
					setString("MIN_ROW", String.valueOf(minRow)).
					setString("DEBIT_ACCOUNT",debitAccount).
					setResultTransformer(new ResultTransformer() {
						private static final long serialVersionUID = 1L;
						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							SPANHostDataDetails spanHostDataDetails;
							spanHostDataDetails = new SPANHostDataDetails(
									(BigDecimal) row[0], 
									String.valueOf(row[1]),
									String.valueOf(row[2]), 
									String.valueOf(row[3]), 
									String.valueOf(row[4]), 
									String.valueOf(row[5]),
									String.valueOf(row[6]),
									String.valueOf(row[7]),
									String.valueOf(row[8]),
									String.valueOf(row[9]),
									String.valueOf(row[10]),
									String.valueOf(row[11]),
									String.valueOf(row[12]));
							return spanHostDataDetails;
						}
						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).list();
		}catch(Exception e){
			e.printStackTrace();
		}
		return details;
	}

	@Override
	public int countAllDetailsRetur(String debitAccount) {
		try{
			BigDecimal jml = (BigDecimal) getCurrentSession().getNamedQuery("SPANDetails#countAllSPANHostDataDetailsRetur").
					setString("DEBIT_ACCOUNT",debitAccount).uniqueResult();
			return jml.intValue();
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public int countAllDetailsRetry(String debitAccount) {
		try{
			BigDecimal jml = (BigDecimal) getCurrentSession().getNamedQuery("SPANDetails#countAllSPANHostDataDetailsRetry").
					setString("DEBIT_ACCOUNT",debitAccount).uniqueResult();
			return jml.intValue();
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	@SuppressWarnings("unchecked")
	@Override
	public SPANHostDataDetails getSearchDocumentNumber(String documentNumber) {
		SPANHostDataDetails spanHostDataDetails = new SPANHostDataDetails();
		List<SPANHostDataDetails> spanHostDataDetailsList = (List<SPANHostDataDetails>) getCurrentSession().getNamedQuery("SPANDetails#getSearchDocumentNumber").
				setParameter("documentNumber", documentNumber).
				setResultTransformer(new ResultTransformer() {
					private static final long serialVersionUID = 1L;
					@Override
					public Object transformTuple(Object[] row, String[] arg1) {
						SPANHostDataDetails spanHostDataDetails;
						spanHostDataDetails = new SPANHostDataDetails(
								String.valueOf(row[0]), 
								String.valueOf(row[1]),
								String.valueOf(row[2]), 
								String.valueOf(row[3]), 
								String.valueOf(row[4]), 
								String.valueOf(row[5]),
								String.valueOf(row[6]));
						return spanHostDataDetails;
					}
					@SuppressWarnings("rawtypes")
					@Override
					public List transformList(List arg0) {
						return arg0;
					}
				}).list();
			for(SPANHostDataDetails shdd : spanHostDataDetailsList){
				spanHostDataDetails = shdd;
			}
		return spanHostDataDetails;
	}

	@Override
	public void updateVoidSpanHostDataDetails(String documentNumber) {
		getCurrentSession().getNamedQuery("SPANDetails#updateVoidSpanHostDataDetails").
			setString("documentNo",documentNumber).executeUpdate();
	}
	
	@Override
	public void updateBulkVoidSpanHostDataDetails(String sp2dNumber, String batchId, String trxHeaderId) {
		getCurrentSession().getNamedQuery("SPANDetails#updateBulkVoidSpanHostDataDetails").
			setString("documentNo",sp2dNumber+"%").
			setString("batchId", batchId).
			setString("trxHeaderId", trxHeaderId).
			executeUpdate();
	}
	
	public static List<String> generatePaymentMethod(String paymentMethod){
		List<String> paymentList = new ArrayList<>();
		if (paymentMethod.equals("3") || paymentMethod.equals("")) {
			for (int i = 0; i < 3; i++) {
				paymentList.add(i+"");
			}
		}
		else if (paymentMethod.equals("0")){
			paymentList.add("0");
		}
		else if (paymentMethod.equals("1")){
			paymentList.add("1");
		}
		else if (paymentMethod.equals("2")){
			paymentList.add("2");
		}
		return paymentList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<InternalReportBJB> getTransactionReportLazyLoading(int maxRow, int minRow,
			Date startDocDate, Date endDocDate, String accountCodeNumber, String paymentMethod) {
		List<InternalReportBJB> details = new ArrayList<InternalReportBJB>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		List<String> paymentList = generatePaymentMethod(paymentMethod);
		try{
			details = (List<InternalReportBJB>) getCurrentSession().
					getNamedQuery("SPANDetails#getTransactionReportLazyLoading").
					setBigInteger("MAX_ROW", new BigInteger(String.valueOf(maxRow))).
					setBigInteger("MIN_ROW", new BigInteger(String.valueOf(minRow))).
					setString("startDate", sdf.format(startDocDate)).
					setString("endDate", sdf.format(endDocDate)).
					setString("accno", "%"+accountCodeNumber+"%").
					setParameterList("paymentMethod", paymentList).
					// TODO add filter by trxstatus
					setResultTransformer(new ResultTransformer() {
						private static final long serialVersionUID = 1L;
						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							InternalReportBJB internalReportBJB;
							internalReportBJB = new InternalReportBJB(
									String.valueOf(row[0]),
									String.valueOf(row[1]),
									String.valueOf(row[2]),
									String.valueOf(row[3]),
									String.valueOf(row[4]),
									String.valueOf(row[5]),
									String.valueOf(row[6]),
									String.valueOf(row[7]),
									String.valueOf(row[8]));
							return internalReportBJB;
						}
						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).list();
		}catch(Exception e){
			e.printStackTrace();
		}
		return details;
	}

	@Override
	public int getRowCountTransactionReport(Date startDate, Date endDate, String accountCodeNumber, String paymentMethod) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		List<String> paymentList = generatePaymentMethod(paymentMethod);
		try {
			BigInteger jml = (BigInteger) getCurrentSession()
					.getNamedQuery("SPANDetails#getRowCountTransactionReport")
					.setString("startDate", sdf.format(startDate))
					.setString("endDate", sdf.format(endDate))
					.setString("accno", "%"+accountCodeNumber+"%")
					.setParameterList("paymentMethod", paymentList)
					.uniqueResult();
			return jml.intValue();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public List<SpanHostResponseCode> getTransactionStatus() {
		List<SpanHostResponseCode> list = getCurrentSession().createCriteria(SpanHostResponseCode.class)
				.add(Restrictions.or(Restrictions.eq("RC_ID", "SPAN02"), 
						Restrictions.eq("RC_ID", "SPAN03")))
				.list();
		return list;
	}

	@Override
	public List<String> getFileNameByDocumentNumber(String documentNumber) {
		List<String> list = getCurrentSession().getNamedQuery("SPANDetails#getSearchFileNameByDocumentNumber")
				.setString("documentNumber","%"+documentNumber+"%").list();
		return list;
	}

	@Override
	public SPANHostDataDetails getSPANDetailsByKey(String trxHeaderId,
			String batchId, String trxDetailId) {
		SPANHostDataDetails result = (SPANHostDataDetails) getCurrentSession().getNamedQuery("SPANDetails#getSPANDetailsByKey").
										setString("trxHeaderId", trxHeaderId).
										setString("batchId", batchId).
										setString("trxDetailId", trxDetailId).uniqueResult();
		return result;
	}
}
