package com.mii.dao;

import java.util.List;

import com.mii.helpers.BasicDao;
import com.mii.models.SpanDepkeuReport;

public interface SpanDepkeuReportDao extends BasicDao {
	public List<SpanDepkeuReport> getSp2dReject(String docDateStart, String docDateEnd,int minRow,int maxRow);
	public List<SpanDepkeuReport> getSp2dRejectApprovedByKemenkeu(String docDateStart, String docDateEnd,int minRow,int maxRow);
	public int countSp2dReject(String docDateStart, String docDateEnd);
	public int countSp2dRejectApprovedByKemenkeu(String docDateStart, String docDateEnd);
}
