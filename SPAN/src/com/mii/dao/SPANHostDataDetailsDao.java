package com.mii.dao;

import java.util.Date;
import java.util.List;

import com.mii.helpers.BasicDao;
import com.mii.models.InternalReportBJB;
import com.mii.models.SPANHostDataDetails;
import com.mii.models.SpanHostResponseCode;

public interface SPANHostDataDetailsDao  extends BasicDao{
	public List<SPANHostDataDetails> getAll();
	public List<SPANHostDataDetails> getAllReturLazyLoading(int maxRow, int minRow, String debitAccount);
	public List<SPANHostDataDetails> getAllRetryLazyLoading(int maxRow, int minRow, String debitAccount);
	public int countAllDetailsRetur(String debitAccount);
	public int countAllDetailsRetry(String debitAccount);
	public SPANHostDataDetails getSearchDocumentNumber(String documentNumber);
	public void updateVoidSpanHostDataDetails (String documentNumber);
	public void updateBulkVoidSpanHostDataDetails (String sp2dNumber, String batchId, String trxHeaderId);
	public List<InternalReportBJB> getTransactionReportLazyLoading(int maxRow, int minRow, Date startDocDate, Date endDocDate, String accountCodeNumber, String paymentMethod);
	public int getRowCountTransactionReport(Date start, Date end, String accountCodeNumber, String paymentMethod);
	public List<SpanHostResponseCode> getTransactionStatus();
	public List<String> getFileNameByDocumentNumber(String documentNumber);
	public SPANHostDataDetails getSPANDetailsByKey(String trxHeaderId, String batchId, String trxDetailId);
}
