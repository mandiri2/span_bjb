package com.mii.dao;

import java.util.List;

import com.mii.models.SorborAttr;

public interface SorborAttrDao {
	public SorborAttr getSorborAttrById(String docNo);
	public List<SorborAttr> getListSorborAttr(List<String> docNo);
	public int countListSorborAttr(List<String> docNo);
	public int getNextValFileSeq();
	public void setValFileSeq();
}
