package com.mii.dao;

import java.util.List;

import com.mii.helpers.BasicDao;
import com.mii.models.Menu;
import com.mii.models.RoleMenu;

public interface MenuDao extends BasicDao {
	public List<Menu> getAllMenu();
	public List<RoleMenu> getAllRoleMenus(List<String> roleIds);
	public int getMenuAuthority(String userId, String menuPath);
}
