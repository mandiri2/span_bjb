package com.mii.dao;

import com.mii.helpers.BasicDao;
import com.mii.models.FastBJBModel;

public interface FASTBJBDao extends BasicDao{
	public int validateUserAccountBJB(String accountName, String accountNo);
	public FastBJBModel checkBlockedUserAccountBJB(String accountName, String accountNo);
}
