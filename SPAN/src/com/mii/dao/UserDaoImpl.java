package com.mii.dao;

import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.transform.ResultTransformer;

import com.mii.constant.Constants;
import com.mii.helpers.BasicDaoImpl;
import com.mii.models.User;
import com.mii.models.UserRole;
import com.mii.models.UserUpload;

public class UserDaoImpl extends BasicDaoImpl implements UserDao{


	@Override
	public void saveUserToDB(User user) {	
		save(user);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> getAllUser() {
		List<User> users = new ArrayList<User>();
		try{
			users = getCurrentSession().getNamedQuery("MUser#getAllUser").list();
		}catch(Exception e){
			e.printStackTrace();
		}
		return users;
	}

	@Override
	public void editUser(User user) {
		try {
			save(user);
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}

	@Override
	public User deleteUser(String userID) {
		User user = new User();
		try {
			user.setUserID(userID);
			delete(User.class, userID);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<User> getAllUserLazyLoading(int maxRow, int minRow) {		
		List<User> users = new ArrayList<User>();
		try{
			users = (List<User>)getCurrentSession().
					getNamedQuery("MUser#getAllUserLazyLoading").
					setBigInteger("MAX_ROW", BigInteger.valueOf(maxRow)).
					setBigInteger("MIN_ROW", BigInteger.valueOf(minRow)).
					setResultTransformer(new ResultTransformer() {						
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
							User user;
							try {
								Date lastLogin = null;								
								if (row[4]!=null) {
									lastLogin = sdf.parse(String.valueOf(row[4]));
								}								
								user = new User(String.valueOf(row[0]), 
										String.valueOf(row[1]), 
										String.valueOf(row[2]), 
										String.valueOf(row[3]), 
										lastLogin, 
										String.valueOf(row[5]),
										String.valueOf(row[6]),
										String.valueOf(row[7]));
								return user;
							} catch (ParseException e) {
								e.printStackTrace();
							}
							return null;
						}
						
						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).
					list();
		}catch(Exception e){
			e.printStackTrace();
		}
		return users;
	}

	@Override
	public int countAllUser() {
		try{
			BigInteger jml = (BigInteger) getCurrentSession().
					getNamedQuery("MUser#countAllRecord").uniqueResult();			
			return jml.intValue();
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public User getDetailedUser(String user) {
		User detailUser = new User();
		try{
			detailUser = (User) getCurrentSession().getNamedQuery("MUser#getUserByExample")
					.setString("userID", user)					
					.uniqueResult();
		}catch(Exception e){
			e.printStackTrace();
		}
		return detailUser;
	}
	
	@Override
	public User getByUserName(String userName) {
		User detailUser = new User();
		try{
			detailUser = (User) getCurrentSession().getNamedQuery("MUser#getUserByUsername")
					.setString("userName", userName)					
					.uniqueResult();
		}catch(Exception e){
			e.printStackTrace();
		}
		return detailUser;
	}
	
	@Override
	public int countUserUploadByUserName(String userName) {
		try{
			BigInteger jml = (BigInteger) getCurrentSession().
					getNamedQuery("TempUser#countUserUploadByUserName").
					setString("userName",userName).
					uniqueResult();			
			return jml.intValue();
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}
	
	@Override
	public UserRole getUserRoleByUsernameAndRole(String username, String roleName) {
		UserRole userRole = new UserRole();
		try{
			userRole = (UserRole) getCurrentSession().getNamedQuery("MUser#getUserRoleByUsernameAndRole")
					.setString("USERNAME", username)		
					.setString("ROLE", roleName)	
					.uniqueResult();
		}catch(Exception e){
			e.printStackTrace();
		}
		return userRole;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> filterUsers(String user, int maxPerPage, int startingAt) {
		List<User> users = new ArrayList<User>();
		try{
			users = (List<User>)getCurrentSession().
					getNamedQuery("MUser#filterUser").
					setString("userID", "%"+user+"%").
					setBigInteger("MAX_ROW", BigInteger.valueOf(maxPerPage)).
					setBigInteger("MIN_ROW", BigInteger.valueOf(startingAt)).
					setResultTransformer(new ResultTransformer() {
						
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
							User user;
							try {
								Date lastLogin = null;								
								if (row[4]!=null) {
									lastLogin = sdf.parse(String.valueOf(row[4]));
								}								
								user = new User(String.valueOf(row[0]), 
										String.valueOf(row[1]),  
										String.valueOf(row[2]), 
										String.valueOf(row[3]), 
										lastLogin, 
										String.valueOf(row[5]),
										String.valueOf(row[6]),
										String.valueOf(row[7]));
								return user;
							} catch (ParseException e) {
								e.printStackTrace();
							}
							return null;
						}
						
						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).
					list();
		}catch(Exception e){
			e.printStackTrace();
		}
		return users;
	}

	@Override
	public int countFilteredUser(String user) {
		try{
			BigInteger jml = (BigInteger) getCurrentSession().
					getNamedQuery("MUser#countFilteredUser").
					setString("userID", "%"+user+"%").
					uniqueResult();			
			return jml.intValue();
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}
	
	@Override
	public UserRole deleteUserRole(String roleId) {
		UserRole userRole = new UserRole();
		userRole.setRole(roleId);
		try {
			getCurrentSession().getNamedQuery("MUser#deleteUserRole")
				.setString("roleId", userRole.getRole()).executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return userRole;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> getUserLazyLoadingByCategory(int maxRow, int minRow,
			String userId, String userName, String fullName, String branch, String roleId, List<String> status) {
		List<User> users = new ArrayList<User>();
		try{
			users = (List<User>)getCurrentSession().
					getNamedQuery("MUser#getUserLazyLoadingByCategory").
					setString("USER_ID", "%"+userId+"%").
					setString("USER_NAME", "%"+userName+"%").
					setString("FULL_NAME", "%"+fullName+"%").
					setString("BRANCH", "%"+branch+"%").
					setString("ROLE_ID", "%"+roleId+"%").
					setParameterList("STATUS", status).
					setBigInteger("MAX_ROW", BigInteger.valueOf(maxRow)).
					setBigInteger("MIN_ROW", BigInteger.valueOf(minRow)).
					setResultTransformer(new ResultTransformer() {
						
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
							User user;
							try {
								Date lastLogin = null;								
								if (row[4]!=null) {
									lastLogin = sdf.parse(String.valueOf(row[4]));
								}								
								user = new User(String.valueOf(row[0]), 
										String.valueOf(row[1]), 
										String.valueOf(row[2]), 
										String.valueOf(row[3]), 
										lastLogin, 
										String.valueOf(row[5]),
										String.valueOf(row[6]),
										String.valueOf(row[7]));
								return user;
							} catch (ParseException e) {
								e.printStackTrace();
							}
							return null;
						}
						
						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).
					list();
		}catch(Exception e){
			e.printStackTrace();
		}
		return users;
	}

	@Override
	public int countUserByCategory(String userId, String userName, String fullName, String branch, String roleId,
			List<String> status) {
		try{
			BigInteger jml = (BigInteger) getCurrentSession().
					getNamedQuery("MUser#countRecordByCategory").
					setString("USER_ID", "%"+userId+"%").
					setString("USER_NAME", "%"+userName+"%").
					setString("FULL_NAME", "%"+fullName+"%").
					setString("BRANCH", "%"+branch+"%").
					setString("ROLE_ID", "%"+roleId+"%").
					setParameterList("STATUS", status).
					uniqueResult();			
			return jml.intValue();
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}
	

	@Override
	public void insertUserUpload(String USERID, String EMAIL, String PHONE,
			String USERNAME, String FULLNAME, String DIVISION,
			String DEPARTMENT,
			String CREATEWHO, String CREATEDATE, String BRANCH) {
		try{
			getCurrentSession().
					getNamedQuery("TempUser#InsertUserUpload").
					setString("USERID", USERID).
					setString("EMAIL", EMAIL).
					setString("PHONE", PHONE).
					setString("USERNAME", USERNAME).
					setString("FULLNAME", FULLNAME).
					setString("DIVISION", DIVISION).
					setString("DEPARTMENT", DEPARTMENT).
					setString("CREATEWHO", CREATEWHO).
					setString("CREATEDATE", CREATEDATE).
					setString("BRANCH", BRANCH).
					executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> getAllUserUploadLazyLoading(int maxRow,
			int minRow, String userID) {
		List<User> users = new ArrayList<User>();
		try{
			users = (List<User>)getCurrentSession().
					getNamedQuery("TempUser#SelectUserUploadLazyLoading").
					setBigInteger("MAX_ROW", BigInteger.valueOf(maxRow)).
					setBigInteger("MIN_ROW", BigInteger.valueOf(minRow)).
					setString("USERIDUPLOAD", String.valueOf(userID)).
					setResultTransformer(new ResultTransformer() {						
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							User user = new User(
									String.valueOf(row[0]), 
									String.valueOf(row[1]), 
									String.valueOf(row[2]), 
									String.valueOf(row[3]),
									String.valueOf(row[4]),
									String.valueOf(row[5]),
									String.valueOf(row[6]), 
									String.valueOf(row[7]), 
									String.valueOf(row[8]),
									String.valueOf(row[9]), 
									String.valueOf(row[10]), 
									String.valueOf(row[11]), 
									String.valueOf(row[12]), 
									String.valueOf(row[13])
									);
							user.setRoleNameTemp(String.valueOf(row[14]));
							if (user.getEmail().equals("null")) user.setEmail(null);
							if (user.getPhone().equals("null")) user.setPhone(null);
							if (user.getMobileNumber().equals("null")) user.setMobileNumber(null);
							if (user.getContactNumber().equals("null")) user.setContactNumber(null);
							if (user.getDivision().equals("null")) user.setDivision(null);
							if (user.getDepartment().equals("null")) user.setDepartment(null);
							if (user.getTellerId().equals("null")) user.setTellerId(null);
							if (user.getDebitLimitTransaction().equals("null")) user.setDebitLimitTransaction(null);
							if (user.getRoleNameTemp().equals("null")) user.setRoleNameTemp(null);
							
							return user;
						}
						
						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).
					list();
		}catch(Exception e){
			e.printStackTrace();
		}
		return users;
	}

	@Override
	public int countAllUserUpload(String userID) {
		try{
			BigInteger jml = (BigInteger) getCurrentSession().
					getNamedQuery("TempUser#countAllUserUpload").
					setString("USERIDUPLOAD",userID).
					uniqueResult();			
			return jml.intValue();
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public void deleteAllUserUpload(String userID) {
		try{
			getCurrentSession().
			getNamedQuery("TempUser#deleteAllUserUpload").
			setString("USERIDUPLOAD",userID).executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Override
	public User getUserByIdBranch(String id, String branch) {
		User user = new User();

		List<String> listStatus = new ArrayList<String>();
						
		listStatus.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
		listStatus.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
		
		try{
			user = (User) getCurrentSession().getNamedQuery("MUser#getUserByUserIdAndBranchCode")
					.setString("userName", id)
					.setString("branchCode", branch)
					.setParameterList("status", listStatus)
					.uniqueResult();
		}catch(Exception e){
			e.printStackTrace();
		}
		return user;
	}
	
	@Override
	public User getValidUserByUsername(String username) {
		User user = new User();

		List<String> listStatus = new ArrayList<String>();
						
		listStatus.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
		listStatus.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
		
		try{
			user = (User) getCurrentSession().getNamedQuery("MUser#getValidUserByUsername")
					.setString("userName", username)
					.setParameterList("status", listStatus)
					.uniqueResult();
		}catch(Exception e){
			e.printStackTrace();
		}
		return user;
	}

	@Override
	public void deleteUserRoleByUsername(String username) {
		try{
			getCurrentSession().getNamedQuery("MUser#deleteUserRoleByUserId")
					.setString("username", username).executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Override
	public void updateUserBranchByBranchCode(String oldBranchCode, String newBranchCode) {
		try{
			getCurrentSession().getNamedQuery("User#updateUserBranchByBranchCode")
					.setString("OLD_BRANCH_CODE", oldBranchCode)
					.setString("NEW_BRANCH_CODE", newBranchCode)
					.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Override
	public void saveUserUploadToDB(UserUpload user) {
		save(user);
	}

	@Override
	public void insertUserRole(String userId, String roleId) {
		try{
			getCurrentSession().getNamedQuery("User#insertUserRole")
					.setString("userId", userId)
					.setString("roleId", roleId)
					.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Override
	public void updateUserRole(String userId, String roleId) {
		try{
			getCurrentSession().getNamedQuery("User#updateUserRole")
					.setString("userId", userId)
					.setString("roleId", roleId)
					.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}
	}

}
