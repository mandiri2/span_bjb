package com.mii.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.transform.ResultTransformer;

import com.mii.helpers.BasicDaoImpl;
import com.mii.models.Holiday;
import com.mii.models.Rtgs;
import com.mii.models.User;

public class HolidayDaoImpl extends BasicDaoImpl implements HolidayDao {

	@Override
	public void saveHoliday(Holiday holiday) {
		// TODO Auto-generated method stub
		try {
			save(holiday);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<Holiday> getHolidayLazyLoadingByCategory(int maxPerPage,
			int startingAt, String holidayId, String holidayName,
			String holidayDate, List<String> status) {
		// TODO Auto-generated method stub
		List<Holiday> listHoliday = new ArrayList<Holiday>();
		try {
			listHoliday = (List<Holiday>) getCurrentSession()
					.getNamedQuery("THoliday#getHolidayLazyLoadingByCategory")
					.setString("holidayId", "%" + holidayId + "%")
					.setString("holidayName", "%" + holidayName + "%")
					.setString("holidayDate", "%" + holidayDate + "%")
					.setParameterList("status", status)
					.setString("MAX_ROW", String.valueOf(maxPerPage))
					.setString("MIN_ROW", String.valueOf(startingAt))
					.setResultTransformer(new ResultTransformer() {

						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							Holiday holiday;
							holiday = new Holiday(String.valueOf(row[0])
										,String.valueOf(row[1])
										,String.valueOf(row[2])
										,String.valueOf(row[3])
										,String.valueOf(row[4])
									  );
							return holiday;
						}

						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listHoliday;
	}

	@Override
	public Integer countHolidayByCategory(String holidayId, String holidayName,
			String holidayDate, List<String> status) {
		// TODO Auto-generated method stub
		try {
			BigDecimal jml = (BigDecimal) getCurrentSession()
					.getNamedQuery("THoliday#countRecordByCategory")
						.setString("holidayId", "%" + holidayId + "%")
					.setString("holidayName", "%" + holidayName + "%")
					.setString("holidayDate", "%" + holidayDate + "%")
					.setParameterList("status", status)
					.uniqueResult();
			return jml.intValue();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public Holiday getDetailedHoliday(String holidayId) {
		// TODO Auto-generated method stub
		Holiday detailHoliday = new Holiday();
		try{
			detailHoliday = (Holiday) getCurrentSession().getNamedQuery("THoliday#getHolidayByExample")
					.setString("holidayId", holidayId)					
					.uniqueResult();
		}catch(Exception e){
			e.printStackTrace();
		}
		return detailHoliday;
	}

	@Override
	public Holiday getDetailedHolidayByHolidayName(String holidayName) {
		// TODO Auto-generated method stub
		Holiday detailHoliday = new Holiday();
		try{
			detailHoliday = (Holiday) getCurrentSession().getNamedQuery("THoliday#getHolidayByName")
					.setString("holidayName", holidayName)					
					.uniqueResult();
		}catch(Exception e){
			e.printStackTrace();
		}
		return detailHoliday;
	}


	@Override
	public Holiday deleteHoliday(String holidayId) {
		// TODO Auto-generated method stub
		Holiday holiday = new Holiday();
		try {
			holiday.setHolidayId(holidayId);
			delete(Holiday.class, holidayId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
