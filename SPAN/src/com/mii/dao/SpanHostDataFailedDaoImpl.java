package com.mii.dao;

import com.mii.helpers.BasicDaoImpl;
import com.mii.models.SpanHostDataFailed;

public class SpanHostDataFailedDaoImpl extends BasicDaoImpl implements SpanHostDataFailedDao {

	@Override
	public void saveSpanHostDataFailed(SpanHostDataFailed spanHostDataFailed) {
		setDefaultValue(spanHostDataFailed);
		save(spanHostDataFailed);
	}

	private void setDefaultValue(SpanHostDataFailed spanHostDataFailed) {
		
		if(spanHostDataFailed!=null){
			if(spanHostDataFailed.getRetryStatus()==null){
				spanHostDataFailed.setRetryStatus(0l);
			}
			
			if(spanHostDataFailed.getReturStatus()==null){
				spanHostDataFailed.setRetryStatus(0l);
			}
			
			if(spanHostDataFailed.getSuccessStatus()==null){
				spanHostDataFailed.setSuccessStatus(0l);
			}
			
			if(spanHostDataFailed.getTotalProcess()==null){
				spanHostDataFailed.setTotalProcess(0l);
			}
			
			if(spanHostDataFailed.getWaitStatus()==null){
				spanHostDataFailed.setWaitStatus(0l);
			}
		}
		
	}

	@Override
	public SpanHostDataFailed getSpanHostDataFailed(String filename) {
		SpanHostDataFailed result = (SpanHostDataFailed) getCurrentSession()
				.getNamedQuery("SpanHostDataFailed#getSpanHostDataFailed")
				.setString("fileName", filename)
				.setMaxResults(1)
				.uniqueResult();
		return result;
	}

}
