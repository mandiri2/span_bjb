package com.mii.dao;

import com.mii.helpers.BasicDao;
import com.mii.models.SpanHostDataFailed;

public interface SpanHostDataFailedDao extends BasicDao {
	void saveSpanHostDataFailed(SpanHostDataFailed spanHostDataFailed);
	public SpanHostDataFailed getSpanHostDataFailed(String filename);
}
