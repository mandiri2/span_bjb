package com.mii.dao;

import java.util.List;

import com.mii.helpers.BasicDao;
import com.mii.models.PaymentM;

public interface PaymentDao extends BasicDao{
	public List<PaymentM> getLogByPaymentcode(String paymentCode);
}
