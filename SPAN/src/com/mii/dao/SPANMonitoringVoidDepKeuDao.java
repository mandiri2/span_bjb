package com.mii.dao;

import java.util.List;

import com.mii.helpers.BasicDao;
import com.mii.models.SPANVoidDocumentNumber;
import com.mii.models.SpanMonitoringVoidDepKeu;


public interface SPANMonitoringVoidDepKeuDao  extends BasicDao{
	public List<SpanMonitoringVoidDepKeu> getLazySpanDataValidationSp2dNo(String fileName, String docDate, String voidFlag, String debitAccountType, 
			String sp2dNumber,  int startRow, int endRow);
	public int countSpanDataValidationSp2dNo (String fileName, String docDate, String voidFlag, String debitAccountType, String sp2dNumber);
	public int countSpanVoidDataTrx (String status, String fileName, String sp2dNumber);
	public int deleteSpanVoidDataTrx(String status, String fileName, String sp2dNumber);
	public int countVoid(String status, String fileName, String sp2dNumber);
	public int sumAmountVoid(String status, String fileName, String sp2dNumber);
	public int countSearchDocumentNumber(String documentNumber, String status);
	public List<SPANVoidDocumentNumber> getVoidDataTransactionListByFilenameSp2d(String status, String fileName, String sp2dNumber);	int countSpanDataValidationBackdate(String filename);

	List<SPANVoidDocumentNumber> getVoidDataTransactionListByFilenameSp2dNoHost(
			String fileName, String sp2dNumber);
	int countSpanVoidDataTrxHist(String fileName, String sp2dNumber);
	List<SpanMonitoringVoidDepKeu> getLazyHistorySp2dNo(String fileName,
			String docDate, String debitAccountType, String sp2dNumber,
			int startRow, int endRow);
	int countHistorySp2dNo(String fileName, String docDate,
			String debitAccountType, String sp2dNumber);
	List<SpanMonitoringVoidDepKeu> getLazySpanDataValidationBackdate(String filename, String sp2d, int startRow,
			int endRow); 
}
