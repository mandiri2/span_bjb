package com.mii.dao;

import java.util.ArrayList;
import java.util.List;

import com.mii.helpers.BasicDaoImpl;
import com.mii.models.InputNoSaktiM;
import com.mii.models.Rtgs;

public class InputNoSaktiDAOImpl extends BasicDaoImpl implements InputNoSaktiDao {
	
	@SuppressWarnings("unchecked")
	@Override
	public List<InputNoSaktiM> getFilterTrx(String startDateS,String tahunDateS,String ntb) {
		List<InputNoSaktiM> list = new ArrayList<InputNoSaktiM>();
		try {
			list = (List<InputNoSaktiM>) getCurrentSession().getNamedQuery("Report#getFilterTrx")
					.setString("startDate", startDateS)
//					.setString("endDate", endDateS)
					.setString("tahunDate", tahunDateS)
					.setString("ntb", "%"+ntb+"%")
//					.setString("currency", currency)
					.list();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}


	@Override
	public InputNoSaktiM updateNoSakti(InputNoSaktiM inputNoSaktiM) {
			getCurrentSession().getNamedQuery("Report#saveNoSakti")
			.setString("kodeBilling", inputNoSaktiM.getKodeBilling())
			.setString("noSakti",inputNoSaktiM.getNoSakti())
			.executeUpdate();	
		return null;
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<InputNoSaktiM> getListMir(String referenceNo) {
		List<InputNoSaktiM> list = new ArrayList<InputNoSaktiM>();
		try {
			list = (List<InputNoSaktiM>) getCurrentSession().getNamedQuery("GetNoMir")
					.setString("referenceNo", referenceNo)
					.list();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@Override
	public void updateNoMir(String noMir, String referenceNo,String startDateS,String trxFlag ) {
			getCurrentSession().getNamedQuery("UpdateNoMir")
			.setString("noMir", noMir)
			.setString("referenceNo",referenceNo)
			.setString("startDate",startDateS)
			.setString("trxFlag",trxFlag)
			.executeUpdate();	
		
	}
	
	@Override
	public void insertDataPelimpahan(String trxId, String startDateS,String referenceNo,String noMir,String trxFlag) {
		System.out.println("masuk sini kah?");
			getCurrentSession().getNamedQuery("InsertDataPelimpahan")
			.setString("trxId", trxId)
			.setString("startDate", startDateS)
			.setString("referenceNo", referenceNo)
			.setString("noMir", noMir)
			.setString("trxFlag",trxFlag)
			.executeUpdate();	
		
	}
	
}
