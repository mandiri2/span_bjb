package com.mii.dao;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import com.mii.helpers.BasicDaoImpl;
import com.mii.models.Menu;
import com.mii.models.RoleMenu;

public class MenuDaoImpl extends BasicDaoImpl implements MenuDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<Menu> getAllMenu() {
		List<Menu> menus = new ArrayList<Menu>();
		
		menus = getCurrentSession().createQuery("FROM com.mii.models.Menu").list();
		return menus;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<RoleMenu> getAllRoleMenus(List<String> roleIds) {
		List<RoleMenu> roleMenus = getCurrentSession().getNamedQuery("menu#getMenuByRole").
				setParameterList("roles", roleIds).list();
		return roleMenus;
	}

	@Override
	public int getMenuAuthority(String userId, String menuPath) {
		BigInteger auth = null;
		try{
			auth = (BigInteger) getCurrentSession().
					getNamedQuery("menu#getMenuAuthority")
					.setParameter("userId", userId)
					.setParameter("menuPath", menuPath)
					.uniqueResult();
		}catch(Exception e){
			e.printStackTrace();
		}
		return Integer.valueOf(auth.toString());
	}

}
