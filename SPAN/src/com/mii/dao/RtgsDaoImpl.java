package com.mii.dao;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.transform.ResultTransformer;

import com.mii.helpers.BasicDaoImpl;
import com.mii.models.ReportTransaksiM;
import com.mii.models.Rtgs;
import com.mii.models.User;

public class RtgsDaoImpl extends BasicDaoImpl implements RtgsDao{

	@Override
	public List<Rtgs> getRtgsLazyLoadingByCategory(int maxRow, int minRow,
			String transactionId,  String transactionDate,String bppxName, String state, String referenceNumber,
			String response, String responseDesc) {
		// TODO Auto-generated method stub
		List<Rtgs> listRtgs = new ArrayList<Rtgs>();
		try{
			listRtgs = (List<Rtgs>)getCurrentSession().
					getNamedQuery("TRtgs#getRtgsLazyLoadingByCategory").
					setString("transactionID", "%"+transactionId+"%").
					setString("transactionDate", "%"+transactionDate+"%").
					setString("bppxName", "%"+bppxName+"%").
					setString("state", "%"+state+"%").
					setString("referenceNumber", "%"+referenceNumber+"%").
					setString("response", "%"+response+"%").
					setString("responseDesc", "%"+responseDesc+"%").
					setString("MAX_ROW", String.valueOf(maxRow)).
					setString("MIN_ROW", String.valueOf(minRow)).
					setResultTransformer(new ResultTransformer() {
						
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							Rtgs rtgs;
								rtgs = new Rtgs(
										String.valueOf(row[0]),
										String.valueOf(row[1]), 
										String.valueOf(row[2]).equals("null") ? null : String.valueOf(row[2]), 
										String.valueOf(row[3]).equals("null") ? null : String.valueOf(row[3]), 
										String.valueOf(row[4]).equals("null") ? null : String.valueOf(row[4]),
										String.valueOf(row[5]).equals("null") ? null : String.valueOf(row[5]), 
										String.valueOf(row[6]).equals("null") ? null : String.valueOf(row[6])
									);
								return rtgs;
						}
						
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).
					list();
		}catch(Exception e){
			e.printStackTrace();
		}
		return listRtgs;
	}

	@Override
	public int countRtgsByCategory(String transactionId, String transactionDate, String bppxName, String state,
			String referenceNumber, String response, String responseDesc) {
		// TODO Auto-generated method stub
		try{
			BigDecimal jml = (BigDecimal) getCurrentSession().
					getNamedQuery("TRtgs#countRecordByCategory").
					setString("transactionID", "%"+transactionId+"%").
					setString("transactionDate", "%"+transactionDate+"%").
					setString("bppxName", "%"+bppxName+"%").
					setString("state", "%"+state+"%").
					setString("referenceNumber", "%"+referenceNumber+"%").
					setString("response", "%"+response+"%").
					setString("responseDesc", "%"+responseDesc+"%").
					uniqueResult();			
			return jml.intValue();
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public Rtgs getDetailedRtgs(String rtgs) {
		Rtgs detailRtgs = new Rtgs();
		try{
			detailRtgs = (Rtgs) getCurrentSession().getNamedQuery("TRtgs#getRtgsByExample")
					.setString("transactionId", rtgs)					
					.uniqueResult();
		}catch(Exception e){
			e.printStackTrace();
		}
		return detailRtgs;
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public List<Rtgs> getRefNo(String td) {

	List<Rtgs> list = new ArrayList<Rtgs>();
		try{
			list = (List<Rtgs>) getCurrentSession().getNamedQuery("TRtgs#getRtgsByDate")
					.setString("transactionDate", td)					
					.list();
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return list;
	}

}
