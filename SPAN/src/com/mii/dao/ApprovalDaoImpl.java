package com.mii.dao;

import java.math.BigInteger;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.rowset.serial.SerialBlob;

import org.hibernate.transform.ResultTransformer;

import com.mii.helpers.BasicDaoImpl;
import com.mii.models.Approval;
import com.mii.models.ApprovalLog;

public class ApprovalDaoImpl extends BasicDaoImpl implements ApprovalDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<Approval> getAllApprovalLazyLoading(int maxRow,
			int minRow, String approverId, String parameterId, String parameterName, List<String> searchParameter,List<String> searchAction, String branchId) {
		List<Approval> approvals = new ArrayList<Approval>();
		try{
			approvals = (List<Approval>)getCurrentSession().
					getNamedQuery("MApproval#getAllApprovalLazyLoading").
					setBigInteger("MAX_ROW", BigInteger.valueOf(maxRow)).
					setBigInteger("MIN_ROW", BigInteger.valueOf(minRow)).
					setString("APPROVER_ID", approverId).
					setString("BRANCH_CODE", branchId).
					setParameterList("LIST_PARAMETER", searchParameter).
					setString("PARAMETER_ID", "%"+parameterId+"%").
					setString("PARAMETER_NAME","%"+ parameterName+"%").
					setParameterList("LIST_ACTION", searchAction).
					setResultTransformer(new ResultTransformer() {						
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg1) {

							Approval approval;
								approval = new Approval(new BigInteger(String.valueOf(row[0])), 
										String.valueOf(row[1]),
										String.valueOf(row[2]), 
										String.valueOf(row[3]), 
										String.valueOf(row[4]), 
										(byte[]) row[5],
										(byte[]) row[6]);
								try{
									if (approval.getNewByteObject() != null){
										approval.setNewBlobObject(new SerialBlob(approval.getNewByteObject()));
									}
								}catch(Exception e){	
								}
								try{
									if (approval.getOldByteObject() != null){
										approval.setOldBlobObject(new SerialBlob(approval.getOldByteObject()));
									}
								}catch(Exception e){
								}	
								return approval;
						}
						
						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).
					list();
		}catch(Exception e){
			e.printStackTrace();
		}
		return approvals;
	}

	@Override
	public int countAllApproval(String approverId, String parameterId, String parameterName, List<String> searchParameter,List<String> searchAction, String branchId) {
		try{
			BigInteger jml = (BigInteger) getCurrentSession().
					getNamedQuery("MApproval#countAllRecord").
					setParameterList("LIST_PARAMETER", searchParameter).
					setString("PARAMETER_ID", "%"+parameterId+"%").
					setString("PARAMETER_NAME", "%"+parameterName+"%").
					setParameterList("LIST_ACTION", searchAction).
					setString("APPROVER_ID", approverId).
					setString("BRANCH_CODE", branchId).
					uniqueResult();			
			return jml.intValue();
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public void saveApproval(Approval approval) {
		try{
			BigInteger id = (BigInteger) getCurrentSession().
					getNamedQuery("MApproval#selectApprovalIdSeq").uniqueResult();
			approval.setApprovalId(id);
			save(approval);
		}catch(Exception e){
			e.printStackTrace();
		}
	}



	@SuppressWarnings("unchecked")
	@Override
	public List<Approval> getApprovalByParameter(String paramater, String approverId) {
		List<Approval> approvals = new ArrayList<Approval>();
		try{
			approvals = getCurrentSession().getNamedQuery("MApproval#selectApprovalByParameter").
					setString("PARAMETER", paramater).
					setString("APPROVER_ID", approverId).list();
			for (Approval a : approvals){
				if (a.getNewByteObject() != null){
					a.setNewBlobObject(new SerialBlob(a.getNewByteObject()));
				}
				if (a.getOldByteObject() != null){
					a.setOldBlobObject(new SerialBlob(a.getOldByteObject()));
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return approvals;
	}

	@Override
	public void deleteApproval(List<Approval> approvals) {
		List<BigInteger> listApprovalId = new ArrayList<BigInteger>();
						
		for (Approval a : approvals){
			listApprovalId.add(a.getApprovalId());
		}
				
		try {
			getCurrentSession().getNamedQuery("MApproval#deleteApproval")
			.setParameterList("LIST_APPROVAL_ID", listApprovalId).executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Approval> getApprovalByParameterId(String parameterId) {
		List<Approval> approvals = new ArrayList<Approval>();
		try{
			approvals = getCurrentSession().getNamedQuery("MApproval#selectApprovalByParameterId").setString("PARAMETER_ID", parameterId).list();
			for (Approval a : approvals){
				if (a.getNewByteObject() != null){
					a.setNewBlobObject(new SerialBlob(a.getNewByteObject()));
				}
				if (a.getOldByteObject() != null){
					a.setOldBlobObject(new SerialBlob(a.getOldByteObject()));
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return approvals;
	}

	@Override
	public void updateApprovalNewObjectByParameterId(byte[] newByteObject, String parameterId) {
		try {
			getCurrentSession().getNamedQuery("MApproval#updateApprovalNewObjectByParameterId")
			.setBinary("NEW_OBJECT",newByteObject)
			.setString("PARAMETER_ID", parameterId).executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void assignApprover(String approverId){
		Integer rowNum = 30;
		try {
			getCurrentSession().getNamedQuery("MApproval#assignApprover").
			setString("APPROVER_ID", approverId).
			setBigInteger("ROW_NUM", BigInteger.valueOf(rowNum)).executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void removeApprover(List<String> listApproverId) {
		try {
			getCurrentSession().getNamedQuery("MApproval#removeApprover")
			.setParameterList("LIST_APPROVER_ID", listApproverId).executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void removeAllApprover() {
		try {
			getCurrentSession().getNamedQuery("MApproval#removeAllApprover")
			.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void deleteApproval(Approval approval) {
		delete(approval);
	}

	@Override
	public void saveApprovalLog(Approval approval, String statusCode, String statusMessage) {
		ApprovalLog approvalLog = new ApprovalLog();
		BigInteger id = (BigInteger) getCurrentSession().
				getNamedQuery("MApprovalLog#selectApprovalLogIdSeq").uniqueResult();
		approvalLog.setApprovalLogId(id);
		approvalLog.setApprovalId(approval.getApprovalId());
		approvalLog.setParameter(approval.getParameter());
		approvalLog.setParameterId(approval.getParameterId());
		approvalLog.setAction(approval.getAction());
		approvalLog.setApproverId(approval.getApproverId());
		approvalLog.setBranchId(approval.getBranchId());
		approvalLog.setApproveDate(new Date());
		approvalLog.setStatusCode(statusCode);
		approvalLog.setStatusMessage(statusMessage);
		save(approvalLog);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Approval> getApprovalByParameterSearch(String approverId,
			String parameterId, String parameterName,
			List<String> searchParameter, List<String> searchAction,
			String branchId) {
		List<Approval> approvals = new ArrayList<Approval>();
		try{
			approvals = getCurrentSession().getNamedQuery("MApproval#selectApprovalByParameterSearch").
					setString("APPROVER_ID", approverId).
					setString("BRANCH_CODE", branchId).
					setParameterList("LIST_PARAMETER", searchParameter).
					setString("PARAMETER_ID", "%"+parameterId+"%").
					setString("PARAMETER_NAME","%"+ parameterName+"%").
					setParameterList("LIST_ACTION", searchAction).
					list();
			for (Approval a : approvals){
				if (a.getNewByteObject() != null){
					a.setNewBlobObject(new SerialBlob(a.getNewByteObject()));
				}
				if (a.getOldByteObject() != null){
					a.setOldBlobObject(new SerialBlob(a.getOldByteObject()));
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return approvals;
	}

}
