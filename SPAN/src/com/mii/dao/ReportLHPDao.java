package com.mii.dao;

import java.util.List;

import com.mii.helpers.BasicDao;
import com.mii.models.ReportLHPM;


public interface ReportLHPDao extends BasicDao {
	
	public List<ReportLHPM> getReportLHP(String tglbukuS,String tahunDateS,String currencyS);
	public String getPathFile(String paramNamePF);	
	public String getKodeBank(String paramNameKB);
}
