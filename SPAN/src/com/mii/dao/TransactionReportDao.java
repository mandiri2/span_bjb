package com.mii.dao;

import java.util.Date;
import java.util.List;

import com.mii.helpers.BasicDao;
import com.mii.models.InternalReportBJB;
import com.mii.models.SpanAccountHistory;

public interface TransactionReportDao extends BasicDao{

	List<InternalReportBJB> getTransactionReport(Date startDocDate,
			Date endDocDate, String accountCodeNumber, String paymentMethod,
			String transactionStatus, int i, int j);

	Integer countTransactionReport(Date startDocDate, Date endDocDate,
			String accountCodeNumber, String paymentMethod,
			String transactionStatus);

	int countAccountHistoryGAJI(String debitaccno, String docDate,
			String inquiryType);

	List<SpanAccountHistory> showAccountHistoryGAJI(String debitaccno,
			String docDate, String inquiryType, int startRow, int endRow);
	
	List<InternalReportBJB> getAccountValidation(Date startDocDate,
			Date endDocDate, String sp2dno, String accountCodeNumber, String accountName,
			String transactionStatus, int start, int end);

	Integer countAccountValidation(Date startDocDate,
			Date endDocDate, String sp2dno, String accountCodeNumber, String accountName,
			String transactionStatus);
	
	List<InternalReportBJB> getTransactionReject(String filename, Date startDocDate, Date endDocDate, Date rejectDate,
			int start, int max);
	
	int countTransactionReject(String filename, Date startDocDate, Date endDocDate, Date rejectDate);
}
