package com.mii.dao;

import java.math.BigInteger;
import java.util.List;

import org.hibernate.transform.ResultTransformer;

import com.mii.helpers.BasicDaoImpl;
import com.mii.models.FastBJBModel;

public class FASTBJBDaoImpl extends BasicDaoImpl implements FASTBJBDao{

	@Override
	public int validateUserAccountBJB(String accountName, String accountNo) {
		try {
			BigInteger jml = (BigInteger) getCurrentSession()
					.getNamedQuery("FASTBJB#validateUserAccountBJB")
					.setString("accountName", accountName)
					.setString("accountNo", accountNo)
					.uniqueResult();
			return jml.intValue();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	@SuppressWarnings("unchecked")
	@Override
	public FastBJBModel checkBlockedUserAccountBJB(String accountName,
			String accountNo) {
		List<FastBJBModel> resultList = getCurrentSession()
		.getNamedQuery("FASTBJB#checkBlockedUserAccountBJB")
		.setString("accountName", accountName)
		.setString("accountNo", accountNo)
		.setResultTransformer(new ResultTransformer() {						
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							FastBJBModel fastBJBModel;
							try {
								fastBJBModel = new FastBJBModel(
										String.valueOf(row[0]),
										String.valueOf(row[1]),
										String.valueOf(row[2]));
								return fastBJBModel;
							} catch (Exception e) {
								e.printStackTrace();
							}
							return null;
						}
						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).list();
		FastBJBModel result = new FastBJBModel();
		for(FastBJBModel a : resultList){
			result  = a;
		}
		return result;
	}

}
