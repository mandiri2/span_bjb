package com.mii.dao;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.transform.ResultTransformer;

import com.mii.helpers.BasicDaoImpl;
import com.mii.models.SPANVoidDocumentNumber;
import com.mii.models.SpanMonitoringVoidDepKeu;

public class SPANMonitoringVoidDepKeuDaoImpl extends BasicDaoImpl implements SPANMonitoringVoidDepKeuDao{

	@SuppressWarnings("unchecked")
	@Override
	public List<SpanMonitoringVoidDepKeu> getLazySpanDataValidationSp2dNo(String fileName, String docDate, String voidFlag, String debitAccountType, 
			String sp2dNumber,  int startRow, int endRow) {
		List<SpanMonitoringVoidDepKeu> details = new ArrayList<SpanMonitoringVoidDepKeu>();
		try {
			fileName = "%"+fileName+"%";
			sp2dNumber = "%"+sp2dNumber+"%";
			details = (List<SpanMonitoringVoidDepKeu>) getCurrentSession().
					getNamedQuery("SPANMonitoringVoidDepKeu#getSpanDataValidationSp2dNo").
					setParameter("fileName", fileName).
					setParameter("docDate", docDate).
					setParameter("voidFlag", voidFlag).
					setParameter("debitAccountType", debitAccountType).
					setParameter("sp2dNumber", sp2dNumber).
					setParameter("startRow", startRow).
					setParameter("endRow", endRow).
					setResultTransformer(new ResultTransformer() {
						private static final long serialVersionUID = 1L;
						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							SpanMonitoringVoidDepKeu spanMonitoringVoidDepKeu;
							spanMonitoringVoidDepKeu = new SpanMonitoringVoidDepKeu(
									String.valueOf(row[0]),
									String.valueOf(row[1]), 
									String.valueOf(row[2]), 
									String.valueOf(row[3]), 
									String.valueOf(row[4]),
									String.valueOf(row[5]),
									String.valueOf(row[6]),
									String.valueOf(row[7]),
									String.valueOf(row[8]),
									String.valueOf(row[9]));
							return spanMonitoringVoidDepKeu;
						}
						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}}).list();
		}catch(Exception e){
			e.printStackTrace();
		}
		return details;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SpanMonitoringVoidDepKeu> getLazyHistorySp2dNo(String fileName, String docDate, String debitAccountType, 
			String sp2dNumber,  int startRow, int endRow) {
		List<SpanMonitoringVoidDepKeu> details = new ArrayList<SpanMonitoringVoidDepKeu>();
		try {
			details = (List<SpanMonitoringVoidDepKeu>) getCurrentSession().
					getNamedQuery("SPANMonitoringVoidDepKeu#getHistoryBackdateSp2dNo").
					setParameter("fileName", fileName).
					setParameter("docDate", docDate).
					setParameter("debitAccountType", debitAccountType).
					setParameter("sp2dNumber", sp2dNumber).
					setResultTransformer(new ResultTransformer() {
						private static final long serialVersionUID = 1L;
						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							SpanMonitoringVoidDepKeu spanMonitoringVoidDepKeu;
							spanMonitoringVoidDepKeu = new SpanMonitoringVoidDepKeu(
									String.valueOf(row[0]),
									String.valueOf(row[1]), 
									String.valueOf(row[2]), 
									String.valueOf(row[3]), 
									String.valueOf(row[4]),
									String.valueOf(row[5]),
									String.valueOf(row[6]),
									String.valueOf(row[7]),
									String.valueOf(row[8]),
									String.valueOf(row[9]));
							spanMonitoringVoidDepKeu.setProcStatus(String.valueOf(row[10]));
							return spanMonitoringVoidDepKeu;
						}
						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}}).list();
		}catch(Exception e){
			e.printStackTrace();
		}
		return details;
	}
	
	@Override
	public int countHistorySp2dNo(String fileName, String docDate,
			String debitAccountType, String sp2dNumber) {
		try {
			BigInteger jml = (BigInteger) getCurrentSession().
					getNamedQuery("SPANMonitoringVoidDepKeu#countHistoryBackDateSp2dNo").
					setParameter("fileName", fileName).
					setParameter("docDate", docDate).
					setParameter("debitAccountType", debitAccountType).
					setParameter("sp2dNumber", sp2dNumber).uniqueResult();
			return jml.intValue();
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SpanMonitoringVoidDepKeu> getLazySpanDataValidationBackdate(String filename,String sp2d, int startRow, int endRow) {
		List<SpanMonitoringVoidDepKeu> details = new ArrayList<SpanMonitoringVoidDepKeu>();
		try {
			details = (List<SpanMonitoringVoidDepKeu>) getCurrentSession().
					getNamedQuery("SPANMonitoringVoidDepKeu#getSpanDataValidationBackdate").
					setParameter("filename", filename).
					setParameter("sp2dno", sp2d).
					setParameter("startRow", startRow).
					setParameter("endRow", endRow).
					setResultTransformer(new ResultTransformer() {
						private static final long serialVersionUID = 1L;
						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							SpanMonitoringVoidDepKeu spanMonitoringVoidDepKeu;
							spanMonitoringVoidDepKeu = new SpanMonitoringVoidDepKeu();
									spanMonitoringVoidDepKeu.setFileName(String.valueOf(row[0]));
									spanMonitoringVoidDepKeu.setDocumentDate(String.valueOf(row[1]));
									SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
									try {
										spanMonitoringVoidDepKeu.setCreateDate(sdf.parse(String.valueOf(row[2])));
									} catch (ParseException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
							return spanMonitoringVoidDepKeu;
						}
						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}}).list();
		}catch(Exception e){
			e.printStackTrace();
		}
		return details;
	}

	@Override
	public int countSpanDataValidationSp2dNo(String fileName, String docDate,
			String voidFlag, String debitAccountType, String sp2dNumber) {
		try {
			fileName = "%"+fileName+"%";
			sp2dNumber = "%"+sp2dNumber+"%";
			BigInteger jml = (BigInteger) getCurrentSession().
					getNamedQuery("SPANMonitoringVoidDepKeu#countSpanDataValidationSp2dNo").
					setParameter("fileName", fileName).
					setParameter("docDate", docDate).
					setParameter("voidFlag", voidFlag).
					setParameter("debitAccountType", debitAccountType).
					setParameter("sp2dNumber", sp2dNumber).uniqueResult();
			return jml.intValue();
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}
	
	@Override
	public int countSpanDataValidationBackdate(String filename) {
		try {
			BigInteger jml = (BigInteger) getCurrentSession().
					getNamedQuery("SPANMonitoringVoidDepKeu#countSpanDataValidationBackdate").
					setParameter("filename", filename).
					uniqueResult();
			return jml.intValue();
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public int countSpanVoidDataTrx(String status, String fileName,
			String sp2dNumber) {
		try {
			sp2dNumber = "%"+sp2dNumber+"%";
			BigInteger jml = (BigInteger) getCurrentSession().
					getNamedQuery("SPANMonitoringVoidDepKeu#countSpanVoidDataTrx").
					setParameter("status", status).
					setParameter("fileName", fileName).
					setParameter("sp2dNumber", sp2dNumber).uniqueResult();
			return jml.intValue();
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}
	
	@Override
	public int countSpanVoidDataTrxHist(String fileName,
			String sp2dNumber) {
		try {
			sp2dNumber = "%"+sp2dNumber+"%";
			BigInteger jml = (BigInteger) getCurrentSession().
					getNamedQuery("SPANMonitoringVoidDepKeu#countSpanVoidDataTrxHist").
					setParameter("fileName", fileName).
					setParameter("sp2dNumber", sp2dNumber).uniqueResult();
			return jml.intValue();
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}


	@Override
	public int deleteSpanVoidDataTrx(String status, String fileName,
			String sp2dNumber) {
		try {
			sp2dNumber = sp2dNumber+"%";
			int result = getCurrentSession().
					getNamedQuery("SPANMonitoringVoidDepKeu#deleteSpanVoidDataTrx").
					setParameter("status", status).
					setParameter("fileName", fileName).
					setParameter("sp2dNumber", sp2dNumber).executeUpdate();
			return result;
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public int countVoid(String status, String fileName, String sp2dNumber) {
		try {
			sp2dNumber = sp2dNumber+"%";
			BigInteger jml = (BigInteger) getCurrentSession().
					getNamedQuery("SPANMonitoringVoidDepKeu#countVoid").
					setParameter("status", status).
					setParameter("fileName", fileName).
					setParameter("sp2dNumber", sp2dNumber).uniqueResult();
			return jml.intValue();
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}
	
	@Override
	public int sumAmountVoid(String status, String fileName, String sp2dNumber) {
		try {
			sp2dNumber = sp2dNumber+"%";
			BigDecimal jml = (BigDecimal) getCurrentSession().
					getNamedQuery("SPANMonitoringVoidDepKeu#sumAmountVoid").
					setParameter("status", status).
					setParameter("fileName", fileName).
					setParameter("sp2dNumber", sp2dNumber).uniqueResult();
			int amount = 0;
			try{
				amount = jml.intValue();
			}catch(Exception ex){}
			return amount;
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public int countSearchDocumentNumber(String documentNumber, String status) {
		try{
			BigInteger jml = (BigInteger) getCurrentSession().
					getNamedQuery("SPANMonitoringVoidDepKeu#countSearchDocumentNumber").
					setParameter("status", status).
					setParameter("documentNumber", documentNumber).uniqueResult();
			return jml.intValue();
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SPANVoidDocumentNumber> getVoidDataTransactionListByFilenameSp2d(
			String status, String fileName, String sp2dNumber) {
		List<SPANVoidDocumentNumber> details = new ArrayList<SPANVoidDocumentNumber>();
		try {
			sp2dNumber = sp2dNumber+"%";
			details = (List<SPANVoidDocumentNumber>) getCurrentSession().
					getNamedQuery("SPANMonitoringVoidDepKeu#getVoidDataTransactionListByFilenameSp2d").
					setParameter("status", status).
					setParameter("fileName", fileName).
					setParameter("sp2dNumber", sp2dNumber).
					setResultTransformer(new ResultTransformer() {
						private static final long serialVersionUID = 1L;
						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							SPANVoidDocumentNumber spanVoidDocumentNumber;
							spanVoidDocumentNumber = new SPANVoidDocumentNumber(
									String.valueOf(row[0]), 
									String.valueOf(row[1]),
									String.valueOf(row[2]), 
									String.valueOf(row[3]));
							return spanVoidDocumentNumber;
						}
						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}}).list();
		}catch(Exception e){
			e.printStackTrace();
		}
		return details;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SPANVoidDocumentNumber> getVoidDataTransactionListByFilenameSp2dNoHost(String fileName, String sp2dNumber) {
		List<SPANVoidDocumentNumber> details = new ArrayList<SPANVoidDocumentNumber>();
		try {
			sp2dNumber = sp2dNumber+"%";
			details = (List<SPANVoidDocumentNumber>) getCurrentSession().
					getNamedQuery("SPANMonitoringVoidDepKeu#getVoidDataTransactionListByFilenameSp2dNoHost").
					setParameter("fileName", fileName).
					setParameter("sp2dNumber", sp2dNumber).
					setResultTransformer(new ResultTransformer() {
						private static final long serialVersionUID = 1L;
						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							SPANVoidDocumentNumber spanVoidDocumentNumber;
							spanVoidDocumentNumber = new SPANVoidDocumentNumber(
									String.valueOf(row[0]), 
									String.valueOf(row[1]),
									String.valueOf(row[2]), 
									String.valueOf(row[3]));
							return spanVoidDocumentNumber;
						}
						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}}).list();
		}catch(Exception e){
			e.printStackTrace();
		}
		return details;
	}


}
