package com.mii.dao;

import java.util.ArrayList;
import java.util.List;

import com.mii.helpers.BasicDaoImpl;
import com.mii.models.PaymentM;

public class PaymentDaoImpl extends BasicDaoImpl implements PaymentDao{

	@SuppressWarnings("unchecked")
	@Override
	public List<PaymentM> getLogByPaymentcode(String kodeBilling) {
		List<PaymentM> list = new ArrayList<PaymentM>();
		list = (List<PaymentM>) getCurrentSession().getNamedQuery("Report#getLogPayment")
				.setString("paymentCode", kodeBilling)
				.list();
		return list;
	}

}
