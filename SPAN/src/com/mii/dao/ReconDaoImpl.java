package com.mii.dao;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.transform.ResultTransformer;

import com.mii.helpers.BasicDaoImpl;
import com.mii.models.Recon;

public class ReconDaoImpl  extends BasicDaoImpl implements ReconDao {

	@Override
	public List<Recon> getSearchReconLazyLoading(String caName, String settlementDate, String reconTime, String billingCode,
			String ntb, String ntpn, String noSakti,
			int maxRow, int minRow) {
		List<Recon> reconList = new ArrayList<Recon>();
		try{
			reconList = (List<Recon>)getCurrentSession().
					getNamedQuery("TRecon#getSearchReconLazyLoading")
					.setString("CANAME", "%"+caName+"%")
					.setString("SETTLEMENTDATE", "%"+settlementDate+"%")
					.setString("RECONTIME", "%"+reconTime+"%")
					.setString("BILLINGCODE", "%"+billingCode+"%")
					.setString("NTB", "%"+ntb+"%")
					.setString("NTPN", "%"+ntpn+"%")
					.setString("NOSAKTI", "%"+noSakti+"%")
					.setString("MAX_ROW", String.valueOf(maxRow))
					.setString("MIN_ROW", String.valueOf(minRow))
					.setResultTransformer(new ResultTransformer() {						
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
								Recon recon = new Recon(
										String.valueOf(row[0]), 
										String.valueOf(row[1]), 
										String.valueOf(row[2]),
										String.valueOf(row[3]),
										String.valueOf(row[4]),
										String.valueOf(row[5]),
										String.valueOf(row[6]),
										String.valueOf(row[7]),
										String.valueOf(row[8]),
										String.valueOf(row[9]));
								return recon;
						}
						
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).
					list();
		}catch(Exception e){
			e.printStackTrace();
		}
		return reconList;
	}

	@Override
	public int countSearchRecon(String caName, String settlementDate, String reconTime, String billingCode,
			String ntb, String ntpn, String noSakti) {
		try{
			BigDecimal jml = (BigDecimal) getCurrentSession().
					getNamedQuery("TRecon#countSearchRecord")
					.setString("CANAME", "%"+caName+"%")
					.setString("SETTLEMENTDATE", "%"+settlementDate+"%")
					.setString("RECONTIME", "%"+reconTime+"%")
					.setString("BILLINGCODE", "%"+billingCode+"%")
					.setString("NTB", "%"+ntb+"%")
					.setString("NTPN", "%"+ntpn+"%")
					.setString("NOSAKTI", "%"+noSakti+"%")
					.uniqueResult();			
			return jml.intValue();
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}
	
}
