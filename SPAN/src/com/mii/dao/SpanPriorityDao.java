package com.mii.dao;

import java.util.List;

import com.mii.helpers.BasicDao;
import com.mii.models.SpanPriority;

public interface SpanPriorityDao extends BasicDao{

	void insertNew(SpanPriority sp,String user);
	void updateDeleteFlag(String sp,String user);
	List<SpanPriority> getAllData();
	void updateNew(SpanPriority sp, String user);

}
