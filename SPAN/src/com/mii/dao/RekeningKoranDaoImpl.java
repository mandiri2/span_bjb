package com.mii.dao;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.hibernate.transform.ResultTransformer;

import com.mii.helpers.BasicDaoImpl;
import com.mii.models.SpanRekeningKoran;

public class RekeningKoranDaoImpl extends BasicDaoImpl implements RekeningKoranDao{

	@SuppressWarnings("unchecked")
	@Override
	public List<SpanRekeningKoran> getAll(String accNo, Date stDate,Date enDate){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		@SuppressWarnings("rawtypes")
		List list = getCurrentSession().getNamedQuery("RekeningKoran#GetAll").
		setParameter("account_number", accNo).
		setParameter("startDate", stDate).
		setParameter("endDate", enDate).
		setResultTransformer(new ResultTransformer() {
			
			private static final long serialVersionUID = 142524939240460506L;

			@Override
			public Object transformTuple(Object[] arg0, String[] arg1) {
				SpanRekeningKoran srk = new SpanRekeningKoran();
				srk.setTransactionDate(String.valueOf(arg0[0]));
				srk.setBankTransactionCode(String.valueOf(arg0[1]));
				srk.setDebit(String.valueOf(arg0[2]));
				srk.setCredit(String.valueOf(arg0[3]));
				srk.setBankReferenceNumber(String.valueOf(arg0[4]).replaceAll("SPAN ", ""));
				srk.setTotalAmount(String.valueOf(arg0[5]));
				return srk;
			}
			
			@Override
			public List transformList(List arg0) {
			
				return arg0;
			}
		}).list();
		
		return list;
	}

}
