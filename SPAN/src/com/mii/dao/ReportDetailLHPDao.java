package com.mii.dao;

import java.util.List;

import com.mii.helpers.BasicDao;
import com.mii.models.ReportDetailLHPM;


public interface ReportDetailLHPDao extends BasicDao {
	

	
	public List<ReportDetailLHPM> getReportDetailLHP(String tglbukuS,String tahunDateS,String currencyS);
	
}
