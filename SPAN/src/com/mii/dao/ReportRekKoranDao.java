package com.mii.dao;

import java.util.List;

import com.mii.helpers.BasicDao;
import com.mii.models.ReportRekKoranM;


public interface ReportRekKoranDao extends BasicDao {
	
	public List<ReportRekKoranM> getFilenameBS(String dateBS,String currency);
	public String getFilenameBSString(String dateBS,String currency);
	public String getPathFile(String paramNamePF);
}
