package com.mii.dao;

import java.util.List;

import com.mii.helpers.BasicDao;
import com.mii.models.InputNoSaktiM;
import com.mii.models.Rtgs;


public interface InputNoSaktiDao extends BasicDao {
	
	public List<InputNoSaktiM> getFilterTrx(String startDateS, String tahunDateS, String ntb);
	public InputNoSaktiM updateNoSakti(InputNoSaktiM inputNoSaktiM);
	public List<InputNoSaktiM> getListMir(String referenceNo);
	public void updateNoMir(String noMir, String referenceNo,String startDateS,String trxFlag ); 
	public void insertDataPelimpahan(String trxId, String startDateS,String referenceNo,String noMir,String trxFlag);
	
}
