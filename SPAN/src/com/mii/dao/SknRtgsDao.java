package com.mii.dao;

import java.util.List;

import com.mii.helpers.BasicDao;
import com.mii.models.SknRtgs;
import com.mii.sknrtgs.ACKDataDocument;

public interface SknRtgsDao extends BasicDao {
	public List<SknRtgs> getSearchSknRtgsLazyLoading(String docNo,String creditAccName, String creditAccNo, String beneficiaryBankName, String creditTrfAmount,String remittanceNo,String sorborNum,String transactionType, List<String> blackListPM, int maxRow, int minRow);
	public SknRtgs getDetailedSknRtgsById(String docNo,String creditAccNo);
	public void updateSknRtgs(SknRtgs sknRtgs);
	public void updateReturFlag(String remittanceNo, String returReasonCode);
	public SknRtgs getDetailedSknRtgsByRemittanceNo(String remittanceNo);
	public SknRtgs isAlreadyRetured(String remittanceNo);
	public int countSearchSknRtgs(String docNo,String creditAccName, String creditAccNo, String beneficiaryBankName, String creditTrfAmount,String remittanceNo,String sorborNum,String transactionType, List<String> blackListPM);
	public boolean isAlreadyProcessed(String docnumber);
//	public List<SknRtgs> getAllSknRtgs();
//	public List<SknRtgs> getAllSknRtgsLazyLoading(int maxRow, int minRow);
//	public List<SknRtgs> getDetailedSknRtgsByCreditAccName(String creditAccName);
//	public List<SknRtgs> getDetailedSknRtgsByCreditAccNo(String creditAccNo);
//	public List<SknRtgs> getDetailedSknRtgsByBeneficiaryBankName(String beneficiaryBankName);
//	public List<SknRtgs> getDetailedSknRtgsByCreditTrfAmount(String creditTrfAmount);
	
	//For Retur
	public ACKDataDocument selectACKDataDocument(String documentNo);
}
