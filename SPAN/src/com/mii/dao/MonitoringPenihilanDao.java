package com.mii.dao;

import java.util.List;

import com.mii.helpers.BasicDao;
import com.mii.models.MonitoringPenihilan;

public interface MonitoringPenihilanDao extends BasicDao {

	List<MonitoringPenihilan> getMonitoringPenihilan(String tanggalPenihilan, String referenceNo, int startingAt, int i);

	int countMonitoringPenihilan(String tanggalPenihilan, String referenceNo);
	
}
