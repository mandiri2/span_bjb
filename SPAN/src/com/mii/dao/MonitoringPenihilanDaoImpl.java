package com.mii.dao;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.transform.ResultTransformer;

import com.mii.helpers.BasicDaoImpl;
import com.mii.models.MonitoringPenihilan;

public class MonitoringPenihilanDaoImpl extends BasicDaoImpl implements
		MonitoringPenihilanDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<MonitoringPenihilan> getMonitoringPenihilan(
			String tanggalPenihilan, String referenceNo, int min, int max) {

		List<MonitoringPenihilan> internalReportBJBs = new ArrayList<MonitoringPenihilan>();
		try {
			internalReportBJBs = (List<MonitoringPenihilan>) getCurrentSession()
					.getNamedQuery("MonitoringPenihilan#getMonitoringPenihilan")
					.setString("tanggalPenihilan", tanggalPenihilan)
					.setString("referenceNo", referenceNo)
					.setInteger("min", min).setInteger("max", max)
					.setResultTransformer(new ResultTransformer() {
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg1) {

							MonitoringPenihilan monitoringPenihilan;
							monitoringPenihilan = new MonitoringPenihilan(
									String.valueOf(row[0]), String
											.valueOf(row[1]), String
											.valueOf(row[2]), String
											.valueOf(row[3]), String
											.valueOf(row[4]), String
											.valueOf(row[5]), String
											.valueOf(row[6]));
							return monitoringPenihilan;
						}

						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return internalReportBJBs;
	}

	@Override
	public int countMonitoringPenihilan(String tanggalPenihilan,
			String referenceNo) {
		/*
		 * try { BigDecimal jml = (BigDecimal) getCurrentSession()
		 * .getNamedQuery("SpanDataAcctHistory#countAccountHistory")
		 * .setParameter("debitaccno", debitaccno) .setParameter("docDate",
		 * docDate) .setParameter("inquiryType", inquiryType).uniqueResult();
		 * return jml.intValue(); } catch (Exception e) { e.printStackTrace(); }
		 */
		
		BigInteger result = (BigInteger) getCurrentSession().getNamedQuery("MonitoringPenihilan#countMonitoringPenihilan").setString("tanggalPenihilan", tanggalPenihilan)
				.setString("referenceNo", referenceNo).uniqueResult();
		
		return result.intValue();
	}
}
