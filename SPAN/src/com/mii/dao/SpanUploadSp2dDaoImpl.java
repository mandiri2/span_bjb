package com.mii.dao;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.transform.ResultTransformer;

import com.mii.helpers.BasicDaoImpl;
import com.mii.models.MonitoringManualUploadSp2d;

public class SpanUploadSp2dDaoImpl extends BasicDaoImpl implements SpanUploadSp2dDao {

	@Override
	public int countFileName(String fileName) {
		try {
			BigInteger jml = (BigInteger) getCurrentSession()
					.getNamedQuery("SpanUploadSp2d#countFileName")
					.setString("fileName", fileName)
					.uniqueResult();
			return jml.intValue();
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	@Override
	public int countSpanValidationFileName(String fileName) {
		try {
			String filename = fileName.replace(".xml", ".jar");
			filename = "%"+filename+"%";
			BigInteger jml = (BigInteger) getCurrentSession()
					.getNamedQuery("SpanUploadSp2d#countSpanValidationFileName")
					.setString("fileName", fileName)
					.uniqueResult();
			return jml.intValue();
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<MonitoringManualUploadSp2d> getUploadFile(int maxRow, int minRow, String startDate, String endDate) {
		List<MonitoringManualUploadSp2d> monitoringManualUploadSp2dList = new ArrayList<MonitoringManualUploadSp2d>();
		try {
			monitoringManualUploadSp2dList = getCurrentSession()
					.getNamedQuery("SpanUploadSp2d#getUploadFile")
					.setParameter("startDate", startDate)
					.setParameter("endDate", endDate)
					.setBigInteger("maxRow", BigInteger.valueOf(maxRow))
					.setBigInteger("minRow", BigInteger.valueOf(minRow))
					.setResultTransformer(new ResultTransformer() {
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg1) {

							MonitoringManualUploadSp2d monitoringManualUploadSp2d;
							monitoringManualUploadSp2d = new MonitoringManualUploadSp2d(
									String.valueOf(row[0]),
									String.valueOf(row[1]), 
									String.valueOf(row[2]),
									String.valueOf(row[3]), 
									String.valueOf(row[4]),
									String.valueOf(row[5]),
									String.valueOf(row[6]),
									String.valueOf(row[7]),
									String.valueOf(row[8]),
									String.valueOf(row[9]),
									String.valueOf(row[10]),
									String.valueOf(row[11]),
									String.valueOf(row[12]),
									String.valueOf(row[13]));
							return monitoringManualUploadSp2d;
						}

						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return monitoringManualUploadSp2dList;
	}

	@Override
	public int countGetUploadFile(String startDate, String endDate) {
		try {
			BigInteger jml = (BigInteger) getCurrentSession()
					.getNamedQuery("SpanUploadSp2d#countGetUploadFile")
					.setParameter("startDate", startDate)
					.setParameter("endDate", endDate)
					.uniqueResult();
			return jml.intValue();
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

}
