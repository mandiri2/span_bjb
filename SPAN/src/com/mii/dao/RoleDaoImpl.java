package com.mii.dao;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.transform.ResultTransformer;

import com.mii.constant.Constants;
import com.mii.helpers.BasicDaoImpl;
import com.mii.models.Role;
import com.mii.models.UserRole;

public class RoleDaoImpl extends BasicDaoImpl implements RoleDao{

	@SuppressWarnings("unchecked")
	@Override
	public List<Role> getAllRole() {
		List<Role> roles = new ArrayList<Role>();
		try {
			roles = getCurrentSession().createQuery("FROM Role").list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return roles;
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public List<Role> getAllValidRole() {
		List<Role> roles = new ArrayList<Role>();
		

		List<String> listStatus = new ArrayList<String>();
						
		listStatus.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
		listStatus.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
		
		try {
			roles = getCurrentSession().createQuery("FROM Role where status not in (:status)")
					.setParameterList("status", listStatus)
					.list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return roles;
	}
	
	@Override
	public Role getAllValidRoleByRoleName(String roleName) {
		Role role = new Role();
		

		List<String> listStatus = new ArrayList<String>();
						
		listStatus.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
		listStatus.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
		
		try {
			role = (Role) getCurrentSession().createQuery("FROM Role where status not in (:status) and role_name = :roleName")
					.setParameterList("status", listStatus)
					.setString("roleName", roleName)
					.uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return role;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Role> getLazyRole(int maxRow, int minRow) {
		List<Role> roles = new ArrayList<Role>();
		try {
			roles = getCurrentSession().getNamedQuery("role#getLazyRole").
					setBigInteger("MAX_ROW", BigInteger.valueOf(maxRow)).
					setBigInteger("MIN_ROW", BigInteger.valueOf(minRow)).
//					setString("MAX_ROW", String.valueOf(maxRow)).
//					setString("MIN_ROW", String.valueOf(minRow)).
					setResultTransformer(new ResultTransformer() {						
						private static final long serialVersionUID = 1L;
						
						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							Role role;
							try {
								role = new Role(String.valueOf(row[0]),
										String.valueOf(row[1]),
										String.valueOf(row[2]),
										String.valueOf(row[3])
										);
								return role;
							} catch (Exception e) {
								e.printStackTrace();
							}
							return null;
						}
						
						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).
					list();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return roles;
	}

	@Override
	public int countAllRole() {
		try{
			BigInteger jml = (BigInteger) getCurrentSession().
					getNamedQuery("MRole#countAllRecord").uniqueResult();			
			return jml.intValue();
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public int countFilteredRole(String roleId) {
		try{
			BigInteger jml = (BigInteger) getCurrentSession().
					getNamedQuery("MRole#countFilteredRole").
					setString("roleId", "%"+roleId+"%").
					uniqueResult();			
			return jml.intValue();
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Role> getFilteredRole(String roleId, int maxRow, int minRow) {
		List<Role> roles = new ArrayList<Role>();
		try {
			roles = getCurrentSession().getNamedQuery("MRole#getFilteredRole").
					setString("MAX_ROW", String.valueOf(maxRow)).
					setString("MIN_ROW", String.valueOf(minRow)).
					setString("roleId", "%"+roleId+"%").
					setResultTransformer(new ResultTransformer() {						
						private static final long serialVersionUID = 1L;
						
						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							Role role;
							try {
								role = new Role(String.valueOf(row[0]),
										String.valueOf(row[1]),
										String.valueOf(row[2]),
										String.valueOf(row[3])
										);
								return role;
							} catch (Exception e) {
								e.printStackTrace();
							}
							return null;
						}
						
						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).
					list();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return roles;
	}

	@Override
	public Role getDetailedRole(Role role) {
		Role detailRole = new Role();
		try {
			detailRole = (Role) getCurrentSession().getNamedQuery("MRole#getDetailedRole").
					setString("roleId", role.getRoleId()).
					uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return detailRole;
	}

	@Override
	public Role getDetailedRoleByRoleName(String roleName) {
		Role detailRole = new Role();
		try {
			detailRole = (Role) getCurrentSession().getNamedQuery("MRole#getDetailedRoleByRoleName").
					setString("roleName", roleName).
					uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return detailRole;
	}

	@Override
	public void saveOrUpdateRole(Role role) {
		try {
			save(role);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void deleteRole(Role role) {
		try {
			delete(Role.class, role.getRoleId());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Role> getRolesByIds(List<UserRole> userRoles) {
		List<Role> roles = new ArrayList<Role>();		
		try {
			List<String> userIds = new ArrayList<String>();
			for (UserRole userRole : userRoles) {
				userIds.add(userRole.getRole());
			}
			roles = getCurrentSession().getNamedQuery("MRole#getRoles").
					setParameterList("roleId", userIds).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return roles;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Role> getLazyRoleByCategory(int maxRow, int minRow,
			String roleId, String roleName, List<String> status) {
		List<Role> roles = new ArrayList<Role>();
		try {
			roles = getCurrentSession().getNamedQuery("role#getLazyRoleByCategory").
					setBigInteger("MAX_ROW", BigInteger.valueOf(maxRow)).
					setBigInteger("MIN_ROW", BigInteger.valueOf(minRow)).
					setString("ROLE_ID", "%" + roleId + "%").
					setString("ROLE_NAME", "%" + roleName + "%").
					setParameterList("STATUS", status).
					setResultTransformer(new ResultTransformer() {						
						private static final long serialVersionUID = 1L;
						
						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							Role role;
							try {
								role = new Role(String.valueOf(row[0]),
										String.valueOf(row[1]),
										String.valueOf(row[2]),
										String.valueOf(row[3])
										);
								return role;
							} catch (Exception e) {
								e.printStackTrace();
							}
							return null;
						}
						
						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).
					list();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return roles;
	}

	@Override
	public int countAllRoleByCategory(String roleId, String roleName, List<String> status) {
		try{
			BigInteger jml = (BigInteger) getCurrentSession().
					getNamedQuery("MRole#countAllRecordByCategory").
					setString("ROLE_ID", "%" + roleId + "%").
					setString("ROLE_NAME", "%" + roleName + "%").
					setParameterList("STATUS", status).uniqueResult();			
			return jml.intValue();
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}


	@Override
	public Role getRoleByUIMRoleId(String uimRoleId) {
		Role detailRole = new Role();
		try {
			detailRole = (Role) getCurrentSession().getNamedQuery("MRole#getRoleByUIMRoleId").
					setString("uimRoleId", uimRoleId).
					uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return detailRole;
	}


}
