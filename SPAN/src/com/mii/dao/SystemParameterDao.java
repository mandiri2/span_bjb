package com.mii.dao;

import java.math.BigInteger;
import java.util.List;

import com.mii.helpers.BasicDao;
import com.mii.models.SystemParameter;

public interface SystemParameterDao extends BasicDao {
	public void saveSystemParameter(SystemParameter systemParameter);
	public List<SystemParameter> getAllSystemParameter();
	public List<SystemParameter> getSearchParameterLazyLoading(String paramId, String paramName, String paramValue, List<String> status, int maxRow, int minRow);
	public List<SystemParameter> getAllParameterLazyLoading(int maxRow, int minRow);
	public SystemParameter getDetailedParameter(String paramId);
	public SystemParameter getDetailedParameterByParamName(String paramName);
	public void editParameter(SystemParameter systemParameter);
	public int countAllParameter();
	public int countSearchParameter(String paramId, String paramName, String paramValue, List<String> status);
	public SystemParameter deleteSystemParameter(String systemParameterId) ;
	public BigInteger getParameterSequence();
}
