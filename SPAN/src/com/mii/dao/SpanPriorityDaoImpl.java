package com.mii.dao;

import java.util.Iterator;
import java.util.List;

import org.hibernate.transform.ResultTransformer;

import com.mii.helpers.BasicDaoImpl;
import com.mii.models.SpanPriority;

public class SpanPriorityDaoImpl extends BasicDaoImpl implements SpanPriorityDao {
	@Override
	public void updateDeleteFlag(String sp,String user) {
		getCurrentSession().getNamedQuery("SPANPriority#updateDeleteFlag")
		.setParameter("user", user)
		.setParameter("sp2dno", sp)
		.executeUpdate();
	}

	@Override
	public void insertNew(SpanPriority sp,String user) {
		getCurrentSession().getNamedQuery("SPANPriority#insertNew")
		.setParameter("sp2dno", sp.getSp2dno())
		.setParameter("level", sp.getLevel())
		.setParameter("reason", sp.getReason())
		.setParameter("user", user)
		.executeUpdate();

	}
	
	@Override
	public void updateNew(SpanPriority sp,String user) {
		getCurrentSession().getNamedQuery("SPANPriority#updateNew")
		.setParameter("sp2dno", sp.getSp2dno())
		.setParameter("level", sp.getLevel())
		.setParameter("reason", sp.getReason())
		.setParameter("user", user)
		.executeUpdate();

	}

	@Override
	public List<SpanPriority> getAllData() {
		List list =  getCurrentSession().createSQLQuery("select sp2d_no,reason,priority_level from span_priority where is_deleted='false'").setResultTransformer(new ResultTransformer() {
			
			@Override
			public Object transformTuple(Object[] arg0, String[] arg1) {
				SpanPriority sp = new SpanPriority();
				sp.setSp2dno(String.valueOf(arg0[0]));
				sp.setReason(String.valueOf(arg0[1]));
				sp.setLevel(Integer.valueOf(String.valueOf(arg0[2])));
				return sp;
			}
			
			@Override
			public List transformList(List arg0) {
				return arg0;
			}
		}).list();
		
		return list;
		
	}

}
