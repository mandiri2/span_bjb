package com.mii.dao;

import java.util.List;

import com.mii.helpers.BasicDao;
import com.mii.models.SpanHostResponse;

public interface SpanHostResponseDao extends BasicDao {
	public SpanHostResponse getResponseByHostDataDetailsRetryOnly(String batchId,
			String trxHeaderId, String trxDetailId);
	public List<SpanHostResponse> getResponseByResponsecodeRetryOnly(String sp2dNo, String fileName);
	public void updateResponseToVOIDId(String trxHeaderId, String batchId, String trxDetailId);
}
