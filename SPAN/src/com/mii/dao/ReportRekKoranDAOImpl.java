package com.mii.dao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.transform.ResultTransformer;

import com.mii.helpers.BasicDaoImpl;
import com.mii.models.ReportRekKoranM;
import com.mii.models.ReportTransaksiM;


public class ReportRekKoranDAOImpl extends BasicDaoImpl implements ReportRekKoranDao {
	
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ReportRekKoranM> getFilenameBS(String dateBS,String currency) {
		List<ReportRekKoranM> list = new ArrayList<ReportRekKoranM>();
		try {

			list = (List<ReportRekKoranM>) getCurrentSession().getNamedQuery("Report#getBSFilename")
					.setString("dateBS", dateBS)
				.list();
		

		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
//	untuk 1 zip 
	@Override
	public String getFilenameBSString(String dateBS,String currency) {
		
		String nameBS = null;
//		System.out.println("dateBS=="+dateBS);
//		System.out.println("currency=="+currency);
		try{
			nameBS =  (String) getCurrentSession().getNamedQuery("Report#uniqueFileName")
					.setString("dateBS", dateBS)
//					.setString("currency", currency)
					.uniqueResult();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return nameBS;
	}
	

	
	@Override
	public String getPathFile(String paramNamePF) {
		
		String pathFile = null;
		try{
			pathFile =  (String) getCurrentSession().getNamedQuery("Report#getPathFileBS")
					.setString("pathfile", paramNamePF)
					.uniqueResult();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return pathFile;
	}
}
