package com.mii.dao;

import java.util.List;

import com.mii.helpers.BasicDao;
import com.mii.models.Provider;


public interface ProviderDao extends BasicDao {
	List<Provider> getByProviderCodeAlias(String providerCodeAlias);
	List<Provider> getByProviderAccount(String providerAccount);
	void updateProviderAccount(String paramName, String paramValue);
}
