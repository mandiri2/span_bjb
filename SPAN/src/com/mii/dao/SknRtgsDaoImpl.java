package com.mii.dao;

import java.math.BigInteger;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.transform.ResultTransformer;

import com.mii.helpers.BasicDaoImpl;
import com.mii.is.util.PubUtil;
import com.mii.models.SknRtgs;
import com.mii.sknrtgs.ACKDataDocument;
import com.wm.app.b2b.server.Session;

public class SknRtgsDaoImpl extends BasicDaoImpl implements SknRtgsDao {
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SknRtgs> getSearchSknRtgsLazyLoading(String docNo,String creditAccName, String creditAccNo, String beneficiaryBankName, String creditTrfAmount,String remittanceNo,
			String sorborNum,String transactionType, List<String> blackListPM, int maxRow, int minRow){
		List<SknRtgs> sknRtgsList = new ArrayList<SknRtgs>();
		try{
			sknRtgsList = (List<SknRtgs>)getCurrentSession().
					getNamedQuery("getSearchSknRtgsLazyLoading")
					.setString("DOC_NO", "%"+docNo+"%")
					.setString("CREDIT_ACC_NAME", "%"+creditAccName+"%")
					.setString("CREDIT_ACC_NO", "%"+creditAccNo+"%")
					.setString("BENEFICIARY_BANK_NAME", "%"+beneficiaryBankName+"%")
					.setString("CREDIT_TRF_AMOUNT", "%"+creditTrfAmount+"%")
					.setString("TRANSACTION_TYPE", "%"+transactionType+"%")
					.setParameterList("blackListPM", blackListPM)
					.setBigInteger("MAX_ROW", BigInteger.valueOf(maxRow))
					.setBigInteger("MIN_ROW", BigInteger.valueOf(minRow))
					.setResultTransformer(new ResultTransformer() {						
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							SknRtgs sknRtgs;
							try {
								sknRtgs = new SknRtgs(
										String.valueOf(row[0]), 
										String.valueOf(row[1]), 
										String.valueOf(row[2]),
										String.valueOf(row[3]),
										String.valueOf(PubUtil.stringIfNotNull(row[4])),
										String.valueOf(PubUtil.stringIfNotNull(row[5])),
										String.valueOf(row[6]),
										String.valueOf(row[7]),
										String.valueOf(row[8])
										);
								return sknRtgs;
							} catch (Exception e) {
								e.printStackTrace();
							}
							return null;
						}
						
						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).
					list();
		}catch(Exception e){
			e.printStackTrace();
		}
		return sknRtgsList;
	}
	
	
	@Override
	public SknRtgs getDetailedSknRtgsById(String docNo,String creditAccNo){
		SknRtgs sknRtgs = new SknRtgs();
		try {
			sknRtgs = (SknRtgs) getCurrentSession().getNamedQuery("getDetailedSknRtgsById")
					.setString("DOC_NO", docNo)				
					.setString("CREDIT_ACC_NO", creditAccNo)
					.setResultTransformer(new ResultTransformer() {
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							SknRtgs sknRtgsTemp;
							sknRtgsTemp = new SknRtgs(
									String.valueOf(row[0]), 
									String.valueOf(row[1]), 
									String.valueOf(row[2]),
									String.valueOf(row[3]),
									String.valueOf(row[4]),
									String.valueOf(row[5]),
									String.valueOf(row[6]),
									String.valueOf(row[7]),
									String.valueOf(row[8]));
							return sknRtgsTemp;
						}

						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sknRtgs;
	}
	
	
	@Override
	public void updateSknRtgs(SknRtgs sknRtgs) {
		
		
		getCurrentSession().getNamedQuery("updateSknRtgs").
		setString("REMITTANCE_NO", sknRtgs.getRemittanceNo()).
		setString("SORBOR_NO", sknRtgs.getsorborNo()).
		setString("CREDIT_ACC_NO", sknRtgs.getCreditAccNo()).
		setString("DOC_NO", sknRtgs.getDocNo()).
		executeUpdate();		
	}

	
	@Override
	public int countSearchSknRtgs(String docNo,String creditAccName, String creditAccNo, String beneficiaryBankName, String creditTrfAmount,String remittanceNo,
			String sorborNum,String transactionType, List<String> blackListPM) {
			try{
				BigInteger jml = (BigInteger) getCurrentSession().
						getNamedQuery("countSearchSknRtgs")
						.setString("DOC_NO", "%"+docNo+"%")
						.setString("CREDIT_ACC_NAME", "%"+creditAccName+"%")
						.setString("CREDIT_ACC_NO", "%"+creditAccNo+"%")
						.setString("BENEFICIARY_BANK_NAME", "%"+beneficiaryBankName+"%")
						.setString("CREDIT_TRF_AMOUNT", "%"+creditTrfAmount+"%")
						.setString("TRANSACTION_TYPE", "%"+transactionType+"%")
						.setParameterList("blackListPM", blackListPM)
						.uniqueResult();
				return jml.intValue();
			}catch(Exception e){
				e.printStackTrace();
			}
			return 0;
	}


	@Override
	public boolean isAlreadyProcessed(String docnumber) {
		BigInteger count = (BigInteger) getCurrentSession().getNamedQuery("isAlreadyProcessed")
				.setString("docNumber", docnumber)
				.uniqueResult();
		if(!PubUtil.isBigIntegerEmpty(count)){
			return true;
		}
		return false;
	}

	
	@Override
	public void updateReturFlag(String remittanceNo, String returReasonCode){
		getCurrentSession().getNamedQuery("updateReturFlag").
		setString("REMITTANCE_NO", remittanceNo).
		setString("returReasonCode", returReasonCode).
		executeUpdate();
	}
	
	
	@Override
	public SknRtgs getDetailedSknRtgsByRemittanceNo(String remittanceNo){
		SknRtgs sknRtgs = new SknRtgs();
		try {
			sknRtgs = (SknRtgs) getCurrentSession().getNamedQuery("getDetailedSknRtgsByRemittanceNo")
					.setString("REMITTANCE_NO", remittanceNo)
					.setResultTransformer(new ResultTransformer() {
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							SknRtgs sknRtgsTemp;
							sknRtgsTemp = new SknRtgs(
									String.valueOf(row[0]), 
									String.valueOf(row[1]), 
									String.valueOf(row[2]),
									String.valueOf(row[3]),
									String.valueOf(row[4]),
									String.valueOf(row[5]),
									String.valueOf(row[6]),
									String.valueOf(row[7]),
									String.valueOf(row[8]));
							return sknRtgsTemp;
						}

						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sknRtgs;
	}
	

	@Override
	public ACKDataDocument selectACKDataDocument(String documentNo) {
		ACKDataDocument aCKDataDocument = new ACKDataDocument();
		try {
			aCKDataDocument = (ACKDataDocument) getCurrentSession().getNamedQuery("SKNRTGS#selectACKDataDocument")
					.setString("documentNo", documentNo)
					.setResultTransformer(new ResultTransformer() {
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							ACKDataDocument aCKDataDocumentTemp;
							aCKDataDocumentTemp = new ACKDataDocument(
									String.valueOf(row[0]), 
									String.valueOf(row[1]), 
									String.valueOf(row[2]));
							return aCKDataDocumentTemp;
						}

						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return aCKDataDocument;
	}


	@Override
	public SknRtgs isAlreadyRetured(String remittanceNo) {
		SknRtgs sknRtgs = new SknRtgs();
		try {
			sknRtgs = (SknRtgs) getCurrentSession().getNamedQuery("isAlreadyRetured")
					.setString("REMITTANCE_NO", remittanceNo)
					.setResultTransformer(new ResultTransformer() {
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							SknRtgs sknRtgsTemp;
							sknRtgsTemp = new SknRtgs(
									String.valueOf(row[0]), 
									String.valueOf(row[1]), 
									String.valueOf(row[2]),
									String.valueOf(row[3]),
									String.valueOf(row[4]),
									String.valueOf(row[5]),
									String.valueOf(row[6]),
									String.valueOf(row[7]),
									String.valueOf(row[8]));
							return sknRtgsTemp;
						}

						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sknRtgs;
	}
	

	/**
	 
	 @SuppressWarnings("unchecked")
	public List<SknRtgs> getAllSknRtgsLazyLoading(int maxRow, int minRow) {		
		List<SknRtgs> sknRtgsList = new ArrayList<SknRtgs>();
		try{
			sknRtgsList = (List<SknRtgs>)getCurrentSession().
					getNamedQuery("getAllSknRtgsLazyLoading").
					setBigInteger("MAX_ROW", BigInteger.valueOf(maxRow)).
					setBigInteger("MIN_ROW", BigInteger.valueOf(minRow)).
					setResultTransformer(new ResultTransformer() {						
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							SknRtgs sknRtgs;
							try {
								sknRtgs = new SknRtgs(
										String.valueOf(row[0]), 
										String.valueOf(row[1]), 
										String.valueOf(row[2]),
										String.valueOf(row[3]),
										String.valueOf(row[4]),
										String.valueOf(row[5]),
										String.valueOf(row[6])
										);
								return sknRtgs;
							} catch (Exception e) {
								e.printStackTrace();
							}
							return null;
						}
						
						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).
					list();
		}catch(Exception e){
			e.printStackTrace();
		}
		return sknRtgsList;
	}
	 
	 
	 @SuppressWarnings("unchecked")
	public List<SknRtgs> getAllSknRtgs() {
		List<SknRtgs> sknRtgsList = new ArrayList<SknRtgs>();
		try{
			sknRtgsList = (List<SknRtgs>)getCurrentSession().
					getNamedQuery("getAllSknRtgs")
					.setResultTransformer(new ResultTransformer() {
						private static final long serialVersionUID = 1L;
						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							SknRtgs sknRtgs;
							sknRtgs = new SknRtgs(
									String.valueOf(row[0]), 
									String.valueOf(row[1]),
									String.valueOf(row[2]), 
									String.valueOf(row[3]),
									String.valueOf(row[4]),
									String.valueOf(row[5]),
									String.valueOf(row[6])
									
									);
									
							return sknRtgs;
						}
						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}}).list();
						
		}catch(Exception e){
			e.printStackTrace();
		}
		return sknRtgsList;
	}
	 
	 
	@Override
	public List<SknRtgs> getDetailedSknRtgsByCreditAccName(String creditAccName) {
		List<SknRtgs> sknRtgsList = new ArrayList<SknRtgs>();
		try{
			sknRtgsList = (List<SknRtgs>)getCurrentSession().
					getNamedQuery("getDetailedSknRtgsByCreditAccName").
					setString("CREDIT_ACC_NAME", "%"+creditAccName+"%").
					setResultTransformer(new ResultTransformer() {						
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							SknRtgs sknRtgs;
							try {
								sknRtgs = new SknRtgs(
										String.valueOf(row[0]), 
										String.valueOf(row[1]), 
										String.valueOf(row[2]),
										String.valueOf(row[3]),
										String.valueOf(row[4]),
										String.valueOf(row[5]),
										String.valueOf(row[6])
										);
								return sknRtgs;
							} catch (Exception e) {
								e.printStackTrace();
							}
							return null;
						}
						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).
					list();
		}catch(Exception e){
			e.printStackTrace();
		}
		return sknRtgsList;
	}
	
	
	@Override
	public List<SknRtgs>  getDetailedSknRtgsByCreditAccNo(String creditAccNo) {
		List<SknRtgs> sknRtgsList = new ArrayList<SknRtgs>();
		try{
			sknRtgsList = (List<SknRtgs>)getCurrentSession().
					getNamedQuery("getDetailedSknRtgsByCreditAccNo").
					setString("CREDIT_ACC_NO", "%"+creditAccNo+"%").
					setResultTransformer(new ResultTransformer() {						
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							SknRtgs sknRtgs;
							try {
								sknRtgs = new SknRtgs(
										String.valueOf(row[0]), 
										String.valueOf(row[1]), 
										String.valueOf(row[2]),
										String.valueOf(row[3]),
										String.valueOf(row[4]),
										String.valueOf(row[5]),
										String.valueOf(row[6])
										);
								return sknRtgs;
							} catch (Exception e) {
								e.printStackTrace();
							}
							return null;
						}
						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).
					list();
		}catch(Exception e){
			e.printStackTrace();
		}
		return sknRtgsList;
	}
	
	@Override
	public List<SknRtgs> getDetailedSknRtgsByBeneficiaryBankName(String beneficiaryBankName) {
		List<SknRtgs> sknRtgsList = new ArrayList<SknRtgs>();
		try{
			sknRtgsList = (List<SknRtgs>)getCurrentSession().
					getNamedQuery("getDetailedSknRtgsByBeneficiaryBankName").
					setString("BENEFICIARY_BANK_NAME", "%"+beneficiaryBankName+"%").
					setResultTransformer(new ResultTransformer() {						
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							SknRtgs sknRtgs;
							try {
								sknRtgs = new SknRtgs(
										String.valueOf(row[0]), 
										String.valueOf(row[1]), 
										String.valueOf(row[2]),
										String.valueOf(row[3]),
										String.valueOf(row[4]),
										String.valueOf(row[5]),
										String.valueOf(row[6]));
								return sknRtgs;
							} catch (Exception e) {
								e.printStackTrace();
							}
							return null;
						}
						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).
					list();
		}catch(Exception e){
			e.printStackTrace();
		}
		return sknRtgsList;
	}
	
	@Override
	public List<SknRtgs> getDetailedSknRtgsByCreditTrfAmount(String creditTrfAmount) {
		List<SknRtgs> sknRtgsList = new ArrayList<SknRtgs>();
		try{
			sknRtgsList = (List<SknRtgs>)getCurrentSession().
					getNamedQuery("getDetailedSknRtgsByCreditTrfAmount").
					setString("CREDIT_TRF_AMOUNT", "%"+creditTrfAmount+"%").
					setResultTransformer(new ResultTransformer() {						
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							SknRtgs sknRtgs;
							try {
								sknRtgs = new SknRtgs(
										String.valueOf(row[0]), 
										String.valueOf(row[1]), 
										String.valueOf(row[2]),
										String.valueOf(row[3]),
										String.valueOf(row[4]),
										String.valueOf(row[5]),
										String.valueOf(row[6]));
								return sknRtgs;
							} catch (Exception e) {
								e.printStackTrace();
							}
							return null;
						}
						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).
					list();
		}catch(Exception e){
			e.printStackTrace();
		}
		return sknRtgsList;
	}
	*/
	
	
}
