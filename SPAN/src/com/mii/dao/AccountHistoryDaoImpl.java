package com.mii.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.transform.ResultTransformer;

import com.mii.helpers.BasicDaoImpl;
import com.mii.models.SpanAccountHistory;

public class AccountHistoryDaoImpl extends BasicDaoImpl implements AccountHistoryDao{

	@SuppressWarnings("unchecked")
	@Override
	public List<SpanAccountHistory> showAccountHistoryGAJI(String debitaccno,
			String docDate, String inquiryType, int startRow, int endRow) {
		List<SpanAccountHistory> accountHistoryList = new ArrayList<SpanAccountHistory>();
		try{
			accountHistoryList = getCurrentSession().getNamedQuery("SpanDataAcctHistory#showAccountHistory").
					setParameter("debitaccno", debitaccno).
					setParameter("docDate", docDate).
					setParameter("inquiryType", inquiryType).
					setParameter("startRow", startRow).
					setParameter("endRow", endRow).
					setResultTransformer(new ResultTransformer() {
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg1) {

							SpanAccountHistory history;
							history = new SpanAccountHistory(String.valueOf(row[1]), 
									String.valueOf(row[2]), 
									String.valueOf(row[3]), 
									String.valueOf(row[4]), 
									String.valueOf(row[5]));
								return history;
						}
						
						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).list();
		}catch(Exception e){
			e.printStackTrace();
		}
		return accountHistoryList;
	}

	@Override
	public int countAccountHistoryGAJI(String debitaccno, String docDate,
			String inquiryType) {
		try{
			BigDecimal jml = (BigDecimal) getCurrentSession().getNamedQuery("SpanDataAcctHistory#countAccountHistory").
					setParameter("debitaccno", debitaccno).
					setParameter("docDate", docDate).
					setParameter("inquiryType", inquiryType).uniqueResult();			
			return jml.intValue();
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SpanAccountHistory> showAccountHistoryGAJIMutasi(String debitaccno,
			String docDate, int startRow, int endRow) {
		List<SpanAccountHistory> accountHistoryList = new ArrayList<SpanAccountHistory>();
		try{
			accountHistoryList = getCurrentSession().getNamedQuery("SpanDataAcctHistory#showAccountHistoryMutasi").
					setParameter("debitaccno", debitaccno).
					setParameter("docDate", docDate).
					setParameter("startRow", startRow).
					setParameter("endRow", endRow).
					setResultTransformer(new ResultTransformer() {
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg1) {

							SpanAccountHistory history;
							history = new SpanAccountHistory(String.valueOf(row[1]), 
									String.valueOf(row[2]), 
									String.valueOf(row[3]), 
									String.valueOf(row[4]), 
									String.valueOf(row[5]));
								return history;
						}
						
						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).list();
		}catch(Exception e){
			e.printStackTrace();
		}
		return accountHistoryList;
	}

	@Override
	public int countAccountHistoryGAJIMutasi(String debitaccno, String docDate) {
		try{
			BigDecimal jml = (BigDecimal) getCurrentSession().getNamedQuery("SpanDataAcctHistory#countAccountHistoryMutasi").
					setParameter("debitaccno", debitaccno).
					setParameter("docDate", docDate).uniqueResult();			
			return jml.intValue();
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

}
