package com.mii.dao;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.transform.ResultTransformer;

import com.mii.constant.Constants;
import com.mii.helpers.BasicDaoImpl;
import com.mii.models.ScheduleTask;

public class SchedulerDaoImpl extends BasicDaoImpl implements SchedulerDao{

	@Override
	public void saveTask(ScheduleTask task) {
		try{
			save(task);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ScheduleTask> getAllTaskLazyLoading(int maxRow,
			int minRow) {
		List<ScheduleTask> tasks = new ArrayList<ScheduleTask>();
		try{
			tasks = (List<ScheduleTask>)getCurrentSession().
					getNamedQuery("Scheduler#getAllScheduleLazyLoading").
					setBigInteger("MAX_ROW", BigInteger.valueOf(maxRow)).
					setBigInteger("MIN_ROW", BigInteger.valueOf(minRow)).
					setResultTransformer(new ResultTransformer() {						
						private static final long serialVersionUID = 1L;
						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							ScheduleTask task;
							task = new ScheduleTask(
										String.valueOf(row[0]), 
										String.valueOf(row[1]), 
										String.valueOf(row[2]), 
										String.valueOf(row[3]), 
										String.valueOf(row[4]), 
										String.valueOf(row[5]), 
										String.valueOf(row[6])
									);
							return task;
						}
						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).
					list();
		}catch(Exception e){
			e.printStackTrace();
		}
		return tasks;
	}

	@Override
	public int countAllTask() {
		try{
			BigInteger jml = (BigInteger) getCurrentSession().
					getNamedQuery("Scheduler#countAllRecord").uniqueResult();			
			return jml.intValue();
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public ScheduleTask getDetailedTask(String task) {
		ScheduleTask detailTask = new ScheduleTask();
		try{
			detailTask = (ScheduleTask) getCurrentSession().getNamedQuery("Scheduler#getTaskByExample")
					.setString("SCHEDULER_ID", task)					
					.uniqueResult();
		}catch(Exception e){
			e.printStackTrace();
		}
		return detailTask;
	}

	@Override
	public ScheduleTask deleteTask(String taskId) {
		ScheduleTask task = new ScheduleTask();
		try {
			task.setSchedulerId(taskId);
			delete(ScheduleTask.class, taskId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ScheduleTask> getAllValidTask() {
		List<ScheduleTask> tasks = new ArrayList<ScheduleTask>();

		List<String> listStatus = new ArrayList<String>();
						
		listStatus.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
		listStatus.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
		try{
			tasks = getCurrentSession().getNamedQuery("MUser#getAllValidTask")
					.setParameterList("status", listStatus)
					.list();
		}catch(Exception e){
			e.printStackTrace();
		}
		return tasks;
	}



	@Override
	public ScheduleTask getDetailedTaskByName(String name) {
		ScheduleTask detailTask = new ScheduleTask();
		try{
			detailTask = (ScheduleTask) getCurrentSession().getNamedQuery("Scheduler#getTaskByName")
					.setString("name", name)					
					.uniqueResult();
		}catch(Exception e){
			e.printStackTrace();
		}
		return detailTask;
	}

	@Override
	public void updateDateTime(String task, String newDate, String newTime){
		try{
			getCurrentSession().getNamedQuery("Scheduler#UpdateDateTime").setString("taskName", task).
			setString("newDate", newDate).setString("newTime", newTime).executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
