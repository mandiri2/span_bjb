package com.mii.dao;

import java.util.List;

import com.mii.helpers.BasicDao;
import com.mii.models.Recon;

public interface ReconDao extends BasicDao {
	public List<Recon> getSearchReconLazyLoading(String caName, String settlementDate, String reconTime, String billingCode,
			String ntb, String ntpn, String noSakti,
			int maxRow, int minRow);
	public int countSearchRecon(String caName, String settlementDate, String reconTime, String billingCode,
			String ntb, String ntpn, String noSakti);
}
