package com.mii.dao;

import java.util.ArrayList;
import java.util.List;

import com.mii.helpers.BasicDaoImpl;
import com.mii.models.ReportDNPM;

public class ReportDNPDAOImpl extends BasicDaoImpl implements ReportDNPDao {
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ReportDNPM> getReportDNP(String tglbukuS,String tahunDateS,String currencyS) {
		List<ReportDNPM> list = new ArrayList<ReportDNPM>();
		try {
			list = (List<ReportDNPM>) getCurrentSession().getNamedQuery("Report#getDNP")
					.setString("tglbuku", tglbukuS)
					.setString("tahunDate", tahunDateS)
//					.setString("currency", currencyS)
					.list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@Override
	public String getPathFile(String paramNamePF) {
		
		String pathFile = null;
		try{
			pathFile =  (String) getCurrentSession().getNamedQuery("Report#getPathFileDNP")
					.setString("pathfile", paramNamePF)
					.uniqueResult();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return pathFile;
	}
	
	@Override
	public String getKodeBank(String paramNameKB) {
		
		String kodeBank = null;
		try{
			kodeBank =  (String) getCurrentSession().getNamedQuery("Report#getKodeBankDNP")
					.setString("kodebank", paramNameKB)
					.uniqueResult();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return kodeBank;
	}

}
