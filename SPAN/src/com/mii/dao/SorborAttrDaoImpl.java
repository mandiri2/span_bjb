package com.mii.dao;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.transform.ResultTransformer;

import com.mii.helpers.BasicDaoImpl;
import com.mii.models.SorborAttr;

public class SorborAttrDaoImpl extends BasicDaoImpl implements SorborAttrDao {

	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SorborAttr> getListSorborAttr(List<String> docNo){
		List<SorborAttr> sorborAttrList = new ArrayList<SorborAttr>();
		try{
			sorborAttrList = (List<SorborAttr>)getCurrentSession().
					getNamedQuery("getListSorborAttr")
					.setParameterList("DOC_NO", docNo)
					.setResultTransformer(new ResultTransformer() {						
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							SorborAttr sorborAttr;
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
							try {
								sorborAttr = new SorborAttr(
										String.valueOf(row[0]), 
										String.valueOf(row[1]), 
										String.valueOf(row[2]),
										String.valueOf(row[3]),
										String.valueOf(row[4]),
										sdf.parse(String.valueOf(row[5])),
										sdf.parse(String.valueOf(row[6]))
										);
								return sorborAttr;
							} catch (Exception e) {
								e.printStackTrace();
							}
							return null;
						}
						
						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).
					list();
		}catch(Exception e){
			e.printStackTrace();
		}
		return sorborAttrList;
	}
	
	
	@Override
	public SorborAttr getSorborAttrById(String docNo){
		SorborAttr sorborAttr = new SorborAttr();
		try {
			sorborAttr = (SorborAttr) getCurrentSession().getNamedQuery("getSorborAttrById")
					.setString("DOC_NO", docNo)				
					.setResultTransformer(new ResultTransformer() {
						private static final long serialVersionUID = 1L;
						SorborAttr sorborAttr;
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
						Date payDate;
						Date sorborDate;
						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							try{
								payDate=sdf.parse(String.valueOf(row[5]));
								sorborDate=sdf.parse(String.valueOf(row[6]));
							}catch(Exception e){
								
							}	
							sorborAttr = new SorborAttr(
										String.valueOf(row[0]), 
										String.valueOf(row[1]),
										String.valueOf(row[2]),
										String.valueOf(row[3]),
										String.valueOf(row[4]),
										payDate,
										sorborDate
										);
							sorborAttr.setBankCode(String.valueOf(row[7]));
								return sorborAttr;
							
						}
						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).uniqueResult();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sorborAttr;
	}
	
	
	@Override
	public int countListSorborAttr(List<String> docNo) {
			try{
				BigInteger jml = (BigInteger) getCurrentSession().
						getNamedQuery("countListSorborAttr")
						.setParameter("DOC_NO", docNo)
						.uniqueResult();			
				return jml.intValue();
			}catch(Exception e){
				e.printStackTrace();
			}
			return 0;
	}
	
	@Override
	public int getNextValFileSeq() {
			try{
				BigInteger jml = (BigInteger) getCurrentSession().
						getNamedQuery("getNextValFileSeq")
						.uniqueResult();			
				return jml.intValue();
			}catch(Exception e){
				e.printStackTrace();
			}
			return 0;
	}
	
	@Override
	public void setValFileSeq() {
		getCurrentSession().getNamedQuery("setValFileSeq");
	}
	
	
}
