package com.mii.dao;

import java.util.Date;
import java.util.List;

import com.mii.helpers.BasicDao;
import com.mii.models.UploadBPSHistory;

public interface UploadBPSHistoryDao extends BasicDao {

	public List<UploadBPSHistory> getSearchUploadBPSHistoryLazyLoading(String fileName, Date uploadDate, String uploadBy, String status, int maxRow, int minRow);
	public int countSearchUploadBPSHistory(String fileName, Date uploadDate, String uploadBy, String status);
	public void saveHistory(UploadBPSHistory obj) ;
}
