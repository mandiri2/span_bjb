package com.mii.dao;

import java.util.List;

import com.mii.helpers.BasicDao;
import com.mii.models.Rtgs;
import com.mii.models.User;

public interface RtgsDao extends BasicDao {
	public List<Rtgs> getRtgsLazyLoadingByCategory(int maxRow, int minRow, String transactionId, String transactionDate, String bppxName, String state, String referenceNumber, String response, String responseDesc);	
	public int countRtgsByCategory(String transactionId, String transactionDate, String bppxName, String state, String referenceNumber, String response, String responseDesc);
	public Rtgs getDetailedRtgs(String rtgs);
	public List<Rtgs> getRefNo(String td);
}
