package com.mii.dao;

import com.mii.helpers.BasicDao;
import com.mii.is.io.SpanRetryOrReturInput;
import com.mii.is.io.SpanRetryOrReturOutput;
import com.mii.models.SPANDataValidationErrorStatusModel;

public interface ToBeProcessDao extends BasicDao{
	public String getSequenceNo();
	public String getBatchIdSeq();
	public SPANDataValidationErrorStatusModel selectSPANDataValidationErrorStatus(String filename);
	public SpanRetryOrReturOutput spanRetryOrRetur(SpanRetryOrReturInput input);
}
