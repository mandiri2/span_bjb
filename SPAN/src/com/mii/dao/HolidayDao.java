package com.mii.dao;

import java.util.List;

import com.mii.helpers.BasicDao;
import com.mii.models.Holiday;
import com.mii.models.User;

public interface HolidayDao extends BasicDao {
	public void saveHoliday(Holiday holiday);
	public List<Holiday> getHolidayLazyLoadingByCategory(int maxPerPage, int startingAt, String holidayId, String holidayName, String holidayDate, List<String> status);
	public Integer countHolidayByCategory(String holidayId, String holidayName, String holidayDate, List<String> status);
	public Holiday getDetailedHoliday(String holidayId);
	public Holiday getDetailedHolidayByHolidayName(String holidayName);
	public Holiday deleteHoliday(String holidayId);
}
