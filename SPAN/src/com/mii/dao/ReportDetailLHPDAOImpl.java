package com.mii.dao;

import java.util.ArrayList;
import java.util.List;

import com.mii.helpers.BasicDaoImpl;
import com.mii.models.ReportDetailLHPM;

public class ReportDetailLHPDAOImpl extends BasicDaoImpl implements ReportDetailLHPDao {
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ReportDetailLHPM> getReportDetailLHP(String tglbukuS,String tahunDateS,String currencyS) {
		List<ReportDetailLHPM> list = new ArrayList<ReportDetailLHPM>();
		try {
			list = (List<ReportDetailLHPM>) getCurrentSession().getNamedQuery("Report#getDetailLHP")
					.setString("tglbuku", tglbukuS)
					.setString("tahunDate", tahunDateS)
//					.setString("currency", currencyS)
					.list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
}
