package com.mii.dao;

import java.util.List;

import com.mii.helpers.BasicDao;
import com.mii.is.io.helper.DetailsOutputList;
import com.mii.is.io.helper.DownloadErrorResponse;
import com.mii.is.io.helper.OutputDetails;
import com.mii.is.io.helper.ReturCodes;
import com.mii.is.io.helper.SPANSummaries;
import com.mii.is.io.helper.SelectFNDataValidationOutput;
import com.mii.is.io.helper.StatusDetailOutput;
import com.mii.models.SPANDataValidation;

public interface SPANDataValidationDao  extends BasicDao{
	/*public List<SPANDataValidation> getAll();
	public List<SPANDataValidation> getAllLazyLoading(int maxRow, int minRow);
	public int countAllValidation();
	public List<SPANDataValidation> searchAllLazyLoading(int maxRow, int minRow, String docDate);
	public int countSearchValidation(String docDate);*/
	
	public SPANDataValidation getSPANDataValidationByFilename(String fileName);
	
	//Menu Void Depkeu//
	public void updateUnvoidSpanDataValidation(String totalAmount, String totalRecord, String fileName, String procStatus, String voidFlag);
	public void updateSpanDataValidationWhenVoid(int recordOri, int amountOri, String fileName, boolean voidFileFlag);
	//New Mechanism Void//
	public void updateSpanDataValidationWhenNewVoid(int recordOri, int amountOri, String fileName, boolean voidFileFlag);
	
	//Menu Monitoring File Reject//
	public List<SPANDataValidation> selectSpanDataValidationLateGAJILazyLoading(String debitAccount, String transactionFlag, int maxRow, int minRow);
	public int countSpanDataValidationLateGAJI(String debitAccount, String transactionFlag);
	public void updateLateFileGaji(List<String> documentNumber);
	public void updateFlagLateFile(List<String> documentNumber);
	public void updateSpanDataValidationSp2dno (String sp2dno);
	
	// getSPANSummaries
	public List<SPANSummaries> selectSPANDataValidationStatusParams(List<String> procStatus, String dateStart, String dateEnd, String accountType);
	public List<SPANSummaries> getSPANSummaries(List<String> procStatus, String legStatus, String tableResponse, String noRekening, String startDate, String endDate);
	public String getReturDataDetails(String filename);
	public String getReturDataDetailsACK(String fileName);
	public List<OutputDetails> selectReturDetails(String legStatus, String tableResponse, String procStatus, String trxType, String noRekening);
	public List<OutputDetails> selectRetryDetails(String legStatus, String tableResponse, String procStatus, String trxType, String noRekening, String addingCondition);
	public OutputDetails selectSuccessDetails(String filename, String legStatus);
	public StatusDetailOutput checkSuccessStatus(String fileName);
	public SelectFNDataValidationOutput selectFNDataValidation(String fileName);
	public int getODBCTotalProcess(String fileName, String legStatus);
	public List<DetailsOutputList> getSPANSummaryDetails(String filename,
			String additionalCondition, String legStatus,
			String retryCondition, String processCondition);
	public List<StatusDetailOutput> getRetryOrReturStatus(String filename,
			int totalProcess);
	public List<DetailsOutputList> getHoldRecord(String messageId);
	public String getLegStatus(String filename);
	public List<SPANSummaries> selectSPANDataValidation(String filename);
	public List<ReturCodes> getReturCode(String rcId);
	
	
	//toBeProcess//
	public void updateProcStatusAndDescriptionByFilename(String procStatus, String description, String fileName);
	public void updateProcStatusAndDescriptionByFilenameForceACK(String procStatus, String description, String fileName);
	public List<DownloadErrorResponse> getDownloadErrorResponse(String paymentMethods, String filename);
	public void updateForceAck(String filename);
	public void updateSpanHostResponse(String responseCode, String errorCode, String batchid, String trxdetailid);
	
	//Summaries
	public String getPostingAmount(String documentDate, String debitAccount);
	public SPANDataValidation getSPANDataValidationByFilenameForSummaryDetail(String fileName);

	SPANDataValidation getOneSPANDataValidation(String filename);
}
