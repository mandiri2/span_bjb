package com.mii.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.transform.ResultTransformer;

import com.mii.constant.Constants;
import com.mii.helpers.BasicDaoImpl;
import com.mii.models.Branch;

public class BranchDAOImpl extends BasicDaoImpl implements BranchDao {

		@Override
		public void saveBranch(Branch branch) {
			// TODO Auto-generated method stub
			try {
				
				save(branch);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		public List<Branch> getBranchLazyLoadingByCategory(int maxPerPage,
				int startingAt, String branchId, String branchCode,
				String branchName, List<String> status) {
			// TODO Auto-generated method stub
			List<Branch> listBranch = new ArrayList<Branch>();
			try {
				listBranch = (List<Branch>) getCurrentSession()
						.getNamedQuery("MBranch#getBranchLazyLoadingByCategory")
						.setString("branchId", "%" + branchId + "%")
						.setString("branchCode", "%" + branchCode + "%")
						.setString("branchName", "%" + branchName + "%")
						.setParameterList("STATUS", status)
						.setString("MAX_ROW", String.valueOf(maxPerPage))
						.setString("MIN_ROW", String.valueOf(startingAt))
						.setResultTransformer(new ResultTransformer() {

							private static final long serialVersionUID = 1L;

							@Override
							public Object transformTuple(Object[] row, String[] arg1) {
								Branch branch;
								branch = new Branch(String.valueOf(row[0])
											,String.valueOf(row[1])
											,String.valueOf(row[2])
											,String.valueOf(row[3])
											,String.valueOf(row[4])
										);
								return branch;
							}

							@Override
							public List transformList(List arg0) {
								return arg0;
							}
						}).list();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return listBranch;
		}

		@Override
		public Integer countBranchByCategory(String branchId, String branchCode,
				String branchName, List<String> status) {
			// TODO Auto-generated method stub
			try {
				BigDecimal jml = (BigDecimal) getCurrentSession()
						.getNamedQuery("MBranch#countRecordByCategory")
							.setString("branchId", "%" + branchId + "%")
						.setString("branchCode", "%" + branchCode + "%")
						.setString("branchName", "%" + branchName + "%")
						.setParameterList("STATUS", status)
						.uniqueResult();
				return jml.intValue();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return 0;
		}

		@Override
		public Branch getDetailedBranch(String branchId) {
			// TODO Auto-generated method stub
			Branch detailBranch = new Branch();
			try{
				detailBranch = (Branch) getCurrentSession().getNamedQuery("MBranch#getBranchByExample")
						.setString("branchId", branchId)					
						.uniqueResult();
			}catch(Exception e){
				e.printStackTrace();
			}
			return detailBranch;
		}

		@Override
		public Branch deleteBranch(String branchId) {
			// TODO Auto-generated method stub
			Branch branch = new Branch();
			try {
				branch.setBranchId(branchId);
				delete(Branch.class, branchId);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		public List<Branch> getAllBranch() {
			// TODO Auto-generated method stub
			List<Branch> branchs = new ArrayList<Branch>();
			try{
				branchs = getCurrentSession().getNamedQuery("MBranch#getAllBranch")				
						.list();
			}catch(Exception e){
				e.printStackTrace();
			}
			return branchs;
		}

		@Override
		public Branch getBranchByBranchCode(String branchCode) {
			// TODO Auto-generated method stub
			Branch detailBranch = new Branch();
			try{
				detailBranch = (Branch) getCurrentSession().getNamedQuery("MBranch#getBranchByBranchCode")
						.setString("branchCode", branchCode)					
						.uniqueResult();
			}catch(Exception e){
				e.printStackTrace();
			}
			return detailBranch;
		}

		@Override
		public List<Branch> getAllValidBranch() {
			// TODO Auto-generated method stub
			// TODO Auto-generated method stub
			
						List<Branch> branchs = new ArrayList<Branch>();

						List<String> listStatus = new ArrayList<String>();
										
						listStatus.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
						listStatus.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
						
						try{
							branchs = getCurrentSession().getNamedQuery("MBranch#getAllValidBranch")	
									.setParameterList("status", listStatus)			
									.list();
						}catch(Exception e){
							e.printStackTrace();
						}
						return branchs;
		}


	}


