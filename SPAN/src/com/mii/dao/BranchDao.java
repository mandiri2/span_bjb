package com.mii.dao;

import java.util.List;

import com.mii.helpers.BasicDao;
import com.mii.models.Branch;

public interface BranchDao extends BasicDao {
	public void saveBranch(Branch branch);
	public List<Branch> getBranchLazyLoadingByCategory(int maxPerPage, int startingAt, String branchId, String branchCode, String branchName, List<String> status);
	public Integer countBranchByCategory(String branchId, String branchCode, String branchName, List<String> status);
	public Branch getDetailedBranch(String branchId);
	public Branch deleteBranch(String branchId);
	public List<Branch> getAllBranch();
	public List<Branch> getAllValidBranch();
	public Branch getBranchByBranchCode(String branchCode);
}
