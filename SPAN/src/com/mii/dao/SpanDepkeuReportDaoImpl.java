package com.mii.dao;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.transform.ResultTransformer;

import com.mii.helpers.BasicDaoImpl;
import com.mii.models.SpanDepkeuReport;

public class SpanDepkeuReportDaoImpl extends BasicDaoImpl implements SpanDepkeuReportDao{

	@SuppressWarnings("unchecked")
	@Override
	public List<SpanDepkeuReport> getSp2dReject(String docDateStart, String docDateEnd,int minRow,int maxRow) {
		List<SpanDepkeuReport> spanDepkeuReportList = new ArrayList<SpanDepkeuReport>();
		try{
			spanDepkeuReportList = (List<SpanDepkeuReport>)getCurrentSession().
					getNamedQuery("getSp2dReject")
					.setString("DOC_DATE_START",docDateStart)
					.setString("DOC_DATE_END",docDateEnd)
					.setBigInteger("MAX_ROW", BigInteger.valueOf(maxRow))
					.setBigInteger("MIN_ROW", BigInteger.valueOf(minRow))
					.setResultTransformer(new ResultTransformer() {						
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							SpanDepkeuReport spanDepkeuReport;
							try {
								spanDepkeuReport = new SpanDepkeuReport(
										String.valueOf(row[0]), 
										String.valueOf(row[1]), 
										String.valueOf(row[2]),
										String.valueOf(row[3]),
										String.valueOf(row[4]),
										String.valueOf(row[5])
										);
								return spanDepkeuReport;
							} catch (Exception e) {
								e.printStackTrace();
							}
							return null;
						}
						
						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					})
					.list();
		}catch(Exception e){
			e.printStackTrace();
		}
		return spanDepkeuReportList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SpanDepkeuReport> getSp2dRejectApprovedByKemenkeu(String docDateStart, String docDateEnd,int minRow,int maxRow) {
		List<SpanDepkeuReport> spanDepkeuReportList = new ArrayList<SpanDepkeuReport>();
		try{
			spanDepkeuReportList = (List<SpanDepkeuReport>)getCurrentSession().
					getNamedQuery("getSp2dRejectApprovedByKemenkeu")
					.setString("DOC_DATE_START",docDateStart)
					.setString("DOC_DATE_END",docDateEnd)
					.setBigInteger("MAX_ROW", BigInteger.valueOf(maxRow))
					.setBigInteger("MIN_ROW", BigInteger.valueOf(minRow))
					.setResultTransformer(new ResultTransformer() {						
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							SpanDepkeuReport spanDepkeuReport;
							try {
								spanDepkeuReport = new SpanDepkeuReport(
										String.valueOf(row[0]), 
										String.valueOf(row[1]), 
										String.valueOf(row[2]),
										String.valueOf(row[3]),
										String.valueOf(row[4]),
										String.valueOf(row[5])
										);
								return spanDepkeuReport;
							} catch (Exception e) {
								e.printStackTrace();
							}
							return null;
						}
						
						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).
					list();
		}catch(Exception e){
			e.printStackTrace();
		}
		return spanDepkeuReportList;
	}

	@Override
	public int countSp2dReject(String docDateStart, String docDateEnd) {
		try{
			BigInteger jml = (BigInteger) getCurrentSession().
					getNamedQuery("countSp2dReject")
					.setString("DOC_DATE_START",docDateStart)
					.setString("DOC_DATE_END",docDateEnd)
					.uniqueResult();			
			return jml.intValue();
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public int countSp2dRejectApprovedByKemenkeu(String docDateStart, String docDateEnd) {
		try{
			BigInteger jml = (BigInteger) getCurrentSession().
					getNamedQuery("countSp2dRejectApprovedByKemenkeu")
					.setString("DOC_DATE_START",docDateStart)
					.setString("DOC_DATE_END",docDateEnd)
					.uniqueResult();			
			return jml.intValue();
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

}
