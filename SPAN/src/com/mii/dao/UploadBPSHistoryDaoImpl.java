package com.mii.dao;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.transform.ResultTransformer;

import com.mii.helpers.BasicDaoImpl;
import com.mii.models.UploadBPSHistory;

public class UploadBPSHistoryDaoImpl extends BasicDaoImpl implements UploadBPSHistoryDao {


	public void saveHistory(UploadBPSHistory obj) {
		// TODO Auto-generated method stub
		BigDecimal id = (BigDecimal) getCurrentSession().
				getNamedQuery("TUploadBPSHistory#selectIdSeq").uniqueResult();
		obj.setId(id);
		super.save(obj);
	}
	
	@Override
	public List<UploadBPSHistory> getSearchUploadBPSHistoryLazyLoading(
			String fileName, Date uploadDate, String uploadBy,  String status, int maxRow,
			int minRow) {
		List<UploadBPSHistory> uploadBPSHistoryList = new ArrayList<UploadBPSHistory>();
		try{
			
			DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
			String date = "";
			if ((uploadDate != null) && (!uploadDate.equals("")))
				date = df.format(uploadDate);
			
			uploadBPSHistoryList = (List<UploadBPSHistory>)getCurrentSession().
					getNamedQuery("TUploadBPSHistory#getSearchUploadBPSHistoryLazyLoading")
					.setString("FILE_NAME", "%"+fileName+"%")
					.setString("UPLOAD_DATE", "%"+date+"%")
					.setString("UPLOAD_BY", "%"+uploadBy+"%")
					.setString("STATUS", "%"+status+"%")
					.setString("MAX_ROW", String.valueOf(maxRow))
					.setString("MIN_ROW", String.valueOf(minRow))
					.setResultTransformer(new ResultTransformer() {						
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
							UploadBPSHistory uploadBPSHistory;
							try {
								Date uploadDate = null;
								if (row[3]!=null) {
									uploadDate = sdf.parse(String.valueOf(row[3]));
								}
								uploadBPSHistory = new UploadBPSHistory(
										new BigDecimal(String.valueOf(row[0])), 
										String.valueOf(row[1]), 
										new BigDecimal(String.valueOf(row[2])),
										uploadDate,
										String.valueOf(row[4]),
										String.valueOf(row[5]));
								return uploadBPSHistory;
							} catch (ParseException e) {
								e.printStackTrace();
							}
							return null;
						}
						
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).
					list();
		}catch(Exception e){
			e.printStackTrace();
		}
		return uploadBPSHistoryList;
	}

	@Override
	public int countSearchUploadBPSHistory(String fileName, Date uploadDate,
			String uploadBy, String status) {
		try{

			DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
			String date = "";
			if ((uploadDate != null) && (!uploadDate.equals("")))
				date = df.format(uploadDate);
			BigDecimal jml = (BigDecimal) getCurrentSession().
					getNamedQuery("TUploadBPSHistory#countSearchUploadBPSHistoryLazyLoading")
					.setString("FILE_NAME", "%"+fileName+"%")
					.setString("UPLOAD_DATE", "%"+date+"%")
					.setString("UPLOAD_BY", "%"+uploadBy+"%")
					.setString("STATUS", "%"+status+"%")
					.uniqueResult();			
			return jml.intValue();
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
	}

	
}
