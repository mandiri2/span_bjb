package com.mii.dao;

import java.util.List;

import com.mii.helpers.BasicDao;
import com.mii.models.Approval;
import com.mii.models.ApprovalLog;

public interface ApprovalDao extends BasicDao {

	public List<Approval> getApprovalByParameter(String paramater, String approverId);
	public List<Approval> getApprovalByParameterSearch(String approverId, String parameterId,String parameterName,List<String> searchParameter,List<String> searchAction, String branchId);
	public void saveApproval(Approval approval);
	public void saveApprovalLog(Approval Approval, String statusCode, String statusMessage);
	public void updateApprovalNewObjectByParameterId(byte[] oldByteObject, String parameterId);
	public void deleteApproval(List<Approval> approvals);
	public void deleteApproval(Approval approval);
	public List<Approval> getAllApprovalLazyLoading(int maxRow, int minRow, String approverId, String parameterId,String parameterName,List<String> searchParameter,List<String> searchAction, String branchId);
	public List<Approval> getApprovalByParameterId(String parameterId);
	public int countAllApproval(String approverId, String parameterId, String parameterName, List<String> searchParameter,List<String> searchAction, String branchId);
	public void assignApprover(String approverId);
	public void removeApprover(List<String> listApproverId);
	public void removeAllApprover();
}
