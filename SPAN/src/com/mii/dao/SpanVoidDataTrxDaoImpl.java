package com.mii.dao;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.transform.ResultTransformer;

import com.mii.helpers.BasicDaoImpl;
import com.mii.models.SpanSp2dVoidLog;
import com.mii.models.SpanVoidDataTrx;

public class SpanVoidDataTrxDaoImpl extends BasicDaoImpl implements
		SpanVoidDataTrxDao {

	@Override
	public void InsertSPANDataVoidTransaction(SpanVoidDataTrx spanVoidDataTrx) {
		getCurrentSession()
				.getNamedQuery("SpanVoidDataTrx#InsertSPANDataVoidTransaction")
				.setString("fileName", spanVoidDataTrx.getFileName())
				.setString("documentNo", spanVoidDataTrx.getDocumentNo())
				.setString("documentDate", spanVoidDataTrx.getDocumentDate())
				.setString("beneficiaryName",
						spanVoidDataTrx.getBeneficiaryName())
				.setString("beneficiaryBankCode",
						spanVoidDataTrx.getBeneficiaryBankCode())
				.setString("beneficiaryBank",
						spanVoidDataTrx.getBeneficiaryBank())
				.setString("beneficiaryAccount",
						spanVoidDataTrx.getBeneficiaryAccount())
				.setString("amount", spanVoidDataTrx.getAmount())
				.setString("description", spanVoidDataTrx.getDescription())
				.setString("agentBankCode", spanVoidDataTrx.getAgentBankCode())
				.setString("agentBankAccountNo",
						spanVoidDataTrx.getAgentBankAccountNo())
				.setString("agentBankAccountName",
						spanVoidDataTrx.getAgentBankAccountName())
				.setString("paymentMethod", spanVoidDataTrx.getPaymentMethod())
				.setString("sp2dCount", spanVoidDataTrx.getSp2dCount())
				.executeUpdate();
	}

	@Override
	public String selectSpanVoidDataTrxAmountByDocNumber(String documentNumber) {
		String amount = (String) getCurrentSession()
				.getNamedQuery(
						"SpanVoidDataTrx#selectSpanVoidDataTrxAmountByDocNumber")
				.setString("documentNo", documentNumber).uniqueResult();
		return amount;
	}

	@Override
	public void updateDao(String sql) {
		getCurrentSession().createSQLQuery(sql).executeUpdate();
	}

	@Override
	public void deleteSPANVoidDataTransaction(String filename,
			String documentNumber) {
		getCurrentSession()
				.getNamedQuery(
						"SPANMonitoringVoidDepKeu#deleteSPANVoidDataTransaction")
				.setString("fileName", filename)
				.setString("documentNo", documentNumber).executeUpdate();

	}

	@Override
	public int countVoid(String status, String filename, String sp2dNumber) {
		sp2dNumber = sp2dNumber + "%";
		try {
			BigInteger jml = (BigInteger) getCurrentSession()
					.getNamedQuery("SPANMonitoringVoidDepKeu#countVoid")
					.setString("status", status)
					.setString("filename", filename)
					.setString("sp2dnumber", sp2dNumber).uniqueResult();
			return jml.intValue();
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	@Override
	public void insertSpanSp2dVoidLogs(List<SpanSp2dVoidLog> spanSp2dVoidLogs) {
		// TODO Auto-generated method stub

		/*getCurrentSession()
				.getNamedQuery("SPANMonitoringVoidDepKeu#insertSpanSp2dVoidLog")
				.setString("fileName", filename).setString("sp2dNo", sp2dNo)
				.setString("datetime_void", datetime_void).executeUpdate();

		SpanSp2dVoidLog spanSp2dVoidLog = new SpanSp2dVoidLog();
		spanSp2dVoidLog.setFilename(filename);
		spanSp2dVoidLog.setSp2dNo(sp2dNo);
		spanSp2dVoidLog.setDatetimeVoid(datetime_void);*/

		Session session = getCurrentSession().getSessionFactory().openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			int seq = 1;
			for (SpanSp2dVoidLog data : spanSp2dVoidLogs) {
				session.save(data);
				if (seq % 50 == 0) {
					session.flush();
					session.clear();
				}
				seq++;
			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			// session.close();
		}
		return;
	}

	@Override
	public List<SpanSp2dVoidLog> viewSpanSp2dVoidLogs(String filenameFilter,
			String sp2dNoFilter, int min, int max) {
		// TODO Auto-generated method stub
		List<SpanSp2dVoidLog> result = new ArrayList<>();

		result = getCurrentSession()
				.getNamedQuery("SPANMonitoringVoidDepKeu#viewSpanSp2dVoidLogs")
				.setString("filenameFilter", filenameFilter)
				.setString("sp2dNoFilter", sp2dNoFilter).setInteger("min", min)
				.setInteger("max", max).list();

		return result;
	}

	@Override
	public int countSpanSp2dVoidLogs(String filenameFilter, String sp2dNoFilter) {
		// TODO Auto-generated method stub
		BigInteger result = BigInteger.ZERO;

		result = (BigInteger) getCurrentSession()
				.getNamedQuery("SPANMonitoringVoidDepKeu#countSpanSp2dVoidLogs")
				.setString("filenameFilter", filenameFilter)
				.setString("sp2dNoFilter", sp2dNoFilter).uniqueResult();

		return result.intValue();
	}

	@Override
	public List<SpanVoidDataTrx> getAutoSendAckVoidListByDocumentList(
			List<String> documentNoList) {
		// TODO Auto-generated method stub
		List<SpanVoidDataTrx> result = new ArrayList<SpanVoidDataTrx>();
		result = getCurrentSession()
				.getNamedQuery(
						"MonitoringVoidDepkeuHandler#getAutoSendAckVoidListByDocumentList")
				.setParameterList("documentNoList", documentNoList).setResultTransformer(new ResultTransformer() {
					
					@Override
					public Object transformTuple(Object[] arg0, String[] arg1) {
						// TODO Auto-generated method stub
						SpanVoidDataTrx spanVoidDataTrx = new SpanVoidDataTrx();
						spanVoidDataTrx.setFileName(String.valueOf(arg0[0]));
						spanVoidDataTrx.setDocumentNo(String.valueOf(arg0[1]));
						spanVoidDataTrx.setDocumentDate(String.valueOf(arg0[2]));
						return spanVoidDataTrx;
					}
					
					@Override
					public List transformList(List arg0) {
						// TODO Auto-generated method stub
						return arg0;
					}
				}).list();
		return result;
	}
	
	public List<String> getDocumentNoBySp2dNo(
			String sp2dNo) {
		List<String> result = (List<String>) getCurrentSession()
				.getNamedQuery("MonitoringVoidDepkeuHandler#getDocumentNoBySp2dNo")
				.setString("sp2dNo", sp2dNo)
				.list();
		return result;
	}
}
