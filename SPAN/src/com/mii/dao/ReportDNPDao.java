package com.mii.dao;

import java.util.List;

import com.mii.helpers.BasicDao;
import com.mii.models.ReportDNPM;


public interface ReportDNPDao extends BasicDao {
	

	public List<ReportDNPM> getReportDNP(String tglbukuS,String tahunDateS,String currencyS);
	public String getPathFile(String paramNamePF);
	public String getKodeBank(String paramNameKB);
}
