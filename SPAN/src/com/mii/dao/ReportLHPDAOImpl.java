package com.mii.dao;

import java.util.ArrayList;
import java.util.List;

import com.mii.helpers.BasicDaoImpl;
import com.mii.models.ReportLHPM;

public class ReportLHPDAOImpl extends BasicDaoImpl implements ReportLHPDao {
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ReportLHPM> getReportLHP(String tglbukuS,String tahunDateS,String currencyS) {
		List<ReportLHPM> list = new ArrayList<ReportLHPM>();
		try {

			list = (List<ReportLHPM>) getCurrentSession().getNamedQuery("Report#getLHP")
					.setString("tglbuku", tglbukuS)
					.setString("tahunDate", tahunDateS)
//					.setString("currency", currencyS)
					.list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@Override
	public String getPathFile(String paramNamePF) {
		
		String pathFile = null;
		try{
			pathFile =  (String) getCurrentSession().getNamedQuery("Report#getPathFileLHP")
					.setString("pathfile", paramNamePF)
					.uniqueResult();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return pathFile;
	}
	
	@Override
	public String getKodeBank(String paramNameKB) {
		
		String kodeBank = null;
		try{
			kodeBank =  (String) getCurrentSession().getNamedQuery("Report#getKodeBankLHP")
					.setString("kodebank", paramNameKB)
					.uniqueResult();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return kodeBank;
	}
	
}
