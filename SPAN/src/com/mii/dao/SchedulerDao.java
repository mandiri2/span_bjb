package com.mii.dao;

import java.util.List;

import com.mii.helpers.BasicDao;
import com.mii.models.ScheduleTask;
//import com.mii.models.User;

public interface SchedulerDao extends BasicDao {
	public List<ScheduleTask> getAllTaskLazyLoading(int maxPerPage, int startingAt);
	public int countAllTask();
	public void saveTask(ScheduleTask task);
	public ScheduleTask getDetailedTask(String task);
	public ScheduleTask getDetailedTaskByName(String name);
	public ScheduleTask deleteTask(String task);
	public List<ScheduleTask> getAllValidTask();
	public void updateDateTime(String task, String newDate, String newTime);
}
