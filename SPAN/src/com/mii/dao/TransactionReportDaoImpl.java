package com.mii.dao;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.transform.ResultTransformer;

import com.mii.helpers.BasicDaoImpl;
import com.mii.is.util.PubUtil;
import com.mii.models.InternalReportBJB;
import com.mii.models.SpanAccountHistory;

public class TransactionReportDaoImpl extends BasicDaoImpl implements
		TransactionReportDao {

	@Override
	public List<SpanAccountHistory> showAccountHistoryGAJI(String debitaccno,
			String docDate, String inquiryType, int startRow, int endRow) {
		List<SpanAccountHistory> accountHistoryList = new ArrayList<SpanAccountHistory>();
		try {
			accountHistoryList = getCurrentSession()
					.getNamedQuery("SpanDataAcctHistory#showAccountHistory")
					.setParameter("debitaccno", debitaccno)
					.setParameter("docDate", docDate)
					.setParameter("inquiryType", inquiryType)
					.setParameter("startRow", startRow)
					.setParameter("endRow", endRow)
					.setResultTransformer(new ResultTransformer() {
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg1) {

							SpanAccountHistory history;
							history = new SpanAccountHistory(String
									.valueOf(row[1]), String.valueOf(row[2]),
									String.valueOf(row[3]), String
											.valueOf(row[4]), String
											.valueOf(row[5]));
							return history;
						}

						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return accountHistoryList;
	}

	@Override
	public int countAccountHistoryGAJI(String debitaccno, String docDate,
			String inquiryType) {
		try {
			BigDecimal jml = (BigDecimal) getCurrentSession()
					.getNamedQuery("SpanDataAcctHistory#countAccountHistory")
					.setParameter("debitaccno", debitaccno)
					.setParameter("docDate", docDate)
					.setParameter("inquiryType", inquiryType).uniqueResult();
			return jml.intValue();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public List<InternalReportBJB> getTransactionReport(Date startDocDate,
			Date endDocDate, String accountCodeNumber, String paymentMethod,
			String transactionStatus, int startRow, int endRow) {
		List<InternalReportBJB> internalReportBJBs = new ArrayList<InternalReportBJB>();
		try {
			String startDate = new SimpleDateFormat("yyyy-MM-dd")
					.format(startDocDate);
			String endDate = new SimpleDateFormat("yyyy-MM-dd")
					.format(endDocDate);
			System.out.println("Start Date : " + startDate);
			System.out.println("End Date : " + endDate);
			internalReportBJBs = (List<InternalReportBJB>) getCurrentSession()
					.getNamedQuery("TransactionReport#getTransactionReport")
					.setParameter("startDocDate", startDate)
					.setParameter("endDocDate", endDate)
					.setParameter("accountCodeNumber", "%"+accountCodeNumber+"%")
					.setParameter("paymentMethod", "%"+paymentMethod+"%")
					.setParameter("transactionStatus", "%"+transactionStatus+"%")
					.setParameter("startRow", startRow)
					.setParameter("endRow", endRow)
					.setResultTransformer(new ResultTransformer() {
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg1) {

							InternalReportBJB internalReportBJB;
							internalReportBJB = new InternalReportBJB(String
									.valueOf(row[0]), String.valueOf(row[1]),
									String.valueOf(row[2]), String
											.valueOf(row[3]), String
											.valueOf(row[4]), String
											.valueOf(row[5]), String
											.valueOf(row[6]), String
											.valueOf(row[7]), String
											.valueOf(row[8]), String
											.valueOf(row[9]));
							return internalReportBJB;
						}

						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return internalReportBJBs;
	}

	@Override
	public Integer countTransactionReport(Date startDocDate, Date endDocDate,
			String accountCodeNumber, String paymentMethod,
			String transactionStatus) {
		try {
			String startDate = new SimpleDateFormat("yyyy-MM-dd")
					.format(startDocDate);
			String endDate = new SimpleDateFormat("yyyy-MM-dd")
					.format(endDocDate);
			BigInteger jml = (BigInteger) getCurrentSession()
					.getNamedQuery("TransactionReport#countTransactionReport")
					.setParameter("startDocDate", startDate)
					.setParameter("endDocDate", endDate)
					.setParameter("accountCodeNumber", "%"+accountCodeNumber+"%")
					.setParameter("paymentMethod", "%"+paymentMethod+"%")
					.setParameter("transactionStatus", "%"+transactionStatus+"%")
					.uniqueResult();
			return jml.intValue();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public List<InternalReportBJB> getAccountValidation(Date startDocDate,
			Date endDocDate, String sp2dno, String accountCodeNumber, String accountName,
			String transactionStatus, int start, int end) {

		List<InternalReportBJB> internalReportBJBs = new ArrayList<InternalReportBJB>();
		try {
			List<String> transactionStatusList = getListTransactionStatusValidation(transactionStatus);
			String startDate = PubUtil.dateToString("yyyy-MM-dd", startDocDate);
			String endDate = PubUtil.dateToString("yyyy-MM-dd", endDocDate);
			internalReportBJBs = (List<InternalReportBJB>) getCurrentSession()
					.getNamedQuery("TransactionReport#getAccountValidation")
					.setParameter("startDocDate", startDate)
					.setParameter("endDocDate", endDate)
					.setParameter("sp2dno", "%"+sp2dno+"%")
					.setParameter("accountCodeNumber", "%"+accountCodeNumber+"%")
					.setParameter("accountName", "%"+StringUtils.upperCase(accountName)+"%")
					.setParameterList("transactionStatusList", transactionStatusList)
					.setParameter("startRow", start)
					.setParameter("endRow", end)
					.setResultTransformer(new ResultTransformer() {
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							InternalReportBJB internalReportBJB = new InternalReportBJB();
							internalReportBJB.setNo(String.valueOf(row[0]));
							internalReportBJB.setSp2dNo(String.valueOf(row[1]));
							internalReportBJB.setDocDate(String.valueOf(row[2]));
							internalReportBJB.setAccountName(String.valueOf(row[3]));
							internalReportBJB.setAccountNumber(String.valueOf(row[4]));
							internalReportBJB.setBranchCode(String.valueOf(row[5]));
							internalReportBJB.setStatus(String.valueOf(row[6]));
							return internalReportBJB;
						}

						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return internalReportBJBs;
	}

	public static List<String> getListTransactionStatusValidation(String transactionStatus) {
		List<String> transactionStatusList = new ArrayList<String>();
		if(StringUtils.isEmpty(transactionStatus)){
			transactionStatusList.add("1");
			transactionStatusList.add("2");
		}
		else{
			transactionStatusList.add(transactionStatus);
		}
		return transactionStatusList;
	}

	@Override
	public Integer countAccountValidation(Date startDocDate,
			Date endDocDate, String sp2dno, String accountCodeNumber, String accountName,
			String transactionStatus) {
		try {
			List<String> transactionStatusList = getListTransactionStatusValidation(transactionStatus);
			String startDate = PubUtil.dateToString("yyyy-MM-dd", startDocDate);
			String endDate = PubUtil.dateToString("yyyy-MM-dd", endDocDate);
			BigInteger jml = (BigInteger) getCurrentSession()
					.getNamedQuery("TransactionReport#countAccountValidation")
					.setParameter("startDocDate", startDate)
					.setParameter("endDocDate", endDate)
					.setParameter("sp2dno", "%"+sp2dno+"%")
					.setParameter("accountCodeNumber", "%"+StringUtils.upperCase(accountCodeNumber)+"%")
					.setParameter("accountName", "%"+accountName+"%")
					.setParameterList("transactionStatusList", transactionStatusList)
					.uniqueResult();
			return jml.intValue();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<InternalReportBJB> getTransactionReject(String filename, Date startDocDate, Date endDocDate, Date rejectDate,
			int start, int max) {
		List<InternalReportBJB> internalReportBJBs = getCurrentSession().getNamedQuery("TransactionReport#getTransactionReject")
				.setString("filename", "%"+StringUtils.upperCase(filename)+"%")
				.setString("startDocDate", PubUtil.dateToString("yyyy-MM-dd", startDocDate))
				.setString("endDocDate", PubUtil.dateToString("yyyy-MM-dd", endDocDate))
				.setString("rejectDate", "%"+getRejectDate(rejectDate)+"%")
				.setInteger("start", start)
				.setInteger("max", max)
				.setResultTransformer(new ResultTransformer() {
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
//							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
							InternalReportBJB internalReportBJB;
							internalReportBJB = new InternalReportBJB(String
									.valueOf(row[0]), String.valueOf(row[1]),
									String.valueOf(row[2]), String
											.valueOf(row[3]), String
											.valueOf(row[4]), String
											.valueOf(row[5]), String
											.valueOf(row[6]));
							return internalReportBJB;
//							internalReportBJB.setNo(String.valueOf(row[0]));
//							internalReportBJB.setFileName(String.valueOf(row[1]));
//							internalReportBJB.setIncommingTime(String.valueOf(row[2]));
//							internalReportBJB.setDocDate(String.valueOf(row[3]));
//							internalReportBJB.setTotalRecord(String.valueOf(row[4]));
//							internalReportBJB.setTotalAmount(String.valueOf(row[5]));
//							internalReportBJB.setRejectDate(String.valueOf(row[6]));
//							return internalReportBJB;
						}

						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					})
				.list();
		return internalReportBJBs;
	}

	public static String getRejectDate(Date rejectDate) {
		if(rejectDate!=null){
			return PubUtil.dateToString("dd-MMM-yy", rejectDate);
		}
		return "";
	}

	@Override
	public int countTransactionReject(String filename, Date startDocDate,
			Date endDocDate, Date rejectDate) {
		BigInteger jml = (BigInteger) getCurrentSession().getNamedQuery("TransactionReport#countTransactionReject")
			.setString("filename", "%"+StringUtils.upperCase(filename)+"%")
			.setString("startDocDate", PubUtil.dateToString("yyyy-MM-dd", startDocDate))
			.setString("endDocDate", PubUtil.dateToString("yyyy-MM-dd", endDocDate))
			.setString("rejectDate", "%"+getRejectDate(rejectDate)+"%")
			.uniqueResult();
		
		if(!PubUtil.isBigIntegerEmpty(jml)){
			return jml.intValue();
		}
		return 0;
	}
}
