package com.mii.dao;

import java.util.List;

import com.mii.helpers.BasicDao;
import com.mii.models.SpanAccountHistory;

public interface AccountHistoryDao extends BasicDao{
	public List<SpanAccountHistory> showAccountHistoryGAJI(String debitaccno, String docDate, String inquiryType, int startRow, int endRow);
	public int countAccountHistoryGAJI (String debitaccno, String docDate, String inquiryType);
	public List<SpanAccountHistory> showAccountHistoryGAJIMutasi(String debitaccno, String docDate, int startRow, int endRow);
	public int countAccountHistoryGAJIMutasi (String debitaccno, String docDate);
}
