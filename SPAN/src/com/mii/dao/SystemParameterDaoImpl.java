package com.mii.dao;

import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.transform.ResultTransformer;

import com.mii.constant.Constants;
import com.mii.helpers.BasicDaoImpl;
import com.mii.models.SystemParameter;

public class SystemParameterDaoImpl extends BasicDaoImpl implements SystemParameterDao {

	@Override
	public void saveSystemParameter(SystemParameter systemParameter) {
		try{
			save(systemParameter);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@Override
	public SystemParameter deleteSystemParameter(String systemParameterId) {
		SystemParameter systemParameter = new SystemParameter();
		try {
			systemParameter.setParamId(systemParameterId);
			delete(SystemParameter.class, systemParameterId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SystemParameter> getAllSystemParameter() {
		List<SystemParameter> systemParameterList = new ArrayList<SystemParameter>();
		try{
			systemParameterList = (List<SystemParameter>)getCurrentSession().
					getNamedQuery("TParameter#getAllSystemParameter").list();
		}catch(Exception e){
			e.printStackTrace();
		}
		return systemParameterList;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SystemParameter> getSearchParameterLazyLoading(String paramId, String paramName, String paramValue, List<String> status, int maxRow, int minRow) {
		List<SystemParameter> systemParameterList = new ArrayList<SystemParameter>();
		try{
			systemParameterList = (List<SystemParameter>)getCurrentSession().
					getNamedQuery("TParameter#getSearchParameterLazyLoading")
					.setString("PARAMID", "%"+paramId+"%")
					.setString("PARAMNAME", "%"+paramName+"%")
					.setString("PARAMVALUE", "%"+paramValue+"%")
					.setParameterList("STATUS", status)
					.setBigInteger("MAX_ROW", BigInteger.valueOf(maxRow))
					.setBigInteger("MIN_ROW", BigInteger.valueOf(minRow))
					.setResultTransformer(new ResultTransformer() {						
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
							SystemParameter systemParameter;
							try {
								Date insertDate = null;
								Date createDate = null;
								if (row[4]!=null) {
									insertDate = sdf.parse(String.valueOf(row[4]));
								}
								if (row[5]!=null) {
									createDate = sdf.parse(String.valueOf(row[5]));
								}
								systemParameter = new SystemParameter(
										String.valueOf(row[0]), 
										String.valueOf(row[1]), 
										String.valueOf(row[2]),
										String.valueOf(row[3]),
										insertDate, 
										createDate,
										String.valueOf(row[6]),
										String.valueOf(row[7]),
										String.valueOf(row[8]));
								return systemParameter;
							} catch (ParseException e) {
								e.printStackTrace();
							}
							return null;
						}
						
						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).
					list();
		}catch(Exception e){
			e.printStackTrace();
		}
		return systemParameterList;
	}
	
	@SuppressWarnings("unchecked")
	public List<SystemParameter> getAllParameterLazyLoading(int maxRow, int minRow) {		
		List<SystemParameter> systemParameterList = new ArrayList<SystemParameter>();
		try{
			systemParameterList = (List<SystemParameter>)getCurrentSession().
					getNamedQuery("TParameter#getAllParameterLazyLoading").
					setBigInteger("MAX_ROW", BigInteger.valueOf(maxRow)).
					setBigInteger("MIN_ROW", BigInteger.valueOf(minRow)).
					setResultTransformer(new ResultTransformer() {						
						private static final long serialVersionUID = 1L;

						@Override
						public Object transformTuple(Object[] row, String[] arg1) {
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
							SystemParameter systemParameter;
							try {
								Date insertDate = null;
								Date createDate = null;
								if (row[4]!=null) {
									insertDate = sdf.parse(String.valueOf(row[4]));
								}
								if (row[5]!=null) {
									createDate = sdf.parse(String.valueOf(row[5]));
								}
								systemParameter = new SystemParameter(
										String.valueOf(row[0]), 
										String.valueOf(row[1]), 
										String.valueOf(row[2]),
										String.valueOf(row[3]),
										insertDate, 
										createDate,
										String.valueOf(row[6]),
										String.valueOf(row[7]),
										String.valueOf(row[8]));
								return systemParameter;
							} catch (ParseException e) {
								e.printStackTrace();
							}
							return null;
						}
						
						@SuppressWarnings("rawtypes")
						@Override
						public List transformList(List arg0) {
							return arg0;
						}
					}).
					list();
		}catch(Exception e){
			e.printStackTrace();
		}
		return systemParameterList;
	}

	@Override
	public SystemParameter getDetailedParameterByParamName(String paramName) {
		SystemParameter systemParameter = new SystemParameter();
		try{
			List<String> listStatus = new ArrayList<String>();
			listStatus.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
			listStatus.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
			systemParameter = (SystemParameter) getCurrentSession().getNamedQuery("TParameter#getSystemParameterByParamName")
					.setString("PARAM_NAME", paramName)				
					.setParameterList("status", listStatus)	
					.uniqueResult();
		}catch(Exception e){
			e.printStackTrace();
		}
		return systemParameter;
	}
	
	@Override
	public SystemParameter getDetailedParameter(String paramId) {
		SystemParameter systemParameter = new SystemParameter();
		try{
			systemParameter = (SystemParameter) getCurrentSession().getNamedQuery("TParameter#getSystemParameterByParamId")
					.setString("PARAMID", paramId)					
					.uniqueResult();
		}catch(Exception e){
			e.printStackTrace();
		}
		return systemParameter;
	}
	
	@Override
	public void editParameter(SystemParameter systemParameter) {
		try {
			save(systemParameter);
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}

	@Override
	public int countAllParameter() {
			try{
				BigInteger jml = (BigInteger) getCurrentSession().
						getNamedQuery("TParameter#countAllRecord").uniqueResult();			
				return jml.intValue();
			}catch(Exception e){
				e.printStackTrace();
			}
			return 0;
	}
	
	@Override
	public int countSearchParameter(String paramId, String paramName, String paramValue, List<String> status) {
			try{
				BigInteger jml = (BigInteger) getCurrentSession().
						getNamedQuery("TParameter#countSearchRecord")
						.setString("PARAMID", "%"+paramId+"%")
						.setString("PARAMNAME", "%"+paramName+"%")
						.setString("PARAMVALUE", "%"+paramValue+"%")
						.setParameterList("STATUS", status)
						.uniqueResult();			
				return jml.intValue();
			}catch(Exception e){
				e.printStackTrace();
			}
			return 0;
	}

	@Override
	public BigInteger getParameterSequence() {
		BigInteger id = (BigInteger) getCurrentSession().
				getNamedQuery("TParameter#selectParameterIdSeq").uniqueResult();
		return id;
	}

}
