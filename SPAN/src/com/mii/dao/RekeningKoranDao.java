package com.mii.dao;

import java.util.Date;
import java.util.List;

import com.mii.helpers.BasicDao;
import com.mii.models.SpanRekeningKoran;

public interface RekeningKoranDao extends BasicDao{

	List<SpanRekeningKoran> getAll(String accNo, Date stDate, Date enDate);

}
