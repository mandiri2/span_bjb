package com.mii.bjb.core;

public class UIMBJBConstants {
	
	public static final String UIM_USER_ID_PASS_SALAH 				= "salah";
	public static final String UIM_USER_TIDAK_ADA	 				= "tidak ada";
	public static final String UIM_SERVER_UNREGISTERED	 			= ":p";
	public static final String UIM_SUCCESS							= "SUCCESS";
	
	public static final String UIM_USER_TIDAK_AKTIF	 				= "N";
	public static final String UIM_PASSWORD_KADALUARSA 				= "Y";
	public static final String UIM_USER_DIKUNCI		 				= "Y";
	
	public static final String UIM_CP_USER_TIDAK_ADA	 			= "tidak ada";
	public static final String UIM_CP_USER_DIKUNCI		 			= "dikunci";
	public static final String UIM_CP_PASSWORD_LAMA_SALAH			= "salah";
	public static final String UIM_CP_PASSWORD_LAIN					= "gunakan password lain";
	public static final String UIM_CP_BERHASIL						= "Berhasil";
	
	public static final String UIM_USER_PASS_SALAH_MESSAGE			= "Password salah.";
	public static final String UIM_SERVER_UNREGISTERED_MESSAGE		= "Server tidak terdaftar di UIM.";
	
	public static final String UIM_USER_TIDAK_AKTIF_MESSAGE	 		= "User Tidak Aktif/Cuti/Belum Disetujui Mendapatkan Akses.";
	public static final String UIM_PASSWORD_KADALUARSA_MESSAGE	 	= "Password Kadaluarsa. Ganti Password.";
	public static final String UIM_USER_DIKUNCI_MESSAGE		 		= "User dikunci, gunakan aplikasi UIM untuk membuka akun.";
	
	public static final String UIM_CP_USER_TIDAK_ADA_MESSAGE	 	= "User id tidak terdaftar.";
	public static final String UIM_CP_USER_DIKUNCI_MESSAGE		 	= "User ID di kunci. Hubungi Administrator UIM.";
	public static final String UIM_CP_PASSWORD_LAMA_SALAH_MESSAGE	= "Password lama salah.";
	public static final String UIM_CP_PASSWORD_LAIN_MESSAGE			= "Gunakan password lain";
	
	public static final String UIM_FLAG_LOGIN						= "0";
	public static final String UIM_FLAG_CHANGE_PASSWORD				= "1";
	
	public static final String TCP_CONNECTION_CLOSE_MESSAGE			= "Tidak dapat terhubung ke server UIM.";
	
}
