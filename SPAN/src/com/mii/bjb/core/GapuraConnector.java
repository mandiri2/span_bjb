package com.mii.bjb.core;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import com.mii.constant.Constants;
import com.mii.dao.SystemParameterDao;
import com.mii.models.SystemParameter;

import id.co.vsi.common.crypto.SymetricCryptoHandler;


public class GapuraConnector {
	public static String connect(String message, SystemParameterDao systemParameterDao){
		
		String key = "tgadapterbjb2012";
        
        Socket tGatewaySocket = null;
        String response = "";
        
        try {
	        SystemParameter parameterIP = systemParameterDao.getDetailedParameterByParamName(Constants.GAPURA_IP);
			String gapuraIP = parameterIP.getParamValue();
			SystemParameter parameterPORT = systemParameterDao.getDetailedParameterByParamName(Constants.GAPURA_PORT);
			int gapuraPORT = Integer.parseInt(parameterPORT.getParamValue());
			
			System.out.println("Connecting to Gapura...");
			tGatewaySocket = new Socket(gapuraIP, gapuraPORT);
			System.out.println("Connected : "+tGatewaySocket.isConnected());
		} catch (UnknownHostException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (Exception e){
			e.printStackTrace();
		}
        
        final byte cEndMessageByte = -0x01;
        
        try {

            ByteArrayOutputStream tRequestByteStream = new ByteArrayOutputStream();
            
            String request = message;
            request = hashString(1, message);
            
            System.out.println("request : "+request);
   
            tRequestByteStream.write(request.getBytes());
            tRequestByteStream.write(cEndMessageByte);
            tGatewaySocket.getOutputStream().write(tRequestByteStream.toByteArray());
            
            byte tMessageByte = cEndMessageByte;
            StringBuffer sb = new StringBuffer();

            while ((tMessageByte = (byte) tGatewaySocket.getInputStream().read()) != cEndMessageByte) {
                sb.append((char) tMessageByte);
            }
            response = sb.toString();

            response = hashString(2, response);
            
            System.out.println("response : "+response);
            return response;
        } catch (Exception e) {
        	e.printStackTrace();
        }
        
        try {
			tGatewaySocket.close();
		} catch (IOException e) {
			System.out.println("Failed To Close Socket");
			e.printStackTrace();
		}
		return response;
    }


	public static String hashString(int act, String input) {
		String key = "tgadapterbjb2012";
		String EncyptMode = "AES/ECB/PKCS5Padding";
	    String output = input;
	    String[] encryptMode = EncyptMode.split("/");
    
	    try {
	        SymetricCryptoHandler crypto = new SymetricCryptoHandler(encryptMode[0], encryptMode[1], encryptMode[2], key, null);
	        output = crypto.getCryptoMessage(act, input);
	    } catch (NoSuchAlgorithmException noSuchAlgorithmException) {
	        System.err.println("NoSuchAlgorithmException: " + noSuchAlgorithmException);
	    } catch (NoSuchPaddingException noSuchPaddingException) {
	        System.err.println("NoSuchPaddingException: " + noSuchPaddingException);
	    } catch (InvalidKeyException invalidKeyException) {
	        System.err.println("InvalidKeyException: " + invalidKeyException);
	    } catch (IllegalBlockSizeException illegalBlockSizeException) {
	        System.err.println("IllegalBlockSizeException: " + illegalBlockSizeException);
	    } catch (BadPaddingException badPaddingException) {
	        System.err.println("BadPaddingException: " + badPaddingException);
	    } catch (InvalidAlgorithmParameterException invalidAlgorithmParameterException) {
	        System.err.println("InvalidAlgorithmParameterException: " + invalidAlgorithmParameterException);
	    }
	    return output;
	}
        
	}
