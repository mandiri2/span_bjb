package com.mii.bjb.core;

import com.mii.constant.Constants;
import com.mii.dao.FASTBJBDao;
import com.mii.dao.SystemParameterDao;
import com.mii.json.AccountBalance;
import com.mii.models.FastBJBModel;

public class FastBJBConnector {
	public static String CheckAccountBJB(FASTBJBDao fastBJBDao, String accountName, String accountNumber, SystemParameterDao systemParameterDao){
		//NEW//
		FastBJBModel result = fastBJBDao.checkBlockedUserAccountBJB(accountName, accountNumber);
		try{
			if(result.getStatusActive().equals("Y")){
				return Constants.FAST_FOUND;
			}else{
				System.out.println("[FAST] No Rekening "+accountNumber+" - "+accountName+" tutup / diblokir.");
				return Constants.FAST_REK_DORMANT;
			}
		}catch(Exception e){
			System.out.println("[FAST] No Rekening "+accountNumber+" - "+accountName+"tidak ditemukan di Fast. ");
			return checkAccountAndName(systemParameterDao, accountName, accountNumber);
		}
		//OLD//
		/*int count = fastBJBDao.validateUserAccountBJB(accountName, accountNumber);
		if(count > 0){
			return Constants.FAST_FOUND;
		}else{
			return checkAccountAndName(systemParameterDao, accountName, accountNumber);
		}*/
	}
	
	public static String checkAccountAndName(SystemParameterDao systemParameterDao, String accountName, String  accountNumber){
		return AccountBalance.checkAccountAndName(systemParameterDao, accountNumber, accountName);
	}

}
