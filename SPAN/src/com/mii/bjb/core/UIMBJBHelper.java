package com.mii.bjb.core;

import com.mii.constant.Constants;
import com.mii.dao.SystemParameterDao;
import com.mii.helpers.Digest;
import com.mii.helpers.PasswordGenerator;
import com.mii.models.SystemParameter;

public class UIMBJBHelper {
	
	public static String packUIMRequestLogin(String userId, String password, SystemParameterDao systemParameterDao){
		int userLength = userId.length();
		SystemParameter systemParameterHost  = systemParameterDao.getDetailedParameterByParamName(Constants.UIM_ID_APLIKASI);
		String idAplikasi = systemParameterHost.getParamValue();
		String encryptPassword = "";
		try {
			encryptPassword = Digest.computeDigest(password);
		} catch (Exception e) {
			e.printStackTrace();
		}
		String userLengthString = Integer.toString(userLength);
		if(userLengthString.length()==1){
			userLengthString = "0".concat(userLengthString);
		}
		String userIdTail = PasswordGenerator.generatePassword().substring(0,10-userLength);
		String passwordTail = PasswordGenerator.generatePassword();
		while(encryptPassword.length()+passwordTail.length()<99){
			passwordTail = passwordTail.concat(PasswordGenerator.generatePassword());
		}
		if(encryptPassword.length()+passwordTail.length()>99){
			passwordTail = passwordTail.substring(0, 99-encryptPassword.length());
		}
		String request = userLengthString+userId+userIdTail+encryptPassword.length()+encryptPassword+passwordTail+idAplikasi;
		return request;
	}
	
	public static String packUIMRequestChangePass(String userId, String oldpassword, String newPassword, SystemParameterDao systemParameterDao){
		int userLength = userId.length();
		SystemParameter systemParameterHost  = systemParameterDao.getDetailedParameterByParamName(Constants.UIM_ID_APLIKASI);
		String idAplikasi = systemParameterHost.getParamValue();
		String encryptOldPassword = "";
		String encryptNewPassword = "";
		try {
			encryptOldPassword = Digest.computeDigest(oldpassword);
			encryptNewPassword = Digest.computeDigest(newPassword);
		} catch (Exception e) {
			e.printStackTrace();
		}
		String userLengthString = Integer.toString(userLength);
		if(userLengthString.length()==1){
			userLengthString = "0".concat(userLengthString);
		}
		String userIdTail = PasswordGenerator.generatePassword().substring(0,10-userLength);
		
		String request = userLengthString+userId+userIdTail+"G4"+encryptOldPassword+encryptNewPassword;
		
		return request;
	}
	
	/**
	 * 
	 * @param response
	 * @param typeMessage (0 = login, 1 = change password)
	 * @return
	 */
	
	public static UIMBJBModel unpackUIMResponse(String response, String typeMessage){
		UIMBJBModel uimBJBModel = new UIMBJBModel();
		String[] valueSplit = response.split("\\|");
		String message = UIMBJBConstants.UIM_SUCCESS;
		if(typeMessage.equals("0")){
			if(valueSplit.length>1){
				uimBJBModel.setInternalUser(true);
				uimBJBModel.setTryLoginDepkeu(false);
				if(valueSplit[17].equals(UIMBJBConstants.UIM_USER_TIDAK_AKTIF)){
					message = UIMBJBConstants.UIM_USER_TIDAK_AKTIF_MESSAGE;
				}else if(valueSplit[18].equals(UIMBJBConstants.UIM_PASSWORD_KADALUARSA)){
					message = UIMBJBConstants.UIM_PASSWORD_KADALUARSA_MESSAGE;
				}else if(valueSplit[19].equals(UIMBJBConstants.UIM_USER_DIKUNCI)){
					message = UIMBJBConstants.UIM_USER_DIKUNCI_MESSAGE;
				}
				uimBJBModel.setUimRoleId(valueSplit[20]);
			}else{
				uimBJBModel = validate(valueSplit[0], typeMessage);
				message = uimBJBModel.getResponseMessage();
			}
		}else{
			uimBJBModel = validate(valueSplit[0], typeMessage);
			message = uimBJBModel.getResponseMessage();
		}
		uimBJBModel.setResponseMessage(message);
		return uimBJBModel;
	}

	private static UIMBJBModel validate(String response, String typeMessage) {
		UIMBJBModel uimBJBModel = new UIMBJBModel();
		String message = "";
		
		if(typeMessage.equals("0")){
			//LOGIN
			if(response.equals(UIMBJBConstants.UIM_USER_ID_PASS_SALAH)){
				message = UIMBJBConstants.UIM_USER_PASS_SALAH_MESSAGE;
				uimBJBModel.setInternalUser(true);
				uimBJBModel.setTryLoginDepkeu(false);
			}else if(response.equals(UIMBJBConstants.UIM_USER_TIDAK_ADA)){
				message = UIMBJBConstants.UIM_USER_TIDAK_ADA;
				uimBJBModel.setInternalUser(false);
				uimBJBModel.setTryLoginDepkeu(true);
			}else if(response.equals(UIMBJBConstants.UIM_SERVER_UNREGISTERED)){
				message = UIMBJBConstants.UIM_SERVER_UNREGISTERED_MESSAGE;
				uimBJBModel.setInternalUser(false);
				uimBJBModel.setTryLoginDepkeu(false);
			}else if(response.equals(UIMBJBConstants.TCP_CONNECTION_CLOSE_MESSAGE)){
				message = UIMBJBConstants.TCP_CONNECTION_CLOSE_MESSAGE;
				uimBJBModel.setInternalUser(false);
				uimBJBModel.setTryLoginDepkeu(false);
			}else{
				message = "Unknown Response from UIM. Please Contact Administrator.";
				uimBJBModel.setInternalUser(false);
				uimBJBModel.setTryLoginDepkeu(false);
			}
		}else if(typeMessage.equals("1")){
			//CHANGE PASSWORD
			if(response.equals(UIMBJBConstants.UIM_CP_USER_TIDAK_ADA)){
				message = UIMBJBConstants.UIM_CP_USER_TIDAK_ADA_MESSAGE;
				uimBJBModel.setInternalUser(false);
				uimBJBModel.setTryLoginDepkeu(false);
			}else if(response.equals(UIMBJBConstants.UIM_CP_USER_DIKUNCI)){
				message = UIMBJBConstants.UIM_CP_USER_DIKUNCI_MESSAGE;
				uimBJBModel.setInternalUser(true);
				uimBJBModel.setTryLoginDepkeu(false);
			}else if(response.equals(UIMBJBConstants.UIM_CP_PASSWORD_LAMA_SALAH)){
				message = UIMBJBConstants.UIM_CP_PASSWORD_LAMA_SALAH_MESSAGE;
				uimBJBModel.setInternalUser(true);
				uimBJBModel.setTryLoginDepkeu(false);
			}else if(response.equals(UIMBJBConstants.UIM_CP_PASSWORD_LAIN)){
				message = UIMBJBConstants.UIM_CP_PASSWORD_LAIN_MESSAGE;
				uimBJBModel.setInternalUser(true);
				uimBJBModel.setTryLoginDepkeu(false);
			}else if(response.equals(UIMBJBConstants.UIM_CP_BERHASIL)){
				message = UIMBJBConstants.UIM_CP_BERHASIL;
				uimBJBModel.setInternalUser(true);
				uimBJBModel.setTryLoginDepkeu(false);
			}else if(response.equals(UIMBJBConstants.TCP_CONNECTION_CLOSE_MESSAGE)){
				message = UIMBJBConstants.TCP_CONNECTION_CLOSE_MESSAGE;
				uimBJBModel.setInternalUser(false);
				uimBJBModel.setTryLoginDepkeu(false);
			}else{
				message = "Unknown Response from UIM. Please Contact Administrator.";
				uimBJBModel.setInternalUser(false);
				uimBJBModel.setTryLoginDepkeu(false);
			}
		}
		uimBJBModel.setResponseMessage(message);
		
		return uimBJBModel;
	}
}
