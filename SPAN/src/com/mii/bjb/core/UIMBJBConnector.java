package com.mii.bjb.core;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.mii.constant.Constants;
import com.mii.dao.SystemParameterDao;
import com.mii.models.SystemParameter;

public class UIMBJBConnector {
//	public static void main(String[] args) throws IOException {
	public static String doSendRequest(String request, SystemParameterDao systemParameterDao) throws IOException{
		SystemParameter systemParameterHost  = systemParameterDao.getDetailedParameterByParamName(Constants.UIM_HOST);
		SystemParameter systemParameterPort  = systemParameterDao.getDetailedParameterByParamName(Constants.UIM_PORT);
		//SIMULATOR//
//		String serverHostname = new String ("localhost");
//		int Port = 9898;
		//UIM//
//		String serverHostname = new String ("10.6.226.26");
//		int port = 1978;
		String serverHostname = new String (systemParameterHost.getParamValue());
		int Port = Integer.parseInt(systemParameterPort.getParamValue());
		
		/*
		 * test:
		 * 1 - Echo
		 * 2 - Login UIM
		 */
        String response = ConnectToServer(serverHostname, Port, 2, request);
		return response;
	}
	
	private static Socket echoSocket = null;
	private static PrintWriter out = null;
	private static BufferedReader in = null;

	private static String ConnectToServer(String serverHostname, int Port, int Route, String request)
			throws IOException {
		System.out.println ("Attemping to connect to host " + serverHostname + " on port "+Port+".");
		String response = "";
        try {
        	//FIXME bypass uim
//        	if(true){
//        		throw new UnknownHostException();
//        	}
//        	
            echoSocket = new Socket(serverHostname, Port);
            System.out.println("connected : "+echoSocket.isConnected());
            out = new PrintWriter(echoSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(echoSocket.getInputStream()));
            
            BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
    		if (Route==1){
    			SimpleEcho(out, in, stdIn);
    		}else if (Route==2){
    			response = LoginUIM(out, in, request);
    		}
    		
    		out.close();
    		in.close();
    		stdIn.close();
    		echoSocket.close();
    		
        } catch (UnknownHostException e) {
            System.err.println("Don't know about host: " + serverHostname);
            response = UIMBJBConstants.TCP_CONNECTION_CLOSE_MESSAGE;
//            response = UIMBJBConstants.UIM_USER_TIDAK_ADA;
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for " + "the connection to: " + serverHostname);
            response = UIMBJBConstants.TCP_CONNECTION_CLOSE_MESSAGE;
//            response = UIMBJBConstants.UIM_USER_TIDAK_ADA;
        }
		//Hardcode user tidak ada
        //response = UIMBJBConstants.UIM_USER_TIDAK_ADA;
        return response;
		}

	public static void main(String[] args) {
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMMyyyy");
		System.out.println(sdf.format(new Date()));
	}
	
	private static void SimpleEcho(PrintWriter out, BufferedReader in,
			BufferedReader stdIn) throws IOException {
		String userInput;
	    System.out.print ("request: ");
		while ((userInput = stdIn.readLine()) != null) {
		    out.println(userInput);
	        System.out.println("response : "+in.readLine() + "\n");
	        System.out.println ("request: ");
		}
	}
	
	private static String LoginUIM(PrintWriter out, BufferedReader in, String request){
		String response = "";
		try {
			System.out.println("request : "+request);
			out.println(request);
			response = in.readLine();
		    System.out.println("response : "+response+ "\n");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
		
	}
}
