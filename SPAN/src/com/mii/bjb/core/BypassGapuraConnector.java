package com.mii.bjb.core;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class BypassGapuraConnector {
	public static void main(String[] args) {
		String serverHostname = "10.6.226.160";
		int Port = 10012;
		String request = "{\"SID\": \"singleUserIDWebTeller\", \"PCC\": \"5\", \"MPI\": { \"ZLVTOZ\": \"020216\", \"ZLEAN\": \"0072932706001\", \"ZLVFRZ\": \"020216\", \"PGNUM\": \"7\" }, \"ST\": \"12147482624\", \"CC\": \"0001\", \"FC\": \"TH\", \"DT\": \"20161026162726\", \"MC\": \"90023\", \"MT\": \"9200\", \"CID\": \"TG-CB-00-0000017\", \"PC\": \"001001\"}3";
		String response = ConnectToServer(serverHostname, Port, request);
		System.out.println(response);
	}
	
	private static Socket echoSocket = null;
	private static PrintWriter out = null;
	private static BufferedReader in = null;

	private static String ConnectToServer(String serverHostname, int port, String request) {
		System.out.println ("Attemping to connect to host " + serverHostname + " on port "+port+".");
		String response = "";
		try {
			echoSocket = new Socket(serverHostname, port);
			System.out.println("connected : "+echoSocket.isConnected());
			System.out.println("request : "+request);
            out = new PrintWriter(echoSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(echoSocket.getInputStream()));
            BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
    		sendMessage(out, in, stdIn);
    		out.close();
    		in.close();
    		stdIn.close();
    		echoSocket.close();
		} catch (UnknownHostException e) {
			System.err.println("Don't know about host: " + serverHostname);
		} catch (IOException e) {
            System.err.println("Couldn't get I/O for " + "the connection to: " + serverHostname);
        }
		return response;
	}

	private static void sendMessage(PrintWriter out2, BufferedReader in2,
			BufferedReader stdIn) throws IOException {
		String userInput;
		System.out.println("Sending request...");
		while ((userInput = stdIn.readLine()) != null) {
		    out.println(userInput);
	        System.out.println("response : "+in.readLine() + "\n");
		}
	}
	
	
}
