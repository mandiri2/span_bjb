package com.mii.test;

public class TestArray {
	
	public static void main(String[] args) {
		String[] s = new String[3];
		s[0] = "1";
		s[1] = "2";
		s[2] = "3";
		String d = "-";
		
		System.out.println(arrayToDelimitedString(s,d));
		
		System.out.println(delimitedStringToArray("1-2-3",d)[0]);
		System.out.println(delimitedStringToArray("1-2-3",d)[1]);
		System.out.println(delimitedStringToArray("1-2-3",d)[2]);
	}
	
	public static String arrayToDelimitedString(String[] s, String d){
		String r = "";
		for (int i =  0; i < s.length ; i++){
			if (i != s.length - 1){
				r = r + s[i] + d;
			}else{
				r = r + s[i];
			}
		}
		return r;
	}
	
	public static String[] delimitedStringToArray(String s, String d){
		String[] r = null;
		r = s.split(d);
		return r;
	}
	

}
