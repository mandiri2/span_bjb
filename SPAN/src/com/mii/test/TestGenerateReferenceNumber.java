package com.mii.test;

import java.math.BigDecimal;
import java.math.BigInteger;

public class TestGenerateReferenceNumber {
	
	public static void main(String[] args) {
		Integer seq = 12;
		BigInteger random = new BigDecimal(Math.round(Math.random()*1000000000000L)).toBigInteger();
		String referenceNumber = String.format("%012d", random) + String.format("%04d", seq);	
		System.out.println(referenceNumber);
	}
	

}
