package com.mii.test;

import java.util.Hashtable;

import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.Name;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

public class LDAP {
	
	public static void main (String[] args) {
		
		String username = "cms14";
		String password = "P@ssw0rd";
		String searchUser = "devi.andriyani.dev";
		// Setup environment for authenticating
		// searchUser = null;
		LDAP ldap = new LDAP();
		try{
			if(ldap.authenticate(username, password,searchUser)){
				System.out.println("TRUE");
			}else{
				System.out.println("FALSE");
			}
		}catch (Exception e){
			e.printStackTrace();
			//System.out.println("FALSE");
		}
		
         
    }
	
	public boolean authenticate(String username, String password, String searchUsername){

		//String base = "OU=IT,OU=Kantor Pusat,DC=dev,DC=corp,DC=btpn,DC=co,DC=id";
		String base = "DC=dev,DC=corp,DC=btpn,DC=co,DC=id";
		//String dn = "CN=" +username+ ","+base;
		String ldapURL = "ldap://10.1.76.9";

		// Setup environment for authenticating
		
		Hashtable<String, String> environment = new Hashtable<String, String>();
		environment.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		environment.put(Context.PROVIDER_URL, ldapURL);
		environment.put(Context.SECURITY_AUTHENTICATION, "simple");
		environment.put(Context.SECURITY_PRINCIPAL, username);
		environment.put(Context.SECURITY_CREDENTIALS, password);
		
		try
		{
			DirContext authContext = new InitialDirContext(environment);
			
			if (searchUsername != null){
				String[] returnAttributes = { "sAMAccountName" };
				SearchControls searchCtls = new SearchControls();
			    searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
			    searchCtls.setReturningAttributes(returnAttributes);
				String filter = "sAMAccountName=" + searchUsername + "";
				
				try {
					NamingEnumeration<SearchResult> result = authContext.search(base, filter, searchCtls);
			        if (result.hasMore()){
			        	/*
			        	Attributes attrs = result.next().getAttributes();
						String temp = attrs.get("samaccountname").toString();
						System.out.println(temp);
						*/
			        	return true;
			        }
				}catch(Exception e){
					e.printStackTrace();
					return false;
		        }
				return false;
			}
			return true;
		}
		catch (AuthenticationException ex)
		{
			ex.printStackTrace();
			return false;
			// Authentication failed
		}
		catch (NamingException ex)
		{
			ex.printStackTrace();
			return false;
		}
	}
	
	
}
