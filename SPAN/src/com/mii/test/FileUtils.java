package com.mii.test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Contains some methods to list files and folders from a directory
 *
 * @author Loiane Groner
 * http://loiane.com (Portuguese)
 * http://loianegroner.com (English)
 */
public class FileUtils {
	/**
	 * List all the files and folders from a directory
	 * @param directoryName to be listed
	 */
	public static void listFilesAndFolders(String directoryName){
		File directory = new File(directoryName);
		//get all the files from a directory
		File[] fList = directory.listFiles();
		for (File file : fList){
			System.out.println(file.getName());
		}
	}
	/**
	 * List all the files under a directory
	 * @param directoryName to be listed
	 * @throws IOException 
	 */
	public static void listFiles(String directoryName) throws IOException{
		File directory = new File(directoryName);
		//get all the files from a directory
		File[] fList = directory.listFiles();
		for (File file : fList){
			if (file.isFile()){

			}
		}
	}
	/**
	 * List all the folder under a directory
	 * @param directoryName to be listed
	 */
	public static void listFolders(String directoryName){
		File directory = new File(directoryName);
		//get all the files from a directory
		File[] fList = directory.listFiles();
		for (File file : fList){
			if (file.isDirectory()){
				System.out.println(file.getName());
			}
		}
	}

	/**
	 * Return downloadedFiles Array with criteria
	 * @author arifino
	 */
	public static List<downloadableFile> getFiles(String directoryName,String likename,String ext){
		File directory = new File(directoryName);
		//get all the files from a directory
		File[] fList = directory.listFiles();
		List<downloadableFile> listD = new ArrayList<downloadableFile>();
		for (File file : fList){
			if (file.isFile()){
				if((checkNames(file.getName(), likename))&&(checkExtension(file.getName(), ext))){
					downloadableFile dl = new downloadableFile();
					dl.setAbsolutePath(file.getAbsolutePath());
					dl.setName(file.getName());
					dl.setModifiedDate(new Date(file.lastModified()));
					listD.add(dl);
				}
			}
		}
		return listD;
	}
	
	/**
	 * Return downloadedFiles Array with criteria
	 * @author arifino
	 */
	public static List<downloadableFile> getFiles(String directoryName){
		File directory = new File(directoryName);
		//get all the files from a directory
		File[] fList = directory.listFiles();
		List<downloadableFile> listD = new ArrayList<downloadableFile>();
		for (File file : fList){
			if (file.isFile()){
					downloadableFile dl = new downloadableFile();
					dl.setAbsolutePath(file.getAbsolutePath());
					dl.setName(file.getName());
					dl.setModifiedDate(new Date(file.lastModified()));
					listD.add(dl);
			}
		}
		return listD;
	}
	
	/**
	 * Return downloadedFiles Array with criteria
	 * @author arifino
	 */
	public static List<downloadableFile> getFiles(String directoryName,String ext){
		File directory = new File(directoryName);
		//get all the files from a directory
		File[] fList = directory.listFiles();
		List<downloadableFile> listD = new ArrayList<downloadableFile>();
		for (File file : fList){
			if (file.isFile()){
				if(checkExtension(file.getName(), ext)){
					downloadableFile dl = new downloadableFile();
					dl.setAbsolutePath(file.getAbsolutePath());
					dl.setName(file.getName());
					dl.setModifiedDate(new Date(file.lastModified()));
					listD.add(dl);
				}
			}
		}
		return listD;
	}

	/**
	 * @author arifino
	 * check if name contains some character sequence
	 * @param filename
	 * @param like
	 * @return
	 */
	public static boolean checkNames(String filename, String like){
		if(filename.contains(like)){
			return true;
		} else return false;
	}

	/**
	 * @author arifino
	 * check if file have the right extension
	 * @param filename
	 * @param ext
	 * @return
	 */
	public static boolean checkExtension(String filename, String ext){
		if(filename.contains("."+ext)){
			return true;
		} else return false;
	}
}

