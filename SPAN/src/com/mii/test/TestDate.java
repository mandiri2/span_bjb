package com.mii.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.validator.ValidatorException;

public class TestDate {
	
	public static void main(String[] args) throws ParseException {
		Date input;
		String value = "2016/01/03";
		try {
			input = new SimpleDateFormat("yyyy/MM/dd").parse(value.toString());
		
			Date current = new Date();
			Boolean isAfter = input.after(current);
			Boolean isNow = (value.equals(new SimpleDateFormat("yyyy/MM/dd").format(current)));
			
			// TODO Auto-generated method stub
			if ((value == null) || (value == "")) {
				return;
			}

			if (isAfter || isNow) {
				//System.out.println("Valid date");
			}else{
				throw new ValidatorException(new FacesMessage(
						FacesMessage.SEVERITY_ERROR, "Validation Error",
						"Invalid date."));
			}
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
