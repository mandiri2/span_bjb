package com.mii.scoped;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.servlet.http.HttpSession;

import org.primefaces.component.panelmenu.PanelMenu;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;

import com.mii.dao.RoleDao;
import com.mii.helpers.FacesUtil;
import com.mii.models.Menu;
import com.mii.models.Role;
import com.mii.models.User;

@ManagedBean(name="sessionBean")
@SessionScoped
@Resource(name="jdbc/MPN")
public class SessionBean implements Serializable {
	private static final long serialVersionUID = 1L;	
	private User user;
	private Map<String, Object> paramMap = new HashMap<String, Object>();
	private MenuModel menuModel; 
	private Set<Integer> actionList = new HashSet<Integer>();
	private String test;
	
	@ManagedProperty(value="#{applicationBean}")
	private ApplicationBean applicationBean;
	
	@PostConstruct
	public void init(){

	}
	
	

	public String getTest() {
		return test;
	}



	public void setTest(String test) {
		this.test = test;
	}



	public List<User> getAllConnectedUser() {	
		List<User> connectedUsers = new ArrayList<User>();
		for (Entry<User, HttpSession> entry : User.getLogins().entrySet()) {
			connectedUsers.add( entry.getKey());
		}
		return connectedUsers;
	}
	
	/**
	 * Panel Menu
	 */
	public void buildPanelMenu(User user){
		buildMenu(user);
		PanelMenu panelMenu = new PanelMenu();
		panelMenu.setModel(menuModel);
	}
	
	/**
	 * DefaultMenuModel
	 * @param user
	 */
	public void buildMenu(User user){
		menuModel = new DefaultMenuModel();		
		RoleDao roleDao = (RoleDao) applicationBean.getCorpGWContext().getBean("roleDAO");
		List<Role> roles = roleDao.getRolesByIds(user.getRoles());
		List<Menu> menus = new ArrayList<Menu>();
		
		List<Menu> rootMenuList = new ArrayList<Menu>();
		Map<Integer, List<Menu>> childMap = new HashMap<Integer, List<Menu>>();

		for (Role role : roles) {
			menus.addAll(role.getMenus());
		}
		for (Menu menu : menus) {
			if (menu.getMenuParentId() == 0 &&  !menu.isAction() && 
					!rootMenuList.contains(menu)) {				
				rootMenuList.add(menu);
				List<Menu> childMenuList = new ArrayList<Menu>();
				
				for (Menu childMenu : menus) {
					if (childMenu.getMenuParentId() == menu.getMenuId() &&
							!childMenuList.contains(childMenu)) {
						childMenuList.add(childMenu);
					}
				}				
				childMap.put(menu.getMenuId(), childMenuList);
			}
			else if (menu.isAction()) {
				actionList.add(menu.getMenuId());
			}
		}
		Collections.sort(rootMenuList);
		
		for (Menu value : rootMenuList) {
			if (value!=null) {
				DefaultSubMenu subMenu = new DefaultSubMenu();
				subMenu.setLabel(value.getMenuName());
				
				createSubMenu(subMenu, value, childMap);
				menuModel.addElement(subMenu);
			}
		}
	}
	
	public void createSubMenu(DefaultSubMenu subMenu, Menu parentMenu, 
			Map<Integer, List<Menu>> childMenu){
		
		List<Menu> childMenuData = childMenu.get(parentMenu.getMenuId());
		
		for (Menu childData : childMenuData) {			
			if (childData.getMenuPath()!=null && 
					childData.getMenuPath().trim().length()>0) {
				
				DefaultMenuItem menuItem = new DefaultMenuItem();  
				menuItem.setValue(childData.getMenuName());
				menuItem.setUrl(childData.getMenuPath()+".jsf");
				menuItem.setStyleClass(childData.getMenuPath());
				menuItem.setOnclick("loadingDlg.show()");
				menuItem.setOncomplete("loadingDlg.hide()");
		        subMenu.getElements().add(menuItem);
			}
		}	
	}
	
	public boolean hasAction(Integer menuId){
		return actionList.contains(menuId);
	}
	
	public void putParam(String key, Object value){
		paramMap.put(key, value);
	}
	public Object getAndRemoveParam(String key){
		return paramMap.remove(key);
	}
	public ApplicationBean getApplicationBean() {
		return applicationBean;
	}

	public void setApplicationBean(ApplicationBean applicationBean) {
		this.applicationBean = applicationBean;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public MenuModel getMenuModel() {
		return menuModel;
	}

	public void setMenuModel(MenuModel menuModel) {
		this.menuModel = menuModel;
	}

}
