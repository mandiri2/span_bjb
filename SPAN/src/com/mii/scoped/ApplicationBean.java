package com.mii.scoped;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.mii.dao.MenuDao;
import com.mii.models.Menu;

@ManagedBean(eager=true, name="applicationBean")
@ApplicationScoped
@Resource(name="jdbc/MPN")
public class ApplicationBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private ApplicationContext corpGWContext;
	private ApplicationContext autoCollectionContext;
	private ApplicationContext ubpContext;
	private ApplicationContext sapContext;
	private ApplicationContext ibftPrimaContext;
	private ApplicationContext losInsuranceContext;
	private ApplicationContext jebeAutopayContext;
	private List<Menu> menus = new ArrayList<Menu>();
	
	public ApplicationBean() {}
	
	@PostConstruct
	public void postConstruct(){
		corpGWContext = new ClassPathXmlApplicationContext("../../resources/config/spring-context.xml");
		loadMenu();
	}
	
	private void loadMenu() {
		MenuDao menuDao = (MenuDao) corpGWContext.getBean("menuDAO");
		this.menus = menuDao.getAllMenu();
	}

	public ApplicationContext getCorpGWContext() {
		return corpGWContext;
	}

	public void setCorpGWContext(ApplicationContext corpGWContext) {
		this.corpGWContext = corpGWContext;
	}

	public ApplicationContext getAutoCollectionContext() {
		return autoCollectionContext;
	}

	public void setAutoCollectionContext(ApplicationContext autoCollectionContext) {
		this.autoCollectionContext = autoCollectionContext;
	}

	public ApplicationContext getUbpContext() {
		return ubpContext;
	}

	public void setUbpContext(ApplicationContext ubpContext) {
		this.ubpContext = ubpContext;
	}

	public List<Menu> getMenus() {
		return menus;
	}

	public void setMenus(List<Menu> menus) {
		this.menus = menus;
	}

	public void setSapContext(ApplicationContext sapContext) {
		this.sapContext = sapContext;
	}

	public ApplicationContext getSapContext() {
		return sapContext;
	}

	public ApplicationContext getIbftPrimaContext() {
		return ibftPrimaContext;
	}

	public void setIbftPrimaContext(ApplicationContext ibftPrimaContext) {
		this.ibftPrimaContext = ibftPrimaContext;
	}

	public ApplicationContext getLosInsurance() {
		return losInsuranceContext;
	}

	public void setLosInsurance(ApplicationContext losInsurance) {
		this.losInsuranceContext = losInsurance;
	}

	public ApplicationContext getJebeAutopayContext() {
		return jebeAutopayContext;
	}

	public void setJebeAutopayContext(ApplicationContext jebeAutopayContext) {
		this.jebeAutopayContext = jebeAutopayContext;
	}

}
