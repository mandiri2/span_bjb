package com.mii.invoker;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

import com.mii.constant.Constants;
import com.mii.dao.SystemParameterDao;
import com.mii.is.util.PubUtil;
import com.mii.scoped.ApplicationBean;

@ManagedBean(name="postgresConnectionUtils")
@ApplicationScoped
public class PostgresConnectionUtils implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@ManagedProperty(value = "#{applicationBean}")
	private ApplicationBean applicationBean;
	
	private String hostName;
	private String dbName;
	private String userName; 
	private String password; 
	private String port;
	
	@PostConstruct
	public void init(){
		SystemParameterDao dao = (SystemParameterDao) applicationBean.getCorpGWContext().getBean("systemParameterDAO");
		hostName = dao.getDetailedParameterByParamName(Constants.DB_JASPER_HOST).getParamValue();
		port = dao.getDetailedParameterByParamName(Constants.DB_JASPER_PORT).getParamValue();
		dbName = dao.getDetailedParameterByParamName(Constants.DB_JASPER_DB_NAME).getParamValue();
		userName = dao.getDetailedParameterByParamName(Constants.DB_JASPER_UNAME).getParamValue();
		password = dao.getDetailedParameterByParamName(Constants.DB_JASPER_PASS).getParamValue();
	}
	
	public Connection getPostGressConnection() throws ClassNotFoundException, SQLException {
		return getMySQLConnection(hostName, port, dbName, userName, password);
	}

	public Connection getMySQLConnection(String hostName, String port, String dbName,
			String userName, String password) throws SQLException,
			ClassNotFoundException {

		Class.forName("org.postgresql.Driver");
		String connectionURL = PubUtil.concatString("jdbc:postgresql://", hostName, ":", port, "/", dbName);

		Connection conn = DriverManager.getConnection(connectionURL, userName,password);
		return conn;
	}
	
	/**GETTER SETTER**/
	public ApplicationBean getApplicationBean() {
		return applicationBean;
	}

	public void setApplicationBean(ApplicationBean applicationBean) {
		this.applicationBean = applicationBean;
	}




	public String getHostName() {
		return hostName;
	}




	public void setHostName(String hostName) {
		this.hostName = hostName;
	}




	public String getDbName() {
		return dbName;
	}




	public void setDbName(String dbName) {
		this.dbName = dbName;
	}




	public String getUserName() {
		return userName;
	}




	public void setUserName(String userName) {
		this.userName = userName;
	}




	public String getPassword() {
		return password;
	}




	public void setPassword(String password) {
		this.password = password;
	}




	public String getPort() {
		return port;
	}




	public void setPort(String port) {
		this.port = port;
	}

}
