package com.mii.invoker;


/**
 *
 */
public class DatabaseParameter {
	private SpanDataInvokeSOA spanDataInvokeSOA;
	
	public SpanDataInvokeSOA getSpanDataInvokeSOA() {
		return spanDataInvokeSOA;
	}
	
	public void setSpanDataInvokeSOA(SpanDataInvokeSOA spanDataInvokeSOA) {
		this.spanDataInvokeSOA = spanDataInvokeSOA;
	}
	
	private DatabaseParameter(){
		spanDataInvokeSOA = new SpanDataInvokeSOA();
		spanDataInvokeSOA.setIp("10.204.47.14:8883");
		spanDataInvokeSOA.setUsername("Administrator");
		spanDataInvokeSOA.setPassword("manage.mandiri123");
	}
	
    private static class InstanceHolder{
        private static final DatabaseParameter INSTANCE = new DatabaseParameter();
    }
    
    public static DatabaseParameter getInstance(){
        return InstanceHolder.INSTANCE;
    }
}
