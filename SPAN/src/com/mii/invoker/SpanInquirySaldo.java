package com.mii.invoker;

import com.wm.data.IData;
import com.wm.data.IDataCursor;
import com.wm.data.IDataFactory;
import com.wm.data.IDataUtil;

public class SpanInquirySaldo {
	private String status;
	private String errorMessage;
	private String availableBalance;
	private String l2TotalAmount;
	private String availableBalance2;
	private String accountNumber;
	
	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getAvailableBalance() {
		return availableBalance;
	}

	public void setAvailableBalance(String availableBalance) {
		this.availableBalance = availableBalance;
	}

	public String getL2TotalAmount() {
		return l2TotalAmount;
	}

	public void setL2TotalAmount(String l2TotalAmount) {
		this.l2TotalAmount = l2TotalAmount;
	}

	public String getAvailableBalance2() {
		return availableBalance2;
	}

	public void setAvailableBalance2(String availableBalance2) {
		this.availableBalance2 = availableBalance2;
	}

	public void doInquirySaldo(String accountNumber, SpanDataInvokeSOA sdi){
		
		IData inputInquiry = IDataFactory.create();
		IDataCursor inputCursorInquiry = inputInquiry.getCursor();
		IDataUtil.put( inputCursorInquiry, "account", accountNumber);
		inputCursorInquiry.destroy();
		IData outputInquiry= IDataFactory.create();
		
		outputInquiry= (IData)WMInvoker.doInvoke(sdi.getIp(),sdi.getUsername(),sdi.getPassword(),"mandiri.span.db.service.odbc","inquirySaldo",inputInquiry);
		
		IDataCursor outputCursorInquiry = outputInquiry.getCursor();
		setAccountNumber(accountNumber);
		setStatus(IDataUtil.getString( outputCursorInquiry, "Status"));
		setErrorMessage(IDataUtil.getString( outputCursorInquiry, "ErrorMessage" ));
		setAvailableBalance(IDataUtil.getString( outputCursorInquiry, "availableBalance"));
		setL2TotalAmount(IDataUtil.getString( outputCursorInquiry, "L2TotalAmount"));
		setAvailableBalance2(IDataUtil.getString( outputCursorInquiry, "availableBalance2"));
		
		outputCursorInquiry.destroy();		
	}

}
