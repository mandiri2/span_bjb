package com.mii.invoker;

/*import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;*/

import com.wm.app.b2b.client.Context;
import com.wm.data.IData;
/*import org.eclipse.persistence.internal.jpa.parsing.SubstringNode;*/

public class WMInvoker {
	private static WMInvoker wMInvoker;
	private static Context context;
	private static String server,username,password;
	public static WMInvoker getInstance(){
	     return (wMInvoker==null)?(wMInvoker=new WMInvoker()):wMInvoker;
	}
	static{
         context=new Context();
    }
	
	public synchronized static Object doInvoke(String ipServer,String username,String password,String packageName,String serviceName, IData inputDocument){
		IData outputDocument=null;
		ConnectToWM(ipServer,username,password);
		outputDocument=(IData)invokeWM(packageName,serviceName,inputDocument);
		DisconnectFromWM();
		return outputDocument;
	}
	
	public static void InvokeNoReturn(String ipServer,String username,String password,String packageName,String serviceName, IData inputDocument){
		//IData outputDocument=null;
		ConnectToWM(ipServer,username,password);
		invokeWMNoReturn(packageName,serviceName,inputDocument);
		DisconnectFromWM();
		
	}
	
	private static String ConnectToWM(String ipServer,String username,String password){
		String var=null;  
		try {
             if(!context.isConnected()){
//            	 System.out.println("connecting to..."+ipServer+":"+username+":"+password);
	             context.connect(ipServer,username,password);
	             var="can connect to WM";
//	             System.out.println("bisa konek");
	         }
          } catch (Exception e) {
        	  	 System.out.println("\n\tCannot connect to server \""+server+"\""+e);
	             e.printStackTrace();
	             var= "cannot connect to WM";   
	      }
		return var;
	 }
	
	

	public static Object invokeWM(String packageName,String serviceName, IData inputDocument){
	 	IData outputDocument=null;
	    try {
	    	System.out.println("invoking..."+packageName+":"+serviceName+"input"+inputDocument);
	        //outputDocument=context.invoke(packageName, serviceName,inputDocument);
	    	outputDocument = context.invoke(packageName, serviceName, inputDocument);
	    	System.out.println("output doc = "+outputDocument);
	    } catch (Exception e) {
	         e.printStackTrace();
	         System.out.println("Cannot invoke :"+packageName+":"+serviceName);
	         outputDocument=null;
	    }
	    return outputDocument;
	}
	
	public static void invokeWMNoReturn(String packageName,String serviceName, IData inputDocument){
	 	
	    try {
	    	System.out.println("invoking..."+packageName+":"+serviceName+"input"+inputDocument);
	        //outputDocument=context.invoke(packageName, serviceName,inputDocument);
	    	context.invoke(packageName, serviceName, inputDocument);
	    	
	    } catch (Exception e) {
	         e.printStackTrace();
	         System.out.println("Cannot invoke :"+packageName+":"+serviceName);
	         
	    }
	    
	}

	public static void DisconnectFromWM(){
	    if(context.isConnected())
	    	context.disconnect();
	}
}

