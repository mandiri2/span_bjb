package com.mii.invoker;

import java.awt.Container;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JFrame;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.view.JRViewer;

public class Report extends JFrame {
//	Map hm = null;
//	Connection con = null;
//	String reportName;
//
//	public Report() {
//		setExtendedState(MAXIMIZED_BOTH);
//		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
//
//	}
//
//	public Report(Map map) {
//		this.hm = map;
//		setExtendedState(MAXIMIZED_BOTH);
//		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
//
//	}
//
//	public Report(Map map, Connection con) {
//		this.hm = map;
//		this.con = con;
//		setExtendedState(MAXIMIZED_BOTH);
//		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
//		setTitle("Report Viewer");
//
//	}
//
//	public void setReportName(String rptName) {
//		this.reportName = rptName;
//	}
//
//	public void callReport() {
//		JasperPrint jasperPrint = generateReport();
//		JRViewer viewer = new JRViewer(jasperPrint);
//		Container c = getContentPane();
//		c.add(viewer);
//		this.setVisible(true);
//	}
//
//	public void callConnectionLessReport() {
//		JasperPrint jasperPrint = generateEmptyDataSourceReport();
//		JRViewer viewer = new JRViewer(jasperPrint);
//		Container c = getContentPane();
//		c.add(viewer);
//		this.setVisible(true);
//	}
//
//	public void closeReport() {
//		// jasperViewer.setVisible(false);
//	}
//
//	/** this method will call the report from data source */
//	public JasperPrint generateReport() {
//		
//		try {
//			
//			if (con == null) {
//				try {
//					con = PostgresConnectionUtils.getPostGressConnection();
//
//				} catch (Exception ex) {
//					ex.printStackTrace();
//				}
//			}
//			JasperPrint jasperPrint = null;
//			if (hm == null) {
//				hm = new HashMap<>();
//			}
//			try {
//
//				getClass().getClassLoader().getResource(".").getPath();
//				jasperPrint = JasperFillManager.fillReport(reportName
//						+ ".jasper", hm, con);
//
//				// exports report to pdf
//				JasperExportManager.exportReportToPdfFile(jasperPrint,
//						"E://sample_report.pdf");
//				
//			} catch (JRException e) {
//				e.printStackTrace();
//			}
//			return jasperPrint;
//		} catch (Exception ex) {
//			ex.printStackTrace();
//			return null;
//		} 
//
//	}
//
//	/** call this method when your report has an empty data source */
//	public JasperPrint generateEmptyDataSourceReport() {
//		try {
//			JasperPrint jasperPrint = null;
//			if (hm == null) {
//				hm = new HashMap();
//			}
//			try {
//				jasperPrint = JasperFillManager.fillReport(reportName
//						+ ".jasper", hm, new JREmptyDataSource());
//			} catch (JRException e) {
//				e.printStackTrace();
//			}
//			return jasperPrint;
//		} catch (Exception ex) {
//			ex.printStackTrace();
//			return null;
//		}
//
//	}
}
