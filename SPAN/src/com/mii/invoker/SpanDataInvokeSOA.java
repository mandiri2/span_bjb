package com.mii.invoker;

public class SpanDataInvokeSOA {
	private String ip;
	private String username;
	private String password;

	
	public void setIp(String ip){
		this.ip=ip;
	}
	
	public String getIp(){
		return ip;
	}
	
	public void setUsername(String username){
		this.username=username;
	}
	
	public String getUsername(){
		return username;
	}
	
	public void setPassword(String password){
		this.password=password;
	}
	
	public String getPassword(){
		return password;
	}
	
}
