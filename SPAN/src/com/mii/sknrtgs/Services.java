package com.mii.sknrtgs;

import javax.faces.bean.ManagedProperty;

import com.mii.dao.SystemParameterDao;
import com.mii.scoped.ApplicationBean;

public class Services {
	
	@ManagedProperty(value = "#{applicationBean}")
	private static ApplicationBean applicationBean;
	
	private static SystemParameterDao getSystemParameterDAOBean(){
		return (SystemParameterDao) applicationBean.getCorpGWContext().getBean("systemParameterDAO");
	}

	/**
	 * mandiri.span.sftp.services:uploadFile
	 */
	public static String uploadFile(String spanPath, String localPath,
			String fileName, String host, String username, String password) {
		String Status = null;

		Status = Atomic.uploadFile(spanPath, localPath, fileName, host,
				username, password);

		// branch on Status
		if ("true".equalsIgnoreCase(Status)) {
			Status = "SUCCESS";
		} else {
			// default
			Status = "Failed";
		}

		// debug log

		return Status;
	}

}
