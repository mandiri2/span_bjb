package com.mii.sknrtgs;

public class MappingDataACKSpanRequest {
	public ACKDataDocument  ackDataDocument;

	public ACKDataDocument getAckDataDocument() {
		return ackDataDocument;
	}

	public void setAckDataDocument(ACKDataDocument ackDataDocument) {
		this.ackDataDocument = ackDataDocument;
	}
	
}
