package com.mii.sknrtgs;

import com.mii.is.util.PubUtil;

public class Log {
	
	public static void debugLog(String sysID, String svcName, String type, String message){
		String concatMsg = PubUtil.concatString("[",sysID,"]","[",svcName,"]","[",type,"]",message);
		System.out.println(concatMsg);
	}
	
}
