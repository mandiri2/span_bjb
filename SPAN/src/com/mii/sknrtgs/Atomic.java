package com.mii.sknrtgs;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.CRC32;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import com.mii.helpers.FTPHelper;

public class Atomic {
	
	/**
	 * mandiri.span.sftp.atomic:getFiles
	 * @param spanPath
	 * @param localPath
	 * @param host
	 * @param username
	 * @param password
	 * @param totalFileNames
	 * @return
	 */
	public static String[] getFiles(String spanPath, String localPath, String host, String username, String password, int totalFileNames){
		String fileNames [] = null;
		try{
			fileNames = FTPHelper.getFileNames(host, username, password, spanPath, localPath, totalFileNames);
//			fileNames = SFTPClient.getFileNames(host, username, password, spanPath, localPath, totalFileNames);
			System.out.println("fileNames size : "+fileNames.length);
		}catch(Exception e){
			e.printStackTrace();
		}
		return fileNames;
	}
	
	/**
	 * Get Files From Local
	 */
	public static String[] getLocalFilesManual(String localPathManual){
		String fileNames [] = null;
		try{
			File folder = new File(localPathManual);
			File[] listOfFiles = folder.listFiles();
			for (int i = 0; i < listOfFiles.length; i++) {
		      if (listOfFiles[i].isFile()) {
		    	  fileNames = new String [listOfFiles.length];
		    	  fileNames[i] = listOfFiles[i].getName();
		    	  System.out.println("File " + listOfFiles[i].getName());
		      }
		    }
		}catch(Exception e){
			e.printStackTrace();
		}
		return fileNames;
	}
	
	/**
	 * mandiri.span.sftp.atomic:uploadFile
	 * 
	 */
	public static String uploadFile(String spanPath, String localPath, String fileName, String host, String username, String password){
		String Status = null;
		
		boolean status = true;
		
		try{
			status = FTPHelper.uploadFileFTP(host, username, password, spanPath, localPath, fileName);
//			status = SFTPClient.uploadFileSFTP(host, username, password, spanPath, localPath, fileName);
			System.out.println("File Sent.");
		}catch(Exception e){
			System.out.println("Failed to Send.");
			e.printStackTrace();
		}
		
		Status = String.valueOf(status);
		
		return Status;
	}
	
	/**
	 * Delete File Local
	 */
	public static String deleteFileLocalManual(String localPathManual, String fileName, String localPathManualBackup){
		//BACKUP
		try{
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
			InputStream inStream = null;
			OutputStream outStream = null;
			File afile =new File(localPathManual.concat(fileName));
    	    File bfile =new File(localPathManualBackup.concat(fileName.concat(sdf.format(new Date()))));

    	    inStream = new FileInputStream(afile);
    	    outStream = new FileOutputStream(bfile);

    	    byte[] buffer = new byte[1024];

    	    int length;
    	    //copy the file content in bytes
    	    while ((length = inStream.read(buffer)) > 0){

    	    	outStream.write(buffer, 0, length);

    	    }

    	    inStream.close();
    	    outStream.close();

    	    System.out.println("File is backup successful!");
		}catch(Exception e){
			System.out.println("Failed To Backup Manual Upload File.");
			e.printStackTrace();
		}
		//DELETE
		boolean status = true;
		File file = new File(localPathManual.concat(fileName));
		if(file.delete()){
			System.out.println(file.getName() + " Berhasil dihapus dari folder local upload manual.");
		}else{
			status = false;
		}
		return String.valueOf(status);
	}
	
	/**
	 * mandiri.span.util.atomic:createAndExtractJAR
	 * return String[] <br />
	 * string[0] Status <br />
	 * string[1] ErrorMessage <br />
	 * @throws IOException 
	 */
	public static String[] createAndExtractJAR(String zipFileName, String localPath, String zipType) throws IOException{
		String[] strings = new String[2];
		
		String Status = "SUCCESS";
		String ErrorMessage = "";
		
		if("C".equals(zipType)){
			String fileName = "";
			FileOutputStream fileOutputStream = null;
			FileInputStream fis = null;
			ZipOutputStream s = null;
			try{
				CRC32 crc = new CRC32();
				zipFileName = zipFileName.replace("txt","jar");
				fileOutputStream = new FileOutputStream(localPath.concat(zipFileName));
				s = new ZipOutputStream((OutputStream)fileOutputStream);
			    	fileName = zipFileName.replace("jar","xml");
			    	fileName = zipFileName.replace("jar","txt");
				s.setLevel(6);
				fis = null;
				ZipEntry entry = null;
			    
		//		int newFileName = localPath.indexOf("SPAN")+5+9;//"TempFile"
				int newFileName = localPath.length();
		
				String[] filenames = new String[2];
				filenames[0] = localPath.concat("META-INF/MANIFEST.MF");
				filenames[1] = localPath.concat(fileName);
				byte[] buf;
		
				File f = null; 
				long fl = 0;
		
				for(int x=0; x<filenames.length; x++){
		
					f = new File(filenames[x]);
					fl = f.length();
					buf = new byte[Integer.parseInt(String.valueOf(f.length()))];
		
					fis = new FileInputStream(filenames[x]);
			    		fis.read(buf, 0, buf.length);
		
				    	entry = new ZipEntry(filenames[x].substring(newFileName));
		
				    	entry.setSize((long) buf.length);
			    		crc.reset();
				    	crc.update(buf);
				    	entry.setCrc(crc.getValue());
			    		s.putNextEntry(entry);
				    	s.write(buf, 0, buf.length);	    	
				    }
		
				s.finish();
		    		s.close();
				
			}catch(Exception e){
				Status = "ERROR";
				ErrorMessage = e.getMessage();
				e.printStackTrace();
			}finally{
				if(fileOutputStream!=null)fileOutputStream.close();
				if(fis!=null)fis.close();
				if(s!=null)s.close();
			}
			
		}else if("E".equals(zipType)){
			try{
				//localPath = "C:\\Data\\Temp\\DataTest\\SPAN\\";
				//zipFileName = "810000980990_SP2D_O_20121011_121048_002.jar";
				    
				byte[] buf = new byte[1024];
				    
				ZipInputStream zipinputstream = new ZipInputStream(new FileInputStream(localPath.concat(zipFileName)));
				ZipEntry zipentry;
			//System.out.println("zip input stream = "+zipinputstream);
						    
				String entryName = "";
				String directory = "";
		
				boolean dirStatus = true;
				    
				File newFile = null;
				FileOutputStream fileoutputstream;
				//System.out.println("masuk sebelum while");
				while (true) {
				
					zipentry = zipinputstream.getNextEntry();
				
				    	if(zipentry==null)break;
				    	
				    	entryName = zipentry.getName();
				
				    	newFile = new File(localPath.concat(entryName));
				    	directory = newFile.getParent();
				      
				    	if (entryName.startsWith("META-INF")) {
				    		if(dirStatus){/*DO NOTHING*/}
				    	  	else dirStatus = false;
				    	}else{
				    		fileoutputstream = new FileOutputStream(localPath.concat(entryName));
				    		int n;
				    		while ((n = zipinputstream.read(buf, 0, 1024)) > -1){
				    			fileoutputstream.write(buf, 0, n);
				    		}
				    		fileoutputstream.close();
				    		zipinputstream.closeEntry();
				    		zipentry = zipinputstream.getNextEntry();
				    	}
				}
				zipinputstream.close();
				
			}catch(Exception e){
				Status = "ERROR";
				ErrorMessage = e.getMessage();
				e.printStackTrace();
			}
		
		}
		
		strings[0] = Status;
		strings[1] = ErrorMessage;
		return strings;
	}
	
	/**
	 * mandiri.span.util.atomic:ACKStringToBytes <br />
	 * return Object[] <br />
	 * Object[0] ACKbytes <br />
	 * Object[1] Status <br />
	 * Object[2] ErrorMessage <br />
	 */
	public static Object[] ACKStringToBytes(String string){
		
		Object[] objects = new Object[3];
		
		
		String sourceString = string;
		byte[] bytes = null;
		String Status = "";
		String ErrorMessage = "";
		
		try{
			bytes = sourceString.getBytes();
			Status = "SUCCESS";
		}catch(Exception e){
			e.printStackTrace();
			Status = "ERROR";
			ErrorMessage = e.getMessage();
		}
		
		objects[0] = bytes;
		objects[1] = Status;
		objects[2] = ErrorMessage;
		
		return objects;
	}
}
