package com.mii.sknrtgs;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.mii.is.util.PubUtil;

public class BusinessUtil {
	
	public static List<String> getFolderName(String spanPath){
		List<String> spanFolderNames = Arrays.asList(PubUtil.tokenize(spanPath, "|"));
		int iteration = 1;
		String mainPath = null;
		List<String> newSpanFolderNames = new ArrayList<String>();
		
		for(String spanFolderName : spanFolderNames){
			if(iteration==1){
				mainPath =  spanFolderName;
			}
			else{
				newSpanFolderNames.add(spanFolderName);
			}
			iteration++;
		}
		
		spanFolderNames = new ArrayList<String>();
		
		for(String newSpanFolderName : newSpanFolderNames){
			newSpanFolderName = PubUtil.concatString(mainPath,"/",newSpanFolderName);
			spanFolderNames.add(newSpanFolderName);
		}
		
		return spanFolderNames;
	}
	
	/**
	 * Return String jika object adalah instance of String dan tidak null
	 */
	public static String stringIfNotNull(Object object){
		if(object!=null
				&& object instanceof String){
			return (String)object;
		}
		return null;
	}
	
	/**
	 * return byte[] if not null
	 */
	public static byte[] bytesIfNotNull(Object value){
		if(value instanceof byte[]){
			return (byte[])value;
		}
		return null;
	}
	
	/**
	 * mandiri.span.util:renameFileName
	 */
	public static String renameFileName(String filename, String Customer){
		
		filename = filename.replace("FTP_", "");
		
		int dotValue = filename.indexOf(".");
		int underValue = filename.indexOf("_");
		String date = getDateByPattern("yyyyMMdd");
		String time = getDateByPattern("HHmmss");
		
		String newFileName = filename.substring(0, dotValue);
		String extFileName = filename.substring(dotValue);
		Customer = PubUtil.concatString("_",Customer,"_",date);
		String prefixFileName = filename.substring(0, underValue);
		
		newFileName = PubUtil.concatString(prefixFileName, Customer);
		extFileName = PubUtil.concatString("_",time,extFileName);
		
		newFileName = PubUtil.concatString(newFileName, extFileName);
		
		return newFileName;
	}
	
	public static String getDateByPattern(String pattren){
		if(pattren!=null){
			SimpleDateFormat sdf = new SimpleDateFormat(pattren);
			try {
				String date = sdf.format(new Date());
				return date;
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
			
		}
		return null;
		
	}
}
