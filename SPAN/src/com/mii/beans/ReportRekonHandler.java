package com.mii.beans;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.apache.commons.lang.SerializationUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.util.StringUtils;

import com.mii.constant.Constants;
import com.mii.dao.ApprovalDao;
import com.mii.dao.ReconDao;
import com.mii.helpers.FacesUtil;
import com.mii.helpers.HelperUtils;
import com.mii.models.Approval;
import com.mii.models.Recon;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.SessionBean;

@ManagedBean
@ViewScoped
public class ReportRekonHandler implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private Recon recon;
	
	private String  caName, settlementDate, reconTime, billingCode,
		 ntb, ntpn, noSakti;

	private LazyDataModel<Recon> reconList;

	@ManagedProperty(value="#{applicationBean}")
	private ApplicationBean applicationBean;
	
	@ManagedProperty(value="#{sessionBean}")
	private SessionBean sessionBean;
	
	private boolean doShowSearch = false;
	
	@PostConstruct
	public void init(){
		FacesUtil.authMenu(sessionBean.getUser(), "reportRekon");

		reconList = new ReconDataModel();

		caName			= "";
		settlementDate	= "";
		reconTime		= "";
		billingCode		= "";
		ntb				= "";
		ntpn			= "";
		noSakti			= "";
		
		doSearch();
	}
	

	public ApplicationBean getApplicationBean() {
		return applicationBean;
	}

	public void setApplicationBean(ApplicationBean applicationBean) {
		this.applicationBean = applicationBean;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public String getCaName() {
		return caName;
	}


	public void setCaName(String caName) {
		this.caName = caName;
	}


	public String getSettlementDate() {
		return settlementDate;
	}


	public void setSettlementDate(String settlementDate) {
		this.settlementDate = settlementDate;
	}


	public String getReconTime() {
		return reconTime;
	}


	public void setReconTime(String reconTime) {
		this.reconTime = reconTime;
	}


	public String getBillingCode() {
		return billingCode;
	}


	public void setBillingCode(String billingCode) {
		this.billingCode = billingCode;
	}


	public String getNtb() {
		return ntb;
	}


	public void setNtb(String ntb) {
		this.ntb = ntb;
	}


	public String getNtpn() {
		return ntpn;
	}


	public void setNtpn(String ntpn) {
		this.ntpn = ntpn;
	}


	public String getNoSakti() {
		return noSakti;
	}


	public void setNoSakti(String noSakti) {
		this.noSakti = noSakti;
	}


	public Recon getRecon() {
		return recon;
	}
	
	public void setRecon(Recon recon) {
		this.recon = recon;
	}

	public LazyDataModel<Recon> getReconList() {
		return reconList;
	}

	public void setReconList(LazyDataModel<Recon> reconList) {
		this.reconList = reconList;
	}
	
	public boolean isDoShowSearch() {
		return doShowSearch;
	}

	public void setDoShowSearch(boolean doShowSearch) {
		this.doShowSearch = doShowSearch;
	}

	public void showSearch(){
		doShowSearch = true;
	}
	
	public void hideSearch(){
		doShowSearch = false;
	}
	
	public void doSearch(){
		

		FacesUtil.resetPage("reconForm:reconTable");
		reconList = new ReconDataModel();	
	}
	
	private class ReconDataModel extends LazyDataModel < Recon > {
		private static final long serialVersionUID = 1L;

		public ReconDataModel() {

		}

		@Override
		public List < Recon > load(int startingAt, int maxPerPage,
		String sortField, SortOrder sortOrder,
			Map < String, String > filters) {
	
			ReconDao reconDao = (ReconDao) applicationBean.getCorpGWContext().getBean("reconDAO");
			List < Recon > recon = null;
	
			recon = reconDao.getSearchReconLazyLoading(caName, settlementDate, reconTime, billingCode,
					 ntb, ntpn, noSakti, maxPerPage + startingAt, startingAt);
			Integer jmlAll = reconDao.countSearchRecon(caName, settlementDate, reconTime, billingCode,
					 ntb, ntpn, noSakti);
			setRowCount(jmlAll);
			setPageSize(maxPerPage);
				
			return recon;
		}
	}
	

}
