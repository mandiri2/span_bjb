package com.mii.beans;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.NumberToTextConverter;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Rectangle;
import com.mii.bjb.core.FastBJBConnector;
import com.mii.constant.Constants;
import com.mii.constant.ConstantsUserLog;
import com.mii.dao.FASTBJBDao;
import com.mii.dao.SystemParameterDao;
import com.mii.dao.UserDao;
import com.mii.helpers.FacesUtil;
import com.mii.is.util.PubUtil;
import com.mii.models.AccountDataValidation;
import com.mii.models.UserLog;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.SessionBean;

@ManagedBean
@ViewScoped
public class DataValidationDepkeuHandler implements Serializable{
	private static final long serialVersionUID = 1L;
	private String checkValue;
	
	private LazyDataModel < AccountDataValidation > accountValidationList;
	List<AccountDataValidation> resultTemps = new ArrayList<AccountDataValidation>();
	List<AccountDataValidation> accountDataValidationList = new ArrayList<AccountDataValidation>();
	private String accountNumber;
	private String accountName;
	private UploadedFile fileUpload;
	private List<String> listError;
	
	private StreamedContent fileContoh;
	
	private Boolean doShowSingle;
	private Boolean doShowBulk;
	
	private int count=0;
	
	@ManagedProperty(value="#{applicationBean}")
	private ApplicationBean applicationBean;
	
	@ManagedProperty(value="#{sessionBean}")
	private SessionBean sessionBean;
	
	@PostConstruct
	public void init(){
		FacesUtil.authMenu(getSessionBean().getUser(), "dataValidationDepkeu");
		checkValue="";
		listError = new ArrayList<String>();
		doLoging(ConstantsUserLog.MENU_DATA_VALIDATION,
				ConstantsUserLog.ACTIVITY_INIT, "Menu Init.");
	}
	
	public void doCheckChange(){
		if(checkValue.equals("0")){
			doShowSingle = true;
			doShowBulk = false;
		}else if(checkValue.equals("1")){
			doShowSingle = false;
			doShowBulk = true;
		}else{
			doShowSingle = false;
			doShowBulk = false;
		}
	}
	
	public void doInquiry(){
		accountValidationList=null;
		accountDataValidationList.clear();
		resultTemps.clear();;
		listError.clear();;
		if(checkValue.equals("0")){
			if(accountName!=null&!accountName.equals("")&accountNumber!=null&!accountNumber.equals("")){
				inquirySingle();
			}else{
				String headerMessage = "Error";
				String bodyMessage = "";
				if(accountName==null&accountName.equals("")){bodyMessage = bodyMessage.concat("Account Name is empty. ");}
				if(accountNumber==null&accountNumber.equals("")){bodyMessage = bodyMessage.concat("Account Number is empty. ");}
				bodyMessage = bodyMessage.concat("Failed. ");
				FacesUtil.setErrorMessageDialog(headerMessage, bodyMessage);
			}
		}else if(checkValue.equals("1")){
			System.out.println(fileUpload.getFileName());
			inquiryBulk();
		}else{
			String headerMessage = "Error";
			String bodyMessage = "Search Mode is Null, please refresh this menu and try again.";
			FacesUtil.setErrorMessageDialog(headerMessage, bodyMessage);
		}
	}

	private void inquirySingle() {
		AccountDataValidation adv = new AccountDataValidation();
		adv.setRecordNo("1");
		adv.setAccountName(accountName);
		adv.setAccountNumber(accountNumber);
		resultTemps.add(adv);
		
		accountValidationList = new DetailDataModel();
		
		doLoging(ConstantsUserLog.MENU_DATA_VALIDATION,
				ConstantsUserLog.ACTIVITY_SINGLE_DATA_VALIDATION, "Inquiry Single : ".concat(accountName).concat(" - ").concat(accountNumber));
	}
	
	private void inquiryBulk() {
		if (!(fileUpload.getFileName().equals(null)||fileUpload.getFileName().equals(""))) {
			String fileName = fileUpload.getFileName();
			String extension = fileName.substring(fileName.lastIndexOf("."));
			if (StringUtils.equalsIgnoreCase(extension, ".xls")) {
				if(fileUpload.getSize()<=1000000){
					try{
						extractFile();
						resetCount(0);
						fileUpload.getInputstream().close();
						String bodyMessage="File Extracted";
						FacesUtil.setInfoMessage(null, bodyMessage, bodyMessage);
						doLoging(ConstantsUserLog.MENU_DATA_VALIDATION,
								ConstantsUserLog.ACTIVITY_BULK_DATA_VALIDATION, "Inquiry Bulk : ".concat(fileUpload.getFileName()));
					}catch(Exception e){
				    	String bodyMessage ="";
				    	if(this.count>0){
				    		bodyMessage="Got Error on file row number "+count+", "+e.getMessage();
				    	}else{
				    		bodyMessage=e.getMessage();
				    	}
				    	System.out.println(bodyMessage);
						FacesUtil.setFatalMessage(null,bodyMessage, bodyMessage);
						resetCount(0);
				    }
				}else{
					FacesUtil.setFatalMessage(null, "Maximum file size is 1MB", null);
				}
			}else{
				FacesUtil.setFatalMessage(null, fileUpload.getFileName()+" is not .xls type", null);
			}
		}else{
			FacesUtil.setFatalMessage(null, "File is required", null);
		}
	}

	private void resetCount(int value) {
		this.count=value;
	}

	private void extractFile() throws IOException {
		BufferedInputStream input = new BufferedInputStream(this.fileUpload.getInputstream());
		POIFSFileSystem fs = new POIFSFileSystem( input );
		HSSFWorkbook wb = new HSSFWorkbook(fs);
		HSSFSheet sheet = wb.getSheetAt(0);
		int i=0;
		for (Row myrow : sheet) {
			
			if(i>0){
				AccountDataValidation adv= new AccountDataValidation();
				adv=extractCell(myrow);
				resultTemps.add(adv);
			}
			
			i++;
			count++;
		}
		if(listError.size()>0){
			resultTemps.clear();
		}else{
			accountValidationList = new DetailDataModel();
		}
		wb.close();
		
	}

	private AccountDataValidation extractCell(Row myrow) {
		AccountDataValidation adv = new AccountDataValidation();
		int cellCounter=0;
		for (Cell mycell : myrow) {
			switch (cellCounter) {
			case 0:
				adv.setRecordNo(getValueCell(mycell));
				break;
			case 1:
				adv.setAccountNumber(getValueCell(mycell));
				break;
			case 2:
				adv.setAccountName(getValueCell(mycell));
				break;
			default:
				listError.add("Unexpected Cell Found in Row "+count);
				break;
			}
			cellCounter++;
			
		}
		return adv;
	}

	private String getValueCell(Cell mycell) {
		String value="";
		switch (mycell.getCellType()) {
		case Cell.CELL_TYPE_NUMERIC:
			value=NumberToTextConverter.toText(mycell.getNumericCellValue());
			break;
		case Cell.CELL_TYPE_STRING:
			value=String.valueOf(mycell.getStringCellValue());
			break;
		default:
			listError.add("Error at row "+count+ " : Account Number is Required; ");
			value="";
			break;
		}
		return value;
	}
	
	private class DetailDataModel extends LazyDataModel < AccountDataValidation > {
		private static final long serialVersionUID = 1L;

		public DetailDataModel() {

		}

		@Override
		public List < AccountDataValidation > load(int startingAt, int maxPerPage,
		String sortField, SortOrder sortOrder,
		Map < String, String > filters) {
			
			for(AccountDataValidation adv : resultTemps){
				Timestamp now = new Timestamp(Calendar.getInstance().getTime().getTime());
				String status = FastBJBConnector.CheckAccountBJB(getFASTBJBDaoBean(), adv.getAccountName(), adv.getAccountNumber(), getSystemParameterDaoBean());
				Timestamp now2 = new Timestamp(Calendar.getInstance().getTime().getTime());
				System.out.println("start : "+now);
				System.out.println("end   : "+now2);
				System.out.println("status : "+status);
				adv.setStatus(status);
				accountDataValidationList.add(adv);
			}
			setRowCount(resultTemps.size());
			setPageSize(maxPerPage);
			return accountDataValidationList;
		}
	}
	
	private FASTBJBDao getFASTBJBDaoBean(){
		return (FASTBJBDao) getApplicationBean().getCorpGWContext().getBean("FASTBJBDao");
	}
	
	private SystemParameterDao getSystemParameterDaoBean(){
		return (SystemParameterDao) getApplicationBean().getCorpGWContext().getBean("systemParameterDAO");
	}
	
	public void downloadContoh(){
		InputStream stream = FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("/resources/template/DataValidationTemplate.xls");
        fileContoh = new DefaultStreamedContent(stream, "application/xls", "DataValidationTemplate.xls");
    }
	
	
	
	public void downloadPdf() {
		String fileName = "Data_validation.pdf";
		String jasperPath = FacesUtil.getExternalContext().getRealPath("resources/report/Data_validation.jasper");
		
		PubUtil.generateReport(fileName, jasperPath, Constants.PDF, null, Constants.CONTENT_TYPE_PDF,accountDataValidationList);
	}

	public void downloadXls() {
		String fileName = "Data_Validation.xls";
		String jasperPath = FacesUtil.getExternalContext().getRealPath("resources/report/Data_validation_XLS.jasper");
		
		PubUtil.generateReport(fileName, jasperPath, Constants.EXCEL, null, Constants.CONTENT_TYPE_XLS,accountDataValidationList);
	}
	
	
	
	public void preProcessPDF(Object document) throws IOException,  
    BadElementException, DocumentException{
        Document pdf = (Document) document;
        Rectangle r = new Rectangle(2000f, 1000f);
        pdf.setPageSize(r);
        pdf.setMargins(0f, 0f, 100f, 100f);
    }
	
	private void doLoging(String menu, String activity, String information) {
		UserLog userLog = new UserLog();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		userLog.setLogId("LOG-" + sdf.format(new Date()) + "-" + sessionBean.getUser().getUserID());
		userLog.setActivity(activity);
		userLog.setInformation(information);
		userLog.setInsertTime(new Date());
		userLog.setMenu(menu);
		userLog.setUserId(sessionBean.getUser().getUserID());
		userLog.setUserName(sessionBean.getUser().getUserName());
		UserLogHandler.doLog(userLog, getUserDAOBean());
	}
	
	private UserDao getUserDAOBean() {
		return (UserDao) applicationBean.getCorpGWContext().getBean("userDAO");
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public ApplicationBean getApplicationBean() {
		return applicationBean;
	}

	public void setApplicationBean(ApplicationBean applicationBean) {
		this.applicationBean = applicationBean;
	}

	public Boolean getDoShowSingle() {
		return doShowSingle;
	}

	public void setDoShowSingle(Boolean doShowSingle) {
		this.doShowSingle = doShowSingle;
	}

	public Boolean getDoShowBulk() {
		return doShowBulk;
	}

	public void setDoShowBulk(Boolean doShowBulk) {
		this.doShowBulk = doShowBulk;
	}

	public String getCheckValue() {
		return checkValue;
	}

	public void setCheckValue(String checkValue) {
		this.checkValue = checkValue;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public UploadedFile getFileUpload() {
		return fileUpload;
	}

	public void setFileUpload(UploadedFile fileUpload) {
		this.fileUpload = fileUpload;
	}

	public LazyDataModel < AccountDataValidation > getAccountValidationList() {
		return accountValidationList;
	}

	public void setAccountValidationList(LazyDataModel < AccountDataValidation > accountValidationList) {
		this.accountValidationList = accountValidationList;
	}

	public StreamedContent getFileContoh() {
		return fileContoh;
	}

	public void setFileContoh(StreamedContent fileContoh) {
		this.fileContoh = fileContoh;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

}
