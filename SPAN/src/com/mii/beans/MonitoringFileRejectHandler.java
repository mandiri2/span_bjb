package com.mii.beans;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.apache.commons.lang.SerializationUtils;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.UploadedFile;

import com.mii.constant.Constants;
import com.mii.constant.ConstantsUserLog;
import com.mii.dao.SPANDataValidationDao;
import com.mii.dao.SystemParameterDao;
import com.mii.dao.UserDao;
import com.mii.helpers.CreateJarFile;
import com.mii.helpers.FacesUtil;
import com.mii.helpers.HelperUtils;
import com.mii.invoker.DatabaseParameter;
import com.mii.invoker.SpanDataInvokeSOA;
import com.mii.invoker.SpanInquirySaldo;
import com.mii.json.AccountBalance;
import com.mii.models.SPANDataValidation;
import com.mii.models.SpanRekeningKoran;
import com.mii.models.SystemParameter;
import com.mii.models.UserLog;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.SessionBean;
import com.zehon.FileTransferStatus;
import com.zehon.exception.FileTransferException;
import com.zehon.ftp.FTP;

@ManagedBean
@ViewScoped
public class MonitoringFileRejectHandler implements Serializable {
	private static final long serialVersionUID = 1L;

	private SPANDataValidation spanDataValidation;
	private LazyDataModel<SPANDataValidation> validationList;
	private List<SPANDataValidation> tempList;
	private Boolean doShowGAJI;
	private Boolean doShowGAJIR;
	private String tabSelected; // GAJI - GAJIR

	private String availableBalance;
	private String accountNumber;
	private String totalRecords;
	private String totalAmount;

	private String hiddenSelectedAmount = "0";

	private UploadedFile file;

	@ManagedProperty(value = "#{applicationBean}")
	private ApplicationBean applicationBean;

	@ManagedProperty(value = "#{sessionBean}")
	private SessionBean sessionBean;

	public static Map<String, SPANDataValidation> resultTemps = new HashMap<String, SPANDataValidation>();

	@PostConstruct
	public void init() {
		FacesUtil.authMenu(sessionBean.getUser(), "monitoringFileReject");
		doClickGaji();
	}

	public void doClickGaji() {
		doShowGAJI = true;
		doShowGAJIR = false;
		initChangeTab();
		tabSelected = Constants.GAJI;
		loadData();
		InquirySaldo();
		doLoging(ConstantsUserLog.MENU_MONITORING_FILE_REJECT,
				tabSelected.concat(" ").concat(ConstantsUserLog.ACTIVITY_INIT), "Tab " + tabSelected + " Init.");
	}

	public void doClickGajiR() {
		doShowGAJI = false;
		doShowGAJIR = true;
		initChangeTab();
		tabSelected = Constants.GAJIR;
		loadData();
		InquirySaldo();
		doLoging(ConstantsUserLog.MENU_MONITORING_FILE_REJECT,
				tabSelected.concat(" ").concat(ConstantsUserLog.ACTIVITY_INIT), "Tab " + tabSelected + " Init.");
	}

	private void initChangeTab() {
		resultTemps.clear();
		setValidationList(null);
		totalRecords = "0";
		totalAmount = "0.00";
		availableBalance = null;
		accountNumber = null;
		hiddenSelectedAmount = "0";
		tempList = new ArrayList<SPANDataValidation>();
	}

	public void processChecked(SPANDataValidation result) {
		SPANDataValidation r = new SPANDataValidation();
		r = (SPANDataValidation) SerializationUtils.clone(result);
		if (result.isSelected()) {
			resultTemps.put(result.getDocNumber(), result);
			try {
				totalRecords = String.valueOf((Integer.parseInt(totalRecords) + 1));
				// selectedAmount = String.format("%.2f",
				// ((Float.parseFloat(selectedAmount) +
				// Float.parseFloat(result.getTotalAmount()))));
				DecimalFormat df = new DecimalFormat("###,###.00");
				BigInteger sum = new BigInteger(getHiddenSelectedAmount());
				sum = sum.add(new BigInteger(result.getAmount()));
				totalAmount = (df.format(sum));
				setHiddenSelectedAmount(sum.toString());
				if (totalAmount.equals(".00")) {
					totalAmount = "0.00";
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			resultTemps.remove(result.getDocNumber());
			try {
				totalRecords = String.valueOf((Integer.parseInt(totalRecords) - 1));
				// selectedAmount = String.format("%.2f",
				// ((Float.parseFloat(selectedAmount) +
				// Float.parseFloat(result.getTotalAmount()))));
				DecimalFormat df = new DecimalFormat("###,###.00");
				BigInteger sum = new BigInteger(getHiddenSelectedAmount());
				sum = sum.subtract(new BigInteger(result.getAmount()));
				totalAmount = (df.format(sum));
				setHiddenSelectedAmount(sum.toString());
				if (totalAmount.equals(".00")) {
					totalAmount = "0.00";
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void loadData() {
		setValidationList(new DetailDataModel());
	}

	private class DetailDataModel extends LazyDataModel<SPANDataValidation> {
		private static final long serialVersionUID = 1L;

		public DetailDataModel() {

		}

		@SuppressWarnings("rawtypes")
		@Override
		public List<SPANDataValidation> load(int startingAt, int maxPerPage, String sortField, SortOrder sortOrder,
				Map<String, String> filters) {
			tempList.clear();
			SPANDataValidationDao validationDao = (SPANDataValidationDao) applicationBean.getCorpGWContext().getBean(
					"SPANDataValidationDao");
			SystemParameterDao parameterDao = (SystemParameterDao) applicationBean.getCorpGWContext().getBean(
					"systemParameterDAO");
			List<SPANDataValidation> details = new ArrayList<SPANDataValidation>();
			SystemParameter parameter = parameterDao.getDetailedParameterByParamName(Constants.PARAM_DEBITACCNO
					+ tabSelected);
			details = validationDao.selectSpanDataValidationLateGAJILazyLoading(parameter.getParamValue(), "0",
					maxPerPage + startingAt, startingAt);
			Integer jmlAll = validationDao.countSpanDataValidationLateGAJI(parameter.getParamValue(), "0");
			tempList = details;
			setRowCount(jmlAll);
			setPageSize(maxPerPage);
			List<SPANDataValidation> detailsFinal = new ArrayList<SPANDataValidation>();
			for (SPANDataValidation a : details) {
				Iterator entries = resultTemps.entrySet().iterator();
				while (entries.hasNext()) {
					Entry thisEntry = (Entry) entries.next();
					SPANDataValidation s = (SPANDataValidation) thisEntry.getValue();
					if (a.getDocNumber().equals(s.getDocNumber())) {
						if (s.isSelected()) {
							a.setSelected(true);
						} else {
							a.setSelected(false);
						}
					}
				}
				detailsFinal.add(a);
			}
			if (detailsFinal.size() > 0) {
				return detailsFinal;
			} else {
				return null;
			}
		}
	}

	public void InquirySaldo() {
		SystemParameterDao parameterDao = (SystemParameterDao) applicationBean.getCorpGWContext().getBean(
				"systemParameterDAO");
		SystemParameter parameter = parameterDao.getDetailedParameterByParamName(Constants.PARAM_DEBITACCNO
				+ tabSelected);
		accountNumber = parameter.getParamValue();
		try {
			String availableBalance = AccountBalance.getAccountBalance(parameterDao, accountNumber);
			// setAvailableBalance(String.format("%1$,.2f",Float.parseFloat(availableBalance)));
			// //DO NOT USE - ERROR!!!!
			DecimalFormat df = new DecimalFormat("###,###.00");
			setAvailableBalance(df.format(new BigInteger((availableBalance))));
			if (availableBalance.equals(".00")) {
				setAvailableBalance("Error");
			}
		} catch (Exception e) {
			setAvailableBalance("Error");
		}
	}

	public void doClickApproveAll() {
		for (SPANDataValidation s : tempList) {
			s.setSelected(true);
			if (!resultTemps.containsKey(s.getDocNumber())) {
				resultTemps.put(s.getDocNumber(), s);
			} else {
				resultTemps.remove(s.getDocNumber());
				resultTemps.put(s.getDocNumber(), s);
			}
		}
		setValidationList(null);
		loadData();
	}

	@SuppressWarnings("rawtypes")
	public void doClickApproveChecked() {
		if (resultTemps.size() > 0) {
			//check available balance
			if(checkInquiryStatus()){
				if (resultTemps.size() <= Constants.MAXAPPROVELATEFILE) {
					List<String> docNumberList = new ArrayList<String>();
					Iterator entries = resultTemps.entrySet().iterator();
					while (entries.hasNext()) {
						Entry thisEntry = (Entry) entries.next();
						SPANDataValidation s = (SPANDataValidation) thisEntry.getValue();
						docNumberList.add(s.getDocNumber());
						doLoging(ConstantsUserLog.MENU_MONITORING_FILE_REJECT,
								tabSelected.concat(" ").concat(ConstantsUserLog.ACTIVITY_APPROVE), "Tab " + tabSelected
								+ ". Approve : " + s.getDocNumber() + ".");
					}
					SPANDataValidationDao spanDataValidationDao = (SPANDataValidationDao) applicationBean
							.getCorpGWContext().getBean("SPANDataValidationDao");
					spanDataValidationDao.updateLateFileGaji(docNumberList);
					spanDataValidationDao.updateFlagLateFile(docNumberList);

					// Finally
					if (tabSelected.equals(Constants.GAJI)) {
						doClickGaji();
					} else {
						doClickGajiR();
					}
				} else {
					String headerMessage = "Error";
					String bodyMessage = "Selected record more than " + Constants.MAXAPPROVELATEFILE + " record. ";
					bodyMessage = bodyMessage.concat("Failed. ");
					FacesUtil.setErrorMessageDialog(headerMessage, bodyMessage);
				}
			}
		} else {
			String headerMessage = "Error";
			String bodyMessage = "Please select at least a record to approve. ";
			FacesUtil.setErrorMessageDialog(headerMessage, bodyMessage);
		}
	}

	public static void main(String[] args) {
		TestFTPPerbend();
	}

	public static void TestFTPPerbend() {
		/*try {
		    System.out.println("here 1");
		    String filePath = "D:/BJB/VIM/_WAR/Test.txt";
		        System.out.println("here2");
		        int status = FTP.sendFile(filePath, "", "10.1.15.51", "c623ftp", "c623ftp");
		        if(FileTransferStatus.SUCCESS == status){
		            System.out.println(filePath + " got ftp-ed successfully to  folder ");
		        }
		        else if(FileTransferStatus.FAILURE == status){
		            System.out.println("Fail to ftp  to  folder ");
		        }
		        System.out.println("here3");
		} catch (FileTransferException e) {
		    e.printStackTrace();
		}*/
		try {
			System.out.println("here 1");
			String filePath = "/opt/test/Test.txt";
			System.out.println("here2");
			int status = FTP.sendFile(filePath, "/u01/spandev/BANKJABARBANTEN/outbound/", "10.242.7.55", "devJBB",
					"DevJBB");
			if (FileTransferStatus.SUCCESS == status) {
				System.out.println(filePath + " got ftp-ed successfully to  folder ");
			} else if (FileTransferStatus.FAILURE == status) {
				System.out.println("Fail to ftp  to  folder ");
			}
			System.out.println("here3");
		} catch (FileTransferException e) {
			e.printStackTrace();
		}
	}

	public boolean checkInquiryStatus(){
		boolean status = false;
		try{
			if(getAvailableBalance().equals("")||getAvailableBalance()==null||getAvailableBalance()=="Error"){
				String headerMessage = "Error";
				String bodyMessage = "Please Inquiry First.";
				FacesUtil.setErrorMessageDialog(headerMessage, bodyMessage);
			}else{
				if(getAvailableBalance().equals(".00")){
					setAvailableBalance("0.00");
				}
				if(Integer.parseInt(getAvailableBalance().replace(".00", "").replace(",", ""))
						>=Integer.parseInt(getTotalAmount().replace(".00", "").replace(",", ""))){
					status=true;
				}else{
					String headerMessage = "Error";
					String bodyMessage = "Not enough balance to execute selected records.";
					FacesUtil.setErrorMessageDialog(headerMessage, bodyMessage);
				}
			}
		}catch(Exception e){
			String headerMessage = "Error";
			String bodyMessage = "Please Inquiry First.";
			FacesUtil.setErrorMessageDialog(headerMessage, bodyMessage);
		}
		return status;
	}

	public void upload() {
		// TestFTPPerbend();
		// System.out.println("Hit FTP");
		if (getFile() != null) {
			String extension = getFile().getFileName().substring(getFile().getFileName().lastIndexOf("."));
			if (extension.equalsIgnoreCase(".xml") || extension.equalsIgnoreCase(".jar")) {
				System.out.println("Upload File SP2D");
				SystemParameter paramLocalPath = getSystemParameterDAOBean().getDetailedParameterByParamName(
						Constants.SPAN_DEPKEU_MANUAL_PUT_LOCAL_DIR);
				try {
					BufferedInputStream input = new BufferedInputStream(this.getFile().getInputstream());
					String fileName = getFile().getFileName();
					OutputStream outputStream = null;
					outputStream = new FileOutputStream(new File(paramLocalPath.getParamValue().concat(fileName)));
					int read = 0;
					byte[] bytes = new byte[1024];

					while ((read = input.read(bytes)) != -1) {
						outputStream.write(bytes, 0, read);
					}
					outputStream.close();
					// Create Jar File
					if (extension.equalsIgnoreCase(".xml")) {
						CreateJarFile.runCreateJar(paramLocalPath.getParamValue().concat(getFile().getFileName()),
								paramLocalPath.getParamValue().concat(getFile().getFileName().replace(".xml", ".jar")));
						HelperUtils.deleteFile(paramLocalPath.getParamValue().concat(getFile().getFileName()));
					}
					System.out.println("Upload Done");
				} catch (IOException e) {
					System.out.println("Upload Error");
					e.printStackTrace();
				}
			} else {
				// TODO Wrong format file
			}

		} else {
			// TODO File must be exist handler
		}
	}

	private void doLoging(String menu, String activity, String information) {
		UserLog userLog = new UserLog();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		userLog.setLogId("LOG-" + sdf.format(new Date()) + "-" + sessionBean.getUser().getUserID());
		userLog.setActivity(activity);
		userLog.setInformation(information);
		userLog.setInsertTime(new Date());
		userLog.setMenu(menu);
		userLog.setUserId(sessionBean.getUser().getUserID());
		userLog.setUserName(sessionBean.getUser().getUserName());
		UserLogHandler.doLog(userLog, getUserDAOBean());
	}

	private UserDao getUserDAOBean() {
		return (UserDao) applicationBean.getCorpGWContext().getBean("userDAO");
	}

	private SystemParameterDao getSystemParameterDAOBean() {
		return (SystemParameterDao) applicationBean.getCorpGWContext().getBean("systemParameterDAO");
	}

	public ApplicationBean getApplicationBean() {
		return applicationBean;
	}

	public void setApplicationBean(ApplicationBean applicationBean) {
		this.applicationBean = applicationBean;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public Boolean getDoShowGAJI() {
		return doShowGAJI;
	}

	public void setDoShowGAJI(Boolean doShowGAJI) {
		this.doShowGAJI = doShowGAJI;
	}

	public Boolean getDoShowGAJIR() {
		return doShowGAJIR;
	}

	public void setDoShowGAJIR(Boolean doShowGAJIR) {
		this.doShowGAJIR = doShowGAJIR;
	}

	public String getTabSelected() {
		return tabSelected;
	}

	public void setTabSelected(String tabSelected) {
		this.tabSelected = tabSelected;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(String totalRecords) {
		this.totalRecords = totalRecords;
	}

	public String getAvailableBalance() {
		return availableBalance;
	}

	public void setAvailableBalance(String availableBalance) {
		this.availableBalance = availableBalance;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

	public SPANDataValidation getSpanDataValidation() {
		return spanDataValidation;
	}

	public void setSpanDataValidation(SPANDataValidation spanDataValidation) {
		this.spanDataValidation = spanDataValidation;
	}

	public LazyDataModel<SPANDataValidation> getValidationList() {
		return validationList;
	}

	public void setValidationList(LazyDataModel<SPANDataValidation> validationList) {
		this.validationList = validationList;
	}

	public UploadedFile getFile() {
		return file;
	}

	public void setFile(UploadedFile file) {
		this.file = file;
	}

	public String getHiddenSelectedAmount() {
		return hiddenSelectedAmount;
	}

	public void setHiddenSelectedAmount(String hiddenSelectedAmount) {
		this.hiddenSelectedAmount = hiddenSelectedAmount;
	}

	public List<SPANDataValidation> getTempList() {
		return tempList;
	}

	public void setTempList(List<SPANDataValidation> tempList) {
		this.tempList = tempList;
	}

}