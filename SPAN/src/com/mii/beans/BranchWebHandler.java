package com.mii.beans;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.apache.commons.lang.SerializationUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.mii.constant.Constants;
import com.mii.dao.ApprovalDao;
import com.mii.dao.BranchDao;
import com.mii.dao.RoleDao;
import com.mii.dao.SystemParameterDao;
import com.mii.helpers.FacesUtil;
import com.mii.helpers.HelperUtils;
import com.mii.models.Approval;
import com.mii.models.Branch;
import com.mii.models.Role;
import com.mii.models.SystemParameter;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.SessionBean;

@ManagedBean
@ViewScoped
public class BranchWebHandler implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private Branch branch;
	
	private Branch branchTemp;

	private LazyDataModel<Branch> branchList;
	
	private String action;
	
	private String branchId = "";
	
	private String branchCode = "";
	
	private String branchName = "";
	private List<String> status;

	private boolean pending;
	private boolean approved;
	private boolean rejected;
	
	@ManagedProperty(value="#{applicationBean}")
	private ApplicationBean applicationBean;
	
	@ManagedProperty(value="#{sessionBean}")
	private SessionBean sessionBean;
	
	private boolean doShowSearch = false;
	
	private boolean doShowHistory = false;

	
	private String currentBranchCode;
	
	
	
	@PostConstruct
	public void init(){
		FacesUtil.authMenu(sessionBean.getUser(), "branchWebAdministration");

		doSearch();
	}
	
	private BranchDao getBranchDAOBean(){
		return (BranchDao) applicationBean.getCorpGWContext().getBean("branchDAO");
	}
	

	private SystemParameterDao getSystemParameterDAOBean(){
		return (SystemParameterDao) applicationBean.getCorpGWContext().getBean("systemParameterDAO");
	}
	
	private ApprovalDao getApprovalDAOBean(){
		return (ApprovalDao) applicationBean.getCorpGWContext().getBean("approvalDAO");
	}

	public ApplicationBean getApplicationBean() {
		return applicationBean;
	}

	public void setApplicationBean(ApplicationBean applicationBean) {
		this.applicationBean = applicationBean;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}
	
	public Branch getBranchTemp() {
		return branchTemp;
	}

	public void setBranchTemp(Branch branchTemp) {
		this.branchTemp = branchTemp;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public LazyDataModel<Branch> getBranchList() {
		return branchList;
	}

	public void setBranchList(LazyDataModel<Branch> branchList) {
		this.branchList = branchList;
	}
	
	public String getBranchId() {
		return branchId;
	}

	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	
	public List<String> getStatus() {
		return status;
	}

	public void setStatus(List<String> status) {
		this.status = status;
	}

	public boolean isPending() {
		return pending;
	}

	public void setPending(boolean pending) {
		this.pending = pending;
	}

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}

	public boolean isRejected() {
		return rejected;
	}

	public void setRejected(boolean rejected) {
		this.rejected = rejected;
	}

	public boolean isDoShowSearch() {
		return doShowSearch;
	}

	public void setDoShowSearch(boolean doShowSearch) {
		this.doShowSearch = doShowSearch;
	}

	public void showSearch(){
		doShowSearch = true;
	}
	
	public void hideSearch(){
		doShowSearch = false;
	}
	
	
	public void showHistory(){
		doShowHistory = true;
	}
	
	public void hideHistory(){
		doShowHistory = false;
	}
	
	public boolean isDoShowHistory() {
		return doShowHistory;
	}

	public void setDoShowHistory(boolean doShowHistory) {
		this.doShowHistory = doShowHistory;
	}
	
	public void doSearch(){
		status 		= new ArrayList<String>();
		
		if (((pending == false) && (approved == false)) && (rejected == false)){
			status.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
			status.add(Constants.APPROVAL_STATUS_PENDING_EDIT);
			status.add(Constants.APPROVAL_STATUS_PENDING_DELETE);
			status.add(Constants.APPROVAL_STATUS_APPROVED_CREATE);
			status.add(Constants.APPROVAL_STATUS_APPROVED_EDIT);
			status.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
			status.add(Constants.APPROVAL_STATUS_REJECTED_EDIT);
			status.add(Constants.APPROVAL_STATUS_REJECTED_DELETE);
		}else{
			if (pending == true){
				status.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
				status.add(Constants.APPROVAL_STATUS_PENDING_EDIT);
				status.add(Constants.APPROVAL_STATUS_PENDING_DELETE);
			}
			if (approved == true){
				status.add(Constants.APPROVAL_STATUS_APPROVED_CREATE);
				status.add(Constants.APPROVAL_STATUS_APPROVED_EDIT);
			}
			if (rejected == true){
				status.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
				status.add(Constants.APPROVAL_STATUS_REJECTED_EDIT);
				status.add(Constants.APPROVAL_STATUS_REJECTED_DELETE);
			}
		}

		FacesUtil.resetPage("branchForm:branchTable");
		branchList = new BranchDataModel();
		
	}

	public void newBranch(){
		this.branch = new Branch();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd-hhmm-sss");
		Date date = new Date();
		String id = sdf.format(date);
		Integer seq = getSystemParameterDAOBean().getParameterSequence().intValue();
		this.branch.setBranchId("BRH-"+id+String.format("%04d", seq));
		this.action = "";
	}
	
	public void doSave(){
		if (this.action!=null && this.action.equals("edit")) {
			if (this.branch.getStatus().equals(Constants.APPROVAL_STATUS_PENDING_EDIT)){
				try {
					Branch branchExist = getBranchDAOBean().getBranchByBranchCode(this.branchTemp.getBranchCode());
					
					if (branchExist == null || (branchExist.getBranchCode().equals(currentBranchCode))){
						getApprovalDAOBean().updateApprovalNewObjectByParameterId(HelperUtils.serialize(this.branchTemp), this.branchTemp.getBranchId());
						
						RequestContext.getCurrentInstance().execute("insertOrEditBranch.hide()");
						RequestContext.getCurrentInstance().execute("insertOrEditBranchPending.hide()");
						
						
						String headerMessage = "Edit Branch Successfully";
						String bodyMessage = "Supervisor must approve the editing of the <br/>parameter &apos;"
								+ branchTemp.getBranchName() + "&apos; to completely the process.";
	
						FacesUtil.setInfoMessageDialog(headerMessage, bodyMessage);
					}else{
						String headerMessage = "Attention";
						String bodyMessage = "Branch "+this.branchTemp.getBranchCode() + " already exist in the application.";

						FacesUtil.setWarningMessageDialog(headerMessage, bodyMessage);
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else{
				Branch branchExist = getBranchDAOBean().getBranchByBranchCode(this.branch.getBranchCode());
				
				if (branchExist == null || (branchExist.getBranchCode().equals(currentBranchCode))){
					editBranch(this.branch);
				}else{
					String headerMessage = "Attention";
					String bodyMessage = "Branch "+this.branch.getBranchCode() + " already exist in the application.";

					FacesUtil.setWarningMessageDialog(headerMessage, bodyMessage);
				}
			}
			
			
		}else{			
			try {
				Branch branchExist = getBranchDAOBean().getBranchByBranchCode(this.branch.getBranchCode());
				
				if (branchExist == null || (branchExist.getBranchCode().equals(currentBranchCode))){
					createBranch(this.branch);
				}else{
					String headerMessage = "Attention";
					String bodyMessage = "Branch "+this.branch.getBranchCode() + " already exist in the application.";

					FacesUtil.setWarningMessageDialog(headerMessage, bodyMessage);
				}
				
			} catch (Exception e) {
				String headerMessage = "Error";
				String bodyMessage = "System error";

				FacesUtil.setErrorMessageDialog(headerMessage, bodyMessage);
				e.printStackTrace();
			}
		}
	}

	public void editBranch(Branch branch){
		BranchDao branchDao = getBranchDAOBean();
		branch.setUpdateDate(HelperUtils.getCurrentDateTime());
		branch.setChangeWho(sessionBean.getUser().getUserName());
		
		try {
			if (branch.getStatus().equals(Constants.APPROVAL_STATUS_PENDING_CREATE)){
				
				getApprovalDAOBean().updateApprovalNewObjectByParameterId(HelperUtils.serialize(this.branch), this.branch.getBranchId());
				
			}else if (branch.getStatus().equals(Constants.APPROVAL_STATUS_REJECTED_CREATE)){
				Approval approval = new Approval();
				approval.setAction(Constants.APPROVAL_ACTION_CREATE);
				approval.setParameter(Constants.APPROVAL_PARAMETER_BRANCH);
				approval.setParameterId(branch.getBranchId());
				approval.setParameterName(branch.getBranchName());
				approval.setBranchId(sessionBean.getUser().getBranch());
				approval.setOldByteObject(HelperUtils.serialize(this.branchTemp));
				approval.setNewByteObject(HelperUtils.serialize(this.branch));
				getApprovalDAOBean().saveApproval(approval);
				
				branch = (Branch) SerializationUtils.clone(this.branchTemp);
				branch.setStatus(Constants.APPROVAL_STATUS_PENDING_CREATE);
			}else {
				Approval approval = new Approval();
				approval.setAction(Constants.APPROVAL_ACTION_EDIT);
				approval.setParameter(Constants.APPROVAL_PARAMETER_BRANCH);
				approval.setParameterId(branch.getBranchId());
				approval.setParameterName(branch.getBranchName());
				approval.setBranchId(sessionBean.getUser().getBranch());
				approval.setOldByteObject(HelperUtils.serialize(this.branchTemp));
				approval.setNewByteObject(HelperUtils.serialize(this.branch));
				getApprovalDAOBean().saveApproval(approval);
				
				branch = (Branch) SerializationUtils.clone(this.branchTemp);
				branch.setStatus(Constants.APPROVAL_STATUS_PENDING_EDIT);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		branchDao.saveBranch(branch);
		

		RequestContext.getCurrentInstance().execute("insertOrEditBranch.hide()");
		RequestContext.getCurrentInstance().execute("insertOrEditBranchPending.hide()");
		
		
		String headerMessage = "Edit Branch Successfully";
		String bodyMessage = "Supervisor must approve the editing of the <br/>branch &apos;"
				+ branch.getBranchName() + "&apos; to completely the process.";

		FacesUtil.setInfoMessageDialog(headerMessage, bodyMessage);
	}
	
	public void createBranch(Branch branch){		
		branch.setCreateDate(HelperUtils.getCurrentDateTime());
		branch.setCreateWho(sessionBean.getUser().getUserName());
		branch.setStatus(Constants.APPROVAL_STATUS_PENDING_CREATE);
		
		Approval approval = new Approval();
		approval.setAction(Constants.APPROVAL_ACTION_CREATE);
		approval.setParameter(Constants.APPROVAL_PARAMETER_BRANCH);
		approval.setParameterId(branch.getBranchId());
		approval.setParameterName(branch.getBranchName());
		approval.setBranchId(sessionBean.getUser().getBranch());
		try {
			approval.setNewByteObject(HelperUtils.serialize(this.branch));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		getApprovalDAOBean().saveApproval(approval);
		getBranchDAOBean().saveBranch(branch);
		

		RequestContext.getCurrentInstance().execute("insertOrEditBranch.hide()");
		RequestContext.getCurrentInstance().execute("insertOrEditBranchPending.hide()");
		
		String headerMessage = "Create Branch Successfully";
		String bodyMessage = "Supervisor must approve the creating of the <br/>branch &apos;"
				+ branch.getBranchName() + "&apos; to completely the process.";

		FacesUtil.setInfoMessageDialog(headerMessage, bodyMessage);
	}
	
	public void viewDetailedBranch(Branch branch){
		BranchDao branchDao = getBranchDAOBean();
		this.branch = branchDao.getDetailedBranch(branch.getBranchId());
		
		if (this.branch.getStatus().equals(Constants.APPROVAL_STATUS_PENDING_EDIT)){
			List<Approval> approvals = getApprovalDAOBean().getApprovalByParameterId(branch.getBranchId());
			if (approvals.size()>0){
				try {
					this.branchTemp = (Branch) HelperUtils.deserialize(approvals.get(0).getNewBlobObject().getBinaryStream());
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				RequestContext.getCurrentInstance().execute(
						"viewBranchEdit.show()");
			}
		}else{
			RequestContext.getCurrentInstance().execute("viewBranch.show()");
		}
	}
	
	public void viewDetailedBranchEdit(Branch branch){
		
		BranchDao branchDao = getBranchDAOBean();
		this.branch = branchDao.getDetailedBranch(branch.getBranchId());
		
		if (this.branch.getDisableEdit() == true){
			FacesUtil.setApprovalWarning("branch", this.branch.getBranchName(), this.branch.getStatus());
		}else{

			if (this.branch.getStatus().equals(Constants.APPROVAL_STATUS_PENDING_EDIT)){
					
				List<Approval> approvals = getApprovalDAOBean().getApprovalByParameterId(branch.getBranchId());
				if (approvals.size()>0){
					try {
						this.branchTemp = (Branch) HelperUtils.deserialize(approvals.get(0).getNewBlobObject().getBinaryStream());
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					RequestContext.getCurrentInstance().execute(
							"insertOrEditBranchPending.show()");
				}
			}
			else {
				
				this.branchTemp =  (Branch) SerializationUtils.clone(this.branch);
				RequestContext.getCurrentInstance().execute(
						"insertOrEditBranch.show()");
			}

			currentBranchCode = this.branchTemp.getBranchCode();
			this.action = "edit";
		}
	}
	
	public void viewDetailedDelete(Branch branch){

		BranchDao branchDao = getBranchDAOBean();
		this.branch = branchDao.getDetailedBranch(branch.getBranchId());
		
		if (this.branch.getDisableDelete() == true){
			FacesUtil.setApprovalWarning("branch", this.branch.getBranchName(), this.branch.getStatus());
		}else{
			RequestContext.getCurrentInstance().execute(
					"dlgConfirm.show()");
		}
		
	}
	
	public void deleteBranch(){
		
		if (branch.getStatus().equals(Constants.APPROVAL_STATUS_REJECTED_CREATE)){

			getBranchDAOBean().deleteBranch(branch.getBranchId());
			
			String headerMessage = "Success";
			String bodyMessage = "Delete Branch " + this.branch.getBranchName() + "Successfully";
	
			FacesUtil.setInfoMessageDialog(headerMessage, bodyMessage);
			
		}else{
		
			Approval approval = new Approval();
			approval.setAction(Constants.APPROVAL_ACTION_DELETE);
			approval.setParameter(Constants.APPROVAL_PARAMETER_BRANCH);
			approval.setParameterId(branch.getBranchId());
			approval.setParameterName(branch.getBranchName());
			approval.setBranchId(sessionBean.getUser().getBranch());
			
			try {
				approval.setOldByteObject(HelperUtils.serialize(this.branch));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			getApprovalDAOBean().saveApproval(approval);
			
			this.branch.setStatus(Constants.APPROVAL_STATUS_PENDING_DELETE);
			getBranchDAOBean().saveBranch(this.branch);
			
			String headerMessage = "Delete Branch Successfully";
			String bodyMessage = "Supervisor must approve the deleting of the <br/>branch &apos;"
					+ branch.getBranchName()+ "&apos; to completely the process.";
	
			FacesUtil.setInfoMessageDialog(headerMessage, bodyMessage);
		
		}
		
	}
	
	private class BranchDataModel extends LazyDataModel<Branch>{
		private static final long serialVersionUID = 1L;
		public BranchDataModel() {
			
		}
		@Override
		public List<Branch> load(int startingAt, 
				int maxPerPage, String sortField, 
				SortOrder sortOrder, Map<String, String> filters) {
			
			BranchDao branchDao = (BranchDao) getBranchDAOBean();
			List<Branch> branchs;
			
			branchs = branchDao.getBranchLazyLoadingByCategory(maxPerPage+startingAt, startingAt, branchId, branchCode, branchName, status);
			Integer jmlAll = branchDao.countBranchByCategory(branchId, branchCode, branchName, status);
			setRowCount(jmlAll);
			setPageSize(maxPerPage);

			return branchs;
		}
	}
	
}