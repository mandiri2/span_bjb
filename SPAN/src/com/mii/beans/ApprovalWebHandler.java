package com.mii.beans;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.apache.commons.lang.SerializationUtils;
import org.omnifaces.model.tree.ListTreeModel;
import org.omnifaces.model.tree.TreeModel;
import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.mii.constant.Constants;
import com.mii.constant.ConstantsUserLog;
import com.mii.dao.ApprovalDao;
import com.mii.dao.BranchDao;
import com.mii.dao.HolidayDao;
import com.mii.dao.ProviderDao;
import com.mii.dao.RoleDao;
import com.mii.dao.SchedulerDao;
import com.mii.dao.SystemParameterDao;
import com.mii.dao.UserDao;
import com.mii.helpers.EmailHelper;
import com.mii.helpers.FacesUtil;
import com.mii.helpers.HelperUtils;
import com.mii.models.Approval;
import com.mii.models.Branch;
import com.mii.models.Holiday;
import com.mii.models.Menu;
import com.mii.models.Role;
import com.mii.models.ScheduleTask;
import com.mii.models.SystemParameter;
import com.mii.models.User;
import com.mii.models.UserLog;
import com.mii.models.UserRole;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.SessionBean;
import com.mii.ws.WebServiceSchedulerExecutor;

@ManagedBean
@ViewScoped
public class ApprovalWebHandler implements Serializable {

	private static final long serialVersionUID = 1L;

	private Approval approval;

	private User user;

	private User prevUser;

	private Role role;

	private Role prevRole;

	private ScheduleTask task;

	private ScheduleTask prevTask;
	
	private SystemParameter systemParameter;
	
	private SystemParameter prevSystemParameter;
	
	private Branch branch;
	
	private Branch prevBranch;
	
	private Holiday holiday;
	
	private Holiday prevHoliday;

	private List < Integer > hours;
	private List < Integer > minutes;

	private List < String > scheduleTypes;

	private boolean runOnce = false;
	private boolean repeating = false;
	private boolean complexRepeating = false;
	private boolean repeatingOrComplexRepeating = false;

	private boolean prevRunOnce = false;
	private boolean prevRepeating = false;
	private boolean prevComplexRepeating = false;
	private boolean prevRepeatingOrComplexRepeating = false;

	private List < Menu > menus;
	private TreeModel < Menu > tree;
	private TreeModel < Menu > selectedMenu;
	private HashMap < Integer, Menu > rootMenu;
	private HashMap < Integer, Menu > childMenu;

	private List < Menu > prevMenus;
	private TreeModel < Menu > prevTree;
	private TreeModel < Menu > prevSelectedMenu;
	private HashMap < Integer, Menu > prevRootMenu;
	private HashMap < Integer, Menu > prevChildMenu;

	private LazyDataModel < Approval > approvalList;

	private List < String > rolesName;
	private List < String > prevRolesName;
	
	private boolean searchParameterUser;
	private boolean searchParameterRole;
	private boolean searchParameterScheduler;
	private boolean searchParameterSystemParameter;
	private boolean searchParameterBranch;
	private boolean searchParameterHoliday;
	
	private boolean searchActionCreate;
	private boolean searchActionEdit;
	private boolean searchActionDelete;
	
	private String parameterId;
	private String parameterName;
	private List<String> searchParameter;
	private List<String> searchAction;
	
	private boolean teller;
	private boolean tellerTemp;
	
	private boolean doShowSearch = false;
	
	
	private String roleIdTeller;
	private String roleIdTellerSupervisor;
	
	public static Map < BigInteger, Approval > approvalTemps = new HashMap < BigInteger, Approval > ();

	@ManagedProperty(value = "#{applicationBean}")
	private ApplicationBean applicationBean;

	@ManagedProperty(value = "#{sessionBean}")
	private SessionBean sessionBean;
	
	@PostConstruct
	public void init() {
		FacesUtil.authMenu(sessionBean.getUser(), "approvalWebAdministration");

		approvalTemps.clear();
		user = new User();
		role = new Role();
		initSelectItemData();
		initSearch();
		doSearch();
		
//		SystemParameter systemParameter	= getSystemParameterDAOBean().getDetailedParameterByParamName(Constants.ROLE_ID_TELLER);
//		roleIdTeller	= systemParameter.getParamValue();
		roleIdTeller = "";
		
//		systemParameter	= getSystemParameterDAOBean().getDetailedParameterByParamName(Constants.ROLE_ID_TELLER_SUPERVISOR);
//		roleIdTellerSupervisor	= systemParameter.getParamValue();
		roleIdTellerSupervisor = "";
		
		doLoging(ConstantsUserLog.MENU_APPROVAL, ConstantsUserLog.ACTIVITY_INIT, "Menu Init.");
	}
	
	public void initSearch(){
		parameterId = "";
		parameterName = "";
		initSearchParameter();
		initSearchAction();
	}
	
	public void initSearchParameter(){
		searchParameter	= new ArrayList<String>();
		searchParameter.add(Constants.APPROVAL_PARAMETER_USER);
		searchParameter.add(Constants.APPROVAL_PARAMETER_ROLE);
		searchParameter.add(Constants.APPROVAL_PARAMETER_SCHEDULER);
		searchParameter.add(Constants.APPROVAL_PARAMETER_SPARAM);
		searchParameter.add(Constants.APPROVAL_PARAMETER_BRANCH);
		searchParameter.add(Constants.APPROVAL_PARAMETER_HOLIDAY);
	}
	
	public void initSearchAction(){
		searchAction = new ArrayList<String>();
		searchAction.add(Constants.APPROVAL_ACTION_CREATE);
		searchAction.add(Constants.APPROVAL_ACTION_EDIT);
		searchAction.add(Constants.APPROVAL_ACTION_DELETE);
	}

	@PreDestroy()
	public void destroy(){
		//System.out.println("destroy");
	}
	
	private SchedulerDao getSchedulerDAOBean() {
		return (SchedulerDao) applicationBean.getCorpGWContext().getBean(
			"schedulerDAO");
	}
	

	public boolean isDoShowSearch() {
		return doShowSearch;
	}

	public void setDoShowSearch(boolean doShowSearch) {
		this.doShowSearch = doShowSearch;
	}

	private SystemParameterDao getSystemParameterDAOBean(){
		return (SystemParameterDao) applicationBean.getCorpGWContext().getBean("systemParameterDAO");
	}
	
	private BranchDao getBranchDAOBean(){
		return (BranchDao) applicationBean.getCorpGWContext().getBean("branchDAO");
	}
	
	private HolidayDao getHolidayDAOBean(){
		return (HolidayDao) applicationBean.getCorpGWContext().getBean("holidayDAO");
	}
	
	private ApprovalDao getApprovalDAOBean(){
		return (ApprovalDao) applicationBean.getCorpGWContext().getBean("approvalDAO");
	}
	
	public String getParameterId() {
		return parameterId;
	}

	public void setParameterId(String parameterId) {
		this.parameterId = parameterId;
	}
	
	public String getParameterName() {
		return parameterName;
	}

	public void setParameterName(String parameterName) {
		this.parameterName = parameterName;
	}

	public Approval getApproval() {
		return approval;
	}

	public void setApproval(Approval approval) {
		this.approval = approval;
	}
	
	public List<String> getRolesName() {
		return rolesName;
	}

	public void setRolesName(List<String> rolesName) {
		this.rolesName = rolesName;
	}

	public List<String> getPrevRolesName() {
		return prevRolesName;
	}

	public void setPrevRolesName(List<String> prevRolesName) {
		this.prevRolesName = prevRolesName;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public User getPrevUser() {
		return prevUser;
	}

	public void setPrevUser(User prevUser) {
		this.prevUser = prevUser;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Role getPrevRole() {
		return prevRole;
	}

	public void setPrevRole(Role prevRole) {
		this.prevRole = prevRole;
	}

	public ScheduleTask getTask() {
		return task;
	}

	public void setTask(ScheduleTask task) {
		this.task = task;
	}

	public ScheduleTask getPrevTask() {
		return prevTask;
	}

	public void setPrevTask(ScheduleTask prevTask) {
		this.prevTask = prevTask;
	}
	
	public SystemParameter getSystemParameter() {
		return systemParameter;
	}

	public void setSystemParameter(SystemParameter systemParameter) {
		this.systemParameter = systemParameter;
	}

	public SystemParameter getPrevSystemParameter() {
		return prevSystemParameter;
	}

	public void setPrevSystemParameter(SystemParameter prevSystemParameter) {
		this.prevSystemParameter = prevSystemParameter;
	}
	
	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	public Branch getPrevBranch() {
		return prevBranch;
	}

	public void setPrevBranch(Branch prevBranch) {
		this.prevBranch = prevBranch;
	}

	public Holiday getHoliday() {
		return holiday;
	}

	public void setHoliday(Holiday holiday) {
		this.holiday = holiday;
	}

	public Holiday getPrevHoliday() {
		return prevHoliday;
	}

	public void setPrevHoliday(Holiday prevHoliday) {
		this.prevHoliday = prevHoliday;
	}

	public List < Menu > getMenus() {
		return menus;
	}

	public void setMenus(List < Menu > menus) {
		this.menus = menus;
	}

	public TreeModel < Menu > getTree() {
		return tree;
	}

	public void setTree(TreeModel < Menu > tree) {
		this.tree = tree;
	}

	public TreeModel < Menu > getSelectedMenu() {
		return selectedMenu;
	}

	public void setSelectedMenu(TreeModel < Menu > selectedMenu) {
		this.selectedMenu = selectedMenu;
	}

	public HashMap < Integer, Menu > getRootMenu() {
		return rootMenu;
	}

	public void setRootMenu(HashMap < Integer, Menu > rootMenu) {
		this.rootMenu = rootMenu;
	}

	public HashMap < Integer, Menu > getChildMenu() {
		return childMenu;
	}

	public void setChildMenu(HashMap < Integer, Menu > childMenu) {
		this.childMenu = childMenu;
	}

	public LazyDataModel < Approval > getApprovalList() {
		return approvalList;
	}

	public void setApprovalList(LazyDataModel < Approval > approvalList) {
		this.approvalList = approvalList;
	}

	public ApplicationBean getApplicationBean() {
		return applicationBean;
	}

	public void setApplicationBean(ApplicationBean applicationBean) {
		this.applicationBean = applicationBean;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public List < Menu > getPrevMenus() {
		return prevMenus;
	}

	public void setPrevMenus(List < Menu > prevMenus) {
		this.prevMenus = prevMenus;
	}

	public TreeModel < Menu > getPrevTree() {
		return prevTree;
	}

	public void setPrevTree(TreeModel < Menu > prevTree) {
		this.prevTree = prevTree;
	}

	public List < Integer > getHours() {
		return hours;
	}

	public void setHours(List < Integer > hours) {
		this.hours = hours;
	}

	public List < Integer > getMinutes() {
		return minutes;
	}

	public void setMinutes(List < Integer > minutes) {
		this.minutes = minutes;
	}

	public List < String > getScheduleTypes() {
		return scheduleTypes;
	}

	public void setScheduleTypes(List < String > scheduleTypes) {
		this.scheduleTypes = scheduleTypes;
	}

	public boolean isRunOnce() {
		return runOnce;
	}

	public void setRunOnce(boolean runOnce) {
		this.runOnce = runOnce;
	}

	public boolean isRepeating() {
		return repeating;
	}

	public void setRepeating(boolean repeating) {
		this.repeating = repeating;
	}

	public boolean isComplexRepeating() {
		return complexRepeating;
	}

	public void setComplexRepeating(boolean complexRepeating) {
		this.complexRepeating = complexRepeating;
	}

	public boolean isRepeatingOrComplexRepeating() {
		return repeatingOrComplexRepeating;
	}

	public void setRepeatingOrComplexRepeating(
	boolean repeatingOrComplexRepeating) {
		this.repeatingOrComplexRepeating = repeatingOrComplexRepeating;
	}

	public boolean isPrevRunOnce() {
		return prevRunOnce;
	}

	public void setPrevRunOnce(boolean prevRunOnce) {
		this.prevRunOnce = prevRunOnce;
	}

	public boolean isPrevRepeating() {
		return prevRepeating;
	}

	public void setPrevRepeating(boolean prevRepeating) {
		this.prevRepeating = prevRepeating;
	}

	public boolean isPrevComplexRepeating() {
		return prevComplexRepeating;
	}

	public void setPrevComplexRepeating(boolean prevComplexRepeating) {
		this.prevComplexRepeating = prevComplexRepeating;
	}

	public boolean isPrevRepeatingOrComplexRepeating() {
		return prevRepeatingOrComplexRepeating;
	}

	public void setPrevRepeatingOrComplexRepeating(
	boolean prevRepeatingOrComplexRepeating) {
		this.prevRepeatingOrComplexRepeating = prevRepeatingOrComplexRepeating;
	}

	public TreeModel < Menu > getPrevSelectedMenu() {
		return prevSelectedMenu;
	}

	public void setPrevSelectedMenu(TreeModel < Menu > prevSelectedMenu) {
		this.prevSelectedMenu = prevSelectedMenu;
	}

	public HashMap < Integer, Menu > getPrevRootMenu() {
		return prevRootMenu;
	}

	public void setPrevRootMenu(HashMap < Integer, Menu > prevRootMenu) {
		this.prevRootMenu = prevRootMenu;
	}

	public HashMap < Integer, Menu > getPrevChildMenu() {
		return prevChildMenu;
	}

	public void setPrevChildMenu(HashMap < Integer, Menu > prevChildMenu) {
		this.prevChildMenu = prevChildMenu;
	}

	private UserDao getUserDAOBean() {
		return (UserDao) applicationBean.getCorpGWContext().getBean("userDAO");
	}

	private RoleDao getRoleDAOBean() {
		return (RoleDao) applicationBean.getCorpGWContext().getBean("roleDAO");
	}
	
	public boolean isSearchParameterUser() {
		return searchParameterUser;
	}

	public void setSearchParameterUser(boolean searchParameterUser) {
		this.searchParameterUser = searchParameterUser;
	}

	public boolean isSearchParameterRole() {
		return searchParameterRole;
	}

	public void setSearchParameterRole(boolean searchParameterRole) {
		this.searchParameterRole = searchParameterRole;
	}

	public boolean isSearchParameterScheduler() {
		return searchParameterScheduler;
	}

	public void setSearchParameterScheduler(boolean searchParameterScheduler) {
		this.searchParameterScheduler = searchParameterScheduler;
	}


	public boolean isSearchActionCreate() {
		return searchActionCreate;
	}

	public boolean isSearchParameterSystemParameter() {
		return searchParameterSystemParameter;
	}

	public void setSearchParameterSystemParameter(
			boolean searchParameterSystemParameter) {
		this.searchParameterSystemParameter = searchParameterSystemParameter;
	}
	

	public boolean isSearchParameterBranch() {
		return searchParameterBranch;
	}

	public void setSearchParameterBranch(boolean searchParameterBranch) {
		this.searchParameterBranch = searchParameterBranch;
	}

	public boolean isSearchParameterHoliday() {
		return searchParameterHoliday;
	}

	public void setSearchParameterHoliday(boolean searchParameterHoliday) {
		this.searchParameterHoliday = searchParameterHoliday;
	}

	public void setSearchActionCreate(boolean searchActionCreate) {
		this.searchActionCreate = searchActionCreate;
	}

	public boolean isSearchActionEdit() {
		return searchActionEdit;
	}

	public void setSearchActionEdit(boolean searchActionEdit) {
		this.searchActionEdit = searchActionEdit;
	}

	public boolean isSearchActionDelete() {
		return searchActionDelete;
	}

	public void setSearchActionDelete(boolean searchActionDelete) {
		this.searchActionDelete = searchActionDelete;
	}
	
	public List<String> getSearchParameter() {
		return searchParameter;
	}

	public void setSearchParameter(List<String> searchParameter) {
		this.searchParameter = searchParameter;
	}

	public List<String> getSearchAction() {
		return searchAction;
	}

	public void setSearchAction(List<String> searchAction) {
		this.searchAction = searchAction;
	}
	
	public void showSearch(){
		doShowSearch = true;
	}
	
	public void hideSearch(){
		doShowSearch = false;
	}
	
	public void checkRoleTeller(List<UserRole> userRoles){
		
		int countTeller = 0;
		for (UserRole userRole : userRoles){
			if (userRole.getRole() != null){
				if ((roleIdTeller != null) && (roleIdTellerSupervisor != null)){
					if ((userRole.getRole().equals(roleIdTeller)) || (userRole.getRole().equals(roleIdTellerSupervisor))){
						countTeller = countTeller + 1;
					}
				}
			}
		}
		if (countTeller > 0 ){
			teller = true;
		}else{
			teller = false;
		}

	}
	
	public void checkRoleTellerTemp(List<UserRole> userRoles){
		
		int countTeller = 0;
		for (UserRole userRole : userRoles){
			if (userRole.getRole() != null){
				if ((roleIdTeller != null) && (roleIdTellerSupervisor != null)){
					if ((userRole.getRole().equals(roleIdTeller)) || (userRole.getRole().equals(roleIdTellerSupervisor))){
						countTeller = countTeller + 1;
					}
				}
			}
		}
		if (countTeller > 0 ){
			tellerTemp = true;
		}else{
			tellerTemp = false;
		}

	}
	
	public boolean isTeller() {
		return teller;
	}

	public void setTeller(boolean teller) {
		this.teller = teller;
	}

	public boolean isTellerTemp() {
		return tellerTemp;
	}

	public void setTellerTemp(boolean tellerTemp) {
		this.tellerTemp = tellerTemp;
	}

	public void doSearch(){
		searchParameter = new ArrayList<String>();
		if ((((searchParameterUser == false) && (searchParameterRole == false)) && (searchParameterScheduler == false)) && (searchParameterSystemParameter == false) && (searchParameterBranch == false) && (searchParameterHoliday == false)){
			initSearchParameter();
		}else{
			if (searchParameterUser == true){
				searchParameter.add(Constants.APPROVAL_PARAMETER_USER);
			}
			if (searchParameterRole == true){
				searchParameter.add(Constants.APPROVAL_PARAMETER_ROLE);
			}
			if (searchParameterScheduler == true){
				searchParameter.add(Constants.APPROVAL_PARAMETER_SCHEDULER);
			}
			if (searchParameterSystemParameter == true){
				searchParameter.add(Constants.APPROVAL_PARAMETER_SPARAM);
			}
			if (searchParameterBranch == true){
				searchParameter.add(Constants.APPROVAL_PARAMETER_BRANCH);
			}
			if (searchParameterHoliday == true){
				searchParameter.add(Constants.APPROVAL_PARAMETER_HOLIDAY);
			}
		}
		
		searchAction = new ArrayList<String>();
		if (((searchActionCreate == false) && (searchActionEdit == false)) && (searchActionDelete == false)){
			initSearchAction();
		}else{
			if (searchActionCreate == true){
				searchAction.add(Constants.APPROVAL_ACTION_CREATE);
			}
			if (searchActionEdit == true){
				searchAction.add(Constants.APPROVAL_ACTION_EDIT);
			}
			if (searchActionDelete == true){
				searchAction.add(Constants.APPROVAL_ACTION_DELETE);
			}
		}

		FacesUtil.resetPage("approvalForm:approvalTable");
		approvalList = new ApprovalDataModel();
	}

	public void viewDetail(Approval approval) {

		try {

			switch (approval.getParameter()){
			
				case Constants.APPROVAL_PARAMETER_USER 		: viewDetailUser(approval);				break;
				
				case Constants.APPROVAL_PARAMETER_ROLE 		: viewDetailRole(approval);				break;
				
				case Constants.APPROVAL_PARAMETER_SCHEDULER : viewDetailScheduler(approval);		break;
				
				case Constants.APPROVAL_PARAMETER_SPARAM 	: viewDetailSystemParameter(approval);	break;
				
				case Constants.APPROVAL_PARAMETER_BRANCH 	: viewDetailBranch(approval);			break;
				
				case Constants.APPROVAL_PARAMETER_HOLIDAY 	: viewDetailHoliday(approval);			break;
				
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void viewDetailUser(Approval approval) throws SQLException, Exception{
		if (approval.getAction().equals(Constants.APPROVAL_ACTION_CREATE)) {
			this.user = (User) HelperUtils.deserialize(approval.getNewBlobObject().getBinaryStream());
			Branch branch = getBranchDAOBean().getBranchByBranchCode(this.user.getBranch());
			if (branch != null) this.user.setBranchName(branch.getBranchName());
			this.rolesName = new ArrayList<String>();
			
			for (UserRole userRole : this.user.getRoles()){
				RoleDao roleDao = (RoleDao) applicationBean.getCorpGWContext().getBean("roleDAO");
				Role role = new Role();
				role.setRoleId(userRole.getRole());
				role = roleDao.getDetailedRole(role);
				this.rolesName.add(role.getRoleName());
			}
			checkRoleTeller(this.user.getRoles());
			RequestContext.getCurrentInstance().execute(
				"viewUser.show()");
		} else if (approval.getAction().equals(
		Constants.APPROVAL_ACTION_EDIT)) {
			this.user = (User) HelperUtils.deserialize(approval.getNewBlobObject().getBinaryStream());
			Branch branch = getBranchDAOBean().getBranchByBranchCode(this.user.getBranch());
			if (branch != null) this.user.setBranchName(branch.getBranchName());
			
			this.prevUser = (User) HelperUtils.deserialize(approval.getOldBlobObject().getBinaryStream());
			branch = getBranchDAOBean().getBranchByBranchCode(this.user.getBranch());
			if (branch != null) this.prevUser.setBranchName(branch.getBranchName());
			
			this.rolesName = new ArrayList<String>();
			
			for (UserRole userRole : this.user.getRoles()){
				RoleDao roleDao = (RoleDao) applicationBean.getCorpGWContext().getBean("roleDAO");
				Role role = new Role();
				role.setRoleId(userRole.getRole());
				role = roleDao.getDetailedRole(role);
				this.rolesName.add(role.getRoleName());
			}
			
			this.prevRolesName = new ArrayList<String>();
			
			for (UserRole userRole : this.prevUser.getRoles()){
				RoleDao roleDao = (RoleDao) applicationBean.getCorpGWContext().getBean("roleDAO");
				Role role = new Role();
				role.setRoleId(userRole.getRole());
				role = roleDao.getDetailedRole(role);
				this.prevRolesName.add(role.getRoleName());
			}
			

			checkRoleTeller(this.user.getRoles());

			checkRoleTellerTemp(this.prevUser.getRoles());
			RequestContext.getCurrentInstance().execute(
				"viewEditUser.show()");
		} else if (approval.getAction().equals(
		Constants.APPROVAL_ACTION_DELETE)) {
			this.user = (User) HelperUtils.deserialize(approval.getOldBlobObject().getBinaryStream());
			
			this.rolesName = new ArrayList<String>();
			
			for (UserRole userRole : this.user.getRoles()){
				RoleDao roleDao = (RoleDao) applicationBean.getCorpGWContext().getBean("roleDAO");
				Role role = new Role();
				role.setRoleId(userRole.getRole());
				role = roleDao.getDetailedRole(role);
				this.rolesName.add(role.getRoleName());
			}
			
			RequestContext.getCurrentInstance().execute(
				"viewUser.show()");
		}
	}
	
	public void viewDetailRole(Approval approval) throws SQLException, Exception{
		if (approval.getAction().equals(Constants.APPROVAL_ACTION_CREATE)) {
			this.role = (Role) HelperUtils.deserialize(approval.getNewBlobObject().getBinaryStream());
			createTreeMenu();
			RequestContext.getCurrentInstance().execute(
				"viewRole.show()");
		} else if (approval.getAction().equals(
		Constants.APPROVAL_ACTION_EDIT)) {
			this.role = (Role) HelperUtils.deserialize(approval.getNewBlobObject().getBinaryStream());
			this.prevRole = (Role) HelperUtils.deserialize(approval.getOldBlobObject().getBinaryStream());
			createTreeMenu();
			createPrevTreeMenu();
			RequestContext.getCurrentInstance().execute(
				"viewEditRole.show()");
		} else if (approval.getAction().equals(
		Constants.APPROVAL_ACTION_DELETE)) {
			this.role = (Role) HelperUtils.deserialize(approval.getOldBlobObject().getBinaryStream());
			createTreeMenu();
			RequestContext.getCurrentInstance().execute(
				"viewRole.show()");
		}
	}
	
	public void viewDetailScheduler(Approval approval) throws SQLException, Exception{
		if (approval.getAction().equals(Constants.APPROVAL_ACTION_CREATE)) {
			this.task = (ScheduleTask) HelperUtils.deserialize(approval.getNewBlobObject().getBinaryStream());
			checkScheduleType();
			RequestContext.getCurrentInstance().execute(
				"viewScheduler.show()");
		} else if (approval.getAction().equals(
		Constants.APPROVAL_ACTION_EDIT)) {
			this.task = (ScheduleTask) HelperUtils.deserialize(approval.getNewBlobObject().getBinaryStream());
			this.prevTask = (ScheduleTask) HelperUtils.deserialize(approval.getOldBlobObject()
				.getBinaryStream());
			checkScheduleType();
			checkScheduleTypePrev();
			RequestContext.getCurrentInstance().execute(
				"viewSchedulerEdit.show()");
		} else if (approval.getAction().equals(
		Constants.APPROVAL_ACTION_DELETE)) {
			this.task = (ScheduleTask) HelperUtils.deserialize(approval.getOldBlobObject().getBinaryStream());
			checkScheduleType();
			RequestContext.getCurrentInstance().execute(
				"viewScheduler.show()");
		}
	}
	
	public void viewDetailSystemParameter(Approval approval) throws SQLException, Exception{
		if (approval.getAction().equals(Constants.APPROVAL_ACTION_CREATE)){
			this.systemParameter = (SystemParameter) HelperUtils.deserialize(approval.getNewBlobObject().getBinaryStream());
			RequestContext.getCurrentInstance().execute(
					"viewParameter.show()");
		}else if (approval.getAction().equals(Constants.APPROVAL_ACTION_EDIT)){
			this.systemParameter = (SystemParameter) HelperUtils.deserialize(approval.getNewBlobObject().getBinaryStream());
			this.prevSystemParameter = (SystemParameter) HelperUtils.deserialize(approval.getOldBlobObject().getBinaryStream());
			RequestContext.getCurrentInstance().execute(
					"viewParameterEdit.show()");
		}else if (approval.getAction().equals(Constants.APPROVAL_ACTION_DELETE)){
			this.systemParameter = (SystemParameter) HelperUtils.deserialize(approval.getOldBlobObject().getBinaryStream());
			RequestContext.getCurrentInstance().execute(
					"viewParameter.show()");
		}
	}
	
	public void viewDetailBranch(Approval approval) throws SQLException, Exception{
		if (approval.getAction().equals(Constants.APPROVAL_ACTION_CREATE)){
			this.branch = (Branch) HelperUtils.deserialize(approval.getNewBlobObject().getBinaryStream());
			RequestContext.getCurrentInstance().execute(
					"viewBranch.show()");
		}else if (approval.getAction().equals(Constants.APPROVAL_ACTION_EDIT)){
			this.branch = (Branch) HelperUtils.deserialize(approval.getNewBlobObject().getBinaryStream());
			this.prevBranch = (Branch) HelperUtils.deserialize(approval.getOldBlobObject().getBinaryStream());
			RequestContext.getCurrentInstance().execute(
					"viewBranchEdit.show()");
		}else if (approval.getAction().equals(Constants.APPROVAL_ACTION_DELETE)){
			this.branch = (Branch) HelperUtils.deserialize(approval.getOldBlobObject().getBinaryStream());
			RequestContext.getCurrentInstance().execute(
					"viewBranch.show()");
		}
	}
	
	public void viewDetailHoliday(Approval approval) throws SQLException, Exception{
		if (approval.getAction().equals(Constants.APPROVAL_ACTION_CREATE)){
			this.holiday = (Holiday) HelperUtils.deserialize(approval.getNewBlobObject().getBinaryStream());
			RequestContext.getCurrentInstance().execute(
					"viewHoliday.show()");
		}else if (approval.getAction().equals(Constants.APPROVAL_ACTION_EDIT)){
			this.holiday = (Holiday) HelperUtils.deserialize(approval.getNewBlobObject().getBinaryStream());
			this.prevHoliday = (Holiday) HelperUtils.deserialize(approval.getOldBlobObject().getBinaryStream());
			RequestContext.getCurrentInstance().execute(
					"viewHolidayEdit.show()");
		}else if (approval.getAction().equals(Constants.APPROVAL_ACTION_DELETE)){
			this.holiday = (Holiday) HelperUtils.deserialize(approval.getOldBlobObject().getBinaryStream());
			RequestContext.getCurrentInstance().execute(
					"viewHoliday.show()");
		}
	}
	
	public void createTreeMenu() {
		this.menus = applicationBean.getMenus();
		rootMenu = new HashMap < Integer, Menu > ();
		childMenu = new HashMap < Integer, Menu > ();
		tree = new ListTreeModel < Menu > ();
		for (Menu menu: this.menus) {
			// agar menu di applicationBean tidak berubah
			Menu tempMenu = new Menu(menu);

			Set < Menu > roleMenus = role.getMenus();
			for (Menu menu2: roleMenus) {
				if (menu2.getMenuId() == tempMenu.getMenuId()) {
					tempMenu.setSelected(true);
				}
			}

			if (tempMenu.getMenuParentId() == 0) {
				rootMenu.put(menu.getMenuId(), tempMenu);
			} else {
				childMenu.put(menu.getMenuId(), tempMenu);
			}
		}

		for (Map.Entry < Integer, Menu > entry: rootMenu.entrySet()) {
			int parentMenuId = entry.getValue().getMenuId();
			TreeModel < Menu > currentRoot = tree.addChild(entry.getValue());
			createChild(currentRoot, parentMenuId, childMenu);
		}
	}

	public void createPrevTreeMenu() {
		this.prevMenus = applicationBean.getMenus();
		prevRootMenu = new HashMap < Integer, Menu > ();
		prevChildMenu = new HashMap < Integer, Menu > ();
		prevTree = new ListTreeModel < Menu > ();
		for (Menu menu: this.prevMenus) {
			// agar menu di applicationBean tidak berubah
			Menu tempMenu = new Menu(menu);

			Set < Menu > roleMenus = prevRole.getMenus();
			for (Menu menu2: roleMenus) {
				if (menu2.getMenuId() == tempMenu.getMenuId()) {
					tempMenu.setSelected(true);
				}
			}

			if (tempMenu.getMenuParentId() == 0) {
				prevRootMenu.put(menu.getMenuId(), tempMenu);
			} else {
				prevChildMenu.put(menu.getMenuId(), tempMenu);
			}
		}

		for (Map.Entry < Integer, Menu > entry: prevRootMenu.entrySet()) {
			int parentMenuId = entry.getValue().getMenuId();
			TreeModel < Menu > currentRoot = prevTree.addChild(entry.getValue());
			createChild(currentRoot, parentMenuId, prevChildMenu);
		}
	}

	private void createChild(TreeModel < Menu > treeMenu, int menuId,
	Map < Integer, Menu > childMenu) {
		for (Map.Entry < Integer, Menu > entry: childMenu.entrySet()) {
			Menu tempMenu = entry.getValue();
			if (tempMenu.getMenuParentId() == menuId) {
				TreeModel < Menu > currentParent = treeMenu.addChild(tempMenu);
				createChild(currentParent, tempMenu.getMenuId(), childMenu);
			}
		}
	}

	public void initSelectItemData() {

		scheduleTypes = new ArrayList < String > ();
		scheduleTypes.add(Constants.SCHEDULER_RUN_ONCE);
		scheduleTypes.add(Constants.SCHEDULER_REPEATING);
		scheduleTypes.add(Constants.SCHEDULER_COMPLEX_REPEATING);

		hours = new ArrayList < Integer > ();
		for (int i = 0; i < 25; i++) {
			hours.add(i);
		}

		minutes = new ArrayList < Integer > ();
		for (int i = 0; i < 61; i++) {
			minutes.add(i);
		}
	}

	public void checkScheduleType() {
		if (task.getSchedulerType().equals(Constants.SCHEDULER_RUN_ONCE)) {
			runOnce = true;
			repeating = false;
			complexRepeating = false;
			repeatingOrComplexRepeating = false;
		} else if (task.getSchedulerType().equals(Constants.SCHEDULER_REPEATING)) {
			runOnce = false;
			repeating = true;
			complexRepeating = false;
			repeatingOrComplexRepeating = true;
		} else if (task.getSchedulerType().equals(
				Constants.SCHEDULER_COMPLEX_REPEATING)) {
			runOnce = false;
			repeating = false;
			complexRepeating = true;
			repeatingOrComplexRepeating = true;
		} else {
			runOnce = false;
			repeating = false;
			complexRepeating = false;
			repeatingOrComplexRepeating = false;
		}
	}

	public void checkScheduleTypePrev() {
		if (prevTask.getSchedulerType().equals(Constants.SCHEDULER_RUN_ONCE)) {
			prevRunOnce = true;
			prevRepeating = false;
			prevComplexRepeating = false;
			prevRepeatingOrComplexRepeating = false;
		} else if (prevTask.getSchedulerType().equals(Constants.SCHEDULER_REPEATING)) {
			prevRunOnce = false;
			prevRepeating = true;
			prevComplexRepeating = false;
			prevRepeatingOrComplexRepeating = true;
		} else if (prevTask.getSchedulerType().equals(
				Constants.SCHEDULER_COMPLEX_REPEATING)) {
			prevRunOnce = false;
			prevRepeating = false;
			prevComplexRepeating = true;
			prevRepeatingOrComplexRepeating = true;
		} else {
			prevRunOnce = false;
			prevRepeating = false;
			prevComplexRepeating = false;
			prevRepeatingOrComplexRepeating = false;
		}
	}

	public void approveChecked() {
		
		List < Approval > checkedItemsUser 				= new ArrayList < Approval > ();

		List < Approval > checkedItemsRole 				= new ArrayList < Approval > ();

		List < Approval > checkedItemsScheduler 		= new ArrayList < Approval > ();

		List < Approval > checkedItemsSystemParameter 	= new ArrayList < Approval > ();

		List < Approval > checkedItemsBranch 			= new ArrayList < Approval > ();

		List < Approval > checkedItemsHoliday 			= new ArrayList < Approval > ();

		Iterator entries = approvalTemps.entrySet().iterator();
		while (entries.hasNext()) {
			Entry thisEntry = (Entry) entries.next();
			Approval a = (Approval) thisEntry.getValue();
			switch (a.getParameter()){
			
				case Constants.APPROVAL_PARAMETER_USER 		: checkedItemsUser.add(a);				break;
				
				case Constants.APPROVAL_PARAMETER_ROLE 		: checkedItemsRole.add(a);				break;	
				
				case Constants.APPROVAL_PARAMETER_SCHEDULER : checkedItemsScheduler.add(a);			break;
				
				case Constants.APPROVAL_PARAMETER_SPARAM 	: checkedItemsSystemParameter.add(a);	break;
				
				case Constants.APPROVAL_PARAMETER_BRANCH 	: checkedItemsBranch.add(a);			break;
				
				case Constants.APPROVAL_PARAMETER_HOLIDAY 	: checkedItemsHoliday.add(a);			break;
				
			}
		}
		
		approveCheckedUser(checkedItemsUser);
		
		approveCheckedRole(checkedItemsRole);
		
		approveCheckedScheduler(checkedItemsScheduler);
		
		approveCheckedScheduler(checkedItemsScheduler);

		approveCheckedSystemParameter(checkedItemsSystemParameter);

		approveCheckedBranch(checkedItemsBranch);

		approveCheckedHoliday(checkedItemsHoliday);
		
		String headerMessage = "Success";
		String bodyMessage = "Approve checked finished";

		FacesUtil.setInfoMessageDialog(headerMessage, bodyMessage);

	}

	public void approveCheckedUser(List < Approval > checkedItemsUser){

		ApprovalDao approvalDao = (ApprovalDao) applicationBean.getCorpGWContext().getBean("approvalDAO");
		
		if (checkedItemsUser.size() != 0) {
			for (Approval a: checkedItemsUser) {
				User user = null;
				try {
					if (a.getAction().equals(Constants.APPROVAL_ACTION_DELETE)) {
						user = (User) HelperUtils.deserialize(a.getOldBlobObject().getBinaryStream());
						List < UserRole > roles = user.getRoles();
						RoleDao roleDao = (RoleDao) applicationBean.getCorpGWContext().getBean("roleDAO");
						for (UserRole r: roles) {
							roleDao.delete(r);
						}
						UserDao userDao = getUserDAOBean();
						userDao.deleteUser(user.getUserID());
						doLoging(ConstantsUserLog.MENU_APPROVAL, ConstantsUserLog.ACTIVITY_APPROVE.concat(" ").concat(ConstantsUserLog.ACTIVITY_DELETE), "Approve Delete User : "+user.getUserName()+".");
					} else if (a.getAction().equals(
					Constants.APPROVAL_ACTION_CREATE)) {
						user = (User) HelperUtils.deserialize(a.getNewBlobObject().getBinaryStream());
						user.setStatus(Constants.APPROVAL_STATUS_APPROVED_CREATE);
						getUserDAOBean().deleteUserRoleByUsername(user.getUserID());
						for (int i = 0 ; i < user.getRoles().size() ; i ++){
							user.getRoles().get(i).setUserId(user.getUserID());
						}
						user.setApprovalBy(sessionBean.getUser().getUserName());
						user.setApprovalDate(new Date());
						getUserDAOBean().saveUserToDB(user);
						doLoging(ConstantsUserLog.MENU_APPROVAL, ConstantsUserLog.ACTIVITY_APPROVE.concat(" ").concat(ConstantsUserLog.ACTIVITY_SAVE), "Approve Save User : "+user.getUserName()+".");
						//SEND EMAIL TO USER
						sendEmailToUser(user.getUserName(), user.getPassword(), user.getEmail());
					} else if (a.getAction()
						.equals(Constants.APPROVAL_ACTION_EDIT)) {
						user = (User) HelperUtils.deserialize(a.getNewBlobObject().getBinaryStream());
						user.setStatus(Constants.APPROVAL_STATUS_APPROVED_EDIT);
						getUserDAOBean().deleteUserRoleByUsername(user.getUserID());
						for (int i = 0 ; i < user.getRoles().size() ; i ++){
							user.getRoles().get(i).setUserId(user.getUserID());
						}
						user.setApprovalBy(sessionBean.getUser().getUserName());
						user.setApprovalDate(new Date());
						getUserDAOBean().saveUserToDB(user);
						doLoging(ConstantsUserLog.MENU_APPROVAL, ConstantsUserLog.ACTIVITY_APPROVE.concat(" ").concat(ConstantsUserLog.ACTIVITY_EDIT), "Approve Edit User : "+user.getUserName()+".");
					}
					approvalDao.saveApprovalLog(a, Constants.APPROVAL_STATUS_CODE_SUCCESS, "Success");
					approvalDao.deleteApproval(a);
				} catch (Exception e) {
					approvalDao.saveApprovalLog(a, Constants.APPROVAL_STATUS_CODE_FAILED, e.getMessage());
				}	
			}
		}
	}
	
	public void approveCheckedRole(List < Approval > checkedItemsRole){
		ApprovalDao approvalDao = (ApprovalDao) applicationBean.getCorpGWContext().getBean("approvalDAO");

		if (checkedItemsRole.size() != 0) {
			for (Approval a: checkedItemsRole) {
				Role role = null;
				try {
					if (a.getAction().equals(Constants.APPROVAL_ACTION_DELETE)) {
						role = (Role) HelperUtils.deserialize(a.getOldBlobObject().getBinaryStream());
						UserDao userDao = (UserDao) applicationBean.getCorpGWContext().getBean("userDAO");
						userDao.deleteUserRole(role.getRoleId());
						RoleDao roleDao = (RoleDao) applicationBean.getCorpGWContext().getBean("roleDAO");
						roleDao.deleteRole(role);
						doLoging(ConstantsUserLog.MENU_APPROVAL, ConstantsUserLog.ACTIVITY_APPROVE.concat(" ").concat(ConstantsUserLog.ACTIVITY_DELETE), "Approve Delete Role : "+role.getRoleName()+".");
					} else if (a.getAction().equals(
					Constants.APPROVAL_ACTION_CREATE)) {
						role = (Role) HelperUtils.deserialize(a.getNewBlobObject().getBinaryStream());
						role.setStatus(Constants.APPROVAL_STATUS_APPROVED_CREATE);
						role.setApprovalBy(sessionBean.getUser().getUserName());
						role.setApprovalDate(new Date());
						getRoleDAOBean().saveOrUpdateRole(role);
						doLoging(ConstantsUserLog.MENU_APPROVAL, ConstantsUserLog.ACTIVITY_APPROVE.concat(" ").concat(ConstantsUserLog.ACTIVITY_SAVE), "Approve Create Role : "+role.getRoleName()+".");
					} else if (a.getAction()
						.equals(Constants.APPROVAL_ACTION_EDIT)) {
						role = (Role) HelperUtils.deserialize(a.getNewBlobObject().getBinaryStream());
						role.setStatus(Constants.APPROVAL_STATUS_APPROVED_EDIT);
						role.setApprovalBy(sessionBean.getUser().getUserName());
						role.setApprovalDate(new Date());
						getRoleDAOBean().saveOrUpdateRole(role);
						doLoging(ConstantsUserLog.MENU_APPROVAL, ConstantsUserLog.ACTIVITY_APPROVE.concat(" ").concat(ConstantsUserLog.ACTIVITY_EDIT), "Approve Edit Role : "+role.getRoleName()+".");
					}
					approvalDao.saveApprovalLog(a, Constants.APPROVAL_STATUS_CODE_SUCCESS, "Success");
					approvalDao.deleteApproval(a);
				} catch (Exception e) {
					approvalDao.saveApprovalLog(a, Constants.APPROVAL_STATUS_CODE_FAILED, e.getMessage());
				}	
			}
		}
	}
	
	public void approveCheckedScheduler(List < Approval > checkedItemsScheduler){
		ApprovalDao approvalDao = (ApprovalDao) applicationBean.getCorpGWContext().getBean("approvalDAO");
		
		if (checkedItemsScheduler.size() != 0) {
			for (Approval a: checkedItemsScheduler) {
				try {
					ScheduleTask task = null;
					if (a.getAction().equals(Constants.APPROVAL_ACTION_DELETE)) {
						task = (ScheduleTask) HelperUtils.deserialize(a.getOldBlobObject()
							.getBinaryStream());
						SchedulerDao schedulerDao = (SchedulerDao) applicationBean.getCorpGWContext().getBean("schedulerDAO");
						
						WebServiceSchedulerExecutor.deleteScheduler(task.getTaskId());

						schedulerDao.deleteTask(task.getSchedulerId());
					} else if (a.getAction().equals(
					Constants.APPROVAL_ACTION_CREATE)) {
						task = (ScheduleTask) HelperUtils.deserialize(a.getNewBlobObject()
							.getBinaryStream());
						task.setApprovalStatus(Constants.APPROVAL_STATUS_APPROVED_CREATE);
						
						String taskId = WebServiceSchedulerExecutor.createScheduler(task);
						task.setTaskId(taskId);
						
						WebServiceSchedulerExecutor.startScheduler(taskId);

						task.setSchedulerStatus(WebServiceSchedulerExecutor.statusScheduler(task.getTaskId()));						

						task.setApprovalBy(sessionBean.getUser().getUserName());
						task.setApprovalDate(new Date());
						getSchedulerDAOBean().saveTask(task);
					} else if (a.getAction()
						.equals(Constants.APPROVAL_ACTION_EDIT)) {
						task = (ScheduleTask) HelperUtils.deserialize(a.getNewBlobObject()
							.getBinaryStream());
						task.setApprovalStatus(Constants.APPROVAL_STATUS_APPROVED_EDIT);

						WebServiceSchedulerExecutor.deleteScheduler(task.getTaskId());
						
						String taskId = WebServiceSchedulerExecutor.createScheduler(task);
						task.setTaskId(taskId);
						
						WebServiceSchedulerExecutor.startScheduler(task.getTaskId());

						task.setSchedulerStatus(WebServiceSchedulerExecutor.statusScheduler(task.getTaskId()));

						task.setApprovalBy(sessionBean.getUser().getUserName());
						task.setApprovalDate(new Date());
						getSchedulerDAOBean().saveTask(task);;
					}
					approvalDao.saveApprovalLog(a, Constants.APPROVAL_STATUS_CODE_SUCCESS, "Success");
					approvalDao.deleteApproval(a);
				} catch (Exception e) {
					approvalDao.saveApprovalLog(a, Constants.APPROVAL_STATUS_CODE_FAILED, e.getMessage());
				}
			}
		}
	}
	
	public void approveCheckedSystemParameter(List < Approval > checkedItemsSystemParameter){
		ApprovalDao approvalDao = (ApprovalDao) applicationBean.getCorpGWContext().getBean("approvalDAO");
		
		if (checkedItemsSystemParameter.size() != 0) {
			for (Approval a: checkedItemsSystemParameter) {
				try {
					SystemParameter systemParameter = null;
					if (a.getAction().equals(Constants.APPROVAL_ACTION_DELETE)) {
						systemParameter = (SystemParameter) HelperUtils.deserialize(a.getOldBlobObject()
							.getBinaryStream());
						SystemParameterDao systemParameterDao = (SystemParameterDao) applicationBean.getCorpGWContext().getBean("systemParameterDAO");
						systemParameterDao.deleteSystemParameter(systemParameter.getParamId());
						doLoging(ConstantsUserLog.MENU_APPROVAL, ConstantsUserLog.ACTIVITY_APPROVE.concat(" ").concat(ConstantsUserLog.ACTIVITY_DELETE), "Approve Delete System Parameter : "+systemParameter.getParamName()+".");
					} else if (a.getAction().equals(
					Constants.APPROVAL_ACTION_CREATE)) {
						systemParameter = (SystemParameter) HelperUtils.deserialize(a.getNewBlobObject()
							.getBinaryStream());
						systemParameter.setStatus(Constants.APPROVAL_STATUS_APPROVED_CREATE);
						SystemParameterDao systemParameterDao = (SystemParameterDao) applicationBean.getCorpGWContext().getBean("systemParameterDAO");
						systemParameter.setApprovalBy(sessionBean.getUser().getUserName());
						systemParameter.setApprovalDate(new Date());
						systemParameterDao.saveSystemParameter(systemParameter);
						doLoging(ConstantsUserLog.MENU_APPROVAL, ConstantsUserLog.ACTIVITY_APPROVE.concat(" ").concat(ConstantsUserLog.ACTIVITY_SAVE), "Approve Create System Parameter : "+systemParameter.getParamName()+".");
					} else if (a.getAction()
						.equals(Constants.APPROVAL_ACTION_EDIT)) {
						systemParameter = (SystemParameter) HelperUtils.deserialize(a.getNewBlobObject()
							.getBinaryStream());
						systemParameter.setStatus(Constants.APPROVAL_STATUS_APPROVED_EDIT);
						SystemParameterDao systemParameterDao = (SystemParameterDao) applicationBean.getCorpGWContext().getBean("systemParameterDAO");
						systemParameter.setApprovalBy(sessionBean.getUser().getUserName());
						systemParameter.setApprovalDate(new Date());
						systemParameterDao.saveSystemParameter(systemParameter);
						
						updateTProviderByParamName(systemParameter, "DEBIT_ACCOUNT_NO_GAJI", Constants.CODE_GAJI, systemParameter.getParamValue());
						updateTProviderByParamName(systemParameter, "DEBIT_ACCOUNT_NO_GAJIR", Constants.CODE_GAJIR, systemParameter.getParamValue());
						
						doLoging(ConstantsUserLog.MENU_APPROVAL, ConstantsUserLog.ACTIVITY_APPROVE.concat(" ").concat(ConstantsUserLog.ACTIVITY_EDIT), "Approve Edit System Parameter : "+systemParameter.getParamName()+".");
					}
					approvalDao.saveApprovalLog(a, Constants.APPROVAL_STATUS_CODE_SUCCESS, "Success");
					approvalDao.deleteApproval(a);
				} catch (Exception e) {
					approvalDao.saveApprovalLog(a, Constants.APPROVAL_STATUS_CODE_FAILED, e.getMessage());
				}
			}
		}
	}
	
	public void updateTProviderByParamName(SystemParameter param, String paramName, String code, String value){
		if (param.getParamName().equals(paramName)) {
			ProviderDao providerDao = (ProviderDao) applicationBean.getCorpGWContext().getBean("providerDao");
			providerDao.updateProviderAccount(code, value);
		}
	}
	
	public void approveCheckedBranch(List < Approval > checkedItemsBranch){
		ApprovalDao approvalDao = (ApprovalDao) applicationBean.getCorpGWContext().getBean("approvalDAO");
		
		if (checkedItemsBranch.size() != 0) {
			for (Approval a: checkedItemsBranch) {
				try {
					Branch branch = null;
					if (a.getAction().equals(Constants.APPROVAL_ACTION_DELETE)) {
						branch = (Branch) HelperUtils.deserialize(a.getOldBlobObject()
							.getBinaryStream());
						BranchDao branchDao = (BranchDao) applicationBean.getCorpGWContext().getBean("branchDAO");
						branchDao.deleteBranch(branch.getBranchId());
					} else if (a.getAction().equals(
					Constants.APPROVAL_ACTION_CREATE)) {
						branch = (Branch) HelperUtils.deserialize(a.getNewBlobObject()
							.getBinaryStream());
						branch.setStatus(Constants.APPROVAL_STATUS_APPROVED_CREATE);
						BranchDao branchDao = (BranchDao) applicationBean.getCorpGWContext().getBean("branchDAO");
						branch.setApprovalBy(sessionBean.getUser().getUserName());
						branch.setApprovalDate(new Date());
						branchDao.saveBranch(branch);
					} else if (a.getAction()
						.equals(Constants.APPROVAL_ACTION_EDIT)) {
						branch = (Branch) HelperUtils.deserialize(a.getNewBlobObject()
							.getBinaryStream());
						branch.setStatus(Constants.APPROVAL_STATUS_APPROVED_EDIT);
						BranchDao branchDao = (BranchDao) applicationBean.getCorpGWContext().getBean("branchDAO");
						branch.setApprovalBy(sessionBean.getUser().getUserName());
						branch.setApprovalDate(new Date());
						branchDao.saveBranch(branch);
					}
					approvalDao.saveApprovalLog(a, Constants.APPROVAL_STATUS_CODE_SUCCESS, "Success");
					approvalDao.deleteApproval(a);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					approvalDao.saveApprovalLog(a, Constants.APPROVAL_STATUS_CODE_FAILED, e.getMessage());
				}
			}
		}
	}
	
	public void approveCheckedHoliday(List < Approval > checkedItemsHoliday){
		ApprovalDao approvalDao = (ApprovalDao) applicationBean.getCorpGWContext().getBean("approvalDAO");
		
		if (checkedItemsHoliday.size() != 0) {
			for (Approval a: checkedItemsHoliday) {
				try {
					Holiday holiday = null;
					if (a.getAction().equals(Constants.APPROVAL_ACTION_DELETE)) {
						holiday = (Holiday) HelperUtils.deserialize(a.getOldBlobObject()
							.getBinaryStream());
						HolidayDao holidayDao = (HolidayDao) applicationBean.getCorpGWContext().getBean("holidayDAO");
						holidayDao.deleteHoliday(holiday.getHolidayId());
					} else if (a.getAction().equals(
					Constants.APPROVAL_ACTION_CREATE)) {
						holiday = (Holiday) HelperUtils.deserialize(a.getNewBlobObject()
							.getBinaryStream());
						holiday.setStatus(Constants.APPROVAL_STATUS_APPROVED_CREATE);
						HolidayDao holidayDao = (HolidayDao) applicationBean.getCorpGWContext().getBean("holidayDAO");
						holiday.setApprovalBy(sessionBean.getUser().getUserName());
						holiday.setApprovalDate(new Date());
						holidayDao.saveHoliday(holiday);
					} else if (a.getAction()
						.equals(Constants.APPROVAL_ACTION_EDIT)) {
						holiday = (Holiday) HelperUtils.deserialize(a.getNewBlobObject()
							.getBinaryStream());
						holiday.setStatus(Constants.APPROVAL_STATUS_APPROVED_EDIT);
						HolidayDao holidayDao = (HolidayDao) applicationBean.getCorpGWContext().getBean("holidayDAO");
						holiday.setApprovalBy(sessionBean.getUser().getUserName());
						holiday.setApprovalDate(new Date());
						holidayDao.saveHoliday(holiday);
					}
					approvalDao.saveApprovalLog(a, Constants.APPROVAL_STATUS_CODE_SUCCESS, "Success");
					approvalDao.deleteApproval(a);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					approvalDao.saveApprovalLog(a, Constants.APPROVAL_STATUS_CODE_FAILED, e.getMessage());
				}
			}
		}
	}
	
	public void approveAll() {

		try{
			

			ApprovalDao approvalDao = (ApprovalDao) applicationBean.getCorpGWContext().getBean("approvalDAO");
			List < Approval > approvals = new ArrayList < Approval > ();
			
			String approverId = sessionBean.getUser().getUserID();
			String branchId = sessionBean.getUser().getBranch();
			
			approvals = approvalDao.getApprovalByParameterSearch(approverId, parameterId, parameterName, searchParameter, searchAction, branchId);

			approveAllUser(approvals);
			
			approveAllRole(approvals);
			
			approveAllScheduler(approvals);
			
			approveAllSystemParameter(approvals);
			
			approveAllBranch(approvals);
			
			approveAllHoliday(approvals);
			
			String headerMessage = "Success";
			String bodyMessage = "Approve all finished";

			FacesUtil.setInfoMessageDialog(headerMessage, bodyMessage);
			
		}catch(Exception e){
			String headerMessage = "Error";
			String bodyMessage = e.getMessage();
			e.printStackTrace();
			FacesUtil.setErrorMessageDialog(headerMessage, bodyMessage);
		}
	

	}

	public void approveAllUser(List<Approval> approvals){

		ApprovalDao approvalDao = (ApprovalDao) applicationBean.getCorpGWContext().getBean("approvalDAO");
		
		if (approvals.size() != 0) {
			
				for (Approval a: approvals) {
					if (a.getParameter().equals(Constants.APPROVAL_PARAMETER_USER)){
						User user = null;
						try{
							if (a.getAction().equals(Constants.APPROVAL_ACTION_DELETE)) {
								user = (User) HelperUtils.deserialize(a.getOldBlobObject().getBinaryStream());
								List < UserRole > roles = user.getRoles();
								RoleDao roleDao = (RoleDao) applicationBean.getCorpGWContext().getBean("roleDAO");
								for (UserRole r: roles) {
									roleDao.delete(r);
								}
								UserDao userDao = getUserDAOBean();
								userDao.deleteUser(user.getUserID());
								doLoging(ConstantsUserLog.MENU_APPROVAL, ConstantsUserLog.ACTIVITY_APPROVE.concat(" ").concat(ConstantsUserLog.ACTIVITY_DELETE), "Approve Delete User : "+user.getUserName()+".");
							} else if (a.getAction().equals(
							Constants.APPROVAL_ACTION_CREATE)) {
								user = (User) HelperUtils.deserialize(a.getNewBlobObject().getBinaryStream());
								user.setStatus(Constants.APPROVAL_STATUS_APPROVED_CREATE);
								getUserDAOBean().deleteUserRoleByUsername(user.getUserID());
								for (int i = 0 ; i < user.getRoles().size() ; i ++){
									user.getRoles().get(i).setUserId(user.getUserID());
								}
								user.setApprovalBy(sessionBean.getUser().getUserName());
								user.setApprovalDate(new Date());
								getUserDAOBean().saveUserToDB(user);
								//SEND EMAIL TO USER
								sendEmailToUser(user.getUserName(), user.getPassword(), user.getEmail());
								doLoging(ConstantsUserLog.MENU_APPROVAL, ConstantsUserLog.ACTIVITY_APPROVE.concat(" ").concat(ConstantsUserLog.ACTIVITY_SAVE), "Approve Save User : "+user.getUserName()+".");
							} else if (a.getAction()
								.equals(Constants.APPROVAL_ACTION_EDIT)) {
								user = (User) HelperUtils.deserialize(a.getNewBlobObject().getBinaryStream());
								user.setStatus(Constants.APPROVAL_STATUS_APPROVED_EDIT);
								getUserDAOBean().deleteUserRoleByUsername(user.getUserID());
								for (int i = 0 ; i < user.getRoles().size() ; i ++){
									user.getRoles().get(i).setUserId(user.getUserID());
								}
								user.setApprovalBy(sessionBean.getUser().getUserName());
								user.setApprovalDate(new Date());
								getUserDAOBean().saveUserToDB(user);
								doLoging(ConstantsUserLog.MENU_APPROVAL, ConstantsUserLog.ACTIVITY_APPROVE.concat(" ").concat(ConstantsUserLog.ACTIVITY_EDIT), "Approve Edit User : "+user.getUserName()+".");
							}
							approvalDao.saveApprovalLog(a, Constants.APPROVAL_STATUS_CODE_SUCCESS, "Success");
							approvalDao.deleteApproval(a);
						}catch (Exception e) {
							// TODO Auto-generated catch block
							approvalDao.saveApprovalLog(a, Constants.APPROVAL_STATUS_CODE_FAILED, e.getMessage());
						}	
					}
				} 
			
			
		}
	}
	
	public void approveAllRole(List<Approval> approvals){
		
		ApprovalDao approvalDao = (ApprovalDao) applicationBean.getCorpGWContext().getBean("approvalDAO");
		
		if (approvals.size() != 0) {
				for (Approval a: approvals) {
					if (a.getParameter().equals(Constants.APPROVAL_PARAMETER_ROLE)){
						Role role = null;
						try{
							if (a.getAction().equals(Constants.APPROVAL_ACTION_DELETE)) {
								role = (Role) HelperUtils.deserialize(a.getOldBlobObject().getBinaryStream());
								UserDao userDao = (UserDao) applicationBean.getCorpGWContext().getBean("userDAO");
								userDao.deleteUserRole(role.getRoleId());
								RoleDao roleDao = (RoleDao) applicationBean.getCorpGWContext().getBean("roleDAO");
								roleDao.deleteRole(role);
								doLoging(ConstantsUserLog.MENU_APPROVAL, ConstantsUserLog.ACTIVITY_APPROVE.concat(" ").concat(ConstantsUserLog.ACTIVITY_DELETE), "Approve Delete Role : "+role.getRoleName()+".");
							} else if (a.getAction().equals(
							Constants.APPROVAL_ACTION_CREATE)) {
								role = (Role) HelperUtils.deserialize(a.getNewBlobObject().getBinaryStream());
								role.setStatus(Constants.APPROVAL_STATUS_APPROVED_CREATE);
								role.setApprovalBy(sessionBean.getUser().getUserName());
								role.setApprovalDate(new Date());
								getRoleDAOBean().saveOrUpdateRole(role);
								doLoging(ConstantsUserLog.MENU_APPROVAL, ConstantsUserLog.ACTIVITY_APPROVE.concat(" ").concat(ConstantsUserLog.ACTIVITY_SAVE), "Approve Create Role : "+role.getRoleName()+".");
							} else if (a.getAction()
								.equals(Constants.APPROVAL_ACTION_EDIT)) {
								role = (Role) HelperUtils.deserialize(a.getNewBlobObject().getBinaryStream());
								role.setStatus(Constants.APPROVAL_STATUS_APPROVED_EDIT);
								role.setApprovalBy(sessionBean.getUser().getUserName());
								role.setApprovalDate(new Date());
								getRoleDAOBean().saveOrUpdateRole(role);
								doLoging(ConstantsUserLog.MENU_APPROVAL, ConstantsUserLog.ACTIVITY_APPROVE.concat(" ").concat(ConstantsUserLog.ACTIVITY_EDIT), "Approve Edit Role : "+role.getRoleName()+".");
							}
							approvalDao.saveApprovalLog(a, Constants.APPROVAL_STATUS_CODE_SUCCESS, "Success");
							approvalDao.deleteApproval(a);
						}catch (Exception e) {
							// TODO Auto-generated catch block
							approvalDao.saveApprovalLog(a, Constants.APPROVAL_STATUS_CODE_FAILED, e.getMessage());
						}
					}
				}
		}
	}
	
	public void approveAllScheduler(List<Approval> approvals){
		
		ApprovalDao approvalDao = (ApprovalDao) applicationBean.getCorpGWContext().getBean("approvalDAO");
		
		if (approvals.size() != 0) {
				for (Approval a: approvals) {
					if (a.getParameter().equals(Constants.APPROVAL_PARAMETER_SCHEDULER)){
						ScheduleTask task = null;
						try{
							if (a.getAction().equals(Constants.APPROVAL_ACTION_DELETE)) {
								task = (ScheduleTask) HelperUtils.deserialize(a.getOldBlobObject()
									.getBinaryStream());
								SchedulerDao schedulerDao = getSchedulerDAOBean();
								
								WebServiceSchedulerExecutor.deleteScheduler(task.getTaskId());
	
								schedulerDao.deleteTask(task.getSchedulerId());
							} else if (a.getAction().equals( Constants.APPROVAL_ACTION_CREATE)) {
								task = (ScheduleTask) HelperUtils.deserialize(a.getNewBlobObject()
									.getBinaryStream());
								task.setApprovalStatus(Constants.APPROVAL_STATUS_APPROVED_CREATE);
								
								String taskId = WebServiceSchedulerExecutor.createScheduler(task);
								task.setTaskId(taskId);
								
								WebServiceSchedulerExecutor.startScheduler(taskId);
	
								task.setSchedulerStatus(WebServiceSchedulerExecutor.statusScheduler(task.getTaskId()));									
	
	
								task.setApprovalBy(sessionBean.getUser().getUserName());
								task.setApprovalDate(new Date());
								getSchedulerDAOBean().saveTask(task);
								
							} else if (a.getAction()
								.equals(Constants.APPROVAL_ACTION_EDIT)) {
								task = (ScheduleTask) HelperUtils.deserialize(a.getNewBlobObject()
									.getBinaryStream());
								task.setApprovalStatus(Constants.APPROVAL_STATUS_APPROVED_EDIT);
								
								WebServiceSchedulerExecutor.deleteScheduler(task.getTaskId());
								
								String taskId = WebServiceSchedulerExecutor.createScheduler(task);
								task.setTaskId(taskId);
								
								WebServiceSchedulerExecutor.startScheduler(task.getTaskId());
	
								task.setSchedulerStatus(WebServiceSchedulerExecutor.statusScheduler(task.getTaskId()));
	
								task.setApprovalBy(sessionBean.getUser().getUserName());
								task.setApprovalDate(new Date());
								getSchedulerDAOBean().saveTask(task);
							}
							approvalDao.saveApprovalLog(a, Constants.APPROVAL_STATUS_CODE_SUCCESS, "Success");
							approvalDao.deleteApproval(a);
						}catch (Exception e) {
							// TODO Auto-generated catch block
							approvalDao.saveApprovalLog(a, Constants.APPROVAL_STATUS_CODE_FAILED, e.getMessage());
						}
					}
				}
		}
	}
	
	public void approveAllSystemParameter(List<Approval> approvals){
		
		ApprovalDao approvalDao = (ApprovalDao) applicationBean.getCorpGWContext().getBean("approvalDAO");
		
		if (approvals.size() != 0){
				for (Approval a : approvals){
					if (a.getParameter().equals(Constants.APPROVAL_PARAMETER_SPARAM)){
						SystemParameter systemParameter = null;
						try{
							if (a.getAction().equals(Constants.APPROVAL_ACTION_DELETE)){
								systemParameter = (SystemParameter) HelperUtils.deserialize(a.getOldBlobObject().getBinaryStream());
								getSystemParameterDAOBean().deleteSystemParameter(systemParameter.getParamId());
								doLoging(ConstantsUserLog.MENU_APPROVAL, ConstantsUserLog.ACTIVITY_APPROVE.concat(" ").concat(ConstantsUserLog.ACTIVITY_DELETE), "Approve Delete System Parameter : "+systemParameter.getParamName()+".");
							}else if (a.getAction().equals(Constants.APPROVAL_ACTION_CREATE)){
		
								systemParameter = (SystemParameter) HelperUtils.deserialize(a.getNewBlobObject().getBinaryStream());
								systemParameter.setStatus(Constants.APPROVAL_STATUS_APPROVED_CREATE);
	
								systemParameter.setApprovalBy(sessionBean.getUser().getUserName());
								systemParameter.setApprovalDate(new Date());
								getSystemParameterDAOBean().saveSystemParameter(systemParameter);
								doLoging(ConstantsUserLog.MENU_APPROVAL, ConstantsUserLog.ACTIVITY_APPROVE.concat(" ").concat(ConstantsUserLog.ACTIVITY_SAVE), "Approve Create System Parameter : "+systemParameter.getParamName()+".");
							}else if (a.getAction().equals(Constants.APPROVAL_ACTION_EDIT)){
		
								systemParameter = (SystemParameter) HelperUtils.deserialize(a.getNewBlobObject().getBinaryStream());
								systemParameter.setStatus(Constants.APPROVAL_STATUS_APPROVED_EDIT);
	
								systemParameter.setApprovalBy(sessionBean.getUser().getUserName());
								systemParameter.setApprovalDate(new Date());
								getSystemParameterDAOBean().saveSystemParameter(systemParameter);
								
								updateTProviderByParamName(systemParameter, "DEBIT_ACCOUNT_NO_GAJI", Constants.CODE_GAJI, systemParameter.getParamValue());
								updateTProviderByParamName(systemParameter, "DEBIT_ACCOUNT_NO_GAJIR", Constants.CODE_GAJIR, systemParameter.getParamValue());
								doLoging(ConstantsUserLog.MENU_APPROVAL, ConstantsUserLog.ACTIVITY_APPROVE.concat(" ").concat(ConstantsUserLog.ACTIVITY_EDIT), "Approve Edit System Parameter : "+systemParameter.getParamName()+".");
							}
							approvalDao.saveApprovalLog(a, Constants.APPROVAL_STATUS_CODE_SUCCESS, "Success");
							approvalDao.deleteApproval(a);
						}catch (Exception e) {
							// TODO Auto-generated catch block
							approvalDao.saveApprovalLog(a, Constants.APPROVAL_STATUS_CODE_FAILED, e.getMessage());
						}
					}
			}
			
		}
	}
	
	public void approveAllBranch(List<Approval> approvals){
		
		ApprovalDao approvalDao = (ApprovalDao) applicationBean.getCorpGWContext().getBean("approvalDAO");
		
		if (approvals.size() != 0){
				for (Approval a : approvals){
					if (a.getParameter().equals(Constants.APPROVAL_PARAMETER_BRANCH)){
					Branch branch = null;
						try{
							if (a.getAction().equals(Constants.APPROVAL_ACTION_DELETE)){
								branch = (Branch) HelperUtils.deserialize(a.getOldBlobObject().getBinaryStream());
								getBranchDAOBean().deleteBranch(branch.getBranchId());
							}else if (a.getAction().equals(Constants.APPROVAL_ACTION_CREATE)){
		
								branch = (Branch) HelperUtils.deserialize(a.getNewBlobObject().getBinaryStream());
								branch.setStatus(Constants.APPROVAL_STATUS_APPROVED_CREATE);
	
								branch.setApprovalBy(sessionBean.getUser().getUserName());
								branch.setApprovalDate(new Date());
								getBranchDAOBean().saveBranch(branch);
							}else if (a.getAction().equals(Constants.APPROVAL_ACTION_EDIT)){
		
								branch = (Branch) HelperUtils.deserialize(a.getNewBlobObject().getBinaryStream());
								Branch branchOld = (Branch) HelperUtils.deserialize(a.getOldBlobObject().getBinaryStream());
								branch.setStatus(Constants.APPROVAL_STATUS_APPROVED_EDIT);
	
								branch.setApprovalBy(sessionBean.getUser().getUserName());
								branch.setApprovalDate(new Date());
								
								getBranchDAOBean().saveBranch(branch);
								
								getUserDAOBean().updateUserBranchByBranchCode(branchOld.getBranchCode(), branch.getBranchCode());
								
							}
							approvalDao.saveApprovalLog(a, Constants.APPROVAL_STATUS_CODE_SUCCESS, "Success");
							approvalDao.deleteApproval(a);
						}catch (Exception e) {
							// TODO Auto-generated catch block
							approvalDao.saveApprovalLog(a, Constants.APPROVAL_STATUS_CODE_FAILED, e.getMessage());
						}
					}
				}
			
		}
	}
	
	public void approveAllHoliday(List<Approval> approvals){
		
		ApprovalDao approvalDao = (ApprovalDao) applicationBean.getCorpGWContext().getBean("approvalDAO");
		
		if (approvals.size() != 0){
				for (Approval a : approvals){
					if (a.getParameter().equals(Constants.APPROVAL_PARAMETER_HOLIDAY)){
						Holiday holiday = null;
						try{
							if (a.getAction().equals(Constants.APPROVAL_ACTION_DELETE)){
								holiday = (Holiday) HelperUtils.deserialize(a.getOldBlobObject().getBinaryStream());
								getHolidayDAOBean().deleteHoliday(holiday.getHolidayId());
							}else if (a.getAction().equals(Constants.APPROVAL_ACTION_CREATE)){
		
								holiday = (Holiday) HelperUtils.deserialize(a.getNewBlobObject().getBinaryStream());
								holiday.setStatus(Constants.APPROVAL_STATUS_APPROVED_CREATE);
								holiday.setApprovalBy(sessionBean.getUser().getUserName());
								holiday.setApprovalDate(new Date());
								getHolidayDAOBean().saveHoliday(holiday);
							}else if (a.getAction().equals(Constants.APPROVAL_ACTION_EDIT)){
		
								holiday = (Holiday) HelperUtils.deserialize(a.getNewBlobObject().getBinaryStream());
								holiday.setStatus(Constants.APPROVAL_STATUS_APPROVED_EDIT);
								holiday.setApprovalBy(sessionBean.getUser().getUserName());
								holiday.setApprovalDate(new Date());
								getHolidayDAOBean().saveHoliday(holiday);
							}
							approvalDao.saveApprovalLog(a, Constants.APPROVAL_STATUS_CODE_SUCCESS, "Success");
							approvalDao.deleteApproval(a);
						}catch (Exception e) {
							// TODO Auto-generated catch block
							approvalDao.saveApprovalLog(a, Constants.APPROVAL_STATUS_CODE_FAILED, e.getMessage());
						}
					}
				}
			
		}
	}
	
	public void rejectChecked() {
		
		List < Approval > checkedItemsUser 				= new ArrayList < Approval > ();

		List < Approval > checkedItemsRole 				= new ArrayList < Approval > ();

		List < Approval > checkedItemsScheduler 		= new ArrayList < Approval > ();

		List < Approval > checkedItemsSystemParameter 	= new ArrayList < Approval > ();

		List < Approval > checkedItemsBranch 			= new ArrayList < Approval > ();

		List < Approval > checkedItemsHoliday		 	= new ArrayList < Approval > ();

		Iterator entries = approvalTemps.entrySet().iterator();
		while (entries.hasNext()) {
			Entry thisEntry = (Entry) entries.next();
			Approval a = (Approval) thisEntry.getValue();
			switch (a.getParameter()){
			
				case Constants.APPROVAL_PARAMETER_USER 		: checkedItemsUser.add(a);				break;
				
				case Constants.APPROVAL_PARAMETER_ROLE 		: checkedItemsRole.add(a);				break;	
				
				case Constants.APPROVAL_PARAMETER_SCHEDULER : checkedItemsScheduler.add(a);			break;
				
				case Constants.APPROVAL_PARAMETER_SPARAM 	: checkedItemsSystemParameter.add(a);	break;
				
				case Constants.APPROVAL_PARAMETER_BRANCH 	: checkedItemsBranch.add(a);			break;
				
				case Constants.APPROVAL_PARAMETER_HOLIDAY 	: checkedItemsHoliday.add(a);			break;
				
			}
		}

		rejectCheckedUser(checkedItemsUser);
		
		rejectCheckedRole(checkedItemsRole);
		
		rejectCheckedScheduler(checkedItemsScheduler);
		
		rejectCheckedSystemParameter(checkedItemsSystemParameter);
		
		rejectCheckedBranch(checkedItemsBranch);
		
		rejectCheckedHoliday(checkedItemsHoliday);
		
		String headerMessage = "Success";
		String bodyMessage = "Reject checked finished";

		FacesUtil.setInfoMessageDialog(headerMessage, bodyMessage);

	}

	public void rejectCheckedUser(List < Approval > checkedItemsUser){
		ApprovalDao approvalDao = (ApprovalDao) applicationBean.getCorpGWContext().getBean("approvalDAO");
		if (checkedItemsUser.size() != 0) {
			for (Approval a: checkedItemsUser) {
				User user = null;
				try {
					if (a.getAction().equals(Constants.APPROVAL_ACTION_DELETE)) {
						user = (User) HelperUtils.deserialize(a.getOldBlobObject().getBinaryStream());
						user = getUserDAOBean().getDetailedUser(
						user.getUserID());
						user.setStatus(Constants.APPROVAL_STATUS_REJECTED_DELETE);
						user.setApprovalBy(sessionBean.getUser().getUserName());
						user.setApprovalDate(new Date());
						getUserDAOBean().saveUserToDB(user);
						doLoging(ConstantsUserLog.MENU_APPROVAL, ConstantsUserLog.ACTIVITY_REJECT.concat(" ").concat(ConstantsUserLog.ACTIVITY_DELETE), "Reject Delete User : "+user.getUserName()+".");
					} else if (a.getAction()
						.equals(Constants.APPROVAL_ACTION_EDIT)) {
						user = (User) HelperUtils.deserialize(a.getNewBlobObject().getBinaryStream());
						user = getUserDAOBean().getDetailedUser(
						user.getUserID());
						user.setStatus(Constants.APPROVAL_STATUS_REJECTED_EDIT);
						user.setApprovalBy(sessionBean.getUser().getUserName());
						user.setApprovalDate(new Date());
						getUserDAOBean().saveUserToDB(user);
						doLoging(ConstantsUserLog.MENU_APPROVAL, ConstantsUserLog.ACTIVITY_REJECT.concat(" ").concat(ConstantsUserLog.ACTIVITY_EDIT), "Reject Edit User : "+user.getUserName()+".");
					} else if (a.getAction().equals(
					Constants.APPROVAL_ACTION_CREATE)) {
						user = (User) HelperUtils.deserialize(a.getNewBlobObject().getBinaryStream());
						user = getUserDAOBean().getDetailedUser(
						user.getUserID());
						user.setStatus(Constants.APPROVAL_STATUS_REJECTED_CREATE);
						user.setApprovalBy(sessionBean.getUser().getUserName());
						user.setApprovalDate(new Date());
						getUserDAOBean().saveUserToDB(user);
						doLoging(ConstantsUserLog.MENU_APPROVAL, ConstantsUserLog.ACTIVITY_REJECT.concat(" ").concat(ConstantsUserLog.ACTIVITY_SAVE), "Reject Save User : "+user.getUserName()+".");
					}
					approvalDao.saveApprovalLog(a, Constants.APPROVAL_STATUS_CODE_SUCCESS, "Success");
					approvalDao.deleteApproval(a);
				}catch (Exception e) {
					// TODO Auto-generated catch block
					approvalDao.saveApprovalLog(a, Constants.APPROVAL_STATUS_CODE_FAILED, e.getMessage());
				}
			}
		}

	}
	
	public void rejectCheckedRole(List < Approval > checkedItemsRole){
		ApprovalDao approvalDao = (ApprovalDao) applicationBean.getCorpGWContext().getBean("approvalDAO");
		if (checkedItemsRole.size() != 0) {
			for (Approval a: checkedItemsRole) {
				Role role = null;
				try {

					if (a.getAction().equals(Constants.APPROVAL_ACTION_DELETE)) {
						role = (Role) HelperUtils.deserialize(a.getOldBlobObject().getBinaryStream());
						role = getRoleDAOBean().getDetailedRole(role);
						role.setStatus(Constants.APPROVAL_STATUS_REJECTED_DELETE);
						role.setApprovalBy(sessionBean.getUser().getUserName());
						role.setApprovalDate(new Date());
						getRoleDAOBean().saveOrUpdateRole(role);
						doLoging(ConstantsUserLog.MENU_APPROVAL, ConstantsUserLog.ACTIVITY_REJECT.concat(" ").concat(ConstantsUserLog.ACTIVITY_DELETE), "Reject Delete Role : "+role.getRoleName()+".");
					} else if (a.getAction()
						.equals(Constants.APPROVAL_ACTION_EDIT)) {
						role = (Role) HelperUtils.deserialize(a.getNewBlobObject().getBinaryStream());
						role = getRoleDAOBean().getDetailedRole(role);
						role.setStatus(Constants.APPROVAL_STATUS_REJECTED_EDIT);
						role.setApprovalBy(sessionBean.getUser().getUserName());
						role.setApprovalDate(new Date());
						getRoleDAOBean().saveOrUpdateRole(role);
						doLoging(ConstantsUserLog.MENU_APPROVAL, ConstantsUserLog.ACTIVITY_REJECT.concat(" ").concat(ConstantsUserLog.ACTIVITY_EDIT), "Reject Edit Role : "+role.getRoleName()+".");
					} else if (a.getAction().equals(
					Constants.APPROVAL_ACTION_CREATE)) {
						role = (Role) HelperUtils.deserialize(a.getNewBlobObject().getBinaryStream());
						role = getRoleDAOBean().getDetailedRole(role);
						role.setStatus(Constants.APPROVAL_STATUS_REJECTED_CREATE);
						role.setApprovalBy(sessionBean.getUser().getUserName());
						role.setApprovalDate(new Date());
						getRoleDAOBean().saveOrUpdateRole(role);
						doLoging(ConstantsUserLog.MENU_APPROVAL, ConstantsUserLog.ACTIVITY_REJECT.concat(" ").concat(ConstantsUserLog.ACTIVITY_SAVE), "Reject Create Role : "+role.getRoleName()+".");
					}
					approvalDao.saveApprovalLog(a, Constants.APPROVAL_STATUS_CODE_SUCCESS, "Success");
					approvalDao.deleteApproval(a);
				}catch (Exception e) {
					// TODO Auto-generated catch block
					approvalDao.saveApprovalLog(a, Constants.APPROVAL_STATUS_CODE_FAILED, e.getMessage());
				}
			}
		}
	}
	
	public void rejectCheckedScheduler(List < Approval > checkedItemsScheduler){
		ApprovalDao approvalDao = (ApprovalDao) applicationBean.getCorpGWContext().getBean("approvalDAO");
		if (checkedItemsScheduler.size() != 0) {
			for (Approval a: checkedItemsScheduler) {
				ScheduleTask task = null;
				try {

					if (a.getAction().equals(Constants.APPROVAL_ACTION_DELETE)) {
						task = (ScheduleTask)  HelperUtils.deserialize(a.getOldBlobObject().getBinaryStream());
						task = getSchedulerDAOBean().getDetailedTask(task.getSchedulerId());
						task.setApprovalStatus(Constants.APPROVAL_STATUS_REJECTED_DELETE);
						task.setApprovalBy(sessionBean.getUser().getUserName());
						task.setApprovalDate(new Date());
						getSchedulerDAOBean().saveTask(task);
					} else if (a.getAction()
						.equals(Constants.APPROVAL_ACTION_EDIT)) {
						task = (ScheduleTask)  HelperUtils.deserialize(a.getNewBlobObject().getBinaryStream());
						task = getSchedulerDAOBean().getDetailedTask(task.getSchedulerId());
						task.setApprovalStatus(Constants.APPROVAL_STATUS_REJECTED_EDIT);
						task.setApprovalBy(sessionBean.getUser().getUserName());
						task.setApprovalDate(new Date());
						getSchedulerDAOBean().saveTask(task);
					} else if (a.getAction().equals(
					Constants.APPROVAL_ACTION_CREATE)) {
						task = (ScheduleTask) HelperUtils.deserialize(a.getNewBlobObject().getBinaryStream());
						task = getSchedulerDAOBean().getDetailedTask(task.getSchedulerId());
						task.setApprovalStatus(Constants.APPROVAL_STATUS_REJECTED_CREATE);
						task.setApprovalBy(sessionBean.getUser().getUserName());
						task.setApprovalDate(new Date());
						getSchedulerDAOBean().saveTask(task);
					}
					approvalDao.saveApprovalLog(a, Constants.APPROVAL_STATUS_CODE_SUCCESS, "Success");
					approvalDao.deleteApproval(a);
				}catch (Exception e) {
					// TODO Auto-generated catch block
					approvalDao.saveApprovalLog(a, Constants.APPROVAL_STATUS_CODE_FAILED, e.getMessage());
				}
			}
		}
	}
	
	public void rejectCheckedSystemParameter(List < Approval > checkedItemsSystemParameter){
		ApprovalDao approvalDao = (ApprovalDao) applicationBean.getCorpGWContext().getBean("approvalDAO");
		if (checkedItemsSystemParameter.size() != 0) {
			for (Approval a: checkedItemsSystemParameter) {
				SystemParameter systemParameter = null;
				try {
					if (a.getAction().equals(Constants.APPROVAL_ACTION_DELETE)) {
						systemParameter = (SystemParameter)  HelperUtils.deserialize(a.getOldBlobObject().getBinaryStream());
						systemParameter = getSystemParameterDAOBean().getDetailedParameter(systemParameter.getParamId());
						systemParameter.setStatus(Constants.APPROVAL_STATUS_REJECTED_DELETE);
						systemParameter.setApprovalBy(sessionBean.getUser().getUserName());
						systemParameter.setApprovalDate(new Date());
						getSystemParameterDAOBean().saveSystemParameter(systemParameter);
						doLoging(ConstantsUserLog.MENU_APPROVAL, ConstantsUserLog.ACTIVITY_REJECT.concat(" ").concat(ConstantsUserLog.ACTIVITY_DELETE), "Reject Delete System Parameter : "+systemParameter.getParamName()+".");
					} else if (a.getAction()
						.equals(Constants.APPROVAL_ACTION_EDIT)) {
						systemParameter = (SystemParameter)  HelperUtils.deserialize(a.getNewBlobObject().getBinaryStream());
						systemParameter = getSystemParameterDAOBean().getDetailedParameter(systemParameter.getParamId());
						systemParameter.setStatus(Constants.APPROVAL_STATUS_REJECTED_EDIT);
						systemParameter.setApprovalBy(sessionBean.getUser().getUserName());
						systemParameter.setApprovalDate(new Date());
						getSystemParameterDAOBean().saveSystemParameter(systemParameter);
						doLoging(ConstantsUserLog.MENU_APPROVAL, ConstantsUserLog.ACTIVITY_REJECT.concat(" ").concat(ConstantsUserLog.ACTIVITY_SAVE), "Reject Create System Parameter : "+systemParameter.getParamName()+".");
					} else if (a.getAction().equals(
					Constants.APPROVAL_ACTION_CREATE)) {
						systemParameter = (SystemParameter) HelperUtils.deserialize(a.getNewBlobObject().getBinaryStream());
						systemParameter = getSystemParameterDAOBean().getDetailedParameter(systemParameter.getParamId());
						systemParameter.setStatus(Constants.APPROVAL_STATUS_REJECTED_DELETE);
						systemParameter.setApprovalBy(sessionBean.getUser().getUserName());
						systemParameter.setApprovalDate(new Date());
						getSystemParameterDAOBean().saveSystemParameter(systemParameter);
						doLoging(ConstantsUserLog.MENU_APPROVAL, ConstantsUserLog.ACTIVITY_REJECT.concat(" ").concat(ConstantsUserLog.ACTIVITY_EDIT), "Reject Edit System Parameter : "+systemParameter.getParamName()+".");
					}
					approvalDao.saveApprovalLog(a, Constants.APPROVAL_STATUS_CODE_SUCCESS, "Success");
					approvalDao.deleteApproval(a);
				}catch (Exception e) {
					// TODO Auto-generated catch block
					approvalDao.saveApprovalLog(a, Constants.APPROVAL_STATUS_CODE_FAILED, e.getMessage());
				}
			}
		}
	}
	
	public void rejectCheckedBranch(List < Approval > checkedItemsBranch){
		ApprovalDao approvalDao = (ApprovalDao) applicationBean.getCorpGWContext().getBean("approvalDAO");
		if (checkedItemsBranch.size() != 0) {
			for (Approval a: checkedItemsBranch) {
				Branch branch = null;
				try {
					if (a.getAction().equals(Constants.APPROVAL_ACTION_DELETE)) {
						branch = (Branch)  HelperUtils.deserialize(a.getOldBlobObject().getBinaryStream());
						branch = getBranchDAOBean().getDetailedBranch(branch.getBranchId());
						branch.setStatus(Constants.APPROVAL_STATUS_REJECTED_DELETE);
						branch.setApprovalBy(sessionBean.getUser().getUserName());
						branch.setApprovalDate(new Date());
						getBranchDAOBean().saveBranch(branch);
					} else if (a.getAction()
						.equals(Constants.APPROVAL_ACTION_EDIT)) {
						branch = (Branch)  HelperUtils.deserialize(a.getNewBlobObject().getBinaryStream());
						branch = getBranchDAOBean().getDetailedBranch(branch.getBranchId());
						branch.setStatus(Constants.APPROVAL_STATUS_REJECTED_EDIT);
						branch.setApprovalBy(sessionBean.getUser().getUserName());
						branch.setApprovalDate(new Date());
						getBranchDAOBean().saveBranch(branch);
					} else if (a.getAction().equals(
					Constants.APPROVAL_ACTION_CREATE)) {
						branch = (Branch) HelperUtils.deserialize(a.getNewBlobObject().getBinaryStream());
						branch = getBranchDAOBean().getDetailedBranch(branch.getBranchId());
						branch.setStatus(Constants.APPROVAL_STATUS_REJECTED_CREATE);
						branch.setApprovalBy(sessionBean.getUser().getUserName());
						branch.setApprovalDate(new Date());
						getBranchDAOBean().saveBranch(branch);
					}
					approvalDao.saveApprovalLog(a, Constants.APPROVAL_STATUS_CODE_SUCCESS, "Success");
					approvalDao.deleteApproval(a);
				}catch (Exception e) {
					// TODO Auto-generated catch block
					approvalDao.saveApprovalLog(a, Constants.APPROVAL_STATUS_CODE_FAILED, e.getMessage());
				}
			}
		}
	}
	
	public void rejectCheckedHoliday(List < Approval > checkedItemsHoliday){
		ApprovalDao approvalDao = (ApprovalDao) applicationBean.getCorpGWContext().getBean("approvalDAO");
		if (checkedItemsHoliday.size() != 0) {
			for (Approval a: checkedItemsHoliday) {
				Holiday holiday = null;
				try {
					if (a.getAction().equals(Constants.APPROVAL_ACTION_DELETE)) {
						holiday = (Holiday)  HelperUtils.deserialize(a.getOldBlobObject().getBinaryStream());
						holiday = getHolidayDAOBean().getDetailedHoliday(holiday.getHolidayId());
						holiday.setStatus(Constants.APPROVAL_STATUS_REJECTED_DELETE);
						holiday.setApprovalBy(sessionBean.getUser().getUserName());
						holiday.setApprovalDate(new Date());
						getHolidayDAOBean().saveHoliday(holiday);
					} else if (a.getAction()
						.equals(Constants.APPROVAL_ACTION_EDIT)) {
						holiday = (Holiday)  HelperUtils.deserialize(a.getNewBlobObject().getBinaryStream());
						holiday = getHolidayDAOBean().getDetailedHoliday(holiday.getHolidayId());
						holiday.setStatus(Constants.APPROVAL_STATUS_REJECTED_EDIT);
						holiday.setApprovalBy(sessionBean.getUser().getUserName());
						holiday.setApprovalDate(new Date());
						getHolidayDAOBean().saveHoliday(holiday);
					} else if (a.getAction().equals(
					Constants.APPROVAL_ACTION_CREATE)) {
						holiday = (Holiday) HelperUtils.deserialize(a.getNewBlobObject().getBinaryStream());
						holiday = getHolidayDAOBean().getDetailedHoliday(holiday.getHolidayId());
						holiday.setStatus(Constants.APPROVAL_STATUS_REJECTED_CREATE);
						holiday.setApprovalBy(sessionBean.getUser().getUserName());
						holiday.setApprovalDate(new Date());
						getHolidayDAOBean().saveHoliday(holiday);
					}
					approvalDao.saveApprovalLog(a, Constants.APPROVAL_STATUS_CODE_SUCCESS, "Success");
					approvalDao.deleteApproval(a);
				}catch (Exception e) {
					// TODO Auto-generated catch block
					approvalDao.saveApprovalLog(a, Constants.APPROVAL_STATUS_CODE_FAILED, e.getMessage());
				}
			}
		}
	}
	
	public void rejectAll() {
		
		ApprovalDao approvalDao = (ApprovalDao) applicationBean.getCorpGWContext().getBean("approvalDAO");
		List < Approval > approvals = new ArrayList < Approval > ();
		
		String approverId = sessionBean.getUser().getUserID();
		String branchId = sessionBean.getUser().getBranch();
		
		approvals = approvalDao.getApprovalByParameterSearch(approverId, parameterId, parameterName, searchParameter, searchAction, branchId);

		rejectAllUser(approvals);
		
		rejectAllRole(approvals);
		
		rejectAllScheduler(approvals);
		
		rejectAllSystemParameter(approvals);
		
		rejectAllBranch(approvals);
		
		rejectAllHoliday(approvals);
		
		String headerMessage = "Success";
		String bodyMessage = "Reject all finished";

		FacesUtil.setInfoMessageDialog(headerMessage, bodyMessage);
	}

	public void rejectAllUser(List<Approval> approvals){
		ApprovalDao approvalDao = (ApprovalDao) applicationBean.getCorpGWContext().getBean("approvalDAO");
		
		if (approvals.size() != 0) {
			for (Approval a: approvals) {
				if (a.getParameter().equals(Constants.APPROVAL_PARAMETER_USER)){
					User user = null;
					try {
	
						if (a.getAction().equals(Constants.APPROVAL_ACTION_DELETE)) {
							user = (User) HelperUtils.deserialize(a.getOldBlobObject().getBinaryStream());
							user = getUserDAOBean().getDetailedUser(
							user.getUserID());
							user.setStatus(Constants.APPROVAL_STATUS_REJECTED_DELETE);
							user.setApprovalBy(sessionBean.getUser().getUserName());
							user.setApprovalDate(new Date());
							getUserDAOBean().saveUserToDB(user);
							doLoging(ConstantsUserLog.MENU_APPROVAL, ConstantsUserLog.ACTIVITY_REJECT.concat(" ").concat(ConstantsUserLog.ACTIVITY_DELETE), "Reject Delete User : "+user.getUserName()+".");
						} else if (a.getAction()
							.equals(Constants.APPROVAL_ACTION_EDIT)) {
							user = (User) HelperUtils.deserialize(a.getNewBlobObject().getBinaryStream());
							user = getUserDAOBean().getDetailedUser(
							user.getUserID());
							user.setStatus(Constants.APPROVAL_STATUS_REJECTED_EDIT);
							user.setApprovalBy(sessionBean.getUser().getUserName());
							user.setApprovalDate(new Date());
							getUserDAOBean().saveUserToDB(user);
							doLoging(ConstantsUserLog.MENU_APPROVAL, ConstantsUserLog.ACTIVITY_REJECT.concat(" ").concat(ConstantsUserLog.ACTIVITY_EDIT), "Reject Edit User : "+user.getUserName()+".");
						} else if (a.getAction().equals(
						Constants.APPROVAL_ACTION_CREATE)) {
							user = (User) HelperUtils.deserialize(a.getNewBlobObject().getBinaryStream());
							user = getUserDAOBean().getDetailedUser(
							user.getUserID());
							user.setStatus(Constants.APPROVAL_STATUS_REJECTED_CREATE);
							user.setApprovalBy(sessionBean.getUser().getUserName());
							user.setApprovalDate(new Date());
							getUserDAOBean().saveUserToDB(user);
							doLoging(ConstantsUserLog.MENU_APPROVAL, ConstantsUserLog.ACTIVITY_REJECT.concat(" ").concat(ConstantsUserLog.ACTIVITY_SAVE), "Reject Save User : "+user.getUserName()+".");
						}
						approvalDao.saveApprovalLog(a, Constants.APPROVAL_STATUS_CODE_SUCCESS, "Success");
						approvalDao.deleteApproval(a);
					}catch (Exception e) {
						// TODO Auto-generated catch block
						approvalDao.saveApprovalLog(a, Constants.APPROVAL_STATUS_CODE_FAILED, e.getMessage());
					}
				}
			}
		}
	}

	public void rejectAllRole(List<Approval> approvals){
		
		ApprovalDao approvalDao = (ApprovalDao) applicationBean.getCorpGWContext().getBean("approvalDAO");

		if (approvals.size() != 0) {
			for (Approval a: approvals) {
				if (a.getParameter().equals(Constants.APPROVAL_PARAMETER_ROLE)){
					Role role = null;
					try {
						if (a.getAction().equals(Constants.APPROVAL_ACTION_DELETE)) {
							role = (Role) HelperUtils.deserialize(a.getOldBlobObject().getBinaryStream());
							role = getRoleDAOBean().getDetailedRole(role);
							role.setStatus(Constants.APPROVAL_STATUS_REJECTED_DELETE);
							role.setApprovalBy(sessionBean.getUser().getUserName());
							role.setApprovalDate(new Date());
							getRoleDAOBean().saveOrUpdateRole(role);
							doLoging(ConstantsUserLog.MENU_APPROVAL, ConstantsUserLog.ACTIVITY_REJECT.concat(" ").concat(ConstantsUserLog.ACTIVITY_DELETE), "Reject Delete Role : "+role.getRoleName()+".");
						} else if (a.getAction()
							.equals(Constants.APPROVAL_ACTION_EDIT)) {
							role = (Role) HelperUtils.deserialize(a.getNewBlobObject().getBinaryStream());
							role = getRoleDAOBean().getDetailedRole(role);
							role.setStatus(Constants.APPROVAL_STATUS_REJECTED_EDIT);
							role.setApprovalBy(sessionBean.getUser().getUserName());
							role.setApprovalDate(new Date());
							getRoleDAOBean().saveOrUpdateRole(role);
							doLoging(ConstantsUserLog.MENU_APPROVAL, ConstantsUserLog.ACTIVITY_REJECT.concat(" ").concat(ConstantsUserLog.ACTIVITY_EDIT), "Reject Edit Role : "+role.getRoleName()+".");
						} else if (a.getAction().equals(
						Constants.APPROVAL_ACTION_CREATE)) {
							role = (Role) HelperUtils.deserialize(a.getNewBlobObject().getBinaryStream());
							role = getRoleDAOBean().getDetailedRole(role);
							role.setStatus(Constants.APPROVAL_STATUS_REJECTED_CREATE);
							role.setApprovalBy(sessionBean.getUser().getUserName());
							role.setApprovalDate(new Date());
							getRoleDAOBean().saveOrUpdateRole(role);
							doLoging(ConstantsUserLog.MENU_APPROVAL, ConstantsUserLog.ACTIVITY_REJECT.concat(" ").concat(ConstantsUserLog.ACTIVITY_SAVE), "Reject Create Role : "+role.getRoleName()+".");
						}
						approvalDao.saveApprovalLog(a, Constants.APPROVAL_STATUS_CODE_SUCCESS, "Success");
						approvalDao.deleteApproval(a);
					}catch (Exception e) {
						// TODO Auto-generated catch block
						approvalDao.saveApprovalLog(a, Constants.APPROVAL_STATUS_CODE_FAILED, e.getMessage());
					}
				}
			}
		}
	}

	public void rejectAllScheduler(List<Approval> approvals){
		
		ApprovalDao approvalDao = (ApprovalDao) applicationBean.getCorpGWContext().getBean("approvalDAO");

		if (approvals.size() != 0) {
			for (Approval a: approvals) {
				if (a.getParameter().equals(Constants.APPROVAL_PARAMETER_SCHEDULER)){
					ScheduleTask task = null;
					try {
						if (a.getAction().equals(Constants.APPROVAL_ACTION_DELETE)) {
							task = (ScheduleTask) HelperUtils.deserialize(a.getOldBlobObject()
								.getBinaryStream());
							task = getSchedulerDAOBean().getDetailedTask(
							task.getSchedulerId());
							task.setApprovalStatus(Constants.APPROVAL_STATUS_REJECTED_DELETE);
							task.setApprovalBy(sessionBean.getUser().getUserName());
							task.setApprovalDate(new Date());
							getSchedulerDAOBean().saveTask(task);
						} else if (a.getAction()
							.equals(Constants.APPROVAL_ACTION_EDIT)) {
							task = (ScheduleTask) HelperUtils.deserialize(a.getNewBlobObject()
								.getBinaryStream());
							task = getSchedulerDAOBean().getDetailedTask(
							task.getSchedulerId());
							task.setApprovalStatus(Constants.APPROVAL_STATUS_REJECTED_EDIT);
							task.setApprovalBy(sessionBean.getUser().getUserName());
							task.setApprovalDate(new Date());
							getSchedulerDAOBean().saveTask(task);
						} else if (a.getAction().equals(
						Constants.APPROVAL_ACTION_CREATE)) {
							task = (ScheduleTask) HelperUtils.deserialize(a.getNewBlobObject()
								.getBinaryStream());
							task = getSchedulerDAOBean().getDetailedTask(
							task.getSchedulerId());
							task.setApprovalStatus(Constants.APPROVAL_STATUS_REJECTED_CREATE);
							task.setApprovalBy(sessionBean.getUser().getUserName());
							task.setApprovalDate(new Date());
							getSchedulerDAOBean().saveTask(task);
						}
						approvalDao.saveApprovalLog(a, Constants.APPROVAL_STATUS_CODE_SUCCESS, "Success");
						approvalDao.deleteApproval(a);
					}catch (Exception e) {
						// TODO Auto-generated catch block
						approvalDao.saveApprovalLog(a, Constants.APPROVAL_STATUS_CODE_FAILED, e.getMessage());
					}
				}
			}
		}
	}

	public void rejectAllSystemParameter(List<Approval> approvals){
		
		ApprovalDao approvalDao = (ApprovalDao) applicationBean.getCorpGWContext().getBean("approvalDAO");

		if (approvals.size() != 0){
			for (Approval a : approvals){
				if (a.getParameter().equals(Constants.APPROVAL_PARAMETER_SPARAM)){
					SystemParameter systemParameter = null;
					try {
						if (a.getAction().equals(Constants.APPROVAL_ACTION_DELETE)){
							systemParameter = (SystemParameter) HelperUtils.deserialize(a.getOldBlobObject().getBinaryStream());
							systemParameter.setStatus(Constants.APPROVAL_STATUS_REJECTED_DELETE);
							systemParameter.setApprovalBy(sessionBean.getUser().getUserName());
							systemParameter.setApprovalDate(new Date());
							getSystemParameterDAOBean().saveSystemParameter(systemParameter);
							doLoging(ConstantsUserLog.MENU_APPROVAL, ConstantsUserLog.ACTIVITY_REJECT.concat(" ").concat(ConstantsUserLog.ACTIVITY_DELETE), "Reject Delete System Parameter : "+systemParameter.getParamName()+".");
						}else if (a.getAction().equals(Constants.APPROVAL_ACTION_CREATE)){
							systemParameter = (SystemParameter) HelperUtils.deserialize(a.getNewBlobObject().getBinaryStream());
							systemParameter.setStatus(Constants.APPROVAL_STATUS_REJECTED_CREATE);
							systemParameter.setApprovalBy(sessionBean.getUser().getUserName());
							systemParameter.setApprovalDate(new Date());
							getSystemParameterDAOBean().saveSystemParameter(systemParameter);
							doLoging(ConstantsUserLog.MENU_APPROVAL, ConstantsUserLog.ACTIVITY_REJECT.concat(" ").concat(ConstantsUserLog.ACTIVITY_SAVE), "Reject Create System Parameter : "+systemParameter.getParamName()+".");
						}else if (a.getAction().equals(Constants.APPROVAL_ACTION_EDIT)){
							systemParameter = (SystemParameter) HelperUtils.deserialize(a.getOldBlobObject().getBinaryStream());
							systemParameter.setStatus(Constants.APPROVAL_STATUS_REJECTED_EDIT);
							systemParameter.setApprovalBy(sessionBean.getUser().getUserName());
							systemParameter.setApprovalDate(new Date());
							getSystemParameterDAOBean().saveSystemParameter(systemParameter);
							doLoging(ConstantsUserLog.MENU_APPROVAL, ConstantsUserLog.ACTIVITY_REJECT.concat(" ").concat(ConstantsUserLog.ACTIVITY_EDIT), "Reject Edit System Parameter : "+systemParameter.getParamName()+".");
						}
						approvalDao.saveApprovalLog(a, Constants.APPROVAL_STATUS_CODE_SUCCESS, "Success");
						approvalDao.deleteApproval(a);
					}catch (Exception e) {
						// TODO Auto-generated catch block
						approvalDao.saveApprovalLog(a, Constants.APPROVAL_STATUS_CODE_FAILED, e.getMessage());
					}
				}
			}
		}
	}

	public void rejectAllBranch(List<Approval> approvals){
		
		ApprovalDao approvalDao = (ApprovalDao) applicationBean.getCorpGWContext().getBean("approvalDAO");

		if (approvals.size() != 0){
			for (Approval a : approvals){
				if (a.getParameter().equals(Constants.APPROVAL_PARAMETER_BRANCH)){
					Branch branch = null;
					try {
						if (a.getAction().equals(Constants.APPROVAL_ACTION_DELETE)){
							branch = (Branch) HelperUtils.deserialize(a.getOldBlobObject().getBinaryStream());
							branch.setStatus(Constants.APPROVAL_STATUS_REJECTED_DELETE);
							branch.setApprovalBy(sessionBean.getUser().getUserName());
							branch.setApprovalDate(new Date());
							getBranchDAOBean().saveBranch(branch);
						}else if (a.getAction().equals(Constants.APPROVAL_ACTION_CREATE)){
							branch = (Branch) HelperUtils.deserialize(a.getNewBlobObject().getBinaryStream());
							branch.setStatus(Constants.APPROVAL_STATUS_REJECTED_CREATE);
							branch.setApprovalBy(sessionBean.getUser().getUserName());
							branch.setApprovalDate(new Date());
							getBranchDAOBean().saveBranch(branch);
						}else if (a.getAction().equals(Constants.APPROVAL_ACTION_EDIT)){
							branch = (Branch) HelperUtils.deserialize(a.getOldBlobObject().getBinaryStream());
							branch.setStatus(Constants.APPROVAL_STATUS_REJECTED_EDIT);
							branch.setApprovalBy(sessionBean.getUser().getUserName());
							branch.setApprovalDate(new Date());
							getBranchDAOBean().saveBranch(branch);
						}
						approvalDao.saveApprovalLog(a, Constants.APPROVAL_STATUS_CODE_SUCCESS, "Success");
						approvalDao.deleteApproval(a);
					}catch (Exception e) {
						// TODO Auto-generated catch block
						approvalDao.saveApprovalLog(a, Constants.APPROVAL_STATUS_CODE_FAILED, e.getMessage());
					}
				}
			}
		}
	}

	public void rejectAllHoliday(List<Approval> approvals){
		
		ApprovalDao approvalDao = (ApprovalDao) applicationBean.getCorpGWContext().getBean("approvalDAO");

		if (approvals.size() != 0){
			for (Approval a : approvals){
				if (a.getParameter().equals(Constants.APPROVAL_PARAMETER_HOLIDAY)){
					Holiday holiday = null;
					try {
						if (a.getAction().equals(Constants.APPROVAL_ACTION_DELETE)){
							holiday = (Holiday) HelperUtils.deserialize(a.getOldBlobObject().getBinaryStream());
							holiday.setStatus(Constants.APPROVAL_STATUS_REJECTED_DELETE);
							holiday.setApprovalBy(sessionBean.getUser().getUserName());
							holiday.setApprovalDate(new Date());
							getHolidayDAOBean().saveHoliday(holiday);
						}else if (a.getAction().equals(Constants.APPROVAL_ACTION_CREATE)){
							holiday = (Holiday) HelperUtils.deserialize(a.getNewBlobObject().getBinaryStream());
							holiday.setStatus(Constants.APPROVAL_STATUS_REJECTED_CREATE);
							holiday.setApprovalBy(sessionBean.getUser().getUserName());
							holiday.setApprovalDate(new Date());
							getHolidayDAOBean().saveHoliday(holiday);
						}else if (a.getAction().equals(Constants.APPROVAL_ACTION_EDIT)){
							holiday = (Holiday) HelperUtils.deserialize(a.getOldBlobObject().getBinaryStream());
							holiday.setStatus(Constants.APPROVAL_STATUS_REJECTED_EDIT);
							holiday.setApprovalBy(sessionBean.getUser().getUserName());
							holiday.setApprovalDate(new Date());
							getHolidayDAOBean().saveHoliday(holiday);
						}
						approvalDao.saveApprovalLog(a, Constants.APPROVAL_STATUS_CODE_SUCCESS, "Success");
						approvalDao.deleteApproval(a);
					}catch (Exception e) {
						// TODO Auto-generated catch block
						approvalDao.saveApprovalLog(a, Constants.APPROVAL_STATUS_CODE_FAILED, e.getMessage());
					}
				}
			}
		}
	}
	
	public void processChecked(Approval approval) {
		Approval a = new Approval();
		a = (Approval) SerializationUtils.clone(approval);
		if (approval.isSelected()) {
			approvalTemps.put(approval.getApprovalId(), approval);
		} else {
			approvalTemps.remove(approval.getApprovalId());
		}

	}

	private class ApprovalDataModel extends LazyDataModel < Approval > {
		private static final long serialVersionUID = 1L;

		public ApprovalDataModel() {

		}

		@Override
		public List < Approval > load(int startingAt, int maxPerPage,
		String sortField, SortOrder sortOrder,
		Map < String, String > filters) {
			
			ApprovalDao approvalDao = (ApprovalDao) applicationBean.getCorpGWContext().getBean("approvalDAO");
			
			List < Approval > approvals;
			
			List<String> userIdLogins =  new ArrayList<String>();
			for(User user : sessionBean.getAllConnectedUser() ){
				if (!user.getUserID().equals(sessionBean.getUser().getUserID()))
					userIdLogins.add(user.getUserID());
			}
			if (userIdLogins.size() > 0){
				getApprovalDAOBean().removeApprover(userIdLogins);
			}else{
				getApprovalDAOBean().removeAllApprover();
			}
			getApprovalDAOBean().assignApprover(sessionBean.getUser().getUserID());
			
			String approverId = sessionBean.getUser().getUserID();
			String branchId = sessionBean.getUser().getBranch();
			
			approvals = approvalDao.getAllApprovalLazyLoading(maxPerPage + startingAt, startingAt, approverId, parameterId, parameterName, searchParameter, searchAction, branchId);
			Integer jmlAll = approvalDao.countAllApproval(approverId, parameterId, parameterName, searchParameter, searchAction, branchId);
			setRowCount(jmlAll);
			setPageSize(maxPerPage);

			List < Approval > approvalsFinal = new ArrayList < Approval > ();
			for (Approval a: approvals) {
				Iterator entries = approvalTemps.entrySet().iterator();
				while (entries.hasNext()) {
					Entry thisEntry = (Entry) entries.next();
					Approval s = (Approval) thisEntry.getValue();
					if (a.getApprovalId().equals(s.getApprovalId())) {
						if (s.isSelected()) {
							a.setSelected(true);
						} else {
							a.setSelected(false);
						}
					}
				}
				approvalsFinal.add(a);
			}

			return approvalsFinal;
		}
	}
	
	public void sendEmailToUser(String Username, String password, String email){
		EmailHelper.newUserMail(Username, password, email, Constants.EMAILAPPROVED, getSystemParameterDAOBean());
	}
	
	private void doLoging(String menu, String activity, String information) {
		UserLog userLog = new UserLog();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		userLog.setLogId("LOG-"+sdf.format(new Date())+"-"+sessionBean.getUser().getUserID());
		userLog.setActivity(activity);
		userLog.setInformation(information);
		userLog.setInsertTime(new Date());
		userLog.setMenu(menu);
		userLog.setUserId(sessionBean.getUser().getUserID());
		userLog.setUserName(sessionBean.getUser().getUserName());
		UserLogHandler.doLog(userLog, getUserDAOBean());
	}
}