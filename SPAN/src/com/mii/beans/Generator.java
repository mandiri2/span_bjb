package com.mii.beans;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Generator  {
	static Date date = generateDate();
	static SimpleDateFormat creationString = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	static SimpleDateFormat xmlString = new SimpleDateFormat("yyyyMMdd_HHmmss_SSS");
	static SimpleDateFormat docDateString = new SimpleDateFormat("yyyy-MM-dd");
	static SimpleDateFormat sp2dnoString = new SimpleDateFormat("yyyyMMddHHmmss");
	static int iterate = 10;
	static String creationDateTime = creationString.format(date)  ;//"2016-11-07 15:19:12";
	static String xmlName = "524110000990_SP2D_O_"+xmlString.format(date)+".xml";
	static String docDate = docDateString.format(date);//"2016-11-07";
	static String sp2dno = sp2dnoString.format(date)+"0";
	static long amount = 1;
	
	public static void main(String[] args) {
		//HEADER
		System.out.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		System.out.println("<SP2D>");
		System.out.println("<ApplicationArea>");
		System.out.println("<ApplicationAreaSenderIdentifier>SPAN</ApplicationAreaSenderIdentifier>");
		System.out.println("<ApplicationAreaReceiverIdentifier>BANK BJB</ApplicationAreaReceiverIdentifier>");
		System.out.println("<ApplicationAreaDetailSenderIdentifier/>");
		System.out.println("<ApplicationAreaDetailReceiverIdentifier/>");
		System.out.println("<ApplicationAreaCreationDateTime>"+creationDateTime+"</ApplicationAreaCreationDateTime>");
		System.out.println("<ApplicationAreaMessageIdentifier>"+xmlName+"</ApplicationAreaMessageIdentifier>");
		System.out.println("<ApplicationAreaMessageTypeIndicator>SP2D</ApplicationAreaMessageTypeIndicator>");
		System.out.println("<ApplicationAreaMessageVersionText>1.0</ApplicationAreaMessageVersionText>");
		System.out.println("</ApplicationArea>");
		//CONTENT
		for(int i = 1; i<=iterate; i++){
			System.out.println("<DataArea>");
			System.out.println("<DocumentDate>"+docDate+"</DocumentDate>");
			System.out.println("<DocumentNumber>"+sp2dno+String.format("%06d", i)+"</DocumentNumber>");
			System.out.println("<BeneficiaryName>M RIZCKY ANANDA PUTRANTO</BeneficiaryName>");
			System.out.println("<BeneficiaryBankCode>524110000990</BeneficiaryBankCode>");
			System.out.println("<BeneficiaryBank>BANK BJB</BeneficiaryBank>");
			System.out.println("<BeneficiaryAccount>0026643155100</BeneficiaryAccount>");
			System.out.println("<Amount>"+amount+"</Amount>");
			System.out.println("<CurrencyTarget>IDR</CurrencyTarget>");
			System.out.println("<Description>GAJI/1000296988/000000 "+i+"</Description>");
			System.out.println("<AgentBankCode>524110000990</AgentBankCode>");
			System.out.println("<AgentBankAccountNumber>0011120171302</AgentBankAccountNumber>");
			System.out.println("<AgentBankAccountName>RPKBUNP GAJI BPD JABAR BANTEN</AgentBankAccountName>");
			System.out.println("<EmailAddress/>");
			System.out.println("<SwiftCode>-</SwiftCode>");
			System.out.println("<IBANCode>-</IBANCode>");
			System.out.println("<PaymentMethod>0</PaymentMethod>");
			System.out.println("<SP2DCount>"+iterate+"</SP2DCount>");
			System.out.println("</DataArea>");
		}
		//CONTENT SKN/RTGS
		/*for(int i = 1; i<=iterate; i++){
			System.out.println("<DataArea>");
			System.out.println("<DocumentDate>"+docDate+"</DocumentDate>");
			System.out.println("<DocumentNumber>"+sp2dno+String.format("%06d", i)+"</DocumentNumber>");
			System.out.println("<BeneficiaryName>BHARA MARTILA</BeneficiaryName>");
			System.out.println("<BeneficiaryBankCode>523213000990</BeneficiaryBankCode>");
			System.out.println("<BeneficiaryBank>BANK TABUNGAN PENSIUNAN NASIONAL</BeneficiaryBank>");
			System.out.println("<BeneficiaryAccount>00162011944</BeneficiaryAccount>");
			System.out.println("<Amount>"+amount+"</Amount>");
			System.out.println("<CurrencyTarget>IDR</CurrencyTarget>");
			System.out.println("<Description>GAJI/1000296988/000000 "+i+"</Description>");
			System.out.println("<AgentBankCode>524110000990</AgentBankCode>");
			System.out.println("<AgentBankAccountNumber>0072932706001</AgentBankAccountNumber>");
			System.out.println("<AgentBankAccountName>RPKBUN P GAJI BANK BJB</AgentBankAccountName>");
			System.out.println("<EmailAddress/>");
			System.out.println("<SwiftCode>-</SwiftCode>");
			System.out.println("<IBANCode>-</IBANCode>");
			System.out.println("<PaymentMethod>2</PaymentMethod>");
			System.out.println("<SP2DCount>"+iterate+"</SP2DCount>");
			System.out.println("</DataArea>");
		}*/
		//FOR RR
		/*for(int i = 1; i<=iterate; i++){
			System.out.println("<DataArea>");
			System.out.println("<DocumentDate>"+docDate+"</DocumentDate>");
			System.out.println("<DocumentNumber>"+sp2dno+String.format("%06d", i)+"</DocumentNumber>");
			System.out.println("<BeneficiaryName>GEMA PUTRI SUKMAJAYA</BeneficiaryName>");
			System.out.println("<BeneficiaryBankCode>524110000990</BeneficiaryBankCode>");
			System.out.println("<BeneficiaryBank>BANK BJB</BeneficiaryBank>");
			System.out.println("<BeneficiaryAccount>0072932684100</BeneficiaryAccount>");
			System.out.println("<Amount>"+amount+"</Amount>");
			System.out.println("<CurrencyTarget>IDR</CurrencyTarget>");
			System.out.println("<Description>GAJI/1000296988/000000 "+i+"</Description>");
			System.out.println("<AgentBankCode>524110000990</AgentBankCode>");
			System.out.println("<AgentBankAccountNumber>0072932714001</AgentBankAccountNumber>");
			System.out.println("<AgentBankAccountName>RR RPKBUN P GAJI BANK BJB</AgentBankAccountName>");
			System.out.println("<EmailAddress/>");
			System.out.println("<SwiftCode>-</SwiftCode>");
			System.out.println("<IBANCode>-</IBANCode>");
			System.out.println("<PaymentMethod>0</PaymentMethod>");
			System.out.println("<SP2DCount>"+iterate+"</SP2DCount>");
			System.out.println("</DataArea>");
		}*/
		//FOOTER
		System.out.println("<Footer>");
		System.out.println("<TotalCount>"+iterate+"</TotalCount>");
		System.out.println("<TotalAmount>"+iterate*amount+"</TotalAmount>");
		System.out.println("<TotalBatchCount>"+iterate+"</TotalBatchCount>");
		System.out.println("</Footer>");
		System.out.println("</SP2D>");
	}

	private static Date generateDate() {
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
//		cal.add(Calendar.DATE, -1);
		return cal.getTime();
	}
}
