package com.mii.beans;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.servlet.http.HttpSession;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.UploadedFile;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Rectangle;
import com.mii.constant.Constants;
import com.mii.dao.SpanUploadSp2dDao;
import com.mii.dao.SystemParameterDao;
import com.mii.dao.TransactionReportDaoImpl;
import com.mii.helpers.CreateJarFile;
import com.mii.helpers.FacesUtil;
import com.mii.helpers.HelperUtils;
import com.mii.is.util.PubUtil;
import com.mii.models.MonitoringManualUploadSp2d;
import com.mii.models.SpanUploadSp2d;
import com.mii.models.SystemParameter;
import com.mii.models.User;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.SessionBean;

@ManagedBean
@ViewScoped
public class MonitoringManualUploadSp2dHandler implements Serializable{
	private static final long serialVersionUID = 1L;

	private LazyDataModel < MonitoringManualUploadSp2d > detailList;
	private List<MonitoringManualUploadSp2d> detailListHidden;

	@ManagedProperty(value="#{applicationBean}")
	private ApplicationBean applicationBean;
	@ManagedProperty(value="#{sessionBean}")
	private SessionBean sessionBean;

	private boolean doShowSearch;
	private boolean doUploadShow;

	private Date startUploadDate;
	private Date endUploadDate;

	private int jmlAll;

	private UploadedFile file;
	private String errorUploadMessage;
	private Boolean showUploadMessage;

	@PostConstruct
	public void init(){
		FacesUtil.authMenu(sessionBean.getUser(), "monitoringManualUploadSp2d");
		loadData();
		setShowUploadMessage(false);
	}

	public void showSearch(){
		doShowSearch = true;
	}

	public void hideSearch(){
		doShowSearch = false;
	}

	public void showUpload(){
		setDoUploadShow(true);
		setShowUploadMessage(false);
	}

	public void hideUpload(){
		setDoUploadShow(false);
	}

	public void doSearch(){
		loadData();
	}

	public void clearSearch(){
		startUploadDate = null;
		endUploadDate = null;
	}

	private void loadData() {
		detailList = new DetailDataModel();

	}

	public void preProcessPDF(Object document) throws IOException,  
	BadElementException, DocumentException{
		Document pdf = (Document) document;
		Rectangle r = new Rectangle(2000f, 1000f);
		pdf.setPageSize(r);
		pdf.setMargins(0f, 0f, 100f, 100f);
	}

	private class DetailDataModel extends LazyDataModel < MonitoringManualUploadSp2d > {
		private static final long serialVersionUID = 1L;

		public DetailDataModel() {
		}

		@Override
		public List < MonitoringManualUploadSp2d > load(int startingAt, int maxPerPage,
				String sortField, SortOrder sortOrder,
				Map < String, String > filters) {

			List<MonitoringManualUploadSp2d> detailList = new ArrayList<MonitoringManualUploadSp2d>();
			try{
				jmlAll = getSpanUploadSp2dDaoBean().countGetUploadFile(validateDate(startUploadDate), validateDate(endUploadDate));
				detailList = getSpanUploadSp2dDaoBean().getUploadFile(maxPerPage+startingAt, startingAt, validateDate(startUploadDate), validateDate(endUploadDate));
			}catch(Exception e){
				e.printStackTrace();
			}
			setRowCount(jmlAll);
			setPageSize(maxPerPage);
			return detailList;
		}

		private SpanUploadSp2dDao getSpanUploadSp2dDaoBean() {
			return (SpanUploadSp2dDao) applicationBean.getCorpGWContext().getBean("spanUploadSp2dDao");
		}
	}

	public String validateDate(Date date){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try{
			return sdf.format(date);
		}catch(Exception e){
			return sdf.format(new Date());
		}
	}

	public void downloadPdf() {
		String fileName = "Monitoring_Manual_Upload_SP2D.pdf";
		String jasperPath = FacesUtil.getExternalContext().getRealPath("resources/report/Monitoring_Manual_Upload_Sp2d.jasper");

		detailListHidden = getSpanUploadSp2dDaoBean().getUploadFile(jmlAll, 0,  validateDate(startUploadDate), validateDate(endUploadDate));

		PubUtil.generateReport(fileName, jasperPath, Constants.PDF, null, Constants.CONTENT_TYPE_PDF,detailListHidden);
	}

	public void downloadXls() {
		String fileName = "Monitoring_Manual_Upload_SP2D.xls";
		String jasperPath = FacesUtil.getExternalContext().getRealPath("resources/report/Monitoring_Manual_Upload_Sp2d_XLS.jasper");

		detailListHidden = getSpanUploadSp2dDaoBean().getUploadFile(jmlAll, 0,  validateDate(startUploadDate), validateDate(endUploadDate));


		PubUtil.generateReport(fileName, jasperPath, Constants.EXCEL, null, Constants.CONTENT_TYPE_XLS,detailListHidden);
	}



	public void upload() {
		try{
			if(getFile() != null) {
				String extension = getFile().getFileName().substring(getFile().getFileName().lastIndexOf("."));
				if(extension.equalsIgnoreCase(".xml")||extension.equalsIgnoreCase(".jar")){
					int count = getSpanUploadSp2dDaoBean().countFileName(getFile().getFileName());
					int count1 = getSpanUploadSp2dDaoBean().countSpanValidationFileName(getFile().getFileName());
					if(count > 0 || count1 > 0){
						errorUploadMessage = "File allready exist in the system.";
						setShowUploadMessage(true);
					}else{
						System.out.println("Upload File SP2D");
						SystemParameter paramLocalPath = getSystemParameterDAOBean().getDetailedParameterByParamName(Constants.SPAN_DEPKEU_MANUAL_PUT_LOCAL_DIR);
						try {
							BufferedInputStream input = new BufferedInputStream(this.getFile().getInputstream());
							String fileName = getFile().getFileName();
							OutputStream outputStream = null;
							outputStream = new FileOutputStream(new File(paramLocalPath.getParamValue().concat(fileName)));
							int read = 0;
							byte[] bytes = new byte[1024];

							while ((read = input.read(bytes)) != -1) {
								outputStream.write(bytes, 0, read);
							}
							outputStream.close();
							//Create Jar File
							if(extension.equalsIgnoreCase(".xml")){
								CreateJarFile.runCreateJar(paramLocalPath.getParamValue().concat(getFile().getFileName()), 
										paramLocalPath.getParamValue().concat(getFile().getFileName().replace(".xml", ".jar")));
								HelperUtils.deleteFile(paramLocalPath.getParamValue().concat(getFile().getFileName()));
							}
							System.out.println("Upload Done");
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
							Date date = new Date();
							String userId = sessionBean.getUser().getUserID();
							SpanUploadSp2d spanUploadSp2d = new SpanUploadSp2d();
							spanUploadSp2d.setFile_name(getFile().getFileName());
							spanUploadSp2d.setUpload_time(date);
							spanUploadSp2d.setInsert_date(sdf.format(date));
							spanUploadSp2d.setUpload_by(userId);
							getSpanUploadSp2dDaoBean().save(spanUploadSp2d);
							loadData();
							errorUploadMessage = "Success to Upload File.";
							setShowUploadMessage(true);
						} catch (IOException e) {
							System.out.println("Upload Error");
							String headerMessage = "Error";
							String bodyMessage = "Failed to Upload File. IO Exception. Check your connection.";
							FacesUtil.setErrorMessageDialog(headerMessage, bodyMessage);
							e.printStackTrace();
						}
					}
				}else{
					errorUploadMessage = "Failed to Upload File. Wrong format File. XML or JAR only.";
					setShowUploadMessage(true);
				}
			}else{
				errorUploadMessage = "Please select file first.";
				setShowUploadMessage(true);
			}
		}catch(Exception e){
			e.printStackTrace();
			errorUploadMessage = "Please select file first.";
			setShowUploadMessage(true);
		}
	}

	private SystemParameterDao getSystemParameterDAOBean(){
		return (SystemParameterDao) applicationBean.getCorpGWContext().getBean("systemParameterDAO");
	}

	private SpanUploadSp2dDao getSpanUploadSp2dDaoBean(){
		return (SpanUploadSp2dDao) applicationBean.getCorpGWContext().getBean("spanUploadSp2dDao");
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public LazyDataModel<MonitoringManualUploadSp2d> getDetailList() {
		return detailList;
	}

	public void setDetailList(LazyDataModel<MonitoringManualUploadSp2d> detailList) {
		this.detailList = detailList;
	}

	public ApplicationBean getApplicationBean() {
		return applicationBean;
	}

	public void setApplicationBean(ApplicationBean applicationBean) {
		this.applicationBean = applicationBean;
	}

	public boolean isDoShowSearch() {
		return doShowSearch;
	}

	public void setDoShowSearch(boolean doShowSearch) {
		this.doShowSearch = doShowSearch;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public boolean isDoUploadShow() {
		return doUploadShow;
	}

	public void setDoUploadShow(boolean doUploadShow) {
		this.doUploadShow = doUploadShow;
	}

	public UploadedFile getFile() {
		return file;
	}

	public void setFile(UploadedFile file) {
		this.file = file;
	}

	public Date getStartUploadDate() {
		return startUploadDate;
	}

	public void setStartUploadDate(Date startUploadDate) {
		this.startUploadDate = startUploadDate;
	}

	public Date getEndUploadDate() {
		return endUploadDate;
	}

	public void setEndUploadDate(Date endUploadDate) {
		this.endUploadDate = endUploadDate;
	}

	public String getErrorUploadMessage() {
		return errorUploadMessage;
	}

	public void setErrorUploadMessage(String errorUploadMessage) {
		this.errorUploadMessage = errorUploadMessage;
	}

	public Boolean getShowUploadMessage() {
		return showUploadMessage;
	}

	public void setShowUploadMessage(Boolean showUploadMessage) {
		this.showUploadMessage = showUploadMessage;
	}

	public List<MonitoringManualUploadSp2d> getDetailListHidden() {
		return detailListHidden;
	}

	public void setDetailListHidden(List<MonitoringManualUploadSp2d> detailListHidden) {
		this.detailListHidden = detailListHidden;
	}
}
