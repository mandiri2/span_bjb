package com.mii.beans;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.mii.dao.InputNoSaktiDao;
import com.mii.dao.ReportDNPDao;
import com.mii.helpers.FacesUtil;
import com.mii.helpers.HelperUtils;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.SessionBean;
import com.mii.models.InputNoSaktiM;
import com.mii.models.Rtgs;


@ManagedBean
@ViewScoped
public class InputNoSaktiHandler implements Serializable {

	private static final long serialVersionUID = 1L;
	@ManagedProperty(value="#{applicationBean}")
	private ApplicationBean applicationBean;
	@ManagedProperty(value="#{sessionBean}")
	private SessionBean sessionBean;
	private List<InputNoSaktiM> filteredTransaction;
	private InputNoSaktiM transactionDetailM = new InputNoSaktiM();
	private List<InputNoSaktiM> listReport = new ArrayList<InputNoSaktiM>();
	private List<InputNoSaktiM> listAwal;
	private List<Rtgs> listDataLimpah = new ArrayList<Rtgs>();
	private Date startDate;
	private Date endDate;
	private BigDecimal totalSetoran;
	private int totalTrx;
	private String noSakti,referenceNo,noMir,trxFlag;
	private String tglbk;
	private String currency;
	private String ntpn,ntb;
	private boolean disableButton=true;
	private boolean selectOption=true;
	private Date tglbk1;
	private String kodeBilling,kodeBillingS;

	private String startDateS,endDateS,tahunDateS;
	private String jmlSet,jmlTrx;
	
	@PostConstruct
	public void init() {
		FacesUtil.authMenu(sessionBean.getUser(), "inputNoSakti");

	}
	
	public void handleDateSelect(InputNoSaktiHandler event){
	}


	public InputNoSaktiHandler() {

		startDateS = "TO_CHAR(SYSDATE, 'MMdd')";
		kodeBillingS = "";
		ntb="";
		totalSetoran = new BigDecimal(0);
		setDisableButton(true);
		Date date=new Date();
		startDate=date;
	}

	public void saveNoSakti() {
		try{
			InputNoSaktiDao inputNoSaktiDao = (InputNoSaktiDao) applicationBean.getCorpGWContext().getBean("inputNoSaktiDao");	
			for (int i = 0; i < listReport.size(); i++) {
				listReport.get(i).setNoSakti(noSakti);
				inputNoSaktiDao.updateNoSakti(listReport.get(i));
			}	
			insertPelimpahan();
			String headerMessage = "Success";
			String bodyMessage = "Input nomor sakti "+ noSakti +" Successfully";
	
			FacesUtil.setInfoMessageDialog(headerMessage, bodyMessage);
		}catch(Exception e){
			String headerMessage = "Error";
			String bodyMessage = "Input nomor sakti "+ noSakti +" Failed";
	
			FacesUtil.setErrorMessageDialog(headerMessage, bodyMessage);
		}
	}

	public void insertPelimpahan(){
		InputNoSaktiDao inputNoSaktiDao = (InputNoSaktiDao) applicationBean.getCorpGWContext().getBean("inputNoSaktiDao");
		List<InputNoSaktiM> listDataLimpah = inputNoSaktiDao.getListMir(referenceNo);
		trxFlag = "10";
		noMir = noSakti;
		String trxId = "MRTGS-"+ new SimpleDateFormat("yyyyMMdd-HHmmss").format(new Date());
		String refno = "";
		String mir = "";
		for (InputNoSaktiM rt : listDataLimpah){
			 refno = rt.getReferenceNo();
			 mir = rt.getNoSakti();
		}
		
		if (listDataLimpah.size() > 0){ //hasil array
				System.out.println("rt.getNoSakti()=="+noMir);
			if (mir == null || mir.isEmpty()){ //mir dalam array
					inputNoSaktiDao.updateNoMir(noMir, referenceNo,startDateS,trxFlag);
//				update no mir by referenceNo 
				}	
		}else
		{
			inputNoSaktiDao.insertDataPelimpahan(trxId, startDateS, referenceNo, noMir, trxFlag);
//			insert tanggal buku, reference no, dan no mir, status diisi '10'
		}
	}
	
	//get data for list data di form nosakti
	public void getAllReport(List<InputNoSaktiM> lisReportMs) {
		InputNoSaktiDao inputNoSaktiDao = (InputNoSaktiDao) applicationBean.getCorpGWContext().getBean("inputNoSaktiDao");
		List<InputNoSaktiM> lisDetailM = inputNoSaktiDao.getFilterTrx(startDateS, tahunDateS,ntb);
		
		for (InputNoSaktiM transactionDetailM : lisDetailM) {
			String tahun = transactionDetailM.getTglTerimaBayar().substring(0,4);
			Date date = null;
			try {
				if (transactionDetailM.getTanggalBuku() != null) {
					date = new SimpleDateFormat("MMddyyyy").parse(transactionDetailM.getTanggalBuku() + tahun);
					transactionDetailM.setTglgabung(date);
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			lisReportMs.add(transactionDetailM);
		}
	}
	
	
//filter for input no sakti
	public void filterTransaction() {
		currency ="IDR";
		totalTrx = 0;
		totalSetoran = new BigDecimal(0);
		noSakti=null;
		
		listReport = new ArrayList<InputNoSaktiM>();	
		listAwal = new ArrayList<InputNoSaktiM>(listReport);
		DateFormat sdFormat = new SimpleDateFormat("MMdd");
		DateFormat thnFormat = new SimpleDateFormat("yyyy");
		
		startDateS = sdFormat.format(startDate);
		tahunDateS = thnFormat.format(startDate);
		
		listAwal.clear();
		getAllReport(listAwal);
		
		for(int i=0;i<listAwal.size();i++){
		
			if (listAwal.get(i).getTglgabung()!= null){
					listReport.add(listAwal.get(i));
					totalTrx++;
					totalSetoran = totalSetoran.add(new BigDecimal(listAwal.get(i).getJumlahSetoran()));
					if (totalSetoran.compareTo(new BigDecimal("0")) == 1)
					{
						disableButton=false;
						
					}
			}
		}
	}


	public List<InputNoSaktiM> getFilteredTransaction() {
		return filteredTransaction;
	}


	public void setFilteredTransaction(List<InputNoSaktiM> filteredTransaction) {
		this.filteredTransaction = filteredTransaction;
	}


	public InputNoSaktiM getTransactionDetailM() {
		return transactionDetailM;
	}


	public void setTransactionDetailM(InputNoSaktiM transactionDetailM) {
		this.transactionDetailM = transactionDetailM;
	}


	public List<InputNoSaktiM> getListReport() {
		return listReport;
	}


	public void setListReport(List<InputNoSaktiM> listReport) {
		this.listReport = listReport;
	}


	public List<InputNoSaktiM> getListAwal() {
		return listAwal;
	}


	public void setListAwal(List<InputNoSaktiM> listAwal) {
		this.listAwal = listAwal;
	}


	public Date getStartDate() {
		return startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public BigDecimal getTotalSetoran() {
		return totalSetoran;
	}


	public void setTotalSetoran(BigDecimal totalSetoran) {
		this.totalSetoran = totalSetoran;
	}


	public int getTotalTrx() {
		return totalTrx;
	}


	public void setTotalTrx(int totalTrx) {
		this.totalTrx = totalTrx;
	}


	public String getNoSakti() {
		return noSakti;
	}


	public void setNoSakti(String noSakti) {
		this.noSakti = noSakti;
	}


	public String getTglbk() {
		return tglbk;
	}


	public void setTglbk(String tglbk) {
		this.tglbk = tglbk;
	}



	public String getNtpn() {
		return ntpn;
	}


	public void setNtpn(String ntpn) {
		this.ntpn = ntpn;
	}


	public boolean isDisableButton() {
		return disableButton;
	}


	public void setDisableButton(boolean disableButton) {
		this.disableButton = disableButton;
	}


	public boolean isSelectOption() {
		return selectOption;
	}


	public void setSelectOption(boolean selectOption) {
		this.selectOption = selectOption;
	}


	public Date getTglbk1() {
		return tglbk1;
	}


	public void setTglbk1(Date tglbk1) {
		this.tglbk1 = tglbk1;
	}


	public String getKodeBilling() {
		return kodeBilling;
	}


	public void setKodeBilling(String kodeBilling) {
		this.kodeBilling = kodeBilling;
	}


	public String getKodeBillingS() {
		return kodeBillingS;
	}


	public void setKodeBillingS(String kodeBillingS) {
		this.kodeBillingS = kodeBillingS;
	}


	public String getStartDateS() {
		return startDateS;
	}


	public void setStartDateS(String startDateS) {
		this.startDateS = startDateS;
	}


	public String getEndDateS() {
		return endDateS;
	}


	public void setEndDateS(String endDateS) {
		this.endDateS = endDateS;
	}


	public String getTahunDateS() {
		return tahunDateS;
	}


	public void setTahunDateS(String tahunDateS) {
		this.tahunDateS = tahunDateS;
	}


	public String getJmlSet() {
		return jmlSet;
	}


	public void setJmlSet(String jmlSet) {
		this.jmlSet = jmlSet;
	}


	public String getJmlTrx() {
		return jmlTrx;
	}


	public void setJmlTrx(String jmlTrx) {
		this.jmlTrx = jmlTrx;
	}


	public String getCurrency() {
		return currency;
	}


	public void setCurrency(String currency) {
		this.currency = currency;
	}


	public ApplicationBean getApplicationBean() {
		return applicationBean;
	}


	public void setApplicationBean(ApplicationBean applicationBean) {
		this.applicationBean = applicationBean;
	}


	public SessionBean getSessionBean() {
		return sessionBean;
	}


	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}


	public String getNtb() {
		return ntb;
	}


	public void setNtb(String ntb) {
		this.ntb = ntb;
	}

	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public String getNoMir() {
		return noMir;
	}

	public void setNoMir(String noMir) {
		this.noMir = noMir;
	}

	public List<Rtgs> getListDataLimpah() {
		return listDataLimpah;
	}

	public void setListDataLimpah(List<Rtgs> listDataLimpah) {
		this.listDataLimpah = listDataLimpah;
	}

	public String getTrxFlag() {
		return trxFlag;
	}

	public void setTrxFlag(String trxFlag) {
		this.trxFlag = trxFlag;
	}
	



}
