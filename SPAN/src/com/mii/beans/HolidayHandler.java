package com.mii.beans;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.apache.commons.lang.SerializationUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.mii.constant.Constants;
import com.mii.dao.ApprovalDao;
import com.mii.dao.HolidayDao;
import com.mii.dao.SystemParameterDao;
import com.mii.helpers.FacesUtil;
import com.mii.helpers.HelperUtils;
import com.mii.models.Approval;
import com.mii.models.Branch;
import com.mii.models.Holiday;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.SessionBean;

@ManagedBean
@ViewScoped
public class HolidayHandler implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private Holiday holiday;
	
	private Holiday holidayTemp;

	private LazyDataModel<Holiday> holidayList;
	
	private String action;
	
	private String holidayId = "";
	
	private String holidayName = "";
	
	private Date holidayDate = null;

	private List<String> status;

	private boolean pending;
	private boolean approved;
	private boolean rejected;
	
	@ManagedProperty(value="#{applicationBean}")
	private ApplicationBean applicationBean;
	
	@ManagedProperty(value="#{sessionBean}")
	private SessionBean sessionBean;
	
	private boolean doShowSearch = false;
	
	private boolean doShowHistory = false;
	
	private String currentHolidayName;
	
	@PostConstruct
	public void init(){
		FacesUtil.authMenu(sessionBean.getUser(), "holiday");
		doSearch();
		
	}
	
	private HolidayDao getHolidayDAOBean(){
		return (HolidayDao) applicationBean.getCorpGWContext().getBean("holidayDAO");
	}
	

	private SystemParameterDao getSystemParameterDAOBean(){
		return (SystemParameterDao) applicationBean.getCorpGWContext().getBean("systemParameterDAO");
	}
	
	private ApprovalDao getApprovalDAOBean(){
		return (ApprovalDao) applicationBean.getCorpGWContext().getBean("approvalDAO");
	}

	public ApplicationBean getApplicationBean() {
		return applicationBean;
	}

	public void setApplicationBean(ApplicationBean applicationBean) {
		this.applicationBean = applicationBean;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public Holiday getHoliday() {
		return holiday;
	}

	public void setHoliday(Holiday holiday) {
		this.holiday = holiday;
	}
	
	public Holiday getHolidayTemp() {
		return holidayTemp;
	}

	public void setHolidayTemp(Holiday holidayTemp) {
		this.holidayTemp = holidayTemp;
	}

	public LazyDataModel<Holiday> getHolidayList() {
		return holidayList;
	}

	public void setHolidayList(LazyDataModel<Holiday> holidayList) {
		this.holidayList = holidayList;
	}

	public String getHolidayId() {
		return holidayId;
	}

	public void setHolidayId(String holidayId) {
		this.holidayId = holidayId;
	}

	public String getHolidayName() {
		return holidayName;
	}

	public void setHolidayName(String holidayName) {
		this.holidayName = holidayName;
	}

	public Date getHolidayDate() {
		return holidayDate;
	}

	public void setHolidayDate(Date holidayDate) {
		this.holidayDate = holidayDate;
	}
	
	public List<String> getStatus() {
		return status;
	}

	public void setStatus(List<String> status) {
		this.status = status;
	}

	public boolean isPending() {
		return pending;
	}

	public void setPending(boolean pending) {
		this.pending = pending;
	}

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}

	public boolean isRejected() {
		return rejected;
	}

	public void setRejected(boolean rejected) {
		this.rejected = rejected;
	}

	public boolean isDoShowSearch() {
		return doShowSearch;
	}

	public void setDoShowSearch(boolean doShowSearch) {
		this.doShowSearch = doShowSearch;
	}
	
	

	public boolean isDoShowHistory() {
		return doShowHistory;
	}

	public void setDoShowHistory(boolean doShowHistory) {
		this.doShowHistory = doShowHistory;
	}

	public void showSearch(){
		doShowSearch = true;
	}
	
	public void hideSearch(){
		doShowSearch = false;
	}
	
	public void showHistory(){
		doShowHistory = true;
	}
	
	public void hideHistory(){
		doShowHistory = false;
	}
	
	public void doSearch(){
		status 		= new ArrayList<String>();
		
		if (((pending == false) && (approved == false)) && (rejected == false)){
			status.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
			status.add(Constants.APPROVAL_STATUS_PENDING_EDIT);
			status.add(Constants.APPROVAL_STATUS_PENDING_DELETE);
			status.add(Constants.APPROVAL_STATUS_APPROVED_CREATE);
			status.add(Constants.APPROVAL_STATUS_APPROVED_EDIT);
			status.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
			status.add(Constants.APPROVAL_STATUS_REJECTED_EDIT);
			status.add(Constants.APPROVAL_STATUS_REJECTED_DELETE);
		}else{
			if (pending == true){
				status.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
				status.add(Constants.APPROVAL_STATUS_PENDING_EDIT);
				status.add(Constants.APPROVAL_STATUS_PENDING_DELETE);
			}
			if (approved == true){
				status.add(Constants.APPROVAL_STATUS_APPROVED_CREATE);
				status.add(Constants.APPROVAL_STATUS_APPROVED_EDIT);
			}
			if (rejected == true){
				status.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
				status.add(Constants.APPROVAL_STATUS_REJECTED_EDIT);
				status.add(Constants.APPROVAL_STATUS_REJECTED_DELETE);
			}
		}

		FacesUtil.resetPage("holidayForm:holidayTable");
		holidayList = new HolidayDataModel();
		
	}

	public void newHoliday(){
		this.holiday = new Holiday();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd-hhmm-sss");
		Date date = new Date();
		String id = sdf.format(date);
		Integer seq = getSystemParameterDAOBean().getParameterSequence().intValue();
		this.holiday.setHolidayId("BRH-"+id+String.format("%04d", seq));
		this.action = "";
	}
	
	public void doSave(){
		if (this.action!=null && this.action.equals("edit")) {
			if (this.holiday.getStatus().equals(Constants.APPROVAL_STATUS_PENDING_EDIT)){
				try {

					Holiday holidayExist = getHolidayDAOBean().getDetailedHolidayByHolidayName(this.holidayTemp.getHolidayName());
					
					if (holidayExist == null || (holidayExist.getHolidayName().equals(currentHolidayName))){
						getApprovalDAOBean().updateApprovalNewObjectByParameterId(HelperUtils.serialize(this.holidayTemp), this.holidayTemp.getHolidayId());
						
						

						RequestContext.getCurrentInstance().execute("insertOrEditHoliday.hide()");
						RequestContext.getCurrentInstance().execute("insertOrEditHolidayPending.hide()");
						
						String headerMessage = "Edit Holiday Successfully";
						String bodyMessage = "Supervisor must approve the editing of the <br/>parameter &apos;"
								+ holiday.getHolidayName() + "&apos; to completely the process.";
	
						FacesUtil.setInfoMessageDialog(headerMessage, bodyMessage);
					}else{
						String headerMessage = "Attention";
						String bodyMessage = "Holiday "+this.holidayTemp.getHolidayName() + " already exist in the application.";

						FacesUtil.setWarningMessageDialog(headerMessage, bodyMessage);
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else{

				Holiday holidayExist = getHolidayDAOBean().getDetailedHolidayByHolidayName(this.holiday.getHolidayName());
				
				if (holidayExist == null || (holidayExist.getHolidayName().equals(currentHolidayName))){
					editHoliday(this.holiday);
				}else{
					String headerMessage = "Attention";
					String bodyMessage = "Holiday "+this.holiday.getHolidayName() + " already exist in the application.";

					FacesUtil.setWarningMessageDialog(headerMessage, bodyMessage);
				}
			}
			
			
		}else{			
			try {

				Holiday holidayExist = getHolidayDAOBean().getDetailedHolidayByHolidayName(this.holiday.getHolidayName());
				
				if (holidayExist == null || (holidayExist.getHolidayName().equals(currentHolidayName))){
					createHoliday(this.holiday);
				}else{
					String headerMessage = "Attention";
					String bodyMessage = "Holiday "+this.holiday.getHolidayName() + " already exist in the application.";

					FacesUtil.setWarningMessageDialog(headerMessage, bodyMessage);
				}
				
			} catch (Exception e) {
				String headerMessage = "Error";
				String bodyMessage = "System error";

				FacesUtil.setErrorMessageDialog(headerMessage, bodyMessage);
				e.printStackTrace();
			}
		}
	}

	public void editHoliday(Holiday holiday){
		HolidayDao holidayDao = getHolidayDAOBean();
		holiday.setUpdateDate(HelperUtils.getCurrentDateTime());
		holiday.setChangeWho(sessionBean.getUser().getUserName());
		
		try {
			if (holiday.getStatus().equals(Constants.APPROVAL_STATUS_PENDING_CREATE)){
				
				getApprovalDAOBean().updateApprovalNewObjectByParameterId(HelperUtils.serialize(this.holiday), this.holiday.getHolidayId());
				
			}else if (holiday.getStatus().equals(Constants.APPROVAL_STATUS_REJECTED_CREATE)){
				
				Approval approval = new Approval();
				approval.setAction(Constants.APPROVAL_ACTION_CREATE);
				approval.setParameter(Constants.APPROVAL_PARAMETER_HOLIDAY);
				approval.setParameterId(holiday.getHolidayId());
				approval.setParameterName(holiday.getHolidayName());
				approval.setBranchId(sessionBean.getUser().getBranch());
				approval.setOldByteObject(HelperUtils.serialize(this.holidayTemp));
				approval.setNewByteObject(HelperUtils.serialize(this.holiday));
				getApprovalDAOBean().saveApproval(approval);
				
				holiday = (Holiday) SerializationUtils.clone(this.holidayTemp);
				holiday.setStatus(Constants.APPROVAL_STATUS_PENDING_CREATE);
				
			}else {
				Approval approval = new Approval();
				approval.setAction(Constants.APPROVAL_ACTION_EDIT);
				approval.setParameter(Constants.APPROVAL_PARAMETER_HOLIDAY);
				approval.setParameterId(holiday.getHolidayId());
				approval.setParameterName(holiday.getHolidayName());
				approval.setBranchId(sessionBean.getUser().getBranch());
				approval.setOldByteObject(HelperUtils.serialize(this.holidayTemp));
				approval.setNewByteObject(HelperUtils.serialize(this.holiday));
				getApprovalDAOBean().saveApproval(approval);
				
				holiday = (Holiday) SerializationUtils.clone(this.holidayTemp);
				holiday.setStatus(Constants.APPROVAL_STATUS_PENDING_EDIT);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		holidayDao.saveHoliday(holiday);
		

		RequestContext.getCurrentInstance().execute("insertOrEditHoliday.hide()");
		RequestContext.getCurrentInstance().execute("insertOrEditHolidayPending.hide()");
		
		String headerMessage = "Edit Holiday Successfully";
		String bodyMessage = "Supervisor must approve the editing of the <br/>holiday &apos;"
				+ holiday.getHolidayName() + "&apos; to completely the process.";

		FacesUtil.setInfoMessageDialog(headerMessage, bodyMessage);
	}
	
	public void createHoliday(Holiday holiday){		
		holiday.setCreateDate(HelperUtils.getCurrentDateTime());
		holiday.setUpdateDate(HelperUtils.getCurrentDateTime());
		holiday.setChangeWho(sessionBean.getUser().getUserName());
		holiday.setStatus(Constants.APPROVAL_STATUS_PENDING_CREATE);
		
		Approval approval = new Approval();
		approval.setAction(Constants.APPROVAL_ACTION_CREATE);
		approval.setParameter(Constants.APPROVAL_PARAMETER_HOLIDAY);
		approval.setParameterId(holiday.getHolidayId());
		approval.setParameterName(holiday.getHolidayName());
		approval.setBranchId(sessionBean.getUser().getBranch());
		try {
			approval.setNewByteObject(HelperUtils.serialize(this.holiday));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		getApprovalDAOBean().saveApproval(approval);
		getHolidayDAOBean().saveHoliday(holiday);
		

		RequestContext.getCurrentInstance().execute("insertOrEditHoliday.hide()");
		RequestContext.getCurrentInstance().execute("insertOrEditHolidayPending.hide()");
		
		String headerMessage = "Create Holiday Successfully";
		String bodyMessage = "Supervisor must approve the creating of the <br/>holiday &apos;"
				+ holiday.getHolidayName() + "&apos; to completely the process.";

		FacesUtil.setInfoMessageDialog(headerMessage, bodyMessage);
	}
	
	public void viewDetailedHoliday(Holiday holiday){
		HolidayDao holidayDao = getHolidayDAOBean();
		this.holiday = holidayDao.getDetailedHoliday(holiday.getHolidayId());
		
		if (this.holiday.getStatus().equals(Constants.APPROVAL_STATUS_PENDING_EDIT)){
			List<Approval> approvals = getApprovalDAOBean().getApprovalByParameterId(holiday.getHolidayId());
			if (approvals.size()>0){
				try {
					this.holidayTemp = (Holiday) HelperUtils.deserialize(approvals.get(0).getNewBlobObject().getBinaryStream());
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				RequestContext.getCurrentInstance().execute(
						"viewHolidayEdit.show()");
			}
		}else{
			doShowHistory = false;
			RequestContext.getCurrentInstance().execute("viewHoliday.show()");
		}
	}
	
	public void viewDetailedHolidayEdit(Holiday holiday){
		
		HolidayDao holidayDao = getHolidayDAOBean();
		this.holiday = holidayDao.getDetailedHoliday(holiday.getHolidayId());
		
		if (this.holiday.getDisableEdit() == true){
			FacesUtil.setApprovalWarning("holiday", this.holiday.getHolidayName(), this.holiday.getStatus());
		}else{

			if (this.holiday.getStatus().equals(Constants.APPROVAL_STATUS_PENDING_EDIT)){
					
				List<Approval> approvals = getApprovalDAOBean().getApprovalByParameterId(holiday.getHolidayId());
				if (approvals.size()>0){
					try {
						this.holidayTemp = (Holiday) HelperUtils.deserialize(approvals.get(0).getNewBlobObject().getBinaryStream());
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					RequestContext.getCurrentInstance().execute(
							"insertOrEditHolidayPending.show()");
				}
			}
			else {
				
				this.holidayTemp =  (Holiday) SerializationUtils.clone(this.holiday);
				RequestContext.getCurrentInstance().execute(
						"insertOrEditHoliday.show()");
			}

			currentHolidayName = this.holidayTemp.getHolidayName();
			this.action = "edit";
		}
	}
	
	public void viewDetailedDelete(Holiday holiday){

		HolidayDao holidayDao = getHolidayDAOBean();
		this.holiday = holidayDao.getDetailedHoliday(holiday.getHolidayId());
		
		if (this.holiday.getDisableDelete() == true){
			FacesUtil.setApprovalWarning("holiday", this.holiday.getHolidayName(), this.holiday.getStatus());
		}else{
			RequestContext.getCurrentInstance().execute(
					"dlgConfirm.show()");
		}
		
	}
	
	public void deleteHoliday(){
		

		if (holiday.getStatus().equals(Constants.APPROVAL_STATUS_REJECTED_CREATE)){

			getHolidayDAOBean().deleteHoliday(holiday.getHolidayId());
			
			String headerMessage = "Success";
			String bodyMessage = "Delete Holiday " + this.holiday.getHolidayName() + "Successfully";
	
			FacesUtil.setInfoMessageDialog(headerMessage, bodyMessage);
			
		}else{
		
			Approval approval = new Approval();
			approval.setAction(Constants.APPROVAL_ACTION_DELETE);
			approval.setParameter(Constants.APPROVAL_PARAMETER_HOLIDAY);
			approval.setParameterId(holiday.getHolidayId());
			approval.setParameterName(holiday.getHolidayName());
			approval.setBranchId(sessionBean.getUser().getBranch());
			
			try {
				approval.setOldByteObject(HelperUtils.serialize(this.holiday));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			getApprovalDAOBean().saveApproval(approval);
			
			this.holiday.setStatus(Constants.APPROVAL_STATUS_PENDING_DELETE);
			getHolidayDAOBean().saveHoliday(this.holiday);
			
			String headerMessage = "Delete Holiday Successfully";
			String bodyMessage = "Supervisor must approve the deleting of the <br/>holiday &apos;"
					+ holiday.getHolidayName()+ "&apos; to completely the process.";
	
			FacesUtil.setInfoMessageDialog(headerMessage, bodyMessage);
		
		}
		
	}
	
	private class HolidayDataModel extends LazyDataModel<Holiday>{
		private static final long serialVersionUID = 1L;
		public HolidayDataModel() {
			
		}
		@Override
		public List<Holiday> load(int startingAt, 
				int maxPerPage, String sortField, 
				SortOrder sortOrder, Map<String, String> filters) {
			
			HolidayDao holidayDao = (HolidayDao) getHolidayDAOBean();
			List<Holiday> holidays;
			
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			String hd = "";
			if ((holidayDate != null) && (!holidayDate.equals("")))
				hd = df.format(holidayDate);
			
			holidays = holidayDao.getHolidayLazyLoadingByCategory(maxPerPage+startingAt, startingAt, holidayId, holidayName, hd, status);
			Integer jmlAll = holidayDao.countHolidayByCategory(holidayId, holidayName, hd, status);
			setRowCount(jmlAll);
			setPageSize(maxPerPage);

			return holidays;
		}
	}
	
}