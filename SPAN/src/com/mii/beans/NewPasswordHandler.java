package com.mii.beans;

import java.io.IOException;
import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.mii.bjb.core.UIMBJBConnector;
import com.mii.bjb.core.UIMBJBConstants;
import com.mii.bjb.core.UIMBJBHelper;
import com.mii.bjb.core.UIMBJBModel;
import com.mii.constant.Constants;
import com.mii.dao.SystemParameterDao;
import com.mii.dao.UserDao;
import com.mii.helpers.EncryptUtils;
import com.mii.helpers.FacesUtil;
import com.mii.models.User;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.SessionBean;

@ManagedBean
@ViewScoped
public class NewPasswordHandler implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String password;
	private String passwordValid;
	
	@ManagedProperty(value="#{applicationBean}")
	private ApplicationBean applicationBean;
	
	@ManagedProperty(value="#{sessionBean}")
	private SessionBean sessionBean;
	
	@PostConstruct
	public void init(){
		
	}
	
	public String doSubmit(){
		if(password.equals(passwordValid)){
			try{
				UserDao userDao = (UserDao) applicationBean.getCorpGWContext().getBean("userDAO");
				User user = sessionBean.getUser();
				if(user.getFirstLogin().equals(Constants.FISTLOGINCHANGE)){
					UIMBJBModel uimBJBModel = new UIMBJBModel();
					uimBJBModel = sendToUIM(user.getUserName(), user.getPassword(), password);
					if(uimBJBModel.getResponseMessage().equals(UIMBJBConstants.UIM_CP_BERHASIL)){
//						user.setPassword(EncryptUtils.EncryptEncode(password));
						user.setFirstLogin(Constants.FISTLOGINNO);
						userDao.editUser(user);
						FacesUtil.getExternalContext().getSessionMap().remove("user");
						return "loginSPAN?faces-redirect=true";
					}else{
						FacesUtil.setErrorMessage("msg", uimBJBModel.getResponseMessage(), null);
					}
				}else{
					user.setPassword(EncryptUtils.EncryptEncode(password));
					user.setFirstLogin(Constants.FISTLOGINNO);
					userDao.editUser(user);
					FacesUtil.getExternalContext().getSessionMap().remove("user");
					return "loginSPAN?faces-redirect=true";
				}
			}catch(Exception e){
				e.printStackTrace();
				String headerMessage = "Error";
				String bodyMessage = "Failed to change password. Please refresh this page. ";
				FacesUtil.setErrorMessageDialog(headerMessage, bodyMessage);
			}
		}else{
			FacesUtil.setErrorMessage("msg", "Password is not match. ", null);
		}
		return null;
	}
	
	public String doCancel(){
		FacesUtil.getExternalContext().getSessionMap().remove("user");
		return "loginSPAN?faces-redirect=true";
	}
	
	public UIMBJBModel sendToUIM(String username, String oldPassword, String newPassword) throws IOException{
		SystemParameterDao parameterDao = (SystemParameterDao) applicationBean.getCorpGWContext().getBean("systemParameterDAO");
		String request = UIMBJBHelper.packUIMRequestChangePass(username, oldPassword, newPassword, parameterDao);
		String response = UIMBJBConnector.doSendRequest(request, parameterDao);
		UIMBJBModel uimBJBModel = UIMBJBHelper.unpackUIMResponse(response,UIMBJBConstants.UIM_FLAG_CHANGE_PASSWORD);
		return uimBJBModel;
	}
	
	public ApplicationBean getApplicationBean() {
		return applicationBean;
	}

	public void setApplicationBean(ApplicationBean applicationBean) {
		this.applicationBean = applicationBean;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordValid() {
		return passwordValid;
	}

	public void setPasswordValid(String passwordValid) {
		this.passwordValid = passwordValid;
	}
}