package com.mii.beans;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JExcelApiExporterParameter;
import net.sf.jasperreports.engine.export.JRXlsExporter;

import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Rectangle;
import com.mii.constant.Constants;
import com.mii.dao.SPANHostDataDetailsDao;
import com.mii.dao.SystemParameterDao;
import com.mii.dao.TransactionReportDao;
import com.mii.dao.TransactionReportDaoImpl;
//import com.mii.dao.TransactionReportDao;
import com.mii.helpers.FacesUtil;
import com.mii.invoker.PostgresConnectionUtils;
import com.mii.is.util.PubUtil;
import com.mii.models.InternalReportBJB;
import com.mii.models.SpanHostResponseCode;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.SessionBean;
import com.mii.util.jasper.JasperExporter;
import com.mii.util.jasper.JasperExporterFactory;

@ManagedBean
@ViewScoped
public class TransactionReportHandler implements Serializable {
	private static final long serialVersionUID = 1L;

	private LazyDataModel<InternalReportBJB> detailList;

	@ManagedProperty(value = "#{applicationBean}")
	private ApplicationBean applicationBean;

	@ManagedProperty(value = "#{sessionBean}")
	private SessionBean sessionBean;
	
	@ManagedProperty(value = "#{postgresConnectionUtils}")
	private PostgresConnectionUtils postgresConnectionUtils;
	
	private boolean doShowGaji;
	private boolean doShowReject;
	private boolean doShowValidation;

	private boolean doShowSearch;

	private Date startDocDate;
	private Date endDocDate;
	private String accountCodeNumber;
	private String paymentMethod;
	private String transactionStatus;
	private List<SelectItem> trxStatusList;
	private String sp2dNo;
	private String accountName;
	private String fileName;
	private Date rejectDate;
	private boolean checkBox;
	private boolean disabledRejectedDate;
	
	@PostConstruct
	public void init() {
		FacesUtil.authMenu(getSessionBean().getUser(), "transactionReport");
		doClickGaji();
		initTrxStatusList();
	}

	private void initTrxStatusList() {
		trxStatusList = new ArrayList<SelectItem>();
		SPANHostDataDetailsDao dao = (SPANHostDataDetailsDao) applicationBean.getCorpGWContext().getBean("SPANHostDataDetailsDao");
		List<SpanHostResponseCode> rspCodes = dao.getTransactionStatus();
		if(PubUtil.isListNotEmpty(rspCodes)){
			SelectItem all = new SelectItem("", "ALL");
			SelectItem success = new SelectItem("00", "SUCCESS");
			
			trxStatusList.add(all);
			trxStatusList.add(success);
			
			for(SpanHostResponseCode rspCode : rspCodes){
				SelectItem selItem = new SelectItem(rspCode.getRESPONSE_CODE(), rspCode.getDESCRIPTION());
				trxStatusList.add(selItem);
			}
		}
	}

	public void doClickGaji() {
		doShowGaji = true;
		doShowReject = false;
		doShowValidation = false;
		
		clearSearch();
		detailList = new DetailDataModel();
	}

	public void doClickReject() {
		doShowGaji = false;
		doShowReject = true;
		doShowValidation = false;
		
		clearSearch();
		detailList = new DetailDataModel();
	}

	public void doClickValidation() {
		doShowGaji = false;
		doShowReject = false;
		doShowValidation = true;
		
		clearSearch();
		detailList = new DetailDataModel();
	}

	public void showSearch() {
		setDoShowSearch(true);
	}

	public void hideSearch() {
		setDoShowSearch(false);
	}

	public void doSearch() {
		System.out.println("start date : " + startDocDate);
		System.out.println("end date : " + endDocDate);
		System.out.println("account code no: " + accountCodeNumber);
		System.out.println("payment method : " + paymentMethod);

		loadData();
	}

	private void loadData() {
		setDetailList(new DetailDataModel());

	}

	/*
	 * public class DetailDataModel extends LazyDataModel<InternalReportBJB>{
	 * 
	 * private static final long serialVersionUID = 1L;
	 * 
	 * public DetailDataModel() {
	 * 
	 * }
	 * 
	 * @Override public java.util.List<InternalReportBJB> load(int first, int
	 * pageSize, String sortField, org.primefaces.model.SortOrder sortOrder,
	 * java.util.Map<String, String> filters) {
	 * 
	 * SPANHostDataDetailsDao dao = (SPANHostDataDetailsDao) applicationBean
	 * .getCorpGWContext().getBean("SPANHostDataDetailsDao"); // check if date
	 * null set sysdate if (startDocDate == null) startDocDate = new Date(); if
	 * (endDocDate == null) endDocDate = new Date(); // TODO filtering query by
	 * trxstatus List<InternalReportBJB> datas = dao
	 * .getTransactionReportLazyLoading(pageSize + first, first, startDocDate,
	 * endDocDate, accountCodeNumber, paymentMethod); int jmlDatas =
	 * dao.getRowCountTransactionReport(startDocDate, endDocDate,
	 * accountCodeNumber, paymentMethod); setRowCount(jmlDatas);
	 * setPageSize(pageSize); return datas;
	 * 
	 * }
	 * 
	 * }
	 */

	public void clearSearch() {
		startDocDate = new Date();
		endDocDate = new Date();
		accountCodeNumber = "";
		paymentMethod = "";
		transactionStatus = "";
		sp2dNo = "";
		accountName = "";
		fileName = "";
		setCheckBox(false);
	}

	public void preProcessPDF(Object document) throws IOException,
			BadElementException, DocumentException {
		Document pdf = (Document) document;
		Rectangle r = new Rectangle(2000f, 1000f);
		pdf.setPageSize(r);
		pdf.setMargins(0f, 0f, 100f, 100f);
	}

	
	public void generatePenyaluranRejectGajiPDFReport() {
		String fileName = "Reject_Penyaluran.pdf";
		SimpleDateFormat sdf= new SimpleDateFormat("yyyyMMdd");
		try{
			if(startDocDate!=null&endDocDate!=null){
				fileName=PubUtil.concatString("Reject_Penyaluran",sdf.format(startDocDate),"-",sdf.format(endDocDate),".pdf");
			}else{
				fileName="Reject_Penyaluran.pdf";
			}
			String jasperPath = FacesUtil.getExternalContext().getRealPath("resources/report/reject_penyaluran.jasper");
			generateReport(fileName, jasperPath, Constants.PDF, getParameterRejectPenyaluranGaji(), Constants.CONTENT_TYPE_PDF);
		}catch(Exception e){
			System.out.println(e.getMessage());
		}

	}
	
	public void generatePenyaluranRejectGajiXLSReport() {
		
		try {
			String fileName = "Reject_Penyaluran.xls";
			String jasperPath = FacesUtil.getExternalContext().getRealPath("resources/report/Reject_Penyaluran_Xls.jasper");
			generateReport(fileName, jasperPath, Constants.EXCEL, getParameterRejectPenyaluranGaji(), Constants.CONTENT_TYPE_XLS);
		} catch (Exception e) {
			       e.printStackTrace();
			       System.out.println(e.getMessage());
	   }
	}
	
	
	private JRXlsExporter getExporterParameter(JasperPrint jasperPrint, ByteArrayOutputStream xlsReport) {
		JRXlsExporter exporter=new JRXlsExporter();
		exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
		exporter.setParameter(JRExporterParameter.OUTPUT_STREAM,xlsReport);
		exporter.setParameter(JExcelApiExporterParameter.MAXIMUM_ROWS_PER_SHEET,5000);
		exporter.setParameter(JExcelApiExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS,true);
		exporter.setParameter(JExcelApiExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS,true);
		exporter.setParameter(JExcelApiExporterParameter.IS_FONT_SIZE_FIX_ENABLED,true);
		exporter.setParameter(JExcelApiExporterParameter.IS_WHITE_PAGE_BACKGROUND,false);
		return exporter;
		}
	
	
	public void generatePenyaluranGajiPDFReport() {
		String fileName = "Penyaluran_Gaji.pdf";
		String jasperPath = FacesUtil.getExternalContext().getRealPath("resources/report/TransactionReport_Penyaluran_Gaji.jasper");
		generateReport(fileName, jasperPath, Constants.PDF, getParameterPenyaluranGaji(), Constants.CONTENT_TYPE_PDF);
	}

	public void generatePenyaluranGajiXLSReport() {
		String fileName = "Penyaluran_Gaji.xls";
		String jasperPath = FacesUtil.getExternalContext().getRealPath("resources/report/TransactionReport_Penyaluran_Gaji_XLS.jasper");
		generateReport(fileName, jasperPath, Constants.EXCEL, getParameterPenyaluranGaji(), Constants.CONTENT_TYPE_XLS);
	}
	
	

	private HashMap getParameterPenyaluranGaji() {
		HashMap param = new HashMap();
		param.put("startDocDate", PubUtil.dateToStringNewDateIfNull("yyyy-MM-dd", startDocDate));
		param.put("endDocDate", PubUtil.dateToStringNewDateIfNull("yyyy-MM-dd", endDocDate));
		param.put("accountCodeNumber", "%"+accountCodeNumber+"%");
		param.put("paymentMethod", "%"+paymentMethod+"%");
		param.put("transactionStatus", "%"+transactionStatus+"%");
		return param;
	}
	
	private HashMap<String, Object> getParameterRejectPenyaluranGaji() {
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("startDocDate", PubUtil.dateToStringNewDateIfNull("yyyy-MM-dd", startDocDate));
		param.put("endDocDate", PubUtil.dateToStringNewDateIfNull("yyyy-MM-dd", endDocDate));
		param.put("accountCodeNumber", "%"+accountCodeNumber+"%");
		param.put("paymentMethod", "%"+paymentMethod+"%");
		param.put("fileName", "%"+fileName+"%");
		param.put("transactionStatus", "%"+transactionStatus+"%");
		param.put("rejectDate", "%"+"%");
		return param;
	}
	
	public void generatePDFAccountValidation() {
		String fileName = "Account_Validation.pdf";
		String jasperPath = FacesUtil.getExternalContext().getRealPath("resources/report/Account_Validation.jasper");
		generateReport(fileName, jasperPath, Constants.PDF, getParameterAccountValidation(), Constants.CONTENT_TYPE_PDF);
	}

	public void generateXLSAccountValidation() {
		String fileName = "Account_Validation.xls";
		String jasperPath = FacesUtil.getExternalContext().getRealPath("resources/report/Account_Validation_XLS.jasper");
		generateReport(fileName, jasperPath, Constants.EXCEL, getParameterAccountValidation(), Constants.CONTENT_TYPE_XLS);
	}

	private HashMap getParameterAccountValidation() {
		HashMap param = new HashMap();
		param.put("startDocDate", PubUtil.dateToStringNewDateIfNull("yyyy-MM-dd", startDocDate));
		param.put("endDocDate", PubUtil.dateToStringNewDateIfNull("yyyy-MM-dd", endDocDate));
		param.put("sp2dno", "%"+sp2dNo+"%");
		param.put("accountName", "%"+accountName+"%");
		param.put("accountCodeNumber", "%"+accountCodeNumber+"%");
		param.put("transactionStatusList", TransactionReportDaoImpl.getListTransactionStatusValidation(transactionStatus));
		return param;
	}
	
	
	/** UTIL **/
	private void generateReport(String fileName, String jasperPath, String type, HashMap parameter, String contentType) {
		RequestContext.getCurrentInstance().execute("loadingDlg.show();");
		JasperExporter jasperExporter = JasperExporterFactory.getJasperExporter(type, fileName, jasperPath, parameter);
		try {
			byte[] report = jasperExporter.exportReportFromDb(postgresConnectionUtils.getPostGressConnection());
			FacesUtil.generateDownloadReport(report, fileName, contentType);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**GETTER SETTER**/
	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public ApplicationBean getApplicationBean() {
		return applicationBean;
	}

	public void setApplicationBean(ApplicationBean applicationBean) {
		this.applicationBean = applicationBean;
	}

	public boolean isDoShowSearch() {
		return doShowSearch;
	}

	public void setDoShowSearch(boolean doShowSearch) {
		this.doShowSearch = doShowSearch;
	}

	public Date getStartDocDate() {
		return startDocDate;
	}

	public void setStartDocDate(Date startDocDate) {
		this.startDocDate = startDocDate;
	}

	public Date getEndDocDate() {
		return endDocDate;
	}

	public void setEndDocDate(Date endDocDate) {
		this.endDocDate = endDocDate;
	}

	public String getAccountCodeNumber() {
		return accountCodeNumber;
	}

	public void setAccountCodeNumber(String accountCodeNumber) {
		this.accountCodeNumber = accountCodeNumber;
	}

	public boolean isDoShowGaji() {
		return doShowGaji;
	}

	public void setDoShowGaji(boolean doShowGaji) {
		this.doShowGaji = doShowGaji;
	}

	public boolean isDoShowReject() {
		return doShowReject;
	}

	public void setDoShowReject(boolean doShowReject) {
		this.doShowReject = doShowReject;
	}

	public boolean isDoShowValidation() {
		return doShowValidation;
	}

	public void setDoShowValidation(boolean doShowValidation) {
		this.doShowValidation = doShowValidation;
	}

	public LazyDataModel<InternalReportBJB> getDetailList() {
		return detailList;
	}

	public void setDetailList(LazyDataModel<InternalReportBJB> detailList) {
		this.detailList = detailList;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getTransactionStatus() {
		return transactionStatus;
	}

	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

	private class DetailDataModel extends LazyDataModel<InternalReportBJB> {
		private static final long serialVersionUID = 1L;

		public DetailDataModel() {
		}

		@Override
		public List<InternalReportBJB> load(int startingAt, int maxPerPage,
				String sortField, SortOrder sortOrder,
				Map<String, String> filters) {
			Integer jmlAll = 0;
			List<InternalReportBJB> detailList = new ArrayList<InternalReportBJB>();

			SPANHostDataDetailsDao dao = (SPANHostDataDetailsDao) applicationBean
					.getCorpGWContext().getBean("SPANHostDataDetailsDao");

			if (startDocDate == null)
				startDocDate = new Date();
			if (endDocDate == null)
				endDocDate = new Date();
			if (accountCodeNumber==null) {
				accountCodeNumber = "";
			}if (paymentMethod==null) {
				paymentMethod = "";
			}if (transactionStatus == null) {
				transactionStatus = "";
			}

			try {
				if (doShowGaji) {
					detailList = getTransactionReportDAOBean()
							.getTransactionReport(startDocDate, endDocDate,
									accountCodeNumber, paymentMethod,
									transactionStatus, startingAt,
									startingAt + maxPerPage);
					jmlAll = getTransactionReportDAOBean()
							.countTransactionReport(startDocDate, endDocDate,
									accountCodeNumber, paymentMethod,
									transactionStatus);
				} 
				
				if(doShowReject) {
					detailList = getTransactionReportDAOBean().getTransactionReject(fileName, startDocDate, endDocDate, rejectDate, 
							startingAt, startingAt + maxPerPage);
					jmlAll = getTransactionReportDAOBean().countTransactionReject(fileName, startDocDate, endDocDate, rejectDate);
				} 
				
				if(doShowValidation) {
					detailList = getTransactionReportDAOBean()
							.getAccountValidation(startDocDate, endDocDate, sp2dNo,
									accountCodeNumber, accountName,
									transactionStatus, startingAt,
									startingAt + maxPerPage);
					jmlAll = getTransactionReportDAOBean()
							.countAccountValidation(startDocDate, endDocDate, sp2dNo,
									accountCodeNumber, accountName,
									transactionStatus);
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
			setRowCount(jmlAll);
			setPageSize(maxPerPage);
			return detailList;
		}

		private TransactionReportDao getTransactionReportDAOBean() {
			return (TransactionReportDao) applicationBean.getCorpGWContext()
					.getBean("transactionReportDao");
		}
	}

	private SystemParameterDao getSystemParameterDaoBean() {
		return (SystemParameterDao) applicationBean.getCorpGWContext().getBean(
				"systemParameterDAO");
	}

	public List<SelectItem> getTrxStatusList() {
		return trxStatusList;
	}

	public void setTrxStatusList(List<SelectItem> trxStatusList) {
		this.trxStatusList = trxStatusList;
	}

	public String getSp2dNo() {
		return sp2dNo;
	}

	public void setSp2dNo(String sp2dNo) {
		this.sp2dNo = sp2dNo;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public PostgresConnectionUtils getPostgresConnectionUtils() {
		return postgresConnectionUtils;
	}

	public void setPostgresConnectionUtils(PostgresConnectionUtils postgresConnectionUtils) {
		this.postgresConnectionUtils = postgresConnectionUtils;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Date getRejectDate() {
		return rejectDate;
	}

	public void setRejectDate(Date rejectDate) {
		this.rejectDate = rejectDate;
	}

	public boolean isCheckBox() {
		return checkBox;
	}

	public void setCheckBox(boolean checkBox) {
		disabledRejectedDate = checkBox ? false : true;
		rejectDate = checkBox ? new Date() : null;
		this.checkBox = checkBox;
	}

	public boolean isDisabledRejectedDate() {
		return disabledRejectedDate;
	}

	public void setDisabledRejectedDate(boolean disabledRejectedDate) {
		this.disabledRejectedDate = disabledRejectedDate;
	}

}
