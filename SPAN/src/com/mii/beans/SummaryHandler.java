package com.mii.beans;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.chart.PieChartModel;

import com.mii.constant.Constants;
import com.mii.constant.ConstantsUserLog;
import com.mii.dao.SPANDataValidationDao;
import com.mii.dao.SPANHostDataDetailsDao;
import com.mii.dao.SystemParameterDao;
import com.mii.dao.UserDao;
import com.mii.helpers.FacesUtil;
import com.mii.is.GetSPANSummaries;
import com.mii.is.GetSPANSummariesDetail;
import com.mii.is.io.GetSPANSummariesDetailRequest;
import com.mii.is.io.GetSPANSummariesDetailResponse;
import com.mii.is.io.GetSPANSummariesRequest;
import com.mii.is.io.GetSPANSummariesResponse;
import com.mii.is.io.helper.DetailsOutputList;
import com.mii.is.io.helper.SPANSummaries;
import com.mii.models.SPANDataValidation;
import com.mii.models.SpanDataSummary;
import com.mii.models.SpanDataSummaryDetails;
import com.mii.models.SpanSummariesOutput;
import com.mii.models.SystemParameter;
import com.mii.models.UserLog;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.SessionBean;

@ManagedBean
@ViewScoped
public class SummaryHandler implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private SPANDataValidationDao getSpanDataValidationDaoBean() {
		return (SPANDataValidationDao) applicationBean.getCorpGWContext().getBean("SPANDataValidationDao");
	}
	
	private SystemParameterDao getSystemParameterDAOBean(){
		return (SystemParameterDao) applicationBean.getCorpGWContext().getBean("systemParameterDAO");
	}
	
	private SPANHostDataDetailsDao getSpanHostDataDetailsDaoBean(){
		return (SPANHostDataDetailsDao) applicationBean.getCorpGWContext().getBean("SPANHostDataDetailsDao");
	}
	
	private LazyDataModel < SpanSummariesOutput > summariesList;
	private LazyDataModel < DetailsOutputList > detailList;
	
	private List<SpanSummariesOutput> summaryList;
	
	@ManagedProperty(value="#{applicationBean}")
	private ApplicationBean applicationBean;
	
	@ManagedProperty(value="#{sessionBean}")
	private SessionBean sessionBean;
	
	private SpanDataSummary spanDataSummary;
	private PieChartModel livePieModel;
	private boolean doShowSearch;
	private Date docDate;
	private Date docDateStart;
	private Date docDateEnd;
	private String sp2dno;
	
	private String searchFilename;
	private String searchTotalRecord;
	private String searchTotalRetry;
	private String searchTotalRetur;
	private String searchTotalSuccess;
	
	private Boolean searchStatus;
	private String status;
	
	private Date postingDate;
	private String postingGaji;
	private String postingGajir;
	private Boolean doShowAmount;
	
	private String detailFileName;
	private String detailTotalRecord;
	private String detailTotalAmount;
	private String detailType;
	private String detailDebitAccount;
	private String detailDebitAccountType;
	private String detailDocumentDate;
	
	public static Map < BigDecimal, SPANDataValidation > resultTemps = new HashMap < BigDecimal, SPANDataValidation > ();
	
	@PostConstruct
	public void init(){
		FacesUtil.authMenu(getSessionBean().getUser(), "summary");
//		loadChart();
		doShowSearch = false;
		spanDataSummary = new SpanDataSummary();
		loadData();
		postingDate = getDocDateTomorrow();
		getAmountPosting();
		doLoging(ConstantsUserLog.MENU_SUMMARY, ConstantsUserLog.ACTIVITY_INIT, "Menu Init.");
	}
	
	public void getAmountPosting() {
		String accountType = "";
		for(int i=0;i<2;i++){
			if(i==0){
				accountType = Constants.GAJI;
			}
			else{
				accountType = Constants.GAJIR;
			}
			SystemParameter systemParameter = getSystemParameterDAOBean().getDetailedParameterByParamName(Constants.PARAM_DEBITACCNO+accountType);
			String accountNo = systemParameter.getParamValue();
			String amount = getAmountPostingEachAcc(accountNo);
			if(i==0){
				setPostingGaji(amount);
			}
			else{
				setPostingGajir(amount);
			}
		}
	}

	private String getAmountPostingEachAcc(String accountNo) {
		DateFormat dtf = new SimpleDateFormat("yyyy-MM-dd");
		String amount = getSpanDataValidationDaoBean().getPostingAmount(dtf.format(postingDate), accountNo);
		try{
			if(amount.equals("")||amount==null){
				amount = "0";
			}
		}catch(Exception e){
			amount = "0";
		}
		DecimalFormat df = new DecimalFormat("###,###");
		amount = df.format(new BigInteger((amount)));
		return amount+".00";
	}

	private static Date getDocDateTomorrow(){
//		DateFormat dtf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, 1);
//		return dtf.format(cal.getTime());
		return cal.getTime();
	}
	
	private void loadData() {
		setSearchStatus(false);
//		setSummariesList(new DetailDataModel());
		summaryList = loadSpanSummary(); // not lazy
	}
	
	private List<SpanSummariesOutput> loadSpanSummary() {
		List < SPANSummaries > validation = new ArrayList<SPANSummaries>();
		
		GetSPANSummariesRequest request = new GetSPANSummariesRequest();
		GetSPANSummariesResponse response = null;
		GetSPANSummariesResponse responseGajiR = null;
		
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		request.setSumType("1");
		/**
		 * logic tambahan untuk fitur search
		 */
		if (docDateStart != null)
			request.setDateTimeStart(dateFormat.format(docDateStart));
		else if(sp2dno !=null && !sp2dno.equals("")){
			Calendar now = Calendar.getInstance();
			now.add(Calendar.YEAR, -2);
			request.setDateTimeStart(dateFormat.format(now.getTime()));
		}
		else
			request.setDateTimeStart(dateFormat.format(new Date()));
		if (docDateEnd != null)
			request.setDateTimeEnd(dateFormat.format(docDateEnd));
		else
			request.setDateTimeEnd(dateFormat.format(new Date()));
		
		List<SpanSummariesOutput> output = new ArrayList<SpanSummariesOutput>();
		System.out.println(request.toString());
		try {
			response = GetSPANSummaries.getSPANSummaries(request, getSpanDataValidationDaoBean(), getSystemParameterDAOBean(), Constants.GAJI);
			responseGajiR = GetSPANSummaries.getSPANSummaries(request, getSpanDataValidationDaoBean(), getSystemParameterDAOBean(), Constants.GAJIR);
			
			/**
			 * logic tambahan ketika dev span bjb
			 */
			if (responseGajiR.getSPANSummaries() != null && responseGajiR.getSPANSummaries().size() > 0)
				response.getSPANSummaries().addAll(responseGajiR.getSPANSummaries());
			
			validation = response.getSPANSummaries();
			for(SPANSummaries s : validation){
				SpanSummariesOutput ssoutput = new SpanSummariesOutput();
				ssoutput.setFileName(s.getFileName());
				ssoutput.setTotalRecord(s.getTotalRecord());
				if(s.getProcStatus().equals("1")){
					ssoutput.setTotalInProgress("0");
					ssoutput.setTotalWaiting(s.getTotalRecord());
				}else{
					if (s.getTotalRecord() == null)
						s.setTotalRecord("0");
					if (s.getReturACKStatus() == null)
						s.setReturACKStatus("0");
					if (s.getSuccessACKStatus() == null)
						s.setSuccessACKStatus("0");
					if (s.getProcStatus().equals("5") || s.getProcStatus().equals("7")) {
						ssoutput.setTotalInProgress(Integer.parseInt(s.getTotalRecord())
									- Integer.parseInt(s.getReturACKStatus())
									- Integer.parseInt(s.getSuccessACKStatus()) + "");
					}
					else {
						ssoutput.setTotalInProgress(s.getTotalRecord());
					}
					ssoutput.setTotalWaiting("0");
				}
				if(s.getProcStatus().equals("4")||s.getProcStatus().equals("6")){
					ssoutput.setTotalRetry(s.getRetryStatus());
					ssoutput.setTotalRetur(s.getReturStatus());
					ssoutput.setTotalSuccess(s.getSuccessStatus());
				}else{
					ssoutput.setTotalRetry("0");
					ssoutput.setTotalRetur("0");
					ssoutput.setTotalSuccess("0");
				}
				if(s.getProcStatus().equals("5")){
					ssoutput.setTotalAckRetur(s.getReturACKStatus());
					ssoutput.setTotalAckSuccess(s.getSuccessACKStatus());
				}else{
					ssoutput.setTotalAckRetur("0");
					ssoutput.setTotalAckSuccess("0");
				}
				
				output.add(ssoutput);
			}
			//check for sp2no filtering
			if (sp2dno !=null ){
				//if not null querying filenames of data based on sp2dno/doc number
				List<SpanSummariesOutput> filOutput = new ArrayList<SpanSummariesOutput>();
				List listName = getSpanHostDataDetailsDaoBean().getFileNameByDocumentNumber(sp2dno);
				//check, if filename in array output available in listName from query result, add to filtered output
				for (Iterator iterator = output.iterator(); iterator.hasNext();) {
					SpanSummariesOutput object = (SpanSummariesOutput) iterator.next();
					if(listName.contains(object.getFileName().replace(".jar", ""))){
						filOutput.add(object);
					}
					
				}
				//replace output with filtered output
				output = filOutput;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return output;
	}

	private class DetailDataModel extends LazyDataModel < SpanSummariesOutput > {
		private static final long serialVersionUID = 1L;

		public DetailDataModel() {

		}

		@Override
		public List < SpanSummariesOutput > load(int startingAt, int maxPerPage,
		String sortField, SortOrder sortOrder,
		Map < String, String > filters) {
//			SPANDataValidationDao validationDao = (SPANDataValidationDao) applicationBean.getCorpGWContext().getBean("SPANDataValidationDao");
			List < SPANSummaries > validation = new ArrayList<SPANSummaries>();
			
			GetSPANSummariesRequest request = new GetSPANSummariesRequest();
			GetSPANSummariesResponse response = null;
			GetSPANSummariesResponse responseGajiR = null;
			
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
			request.setSumType("1");
			/**
			 * logic tambahan untuk fitur search
			 */
			if (docDateStart != null)
				request.setDateTimeStart(dateFormat.format(docDateStart));
			else
				request.setDateTimeStart(dateFormat.format(new Date()));
			if (docDateEnd != null)
				request.setDateTimeEnd(dateFormat.format(docDateEnd));
			else
				request.setDateTimeEnd(dateFormat.format(new Date()));
			
			List<SpanSummariesOutput> output = new ArrayList<SpanSummariesOutput>();
			try {
				response = GetSPANSummaries.getSPANSummaries(request, getSpanDataValidationDaoBean(), getSystemParameterDAOBean(), Constants.GAJI);
				responseGajiR = GetSPANSummaries.getSPANSummaries(request, getSpanDataValidationDaoBean(), getSystemParameterDAOBean(), Constants.GAJIR);
				
				/**
				 * logic tambahan ketika dev span bjb
				 */
				if (responseGajiR.getSPANSummaries() != null && responseGajiR.getSPANSummaries().size() > 0)
					response.getSPANSummaries().addAll(responseGajiR.getSPANSummaries());
				
				validation = response.getSPANSummaries();
				for(SPANSummaries s : validation){
					SpanSummariesOutput ssoutput = new SpanSummariesOutput();
					ssoutput.setFileName(s.getFileName());
					ssoutput.setTotalRecord(s.getTotalRecord());
					if(s.getProcStatus().equals("1")){
						ssoutput.setTotalInProgress("0");
						ssoutput.setTotalWaiting(s.getTotalRecord());
					}else{
						if (s.getTotalRecord() == null)
							s.setTotalRecord("0");
						if (s.getReturACKStatus() == null)
							s.setReturACKStatus("0");
						if (s.getSuccessACKStatus() == null)
							s.setSuccessACKStatus("0");
						if (s.getProcStatus().equals("5") || s.getProcStatus().equals("7")) {
							ssoutput.setTotalInProgress(Integer.parseInt(s.getTotalRecord())
										- Integer.parseInt(s.getReturACKStatus())
										- Integer.parseInt(s.getSuccessACKStatus()) + "");
						}
						else {
							ssoutput.setTotalInProgress(s.getTotalRecord());
						}
						ssoutput.setTotalWaiting("0");
					}
					if(s.getProcStatus().equals("4")||s.getProcStatus().equals("6")){
						ssoutput.setTotalRetry(s.getRetryStatus());
						ssoutput.setTotalRetur(s.getReturStatus());
						ssoutput.setTotalSuccess(s.getSuccessStatus());
					}else{
						ssoutput.setTotalRetry("0");
						ssoutput.setTotalRetur("0");
						ssoutput.setTotalSuccess("0");
					}
					if(s.getProcStatus().equals("5")){
						ssoutput.setTotalAckRetur(s.getReturACKStatus());
						ssoutput.setTotalAckSuccess(s.getSuccessACKStatus());
					}else{
						ssoutput.setTotalAckRetur("0");
						ssoutput.setTotalAckSuccess("0");
					}
					
					output.add(ssoutput);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			/*if(!getSearchStatus()){
//				validation = validationDao.getAllLazyLoading(maxPerPage + startingAt, startingAt);
//				jmlAll = validationDao.countAllValidation();
			}else{
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				String dateString = "";
				try{
					dateString = sdf.format(docDate);
				}catch(Exception e){
					dateString = "";
				}
				dateString = "%"+dateString+"%";
//				validation = validationDao.searchAllLazyLoading(maxPerPage + startingAt, startingAt, dateString);
//				jmlAll = validationDao.countSearchValidation(dateString);
			}*/
			setRowCount(validation.size());
			setPageSize(maxPerPage);
			return output;
		}
	}
	
	public void doShowDetail(SpanSummariesOutput spanSummariesOutput, String status){
		System.out.println("Show detail summaries.");
		this.status = status;
		SPANDataValidation spdv = getSpanDataValidationDaoBean().getSPANDataValidationByFilenameForSummaryDetail(spanSummariesOutput.getFileName());
		spanDataSummary.setFileName(spanSummariesOutput.getFileName());
		detailFileName = spdv.getFileName();
		detailTotalRecord = spdv.getTotalRecord();
		detailTotalAmount = spdv.getTotalAmount();
		detailType = spdv.getSpanFnType() ;
		detailDebitAccount = spdv.getDebitAccount();
		detailDebitAccountType = spdv.getDebitAccountType();
		detailDocumentDate = spdv.getDocumentDate();
		loadDataDialog();
	}
	
	private void loadDataDialog() {
		setDetailList(new DialogDetailDataModel());
	}

	private class DialogDetailDataModel extends LazyDataModel < DetailsOutputList > {
		private static final long serialVersionUID = 1L;
		
		public DialogDetailDataModel() {

		}
		
		@Override
		public List < DetailsOutputList > load(int startingAt, int maxPerPage, String sortField, SortOrder sortOrder, Map < String, String > filters) {
			GetSPANSummariesDetailRequest request = new GetSPANSummariesDetailRequest();
			request.setFilename(spanDataSummary.getFileName());
			request.setClickOn(status);
			GetSPANSummariesDetailResponse getSPANSummariesDetailResponse = new GetSPANSummariesDetailResponse();
			List<DetailsOutputList> detailsOutputList = new ArrayList<DetailsOutputList>();
			try {
				getSPANSummariesDetailResponse = GetSPANSummariesDetail.getSPANSummariesDetail(request, getSpanDataValidationDaoBean(), getSystemParameterDAOBean(), "GAJI");
				detailsOutputList = getSPANSummariesDetailResponse.getDetailsOutputList();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			int jmlAll = detailsOutputList.size();
			
			List < DetailsOutputList > detailsFinalTemp = new ArrayList<DetailsOutputList>();
			int count = 0;
			for(int i=startingAt; i<detailsOutputList.size(); i++){
				detailsFinalTemp.add(detailsOutputList.get(i));
				count++;
				if(count==maxPerPage){
					break;
				}
			}
			setRowCount(jmlAll);
			setPageSize(maxPerPage);
			
			if(detailsFinalTemp.size()>0){
				return detailsFinalTemp;
			}else{
				return null;
			}
		}
	}
	
	public void loadChart() {
        int random1 = (int)(Math.random() * 1000);
        int random2 = (int)(Math.random() * 1000);
        int random3 = (int)(Math.random() * 1000);
        livePieModel = new PieChartModel();
        livePieModel.set("Candidate 1", random1);
        livePieModel.set("Candidate 2", random2);
        livePieModel.set("Candidate 3", random3);
    }
	
	public void showSearch(){
		setDoShowSearch(true);
	}
	
	public void hideSearch(){
		setDoShowSearch(false);
		
	}
	
	public void showAmount(){
		setDoShowAmount(true);
	}
	
	public void hideAmount(){
		setDoShowAmount(false);
		
	}
	
	public void clearSearch(){
		setDocDateStart(null);
		setDocDateEnd(null);
		setSp2dno("");
		setSearchFilename("");
		setSearchTotalRecord("");
		setSearchTotalRetry("");
		setSearchTotalRetur("");
		setSearchTotalSuccess("");
	}
	
	public void doSearch(){
		setSearchStatus(true);
		resultTemps = null;
//		setSummariesList(new DetailDataModel());
		summaryList = loadSpanSummary(); // not lazy
	}
	
	private void doLoging(String menu, String activity, String information) {
		UserLog userLog = new UserLog();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		userLog.setLogId("LOG-"+sdf.format(new Date())+"-"+sessionBean.getUser().getUserID());
		userLog.setActivity(activity);
		userLog.setInformation(information);
		userLog.setInsertTime(new Date());
		userLog.setMenu(menu);
		userLog.setUserId(sessionBean.getUser().getUserID());
		userLog.setUserName(sessionBean.getUser().getUserName());
		UserLogHandler.doLog(userLog, getUserDAOBean());
	}
	
	private UserDao getUserDAOBean() {
		return (UserDao) applicationBean.getCorpGWContext().getBean("userDAO");
	}

	public void setLivePieModel(PieChartModel livePieModel) {
		this.livePieModel = livePieModel;
	}
	
	public PieChartModel getLivePieModel() {
		return livePieModel;
		}

	public ApplicationBean getApplicationBean() {
		return applicationBean;
	}

	public void setApplicationBean(ApplicationBean applicationBean) {
		this.applicationBean = applicationBean;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public boolean getDoShowSearch() {
		return doShowSearch;
	}

	public void setDoShowSearch(boolean doShowSearch) {
		this.doShowSearch = doShowSearch;
	}

	public Date getDocDate() {
		return docDate;
	}

	public void setDocDate(Date docDate) {
		this.docDate = docDate;
	}

	public String getSearchFilename() {
		return searchFilename;
	}

	public void setSearchFilename(String searchFilename) {
		this.searchFilename = searchFilename;
	}

	public String getSearchTotalRecord() {
		return searchTotalRecord;
	}

	public void setSearchTotalRecord(String searchTotalRecord) {
		this.searchTotalRecord = searchTotalRecord;
	}

	public String getSearchTotalRetry() {
		return searchTotalRetry;
	}

	public void setSearchTotalRetry(String searchTotalRetry) {
		this.searchTotalRetry = searchTotalRetry;
	}

	public String getSearchTotalRetur() {
		return searchTotalRetur;
	}

	public void setSearchTotalRetur(String searchTotalRetur) {
		this.searchTotalRetur = searchTotalRetur;
	}

	public String getSearchTotalSuccess() {
		return searchTotalSuccess;
	}

	public void setSearchTotalSuccess(String searchTotalSuccess) {
		this.searchTotalSuccess = searchTotalSuccess;
	}

	public Boolean getSearchStatus() {
		return searchStatus;
	}

	public void setSearchStatus(Boolean searchStatus) {
		this.searchStatus = searchStatus;
	}

	public LazyDataModel < SpanSummariesOutput > getSummariesList() {
		return summariesList;
	}

	public void setSummariesList(LazyDataModel < SpanSummariesOutput > summariesList) {
		this.summariesList = summariesList;
	}

	public LazyDataModel < DetailsOutputList > getDetailList() {
		return detailList;
	}

	public void setDetailList(LazyDataModel < DetailsOutputList > detailList) {
		this.detailList = detailList;
	}

	public SpanDataSummary getSpanDataSummary() {
		return spanDataSummary;
	}

	public void setSpanDataSummary(SpanDataSummary spanDataSummary) {
		this.spanDataSummary = spanDataSummary;
	}

	public Date getPostingDate() {
		return postingDate;
	}

	public void setPostingDate(Date postingDate) {
		this.postingDate = postingDate;
	}

	public String getPostingGaji() {
		return postingGaji;
	}

	public void setPostingGaji(String postingGaji) {
		this.postingGaji = postingGaji;
	}

	public String getPostingGajir() {
		return postingGajir;
	}

	public void setPostingGajir(String postingGajir) {
		this.postingGajir = postingGajir;
	}

	public Boolean getDoShowAmount() {
		return doShowAmount;
	}

	public void setDoShowAmount(Boolean doShowAmount) {
		this.doShowAmount = doShowAmount;
	}

	public String getDetailFileName() {
		return detailFileName;
	}

	public void setDetailFileName(String detailFileName) {
		this.detailFileName = detailFileName;
	}

	public String getDetailTotalRecord() {
		return detailTotalRecord;
	}

	public void setDetailTotalRecord(String detailTotalRecord) {
		this.detailTotalRecord = detailTotalRecord;
	}

	public String getDetailTotalAmount() {
		return detailTotalAmount;
	}

	public void setDetailTotalAmount(String detailTotalAmount) {
		this.detailTotalAmount = detailTotalAmount;
	}

	public String getDetailType() {
		return detailType;
	}

	public void setDetailType(String detailType) {
		this.detailType = detailType;
	}

	public String getDetailDebitAccount() {
		return detailDebitAccount;
	}

	public void setDetailDebitAccount(String detailDebitAccount) {
		this.detailDebitAccount = detailDebitAccount;
	}

	public String getDetailDebitAccountType() {
		return detailDebitAccountType;
	}

	public void setDetailDebitAccountType(String detailDebitAccountType) {
		this.detailDebitAccountType = detailDebitAccountType;
	}

	public String getDetailDocumentDate() {
		return detailDocumentDate;
	}

	public void setDetailDocumentDate(String detailDocumentDate) {
		this.detailDocumentDate = detailDocumentDate;
	}

	public Date getDocDateStart() {
		return docDateStart;
	}

	public void setDocDateStart(Date docDateStart) {
		this.docDateStart = docDateStart;
	}

	public Date getDocDateEnd() {
		return docDateEnd;
	}

	public void setDocDateEnd(Date docDateEnd) {
		this.docDateEnd = docDateEnd;
	}

	public List<SpanSummariesOutput> getSummaryList() {
		return summaryList;
	}

	public void setSummaryList(List<SpanSummariesOutput> summaryList) {
		this.summaryList = summaryList;
	}

	public String getSp2dno() {
		return sp2dno;
	}

	public void setSp2dno(String sp2dno) {
		this.sp2dno = sp2dno;
	}
	
}