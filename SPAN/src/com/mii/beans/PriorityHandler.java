package com.mii.beans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.primefaces.context.RequestContext;

import com.mii.dao.SpanPriorityDao;
import com.mii.models.SpanPriority;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.SessionBean;

@ManagedBean
@ViewScoped
public class PriorityHandler {

	private SpanPriority sp;

	private boolean isCreate;

	public boolean isCreate() {
		return isCreate;
	}

	public void setCreate(boolean isCreate) {
		this.isCreate = isCreate;
	}

	List<SpanPriority> dataList = new ArrayList<>();

	@ManagedProperty(value = "#{applicationBean}")
	private ApplicationBean applicationBean;
	@ManagedProperty(value = "#{sessionBean}")
	private SessionBean sessionBean;

	public SpanPriority getSp() {
		return sp;
	}

	public void setSp(SpanPriority sp) {
		this.sp = sp;
	}

	@PostConstruct
	public void init(){
		dataList = getSpanPriorityDao().getAllData();
	}

	public SpanPriorityDao getSpanPriorityDao(){
		return (SpanPriorityDao) applicationBean.getCorpGWContext().getBean("spanPriorityDao");
	}

	public List<SpanPriority> getDataList() {
		return dataList;
	}

	public void setDataList(List<SpanPriority> dataList) {
		this.dataList = dataList;
	}

	public ApplicationBean getApplicationBean() {
		return applicationBean;
	}

	public void setApplicationBean(ApplicationBean applicationBean) {
		this.applicationBean = applicationBean;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public void deleteSP(SpanPriority sp){
		String user = getSessionBean().getUser().getUserName();
		getSpanPriorityDao().updateDeleteFlag(sp.getSp2dno(),user);
		init();
	}

	public void editSP(SpanPriority sp){
		this.sp = sp;
		RequestContext.getCurrentInstance().execute("insertOrEditPriority.show()");
	}

	public void newPriority(){
		this.sp = new SpanPriority();
		this.isCreate = true;
	}

	public void doSave(){
		if(isCreate){
			String user = getSessionBean().getUser().getUserName();
			getSpanPriorityDao().insertNew(sp, user);
			RequestContext.getCurrentInstance().execute("insertOrEditPriority.hide()");
			isCreate = false;
			init();
		}else{
			String user = getSessionBean().getUser().getUserName();
			getSpanPriorityDao().updateNew(sp, user);
			RequestContext.getCurrentInstance().execute("insertOrEditPriority.hide()");
			isCreate = false;
			init();
		}
	}
}
