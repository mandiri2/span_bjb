package com.mii.beans;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.apache.commons.lang.SerializationUtils;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.mii.constant.Constants;
import com.mii.helpers.FacesUtil;
import com.mii.invoker.DatabaseParameter;
import com.mii.invoker.SpanDataInvokeSOA;
import com.mii.invoker.WMInvoker;
import com.mii.models.SpanDataReportMonitoring;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.SessionBean;
import com.wm.data.IData;
import com.wm.data.IDataCursor;
import com.wm.data.IDataFactory;
import com.wm.data.IDataUtil;

@ManagedBean
@ViewScoped
public class ResendFileDepkeuHandler implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private Boolean doShowACK;
	private Boolean doShowBS;
	private Boolean doShowSORBOR;
	private String tabSelected; //ACK-BS-SORBOR
	
	private Date searchDate;
	private String fileName;
	private String accountValue;
	private String radioChecked = "date";
	
	private SpanDataReportMonitoring spanDataReportMonitoring;
	
	private LazyDataModel < SpanDataReportMonitoring > reportList;
	private static Map < String, SpanDataReportMonitoring > resultTemps = new HashMap < String, SpanDataReportMonitoring > ();
	
	@ManagedProperty(value="#{applicationBean}")
	private ApplicationBean applicationBean;
	
	@ManagedProperty(value="#{sessionBean}")
	private SessionBean sessionBean;
	
	@PostConstruct
	public void init(){
		FacesUtil.authMenu(getSessionBean().getUser(), "resendFileDepkeu");
		doClickACK();
	}
	
	public void doClickACK() {
		setDoShowACK(true);
		setDoShowBS(false);
		setDoShowSORBOR(false);
		setTabSelected(Constants.ACK);
		resultTemps.clear();
		searchDate=null;
	}
	
	public void doClickBS() {
		setDoShowACK(false);
		setDoShowBS(true);
		setDoShowSORBOR(false);
		setTabSelected(Constants.BS);
		resultTemps.clear();
		searchDate=null;
	}
	
	public void doClickSORBOR() {
		setDoShowACK(false);
		setDoShowBS(false);
		setDoShowSORBOR(true);
		setTabSelected(Constants.SORBOR);
		resultTemps.clear();
		searchDate=null;
	}
	
	public void processChecked(SpanDataReportMonitoring result) {
		setSpanDataReportMonitoring((SpanDataReportMonitoring) SerializationUtils.clone(result));
		if (result.isSelected()) {
			if(tabSelected.equals(Constants.ACK)){
				getResultTemps().put(result.getACK_fileName(), result);
			}else if(tabSelected.equals(Constants.BS)){
				getResultTemps().put(result.getBS_fileName(), result);
			}else if(tabSelected.equals(Constants.SORBOR)){
				getResultTemps().put(result.getSORBOR_fileName(), result);
			}
		} else {
			if(tabSelected.equals(Constants.ACK)){
				getResultTemps().remove(result.getACK_fileName());
			}else if(tabSelected.equals(Constants.BS)){
				getResultTemps().remove(result.getBS_fileName());
			}else if(tabSelected.equals(Constants.SORBOR)){
				getResultTemps().remove(result.getSORBOR_fileName());
			}
		}
	}

	
	public void doSearch(){
		setReportList(new DetailDataModel());
	}
	
	private class DetailDataModel extends LazyDataModel < SpanDataReportMonitoring > {
		private static final long serialVersionUID = 1L;

		public DetailDataModel() {
			
		}
		
		@Override
		public List < SpanDataReportMonitoring > load(int startingAt, int maxPerPage,
		String sortField, SortOrder sortOrder,
		Map < String, String > filters) {
			if(tabSelected.equalsIgnoreCase(Constants.ACK)){
				return getReportACK(startingAt, maxPerPage);
			}else if(tabSelected.equalsIgnoreCase(Constants.BS)){
				return getReportBS(startingAt, maxPerPage);
			}else if(tabSelected.equalsIgnoreCase(Constants.SORBOR)){
				return getReportSORBOR(startingAt, maxPerPage);
			}
			return null;
		}

		@SuppressWarnings("rawtypes")
		private List<SpanDataReportMonitoring> getReportSORBOR(int startingAt, int maxPerPage) {
			//SORBOR//
			List < SpanDataReportMonitoring > spanDataReportMonitoringList = new ArrayList<SpanDataReportMonitoring>();
			Integer jmlAll = 0;
			IData output = IDataFactory.create();
			try{
				// Input to WebMethod
				IData input = IDataFactory.create();
				IDataCursor inputCursor = input.getCursor();
				IDataUtil.put( inputCursor, "tabSelected", tabSelected);
				IDataUtil.put( inputCursor, "dateInput", validateDate(searchDate));
				inputCursor.destroy();
				SpanDataInvokeSOA sdi = DatabaseParameter.getInstance().getSpanDataInvokeSOA();
				// Output from WebMethod
				output=(IData)WMInvoker.doInvoke(sdi.getIp(),sdi.getUsername(),sdi.getPassword(),"mandiri.span.service.report", "reportMonitoring", input);
				IDataCursor outputCursor = output.getCursor();
				IData[]	reportData = IDataUtil.getIDataArray( outputCursor, "reportMonitoringList" );
				if ( reportData != null)
				{
					jmlAll = reportData.length;
					for ( int i = startingAt; i < reportData.length; i++ )
					{
							IDataCursor SpanReportMonitoringCursor = reportData[i].getCursor();
							SpanDataReportMonitoring spr=new SpanDataReportMonitoring();
							spr.setSORBOR_fileName(IDataUtil.getString( SpanReportMonitoringCursor, "SORBOR_fileName"));
							spr.setSORBOR_totalRecord(IDataUtil.getString( SpanReportMonitoringCursor, "SORBOR_totalRecord"));
							spr.setSORBOR_totalAmount(IDataUtil.getString( SpanReportMonitoringCursor, "SORBOR_totalAmount"));
							spr.setSORBOR_dateSent(IDataUtil.getString( SpanReportMonitoringCursor, "SORBOR_datesent"));
							spr.setSORBOR_status(IDataUtil.getString( SpanReportMonitoringCursor, "SORBOR_status"));
							SpanReportMonitoringCursor.destroy();
							spanDataReportMonitoringList.add(spr);
					}
				}
			    outputCursor.destroy();
			}catch( Exception e){
				e.printStackTrace();
			}
			setRowCount(jmlAll);
			setPageSize(maxPerPage);
			List < SpanDataReportMonitoring > spanDataReportMonitoringListFinal = new ArrayList < SpanDataReportMonitoring > ();
			for (SpanDataReportMonitoring a: spanDataReportMonitoringList) {
				Iterator entries = resultTemps.entrySet().iterator();
				while (entries.hasNext()) {
					Entry thisEntry = (Entry) entries.next();
					SpanDataReportMonitoring s = (SpanDataReportMonitoring) thisEntry.getValue();
					if (a.getSORBOR_fileName().equals(s.getSORBOR_fileName())) {
						if (s.isSelected()) {
							a.setSelected(true);
						} else {
							a.setSelected(false);
						}
					}
				}
				spanDataReportMonitoringListFinal.add(a);
			}
			if(spanDataReportMonitoringListFinal.size()>0){
				return spanDataReportMonitoringListFinal;
			}else{
				return null;
			}
		}

		@SuppressWarnings("rawtypes")
		private List<SpanDataReportMonitoring> getReportBS(int startingAt, int maxPerPage) {
			//BS//
			List < SpanDataReportMonitoring > spanDataReportMonitoringList = new ArrayList<SpanDataReportMonitoring>();
			Integer jmlAll = 0;
			IData output = IDataFactory.create();
			try{
				// Input to WebMethod
				IData input = IDataFactory.create();
				IDataCursor inputCursor = input.getCursor();
				IDataUtil.put( inputCursor, "tabSelected", tabSelected);
				IDataUtil.put( inputCursor, "dateInput", validateDate(searchDate));
				IDataUtil.put( inputCursor, "account", accountValue);
				inputCursor.destroy();
				SpanDataInvokeSOA sdi = DatabaseParameter.getInstance().getSpanDataInvokeSOA();
				// Output from WebMethod
				output=(IData)WMInvoker.doInvoke(sdi.getIp(),sdi.getUsername(),sdi.getPassword(),"mandiri.span.service.report", "reportMonitoring", input);
				IDataCursor outputCursor = output.getCursor();
				IData[]	reportData = IDataUtil.getIDataArray( outputCursor, "reportMonitoringList" );
				if ( reportData != null)
				{
					jmlAll = reportData.length;
					for ( int i = startingAt; i < reportData.length; i++ )
					{
							IDataCursor SpanReportMonitoringCursor = reportData[i].getCursor();
							SpanDataReportMonitoring spr=new SpanDataReportMonitoring();
							spr.setBS_account(IDataUtil.getString( SpanReportMonitoringCursor, "BS_account"));
							spr.setBS_fileName(IDataUtil.getString( SpanReportMonitoringCursor, "BS_fileName"));
							spr.setBS_status(IDataUtil.getString(SpanReportMonitoringCursor, "BS_status"));
							SpanReportMonitoringCursor.destroy();
							spanDataReportMonitoringList.add(spr);
					}
				}
			    outputCursor.destroy();
			}catch( Exception e){
				e.printStackTrace();
			}
			setRowCount(jmlAll);
			setPageSize(maxPerPage);
			List < SpanDataReportMonitoring > spanDataReportMonitoringListFinal = new ArrayList < SpanDataReportMonitoring > ();
			for (SpanDataReportMonitoring a: spanDataReportMonitoringList) {
				Iterator entries = resultTemps.entrySet().iterator();
				while (entries.hasNext()) {
					Entry thisEntry = (Entry) entries.next();
					SpanDataReportMonitoring s = (SpanDataReportMonitoring) thisEntry.getValue();
					if (a.getBS_fileName().equals(s.getBS_fileName())) {
						if (s.isSelected()) {
							a.setSelected(true);
						} else {
							a.setSelected(false);
						}
					}
				}
				spanDataReportMonitoringListFinal.add(a);
			}
			if(spanDataReportMonitoringListFinal.size()>0){
				return spanDataReportMonitoringListFinal;
			}else{
				return null;
			}
		}
		
		@SuppressWarnings("rawtypes")
		private List<SpanDataReportMonitoring> getReportACK(int startingAt, int maxPerPage) {
			//ACK//
			List < SpanDataReportMonitoring > spanDataReportMonitoringList = new ArrayList<SpanDataReportMonitoring>();
			Integer jmlAll = 0;
			IData output = IDataFactory.create();
			try{
				// Input to WebMethod
				IData input = IDataFactory.create();
				IDataCursor inputCursor = input.getCursor();
				IDataUtil.put( inputCursor, "tabSelected", tabSelected);
				IDataUtil.put( inputCursor, "dateInput", validateDate(searchDate));
				IDataUtil.put( inputCursor, "fileName", fileName);
				IDataUtil.put( inputCursor, "radioChecked", radioChecked);
				inputCursor.destroy();
				SpanDataInvokeSOA sdi = DatabaseParameter.getInstance().getSpanDataInvokeSOA();
				// Output from WebMethod
				output=(IData)WMInvoker.doInvoke(sdi.getIp(),sdi.getUsername(),sdi.getPassword(),"mandiri.span.service.report", "reportMonitoring", input);
				IDataCursor outputCursor = output.getCursor();
				IData[]	reportData = IDataUtil.getIDataArray( outputCursor, "reportMonitoringList" );
				if ( reportData != null){
					jmlAll = reportData.length;
					for ( int i = startingAt; i < reportData.length; i++ ){
						IDataCursor SpanReportMonitoringCursor = reportData[i].getCursor();
						SpanDataReportMonitoring spr=new SpanDataReportMonitoring();
						spr.setACK_fileName(IDataUtil.getString( SpanReportMonitoringCursor, "ACK_fileName"));
						spr.setACK_ACKFilename(IDataUtil.getString( SpanReportMonitoringCursor, "ACK_ACKFilename"));
						spr.setACK_totalRecord(IDataUtil.getString( SpanReportMonitoringCursor, "ACK_totalRecord"));
						spr.setACK_totalAmount(IDataUtil.getString( SpanReportMonitoringCursor, "ACK_totalAmount"));
						spr.setACK_dateSent(IDataUtil.getString( SpanReportMonitoringCursor, "ACK_dateSent"));
						spr.setACK_status(IDataUtil.getString( SpanReportMonitoringCursor, "ACK_status"));
						SpanReportMonitoringCursor.destroy();
						spanDataReportMonitoringList.add(spr);
					}
				}
			    outputCursor.destroy();
			}catch( Exception e){
				e.printStackTrace();
			}
			setRowCount(jmlAll);
			setPageSize(maxPerPage);
			List < SpanDataReportMonitoring > spanDataReportMonitoringListFinal = new ArrayList < SpanDataReportMonitoring > ();
			for (SpanDataReportMonitoring a: spanDataReportMonitoringList) {
				Iterator entries = resultTemps.entrySet().iterator();
				while (entries.hasNext()) {
					Entry thisEntry = (Entry) entries.next();
					SpanDataReportMonitoring s = (SpanDataReportMonitoring) thisEntry.getValue();
					if (a.getACK_fileName().equals(s.getACK_fileName())) {
						if (s.isSelected()) {
							a.setSelected(true);
						} else {
							a.setSelected(false);
						}
					}
				}
				spanDataReportMonitoringListFinal.add(a);
			}
			if(spanDataReportMonitoringListFinal.size()>0){
				return spanDataReportMonitoringListFinal;
			}else{
				return null;
			}
		}
	}
	
	public void resendAll(){
		System.out.println("resendAll executed");
	}
	
	public void resendChecked(){
		System.out.println("resendChecked executed");
		String result = resendFile(tabSelected, "");
		System.out.println("Result : "+result);
	}
	
	private String resendFile(String tabSelected, String fileName){
		String message = "";
		try{
			//PrepareInput
			IData input = IDataFactory.create();
			IDataCursor inputCursor = input.getCursor();
			IDataUtil.put( inputCursor, "tabSelected", tabSelected);
			IDataUtil.put( inputCursor, "fileName", fileName );
			inputCursor.destroy();
			//PrepareOutput
			IData 	output = IDataFactory.create();
			SpanDataInvokeSOA sdi = DatabaseParameter.getInstance().getSpanDataInvokeSOA();
			output=(IData)WMInvoker.doInvoke(sdi.getIp(),sdi.getUsername(),sdi.getPassword(),"mandiri.span.service.report", "resendFile", input);
			IDataCursor outputCursor = output.getCursor();
			//String	status = IDataUtil.getString( outputCursor, "status" );
			message = IDataUtil.getString( outputCursor, "message" );
			outputCursor.destroy();
		}catch(Exception e){
			e.printStackTrace();
		}
		return message;
	}
	
	public String validateDate(Date docDate){
		String iDate = "";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try{
			if(docDate!=null){
				iDate = sdf.format(docDate);
			}else{
				iDate = sdf.format(new Date());
			}
		}catch(Exception e){
			iDate = sdf.format(new Date());
		}
		return iDate;
	}
	
	public Boolean getDoShowACK() {
		return doShowACK;
	}

	public void setDoShowACK(Boolean doShowACK) {
		this.doShowACK = doShowACK;
	}

	public Boolean getDoShowBS() {
		return doShowBS;
	}

	public void setDoShowBS(Boolean doShowBS) {
		this.doShowBS = doShowBS;
	}

	public Boolean getDoShowSORBOR() {
		return doShowSORBOR;
	}

	public void setDoShowSORBOR(Boolean doShowSORBOR) {
		this.doShowSORBOR = doShowSORBOR;
	}

	public String getTabSelected() {
		return tabSelected;
	}

	public void setTabSelected(String tabSelected) {
		this.tabSelected = tabSelected;
	}

	public ApplicationBean getApplicationBean() {
		return applicationBean;
	}

	public void setApplicationBean(ApplicationBean applicationBean) {
		this.applicationBean = applicationBean;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public LazyDataModel < SpanDataReportMonitoring > getReportList() {
		return reportList;
	}

	public void setReportList(LazyDataModel < SpanDataReportMonitoring > reportList) {
		this.reportList = reportList;
	}

	public Date getSearchDate() {
		return searchDate;
	}

	public void setSearchDate(Date searchDate) {
		this.searchDate = searchDate;
	}

	public static Map < String, SpanDataReportMonitoring > getResultTemps() {
		return resultTemps;
	}

	public static void setResultTemps(Map < String, SpanDataReportMonitoring > resultTemps) {
		ResendFileDepkeuHandler.resultTemps = resultTemps;
	}

	public SpanDataReportMonitoring getSpanDataReportMonitoring() {
		return spanDataReportMonitoring;
	}

	public void setSpanDataReportMonitoring(SpanDataReportMonitoring spanDataReportMonitoring) {
		this.spanDataReportMonitoring = spanDataReportMonitoring;
	}
	
}
