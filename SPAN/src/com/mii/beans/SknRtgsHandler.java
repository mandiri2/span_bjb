package com.mii.beans;

import java.io.BufferedInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.NumberToTextConverter;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

import com.mii.constant.Constants;
import com.mii.constant.ConstantsUserLog;
import com.mii.dao.SPANHostDataDetailsDao;
import com.mii.dao.SknRtgsDao;
import com.mii.dao.SorborAttrDao;
import com.mii.dao.SystemParameterDao;
import com.mii.dao.UserDao;
import com.mii.helpers.FTPHelper;
import com.mii.helpers.FacesUtil;
import com.mii.is.util.PubUtil;
import com.mii.models.SknRtgs;
import com.mii.models.SorborAttr;
import com.mii.models.SystemParameter;
import com.mii.models.UserLog;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.SessionBean;
import com.mii.sknrtgs.ACKDataDocument;
import com.mii.sknrtgs.ACKSknRtgs;
import com.mii.sknrtgs.MappingDataACKSpanRequest;

@ManagedBean
@ViewScoped
public class SknRtgsHandler implements Serializable{
	private static final long serialVersionUID = 1L;

	private SknRtgs sknRtgs;
	private SknRtgs retur;
	private SknRtgs sknRtgsTemp;
	
	private String returRemittanceNo;
	private String searchMessage;
	private boolean doShowSearchMessage;
	private boolean doShowUpdateRetur;
	private boolean doShowReturBtn;
	
	private String action;
	private String docNo;
	private String creditAccName;
	private String creditAccNo;
	private String beneficiaryBankName;
	private String creditTrfAmount;
	private String remittanceNo;
	private String sorborNo;
	private String transactionType;
	
	
	private String returReason;
	
	private boolean doShowUpdateMessage;
	private String updateMessage;
	
	private UploadedFile fileUpload;
	private int count=0;
	private List<String> listError=new ArrayList<String>();

	private boolean inputRefNo;

	private LazyDataModel<SknRtgs> sknRtgsList;
	private List<SknRtgs> sknRtgsBulkList=new ArrayList<SknRtgs>();
	
	@ManagedProperty(value = "#{applicationBean}")
	private ApplicationBean applicationBean;

	@ManagedProperty(value = "#{sessionBean}")
	private SessionBean sessionBean;

	private boolean doShowSearch = false;
	
	private boolean doBulkShow = false;

	private StreamedContent fileContoh;

	private String host = null;
	private String username = null;
	private String password = null;
	private String spanPath = null; 
	private String localPath = null;
	private String backupLocalPath = null;
	private String bankName = null;
	private String bankCode = null;
	
	
	@PostConstruct
	public void init() {
		FacesUtil.authMenu(sessionBean.getUser(), "sknRtgs");
		
		sknRtgsList = new SknRtgsDataModel();
		
		creditAccName="";
		creditAccNo="";
		beneficiaryBankName="";
		creditTrfAmount="";
		remittanceNo="";
		sorborNo="";
		docNo="";
		transactionType="";
		doShowUpdateMessage = false;
		updateMessage = "";
		sknRtgs = new SknRtgs();
		constructFTPParameter();
		doLoging(ConstantsUserLog.MENU_SKNRTGS, ConstantsUserLog.ACTIVITY_INIT, "Menu Init.");
	}
	
	private void constructFTPParameter() {
		host = getSystemParameterDAOBean().getDetailedParameterByParamName(Constants.SPAN_DEPKEU_SFTP_IPADDRESS).getParamValue();
		username = getSystemParameterDAOBean().getDetailedParameterByParamName(Constants.SPAN_DEPKEU_SFTP_USERNAME).getParamValue();
		password = getSystemParameterDAOBean().getDetailedParameterByParamName(Constants.SPAN_DEPKEU_SFTP_PASSWORD).getParamValue();
		spanPath = getSystemParameterDAOBean().getDetailedParameterByParamName(Constants.SPAN_DEPKEU_SFTP_PUT_SORBOR_REMOTE_DIR).getParamValue();
		localPath = getSystemParameterDAOBean().getDetailedParameterByParamName(Constants.SPAN_CREATE_SORBOR_PATH).getParamValue();
		backupLocalPath = getSystemParameterDAOBean().getDetailedParameterByParamName(Constants.JAR_BACKUP_SORBOR).getParamValue();
		bankName = getSystemParameterDAOBean().getDetailedParameterByParamName(Constants.SPAN_BANK_NAME).getParamValue();
		bankCode = getSystemParameterDAOBean().getDetailedParameterByParamName(Constants.SPAN_BANK_CODE).getParamValue();
	}

	public void doViewAndUpdate(){
		RequestContext.getCurrentInstance().execute("persistRemittanceNumberBulk.show()");
	}
	
	public void initDialogRetur(){
		returRemittanceNo = "";
		retur = null;
		setDoShowUpdateRetur(false);
		setDoShowSearchMessage(false);
		setDoShowReturBtn(false);
	}
	
	public void searchRemittanceNo(){
		if(!returRemittanceNo.equals("")&returRemittanceNo!=null){
			searchMessage = "";
			SknRtgsDao sknRtgsDao = getSknRtgsDAOBean();
			this.retur = sknRtgsDao.getDetailedSknRtgsByRemittanceNo(returRemittanceNo);
			try{
				this.retur.setReturReasonMsg(checkReturCode(this.retur.getReturReason()));
			}catch(Exception e){}
			setDoShowReturBtn(true);
			try{
				if(retur.getDocNo().equals("")||retur.getDocNo()==null){
					searchMessage="Transaction Not Found";
					setDoShowReturBtn(false);
					setDoShowSearchMessage(true);
				}else if(sknRtgsDao.isAlreadyRetured(returRemittanceNo)!=null){
					searchMessage="This Transaction Is Already Retured";
					setDoShowReturBtn(false);
					setDoShowSearchMessage(true);
				}
			}catch(Exception e){
				searchMessage="Transaction Not Found";
				setDoShowReturBtn(false);
				setDoShowSearchMessage(true);
			}
		}else{
			setDoShowSearchMessage(true);
			retur = null;
			searchMessage = "*Remittance Number can't be null*";
			setDoShowReturBtn(false);
			setDoShowSearchMessage(true);
		}
		
	}

	private String checkReturCode(String errorCode) {
		try{
			if(errorCode.equals("95")){
				return "SALAH NO REKENING";
			}else{
				return "SALAH NAMA";
			}
		}catch(Exception e){
			return "";
		}
	}
	
	public void doExtract(){
		sknRtgsBulkList.clear();
		listError.clear();
		if (!(fileUpload.getFileName().equals(null)||fileUpload.getFileName().equals(""))) {
			String fileName = fileUpload.getFileName();
			String extension = fileName.substring(fileName.lastIndexOf("."));
			if (StringUtils.equalsIgnoreCase(extension, ".xls")) {
				if(fileUpload.getSize()<=5000000){
					try{
						extractFile();
						if(listError.size()>0){
							FacesUtil.setFatalMessage(null,listError.get(0), listError.get(0));
						}else{
							System.out.println("Size : "+sknRtgsBulkList.size());
							resetCount(0);
							fileUpload.getInputstream().close();
							String bodyMessage="Data Uploaded,Please View Data Before Submit";
							FacesUtil.setInfoMessage(null, bodyMessage, bodyMessage);
						}
					}catch(Exception e){
				    	String bodyMessage ="";
				    	if(this.count>0){
				    		bodyMessage="Got Error on file row number "+count+", "+e.getMessage();
				    	}else{
				    		bodyMessage=e.getMessage();
				    	}
				    	System.out.println(bodyMessage);
						FacesUtil.setFatalMessage(null,bodyMessage, bodyMessage);
						resetCount(0);
				    }
				}else{
					FacesUtil.setFatalMessage(null, "Maximum file size is 1MB", null);
				}
			}else{
				FacesUtil.setFatalMessage(null, fileUpload.getFileName()+" is not .xls type", null);
			}
		}else{
			FacesUtil.setFatalMessage(null, "File is required", null);
		}
	}
	
	private void resetCount(int value) {
		this.count=value;
	}
	
	private void extractFile() throws IOException {
		BufferedInputStream input = new BufferedInputStream(this.fileUpload.getInputstream());
		POIFSFileSystem fs = new POIFSFileSystem( input );
		HSSFWorkbook wb = new HSSFWorkbook(fs);
		HSSFSheet sheet = wb.getSheetAt(0);
		int i=0;
		for (Row myrow : sheet) {
			if(i>0){
				SknRtgs sknRtgs= new SknRtgs();
				sknRtgs=extractCell(myrow);
				sknRtgsBulkList.add(sknRtgs);
				if(listError.size()>0){
					sknRtgsBulkList.clear();
					System.out.println("HERE ADA ERROR");
					System.out.println(listError.get(0));
					break;
				}
			}
			
			i++;
			count++;
		}
		
		wb.close();
		
	}
	
	private SknRtgs extractCell(Row myrow) {
		SknRtgs sknRtgs = new SknRtgs();
		int cellCounter=0;
		for (Cell mycell : myrow) {
				switch (cellCounter) {
				case 0:
					sknRtgs.setDocNo(getValueCell(mycell));
					break;
				case 1:
					sknRtgs.setCreditAccName(getValueCell(mycell));
					break;
				case 2:
					sknRtgs.setCreditAccNo(getValueCell(mycell));
					break;
				case 3:
					sknRtgs.setBeneficiaryBankName(getValueCell(mycell));
					break;
				case 4:
					System.out.println(getValueCell(mycell));
					sknRtgs.setTransactionType(getValueCell(mycell));
					break;
				case 5:
					sknRtgs.setCreditTrfAmount(getValueCell(mycell));
					break;
				case 6:
					sknRtgs.setRemittanceNo(getValueCell(mycell));
					break;
				case 7:
					sknRtgs.setsorborNo(getValueCell(mycell));
					break;
				}
				cellCounter++;
			
			}
		
			return sknRtgs;
		}
	
	
	private String getValueCell(Cell mycell) {
		String value="";
		switch (mycell.getCellType()) {
		case Cell.CELL_TYPE_NUMERIC:
			value=NumberToTextConverter.toText(mycell.getNumericCellValue());
			break;
		case Cell.CELL_TYPE_STRING:
			value=String.valueOf(mycell.getStringCellValue());
			break;
		case Cell.CELL_TYPE_BLANK:
			listError.add("Error at Row "+count+ " And Cell "+mycell.getColumnIndex()+" is Required");
			value="";
			break;
		default:
			listError.add("Error at Row "+count+ " And Cell "+mycell.getColumnIndex()+" Undefined");
			value="";
			break;
			
		}
		return value;
	}
	
	public void doSearch() {
		sknRtgsList = new SknRtgsDataModel();
//		FacesUtil.resetPage("transactionForm");
	}
	
	public void downloadContoh(){
		InputStream stream = FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("/resources/template/BulkSKNRTGSTemplate.xls");
		fileContoh=new DefaultStreamedContent(stream, "application/xls", "BulkSKNRTGSTemplate.xls");
	}
	
	public void doSave() {
		SknRtgsDao sknRtgsDao = getSknRtgsDAOBean();
		boolean isAlreadyProcessed = sknRtgsDao.isAlreadyProcessed(this.sknRtgs.getDocNo());
		if(!isAlreadyProcessed){
//			if(sknRtgs.getsorborNo().equals("")||sknRtgs.getRemittanceNo().equals("")){
			if(sknRtgs.getsorborNo().equals("")){
//				String message = "Update Failed,Please Insert Sorbor And Remittance Number";
//				FacesUtil.setInfoMessage(null, message, message);
				doShowUpdateMessage = true;
				updateMessage = "Sorbor Number Can't be null.";
			}else{
				if(sknRtgs.getRemittanceNo().equals("")||sknRtgs.getRemittanceNo()==null){
					doShowUpdateMessage = true;
					updateMessage = "Remittance Number Can't be null.";
				}else{
					doShowUpdateMessage = false;
					updateMessage = "";
					sknRtgsDao.updateSknRtgs(this.sknRtgs);
					String fileName = createSorborFile(this.sknRtgs.getDocNo());
					String jarFileName = fileName.replace("SBR", "jar");
							PubUtil.concatString(localPath, fileName.replace("SBR", "jar"));
//					boolean createdJar = createJar(PubUtil.concatString(localPath, fileName), PubUtil.concatString(localPath, jarFileName));
					boolean createdJar = createJar(PubUtil.concatString(localPath, fileName),fileName);
					if(createdJar){
						String status = sendFtp(jarFileName); //FIXME Enable on Dev
	//					String status = PubUtil.byPassString("true", "*************BYPASS PLEASE ENABLED sendFTP()*************"); //bypass
						if("true".equalsIgnoreCase(status)){
							PubUtil.moveFile(jarFileName, localPath, backupLocalPath);
							String headerMessage = "Success";
							String bodyMessage = "Success send SORBOR file.";
							FacesUtil.setInfoMessageDialog(headerMessage, bodyMessage);
							doLoging(ConstantsUserLog.MENU_SKNRTGS, ConstantsUserLog.ACTIVITY_RTBP_EXECUTE, bodyMessage.concat(" : ").concat(this.sknRtgs.getDocNo()));
						}
						else{
							System.out.println("FAILED TO SEND JAR TO FTP. Filename : "+jarFileName);
							String headerMessage = "Failed To Send";
							String bodyMessage = "Failed To Send SORBOR file. Filename : "+jarFileName;
							FacesUtil.setErrorMessageDialog(headerMessage, bodyMessage);
							doLoging(ConstantsUserLog.MENU_SKNRTGS, ConstantsUserLog.ACTIVITY_RTBP_EXECUTE, bodyMessage.concat(" : ").concat(this.sknRtgs.getDocNo()));
						}
					}
					else{
						System.out.println("FAILED TO CREATE JAR. Filename : "+fileName);
						String headerMessage = "Failed To Create Jar";
						String bodyMessage = "Failed To Create JAR file. Filename : "+fileName;
						FacesUtil.setErrorMessageDialog(headerMessage, bodyMessage);
						doLoging(ConstantsUserLog.MENU_SKNRTGS, ConstantsUserLog.ACTIVITY_RTBP_EXECUTE, bodyMessage.concat(" : ").concat(this.sknRtgs.getDocNo()));
					}
					sknRtgsList=new SknRtgsDataModel();
					RequestContext.getCurrentInstance().execute("persistRemitanceNumber.hide()");
				}
			}
		}
	}
	
	public void doRetur(){
		
		try{
			SknRtgsDao sknRtgsDao = getSknRtgsDAOBean();
			sknRtgsDao.updateReturFlag(returRemittanceNo, returReason);
			System.out.println("Retur executed");
			ACKDataDocument doc = new ACKDataDocument();
			//TODO get from database
			doc = getSknRtgsDAOBean().selectACKDataDocument(this.retur.getDocNo());
//			doc.setFileName("520008000990_SP2D_O_20161010_000002_VIT.jar");
//			doc.setDocumentType("SP2D");
//			doc.setDocumentDate("2016-10-27");
			doc.setDocumentNo(this.retur.getDocNo());
			doc.setReturnCode(returReason);
			doc.setDescription(checkReturCode(returReason));
			
			/*String oriFN = doc.getFileName();
			int rf = doc.getFileName().indexOf("RF");
			String convertedFN = "";
			if (rf >= 0) {
				convertedFN = getSPANHostDataDetailDAOBean().selectOriginalFileName(oriFN);
				if (!"".equalsIgnoreCase(convertedFN) && convertedFN != null) {
					file.setFileName(convertedFN);
				} else {
					// do nothing
				}
			}*/
			
			MappingDataACKSpanRequest request = new MappingDataACKSpanRequest();
			request.setAckDataDocument(doc);
			try {
				ACKSknRtgs ack = new ACKSknRtgs();
				ack.createACKSknRtgs(request, getSystemParameterDAOBean());
				doLoging(ConstantsUserLog.MENU_SKNRTGS, ConstantsUserLog.ACTIVITY_RTBP_EXECUTE, "SKNRTGS Retur".concat(" : ").concat(this.retur.getDocNo()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}catch(Exception ex){
			ex.printStackTrace();
			
		}
	}
	
	/*private boolean createJar(String targetFile, String target) {
		try {
			PubUtil.runCreateJar(targetFile, target);
			return true;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}*/
	
    private boolean createJar(String sorborFile, String fileName) {
		PubUtil.runJarWithoutManifest(sorborFile,fileName);
		//PubUtil.runCreateJar(targetFile, target);
		return true;
	}

	private String sendFtp(String fileName) {
		boolean status = FTPHelper.uploadFileFTP(host, username, password, spanPath, localPath, fileName);
		return String.valueOf(status);
	}

	public String createSorborFile(String docNo){
		String fullPath = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		String bankName="BJB";
		String tanggalFile=sdf.format(new Date());
		String seqFile = "";
		String ext=".SBR";
		String filename = null;

		
		boolean overWrite=true;
		
		SorborAttrDao sorborAttrDao = getSorborAttrDAOBean();
		SorborAttr sorborAttr=sorborAttrDao.getSorborAttrById(docNo);
		try{
			seqFile=padLeft(String.valueOf(sorborAttrDao.getNextValFileSeq()),3,"0"); 
			filename = PubUtil.concatString(bankName, "_",tanggalFile,"_",seqFile,ext);
			fullPath = localPath.concat(filename);
			
			FileWriter fileWriter = new FileWriter(fullPath,overWrite);
			PrintWriter printWriter = new PrintWriter(fileWriter);
			printWriter.print(createSorborLine(sorborAttr));
			printWriter.close();
			System.out.println("here");
			
		}catch(Exception e){
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		return filename;
	}
	
	public static String padLeft(String originalString, int length,
	         String padCharacter) {
	      String paddedString = originalString;
	      while (paddedString.length() < length) {
	         paddedString = padCharacter + paddedString;
	      }
	      return paddedString;
	   }

	
	public String createSorborLine(SorborAttr sorborAttr){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return PubUtil.concatString(sorborAttr.getBankCode(),"|",
									sdf.format(sorborAttr.getPaymentDate()),"|",
									sorborAttr.getDocNo(),"|",
									String.valueOf(sorborAttr.getCheckAmount()),"|",
									sorborAttr.getFtpFileName(),"|",
									sorborAttr.getPaymentMethods(),"|",
									sdf.format(sorborAttr.getSorborDate()),"|",
									sorborAttr.getSorborNo());
	}
	
	public void doBulkSave() {
		List<String> listDocNo=new ArrayList<String>();
		SknRtgsDao sknRtgsDao = getSknRtgsDAOBean();
		String docNoLog = "";
		for(SknRtgs sknRtgs:sknRtgsBulkList){
			try{
				boolean isAlreadyProcessed = sknRtgsDao.isAlreadyProcessed(sknRtgs.getDocNo());
				if(!isAlreadyProcessed){
					sknRtgsDao.updateSknRtgs(sknRtgs);
					listDocNo.add(sknRtgs.getDocNo());
					docNoLog = docNoLog.concat(sknRtgs.getDocNo()).concat(". ");
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
			}
		}
		List<String> filenames = createSorborFile(listDocNo);
		List<String> jarFileNames = new ArrayList<String>();
		for(String fileName : filenames){
			String jarFileName = fileName.replace("SBR", "jar");
//			boolean createdJar = createJar(PubUtil.concatString(localPath, fileName), PubUtil.concatString(localPath, jarFileName));
			boolean createdJar = createJar(PubUtil.concatString(localPath, fileName),fileName);
			if(createdJar){
				jarFileNames.add(jarFileName);
			}
		}
		for(String jarFileName : jarFileNames){
			String status = sendFtp(jarFileName);
//			String status = PubUtil.byPassString("true", "*************BYPASS PLEASE ENABLED sendFTP BULK*************"); //bypass
			if("true".equalsIgnoreCase(status)){
				PubUtil.moveFile(jarFileName, localPath, backupLocalPath);
				doLoging(ConstantsUserLog.MENU_SKNRTGS, ConstantsUserLog.ACTIVITY_RTBP_EXECUTE, "Execute Bulk".concat(" : ").concat(docNoLog));
			}
			else{
				System.out.println("FAILED TO SEND JAR TO FTP");
				doLoging(ConstantsUserLog.MENU_SKNRTGS, ConstantsUserLog.ACTIVITY_RTBP_EXECUTE, "Execute Bulk".concat(" : ").concat(docNoLog).concat(". Failed Send Jar to FTP."));
			}
		}
		sknRtgsBulkList.clear();
		sknRtgsList=new SknRtgsDataModel();
		RequestContext.getCurrentInstance().execute("persistRemittanceNumberBulk.hide()");
	}
	
	
	public List<String> createSorborFile(List<String> listDocNo){
		List<String> filenames = new ArrayList<String>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		String tanggalFile=sdf.format(new Date());
		String seqFile = "";
		String ext=Constants.SORBOREXT;
		
		boolean overWrite=true;
		
		SorborAttrDao sorborAttrDao = getSorborAttrDAOBean();
		List<SorborAttr> listSorborAttr=sorborAttrDao.getListSorborAttr(listDocNo);
		int listSorborAttrSize =listSorborAttr.size();
		try{
			FileWriter fileWriter=null;
			PrintWriter printWriter=null;
			for (int i=0; i<listSorborAttrSize; i++){
				if(i%100==0){
					if(printWriter!=null){
						printWriter.close();
					}
					seqFile=padLeft(String.valueOf(sorborAttrDao.getNextValFileSeq()),3,"0"); 
					String filename = PubUtil.concatString(bankName, "_",tanggalFile,"_",seqFile,ext);
					String fullPath =localPath.concat(filename);
					filenames.add(filename);
					fileWriter = new FileWriter(fullPath,overWrite);
				}
				SorborAttr sorborAttr = listSorborAttr.get(i);
				sorborAttr.setBankCode(bankCode);
				printWriter = new PrintWriter(fileWriter);
				printWriter.println(createSorborLine(sorborAttr));
			}	
			printWriter.close();
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return filenames;
	}
	
	public void viewDetailedSknRtgs(SknRtgs sknRtgs) {
		SknRtgsDao sknRtgsDao = getSknRtgsDAOBean();
		this.sknRtgs = sknRtgsDao.getDetailedSknRtgsById(sknRtgs.getDocNo(),sknRtgs.getCreditAccNo());
		doShowUpdateMessage = false;
		updateMessage = "";
//		RequestContext.getCurrentInstance().execute(
//				"persistRemitanceNumber.show()");
	}
	
	private SknRtgsDao getSknRtgsDAOBean() {
		return (SknRtgsDao) applicationBean.getCorpGWContext().getBean("sknRtgsDao");
	}
	
	private SorborAttrDao getSorborAttrDAOBean() {
		return (SorborAttrDao) applicationBean.getCorpGWContext().getBean("sorborAttrDao");
	}
	
	private SPANHostDataDetailsDao getSPANHostDataDetailDAOBean() {
		return (SPANHostDataDetailsDao) applicationBean.getCorpGWContext().getBean("SPANHostDataDetailsDao");
	}
	
	public void showSearch() {
		doShowSearch = true;
	}
	
	public LazyDataModel<SknRtgs> getSknRtgsList() {
		return sknRtgsList;
	}


	public void setSknRtgsList(LazyDataModel<SknRtgs> sknRtgsList) {
		this.sknRtgsList = sknRtgsList;
	}


	public void hideSearch() {
		doShowSearch = false;
	}
	
	public void hideBulk() {
		doBulkShow = false;
	}
	
	public void showBulk() {
		doBulkShow = true;
	}
	
	public String getCreditAccName() {
		return creditAccName;
	}



	public void setCreditAccName(String creditAccName) {
		this.creditAccName = creditAccName;
	}





	private class SknRtgsDataModel extends LazyDataModel<SknRtgs> {
		private static final long serialVersionUID = 1L;

		public SknRtgsDataModel(){
			
		}
		
		@Override
		public List<SknRtgs> load(int startingAt, int maxPerPage,
				String sortField, SortOrder sortOrder,
				Map<String, String> filters) {

			SknRtgsDao sknRtgsDao = (SknRtgsDao) applicationBean
					.getCorpGWContext().getBean("sknRtgsDao");
			List<String> blackListPM = new ArrayList<String>();
			SystemParameter sysparam = getSystemParameterDAOBean().getDetailedParameterByParamName("SCH_RUN_FLAG_AUTO_OB_SORBOR");
			if(sysparam.getParamValue().equals("1")){
				blackListPM.add("0");
			}else{
				blackListPM.add("X");
			}
			List<SknRtgs> sknRtgsListModel = sknRtgsDao.getSearchSknRtgsLazyLoading(docNo,creditAccName, creditAccNo,beneficiaryBankName,creditTrfAmount,
					remittanceNo,sorborNo,transactionType, blackListPM, maxPerPage+startingAt, startingAt);
			
			Integer jmlAll = sknRtgsDao.countSearchSknRtgs(docNo,creditAccName, creditAccNo,beneficiaryBankName,creditTrfAmount,
					remittanceNo,sorborNo,transactionType, blackListPM);
			setRowCount(jmlAll);
			setPageSize(maxPerPage);

			return sknRtgsListModel;
		}

	}
	
	private SystemParameterDao getSystemParameterDAOBean(){
		return (SystemParameterDao) applicationBean.getCorpGWContext().getBean("systemParameterDAO");
	}
	
	public void downloadXls(){
		String fileName="SknRtgs.xls";
		SknRtgsDao sknRtgsDao = (SknRtgsDao) applicationBean.getCorpGWContext().getBean("sknRtgsDao");
		List<String> blackListPM = new ArrayList<String>();
		SystemParameter sysparam = getSystemParameterDAOBean().getDetailedParameterByParamName("SCH_RUN_FLAG_AUTO_OB_SORBOR");
		if(sysparam.getParamValue().equals("1")){
			blackListPM.add("0");
		}else{
			blackListPM.add("X");
		}
		Integer jmlAll = sknRtgsDao.countSearchSknRtgs(docNo,creditAccName, creditAccNo,beneficiaryBankName,creditTrfAmount,
				remittanceNo,sorborNo,transactionType, blackListPM);
		List listData = sknRtgsDao.getSearchSknRtgsLazyLoading(docNo,creditAccName, creditAccNo,beneficiaryBankName,creditTrfAmount,
				remittanceNo,sorborNo,transactionType, blackListPM, jmlAll, 0);
		
		String jasperPath = FacesUtil.getExternalContext().getRealPath("resources/report/SknRtgs.jasper");
    	PubUtil.generateReport(fileName, jasperPath, Constants.EXCEL, null, Constants.CONTENT_TYPE_XLS,listData);
	}
	
	
	
	
	private void doLoging(String menu, String activity, String information) {
		UserLog userLog = new UserLog();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		userLog.setLogId("LOG-"+sdf.format(new Date())+"-"+sessionBean.getUser().getUserID());
		userLog.setActivity(activity);
		userLog.setInformation(information);
		userLog.setInsertTime(new Date());
		userLog.setMenu(menu);
		userLog.setUserId(sessionBean.getUser().getUserID());
		userLog.setUserName(sessionBean.getUser().getUserName());
		UserLogHandler.doLog(userLog, getUserDAOBean());
	}
	
	private UserDao getUserDAOBean() {
		return (UserDao) applicationBean.getCorpGWContext().getBean("userDAO");
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSpanPath() {
		return spanPath;
	}

	public void setSpanPath(String spanPath) {
		this.spanPath = spanPath;
	}

	public String getLocalPath() {
		return localPath;
	}

	public void setLocalPath(String localPath) {
		this.localPath = localPath;
	}

	public SknRtgs getSknRtgs() {
		return sknRtgs;
	}

	public void setSknRtgs(SknRtgs sknRtgs) {
		this.sknRtgs = sknRtgs;
	}

	public SknRtgs getSknRtgsTemp() {
		return sknRtgsTemp;
	}

	public void setSknRtgsTemp(SknRtgs sknRtgsTemp) {
		this.sknRtgsTemp = sknRtgsTemp;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getCreditAccNo() {
		return creditAccNo;
	}

	public void setCreditAccNo(String creditAccNo) {
		this.creditAccNo = creditAccNo;
	}

	public String getBeneficiaryBankName() {
		return beneficiaryBankName;
	}

	public void setBeneficiaryBankName(String beneficiaryBankName) {
		this.beneficiaryBankName = beneficiaryBankName;
	}

	public String getCreditTrfAmount() {
		return creditTrfAmount;
	}

	public void setCreditTrfAmount(String creditTrfAmount) {
		this.creditTrfAmount = creditTrfAmount;
	}

	public String getRemittanceNo() {
		return remittanceNo;
	}

	public void setRemittanceNo(String remittanceNo) {
		this.remittanceNo = remittanceNo;
	}

	public String getSorborNo() {
		return sorborNo;
	}

	public void setSorborNo(String sorborNo) {
		this.sorborNo = sorborNo;
	}

	public boolean isInputRefNo() {
		return inputRefNo;
	}

	public void setInputRefNo(boolean inputRefNo) {
		this.inputRefNo = inputRefNo;
	}

	public ApplicationBean getApplicationBean() {
		return applicationBean;
	}

	public void setApplicationBean(ApplicationBean applicationBean) {
		this.applicationBean = applicationBean;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public boolean isDoShowSearch() {
		return doShowSearch;
	}

	public void setDoShowSearch(boolean doShowSearch) {
		this.doShowSearch = doShowSearch;
	}

	public StreamedContent getFileContoh() {
		return fileContoh;
	}


	public void setFileContoh(StreamedContent fileContoh) {
		this.fileContoh = fileContoh;
	}


	public boolean isDoBulkShow() {
		return doBulkShow;
	}


	public void setDoBulkShow(boolean doBulkShow) {
		this.doBulkShow = doBulkShow;
	}


	public UploadedFile getFileUpload() {
		return fileUpload;
	}


	public void setFileUpload(UploadedFile fileUpload) {
		this.fileUpload = fileUpload;
	}


	public List<SknRtgs> getSknRtgsBulkList() {
		return sknRtgsBulkList;
	}


	public void setSknRtgsBulkList(List<SknRtgs> sknRtgsBulkList) {
		this.sknRtgsBulkList = sknRtgsBulkList;
	}


	public List<String> getListError() {
		return listError;
	}


	public void setListError(List<String> listError) {
		this.listError = listError;
	}

	public String getDocNo() {
		return docNo;
	}

	public void setDocNo(String docNo) {
		this.docNo = docNo;
	}

	public String getBackupLocalPath() {
		return backupLocalPath;
	}

	public void setBackupLocalPath(String backupLocalPath) {
		this.backupLocalPath = backupLocalPath;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType=transactionType;
	}

	public boolean isDoShowUpdateMessage() {
		return doShowUpdateMessage;
	}

	public void setDoShowUpdateMessage(boolean doShowUpdateMessage) {
		this.doShowUpdateMessage = doShowUpdateMessage;
	}

	public String getUpdateMessage() {
		return updateMessage;
	}

	public void setUpdateMessage(String updateMessage) {
		this.updateMessage = updateMessage;
	}

	

	public String getReturReason() {
		return returReason;
	}

	public void setReturReason(String returReason) {
		this.returReason = returReason;
	}

	public SknRtgs getRetur() {
		return retur;
	}

	public void setRetur(SknRtgs retur) {
		this.retur = retur;
	}

	public String getReturRemittanceNo() {
		return returRemittanceNo;
	}

	public void setReturRemittanceNo(String returRemittanceNo) {
		this.returRemittanceNo = returRemittanceNo;
	}

	public boolean isDoShowSearchMessage() {
		return doShowSearchMessage;
	}

	public void setDoShowSearchMessage(boolean doShowSearchMessage) {
		this.doShowSearchMessage = doShowSearchMessage;
	}

	public String getSearchMessage() {
		return searchMessage;
	}

	public void setSearchMessage(String searchMessage) {
		this.searchMessage = searchMessage;
	}

	public boolean isDoShowUpdateRetur() {
		return doShowUpdateRetur;
	}

	public void setDoShowUpdateRetur(boolean doShowUpdateRetur) {
		this.doShowUpdateRetur = doShowUpdateRetur;
	}

	

	public boolean isDoShowReturBtn() {
		return doShowReturBtn;
	}

	public void setDoShowReturBtn(boolean doShowReturBtn) {
		this.doShowReturBtn = doShowReturBtn;
	}

	


}
