package com.mii.beans;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import com.mii.constant.Constants;
import com.mii.dao.SystemParameterDao;
import com.mii.scoped.ApplicationBean;
import com.mii.test.FileUtils;
import com.mii.test.JsfUtil;
import com.mii.test.downloadableFile;

@ManagedBean
@ViewScoped
public class DownloadFileHandler {

	private static final int DEFAULT_BUFFER_SIZE = 10240;
	private String dir;
	private String specialName;
	private String specialExtension;

	private String folder;

	public String getFolder() {
		return folder;
	}

	public void setFolder(String folder) {
		this.folder = folder;
	}

	public String toDownload(){
		return "download.jsf";
	}

	private String sort = "date";
	private String sortOrder = "desc";

	public String getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	@ManagedProperty(value="#{applicationBean}")
	private ApplicationBean applicationBean;

	public ApplicationBean getApplicationBean() {
		return applicationBean;
	}

	public void setApplicationBean(ApplicationBean applicationBean) {
		this.applicationBean = applicationBean;
	}

	private SystemParameterDao getSystemParameterDAOBean(){
		return (SystemParameterDao) applicationBean.getCorpGWContext().getBean("systemParameterDAO");
	}

	private String lookupDir(String dirCode){
		if(dirCode.equals(Constants.BS_DIR)){
			folder = "BS";
			return getSystemParameterDAOBean().getDetailedParameterByParamName(dirCode).getParamValue();
		} else if(dirCode.equals(Constants.SORBOR_DIR)){
			folder = "SBR";
			return getSystemParameterDAOBean().getDetailedParameterByParamName(dirCode).getParamValue();
		} else if(dirCode.equals(Constants.INBOUND_DIR)){
			folder = "INB";
			return getSystemParameterDAOBean().getDetailedParameterByParamName(dirCode).getParamValue();
		} else return null;
	}

	private String lookupLike(String likeCode){
		if(likeCode.equals(Constants.BS_LIKE)){
			return getSystemParameterDAOBean().getDetailedParameterByParamName(likeCode).getParamValue();
		} else if(likeCode.equals(Constants.SORBOR_LIKE)){
			return getSystemParameterDAOBean().getDetailedParameterByParamName(likeCode).getParamValue();
		} else if(likeCode.equals(Constants.INBOUND_LIKE)){
			return getSystemParameterDAOBean().getDetailedParameterByParamName(likeCode).getParamValue();
		} else return null;
	}

	private String lookupExt(String extCode){
		if(extCode.equals(Constants.BS_EXT)){
			return getSystemParameterDAOBean().getDetailedParameterByParamName(extCode).getParamValue();
		} else if(extCode.equals(Constants.SORBOR_EXT)){
			return getSystemParameterDAOBean().getDetailedParameterByParamName(extCode).getParamValue();
		} else if(extCode.equals(Constants.INBOUND_EXT)){
			return getSystemParameterDAOBean().getDetailedParameterByParamName(extCode).getParamValue();
		} else return null;
	}

	public String getSpecialName() {
		return specialName;
	}

	public void setSpecialName(String specialName) {
		this.specialName = specialName;
	}

	public String getSpecialExtension() {
		return specialExtension;
	}

	public void setSpecialExtension(String specialExtension) {
		this.specialExtension = specialExtension;
	}

	public String getDir() {
		return dir;
	}

	public void setDir(String dir) {
		this.dir = dir;
	}

	public List<downloadableFile> getFiles() {
		return files;
	}

	public void setFiles(List<downloadableFile> files) {
		this.files = files;
	}

	private List<downloadableFile> files = new ArrayList<downloadableFile>();

	@PostConstruct
	public void init(){
		System.out.println("INIT");
		generatePage();
	}

	public void generatePage(){
		if(JsfUtil.getRequestParameter("dl")!=null){
			if(JsfUtil.getRequestParameter("dl").equals("0")){
				if(JsfUtil.getRequestParameter("dir")!=null){
					dir = lookupDir(JsfUtil.getRequestParameter("dir"));
					System.out.println(dir);
				} else {
					dir = null;
					specialExtension = null;
					specialName = null;
					files = new ArrayList<>();
					return;
				}
				if(JsfUtil.getRequestParameter("like")!=null){
					specialName = lookupLike(JsfUtil.getRequestParameter("like"));
					System.out.println(specialName);
				}
				if(JsfUtil.getRequestParameter("ext")!=null){
					specialExtension = lookupExt(JsfUtil.getRequestParameter("ext"));
					System.out.println(specialExtension);
				}
				if(dir!=null){
					populateFiles();
				}
			} else if(JsfUtil.getRequestParameter("dl").equals("1")){
				if(JsfUtil.getRequestParameter("dir")!=null){
					dir = JsfUtil.getRequestParameter("dir");
					System.out.println(dir);
				} else {
					dir = null;
					specialExtension = null;
					specialName = null;
					files = new ArrayList<>();
					return;
				}
				if(JsfUtil.getRequestParameter("like")!=null){
					specialName = JsfUtil.getRequestParameter("like");
					System.out.println(specialName);
				}
				if(JsfUtil.getRequestParameter("ext")!=null){
					specialExtension = JsfUtil.getRequestParameter("ext");
					System.out.println(specialExtension);
				}
				if(dir!=null){
					populateFiles();
				}
			}
		} else {
			//FIXME default page
			if(true){
				dir = lookupDir((String) Constants.BS_DIR);
				specialExtension = lookupExt((String) Constants.BS_EXT);
				specialName = lookupLike((String) Constants.BS_LIKE);
				if(dir!=null){
					populateFiles();
				}
			}
			if(files.isEmpty()){
				dir = null;
				specialExtension = null;
				specialName = null;
				files = new ArrayList<>();
				return;
			}
		}
	}

	public boolean isNull(String in){
		try{
			if(in.equals(null)){
				return true;
			}else if(in.equals("")){
				return true;
			}else if(in.equals("null")){
				return true;
			}else return false;
		}catch(Exception e){
			return true;
		}
	}

	public void populateFiles(){
		System.out.println("populate files with : ");
		if((!isNull(specialExtension)) && (!isNull(specialName))){
			System.out.println(specialExtension+" "+specialName);
			files = FileUtils.getFiles(dir, specialName, specialExtension);
			sortFiles();
		} else if((!isNull(specialExtension))&&(isNull(specialName))){
			System.out.println(specialExtension);
			files = FileUtils.getFiles(dir, specialExtension);
			sortFiles();
		} else {
			files = FileUtils.getFiles(dir);
			sortFiles();
		}
	}

	public String lookupPath(String filename){
		System.out.println("files list : "+files.size());
		for (Iterator iterator = files.iterator(); iterator.hasNext();) {
			downloadableFile downloadableFile = (downloadableFile) iterator.next();
			System.out.println(downloadableFile.getName()+" -> "+downloadableFile.getAbsolutePath());
			if(downloadableFile.getName().equalsIgnoreCase(filename)){
				return downloadableFile.getAbsolutePath();
			}
		}
		return null;
	}

	public void sortFiles(){

		if(sort.equals("name")){
			if(sortOrder.equals("asc")){
				Collections.sort(files, new Comparator<downloadableFile>() {

					@Override
					public int compare(downloadableFile o1, downloadableFile o2) {
						return o1.getName().compareTo(o2.getName());
					}
				});
			}else if(sortOrder.equals("desc")){
				Collections.sort(files, new Comparator<downloadableFile>() {

					@Override
					public int compare(downloadableFile o1, downloadableFile o2) {
						return o2.getName().compareTo(o1.getName());
					}
				});
			}
		}else if(sort.equals("date")){
			if(sortOrder.equals("asc")){
				Collections.sort(files, new Comparator<downloadableFile>() {

					@Override
					public int compare(downloadableFile o1, downloadableFile o2) {
						return o1.getModifiedDate().compareTo(o2.getModifiedDate());
					}
				});
			}
			else if(sortOrder.equals("desc")){
				Collections.sort(files, new Comparator<downloadableFile>() {

					@Override
					public int compare(downloadableFile o1, downloadableFile o2) {
						return o2.getModifiedDate().compareTo(o1.getModifiedDate());
					}

				});
			}
		}
	}

	public void downloadFile(){
		if(files.isEmpty()){
			System.out.println("generate page for download");
			System.out.println("DIR : "+dir);
			System.out.println("Special : "+specialName+" "+specialExtension);
			generatePage();
		}
		System.out.println("DOWNLOADING");
		String filename = JsfUtil.getRequestParameter("filename");
		System.out.println("Downloading : "+filename);
		// Access the resource to be sent
		ServletContext ctx = (ServletContext) FacesContext.getCurrentInstance()
				.getExternalContext().getContext();
		HttpServletResponse response = (HttpServletResponse) FacesContext
				.getCurrentInstance().getExternalContext().getResponse();
		//get absolute path
		String path = lookupPath(filename);
		System.out.println(path);
		//check if file exists
		File file = new File(path);  
		if (!file.exists()) { 
			try{
				System.out.println("file doesnt exist.");
				response.sendError(HttpServletResponse.SC_NOT_FOUND);  
			}catch(Exception e){
				System.out.println("response error");
				e.printStackTrace();
			}
			return;  
		}  else{
			//InputStream fis = ctx.getResourceAsStream(path);
			//prepare response
			response.reset();  
			response.setBufferSize(DEFAULT_BUFFER_SIZE);  
			response.setContentType("application/octet-stream");  
			response.setHeader("Content-Length", String.valueOf(file.length()));  
			response.setHeader("Content-Disposition", "attachment;filename=\""+ file.getName() + "\"");  
			//response.setHeader("Content-Disposition", "attachment;filename="+filename);
			BufferedInputStream input = null;  
			BufferedOutputStream output = null;  
			try {  
				input = new BufferedInputStream(new FileInputStream(file),  
						DEFAULT_BUFFER_SIZE);  
				output = new BufferedOutputStream(response.getOutputStream(),  
						DEFAULT_BUFFER_SIZE);  
				byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];  
				int length;  
				while ((length = input.read(buffer)) > 0) {  
					output.write(buffer, 0, length);  
				}  
			}catch(Exception e){
				e.printStackTrace();
			}finally { 
				try{
					input.close();  
					output.close();  
				}catch(Exception e){
					e.printStackTrace();
				}
			}  
			FacesContext.getCurrentInstance().responseComplete();
		} 
	}
}
