package com.mii.beans;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JExcelApiExporterParameter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;

import org.apache.commons.lang.SerializationUtils;
import org.apache.commons.lang.StringUtils;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.mii.constant.Constants;
import com.mii.constant.ConstantsUserLog;
import com.mii.dao.SPANDataValidationDao;
import com.mii.dao.SystemParameterDao;
import com.mii.dao.ToBeProcessDao;
import com.mii.dao.UserDao;
import com.mii.helpers.FacesUtil;
import com.mii.is.GetSPANSummaries;
import com.mii.is.GetSPANSummariesDetail;
import com.mii.is.ToBeProcess;
import com.mii.is.io.GetSPANSummariesDetailRequest;
import com.mii.is.io.GetSPANSummariesDetailResponse;
import com.mii.is.io.GetSPANSummariesRequest;
import com.mii.is.io.GetSPANSummariesResponse;
import com.mii.is.io.ToBeProcessInput;
import com.mii.is.io.ToBeProcessOutput;
import com.mii.is.io.helper.DetailsOutputList;
import com.mii.is.io.helper.DownloadErrorResponse;
import com.mii.is.io.helper.Files;
import com.mii.is.io.helper.OutputDetails;
import com.mii.is.util.PubUtil;
import com.mii.json.AccountBalance;
import com.mii.models.SPANDataValidation;
import com.mii.models.SpanDataSummary;
import com.mii.models.SpanDataSummaryDetails;
import com.mii.models.SystemParameter;
import com.mii.models.UserLog;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.SessionBean;

@ManagedBean
@ViewScoped
public class resultToBeProcessHandler implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private SpanDataSummary spanDataSummary;
	private SpanDataSummaryDetails spanDataSummaryDetails;
	private SpanDataSummary spanDataSummaryTemp;
	private LazyDataModel < SpanDataSummary > summaryList;
	private LazyDataModel < DetailsOutputList > detailList;
	private Boolean doShowGAJI;
	private Boolean doShowGAJIR;
	private Boolean doShowRetry;
	private Boolean doShowRetur;
	private Boolean doShowSendACK;
	private String tabSelected; //GAJI - GAJIR
	
	private Boolean doShowDownloadFile;
	private String typeOfProcess;
	private String formatOfFile;
	private Boolean doShowPDFXLS;
	
	private String availableBalance;
	private String accountNumber;
	private String totalRecords;
	private String selectedAmount;
	
	@ManagedProperty(value="#{applicationBean}")
	private ApplicationBean applicationBean;
	
	@ManagedProperty(value="#{sessionBean}")
	private SessionBean sessionBean;
	
	private String detailFileName;
	private String detailTotalRecord;
	private String detailTotalAmount;
	private String detailType;
	private String detailDebitAccount;
	private String detailDebitAccountType;
	private String detailDocumentDate;
	
//	public static Map < BigDecimal, SPANHostDataDetails > resultTemps = new HashMap < BigDecimal, SPANHostDataDetails > ();
	public static Map < String, SpanDataSummary > resultTemps = new HashMap < String, SpanDataSummary > ();
	public static Map < String, DetailsOutputList > resultTempsDetails = new HashMap < String, DetailsOutputList > ();
	public static Map < String, SpanDataSummary > resultTempsACK = new HashMap < String, SpanDataSummary > ();
	
	@PostConstruct
	public void init(){
		FacesUtil.authMenu(sessionBean.getUser(), "resultToBeProcess");
		doClickGaji();
		formatOfFile = "2";
	}

	public void doClickGaji(){
		doShowGAJI = true;
		doShowGAJIR = false;
		tabSelected = Constants.GAJI;
		initChangeTab();
		doLoging(ConstantsUserLog.MENU_RESULT_TO_BE_PROCESS, tabSelected.concat(" ").
				concat(ConstantsUserLog.ACTIVITY_RTBP_RETRY).concat(" ").
				concat(ConstantsUserLog.ACTIVITY_INIT), "Tab "+tabSelected+" RETRY Init.");
	}

	public void doClickGajiR(){
		doShowGAJI = false;
		doShowGAJIR = true;
		tabSelected = Constants.GAJIR;
		initChangeTab();
		doLoging(ConstantsUserLog.MENU_RESULT_TO_BE_PROCESS, tabSelected.concat(" ").
				concat(ConstantsUserLog.ACTIVITY_RTBP_RETRY).concat(" ").
				concat(ConstantsUserLog.ACTIVITY_INIT), "Tab "+tabSelected+" RETRY Init.");
	}

	private void initChangeTab() {
		resultTemps.clear();
		setSummaryList(null);
		totalRecords = "0";
		selectedAmount = "0.00";
		availableBalance = null;
		accountNumber = null;
		doShowDownloadFile = false;
		secondaryTabInit();
	}
	
	private void secondaryTabInit() {
		doShowRetry = true;
		doShowRetur = false;
		doShowSendACK = false;
		loadData();
	}
	
	public void doClickRetry(){
		doShowRetry = true;
		doShowRetur = false;
		doShowSendACK = false;
		resultTemps.clear();
		loadData();
		totalRecords = "0";
		selectedAmount = "0.00";
		availableBalance = null;
		accountNumber = null;
		doShowDownloadFile = false;
		doLoging(ConstantsUserLog.MENU_RESULT_TO_BE_PROCESS, tabSelected.concat(" ").
				concat(ConstantsUserLog.ACTIVITY_RTBP_RETRY).concat(" ").
				concat(ConstantsUserLog.ACTIVITY_INIT), "Tab "+tabSelected+" RETRY Init.");
	}
	
	public void doClickRetur(){
		doShowRetry = false;
		doShowRetur = true;
		doShowSendACK = false;
		resultTemps.clear();
		loadData();
		totalRecords = "0";
		selectedAmount = "0.00";
		availableBalance = null;
		accountNumber = null;
		doShowDownloadFile = false;
		doLoging(ConstantsUserLog.MENU_RESULT_TO_BE_PROCESS, tabSelected.concat(" ").
				concat(ConstantsUserLog.ACTIVITY_RTBP_RETUR).concat(" ").
				concat(ConstantsUserLog.ACTIVITY_INIT), "Tab "+tabSelected+" RETUR Init.");
	}
	
	private String hiddenSelectedAmount = "0";
	
	public void doClickSendACK(){
		doShowRetry = false;
		doShowRetur = false;
		doShowSendACK = true;
		resultTemps.clear();
		loadData();
		totalRecords = "0";
		selectedAmount = "0.00";
		hiddenSelectedAmount = "0";
		availableBalance = null;
		accountNumber = null;
		doShowDownloadFile = false;
		doShowPDFXLS = false;
		resultTempsACK.clear();
		doLoging(ConstantsUserLog.MENU_RESULT_TO_BE_PROCESS, tabSelected.concat(" ").
				concat(ConstantsUserLog.ACTIVITY_RTBP_SEND_ACK).concat(" ").
				concat(ConstantsUserLog.ACTIVITY_INIT), "Tab "+tabSelected+" SEND ACK Init.");
	}
	
	public void processChecked(SpanDataSummary result) {
		SpanDataSummary r = new SpanDataSummary();
		r = (SpanDataSummary) SerializationUtils.clone(result);
		if (result.isSelected()) {
			resultTemps.put(result.getFileName(), result);
			try{
				totalRecords = String.valueOf((Integer.parseInt(totalRecords) + Integer.parseInt(result.getTotalRecord())));
//				selectedAmount = String.format("%.2f", ((Float.parseFloat(selectedAmount) + Float.parseFloat(result.getTotalAmount()))));
				DecimalFormat df = new DecimalFormat("###,###.00");
				BigInteger sum = new BigInteger(hiddenSelectedAmount);
				sum = sum.add(new BigInteger(result.getTotalAmount()));
				selectedAmount = (df.format(sum));
				hiddenSelectedAmount = sum.toString();
				if(selectedAmount.equals(".00")){
					selectedAmount = "0.00";
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		} else {
			resultTemps.remove(result.getFileName());
			try{
				totalRecords = String.valueOf((Integer.parseInt(totalRecords) - Integer.parseInt(result.getTotalRecord())));
//				selectedAmount = String.format("%.2f", ((Float.parseFloat(selectedAmount) - Float.parseFloat(result.getTotalAmount()))));
				DecimalFormat df = new DecimalFormat("###,###.00");
				BigInteger sum = new BigInteger(hiddenSelectedAmount);
				sum = sum.subtract(new BigInteger(result.getTotalAmount()));
				selectedAmount = (df.format(sum));
				hiddenSelectedAmount = sum.toString();
				if(selectedAmount.equals(".00")){
					selectedAmount = "0.00";
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		if(doShowSendACK){
			if(result.getTotalAmount().equals("0")||result.getTotalAmount().equals("")||
					result.getTotalAmount()==null||result.getTotalAmount().equals("0.00")||result.getTotalAmount()==null){
				if (result.isSelected()) {
					resultTempsACK.put(result.getFileName(), result);
				}else{
					resultTempsACK.remove(result.getFileName());
				}
			}
			if(resultTempsACK.size()>0){
				doShowDownloadFile = true;
			}else{
				doShowDownloadFile = false;
			}
		}
	}
	
	public void processCheckedDetail(DetailsOutputList result) {
		DetailsOutputList r = new DetailsOutputList();
		r = (DetailsOutputList) SerializationUtils.clone(result);
		if (result.isSelected()) {
			resultTempsDetails.put(result.getDocNumber(), result);
//			totalRecords = String.valueOf((Integer.parseInt(totalRecords) + 1));
//			selectedAmount = String.format("%.2f", ((Float.parseFloat(selectedAmount) + Float.parseFloat(result.getDebitAmount()))));
		} else {
			resultTempsDetails.remove(result.getDocNumber());
//			totalRecords = String.valueOf((Integer.parseInt(totalRecords) - 1));
//			selectedAmount = String.format("%.2f", ((Float.parseFloat(selectedAmount) - Float.parseFloat(result.getDebitAmount()))));
		}
	}
	
	public void doShowDetail(SpanDataSummary spanDataSummary){
		SystemParameterDao parameterDao = (SystemParameterDao) applicationBean.getCorpGWContext().getBean("systemParameterDAO");
		SystemParameter parameter = parameterDao.getDetailedParameterByParamName(Constants.PARAM_DEBITACCNO+tabSelected);	
		spanDataSummaryDetails = new SpanDataSummaryDetails();
		spanDataSummaryDetails.setDebitAccount(parameter.getParamValue());
		spanDataSummaryDetails.setDebitAccountType(getTabSelected());
		SPANDataValidation spdv = getSpanDataValidationDaoBean().getSPANDataValidationByFilenameForSummaryDetail(spanDataSummary.getFileName());
		detailFileName = spdv.getFileName();
		detailTotalRecord = spdv.getTotalRecord();
		detailTotalAmount = spdv.getTotalAmount();
		detailType = spdv.getSpanFnType() ;
		detailDebitAccount = spdv.getDebitAccount();
		detailDebitAccountType = spdv.getDebitAccountType();
		detailDocumentDate = spdv.getDocumentDate();
		loadDataDialog();
		this.spanDataSummary = spanDataSummary;
	}
	
	public void doCloseDetail(){
		resultTemps.clear();
		totalRecords = "0";
		selectedAmount = "0.00";
		hiddenSelectedAmount = "0";
		availableBalance = null;
		accountNumber = null;
		setDetailList(null);
	}

	private void loadData() {
		setSummaryList(new DetailDataModel());
	}
	
	private class DetailDataModel extends LazyDataModel < SpanDataSummary > {
		private static final long serialVersionUID = 1L;

		public DetailDataModel() {

		}

		@SuppressWarnings("rawtypes")
		@Override
		public List < SpanDataSummary > load(int startingAt, int maxPerPage, String sortField, SortOrder sortOrder, Map < String, String > filters) {
			SPANDataValidationDao spanDataValidationDao = getSpanDataValidationDaoBean();
			SystemParameterDao systemParameterDao = (SystemParameterDao) applicationBean.getCorpGWContext().getBean("systemParameterDAO");
			List < SpanDataSummary > details = new ArrayList<SpanDataSummary>();
			Integer jmlAll = 0;
			GetSPANSummariesRequest request = new GetSPANSummariesRequest();
			GetSPANSummariesResponse response = null;
			//FIXME masih hardcode
			request.setSumType("0");
			//Status = tab type (retry/retur/sendack)
			request.setStatus(getTabId());
			
			//Provider Code
			if(tabSelected.equals(Constants.GAJI)){
				request.setProviderCode("SPN");
			}else{
				request.setProviderCode("SPN01");
			}
			 
			try {
				response = GetSPANSummaries.getSPANSummaries(request, spanDataValidationDao, systemParameterDao, tabSelected);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			List<OutputDetails> OutputDetailsList= response.getOutputDetails();
			
			
			
			for(OutputDetails outputDetails : OutputDetailsList){
				SpanDataSummary sds = new SpanDataSummary();
				sds.setFileName(outputDetails.getFileName());
				sds.setTotalAmount(outputDetails.getTotalAmount());
				sds.setTotalRecord(outputDetails.getTotalRecord());
				details.add(sds);
			}
			
			/*GetSPANSummariesDetailRequest requestDetail = new GetSPANSummariesDetailRequest();
			// FIXME remove hardcode
			requestDetail.setSearchBy("1");
			try {
				GetSPANSummariesDetailResponse responseDetail = new GetSPANSummariesDetailResponse();
				responseDetail = GetSPANSummariesDetail.getSPANSummariesDetail(requestDetail, spanDataValidationDao, systemParameterDao);
				System.out.println(responseDetail);
			} catch (Exception e) {
				e.printStackTrace();
			}*/
					
			/*//DUMMY DATA!
			SpanDataSummary sds = new SpanDataSummary();
			sds.setFileName("Dummy1");sds.setTotalAmount("100");sds.setTotalRecord("10");details.add(sds);
			sds = new SpanDataSummary();sds.setFileName("Dummy2");sds.setTotalAmount("0");sds.setTotalRecord("0");details.add(sds);
			sds = new SpanDataSummary();sds.setFileName("Dummy3");sds.setTotalAmount("0");sds.setTotalRecord("0");details.add(sds);
			sds = new SpanDataSummary();sds.setFileName("Dummy4");sds.setTotalAmount("0");sds.setTotalRecord("0");details.add(sds);
			sds = new SpanDataSummary();sds.setFileName("Dummy5");sds.setTotalAmount("0");sds.setTotalRecord("0");details.add(sds);
			sds = new SpanDataSummary();sds.setFileName("Dummy6");sds.setTotalAmount("0");sds.setTotalRecord("0");details.add(sds);*/
			jmlAll = details.size();
			
			List < SpanDataSummary > detailsFinalTemp = new ArrayList<SpanDataSummary>();
			int count = 0;
			for(int i=startingAt; i<details.size(); i++){
				detailsFinalTemp.add(details.get(i));
				count++;
				if(count==maxPerPage){
					break;
				}
			}
			
			setRowCount(jmlAll);
			setPageSize(maxPerPage);
			List < SpanDataSummary > detailsFinal = new ArrayList < SpanDataSummary > ();
			for (SpanDataSummary a: detailsFinalTemp) {
				Iterator entries = resultTemps.entrySet().iterator();
				while (entries.hasNext()) {
					Entry thisEntry = (Entry) entries.next();
					SpanDataSummary s = (SpanDataSummary) thisEntry.getValue();
					if (a.getFileName().equals(s.getFileName())) {
						if (s.isSelected()) {
							a.setSelected(true);
						} else {
							a.setSelected(false);
						}
					}
				}
				detailsFinal.add(a);
			}
			if(detailsFinal.size()>0){
				return detailsFinal;
			}else{
				return null;
			}
		}
	}
	
	private void loadDataDialog(){
		setDetailList(new DialogDetailDataModel());
	}
	
	private class DialogDetailDataModel extends LazyDataModel < DetailsOutputList > {
		private static final long serialVersionUID = 1L;

		public DialogDetailDataModel() {

		}

		@SuppressWarnings("rawtypes")
		@Override
		public List < DetailsOutputList > load(int startingAt, int maxPerPage, String sortField, SortOrder sortOrder, Map < String, String > filters) {
			GetSPANSummariesDetailRequest request = new GetSPANSummariesDetailRequest();
			request.setFilename(spanDataSummary.getFileName());
			request.setClickOn(getTabIdDetails());
			GetSPANSummariesDetailResponse getSPANSummariesDetailResponse = new GetSPANSummariesDetailResponse();
			List<DetailsOutputList> detailsOutputList = new ArrayList<DetailsOutputList>();
			try {
				getSPANSummariesDetailResponse = GetSPANSummariesDetail.getSPANSummariesDetail(request, getSpanDataValidationDaoBean(), getSystemParameterDAOBean(), tabSelected);
				detailsOutputList = getSPANSummariesDetailResponse.getDetailsOutputList();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			int jmlAll = detailsOutputList.size();
			
			List < DetailsOutputList > detailsFinalTemp = new ArrayList<DetailsOutputList>();
			int count = 0;
			for(int i=startingAt; i<detailsOutputList.size(); i++){
				detailsFinalTemp.add(detailsOutputList.get(i));
				count++;
				if(count==maxPerPage){
					break;
				}
			}
			
			setRowCount(jmlAll);
			setPageSize(maxPerPage);
			List < DetailsOutputList > detailsFinal = new ArrayList < DetailsOutputList > ();
			for (DetailsOutputList a: detailsFinalTemp) {
//				Iterator entries = resultTemps.entrySet().iterator();
//				while (entries.hasNext()) {
//					Entry thisEntry = (Entry) entries.next();
//					DetailsOutputList s = (DetailsOutputList) thisEntry.getValue();
//					if (a.getDocNumber().equals(s.getDocNumber())) {
//						if (s.isSelected()) {
//							a.setSelected(true);
//						} else {
//							a.setSelected(false);
//						}
//					}
//				}
				detailsFinal.add(a);
			}
			if(detailsFinal.size()>0){
				return detailsFinal;
			}else{
				return null;
			}
		}
	}
	
	private SPANDataValidationDao getSpanDataValidationDaoBean() {
		SPANDataValidationDao spanDataValidationDao = (SPANDataValidationDao) applicationBean.getCorpGWContext().getBean("SPANDataValidationDao");
		return spanDataValidationDao;
	}
	
	private ToBeProcessDao getToBeProcessDaoBean() {
		ToBeProcessDao toBeProcessDao = (ToBeProcessDao) applicationBean.getCorpGWContext().getBean("ToBeProcessDao");
		return toBeProcessDao;
	}
	
	private SystemParameterDao getSystemParameterDAOBean(){
		return (SystemParameterDao) applicationBean.getCorpGWContext().getBean("systemParameterDAO");
	}
	
	public void ExecuteChecked(){
		boolean status = false;
		if(doShowSendACK){
			if(totalRecords.equals("0")||totalRecords==null){
				String headerMessage = "Error";
				String bodyMessage = "Please check at least 1 record.";
				FacesUtil.setErrorMessageDialog(headerMessage, bodyMessage);
			}else{
				status = true;
			}
		}else{
			try{
				if(getAvailableBalance().equals("")||getAvailableBalance()==null||getAvailableBalance()=="Error"){
					String headerMessage = "Error";
					String bodyMessage = "Please Inquiry First.";
					FacesUtil.setErrorMessageDialog(headerMessage, bodyMessage);
				}else if(totalRecords.equals("0")||totalRecords==null){
					String headerMessage = "Error";
					String bodyMessage = "Please check at least 1 record.";
					FacesUtil.setErrorMessageDialog(headerMessage, bodyMessage);
				}else{
					if(getAvailableBalance().equals(".00")){
						setAvailableBalance("0.00");
					}
					if(Integer.parseInt(getAvailableBalance().replace(".00", "").replace(",", ""))
							>=Integer.parseInt(getSelectedAmount().replace(".00", "").replace(",", ""))){
						status=true;
					}else{
						String headerMessage = "Error";
						String bodyMessage = "Not enough balance to execute selected records.";
						FacesUtil.setErrorMessageDialog(headerMessage, bodyMessage);
					}
				}
			}catch(Exception e){
				String headerMessage = "Error";
				String bodyMessage = "Please Inquiry First.";
				FacesUtil.setErrorMessageDialog(headerMessage, bodyMessage);
			}
		}
		status = true;
		if(status){
			//Construct Input
			ToBeProcessInput input = new ToBeProcessInput();
			input.setExeType("0");
			input.setTypeProcess(getTabId());
			String log = "";
			if(doShowRetry){
				System.out.println("Send Retry Checked");
				input.setLegStatus("L1");
				log = "RETRY";
			}else if(doShowRetur){
				System.out.println("Send Retur Checked");
				input.setLegStatus("L1");
				SystemParameter param = getSystemParameterDAOBean().getDetailedParameterByParamName(Constants.PARAM_DEBITACCNO.concat(Constants.GAJIR));
				input.setAccountRetur(param.getParamValue());
				log = "RETUR";
			}else if(doShowSendACK){
				System.out.println("Send ACK Checked");
				log = "SEND ACK";
			}
			List<Files> files = new ArrayList<Files>();
			
			Iterator entries = resultTemps.entrySet().iterator();
			while (entries.hasNext()) {
				Files fileData = new Files();
				Entry thisEntry = (Entry) entries.next();
				SpanDataSummary s = (SpanDataSummary) thisEntry.getValue();
				fileData.setFileName(s.getFileName());
				files.add(fileData);
				doLoging(ConstantsUserLog.MENU_RESULT_TO_BE_PROCESS, ConstantsUserLog.ACTIVITY_RTBP_EXECUTE.concat(tabSelected).concat(log), log.concat(" : ").concat(fileData.getFileName()));
			}
			input.setFiles(files);
			ToBeProcessOutput toBeProcessOutput = new ToBeProcessOutput();
			toBeProcessOutput = ToBeProcess.toBeProcess(input, getSpanDataValidationDaoBean(), getToBeProcessDaoBean(), getSystemParameterDAOBean());
			System.out.println(toBeProcessOutput.getStatus());
			String headerMessage = "Success";
			String bodyMessage = "Success to execute selected records.";
			totalRecords = "0";
			selectedAmount = "0.00";
			hiddenSelectedAmount = "0";
			FacesUtil.setInfoMessageDialog(headerMessage, bodyMessage);
		}
	}
	
	public void InquirySaldo(){
		SystemParameterDao parameterDao = (SystemParameterDao) applicationBean.getCorpGWContext().getBean("systemParameterDAO");
		SystemParameter parameter = parameterDao.getDetailedParameterByParamName(Constants.PARAM_DEBITACCNO+tabSelected);
		accountNumber =	parameter.getParamValue();
		try{
			String availableBalance = AccountBalance.getAccountBalance(parameterDao, accountNumber);
//			setAvailableBalance(String.format("%1$,.2f",Float.parseFloat(availableBalance))); //DO NOT USE - ERROR!!!!
			DecimalFormat df = new DecimalFormat("###,###.00");
			setAvailableBalance(df.format(new BigInteger((availableBalance))));
			if(availableBalance.equals(".00")){
//				availableBalance = "0.00";
				setAvailableBalance("Error");
			}
			doLoging(ConstantsUserLog.MENU_RESULT_TO_BE_PROCESS, ConstantsUserLog.ACTIVITY_RTBP_INQUIRY_SALDO, "Inquiry Saldo "+tabSelected+". Success : "+getAvailableBalance());
		}catch(Exception e){
			setAvailableBalance("Error");
			doLoging(ConstantsUserLog.MENU_RESULT_TO_BE_PROCESS, ConstantsUserLog.ACTIVITY_RTBP_INQUIRY_SALDO, "Inquiry Saldo "+tabSelected+". Failed.");
		}
	}
	
	public void prepareForceRetur(SpanDataSummary summary){
		spanDataSummaryTemp = summary;
	}
	
	public void ForceRetur(){
		//TODO Force Retur logic here
		System.out.println("Force Retur");
		spanDataSummaryTemp = null;
	}
	
	private String getTabId(){
		if(doShowRetry){
			return "2";
		}else if(doShowRetur){
			return "1";
		}else if(doShowSendACK){
			return "3";
		}
		return null;
	}
	
	private String getTabIdDetails(){
		if(doShowRetry){
			return "1";
		}else if(doShowRetur){
			return "2";
		}else if(doShowSendACK){
			return "3";
		}
		return null;
	}
	
	public void downloadError(String type){
		try {
			List<SpanDataSummary> spanDataSummaries = (List<SpanDataSummary>) PubUtil.mapToList(resultTempsACK);
			if(PubUtil.isListNotEmpty(spanDataSummaries)){
				for(SpanDataSummary summary : spanDataSummaries){
					if((summary.getTotalAmount().equals("0")||summary.getTotalAmount().equals("")||
							summary.getTotalAmount()==null||summary.getTotalAmount().equals("0.00")||summary.getTotalAmount()==null)
							&& StringUtils.isNotEmpty(formatOfFile)){
						
						String paymentMethod = null;
						String fileType = null;
						if("0".equalsIgnoreCase(formatOfFile)){ //RTGS
							paymentMethod = "1";
							fileType = "RTGS";
						}
						else if("1".equalsIgnoreCase(formatOfFile)){ //skn
							paymentMethod = "2";
							fileType = "SKN";
						}
						else if("2".equalsIgnoreCase(formatOfFile)){//ob
							paymentMethod = "0";
							fileType = "OB";
						}
						
						SPANDataValidationDao spanDataValidationDao = getSpanDataValidationDaoBean();
						
						List<DownloadErrorResponse> downloadErrorResponses = spanDataValidationDao.getDownloadErrorResponse(paymentMethod, summary.getFileName());
						if(PubUtil.isListNotEmpty(downloadErrorResponses)){
							generateFile(downloadErrorResponses, type);
//							MappingErrorData_SVC.mappingErrorData("", "D:/", fileType, downloadErrorResponses); //by pass untuk create file
							for(DownloadErrorResponse downloadErrorResponse : downloadErrorResponses){
								spanDataValidationDao.updateSpanHostResponse("00", "0", downloadErrorResponse.getBatchid(), downloadErrorResponse.getTrxDetailId());
							}
							spanDataValidationDao.updateForceAck(summary.getFileName());
							doLoging(ConstantsUserLog.MENU_RESULT_TO_BE_PROCESS, ConstantsUserLog.ACTIVITY_RTBP_FORCE_ACK.concat(tabSelected), "FORCE ACK : ".concat(summary.getFileName()));
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		resultTempsACK.clear();
		FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().add("resultToBeProcessForm");
	}

	private void generateFile(List<DownloadErrorResponse> downloadErrorResponses, String type) throws IOException {
		FacesContext fc = FacesContext.getCurrentInstance();
		String jasperPath = null;
		JRBeanCollectionDataSource beanCollectionDataSource = null;
		String fileName = null;
		String printFIle = null;
		JRXlsExporter exporter = null;
		OutputStream outputStream = null;
		ByteArrayOutputStream baos = null;
		byte[] report = null;
		try {
			ExternalContext ec = fc.getExternalContext();
			ec.responseReset();// Some JSF component library or some Filter might have set some headers in the buffer beforehand. We want to get rid of them, else it may collide.
			beanCollectionDataSource = new JRBeanCollectionDataSource(downloadErrorResponses);
			if("xls".equalsIgnoreCase(type)){
				baos = new ByteArrayOutputStream();
				fileName = "force_ack.xls";
				ec.setResponseContentType(Constants.CONTENT_TYPE_XLS);
				jasperPath = FacesUtil.getExternalContext().getRealPath("resources/report/forceAckXls.jasper");
				printFIle = JasperFillManager.fillReportToFile(jasperPath,null, beanCollectionDataSource);
				exporter = new JRXlsExporter();
				exporter.setParameter(JRExporterParameter.INPUT_FILE_NAME,printFIle);
				exporter.setParameter(JExcelApiExporterParameter.MAXIMUM_ROWS_PER_SHEET,50000);
				exporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
				exporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
				exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
				exporter.setParameter(JRExporterParameter.OUTPUT_STREAM,baos);
				exporter.exportReport();
				report = baos.toByteArray();
			}
			else{
				fileName = "force_ack.pdf";
				jasperPath = FacesUtil.getExternalContext().getRealPath("resources/report/forceAckPdf.jasper");
				ec.setResponseContentType(Constants.CONTENT_TYPE_PDF);
				report = JasperRunManager.runReportToPdf(jasperPath,null, beanCollectionDataSource);
			}
			ec.setResponseHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
			ec.setResponseHeader("Refresh", "1; url = resultToBeProcess.jsf");
			outputStream = ec.getResponseOutputStream();
			outputStream.write(report);
			outputStream.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(outputStream!=null)outputStream.close();
			if(baos!=null)baos.close();
		}
		fc.responseComplete(); // Important! Otherwise JSF will attempt to render the response which obviously will fail since it's already written with a file and closed.
	}
	
	private void doLoging(String menu, String activity, String information) {
		UserLog userLog = new UserLog();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		userLog.setLogId("LOG-"+sdf.format(new Date())+"-"+sessionBean.getUser().getUserID());
		userLog.setActivity(activity);
		userLog.setInformation(information);
		userLog.setInsertTime(new Date());
		userLog.setMenu(menu);
		userLog.setUserId(sessionBean.getUser().getUserID());
		userLog.setUserName(sessionBean.getUser().getUserName());
		UserLogHandler.doLog(userLog, getUserDAOBean());
	}
	
	private UserDao getUserDAOBean() {
		return (UserDao) applicationBean.getCorpGWContext().getBean("userDAO");
	}

	public void clearPrepForceRetur(){
		spanDataSummaryTemp = null;
	}
	
	public void doDownloadFile(){
		doShowPDFXLS = true;
	}
	
	public ApplicationBean getApplicationBean() {
		return applicationBean;
	}

	public void setApplicationBean(ApplicationBean applicationBean) {
		this.applicationBean = applicationBean;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public Boolean getDoShowGAJI() {
		return doShowGAJI;
	}

	public void setDoShowGAJI(Boolean doShowGAJI) {
		this.doShowGAJI = doShowGAJI;
	}

	public Boolean getDoShowGAJIR() {
		return doShowGAJIR;
	}

	public void setDoShowGAJIR(Boolean doShowGAJIR) {
		this.doShowGAJIR = doShowGAJIR;
	}

	public Boolean getDoShowRetry() {
		return doShowRetry;
	}

	public void setDoShowRetry(Boolean doShowRetry) {
		this.doShowRetry = doShowRetry;
	}

	public Boolean getDoShowRetur() {
		return doShowRetur;
	}

	public void setDoShowRetur(Boolean doShowRetur) {
		this.doShowRetur = doShowRetur;
	}

	public Boolean getDoShowSendACK() {
		return doShowSendACK;
	}

	public void setDoShowSendACK(Boolean doShowSendACK) {
		this.doShowSendACK = doShowSendACK;
	}

	public String getTabSelected() {
		return tabSelected;
	}

	public void setTabSelected(String tabSelected) {
		this.tabSelected = tabSelected;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getSelectedAmount() {
		return selectedAmount;
	}

	public void setSelectedAmount(String selectedAmount) {
		this.selectedAmount = selectedAmount;
	}

	public String getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(String totalRecords) {
		this.totalRecords = totalRecords;
	}

	public String getAvailableBalance() {
		return availableBalance;
	}

	public void setAvailableBalance(String availableBalance) {
		this.availableBalance = availableBalance;
	}

	public SpanDataSummary getSpanDataSummary() {
		return spanDataSummary;
	}

	public void setSpanDataSummary(SpanDataSummary spanDataSummary) {
		this.spanDataSummary = spanDataSummary;
	}

	public LazyDataModel < SpanDataSummary > getSummaryList() {
		return summaryList;
	}

	public void setSummaryList(LazyDataModel < SpanDataSummary > summaryList) {
		this.summaryList = summaryList;
	}

	public SpanDataSummaryDetails getSpanDataSummaryDetails() {
		return spanDataSummaryDetails;
	}

	public void setSpanDataSummaryDetails(SpanDataSummaryDetails spanDataSummaryDetails) {
		this.spanDataSummaryDetails = spanDataSummaryDetails;
	}

	public SpanDataSummary getSpanDataSummaryTemp() {
		return spanDataSummaryTemp;
	}

	public void setSpanDataSummaryTemp(SpanDataSummary spanDataSummaryTemp) {
		this.spanDataSummaryTemp = spanDataSummaryTemp;
	}

	public Boolean getDoShowDownloadFile() {
		return doShowDownloadFile;
	}

	public void setDoShowDownloadFile(Boolean doShowDownloadFile) {
		this.doShowDownloadFile = doShowDownloadFile;
	}

	public String getTypeOfProcess() {
		return typeOfProcess;
	}

	public void setTypeOfProcess(String typeOfProcess) {
		this.typeOfProcess = typeOfProcess;
	}

	public String getFormatOfFile() {
		return formatOfFile;
	}

	public void setFormatOfFile(String formatOfFile) {
		this.formatOfFile = formatOfFile;
	}

	public Boolean getDoShowPDFXLS() {
		return doShowPDFXLS;
	}

	public void setDoShowPDFXLS(Boolean doShowPDFXLS) {
		this.doShowPDFXLS = doShowPDFXLS;
	}

	public LazyDataModel < DetailsOutputList > getDetailList() {
		return detailList;
	}

	public void setDetailList(LazyDataModel < DetailsOutputList > detailList) {
		this.detailList = detailList;
	}

	public String getHiddenSelectedAmount() {
		return hiddenSelectedAmount;
	}

	public void setHiddenSelectedAmount(String hiddenSelectedAmount) {
		this.hiddenSelectedAmount = hiddenSelectedAmount;
	}

	public String getDetailFileName() {
		return detailFileName;
	}

	public void setDetailFileName(String detailFileName) {
		this.detailFileName = detailFileName;
	}

	public String getDetailTotalRecord() {
		return detailTotalRecord;
	}

	public void setDetailTotalRecord(String detailTotalRecord) {
		this.detailTotalRecord = detailTotalRecord;
	}

	public String getDetailTotalAmount() {
		return detailTotalAmount;
	}

	public void setDetailTotalAmount(String detailTotalAmount) {
		this.detailTotalAmount = detailTotalAmount;
	}

	public String getDetailType() {
		return detailType;
	}

	public void setDetailType(String detailType) {
		this.detailType = detailType;
	}

	public String getDetailDebitAccount() {
		return detailDebitAccount;
	}

	public void setDetailDebitAccount(String detailDebitAccount) {
		this.detailDebitAccount = detailDebitAccount;
	}

	public String getDetailDebitAccountType() {
		return detailDebitAccountType;
	}

	public void setDetailDebitAccountType(String detailDebitAccountType) {
		this.detailDebitAccountType = detailDebitAccountType;
	}

	public String getDetailDocumentDate() {
		return detailDocumentDate;
	}

	public void setDetailDocumentDate(String detailDocumentDate) {
		this.detailDocumentDate = detailDocumentDate;
	}
	
}