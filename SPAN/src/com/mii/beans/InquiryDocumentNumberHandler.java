package com.mii.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.mii.helpers.FacesUtil;
import com.mii.invoker.DatabaseParameter;
import com.mii.invoker.SpanDataInvokeSOA;
import com.mii.invoker.WMInvoker;
import com.mii.models.DocumentNoDetail;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.SessionBean;
import com.wm.data.IData;
import com.wm.data.IDataCursor;
import com.wm.data.IDataFactory;
import com.wm.data.IDataUtil;

@ManagedBean
@ViewScoped
public class InquiryDocumentNumberHandler implements Serializable{
	private static final long serialVersionUID = 1L;

	@ManagedProperty(value="#{applicationBean}")
	private ApplicationBean applicationBean;
	
	@ManagedProperty(value="#{sessionBean}")
	private SessionBean sessionBean;
	
	private LazyDataModel < DocumentNoDetail > inquiryList;
	
	private String documentNumber;
	
	@PostConstruct
	public void init(){
		FacesUtil.authMenu(getSessionBean().getUser(), "inquiryDocumentNumber");
	}
	
	public void doInquiry(){
		setInquiryList(new DetailDataModel());
	}
	
	private IData constructInputWM() {
		// input
		IData input = IDataFactory.create();
		IDataCursor inputCursor = input.getCursor();
		
		// documentNumberList
		String SP2DNumberForLink = "";
		String documentNumber = this.documentNumber;
		SP2DNumberForLink = SP2DNumberForLink+documentNumber+",";
		
		//Tambahan untuk invoke lazy paging
		IDataUtil.put(inputCursor, "page", "1");
		IDataUtil.put(inputCursor, "rowsNumber", "9999999");
		IDataUtil.put(inputCursor, "SP2DList", SP2DNumberForLink);
		inputCursor.destroy();
		return input;
	}
	
	private class DetailDataModel extends LazyDataModel < DocumentNoDetail > {
		private static final long serialVersionUID = 1L;

		public DetailDataModel() {
		}

		@Override
		public List < DocumentNoDetail > load(int startingAt, int maxPerPage,
		String sortField, SortOrder sortOrder,
		Map < String, String > filters) {
			List < DocumentNoDetail > detailList = new ArrayList<DocumentNoDetail>();
			Integer jmlAll = 0;
			 IData input = constructInputWM();
			 IData output = IDataFactory.create();
			 try{
				SpanDataInvokeSOA sdi = DatabaseParameter.getInstance().getSpanDataInvokeSOA();
				output = (IData) WMInvoker.doInvoke(sdi.getIp(), sdi.getUsername(), sdi.getPassword(),
						"mandiri.span.atomic.request", "inquiryTransactionDetail", input );
			 }catch( Exception e){
				e.printStackTrace();
			 }
			 if (output !=null) {
				 IDataCursor outputCursor = output.getCursor();
				 	// selectSpanDocNoDetailListResult
					IData[]	selectSpanDocNoDetailListResult = IDataUtil.getIDataArray( outputCursor, "oDataInquiry" );
					String totalRecord = IDataUtil.getString( outputCursor, "totalRecord" );
					jmlAll = Integer.parseInt(totalRecord);
					if ( selectSpanDocNoDetailListResult != null){
						for ( int i = 0; i < selectSpanDocNoDetailListResult.length; i++ ){
							IDataCursor selectSpanDocNoDetailListResultCursor = selectSpanDocNoDetailListResult[i].getCursor();
								String	FILE_NAME = IDataUtil.getString( selectSpanDocNoDetailListResultCursor, "FILE_NAME" );
								String	DOCUMENT_NO = IDataUtil.getString( selectSpanDocNoDetailListResultCursor, "DOC_NUMBER" );
								String	DOCUMENT_DATE = IDataUtil.getString( selectSpanDocNoDetailListResultCursor, "DOC_DATE" );
								String	CREDIT_ACCOUNT_NAME = IDataUtil.getString( selectSpanDocNoDetailListResultCursor, "CREDITACCNO" );
								String	CREDIT_ACCOUNT_NO = IDataUtil.getString( selectSpanDocNoDetailListResultCursor, "CREDITACCNAME" );
								String	DEBIT_ACCOUNT_NO = IDataUtil.getString( selectSpanDocNoDetailListResultCursor, "DEBITACCNO" );
								String	AMOUNT = IDataUtil.getString( selectSpanDocNoDetailListResultCursor, "AMOUNT" );
								String	REMITTANCE_NO = IDataUtil.getString( selectSpanDocNoDetailListResultCursor, "REMIT_NUM" );
								String 	SORBOR_NUM = IDataUtil.getString( selectSpanDocNoDetailListResultCursor, "SORBOR_NUM" );
								String  STATUS_TRX = IDataUtil.getString( selectSpanDocNoDetailListResultCursor, "STATUS_TRX" );
								String	EXECUTOR = IDataUtil.getString( selectSpanDocNoDetailListResultCursor, "EXECUTOR" );
								
							selectSpanDocNoDetailListResultCursor.destroy();
							
							DocumentNoDetail dnd = new DocumentNoDetail();
							dnd.setFileName(FILE_NAME);
							dnd.setDocumentNo(DOCUMENT_NO);
							dnd.setDocumentDate(DOCUMENT_DATE);
							dnd.setCreditAccountName(CREDIT_ACCOUNT_NAME);
							dnd.setCreditAccountNo(CREDIT_ACCOUNT_NO);
							dnd.setDebitAccountNo(DEBIT_ACCOUNT_NO);
							dnd.setAmount(AMOUNT);
							dnd.setRemittanceNo(REMITTANCE_NO);
							dnd.setSorBorNum(SORBOR_NUM);
							dnd.setStatusTrx(STATUS_TRX);
							dnd.setExecutor(EXECUTOR);
						detailList.add(dnd);		
						}
					}
					outputCursor.destroy();
			 }
			setRowCount(jmlAll);
			setPageSize(maxPerPage);
			return detailList;
		}
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public ApplicationBean getApplicationBean() {
		return applicationBean;
	}

	public void setApplicationBean(ApplicationBean applicationBean) {
		this.applicationBean = applicationBean;
	}

	public LazyDataModel < DocumentNoDetail > getInquiryList() {
		return inquiryList;
	}

	public void setInquiryList(LazyDataModel < DocumentNoDetail > inquiryList) {
		this.inquiryList = inquiryList;
	}

	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}
	
	
}
