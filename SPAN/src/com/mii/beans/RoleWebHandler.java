package com.mii.beans;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.apache.commons.lang.SerializationUtils;
import org.omnifaces.model.tree.ListTreeModel;
import org.omnifaces.model.tree.TreeModel;
import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.mii.constant.Constants;
import com.mii.constant.ConstantsUserLog;
import com.mii.dao.ApprovalDao;
import com.mii.dao.RoleDao;
import com.mii.dao.SystemParameterDao;
import com.mii.dao.UserDao;
import com.mii.helpers.FacesUtil;
import com.mii.helpers.HelperUtils;
import com.mii.models.Approval;
import com.mii.models.Menu;
import com.mii.models.Role;
import com.mii.models.UserLog;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.SessionBean;

@ManagedBean
@ViewScoped
public class RoleWebHandler implements Serializable {
	private static final long serialVersionUID = 1L;
	private Role role;
	private Role tempRole;
	private Menu menu;
	private LazyDataModel<Role> roleList;
	private List<Menu> menus;
	private TreeModel<Menu> tree;
	private TreeModel<Menu> selectedMenu;
	private HashMap<Integer, Menu> rootMenu;
	private HashMap<Integer, Menu> childMenu;
	
	private List<Menu> tempMenus;
	private TreeModel<Menu> tempTree;
	private TreeModel<Menu> tempSelectedMenu;
	private HashMap<Integer, Menu> tempRootMenu;
	private HashMap<Integer, Menu> tempChildMenu;
	private String action;
	
	private String roleId;
	private String roleName;
	private List<String> status;
	private boolean pending;
	private boolean approved;
	private boolean rejected;
	
	@ManagedProperty(value="#{applicationBean}")
	private ApplicationBean applicationBean;
	@ManagedProperty(value="#{sessionBean}")
	private SessionBean sessionBean;
	
	private boolean doShowSearch = false;
	
	private boolean doShowHistory = false;

	
	private String currentRolename;
	
	@PostConstruct
	public void init(){
		FacesUtil.authMenu(sessionBean.getUser(), "roleWebAdministration");

		pending = false;
		approved = false;
		rejected = false;
		roleId = "";
		roleName = "";
		doSearch();
		
		doLoging(ConstantsUserLog.MENU_ROLE, ConstantsUserLog.ACTIVITY_INIT, "Menu Init.");
	}
	
	public void newRole(){
		this.role = new Role();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd-hhmm-sss");
		Date date = new Date();
		String id = sdf.format(date);
		Integer seq = getSystemParameterDAOBean().getParameterSequence().intValue();
		this.role.setRoleId("RLE-"+id+String.format("%04d", seq));
		createTreeMenu();
		this.action = "";
	}
	
	private ApprovalDao getApprovalDAOBean(){
		return (ApprovalDao) applicationBean.getCorpGWContext().getBean("approvalDAO");
	}
	

	private SystemParameterDao getSystemParameterDAOBean(){
		return (SystemParameterDao) applicationBean.getCorpGWContext().getBean("systemParameterDAO");
	}
	
	
	public Role getTempRole() {
		return tempRole;
	}

	public void setTempRole(Role tempRole) {
		this.tempRole = tempRole;
	}

	public List<Menu> getTempMenus() {
		return tempMenus;
	}

	public void setTempMenus(List<Menu> tempMenus) {
		this.tempMenus = tempMenus;
	}

	public TreeModel<Menu> getTempTree() {
		return tempTree;
	}

	public void setTempTree(TreeModel<Menu> tempTree) {
		this.tempTree = tempTree;
	}

	public TreeModel<Menu> getTempSelectedMenu() {
		return tempSelectedMenu;
	}

	public void setTempSelectedMenu(TreeModel<Menu> tempSelectedMenu) {
		this.tempSelectedMenu = tempSelectedMenu;
	}

	public HashMap<Integer, Menu> getTempRootMenu() {
		return tempRootMenu;
	}

	public void setTempRootMenu(HashMap<Integer, Menu> tempRootMenu) {
		this.tempRootMenu = tempRootMenu;
	}

	public HashMap<Integer, Menu> getTempChildMenu() {
		return tempChildMenu;
	}

	public void setTempChildMenu(HashMap<Integer, Menu> tempChildMenu) {
		this.tempChildMenu = tempChildMenu;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public List<String> getStatus() {
		return status;
	}

	public void setStatus(List<String> status) {
		this.status = status;
	}

	public boolean isPending() {
		return pending;
	}

	public void setPending(boolean pending) {
		this.pending = pending;
	}

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}

	public boolean isRejected() {
		return rejected;
	}

	public void setRejected(boolean rejected) {
		this.rejected = rejected;
	}

	public boolean isDoShowSearch() {
		return doShowSearch;
	}

	public void setDoShowSearch(boolean doShowSearch) {
		this.doShowSearch = doShowSearch;
	}

	public void showSearch(){
		doShowSearch = true;
	}
	
	public void hideSearch(){
		doShowSearch = false;
	}
	
	
	public void showHistory(){
		doShowHistory = true;
	}
	
	public void hideHistory(){
		doShowHistory = false;
	}
	
	public boolean isDoShowHistory() {
		return doShowHistory;
	}

	public void setDoShowHistory(boolean doShowHistory) {
		this.doShowHistory = doShowHistory;
	}
	
	private class RoleDataModel extends LazyDataModel<Role>{
		private static final long serialVersionUID = 1L;
		public RoleDataModel() {
			
		}
		@Override
		public List<Role> load(int startingAt, 
				int maxPerPage, String sortField, 
				SortOrder sortOrder, Map<String, String> filters) {
			
			RoleDao roleDao = (RoleDao) applicationBean.getCorpGWContext().getBean("roleDAO");
			List<Role> roles;
			
			System.out.println("load List Role");
			roles = roleDao.getLazyRoleByCategory(maxPerPage+startingAt, startingAt, roleId, roleName, status);
			Integer jmlAll = roleDao.countAllRoleByCategory(roleId, roleName, status);
			setRowCount(jmlAll);
			setPageSize(maxPerPage);
			
			return roles;
		}
	}
	
	public void viewDetailedRole(Role role){
		RoleDao roleDao = (RoleDao) applicationBean.getCorpGWContext().getBean("roleDAO");
		this.role = roleDao.getDetailedRole(role);
		createTreeMenu();
		
		if (this.role.getStatus().equals(Constants.APPROVAL_STATUS_PENDING_EDIT)){
			List<Approval> approvals = getApprovalDAOBean().getApprovalByParameterId(role.getRoleId());
			if (approvals.size()>0){
				try {
					this.tempRole = (Role) HelperUtils.deserialize(approvals.get(0).getNewBlobObject().getBinaryStream());
					createTempTreeMenu();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				RequestContext.getCurrentInstance().execute(
						"viewRoleEdit.show()");
			}
		}else{
			RequestContext.getCurrentInstance().execute(
					"viewRole.show()");
		}
	}	
	

	public void viewDetailedRoleEdit(Role role){
		RoleDao roleDao = (RoleDao) applicationBean.getCorpGWContext().getBean("roleDAO");
		this.role = roleDao.getDetailedRole(role);
		createTreeMenu();
		
		if (this.role.getDisableEdit() == true){
			FacesUtil.setApprovalWarning("role", this.role.getRoleName(), this.role.getStatus());
		}else{
			if (this.role.getStatus().equals(Constants.APPROVAL_STATUS_PENDING_EDIT)){
					
				List<Approval> approvals = getApprovalDAOBean().getApprovalByParameterId(role.getRoleId());
				if (approvals.size()>0){
					try {
						this.tempRole = (Role) HelperUtils.deserialize(approvals.get(0).getNewBlobObject().getBinaryStream());
						createTempTreeMenu();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					RequestContext.getCurrentInstance().execute(
							"insertOrEditRolePending.show()");
				}
			}
			else {
				
				this.tempRole =  (Role) SerializationUtils.clone(this.role);
				RequestContext.getCurrentInstance().execute(
						"insertOrEditRole.show()");
			}

			currentRolename = this.tempRole.getRoleName();
			this.action = "edit";
		}
	}
	
	public void viewDetailedDelete(Role role){

		RoleDao roleDao = (RoleDao) applicationBean.getCorpGWContext().getBean("roleDAO");
		this.role = roleDao.getDetailedRole(role);
		
		if (this.role.getDisableDelete() == true){
			FacesUtil.setApprovalWarning("role", this.role.getRoleName(), this.role.getStatus());
		}else{
			RequestContext.getCurrentInstance().execute(
					"dlgConfirm.show()");
		}
		
	}
	
	public void deleteRole(){
		
		if (role.getStatus().equals(Constants.APPROVAL_STATUS_REJECTED_CREATE)){

			UserDao userDao = (UserDao) applicationBean.getCorpGWContext().getBean("userDAO");
			userDao.deleteUserRole(role.getRoleId());
			
			RoleDao roleDao = (RoleDao) applicationBean.getCorpGWContext().getBean("roleDAO");
			roleDao.deleteRole(role);
			
			String headerMessage = "Success";
			String bodyMessage = "Delete Role "+ role.getRoleName() +" Successfully";
			
			FacesUtil.setInfoMessageDialog(headerMessage, bodyMessage);
			
		}else{
		
			Approval approval = new Approval();
			approval.setAction(Constants.APPROVAL_ACTION_DELETE);
			approval.setParameter(Constants.APPROVAL_PARAMETER_ROLE);
			approval.setParameterId(role.getRoleId());
			approval.setParameterName(role.getRoleName());
			approval.setBranchId(sessionBean.getUser().getBranch());
			
			try {
				approval.setOldByteObject(HelperUtils.serialize(role));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			getApprovalDAOBean().saveApproval(approval);
			
			role.setStatus(Constants.APPROVAL_STATUS_PENDING_DELETE);
			RoleDao roleDao = (RoleDao) applicationBean.getCorpGWContext().getBean("roleDAO");
			roleDao.saveOrUpdateRole(role);
			
			String headerMessage = "Delete Role Successfully";
			String bodyMessage = "Supervisor must approve the deleting of the <br/>role &apos;"
					+ role.getRoleName() + "&apos; to completely the process.";
			doLoging(ConstantsUserLog.MENU_ROLE, ConstantsUserLog.ACTIVITY_DELETE, "Delete Role "+role.getRoleName()+". Waiting for Approval.");
			FacesUtil.setInfoMessageDialog(headerMessage, bodyMessage);
		}
		
	}
	
	public void doSave(){
		
		
		Set<Menu> selectedMenu = new HashSet<Menu>();
		for (Map.Entry<Integer, Menu> entry : this.childMenu.entrySet()) {
			Menu menu = entry.getValue();
			if (menu!=null && menu.isSelected() == true) {
				selectedMenu.add(menu);
			}
		}
		
		for (Map.Entry<Integer, Menu> entry : this.rootMenu.entrySet()) {
			Menu menu = entry.getValue();
			if (menu!=null && menu.isSelected() == true) {
				selectedMenu.add(menu);
			}
		}
		
		this.role.setMenus(selectedMenu);
		
		if (this.action.equals("edit")){
			if (this.role.getStatus().equals(Constants.APPROVAL_STATUS_PENDING_EDIT)){
				try {
					RoleDao roleDao = (RoleDao) applicationBean.getCorpGWContext().getBean("roleDAO");
					Role roleExist = roleDao.getDetailedRoleByRoleName(this.tempRole.getRoleName());
					
					if (roleExist == null || (roleExist.getRoleName().equals(currentRolename))){
						selectedMenu = new HashSet<Menu>();
						for (Map.Entry<Integer, Menu> entry : this.tempChildMenu.entrySet()) {
							Menu menu = entry.getValue();
							if (menu!=null && menu.isSelected() == true) {
								selectedMenu.add(menu);
							}
						}
						
						for (Map.Entry<Integer, Menu> entry : this.tempRootMenu.entrySet()) {
							Menu menu = entry.getValue();
							if (menu!=null && menu.isSelected() == true) {
								selectedMenu.add(menu);
							}
						}
						
						this.tempRole.setMenus(selectedMenu);
						
						getApprovalDAOBean().updateApprovalNewObjectByParameterId(HelperUtils.serialize(this.tempRole), this.tempRole.getRoleId());
						
						RequestContext.getCurrentInstance().execute("insertOrEditRole.hide()");
						RequestContext.getCurrentInstance().execute("insertOrEditRolePending.hide()");
						
							
						String headerMessage = "Edit Role Successfully";
						String bodyMessage = "Supervisor must approve the editing of the <br/>role &apos;"
								+ tempRole.getRoleName() + "&apos; to completely the process.";
						doLoging(ConstantsUserLog.MENU_ROLE, ConstantsUserLog.ACTIVITY_EDIT, "Edit Role "+role.getRoleName()+". Waiting for Approval.");
						FacesUtil.setInfoMessageDialog(headerMessage, bodyMessage);
					}else{
						String headerMessage = "Attention";
						String bodyMessage = "Role "+this.tempRole.getRoleName() + " already exist in the application.";

						FacesUtil.setWarningMessageDialog(headerMessage, bodyMessage);
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else{
				RoleDao roleDao = (RoleDao) applicationBean.getCorpGWContext().getBean("roleDAO");
				Role roleExist = roleDao.getDetailedRoleByRoleName(this.role.getRoleName());
				
				if ((roleExist == null || (roleExist.getRoleName().equals(currentRolename)))){

					editRole(role);
					
				}else{
					String headerMessage = "Attention";
					String bodyMessage = "Role "+this.role.getRoleName() + " already exist in the application.";

					FacesUtil.setWarningMessageDialog(headerMessage, bodyMessage);
				}
			}
		}else{
			
			RoleDao roleDao = (RoleDao) applicationBean.getCorpGWContext().getBean("roleDAO");
			Role roleExist = roleDao.getDetailedRoleByRoleName(this.role.getRoleName());
			
			if (roleExist == null){

				createRole(role);
				
			}else{
				String headerMessage = "Attention";
				String bodyMessage = "Role "+this.role.getRoleName() + " already exist in the application.";

				FacesUtil.setWarningMessageDialog(headerMessage, bodyMessage);
			}
			
				
		}
	}
	
	public void editRole(Role role){
		role.setChangeDate(HelperUtils.getCurrentDateTime());
		role.setChangeWho(sessionBean.getUser().getUserName());
		try {
			if (role.getStatus().equals(Constants.APPROVAL_STATUS_PENDING_CREATE)){
				
				getApprovalDAOBean().updateApprovalNewObjectByParameterId(HelperUtils.serialize(this.role), this.role.getRoleId());
				
			}else if (role.getStatus().equals(Constants.APPROVAL_STATUS_REJECTED_CREATE)){
				

				Approval approval = new Approval();
				approval.setAction(Constants.APPROVAL_ACTION_CREATE);
				approval.setParameter(Constants.APPROVAL_PARAMETER_ROLE);
				approval.setParameterId(role.getRoleId());
				approval.setParameterName(role.getRoleName());
				approval.setOldByteObject(HelperUtils.serialize(this.tempRole));
				approval.setNewByteObject(HelperUtils.serialize(this.role));
				approval.setBranchId(sessionBean.getUser().getBranch());
				getApprovalDAOBean().saveApproval(approval);
				
				role = (Role) SerializationUtils.clone(this.tempRole);
				role.setStatus(Constants.APPROVAL_STATUS_PENDING_CREATE);
				
			}else {
				

				Approval approval = new Approval();
				approval.setAction(Constants.APPROVAL_ACTION_EDIT);
				approval.setParameter(Constants.APPROVAL_PARAMETER_ROLE);
				approval.setParameterId(role.getRoleId());
				approval.setParameterName(role.getRoleName());
				approval.setOldByteObject(HelperUtils.serialize(this.tempRole));
				approval.setNewByteObject(HelperUtils.serialize(this.role));
				approval.setBranchId(sessionBean.getUser().getBranch());
				getApprovalDAOBean().saveApproval(approval);
				
				role = (Role) SerializationUtils.clone(this.tempRole);
				role.setStatus(Constants.APPROVAL_STATUS_PENDING_EDIT);
				
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		RoleDao roleDao = (RoleDao) applicationBean.getCorpGWContext().getBean("roleDAO");
		roleDao.saveOrUpdateRole(role);
		

		RequestContext.getCurrentInstance().execute("insertOrEditRole.hide()");
		RequestContext.getCurrentInstance().execute("insertOrEditRolePending.hide()");
		
		String headerMessage = "Edit Role Successfully";
		String bodyMessage = "Supervisor must approve the editing of the <br/>role &apos;"
				+ role.getRoleName() + "&apos; to completely the process.";
		doLoging(ConstantsUserLog.MENU_ROLE, ConstantsUserLog.ACTIVITY_EDIT, "Edit Role "+role.getRoleName()+". Waiting for Approval.");
		FacesUtil.setInfoMessageDialog(headerMessage, bodyMessage);
	}
	
	public void createRole(Role role){
		role.setCreateDate(HelperUtils.getCurrentDateTime());
		role.setCreateWho(sessionBean.getUser().getUserName());
		role.setStatus(Constants.APPROVAL_STATUS_PENDING_CREATE);
		Approval approval = new Approval();
		approval.setAction(Constants.APPROVAL_ACTION_CREATE);
		approval.setParameter(Constants.APPROVAL_PARAMETER_ROLE);
		approval.setParameterId(role.getRoleId());
		approval.setParameterName(role.getRoleName());
		approval.setBranchId(sessionBean.getUser().getBranch());
		
		try {
			approval.setNewByteObject(HelperUtils.serialize(role));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		getApprovalDAOBean().saveApproval(approval);
		

		RoleDao roleDao = (RoleDao) applicationBean.getCorpGWContext().getBean("roleDAO");
		roleDao.saveOrUpdateRole(this.role);
		

		RequestContext.getCurrentInstance().execute("insertOrEditRole.hide()");
		RequestContext.getCurrentInstance().execute("insertOrEditRolePending.hide()");
		
		String headerMessage = "Create Role Successfully";
		String bodyMessage = "Supervisor must approve the creating of the <br/>role &apos;"
				+ role.getRoleName() + "&apos; to completely the process.";
		doLoging(ConstantsUserLog.MENU_ROLE, ConstantsUserLog.ACTIVITY_SAVE, "Create Role "+role.getRoleName()+". Waiting for Approval.");
		FacesUtil.setInfoMessageDialog(headerMessage, bodyMessage);
	}
	
	public void doSearch(){
		
		status 		= new ArrayList<String>();
		
		if (((pending == false) && (approved == false)) && (rejected == false)){
			status.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
			status.add(Constants.APPROVAL_STATUS_PENDING_EDIT);
			status.add(Constants.APPROVAL_STATUS_PENDING_DELETE);
			status.add(Constants.APPROVAL_STATUS_APPROVED_CREATE);
			status.add(Constants.APPROVAL_STATUS_APPROVED_EDIT);
			status.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
			status.add(Constants.APPROVAL_STATUS_REJECTED_EDIT);
			status.add(Constants.APPROVAL_STATUS_REJECTED_DELETE);
		}else{
			if (pending == true){
				status.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
				status.add(Constants.APPROVAL_STATUS_PENDING_EDIT);
				status.add(Constants.APPROVAL_STATUS_PENDING_DELETE);
			}
			if (approved == true){
				status.add(Constants.APPROVAL_STATUS_APPROVED_CREATE);
				status.add(Constants.APPROVAL_STATUS_APPROVED_EDIT);
			}
			if (rejected == true){
				status.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
				status.add(Constants.APPROVAL_STATUS_REJECTED_EDIT);
				status.add(Constants.APPROVAL_STATUS_REJECTED_DELETE);
			}
		}

		FacesUtil.resetPage("roleForm:roleTable");
		roleList = new RoleDataModel();
	}
	
	public void checkThebox(Menu checkedMenu){
		this.selectedMenu = null;
		List<TreeModel<Menu>> root = tree.getChildren();
		getCheckedMenuFromTree(root, checkedMenu);
		if (this.selectedMenu != null) {
			checkTheChild(selectedMenu);
			checkTheParent(selectedMenu);
		}
		
	}
	
	public void checkTheboxPending(Menu checkedMenu){
		this.tempSelectedMenu = null;
		List<TreeModel<Menu>> root = tempTree.getChildren();
		getCheckedMenuFromTreePending(root, checkedMenu);
		if (this.tempSelectedMenu != null) {
			checkTheChild(tempSelectedMenu);
			checkTheParent(tempSelectedMenu);
		}
		
	}
	
	public void getCheckedMenuFromTree(List<TreeModel<Menu>> root, Menu checkedMenu){		
		for (TreeModel<Menu> treeModel : root) {
			Menu data = treeModel.getData();
			if (data.getMenuId() == checkedMenu.getMenuId()) {
				this.selectedMenu = treeModel;
				return;
			}
			if (this.selectedMenu != null) {
				return;
			}
			getCheckedMenuFromTree(treeModel.getChildren(), checkedMenu);
		}
	}
	
	public void getCheckedMenuFromTreePending(List<TreeModel<Menu>> root, Menu checkedMenu){		
		for (TreeModel<Menu> treeModel : root) {
			Menu data = treeModel.getData();
			if (data.getMenuId() == checkedMenu.getMenuId()) {
				this.tempSelectedMenu = treeModel;
				return;
			}
			if (this.tempSelectedMenu != null) {
				return;
			}
			getCheckedMenuFromTree(treeModel.getChildren(), checkedMenu);
		}
	}
	
	public void checkTheChild(TreeModel<Menu> currentParent){
		List<TreeModel<Menu>> children = currentParent.getChildren();
		Menu dataParent = currentParent.getData();
		for (TreeModel<Menu> treeModel : children) {
			Menu data = treeModel.getData();
			data.setSelected(dataParent.isSelected());			
			checkTheChild(treeModel);
		}
	}
	
	public void checkTheParent(TreeModel<Menu> currentParent){
		Menu currentParentData = currentParent.getData();		
		TreeModel<Menu> parent = currentParent.getParent();
		
		if (parent != null && parent.getData() != null && 
				currentParentData != null) {
			Menu data = parent.getData();
			if (currentParentData.isSelected()) { // kalau di check doang parent nya di check
				data.setSelected(currentParentData.isSelected());
				checkTheParent(parent);
			}
		}
	}
	
	public void createTreeMenu(){
		this.menus = applicationBean.getMenus();
		rootMenu = new HashMap<Integer, Menu>();
		childMenu = new HashMap<Integer, Menu>();
		tree = new ListTreeModel<Menu>();
		for (Menu menu : this.menus) {
			// agar menu di applicationBean tidak berubah
			Menu tempMenu = new Menu(menu);
			
			Set<Menu> roleMenus = role.getMenus();
			for (Menu menu2 : roleMenus) {
				if (menu2.getMenuId()==tempMenu.getMenuId()) {
					tempMenu.setSelected(true);
				}
			}
			
			if (tempMenu.getMenuParentId() == 0) {
				rootMenu.put(menu.getMenuId(), tempMenu);
			}else{
				childMenu.put(menu.getMenuId(), tempMenu);
			}
		}
		
		for (Map.Entry<Integer, Menu> entry : rootMenu.entrySet()) {
			int parentMenuId = entry.getValue().getMenuId();
			TreeModel<Menu> currentRoot = tree.addChild(entry.getValue());
			createChild(currentRoot, parentMenuId, childMenu);
		}
	}
	
	public void createTempTreeMenu(){
		this.tempMenus = applicationBean.getMenus();
		tempRootMenu = new HashMap<Integer, Menu>();
		tempChildMenu = new HashMap<Integer, Menu>();
		tempTree = new ListTreeModel<Menu>();
		for (Menu menu : this.tempMenus) {
			// agar menu di applicationBean tidak berubah
			Menu tempMenu = new Menu(menu);
			
			Set<Menu> roleMenus = tempRole.getMenus();
			for (Menu menu2 : roleMenus) {
				if (menu2.getMenuId()==tempMenu.getMenuId()) {
					tempMenu.setSelected(true);
				}
			}
			
			if (tempMenu.getMenuParentId() == 0) {
				tempRootMenu.put(menu.getMenuId(), tempMenu);
			}else{
				tempChildMenu.put(menu.getMenuId(), tempMenu);
			}
		}
		
		for (Map.Entry<Integer, Menu> entry : tempRootMenu.entrySet()) {
			int parentMenuId = entry.getValue().getMenuId();
			TreeModel<Menu> currentRoot = tempTree.addChild(entry.getValue());
			createChild(currentRoot, parentMenuId, tempChildMenu);
		}
	}
	
	private void createChild(TreeModel<Menu> treeMenu, 
			int menuId, Map<Integer, Menu> childMenu){
		for (Map.Entry<Integer, Menu> entry : childMenu.entrySet()) {
			Menu tempMenu = entry.getValue();
			if (tempMenu.getMenuParentId() == menuId) {
				TreeModel<Menu> currentParent = treeMenu.addChild(tempMenu);				
				createChild(currentParent, tempMenu.getMenuId(), childMenu);
			}
		}
	}
	
	private void doLoging(String menu, String activity, String information) {
		UserLog userLog = new UserLog();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		userLog.setLogId("LOG-"+sdf.format(new Date())+"-"+sessionBean.getUser().getUserID());
		userLog.setActivity(activity);
		userLog.setInformation(information);
		userLog.setInsertTime(new Date());
		userLog.setMenu(menu);
		userLog.setUserId(sessionBean.getUser().getUserID());
		userLog.setUserName(sessionBean.getUser().getUserName());
		UserLogHandler.doLog(userLog, getUserDAOBean());
	}
	
	private UserDao getUserDAOBean() {
		return (UserDao) applicationBean.getCorpGWContext().getBean("userDAO");
	}
	
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}
	public Menu getMenu() {
		return menu;
	}
	public void setMenu(Menu menu) {
		this.menu = menu;
	}
	public ApplicationBean getApplicationBean() {
		return applicationBean;
	}
	public void setApplicationBean(ApplicationBean applicationBean) {
		this.applicationBean = applicationBean;
	}
	public SessionBean getSessionBean() {
		return sessionBean;
	}
	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}
	public LazyDataModel<Role> getRoleList() {
		return roleList;
	}
	public void setRoleList(LazyDataModel<Role> roleList) {
		this.roleList = roleList;
	}

	public List<Menu> getMenus() {
		return menus;
	}

	public void setMenus(List<Menu> menus) {
		this.menus = menus;
	}

	public TreeModel<Menu> getTree() {
		return tree;
	}

	public void setTree(TreeModel<Menu> tree) {
		this.tree = tree;
	}

	public HashMap<Integer, Menu> getRootMenu() {
		return rootMenu;
	}

	public void setRootMenu(HashMap<Integer, Menu> rootMenu) {
		this.rootMenu = rootMenu;
	}

	public HashMap<Integer, Menu> getChildMenu() {
		return childMenu;
	}

	public void setChildMenu(HashMap<Integer, Menu> childMenu) {
		this.childMenu = childMenu;
	}

	public TreeModel<Menu> getSelectedMenu() {
		return selectedMenu;
	}

	public void setSelectedMenu(TreeModel<Menu> selectedMenu) {
		this.selectedMenu = selectedMenu;
	}
	
}
