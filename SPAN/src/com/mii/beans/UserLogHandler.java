package com.mii.beans;

import com.mii.dao.UserDao;
import com.mii.models.UserLog;

public class UserLogHandler {
	public static void doLog(UserLog userLog, UserDao userDao){
		int size = userLog.getInformation().length();
		if(size>500){
			size=500;
		}
		userLog.setInformation(userLog.getInformation().substring(0, size));
		userDao.save(userLog);
	}
	
	public static String validateMessage(String information){
		return information;
	}
	
}
