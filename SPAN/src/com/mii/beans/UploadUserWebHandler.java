package com.mii.beans;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.UploadedFile;

import com.mii.constant.Constants;
import com.mii.dao.ApprovalDao;
import com.mii.dao.BranchDao;
import com.mii.dao.RoleDao;
import com.mii.dao.SystemParameterDao;
import com.mii.dao.UserDao;
import com.mii.helpers.EmailValidator;
import com.mii.helpers.FacesUtil;
import com.mii.helpers.HelperUtils;
import com.mii.helpers.LdapConnector;
import com.mii.models.Approval;
import com.mii.models.Branch;
import com.mii.models.Role;
import com.mii.models.SystemParameter;
import com.mii.models.User;
import com.mii.models.UserRole;
import com.mii.models.UserUpload;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.SessionBean;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
 


import org.primefaces.event.CloseEvent;

@ManagedBean
@ViewScoped
public class UploadUserWebHandler {
	private static final long serialVersionUID = 1L;
		
	private LazyDataModel<User> userList;
	
	private UploadedFile file;
	
	List<UserRole> roles = new ArrayList<UserRole>();
	List<String> listError = new ArrayList<String>();
	
	@ManagedProperty(value="#{applicationBean}")
	private ApplicationBean applicationBean;
	@ManagedProperty(value="#{sessionBean}")
	private SessionBean sessionBean;
	private boolean doViewError;
	int error ;
	
	@PostConstruct
	public void init(){
		FacesUtil.authMenu(sessionBean.getUser(), "uploadUser");

		userList = new UserDataModel();
		file = null;
		
		clearUserTemp();
		doViewError = false;
	}
	
	
	public List<String> getListError() {
		return listError;
	}


	public void setListError(List<String> listError) {
		this.listError = listError;
	}


	private ApprovalDao getApprovalDAOBean(){
		return (ApprovalDao) applicationBean.getCorpGWContext().getBean("approvalDAO");
	}
	
	private BranchDao getBranchDAOBean(){
		return (BranchDao) applicationBean.getCorpGWContext().getBean("branchDAO");
	}
	
	public LazyDataModel<User> getUserList() {
		return userList;
	}
	
	public UploadedFile getFile() {
	        return file;
    }
 
    public void setFile(UploadedFile file) {
        this.file = file;
    }

	public void setUserList(LazyDataModel<User> userList) {
		this.userList = userList;
	}

	public ApplicationBean getApplicationBean() {
		return applicationBean;
	}
	
	public void setApplicationBean(ApplicationBean applicationBean) {
		this.applicationBean = applicationBean;
	}
	
	public SessionBean getSessionBean() {
		return sessionBean;
	}
	
	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	private UserDao getUserDAOBean(){
		return (UserDao) applicationBean.getCorpGWContext().getBean("userDAO");
	}
	
	private SystemParameterDao getSystemParameterDAOBean(){
		return (SystemParameterDao) applicationBean.getCorpGWContext().getBean("systemParameterDAO");
	}
	
	public void clearUserTemp(){
		UserDao userDao = (UserDao) applicationBean.getCorpGWContext().getBean("userDAO");
		userDao.deleteAllUserUpload(sessionBean.getUser().getUserID());
	}
	
	public void uploadFile(){
		doViewError = false;
		 
		if (!(file.getFileName().equals(null)||file.getFileName().equals(""))) {
			String fileName = file.getFileName();
			String extension = fileName.substring(fileName.lastIndexOf("."));
			if (StringUtils.equalsIgnoreCase(extension, ".xls")||StringUtils.equalsIgnoreCase(extension, ".xlsx")) {
				if(file.getSize()<=1000000){
					UserDao userDao = (UserDao) applicationBean.getCorpGWContext().getBean("userDAO");
					userDao.deleteAllUserUpload(sessionBean.getUser().getUserID());
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
					Date CREATEDATE = HelperUtils.getCurrentDateTime();
					Calendar cal = Calendar.getInstance();
					cal.add(Calendar.YEAR, 5);
					int count = 1;
					try{
						BufferedInputStream input = new BufferedInputStream(this.file.getInputstream());
				        POIFSFileSystem fs = new POIFSFileSystem( input );
				        HSSFWorkbook wb = new HSSFWorkbook(fs);
				        HSSFSheet sheet = wb.getSheetAt(0);
				        int i=0;
				        String validatormessage = "";
				        boolean validator = true;
			        
				        listError.clear();
				        for (Row myrow : sheet) {
				        	UserUpload user = new UserUpload();
			        		int x=0;
			        		if(i>=1){
				        		for (Cell mycell : myrow) {
				        			if(x==0){
				        				if (!mycell.getStringCellValue().isEmpty()){
				        					user.setUserName(mycell.getStringCellValue());
				        				}else{
//				        					throw new Exception("Username is required");
				        					listError.add("Error at row "+count+ " : Username is required; " + System.getProperty("line.separator"));
				        				}
				        			}else if(x==1){
				        				if (!mycell.getStringCellValue().isEmpty()){
				        					user.setFullName(mycell.getStringCellValue());
					        			}else{
//				        					throw new Exception("Full Name is required");
				        					listError.add("Error at row "+count+ " : Full Name is required; ");
				        				}
				        			}else if(x==2){
				        				if (!mycell.getStringCellValue().isEmpty()){
				        					EmailValidator emailValidator = new  EmailValidator();
				        					if (emailValidator.basicvalidate(mycell.getStringCellValue())){
					        					user.setEmail(mycell.getStringCellValue());
				        					}else{
//				        						throw new Exception("Invalid email");
				        						listError.add("Error at row "+count+ " : Invalid email; ");
				        					}
										}
				        			}else if(x==3){
				        				if (!mycell.getStringCellValue().isEmpty()){
				        					user.setDivision(mycell.getStringCellValue());
										}
				        			}else if(x==4){
				        				if (!mycell.getStringCellValue().isEmpty()){
				        					user.setDepartment(mycell.getStringCellValue());
					        			}
				        			}else if(x==5){
				        				if (!mycell.getStringCellValue().isEmpty()){
				        					user.setPhone(mycell.getStringCellValue());
					        			}
				        			}else if(x==6){
				        				if (!mycell.getStringCellValue().isEmpty()){
				        					user.setMobileNumber(mycell.getStringCellValue());
					        			}
				        			}else if(x==7){
				        				if (!mycell.getStringCellValue().isEmpty()){
				        					user.setContactNumber(mycell.getStringCellValue());
					        			}
				        			}else if(x==8){
				        				if (!mycell.getStringCellValue().isEmpty()){
				        					Branch branch = getBranchDAOBean().getBranchByBranchCode(mycell.getStringCellValue());
				        					if (branch != null){
					        					user.setBranch(mycell.getStringCellValue());
				        					}else{

//					        					throw new Exception("Branch is not registered");
					        					listError.add("Error at row "+count+ " : Branch is not registered; ");
				        					}
					        			}else{
//				        					throw new Exception("Branch is required");
				        					listError.add("Error at row "+count+ " : Branch is required; ");
				        				}
				        			}else if(x==9){
				        				if (!mycell.getStringCellValue().isEmpty()){
				        					user.setTellerId(mycell.getStringCellValue());
					        			}
				        			}else if(x==10){
				        				if (String.valueOf(mycell.getNumericCellValue()) != ""){
					        				user.setDebitLimitTransaction(new BigInteger(mycell.getStringCellValue()));
				        				}else{
//				        					throw new Exception("Debit Limit Transaction is required");
				        					listError.add("Error at row "+count+ " : Debit Limit Transaction is required; ");
				        				}
				        			}else if(x==11){
				        				if (!mycell.getStringCellValue().isEmpty()){
				        					RoleDao roleDao = (RoleDao) applicationBean.getCorpGWContext().getBean("roleDAO");
				        						
				        					Role role = roleDao.getAllValidRoleByRoleName(mycell.getStringCellValue());
				        					if (role != null){
				        						user.setRoleName(mycell.getStringCellValue());
				        					}else{
//				        						throw new Exception("Role is not registered");
				        						listError.add("Error at row "+count+ " : Role is not registered; ");
				        					}
					        			}
				        			}
				        			x++;
				    			}
				        		user.setCreateWho(sessionBean.getUser().getUserID());
				        		user.setCreateDate(HelperUtils.getCurrentDateTime());
				        		
				        		sdf = new SimpleDateFormat("yyyyMMdd-hhmm-sss");
				        		Date date = new Date();
				        		String id = sdf.format(date);
				        		Integer seq = getSystemParameterDAOBean().getParameterSequence().intValue();
				        		user.setUserID("USR-"+id+String.format("%04d", seq));
				        		
				        		
				        		if (!validateDuplicateUser(user)){
				        			userDao.deleteAllUserUpload(sessionBean.getUser().getUserID());
//				        			throw new Exception("User " + user.getUserName() + " is duplicate.");
				        			listError.add("Error at row "+count+ " : User " + user.getUserName() + " is duplicate; ");
				        		}else{
				        			if (!validateUser(user)){
					        			userDao.deleteAllUserUpload(sessionBean.getUser().getUserID());
//					        			throw new Exception("User " + user.getUserName() + " already exist in the application.");
					        			listError.add("Error at row "+count+ " : User " + user.getUserName() + " already exist in the application; "+ System.getProperty("line.separator"));
					        		}else{
					        			if(!validateLDAP(user)){
					        				userDao.deleteAllUserUpload(sessionBean.getUser().getUserID());
//						        			throw new Exception("User " + user.getUserName() + " not exist in LDAP or/ wrong password.");
						        			listError.add("Error at row "+count+ " : User " + user.getUserName() + " not exist in LDAP or/ wrong password; ");
					        			}else{
					        				if(validator){
					        					user.setStatus(Constants.APPROVAL_STATUS_PENDING_CREATE);
					        					getUserDAOBean().saveUserUploadToDB(user);
					        				}else{
					        					userDao.deleteAllUserUpload(sessionBean.getUser().getUserID());
//							        			throw new Exception(user.getUserID() +" "+ validatormessage);
							        			listError.add(user.getUserID() +" "+ validatormessage );
					        				}
							        	}
					        		}
				        		}
				        		
			        		}
			        		i++;
			        		count++;
//				        }catch(Exception e){
//				        	e.printStackTrace();
//				        }
				        }
				        
				        if(listError.size() > 0){
				        	userDao.deleteAllUserUpload(sessionBean.getUser().getUserID());
					        doViewError = true;
				        }else{
				        	Integer jmlAll = userDao.countAllUserUpload(sessionBean.getUser().getUserID());
					        String headerMessage = "Success";
							String bodyMessage = jmlAll+" record succesfuly uploaded.";

							FacesUtil.setInfoMessage(null, bodyMessage, bodyMessage);
				        }
				        
				        
					}catch(IOException ioe){
						
				        userDao.deleteAllUserUpload(sessionBean.getUser().getUserID());
				        String headerMessage = "Error";
						String bodyMessage = "Got Error on file row number "+count+" , please check your file and upload again.";

//						FacesUtil.setFatalMessage(null,bodyMessage, bodyMessage);
						listError.add("Got Error on file row number "+count+" , please check your file and upload again; ");
				    }catch(Exception e){
				    	e.printStackTrace();
				    	userDao.deleteAllUserUpload(sessionBean.getUser().getUserID());
				    	String headerMessage = "Error";
						String bodyMessage = "Got Error on file row number "+count+", "+e.getMessage();
						
						FacesUtil.setFatalMessage(null,bodyMessage, bodyMessage);
						

				    }
				}else{
//					listError.add("Maximum file size is 1MB");
					FacesUtil.setFatalMessage(null, "Maximum file size is 1MB", null);
				}
			}else{
//				listError.add(file.getFileName()+" is not .xls / .xlsx type");
				FacesUtil.setFatalMessage(null, file.getFileName()+" is not .xls / .xlsx type", null);
			}
		}else{
//			listError.add("File is required");
			FacesUtil.setFatalMessage(null, "File is required", null);
		}
		
	}
	
	

	public void insertAll(){
		UserDao userDao = (UserDao) applicationBean.getCorpGWContext().getBean("userDAO");
		List<User> users;
		
		Integer jmlAll = userDao.countAllUserUpload(sessionBean.getUser().getUserID());
		
		users = userDao.getAllUserUploadLazyLoading(jmlAll, 0, sessionBean.getUser().getUserID());

		RoleDao roleDao = (RoleDao) applicationBean.getCorpGWContext().getBean("roleDAO");
		
		for(User user : users){
			Role role = roleDao.getDetailedRoleByRoleName(user.getRoleNameTemp());
			String roleId = role.getRoleId();
			UserRole userrole = new UserRole();
			userrole.setRole(roleId);	
			userrole.setUserId(user.getUserID());
			roles = new ArrayList<UserRole>();
			roles.add(userrole);
			roles.get(0).setUserId(user.getUserID());
			user.setRoles(roles);
			user.setStatus(Constants.APPROVAL_STATUS_PENDING_CREATE);
			user.setCreateDate(new Date());
			user.setCreateWho(sessionBean.getUser().getUserName());
			
			Approval approval = new Approval();
			approval.setAction(Constants.APPROVAL_ACTION_CREATE);
			approval.setParameter(Constants.APPROVAL_PARAMETER_USER);
			approval.setParameterName(user.getUserName());
			approval.setParameterId(user.getUserID());
			approval.setBranchId(sessionBean.getUser().getBranch());
			try {
				approval.setNewByteObject(HelperUtils.serialize(user));
			} catch (IOException e) {
				e.printStackTrace();
			}
			getApprovalDAOBean().saveApproval(approval);
			
			userDao.saveUserToDB(user);
			try {
				getApprovalDAOBean().updateApprovalNewObjectByParameterId(HelperUtils.serialize(user),
						user.getUserID());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		userDao.deleteAllUserUpload(sessionBean.getUser().getUserID());
		String headerMessage = "Success";
		String bodyMessage = "All User Inserted";

		FacesUtil.setInfoMessageDialog(headerMessage, bodyMessage);
	}
	
	public boolean validateUser(User user){
		UserDao userDao = (UserDao) applicationBean.getCorpGWContext().getBean("userDAO");
		User result = userDao.getByUserName(user.getUserName());
		if (result == null)
			return true;
		else
			return false;
	}
	
	public boolean validateDuplicateUser(User user){
		UserDao userDao = (UserDao) applicationBean.getCorpGWContext().getBean("userDAO");
		int result = userDao.countUserUploadByUserName(user.getUserName());
		if (result == 0)
			return true;
		else
			return false;
	}
	
	public boolean validateLDAP(User user){
		if (LdapConnector.authenticate(sessionBean.getUser().getUserName(), sessionBean.getUser().getPassword(),user.getUserName())){
			return true;
		}
		return false;
	}
	
	private class UserDataModel extends LazyDataModel<User>{
		private static final long serialVersionUID = 1L;
		public UserDataModel() {
			
		}
		@Override
		public List<User> load(int startingAt, 
				int maxPerPage, String sortField, 
				SortOrder sortOrder, Map<String, String> filters) {
			
			UserDao userDao = (UserDao) applicationBean.getCorpGWContext().getBean("userDAO");
			List<User> users;
			
			users = userDao.getAllUserUploadLazyLoading(maxPerPage+startingAt, startingAt,sessionBean.getUser().getUserID());
			Integer jmlAll = userDao.countAllUserUpload(sessionBean.getUser().getUserID());
			setRowCount(jmlAll);
			setPageSize(maxPerPage);
		
			return users;
		}
	}

	public boolean isDoViewError() {
		return doViewError;
	}


	public void setDoViewError(boolean doViewError) {
		this.doViewError = doViewError;
	}


	public int getError() {
		return error;
	}


	public void setError(int error) {
		this.error = error;
	}



	
}
