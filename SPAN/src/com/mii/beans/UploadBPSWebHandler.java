package com.mii.beans;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

//import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
//import org.apache.poi.util.StringUtil;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.UploadedFile;

//import com.jcraft.jsch.Channel;
//import com.jcraft.jsch.ChannelSftp;
//import com.jcraft.jsch.JSch;
//import com.jcraft.jsch.Session;
import com.mii.dao.SystemParameterDao;
import com.mii.dao.UploadBPSHistoryDao;
import com.mii.helpers.FacesUtil;
import com.mii.models.SystemParameter;
import com.mii.models.UploadBPSHistory;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.SessionBean;

@ManagedBean
@ViewScoped
public class UploadBPSWebHandler implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private UploadedFile file;
	
	private LazyDataModel<UploadBPSHistory> uploadBPSHistoryList;
	
	private String fileName;
	private Date uploadDate;
	private String uploadBy;
	private String status;

	@ManagedProperty(value="#{applicationBean}")
	private ApplicationBean applicationBean;
	@ManagedProperty(value="#{sessionBean}")
	private SessionBean sessionBean;
	
	private boolean doShowSearch = false;
	
	@PostConstruct
	public void init(){
		FacesUtil.authMenu(sessionBean.getUser(), "uploadBPS");

		uploadBPSHistoryList = new UploadBPSHistoryListDataModel();
		file = null;
		fileName = "";
		uploadDate = null;
		uploadBy = "";
		status = "";
	}
	
	public UploadedFile getFile() {
		return file;
	}

	public void setFile(UploadedFile file) {
		this.file = file;
	}

	public ApplicationBean getApplicationBean() {
		return applicationBean;
	}

	public void setApplicationBean(ApplicationBean applicationBean) {
		this.applicationBean = applicationBean;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}
	
	public LazyDataModel<UploadBPSHistory> getUploadBPSHistoryList() {
		return uploadBPSHistoryList;
	}

	public void setUploadBPSHistoryList(
			LazyDataModel<UploadBPSHistory> uploadBPSHistoryList) {
		this.uploadBPSHistoryList = uploadBPSHistoryList;
	}
	
	public boolean isDoShowSearch() {
		return doShowSearch;
	}

	public void setDoShowSearch(boolean doShowSearch) {
		this.doShowSearch = doShowSearch;
	}

	public void showSearch(){
		doShowSearch = true;
	}
	
	public void hideSearch(){
		doShowSearch = false;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Date getUploadDate() {
		return uploadDate;
	}

	public void setUploadDate(Date uploadDate) {
		this.uploadDate = uploadDate;
	}

	public String getUploadBy() {
		return uploadBy;
	}

	public void setUploadBy(String uploadBy) {
		this.uploadBy = uploadBy;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	private SystemParameterDao getSystemParameterDAOBean(){
		return (SystemParameterDao) applicationBean.getCorpGWContext().getBean("systemParameterDAO");
	}
	
	public void doSearch(){

		FacesUtil.resetPage("uploadBPSHistoryForm:uploadBPSHistoryTable");
		uploadBPSHistoryList = new UploadBPSHistoryListDataModel();
		
	}

	public void uploadFile(){
		UploadBPSHistoryDao uploadBPSHistoryDao = (UploadBPSHistoryDao) applicationBean.getCorpGWContext().getBean("uploadBPSHistoryDao");
		UploadBPSHistory history = new UploadBPSHistory();
		history.setFileName(file.getFileName());
		history.setFileSize(new BigDecimal(file.getSize()));
		history.setUploadDate(new Date());
		history.setUploadBy(sessionBean.getUser().getUserName());
		
		try{
			SystemParameter workDirBps = getSystemParameterDAOBean().getDetailedParameterByParamName("BPS_TARGET_DIRECTORY");
			SystemParameter workDirCa = getSystemParameterDAOBean().getDetailedParameterByParamName("CA_TARGET_DIRECTORY");
			if (file.getFileName().contains("BPS")){
				save( workDirBps.getParamValue() + file.getFileName());
			}else if ((file.getFileName().contains("IDR"))){
				save(workDirCa.getParamValue() + file.getFileName());
			}else{

				String headerMessage = "Filename is not valid";

				FacesUtil.setErrorMessage(null,headerMessage, headerMessage);
				throw new Exception("Invalid file");
			}

			String headerMessage = "Upload File '"+file.getFileName()+"' Successfully";

			FacesUtil.setInfoMessage(null,headerMessage, headerMessage);
			
			
			history.setStatus("Success");
		
		}catch(Exception e){
			e.printStackTrace();
			
			String headerMessage = "Upload File '"+file.getFileName()+"' Failed";

			FacesUtil.setErrorMessage(null,headerMessage, headerMessage);
			

			String status = e.getMessage();
			if (status.length() >= 150) status = status.substring(0, 149);
			history.setStatus(status);
		}
		
		uploadBPSHistoryDao.saveHistory(history);
	}
	
	public void save(String localPath) throws IOException{

		InputStream input =  file.getInputstream();
		OutputStream output = new FileOutputStream(new File(localPath));
		try{
			IOUtils.copy(input, output);
		}finally{
			IOUtils.closeQuietly(input);
			IOUtils.closeQuietly(output);
		}
		
	}
	
	private class UploadBPSHistoryListDataModel extends LazyDataModel < UploadBPSHistory > {
		private static final long serialVersionUID = 1L;

		public UploadBPSHistoryListDataModel() {

		}

		@Override
		public List < UploadBPSHistory > load(int startingAt, int maxPerPage,
		String sortField, SortOrder sortOrder,
			Map < String, String > filters) {
	
			UploadBPSHistoryDao uploadBPSHistoryDao = (UploadBPSHistoryDao) applicationBean.getCorpGWContext().getBean("uploadBPSHistoryDao");
			List < UploadBPSHistory > uploadBPSHistory = null;
	
			uploadBPSHistory = uploadBPSHistoryDao.getSearchUploadBPSHistoryLazyLoading(fileName, uploadDate, uploadBy, status, maxPerPage + startingAt, startingAt);
			Integer jmlAll = uploadBPSHistoryDao.countSearchUploadBPSHistory(fileName, uploadDate, uploadBy, status);
			setRowCount(jmlAll);
			setPageSize(maxPerPage);
				
			return uploadBPSHistory;
		}
	}
	
}
