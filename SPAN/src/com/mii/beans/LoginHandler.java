package com.mii.beans;

import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.servlet.http.HttpServletRequest;

import com.mii.bjb.core.UIMBJBConnector;
import com.mii.bjb.core.UIMBJBConstants;
import com.mii.bjb.core.UIMBJBHelper;
import com.mii.bjb.core.UIMBJBModel;
import com.mii.constant.Constants;
import com.mii.constant.ConstantsUserLog;
import com.mii.dao.ApprovalDao;
import com.mii.dao.RoleDao;
import com.mii.dao.SPANDataValidationDao;
import com.mii.dao.SystemParameterDao;
import com.mii.dao.UserDao;
import com.mii.helpers.ChaptaKeyResolver;
import com.mii.helpers.EncryptUtils;
import com.mii.helpers.FacesUtil;
import com.mii.helpers.HelperUtils;
import com.mii.helpers.LdapConfigurator;
import com.mii.helpers.LdapConnector;
import com.mii.is.GetSPANSummaries;
import com.mii.is.io.GetSPANSummariesRequest;
import com.mii.is.io.GetSPANSummariesResponse;
import com.mii.is.io.SpanSummariesType0Input;
import com.mii.is.io.SpanSummariesType0Output;
import com.mii.models.Role;
import com.mii.models.SystemParameter;
import com.mii.models.User;
import com.mii.models.UserLog;
import com.mii.models.UserRole;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.SessionBean;

@ManagedBean
@RequestScoped
@FacesValidator(value = "captchaValidator")
public class LoginHandler implements Serializable, Validator {
	private static final long serialVersionUID = 1L;
	private User user = new User();
	private String chImage;
	
	@ManagedProperty(value = "#{sessionBean}")
	private SessionBean sessionBean;
	
	@ManagedProperty(value = "#{applicationBean}")
	private ApplicationBean applicationBean;

	private ExternalContext externalContext;
	private HttpServletRequest httpServletRequest;
	private String sessionId;
	
	private String password;
	
	@PostConstruct
	public void init(){
		
	}
	
	public RoleDao getRoleDaoBean(){
		RoleDao roleDao = (RoleDao) applicationBean.getCorpGWContext().getBean("roleDAO");
		return roleDao;
	}
	
	public UserDao getUserDaoBean(){
		return (UserDao) applicationBean.getCorpGWContext().getBean("userDAO");
	}
		
	
	public LoginHandler() {
		externalContext = FacesContext.getCurrentInstance().getExternalContext();
		
		httpServletRequest = (HttpServletRequest) externalContext.getRequest();
	
		sessionId = httpServletRequest.getSession().getId();
		System.out.println("externalContext"+externalContext);
		System.out.println("session"+sessionId);
	}
	
	private SystemParameterDao getSystemParameterDAOBean(){
		return (SystemParameterDao) applicationBean.getCorpGWContext().getBean("systemParameterDAO");
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	String validate;

	public String getChImage() {
		return chImage;
	}

	public void setChImage(String chImage) {
		this.chImage = chImage;
	}

	public String getValidate() {
		return validate;
	}

	public void setValidate(String validate) {
		this.validate = validate;
	}

	public void keepUserSessionAlive() {
		FacesContext context = FacesContext.getCurrentInstance();
		System.out.println("context"+context);
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		System.out.println("session"+request.getSession());
		request.getSession();
	}

	public String doLogin(){
		System.out.println("sini 00");
		UIMBJBModel uimBJBModel = new UIMBJBModel();
		boolean checked = false;
		System.out.println("sini 01");
		String usernameBck = user.getUserName();
		try {
			System.out.println("sini 01");
			uimBJBModel = sendToUIM(user.getUserName(), password.toString());
			System.out.println("sini 02");
			//TEMP connect ke eq BJB
			uimBJBModel.setResponseMessage("SUCCESS");
			checked = checkAkun(user.getUserName());
			System.out.println("sini 03 "+checked);
		} catch (Exception e) {
			e.printStackTrace();
		}
		   System.out.println("sini 1 " + uimBJBModel.getResponseMessage().equals(UIMBJBConstants.UIM_SUCCESS));
		if (uimBJBModel.getResponseMessage().equals(UIMBJBConstants.UIM_SUCCESS)){
			usernameBck = usernameBck.toUpperCase();
			 System.out.println("sini 11 "+usernameBck);
			if (checked) {
				UserDao userDao = getUserDAOBean();
				 System.out.println("sini 12 "+userDao);
				user.setLastLogin(HelperUtils.getCurrentDateTime());
				 
				userDao.editUser(this.user);
				 System.out.println("sini 13 "+this.user);
				//Get Role by UIMROLEID
					//TEMP get UIM ROLE ke eq BJB
				 uimBJBModel.setUimRoleId("403");
				Role role = getRoleDaoBean().getRoleByUIMRoleId(uimBJBModel.getUimRoleId());
				 System.out.println("sini 14 "+uimBJBModel.getUimRoleId());
				//Update Map User and Role
				getUserDaoBean().updateUserRole(user.getUserID(), role.getRoleId());
				FacesUtil.getExternalContext().getSessionMap().put("user", user);
				sessionBean.setUser(getUser());
				sessionBean.buildMenu(getUser());
				  System.out.println("sini 15");
				if(user.getFirstLogin().equalsIgnoreCase(Constants.FISTLOGINYES)){
					doLoging(ConstantsUserLog.MENU_LOGIN, ConstantsUserLog.ACTIVITY_LOGIN, "Login Success, First Login.");
					 System.out.println("sini 16");
					return "newPasswordSPAN?faces-redirect=true";
				
				}else{
					doLoging(ConstantsUserLog.MENU_LOGIN, ConstantsUserLog.ACTIVITY_LOGIN, "Login Success, UIM Login.");
					System.out.println("sini 22");
					return "welcome?faces-redirect=true";
				}
				
			}else{
				UserDao userDao = getUserDAOBean();
				user.setUserName(usernameBck);
				user.setUserID(GenerateUserID());
				user.setLastLogin(HelperUtils.getCurrentDateTime());
				user.setFullName(user.getUserName());
				user.setStatus(Constants.APPROVAL_STATUS_APPROVED_CREATE);
				user.setFirstLogin(Constants.FISTLOGINNO);
				user.setBranch(Constants.USER_DEFAULT_BRANCH);
				
				userDao.save(this.user);
				System.out.println("sini 21 " +this.user);
				//Get Role by UIMROLEID
				Role role = getRoleDaoBean().getRoleByUIMRoleId(uimBJBModel.getUimRoleId());
				//Map User And Role
				System.out.println("sini 3");
				getUserDaoBean().insertUserRole(user.getUserID(), role.getRoleId());
				try {
					checkAkun(user.getUserName());
				} catch (Exception e) {
					System.out.println("Failed To Map UIM into SPAN Application");
				}
				FacesUtil.getExternalContext().getSessionMap().put("user", user);
				sessionBean.setUser(getUser());
				sessionBean.buildMenu(getUser());
				doLoging(ConstantsUserLog.MENU_LOGIN, ConstantsUserLog.ACTIVITY_LOGIN, "Login Success, UIM Login.");
				System.out.println("sini 4");
			}
			return "welcome?faces-redirect=true";
		}else {
			if(uimBJBModel.isInternalUser()){
				if(uimBJBModel.getResponseMessage().equals(UIMBJBConstants.UIM_PASSWORD_KADALUARSA_MESSAGE)){
					if (checked) {
						UserDao userDao = getUserDAOBean();
						user.setLastLogin(HelperUtils.getCurrentDateTime());
						user.setFirstLogin(Constants.FISTLOGINCHANGE);
						user.setPassword(password);
						userDao.editUser(this.user);
						FacesUtil.getExternalContext().getSessionMap().put("user", user);
						sessionBean.setUser(getUser());
						sessionBean.buildMenu(getUser());
						doLoging(ConstantsUserLog.MENU_LOGIN, ConstantsUserLog.ACTIVITY_LOGIN, "Login Success, Password Expired.");
						return "newPasswordSPAN?faces-redirect=true";
						
					}else{
						FacesUtil.setErrorMessage("msg", "UIM password Expired. Can't change password. Username not registered on this aplication.",
								null);
					}
				}else{
					FacesUtil.setErrorMessage("msg", uimBJBModel.getResponseMessage(),null);
				}
			}else{
				if(uimBJBModel.isTryLoginDepkeu()){
					if (checked) {
						if (validatePassword(user.getUserName(),password.toString())) {
							UserDao userDao = getUserDAOBean();
							user.setLastLogin(HelperUtils.getCurrentDateTime());
							userDao.editUser(this.user);
							FacesUtil.getExternalContext().getSessionMap().put("user", user);
							sessionBean.setUser(getUser());
							sessionBean.buildMenu(getUser());
							
							if(user.getFirstLogin().equalsIgnoreCase(Constants.FISTLOGINNO)){
								doLoging(ConstantsUserLog.MENU_LOGIN, ConstantsUserLog.ACTIVITY_LOGIN, "Login Success, Normal Login.");
								return "welcome?faces-redirect=true";
							}else{
								doLoging(ConstantsUserLog.MENU_LOGIN, ConstantsUserLog.ACTIVITY_LOGIN, "Login Success, First Login.");
								return "newPasswordSPAN?faces-redirect=true";
							}
						}else{
							FacesUtil.setErrorMessage("msg", "Username or password is incorect",
									null);
						}
					}else{
						FacesUtil.setErrorMessage("msg", "Username or password is incorect.",null);
					}
				}else{
					FacesUtil.setErrorMessage("msg", uimBJBModel.getResponseMessage(),null);
				}
			}
		}
		return null;
	}

	private void doLoging(String menu, String activity, String information) {
		UserLog userLog = new UserLog();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		userLog.setLogId("LOG-"+sdf.format(new Date())+"-"+sessionBean.getUser().getUserID());
		userLog.setActivity(activity);
		userLog.setInformation(information);
		userLog.setInsertTime(new Date());
		userLog.setMenu(menu);
		userLog.setUserId(sessionBean.getUser().getUserID());
		userLog.setUserName(sessionBean.getUser().getUserName());
		UserLogHandler.doLog(userLog, getUserDAOBean());
	}
	
	public UIMBJBModel sendToUIM(String username, String password) throws IOException{
		String request = UIMBJBHelper.packUIMRequestLogin(username,password, getSystemParameterDAOBean());
		String response = UIMBJBConnector.doSendRequest(request, getSystemParameterDAOBean());
		UIMBJBModel uimBJBModel = UIMBJBHelper.unpackUIMResponse(response,UIMBJBConstants.UIM_FLAG_LOGIN);
		return uimBJBModel;
	}
	
	public boolean validatePassword(String username, String password){
		UserDao userDao = getUserDAOBean();
		User user = userDao.getByUserName(username);
		if(user.getFirstLogin().equals(Constants.FISTLOGINYES)){
			if(user.getPassword().equalsIgnoreCase(password)){
				return true;
			}else{
				return false;
			}
		}else{
			if(EncryptUtils.DecodeDecrypt(user.getPassword()).equalsIgnoreCase(password)){
				return true;
			}else{
				return false;
//				return true;
			}
		}
		
	}

	public boolean checkAkun(String username) throws Exception {

		boolean status = false;

		UserDao userDao = getUserDAOBean();
		this.user = userDao.getValidUserByUsername(username);

		if (this.user != null && this.user.getUserName().equalsIgnoreCase(username)) {
			
			status = true;
		}else{
			this.user = new User();
		}
		return status;
	}

	private UserDao getUserDAOBean() {
		UserDao userDao = (UserDao) applicationBean.getCorpGWContext().getBean(
			"userDAO");
		return userDao;
	}

	public String doLogout() {
		FacesUtil.getExternalContext().getSessionMap().remove("user");
		return "loginSPAN?faces-redirect=true";
	}
	
	public String doChangePassword(){
		return "changePasswordConfirm?faces-redirect=true";
	}
	
	public void killAnotherSession(User user) {
		User.getLogins().remove(user);
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public ApplicationBean getApplicationBean() {
		return applicationBean;
	}

	public void setApplicationBean(ApplicationBean applicationBean) {
		this.applicationBean = applicationBean;
	}

	@Override
	public void validate(FacesContext context, UIComponent component,
	Object value) throws ValidatorException {
		String userInput = (String) value;
		String captcha = null;
		System.out.println("capcha 1");
        captcha = ChaptaKeyResolver.getInstance().chaptchaTemp.get(sessionId);
        System.out.println("capcha 2");
		if (userInput == null || userInput == "" || captcha == null || !userInput.equals(captcha)) {
			System.out.println("capcha is active");
			throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Wrong Captcha", 
                    null));
                   
		}
        System.out.println("capcha is not active and not required!");
       

		ChaptaKeyResolver.getInstance().chaptchaTemp.remove(sessionId);

	}
	
	public String GenerateUserID(){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd-hhmm-sss");
		Date date = new Date();
		String id = sdf.format(date);
		Integer seq = getSystemParameterDAOBean().getParameterSequence().intValue();
		return "USR-"+id+String.format("%04d", seq);
	}

}