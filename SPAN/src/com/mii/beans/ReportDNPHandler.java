package com.mii.beans;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;




import com.mii.dao.ReportDNPDao;
import com.mii.helpers.FacesUtil;
import com.mii.models.ReportDNPM;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.SessionBean;


@ManagedBean
@ViewScoped
public class ReportDNPHandler implements Serializable {

	private static final long serialVersionUID = 1L;
	@ManagedProperty(value="#{applicationBean}")
	private ApplicationBean applicationBean;
	@ManagedProperty(value="#{sessionBean}")
	private SessionBean sessionBean;
	private ReportDNPM transactionDetailM = new ReportDNPM();
	private List<ReportDNPM> listReport = new ArrayList<ReportDNPM>();
	private List<ReportDNPM> listAwal;
	private Date tglbuku;
	private Date tahunDate;
	private BigDecimal totalSetoran;
	private int totalTrx;
	private String noSakti;
	private String ntpn;
	private String kodeBilling,kodeBillingS;
	private String tglbukuS,tahunDateS,currencyS;
	private String currency;
	private String jmlSet,jmlTrx;
	private String nameFileDNPUSD,nameFileDNPIDR,letakFile,paramNamePF,kodeBank,paramNameKB;
	
	@PostConstruct
	public void init() {
		FacesUtil.authMenu(sessionBean.getUser(), "reportDNP");

	}
	
	public ReportDNPHandler() {

		tglbukuS="TO_CHAR(SYSDATE, 'MMdd')";
		tahunDateS="TO_CHAR(SYSDATE, 'yyyy')";
		kodeBillingS = "";
		paramNamePF="";
		paramNameKB="";
		kodeBank="";
		
		Date date=new Date();
		tglbuku=date;
		tahunDate=date;
		nameFileDNPUSD="";
		nameFileDNPIDR="";
		currency = "IDR";
	}


	public void getIsiReport(List<ReportDNPM> lisReportMs) {
		ReportDNPDao reportDNPDao = (ReportDNPDao) applicationBean.getCorpGWContext().getBean("reportDNPDao");
		List<ReportDNPM> lisDetailM = reportDNPDao.getReportDNP(tglbukuS, tahunDateS,currencyS);
		String string = "January 2, 2010"; 
		
		for (ReportDNPM transactionDetailM : lisDetailM) {
			String tahun = transactionDetailM.getTglTerimaBayar().substring(4,8);
			Date date = null;

			try {
				if (transactionDetailM.getTanggalBuku() != null) {
					date = new SimpleDateFormat("MMddyyyy").parse(transactionDetailM.getTanggalBuku() + tahun);
					transactionDetailM.setTglgabung(date);
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			lisReportMs.add(transactionDetailM);
		}
	}
	

	public void generateFile() {
		try {
			totalTrx = 0;
			paramNamePF="PATH_FILE_REPORT_RESULT";
			paramNameKB="BANK_CODE";
			
			listReport = new ArrayList<ReportDNPM>();
			listAwal = new ArrayList<ReportDNPM>(listReport);
			
			ReportDNPDao reportDNPDao = (ReportDNPDao) applicationBean.getCorpGWContext().getBean("reportDNPDao");
			letakFile = reportDNPDao.getPathFile(paramNamePF);
			kodeBank=reportDNPDao.getKodeBank(paramNameKB);
			nameFileDNPUSD=kodeBank+"DNPUSD";
			nameFileDNPIDR=kodeBank+"DNP";
			listReport = new ArrayList<ReportDNPM>();
			DateFormat sdFormat = new SimpleDateFormat("MMdd");
			DateFormat thnFormat = new SimpleDateFormat("yyyy");
			tglbukuS = sdFormat.format(tglbuku);
			tahunDateS = thnFormat.format(tahunDate);
			currencyS = currency;
			String tglBkNameFile =tglbukuS.substring(2)+tglbukuS.substring(0,2)+ tahunDateS.substring(2, 4);
			listAwal.clear();
			getIsiReport(listAwal);
			FileWriter fw=null;
			if (currency.equals("USD")){
				File file = new File(letakFile+nameFileDNPUSD+ tglBkNameFile + ".txt");
				fw = new FileWriter(file);
			}else{
				File file = new File(letakFile+nameFileDNPIDR+ tglBkNameFile + ".txt");
				fw = new FileWriter(file);
			}
			
			for (int i = 0; i < listAwal.size(); i++) {
					if (listAwal.get(i).getTanggalBuku() != null) {
						if (listAwal.get(i).getTanggalBuku().equals(tglbukuS)) {
							listReport.add(listAwal.get(i));
							totalTrx++;
							String tglTerimaBayar = listAwal.get(i).getTglTerimaBayar().substring(6, 8) + listAwal.get(i).getTglTerimaBayar().substring(4, 6) +listAwal.get(i).getTglTerimaBayar().substring(2, 4) ;
							String tglBuku =listAwal.get(i).getTanggalBuku().substring(2)+listAwal.get(i).getTanggalBuku().substring(0,2)+ listAwal.get(i).getTglTerimaBayar().substring(2, 4);
							fw.write(
									listAwal.get(i).getKodeBank()+ ";"
									+ tglTerimaBayar+ ";"
									+ listAwal.get(i).getJamTerimaBayar()+ ";"
									+ tglBuku+ ";"
									+ listAwal.get(i).getKodeBilling() + ";"
									+ listAwal.get(i).getNTB() + ";"
									+ ((listAwal.get(i).getNTPN() == null) || (listAwal.get(i).getNTPN().equals("null")) ? "" :  listAwal.get(i).getNTPN()) +";"
									+ listAwal.get(i).getJumlahSetoran() + ";"
									+ listAwal.get(i).getMataUang()+ ";"
									+ ((listAwal.get(i).getNoSakti() == null) || (listAwal.get(i).getNoSakti().equals("null")) ? "" :  listAwal.get(i).getNoSakti())
									+"\r\n");
							
						}
				}
			}
			fw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		downloadFile();
	}

	public void downloadFile(){
		String nameOfFile="";
		String pathFile="";
		DateFormat sdFormat = new SimpleDateFormat("MMdd");
		DateFormat thnFormat = new SimpleDateFormat("yyyy");
		tglbukuS = sdFormat.format(tglbuku);
		tahunDateS = thnFormat.format(tahunDate);
		currencyS = currency;
		String tglBkNameFile2 =tglbukuS.substring(2)+tglbukuS.substring(0,2)+ tahunDateS.substring(2, 4);
		if (currency.equals("USD")){
			nameOfFile = nameFileDNPUSD + tglBkNameFile2 + ".txt";
			pathFile = letakFile;
		}else{
			nameOfFile = nameFileDNPIDR + tglBkNameFile2 + ".txt";
			pathFile = letakFile;
		}
		
		FacesContext fc = FacesContext.getCurrentInstance();
		HttpServletResponse response = (HttpServletResponse) fc
				.getExternalContext().getResponse();
		response.setContentType("application/octet-stream");
		response.setHeader("Content-Disposition", "attachment;filename="
				+ nameOfFile);
		System.out.println("DNP "+response.getCharacterEncoding());
		try {
			FileInputStream in = new FileInputStream(new File(
					pathFile.concat(nameOfFile)));

			ServletOutputStream out = response.getOutputStream();
			byte[] buffer = new byte[2048];
			int length;
			while ((length = in.read(buffer)) > 0) {
				out.write(buffer, 0, length);
			}
			in.close();
			out.flush();
			out.close();
			fc.responseComplete();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	


	public String getNtpn() {
		return ntpn;
	}

	public void setNtpn(String ntpn) {
		this.ntpn = ntpn;
	}

	public List<ReportDNPM> getListReport() {
		return listReport;
	}

	public void setListReport(List<ReportDNPM> listReport) {
		this.listReport = listReport;
	}

	public ReportDNPM getTransactionDetailM() {
		return transactionDetailM;
	}

	public void setTransactionDetailM(ReportDNPM transactionDetailM) {
		this.transactionDetailM = transactionDetailM;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<ReportDNPM> getListAwal() {
		return listAwal;
	}

	public void setListAwal(List<ReportDNPM> listAwal) {
		this.listAwal = listAwal;
	}

	public BigDecimal getTotalSetoran() {
		return totalSetoran;
	}

	public void setTotalSetoran(BigDecimal totalSetoran) {
		this.totalSetoran = totalSetoran;
	}

	public int getTotalTrx() {
		return totalTrx;
	}

	public void setTotalTrx(int totalTrx) {
		this.totalTrx = totalTrx;
	}


	public String getNoSakti() {
		return noSakti;
	}

	public void setNoSakti(String noSakti) {
		this.noSakti = noSakti;
	}

	public String getKodeBilling() {
		return kodeBilling;
	}

	public void setKodeBilling(String kodeBilling) {
		this.kodeBilling = kodeBilling;
	}


	public String getKodeBillingS() {
		return kodeBillingS;
	}

	public void setKodeBillingS(String kodeBillingS) {
		this.kodeBillingS = kodeBillingS;
	}

	public String getJmlSet() {
		return jmlSet;
	}


	public void setJmlSet(String jmlSet) {
		this.jmlSet = jmlSet;
	}


	public String getJmlTrx() {
		return jmlTrx;
	}


	public void setJmlTrx(String jmlTrx) {
		this.jmlTrx = jmlTrx;
	}

	public String getTahunDateS() {
		return tahunDateS;
	}


	public void setTahunDateS(String tahunDateS) {
		this.tahunDateS = tahunDateS;
	}


	public Date getTglbuku() {
		return tglbuku;
	}


	public void setTglbuku(Date tglbuku) {
		this.tglbuku = tglbuku;
	}


	public Date getTahunDate() {
		return tahunDate;
	}


	public void setTahunDate(Date tahunDate) {
		this.tahunDate = tahunDate;
	}


	public String getTglbukuS() {
		return tglbukuS;
	}


	public void setTglbukuS(String tglbukuS) {
		this.tglbukuS = tglbukuS;
	}


	public String getCurrencyS() {
		return currencyS;
	}


	public void setCurrencyS(String currencyS) {
		this.currencyS = currencyS;
	}


	public String getCurrency() {
		return currency;
	}


	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public ApplicationBean getApplicationBean() {
		return applicationBean;
	}


	public void setApplicationBean(ApplicationBean applicationBean) {
		this.applicationBean = applicationBean;
	}


	public SessionBean getSessionBean() {
		return sessionBean;
	}


	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}


	public void handleDateSelect(ReportDNPHandler event){

	}
}
