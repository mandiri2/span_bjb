package com.mii.beans;

import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.apache.commons.lang.StringUtils;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Rectangle;
import com.mii.dao.MonitoringPenihilanDao;
import com.mii.helpers.FacesUtil;
import com.mii.models.MonitoringPenihilan;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.SessionBean;

@ManagedBean
@ViewScoped
public class MonitoringPenihilanHandler implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private LazyDataModel < MonitoringPenihilan > detailList;
	
	@ManagedProperty(value="#{applicationBean}")
	private ApplicationBean applicationBean;
	
	@ManagedProperty(value="#{sessionBean}")
	private SessionBean sessionBean;
	private boolean doShowSearch;
	
	private Date tanggalPenihilan;
	private String referenceNo;
	
	@PostConstruct
	public void init(){
		FacesUtil.authMenu(sessionBean.getUser(), "monitoringPenihilan");
		//doClickGaji();
		loadData();
	}
	
	private void initMonitoringPenihilanList() {
		detailList = new DetailDataModel();
	}
	
	public void showSearch(){
		doShowSearch = true;
	}
	
	public void hideSearch(){
		doShowSearch = false;
	}
	
	public void doSearch(){
		loadData();
	}
	
	private void loadData() {
		detailList = new DetailDataModel();
		
	}

	public void clearSearch(){
		tanggalPenihilan = null;
		referenceNo = "";
	}
	
	public void preProcessPDF(Object document) throws IOException,  
    BadElementException, DocumentException{
        Document pdf = (Document) document;
        Rectangle r = new Rectangle(2000f, 1000f);
        pdf.setPageSize(r);
        pdf.setMargins(0f, 0f, 100f, 100f);
    }

	private class DetailDataModel extends LazyDataModel < MonitoringPenihilan > {
		private static final long serialVersionUID = 1L;

		public DetailDataModel() {
		}

		@Override
		public List < MonitoringPenihilan > load(int startingAt, int maxPerPage,
		String sortField, SortOrder sortOrder,
		Map < String, String > filters) {
			Integer jmlAll = 0;
			List<MonitoringPenihilan> detailList = new ArrayList<MonitoringPenihilan>();

			String tanggalPenihilan = "%"+StringUtils.defaultIfEmpty(getStringDate(getTanggalPenihilan()), "")+"%";
			String referenceNo = "%"+StringUtils.defaultIfEmpty(getReferenceNo(), "")+"%";
			
			try{
				
				detailList = getMonitoringPenihilanDAOBean().getMonitoringPenihilan(tanggalPenihilan, referenceNo, startingAt, startingAt+maxPerPage);
					jmlAll = getMonitoringPenihilanDAOBean().countMonitoringPenihilan(tanggalPenihilan, referenceNo);
				
			}catch(Exception e){
				e.printStackTrace();
			}
			setRowCount(jmlAll);
			setPageSize(maxPerPage);
			return detailList;
		}

		private MonitoringPenihilanDao getMonitoringPenihilanDAOBean() {
			return (MonitoringPenihilanDao) applicationBean.getCorpGWContext().getBean("MonitoringPenihilanDao");
		}
	}

	public String getStringDate(Date tanggalPenihilan) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String result = "";
		
		if (null != tanggalPenihilan){
			result = simpleDateFormat.format(tanggalPenihilan);
		}
		
		return result;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public LazyDataModel<MonitoringPenihilan> getDetailList() {
		return detailList;
	}

	public void setDetailList(LazyDataModel<MonitoringPenihilan> detailList) {
		this.detailList = detailList;
	}

	public ApplicationBean getApplicationBean() {
		return applicationBean;
	}

	public void setApplicationBean(ApplicationBean applicationBean) {
		this.applicationBean = applicationBean;
	}

	public boolean isDoShowSearch() {
		return doShowSearch;
	}

	public void setDoShowSearch(boolean doShowSearch) {
		this.doShowSearch = doShowSearch;
	}

	public Date getTanggalPenihilan() {
		return tanggalPenihilan;
	}

	public void setTanggalPenihilan(Date tanggalPenihilan) {
		this.tanggalPenihilan = tanggalPenihilan;
	}

	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
