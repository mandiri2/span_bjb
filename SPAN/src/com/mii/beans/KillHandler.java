package com.mii.beans;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.primefaces.context.RequestContext;

import com.mii.constant.Constants;
import com.mii.dao.ApprovalDao;
import com.mii.dao.SystemParameterDao;
import com.mii.dao.UserDao;
import com.mii.helpers.ChaptaKeyResolver;
import com.mii.helpers.FacesUtil;
import com.mii.helpers.HelperUtils;
import com.mii.helpers.LdapConfigurator;
import com.mii.helpers.LdapConnector;
import com.mii.models.SystemParameter;
import com.mii.models.User;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.SessionBean;

@ManagedBean
@SessionScoped
@FacesValidator(value = "captchaValidator")
public class KillHandler implements Serializable, Validator {
	private static final long serialVersionUID = 1L;
	private User user = new User();
	private User userkill = new User();
	private String chImage;
	
	@ManagedProperty(value = "#{sessionBean}")
	private SessionBean sessionBean;
	
	@ManagedProperty(value = "#{applicationBean}")
	private ApplicationBean applicationBean;

	private ExternalContext externalContext;
	private HttpServletRequest httpServletRequest;
	private String sessionId;
	
	private String password;
	
	String yes;
	
	@PostConstruct
	public void init(){
	}
	
	public KillHandler() {
		externalContext = FacesContext.getCurrentInstance().getExternalContext();

		httpServletRequest = (HttpServletRequest) externalContext.getRequest();
		
		sessionId = httpServletRequest.getSession().getId();
	}
	
	private SystemParameterDao getSystemParameterDAOBean(){
		return (SystemParameterDao) applicationBean.getCorpGWContext().getBean("systemParameterDAO");
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	String validate;

	public String getChImage() {
		return chImage;
	}

	public void setChImage(String chImage) {
		this.chImage = chImage;
	}

	public String getValidate() {
		return validate;
	}

	public void setValidate(String validate) {
		this.validate = validate;
	}

	public void keepUserSessionAlive() {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
		request.getSession();
	}

	public String doLogin() throws Exception {
		boolean checked = checkAkun(user.getUserName());
		if (checked) {
			
			if (LdapConnector.authenticate(user.getUserName(), password.toString(),null)){
				UserDao userDao = (UserDao) applicationBean.getCorpGWContext()
					.getBean("userDAO");
				user.setPassword(password.toString());
				user.setLastLogin(HelperUtils.getCurrentDateTime());
				userDao.editUser(this.user);
				FacesUtil.getExternalContext().getSessionMap()
					.put("user", user);
				sessionBean.setUser(getUser());
				sessionBean.buildMenu(getUser());
	
				return "welcome?faces-redirect=true";
			}else{
				FacesUtil.setErrorMessage("msg", "Username or password is incorect",
						null);
			}

		} else {
			FacesUtil.setErrorMessage("msg", "Username is incorect",
				null);
			
		}

		return null;

	}


	public boolean checkAkun(String username) throws Exception {

		boolean status = false;

		UserDao userDao = (UserDao) applicationBean.getCorpGWContext().getBean(
			"userDAO");
		this.user = userDao.getValidUserByUsername(username);

		if (this.user != null && this.user.getUserName().equals(username)) {
			
			status = true;
		}
		return status;
	}

	public String doLogout() {
		FacesUtil.getExternalContext().getSessionMap().remove("user");
		return "loginSPAN?faces-redirect=true";
	}
	

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public ApplicationBean getApplicationBean() {
		return applicationBean;
	}

	public void setApplicationBean(ApplicationBean applicationBean) {
		this.applicationBean = applicationBean;
	}

	@Override
	public void validate(FacesContext context, UIComponent component,
	Object value) throws ValidatorException {
		String userInput = (String) value;
		String captcha = null;
		
        captcha = ChaptaKeyResolver.getInstance().chaptchaTemp.get(sessionId);
        
		if (userInput == null || userInput == "" || captcha == null || !userInput.equals(captcha)) {
			throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Wrong Captcha", 
                    null));
		}
		
		ChaptaKeyResolver.getInstance().chaptchaTemp.remove(sessionId);

	}
	
	public void killAnotherSession() {
		User.getLogins().remove(userkill);
	}
	private UserDao getUserDAOBean() {
		return (UserDao) applicationBean.getCorpGWContext().getBean("userDAO");
	}

	public String getYes() {
		return yes;
	}

	public void setYes(String yes) {
		this.yes = yes;
	}

	public User getUserkill() {
		return userkill;
	}

	public void setUserkill(User userkill) {
		this.userkill = userkill;
	}
	
	
}