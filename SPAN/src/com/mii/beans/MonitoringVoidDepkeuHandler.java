package com.mii.beans;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.apache.commons.lang.SerializationUtils;
import org.apache.commons.lang.StringUtils;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.mii.constant.Constants;
import com.mii.constant.ConstantsUserLog;
import com.mii.dao.SPANDataValidationDao;
import com.mii.dao.SPANHostDataDetailsDao;
import com.mii.dao.SPANMonitoringVoidDepKeuDao;
import com.mii.dao.SpanHostDataFailedDao;
import com.mii.dao.SpanHostResponseDao;
import com.mii.dao.SpanVoidDataTrxDao;
import com.mii.dao.SystemParameterDao;
import com.mii.dao.UserDao;
import com.mii.doc.DataArea;
import com.mii.doc.SP2DXmltoObject;
import com.mii.helpers.FacesUtil;
import com.mii.is.DeleteSPANVoidDataTransaction;
import com.mii.is.VoidDocumentNo;
import com.mii.is.io.DeleteSPANVoidDataTransactionRequest;
import com.mii.is.io.VoidDocumentNumberRequest;
import com.mii.is.io.VoidDocumentNumberVFiles;
import com.mii.is.io.helper.DetailsOutputList;
import com.mii.is.util.PubUtil;
import com.mii.models.SPANDataValidation;
import com.mii.models.SPANHostDataDetails;
import com.mii.models.SPANVoidDocumentNumber;
import com.mii.models.SpanDataSummary;
import com.mii.models.SpanHostDataFailed;
import com.mii.models.SpanHostResponse;
import com.mii.models.SpanMonitoringVoidDepKeu;
import com.mii.models.SpanSp2dVoidLog;
import com.mii.models.SpanSummariesOutput;
import com.mii.models.SpanVoidDataTrx;
import com.mii.models.UserLog;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.SessionBean;
import com.mii.sknrtgs.ACKSknRtgs;
import com.mii.sknrtgs.SentAckDataFileRequest;
import com.mii.sknrtgs.SentAckDataFileResponse;
import com.wm.app.b2b.client.Context;
import com.wm.app.b2b.client.ServiceException;
import com.wm.data.IData;

@ManagedBean
@ViewScoped
public class MonitoringVoidDepkeuHandler implements Serializable {
	private static final long serialVersionUID = 1L;

	private Boolean doShowGAJI;
	private Boolean doShowGAJIR;
	private String tabSelected; // GAJI - GAJIR

	private Boolean doShowVoidDocumentNumber;

	private String fileName;
	private String sp2dNumber;
	private String documentNumber;

	private Boolean doShowSearchMessage;
	private String searchMessage;

	private LazyDataModel<SpanMonitoringVoidDepKeu> monitoringVoidList;

	private SPANHostDataDetails spanHostDataDetail;
	private LazyDataModel<SPANVoidDocumentNumber> voidDocumentNumberList;
	private String filenameDetail;
	private String sp2dDetail;

	public static Map<String, SpanMonitoringVoidDepKeu> resultTemps = new HashMap<String, SpanMonitoringVoidDepKeu>();

	private List<SpanSp2dVoidLog> spanSp2dVoidLogs;

	private LazyDataModel<SpanSp2dVoidLog> monitoringVoidSp2dList;

	private LazyDataModel<SpanMonitoringVoidDepKeu> spanDataValidationBackdateList;

	private List<SpanMonitoringVoidDepKeu> filspanDataValidationBackdateList;

	private List<SpanMonitoringVoidDepKeu> voidHistoryList;

	private LazyDataModel<DetailsOutputList> detailList;

	private String status;

	private SpanDataSummary spanDataSummary;
	private SPANDataValidation spanDataValidation;

	private String detailFileName;
	private String detailTotalRecord;
	private String detailTotalAmount;
	private String detailType;
	private String detailDebitAccount;
	private String detailDebitAccountType;
	private String detailDocumentDate;
	
	private String sp2dback;
	

	// private LazyDataModel<SpanMonitoringVoidDepKeu> voidHistoryList;
	//
	// public LazyDataModel<SpanMonitoringVoidDepKeu> getVoidHistoryList() {
	// return voidHistoryList;
	// }
	//
	// public void setVoidHistoryList(LazyDataModel<SpanMonitoringVoidDepKeu>
	// voidHistoryList) {
	// this.voidHistoryList = voidHistoryList;
	// }

	public String getSp2dback() {
		return sp2dback;
	}

	public void setSp2dback(String sp2dback) {
		this.sp2dback = sp2dback;
	}

	public String getDetailFileName() {
		return detailFileName;
	}

	public void setDetailFileName(String detailFileName) {
		this.detailFileName = detailFileName;
	}

	public String getDetailTotalRecord() {
		return detailTotalRecord;
	}

	public void setDetailTotalRecord(String detailTotalRecord) {
		this.detailTotalRecord = detailTotalRecord;
	}

	public String getDetailTotalAmount() {
		return detailTotalAmount;
	}

	public void setDetailTotalAmount(String detailTotalAmount) {
		this.detailTotalAmount = detailTotalAmount;
	}

	public String getDetailType() {
		return detailType;
	}

	public void setDetailType(String detailType) {
		this.detailType = detailType;
	}

	public String getDetailDebitAccount() {
		return detailDebitAccount;
	}

	public void setDetailDebitAccount(String detailDebitAccount) {
		this.detailDebitAccount = detailDebitAccount;
	}

	public String getDetailDebitAccountType() {
		return detailDebitAccountType;
	}

	public void setDetailDebitAccountType(String detailDebitAccountType) {
		this.detailDebitAccountType = detailDebitAccountType;
	}

	public String getDetailDocumentDate() {
		return detailDocumentDate;
	}

	public void setDetailDocumentDate(String detailDocumentDate) {
		this.detailDocumentDate = detailDocumentDate;
	}

	public SpanDataSummary getSpanDataSummary() {
		return spanDataSummary;
	}

	public void setSpanDataSummary(SpanDataSummary spanDataSummary) {
		this.spanDataSummary = spanDataSummary;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public LazyDataModel<DetailsOutputList> getDetailList() {
		return detailList;
	}

	public void setDetailList(LazyDataModel<DetailsOutputList> detailList) {
		this.detailList = detailList;
	}

	public List<SpanMonitoringVoidDepKeu> getVoidHistoryList() {
		return voidHistoryList;
	}

	public void setVoidHistoryList(List<SpanMonitoringVoidDepKeu> voidHistoryList) {
		this.voidHistoryList = voidHistoryList;
	}

	// public LazyDataModel<SpanMonitoringVoidDepKeu>
	// getFilspanDataValidationBackdateList() {
	// return filspanDataValidationBackdateList;
	// }
	//
	// public void setFilspanDataValidationBackdateList(
	// LazyDataModel<SpanMonitoringVoidDepKeu>
	// filspanDataValidationBackdateList) {
	// this.filspanDataValidationBackdateList =
	// filspanDataValidationBackdateList;
	// }

	// private class HistoryDataModel extends
	// LazyDataModel<SpanMonitoringVoidDepKeu> {
	//
	// private static final long serialVersionUID = -5221344722902282324L;
	//
	// public HistoryDataModel(){
	//
	// }
	//
	// @SuppressWarnings("rawtypes")
	// @Override
	// public List < SpanMonitoringVoidDepKeu > load(int startingAt, int
	// maxPerPage,
	// String sortField, SortOrder sortOrder,
	// Map < String, String > filters) {
	// Integer jmlAll = 0;
	//
	// SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	// List<SpanMonitoringVoidDepKeu> spanMonitoringVoidDepKeuList = new
	// ArrayList<SpanMonitoringVoidDepKeu>();
	// try{
	// //two years ago
	// Calendar cal = Calendar.getInstance();
	// cal.add(Calendar.YEAR, -2);
	// //
	//
	// String filename =
	// "%"+StringUtils.defaultIfEmpty(filters.get("fileName"),"")+"%";
	// String sp2 =
	// "%"+StringUtils.defaultIfEmpty(filters.get("sp2dNumber"),"")+"%";
	// String docDate = sdf.format(cal.getTime());
	// String voidFlag = Constants.VOIDFLAG;
	// spanMonitoringVoidDepKeuList =
	// getSpanVoidDepKeuDAOBean().getLazyHistorySp2dNo(filename, docDate,
	// voidFlag, tabSelected, sp2, startingAt, startingAt+maxPerPage);
	// jmlAll =
	// spanMonitoringVoidDepKeuList.size();//getSpanVoidDepKeuDAOBean().countHistorySp2dNo(filename,
	// docDate, voidFlag, tabSelected, sp2);
	// }catch(Exception e){
	// e.printStackTrace();
	// }
	//
	//
	// List < SpanMonitoringVoidDepKeu > detailsFinal = new ArrayList <
	// SpanMonitoringVoidDepKeu > ();
	// for (SpanMonitoringVoidDepKeu a: spanMonitoringVoidDepKeuList) {
	// Iterator entries = resultTemps.entrySet().iterator();
	// while (entries.hasNext()) {
	// Entry thisEntry = (Entry) entries.next();
	// SpanMonitoringVoidDepKeu s = (SpanMonitoringVoidDepKeu)
	// thisEntry.getValue();
	// if (a.getSp2dNumber().equals(s.getSp2dNumber())) {
	// if (s.isSelected()) {
	// a.setSelected(true);
	// } else {
	// a.setSelected(false);
	// }
	// }
	// }
	// int jmlVoid = 0;
	// try{
	// jmlVoid = getSpanVoidDepKeuDAOBean().countSpanVoidDataTrx("0",
	// a.getFileName(), a.getSp2dNumber());
	// }catch(Exception e){
	// e.printStackTrace();
	// }
	// if(jmlVoid > 0){
	// a.setVoidStatus(true);
	// detailsFinal.add(a);
	// }else{
	// a.setVoidStatus(false);
	// }
	// }
	// if(detailsFinal.size()>0){
	// jmlAll = detailsFinal.size();
	// setRowCount(jmlAll);
	// setPageSize(maxPerPage);
	// return detailsFinal;
	// }else{
	// jmlAll = detailsFinal.size();
	// setRowCount(jmlAll);
	// setPageSize(maxPerPage);
	// return null;
	// }
	// }
	//
	// }

	public List<SpanMonitoringVoidDepKeu> getFilspanDataValidationBackdateList() {
		return filspanDataValidationBackdateList;
	}

	public void setFilspanDataValidationBackdateList(List<SpanMonitoringVoidDepKeu> filspanDataValidationBackdateList) {
		this.filspanDataValidationBackdateList = filspanDataValidationBackdateList;
	}

	private List<SpanMonitoringVoidDepKeu> loadHist() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		List<SpanMonitoringVoidDepKeu> spanMonitoringVoidDepKeuList = new ArrayList<SpanMonitoringVoidDepKeu>();
		try {
			// two years ago
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.YEAR, -2);
			//

			String filename = "%%";// +StringUtils.defaultIfEmpty(filters.get("fileName"),"")+"%";
			String sp2 = "%%";// +StringUtils.defaultIfEmpty(filters.get("sp2dNumber"),"")+"%";
			String docDate = sdf.format(cal.getTime());
			spanMonitoringVoidDepKeuList = getSpanVoidDepKeuDAOBean().getLazyHistorySp2dNo(filename, docDate,
					tabSelected, sp2, 0, 0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		List<SpanMonitoringVoidDepKeu> detailsFinal = new ArrayList<SpanMonitoringVoidDepKeu>();
		for (SpanMonitoringVoidDepKeu a : spanMonitoringVoidDepKeuList) {
			Iterator entries = resultTemps.entrySet().iterator();
			while (entries.hasNext()) {
				Entry thisEntry = (Entry) entries.next();
				SpanMonitoringVoidDepKeu s = (SpanMonitoringVoidDepKeu) thisEntry.getValue();
				if (a.getSp2dNumber().equals(s.getSp2dNumber())) {
					if (s.isSelected()) {
						a.setSelected(true);
					} else {
						a.setSelected(false);
					}
				}
			}
			int jmlVoid = 0;
			try {
				jmlVoid = getSpanVoidDepKeuDAOBean().countSpanVoidDataTrxHist(a.getFileName(), a.getSp2dNumber());
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (jmlVoid > 0) {
				a.setVoidStatus(true);
				detailsFinal.add(a);
			} else {
				a.setVoidStatus(false);
			}
		}
		if (detailsFinal.size() > 0) {
			return detailsFinal;
		} else {
			return null;
		}
	}

	private class BackdateDataModel extends LazyDataModel<SpanMonitoringVoidDepKeu> {

		private static final long serialVersionUID = -1900689054736367259L;

		public BackdateDataModel() {

		}

		@Override
		public List<SpanMonitoringVoidDepKeu> load(int startingAt, int maxPerPage, String sortField,
				SortOrder sortOrder, Map<String, String> filters) {
			Integer jmlAll = 0;
			List<SpanMonitoringVoidDepKeu> spanMonitoringVoidDepKeuList = new ArrayList<SpanMonitoringVoidDepKeu>();
			try {
				String filename = "%" + StringUtils.defaultIfEmpty(filters.get("fileName"), "") + "%";
				String sp2dfsearch = "%" + StringUtils.defaultIfEmpty(sp2dback, "") +"%";
				spanMonitoringVoidDepKeuList = getSpanVoidDepKeuDAOBean().getLazySpanDataValidationBackdate(filename,sp2dfsearch,
						startingAt, startingAt + maxPerPage);
				jmlAll = getSpanVoidDepKeuDAOBean().countSpanDataValidationBackdate(filename);
			} catch (Exception e) {
				e.printStackTrace();
			}
			setRowCount(jmlAll);
			setPageSize(maxPerPage);

			if (spanMonitoringVoidDepKeuList.size() > 0) {

				return spanMonitoringVoidDepKeuList;
			} else
				return new ArrayList<>();
		}
	}

	public LazyDataModel<SpanMonitoringVoidDepKeu> getSpanDataValidationBackdateList() {
		return spanDataValidationBackdateList;
	}

	public void setSpanDataValidationBackdateList(
			LazyDataModel<SpanMonitoringVoidDepKeu> spanDataValidationBackdateList) {
		this.spanDataValidationBackdateList = spanDataValidationBackdateList;
	}

	@ManagedProperty(value = "#{applicationBean}")
	private ApplicationBean applicationBean;

	@ManagedProperty(value = "#{sessionBean}")
	private SessionBean sessionBean;

	public String filenameFilter;

	public String sp2dNoFilter;
	
	private List<SpanVoidDataTrx> spanVoidDataTrxList;
	
	@PostConstruct
	public void init() {
		FacesUtil.authMenu(getSessionBean().getUser(), "monitoringVoidDepkeu");
		doClickGaji();
		documentNumber = "";
		spanHostDataDetail = new SPANHostDataDetails();
		setDoShowSearchMessage(false);
		spanSp2dVoidLogs = new ArrayList<>();
		spanDataSummary = new SpanDataSummary();
		spanDataValidation = new SPANDataValidation();
	}

	public void loadData() {
		monitoringVoidList = new DetailDataModel();
	}

	public void loadBackdate() {
		spanDataValidationBackdateList = new BackdateDataModel();
	}

	public void loadVoidDocumentNumberList(SpanMonitoringVoidDepKeu monitoringVoid) {
		filenameDetail = monitoringVoid.getFileName();
		sp2dDetail = monitoringVoid.getSp2dNumber();
//		voidDocumentNumberList = new DetailVoidDataModel();
		voidDocumentNumberList = new DetailVoidHistDataModel();
	}

	public void loadVoidDocumentHistNumberList(SpanMonitoringVoidDepKeu monitoringVoid) {
		filenameDetail = monitoringVoid.getFileName();
		sp2dDetail = monitoringVoid.getSp2dNumber();
		voidDocumentNumberList = new DetailVoidHistDataModel();
	}

	private class DetailDataModel extends LazyDataModel<SpanMonitoringVoidDepKeu> {
		private static final long serialVersionUID = 1L;

		public DetailDataModel() {
		}

		@SuppressWarnings("rawtypes")
		@Override
		public List<SpanMonitoringVoidDepKeu> load(int startingAt, int maxPerPage, String sortField,
				SortOrder sortOrder, Map<String, String> filters) {
			Integer jmlAll = 0;
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			List<SpanMonitoringVoidDepKeu> spanMonitoringVoidDepKeuList = new ArrayList<SpanMonitoringVoidDepKeu>();
			try {
				String docDate = sdf.format(new Date());
				String voidFlag = Constants.VOIDFLAG;
				spanMonitoringVoidDepKeuList = getSpanVoidDepKeuDAOBean().getLazySpanDataValidationSp2dNo(getFileName(),
						docDate, voidFlag, tabSelected, getSp2dNumber(), startingAt, startingAt + maxPerPage);
				jmlAll = getSpanVoidDepKeuDAOBean().countSpanDataValidationSp2dNo(getFileName(), docDate, voidFlag,
						tabSelected, getSp2dNumber());
			} catch (Exception e) {
				e.printStackTrace();
			}
			setRowCount(jmlAll);
			setPageSize(maxPerPage);

			List<SpanMonitoringVoidDepKeu> detailsFinal = new ArrayList<SpanMonitoringVoidDepKeu>();
			for (SpanMonitoringVoidDepKeu a : spanMonitoringVoidDepKeuList) {
				Iterator entries = resultTemps.entrySet().iterator();
				while (entries.hasNext()) {
					Entry thisEntry = (Entry) entries.next();
					SpanMonitoringVoidDepKeu s = (SpanMonitoringVoidDepKeu) thisEntry.getValue();
					if (a.getSp2dNumber().equals(s.getSp2dNumber())) {
						if (s.isSelected()) {
							a.setSelected(true);
						} else {
							a.setSelected(false);
						}
					}
				}
				int jmlVoid = 0;
				try {
					jmlVoid = getSpanVoidDepKeuDAOBean().countSpanVoidDataTrx("0", a.getFileName(), a.getSp2dNumber());
				} catch (Exception e) {
					e.printStackTrace();
				}
				if (jmlVoid > 0) {
					a.setVoidStatus(true);
				} else {
					a.setVoidStatus(false);
				}
				detailsFinal.add(a);
			}
			if (detailsFinal.size() > 0) {
				return detailsFinal;
			} else {
				return null;
			}
		}
	}

	private class DetailVoidDataModel extends LazyDataModel<SPANVoidDocumentNumber> {
		private static final long serialVersionUID = 1L;

		public DetailVoidDataModel() {
		}

		@Override
		public List<SPANVoidDocumentNumber> load(int startingAt, int maxPerPage, String sortField, SortOrder sortOrder,
				Map<String, String> filters) {
			Integer jmlAll = 0;
			List<SPANVoidDocumentNumber> SPANVoidDocumentNumberList = new ArrayList<SPANVoidDocumentNumber>();
			try {
				SPANVoidDocumentNumberList = getSpanVoidDepKeuDAOBean().getVoidDataTransactionListByFilenameSp2d("0",
						filenameDetail, sp2dDetail);
				jmlAll = SPANVoidDocumentNumberList.size();
			} catch (Exception e) {
				e.printStackTrace();
			}
			setRowCount(jmlAll);
			setPageSize(maxPerPage);

			List<SPANVoidDocumentNumber> detailFinal = new ArrayList<SPANVoidDocumentNumber>();
			for (int i = startingAt; i < SPANVoidDocumentNumberList.size(); i++) {
				detailFinal.add(SPANVoidDocumentNumberList.get(i));
			}
			return detailFinal;
		}
	}

	private class DetailVoidHistDataModel extends LazyDataModel<SPANVoidDocumentNumber> {
		private static final long serialVersionUID = 1L;

		public DetailVoidHistDataModel() {
		}

		@Override
		public List<SPANVoidDocumentNumber> load(int startingAt, int maxPerPage, String sortField, SortOrder sortOrder,
				Map<String, String> filters) {
			Integer jmlAll = 0;
			List<SPANVoidDocumentNumber> SPANVoidDocumentNumberList = new ArrayList<SPANVoidDocumentNumber>();
			try {
				SPANVoidDocumentNumberList = getSpanVoidDepKeuDAOBean()
						.getVoidDataTransactionListByFilenameSp2dNoHost(filenameDetail, sp2dDetail);
				jmlAll = SPANVoidDocumentNumberList.size();
			} catch (Exception e) {
				e.printStackTrace();
			}
			setRowCount(jmlAll);
			setPageSize(maxPerPage);

			List<SPANVoidDocumentNumber> detailFinal = new ArrayList<SPANVoidDocumentNumber>();
			for (int i = startingAt; i < SPANVoidDocumentNumberList.size(); i++) {
				detailFinal.add(SPANVoidDocumentNumberList.get(i));
			}
			return detailFinal;
		}
	}

	private class VoidSp2dLogModel extends LazyDataModel<SpanSp2dVoidLog> {
		private static final long serialVersionUID = 1L;

		public VoidSp2dLogModel() {
		}

		@Override
		public List<SpanSp2dVoidLog> load(int startingAt, int maxPerPage, String sortField, SortOrder sortOrder,
				Map<String, String> filters) {
			try {
				filenameFilter = "%" + StringUtils.defaultIfEmpty(filters.get("filename"), "") + "%";
				sp2dNoFilter = "%" + StringUtils.defaultIfEmpty(filters.get("sp2dNo"), "") + "%";

				List<SpanSp2dVoidLog> result = new ArrayList<>();
				result = VoidDocumentNo.viewSpanSp2dVoidLogs(filenameFilter, sp2dNoFilter, startingAt,
						startingAt + maxPerPage, getSpanVoidDataTrxDAOBean());

				setRowCount(VoidDocumentNo.countSpanSp2dVoidLogs(filenameFilter, sp2dNoFilter,
						getSpanVoidDataTrxDAOBean()));
				setPageSize(maxPerPage);

				return result;
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}
	}

	public void processChecked(SpanMonitoringVoidDepKeu result) {
		SpanMonitoringVoidDepKeu r = new SpanMonitoringVoidDepKeu();
		r = (SpanMonitoringVoidDepKeu) SerializationUtils.clone(result);
		if (result.isSelected()) {
			resultTemps.put(result.getSp2dNumber(), result);
		} else {
			resultTemps.remove(result.getSp2dNumber());
		}
	}

	public void doClickGaji() {
		doShowGAJI = true;
		doShowGAJIR = false;
		setTabSelected(Constants.GAJI);
		changeTab();
	}

	public void doClickGajiR() {
		doShowGAJI = false;
		doShowGAJIR = true;
		setTabSelected(Constants.GAJI_R);
		changeTab();
	}

	private void changeTab() {
		setMonitoringVoidList(null);
		resultTemps.clear();
		loadData();
		setFileName("");
		setSp2dNumber("");
		doLoging(ConstantsUserLog.MENU_MONITORING_VOID_DEPKEU,
				tabSelected.concat(" ").concat(ConstantsUserLog.ACTIVITY_INIT), "Tab " + tabSelected + " Init.");
	}

	@SuppressWarnings("rawtypes")
	public void voidChecked() {
		doLoging(ConstantsUserLog.MENU_MONITORING_VOID_DEPKEU,
				tabSelected.concat(" ").concat(ConstantsUserLog.ACTIVITY_VOID_CHECKED), ": VoidCheckedExecute.");

		SystemParameterDao systemParameterDao = (SystemParameterDao) applicationBean.getCorpGWContext()
				.getBean("systemParameterDAO");
		VoidDocumentNumberRequest voidDocumentNumberRequest = new VoidDocumentNumberRequest();
		voidDocumentNumberRequest.setInvokeType("1");
		List<VoidDocumentNumberVFiles> voidDocumentNumberVFilesList = new ArrayList<VoidDocumentNumberVFiles>();
		Iterator entries = resultTemps.entrySet().iterator();

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
		boolean flag = false;
		List<String> sp2dNoNoList = new ArrayList<String>();
		while (entries.hasNext()) {
			Entry thisEntry = (Entry) entries.next();
			SpanMonitoringVoidDepKeu s = (SpanMonitoringVoidDepKeu) thisEntry.getValue();
			SPANDataValidation spdv = getSpanDataValidationDaoBean().getSPANDataValidationByFilename(s.getFileName());
			sp2dNoNoList.add(s.getSp2dNumber());
			if(spdv.getProcStatus().equals("4")){
				//NEW VOID MECHANISM
				List<SpanHostResponse> spanHostResponseList = getSpanHostResponseDaoBean().getResponseByResponsecodeRetryOnly(s.getSp2dNumber(), s.getFileName());
				int totalRecord = Integer.parseInt(spdv.getTotalRecord());
				int totalAmount = Integer.parseInt(spdv.getTotalAmount());
				for(SpanHostResponse resp : spanHostResponseList){
					totalRecord = totalRecord - 1;
					totalAmount = totalAmount - Integer.parseInt(resp.getDebitAmount());
					//Update SpanHostResponse
					updateResponseToVOIDRC(resp);
					//Insert to VOIDDataTrx
					SpanVoidDataTrx spanVoidDataTrx = constructSpanVoidDataTrx(resp, spdv.getFileName());
					InsertSPANDataVoidTransaction(spanVoidDataTrx, getSpanVoidDataTrxDAOBean());
					updateResponseToVOIDId(resp);
				}
				updateSpanDataValidation(totalRecord, totalAmount, spdv.getFileName()); //Update SpanDataValidation
				updateSpanDataFailed(s.getFileName(),spanHostResponseList.size()); //Update Span Host Data Failed
				
				createSpanSp2dvoidLogList(spdv.getFileName(), s.getSp2dNumber(),dateFormat.format(new Date()));
				doLoging(ConstantsUserLog.MENU_MONITORING_VOID_DEPKEU,
						tabSelected.concat(" ").concat(ConstantsUserLog.ACTIVITY_VOID_CHECKED),
						": " + spdv.getFileName() + "-" + s.getSp2dNumber());
			}else{
				VoidDocumentNumberVFiles voidDocumentNumberVFiles = new VoidDocumentNumberVFiles();
				voidDocumentNumberVFiles.setFileName(s.getFileName());
				List<String> documentNoList = new ArrayList<String>();
				documentNoList.add(s.getSp2dNumber());
				voidDocumentNumberVFiles.setDocumentNoList(sp2dNoNoList);
				voidDocumentNumberVFilesList.add(voidDocumentNumberVFiles);
				createSpanSp2dvoidLogList(voidDocumentNumberVFiles.getFileName(), s.getSp2dNumber(),
						dateFormat.format(new Date()));
				doLoging(ConstantsUserLog.MENU_MONITORING_VOID_DEPKEU,
						tabSelected.concat(" ").concat(ConstantsUserLog.ACTIVITY_VOID_CHECKED),
						": " + s.getFileName() + "-" + s.getSp2dNumber());
				flag = true;
			}
		}
		
		if(flag){
			voidDocumentNumberRequest.setVoidDocumentNumberVFilesList(voidDocumentNumberVFilesList);
			VoidDocumentNo.VoidDocumentNoProcess(voidDocumentNumberRequest, getSPANDataValidationDao(), systemParameterDao,
					getSpanVoidDataTrxDAOBean(), getSPANHostDataDetailsDao());
			VoidDocumentNo.insertSpanSp2dVoidLogs(spanSp2dVoidLogs, getSpanVoidDataTrxDAOBean());
		}
		
		List<String> documentNoList = new ArrayList<String>();
		for (String sp2dNo : sp2dNoNoList) {
			documentNoList.addAll(getSpanVoidDataTrxDAOBean().getDocumentNoBySp2dNo(sp2dNo));
		}
		autoSendAck(documentNoList);
		
		System.out.println("VoidChecked Executed");
	}
	
	private SpanVoidDataTrx constructSpanVoidDataTrx(SpanHostResponse resp, String fileName) {
		SPANHostDataDetails details = getSPANHostDataDetailsDao().getSPANDetailsByKey(resp.getTrxHeaderId(), resp.getBatchId(), resp.getTrxDetailId());
		SpanVoidDataTrx spanVoidDataTrx = new SpanVoidDataTrx();
		spanVoidDataTrx.setFileName(fileName);
		spanVoidDataTrx.setDocumentNo(details.getDocumentNumber());
		spanVoidDataTrx.setDocumentDate(details.getDocumentDate());
		spanVoidDataTrx.setBeneficiaryName(details.getBenName());
		spanVoidDataTrx.setBeneficiaryBankCode(details.getBeneficiaryBankCode());
		spanVoidDataTrx.setBeneficiaryBank(details.getBeneficiaryBankName());
		spanVoidDataTrx.setBeneficiaryAccount(details.getBenAccount());
		spanVoidDataTrx.setAmount(details.getAmount());
		spanVoidDataTrx.setDescription(details.getDescription());
		spanVoidDataTrx.setAgentBankCode(details.getAgentBankCode());
		spanVoidDataTrx.setAgentBankAccountNo(details.getAgentBankAccNo());
		spanVoidDataTrx.setAgentBankAccountName(details.getAgentBankAccName());
		spanVoidDataTrx.setPaymentMethod(details.getPaymentMethods());
		spanVoidDataTrx.setSp2dCount(details.getSpanCount());
		return spanVoidDataTrx;
	}
	
	private static void InsertSPANDataVoidTransaction(SpanVoidDataTrx spanVoidDataTrx, SpanVoidDataTrxDao spanVoidDataTrxDao) {
		spanVoidDataTrxDao.InsertSPANDataVoidTransaction(spanVoidDataTrx);
	}
	
	private void updateResponseToVOIDRC(SpanHostResponse spanHostResponse) {
		spanHostResponse.setErrorCode("78");
		spanHostResponse.setResponseCode("78");
		spanHostResponse.setErrorMessage("VOID");
		spanHostResponse.setInstructionCode1("1");
		getSpanHostResponseDaoBean().save(spanHostResponse);
	}
	
	private void updateResponseToVOIDId(SpanHostResponse spanHostResponse) {
		getSpanHostResponseDaoBean().updateResponseToVOIDId(spanHostResponse.getTrxHeaderId(), spanHostResponse.getBatchId(), spanHostResponse.getTrxDetailId());
	}
	
	private void updateSpanDataValidation(int recordOri, int amountOri, String fileName) {
		boolean voidFileFlag = false;
		if(recordOri==0){
			voidFileFlag = true;
		}
		getSpanDataValidationDaoBean().updateSpanDataValidationWhenNewVoid(recordOri, amountOri, fileName, voidFileFlag);
	}
	
	private void updateSpanDataFailed(String fileName, int size) {
		SpanHostDataFailed spanHostDataFailed = getSpanHostDataFailedDaoBean().getSpanHostDataFailed(fileName);
		spanHostDataFailed.setRetryStatus(spanHostDataFailed.getRetryStatus()-size);
		if(spanHostDataFailed.getSumStatusRty() != 2){
			spanHostDataFailed.setSumStatusRty((long) 1);
		}
		getSpanHostDataFailedDaoBean().save(spanHostDataFailed);
	}
	
	public SPANDataValidationDao getSpanDataValidationDaoBean(){
		return (SPANDataValidationDao) applicationBean.getCorpGWContext().getBean("SPANDataValidationDao");
	}
	
	public SpanHostResponseDao getSpanHostResponseDaoBean(){
		return (SpanHostResponseDao) applicationBean.getCorpGWContext().getBean("SpanHostResponseDao");
	}
	
	public SpanHostDataFailedDao getSpanHostDataFailedDaoBean(){
		return (SpanHostDataFailedDao) applicationBean.getCorpGWContext().getBean("SpanHostDataFailedDao");
	}

	private void createSpanSp2dvoidLogList(String filename, String sp2dNumber, String datetimeVoid) {
		SpanSp2dVoidLog spanSp2dVoidLog = new SpanSp2dVoidLog();
		spanSp2dVoidLog.setFilename(filename);
		spanSp2dVoidLog.setSp2dNo(sp2dNumber);
		spanSp2dVoidLog.setDatetimeVoid(datetimeVoid);

		spanSp2dVoidLogs.add(spanSp2dVoidLog);
	}

	@SuppressWarnings("rawtypes")
	public void unvoidChecked() {
		doLoging(ConstantsUserLog.MENU_MONITORING_VOID_DEPKEU,
				tabSelected.concat(" ").concat(ConstantsUserLog.ACTIVITY_UNVOID_CHECKED), ": UnVoidCheckedExecute.");
		Iterator entries = resultTemps.entrySet().iterator();
		while (entries.hasNext()) {
			Entry thisEntry = (Entry) entries.next();
			SpanMonitoringVoidDepKeu s = (SpanMonitoringVoidDepKeu) thisEntry.getValue();
			SPANDataValidationDao spanDataValidationDao = getSPANDataValidationDao();
			Integer jml = 0;
			jml = getSpanVoidDepKeuDAOBean().countVoid("0", s.getFileName(), s.getSp2dNumber());
			int totalAmountVoid = getSpanVoidDepKeuDAOBean().sumAmountVoid("0", s.getFileName(), s.getSp2dNumber());
			int deleteSpanVoidDataTrx = getSpanVoidDepKeuDAOBean().deleteSpanVoidDataTrx("0", s.getFileName(),
					s.getSp2dNumber());
			if (jml > 0) {
				// spanDataValidationDao.updateUnvoidSpanDataValidation(s.getTotalAmount(),
				// s.getTotalRecord(), s.getFileName(), "1", "0"); //Masih bug
				spanDataValidationDao.updateUnvoidSpanDataValidation(Integer.toString(totalAmountVoid),
						Integer.toString(jml), s.getFileName(), "1", "0"); // Solved
			}

			if (deleteSpanVoidDataTrx > 0) {
				String headerMessage = "Success";
				String bodyMessage = "Unvoid Success ";
				FacesUtil.setInfoMessageDialog(headerMessage, bodyMessage);
				doLoging(ConstantsUserLog.MENU_MONITORING_VOID_DEPKEU,
						tabSelected.concat(" ").concat(ConstantsUserLog.ACTIVITY_UNVOID_CHECKED),
						": " + s.getFileName() + "-" + s.getSp2dNumber());
			}
		}
		System.out.println("UnVoidChecked Executed");
	}

	public void initDialogVoidDocumentNumber() {
		documentNumber = "";
		spanHostDataDetail = null;
		setDoShowVoidDocumentNumber(false);
		setDoShowSearchMessage(false);
		doLoging(ConstantsUserLog.MENU_MONITORING_VOID_DEPKEU,
				tabSelected.concat(" ").concat(ConstantsUserLog.ACTIVITY_INIT), ": initDialogVoidDocumentNumber.");
	}

	public void initDialogVoidSp2d() {
		filenameFilter = "";
		sp2dNoFilter = "";

		monitoringVoidSp2dList = new VoidSp2dLogModel();
		doLoging(ConstantsUserLog.MENU_MONITORING_VOID_DEPKEU,
				tabSelected.concat(" ").concat(ConstantsUserLog.ACTIVITY_INIT), ": initDialogVoidSp2d.");
	}

	public void initDialogVoidSp2dBackdate() {
		spanDataValidationBackdateList = new BackdateDataModel();
		doLoging(ConstantsUserLog.MENU_MONITORING_VOID_DEPKEU,
				tabSelected.concat(" ").concat(ConstantsUserLog.ACTIVITY_INIT), ": initDialogVoidSp2dBackdate.");
	}

	public void initDialogVoidHistory() {
		// filspanDataValidationBackdateList = new
		// ArrayList<SpanMonitoringVoidDepKeu>();
		voidHistoryList = loadHist();
		doLoging(ConstantsUserLog.MENU_MONITORING_VOID_DEPKEU,
				tabSelected.concat(" ").concat(ConstantsUserLog.ACTIVITY_INIT), ": initDialogVoidHistory.");
	}

	public void searchDocumentNumber() {
		if (!documentNumber.equals("") & documentNumber != null) {
			if (documentNumber.length() == 21) {
				setDoShowSearchMessage(false);
				searchMessage = "";
				spanHostDataDetail = getSPANHostDataDetailsDao().getSearchDocumentNumber(documentNumber);
				try {
					if (spanHostDataDetail.getDocumentNumber() != null
							|| !spanHostDataDetail.getDocumentNumber().equals("")) {
						int jml = getSpanVoidDepKeuDAOBean().countSearchDocumentNumber(documentNumber, "0");
						if (jml == 0) {
							if (spanHostDataDetail.getExecStatus().equals("1")) {
								setDoShowVoidDocumentNumber(false);
								setDoShowSearchMessage(true);
								searchMessage = "*Document Number allready executed.*";
							} else {
								setDoShowVoidDocumentNumber(true);
							}
						} else {
							setDoShowVoidDocumentNumber(false);
							setDoShowSearchMessage(true);
							searchMessage = "*Document Number allready Void*";
						}
					} else {
						setDoShowVoidDocumentNumber(false);
						setDoShowSearchMessage(true);
						searchMessage = "*Document Number not found, or not extracted yet.*";
					}
				} catch (Exception e) {
					setDoShowVoidDocumentNumber(false);
					setDoShowSearchMessage(true);
					searchMessage = "*Document Number not found, or not extracted yet.*";
				}

			} else {
				setDoShowSearchMessage(true);
				spanHostDataDetail = null;
				searchMessage = "*Document Number length must be 21*";
			}
		} else {
			setDoShowSearchMessage(true);
			spanHostDataDetail = null;
			searchMessage = "*Document Number can't be null*";
		}
		doLoging(ConstantsUserLog.MENU_MONITORING_VOID_DEPKEU,
				tabSelected.concat(" ").concat(ConstantsUserLog.ACTIVITY_SEARCH_DOC_NO),
				": ".concat(documentNumber).concat(".").concat(searchMessage));
	}

	public void doVoidDocumentNumber() {
		SystemParameterDao systemParameterDao = (SystemParameterDao) applicationBean.getCorpGWContext()
				.getBean("systemParameterDAO");
		VoidDocumentNumberRequest voidDocumentNumberRequest = new VoidDocumentNumberRequest();
		voidDocumentNumberRequest.setInvokeType("1");

		List<VoidDocumentNumberVFiles> voidDocumentNumberVFilesList = new ArrayList<VoidDocumentNumberVFiles>();
		VoidDocumentNumberVFiles voidDocumentNumberVFiles = new VoidDocumentNumberVFiles();
		voidDocumentNumberVFiles.setFileName(spanHostDataDetail.getFileName());
		List<String> documentNoList = new ArrayList<String>();
		documentNoList.add(spanHostDataDetail.getDocumentNumber());
		voidDocumentNumberVFiles.setDocumentNoList(documentNoList);
		voidDocumentNumberVFilesList.add(voidDocumentNumberVFiles);

		voidDocumentNumberRequest.setVoidDocumentNumberVFilesList(voidDocumentNumberVFilesList);
		VoidDocumentNo.VoidDocumentNoProcess(voidDocumentNumberRequest, getSPANDataValidationDao(), systemParameterDao, getSpanVoidDataTrxDAOBean(), getSPANHostDataDetailsDao());
			
		autoSendAck(documentNoList);
		
		setMonitoringVoidList(null);
		resultTemps.clear();
		loadData();
	}	
	
	public void autoSendAck(List<String> documentNoList){
		SystemParameterDao systemParameterDao = (SystemParameterDao) applicationBean.getCorpGWContext().getBean("systemParameterDAO");
		spanVoidDataTrxList = new ArrayList<SpanVoidDataTrx>();
		fillVoidList(documentNoList);
		//o mandiri.span.atomic.response.odbc.ack:sentACKDataFile
		String ackDataFile = "";
		for (SpanVoidDataTrx spanVoidDataTrx : spanVoidDataTrxList) {
			ackDataFile = ackDataFile.concat(constructAckString(spanVoidDataTrx.getDocumentDate(), spanVoidDataTrx.getDocumentNo(), spanVoidDataTrx.getFileName())).concat("\n");
		}
		if(null != spanVoidDataTrxList && spanVoidDataTrxList.size()>0){
			String documentType = "SP2D";
			SentAckDataFileRequest sentAckDataFileRequest = new SentAckDataFileRequest();
			sentAckDataFileRequest.setFilename(constructFilename());
			sentAckDataFileRequest.setDocumentType(documentType);
			sentAckDataFileRequest.setErrorMessage(Constants.MSG_VOID);
			sentAckDataFileRequest.setErrorCode(Constants.RC_VOID);
			sentAckDataFileRequest.setAckDataString(ackDataFile);

			SentAckDataFileResponse sentAckDataFileResponse = ACKSknRtgs.sentACKVoidDataFile(sentAckDataFileRequest, systemParameterDao);
			System.out.println(sentAckDataFileResponse.toString());
		}
	}
	
	private String constructFilename() {
		Timestamp now = new Timestamp(Calendar.getInstance().getTime().getTime());
		String bankCode = getSystemParameterDAOBean().getDetailedParameterByParamName(Constants.KODE_BANK_12_DIGIT).getParamValue();
		String sp2d = "SP2D";
		String fa = "FA";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
		String dateTime = sdf.format(now);
		String ext = ".jar";
		return bankCode+"_"+sp2d+"_"+fa+"_"+dateTime+ext;
	}
	
	public void fillVoidList(List<String> documentNoList){
		spanVoidDataTrxList = getSpanVoidDataTrxDAOBean().getAutoSendAckVoidListByDocumentList(documentNoList);
	}
	
	private String constructAckString(String docDate,String docNumber,String fileName){
		String documentDate = PubUtil.padRight(docDate, 10);
		String documentType = "SP2D";
		String documentNumber = PubUtil.padRight(docNumber, 21);
		String filename = PubUtil.padRight(fileName,100);
		String returnCode = PubUtil.padLeft(Constants.RC_VOID, "0", 4) ;
		String description = PubUtil.padRight(Constants.MSG_VOID, 200);
		return documentDate+"|"+documentType+"|"+documentNumber+"|"+filename+"|"+returnCode+"|"+description;
	}

	public void doUnvoidDocumentNumber(SPANVoidDocumentNumber spanVoidDocumentNumber) {
		DeleteSPANVoidDataTransactionRequest deleteSPANVoidDataTransactionRequest = new DeleteSPANVoidDataTransactionRequest();
		deleteSPANVoidDataTransactionRequest.setDocumentNo(spanVoidDocumentNumber.getDocumentNumber());
		deleteSPANVoidDataTransactionRequest.setFileName(filenameDetail);
		DeleteSPANVoidDataTransaction.unvoidDocumentNumber(deleteSPANVoidDataTransactionRequest,
				getSpanVoidDataTrxDAOBean());

		doLoging(ConstantsUserLog.MENU_MONITORING_VOID_DEPKEU,
				tabSelected.concat(" ").concat(ConstantsUserLog.ACTIVITY_UNVOID_DOC_NO),
				": ".concat(documentNumber).concat("."));

		setMonitoringVoidList(null);
		resultTemps.clear();
		loadData();
	}

	private void doLoging(String menu, String activity, String information) {
		UserLog userLog = new UserLog();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		userLog.setLogId("LOG-" + sdf.format(new Date()) + "-" + sessionBean.getUser().getUserID());
		userLog.setActivity(activity);
		userLog.setInformation(information);
		userLog.setInsertTime(new Date());
		userLog.setMenu(menu);
		userLog.setUserId(sessionBean.getUser().getUserID());
		userLog.setUserName(sessionBean.getUser().getUserName());
		UserLogHandler.doLog(userLog, getUserDAOBean());
	}

	private UserDao getUserDAOBean() {
		return (UserDao) applicationBean.getCorpGWContext().getBean("userDAO");
	}

	/**
	 * Method for invoke WebMesthods service
	 * 
	 * @param isserver
	 * @param isusername
	 * @param ispassword
	 * @param packageName
	 * @param serviceName
	 * @param input
	 * @return
	 */
	public static IData invoke(String isserver, String isusername, String ispassword, String packageName,
			String serviceName, IData input) {
		Context context = new Context();
		IData output = null;
		try {
			context.connect(isserver, isusername, ispassword);
			output = context.invoke(packageName, serviceName, input);
			context.disconnect();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return output;
	}

	private SpanVoidDataTrxDao getSpanVoidDataTrxDAOBean() {
		return (SpanVoidDataTrxDao) applicationBean.getCorpGWContext().getBean("SPANVoidDataTrxDao");
	}

	private SPANMonitoringVoidDepKeuDao getSpanVoidDepKeuDAOBean() {
		return (SPANMonitoringVoidDepKeuDao) applicationBean.getCorpGWContext().getBean("SPANMonitoringVoidDepKeuDao");
	}

	private SPANHostDataDetailsDao getSPANHostDataDetailsDao() {
		return (SPANHostDataDetailsDao) applicationBean.getCorpGWContext().getBean("SPANHostDataDetailsDao");
	}

	private SPANDataValidationDao getSPANDataValidationDao() {
		return (SPANDataValidationDao) applicationBean.getCorpGWContext().getBean("SPANDataValidationDao");
	}

	private SystemParameterDao getSystemParameterDAOBean() {
		return (SystemParameterDao) applicationBean.getCorpGWContext().getBean("systemParameterDAO");
	}
	
	public ApplicationBean getApplicationBean() {
		return applicationBean;
	}

	public void setApplicationBean(ApplicationBean applicationBean) {
		this.applicationBean = applicationBean;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public Boolean getDoShowGAJI() {
		return doShowGAJI;
	}

	public void setDoShowGAJI(Boolean doShowGAJI) {
		this.doShowGAJI = doShowGAJI;
	}

	public Boolean getDoShowGAJIR() {
		return doShowGAJIR;
	}

	public void setDoShowGAJIR(Boolean doShowGAJIR) {
		this.doShowGAJIR = doShowGAJIR;
	}

	public String getTabSelected() {
		return tabSelected;
	}

	public void setTabSelected(String tabSelected) {
		this.tabSelected = tabSelected;
	}

	public LazyDataModel<SpanMonitoringVoidDepKeu> getMonitoringVoidList() {
		return monitoringVoidList;
	}

	public void setMonitoringVoidList(LazyDataModel<SpanMonitoringVoidDepKeu> monitoringVoidList) {
		this.monitoringVoidList = monitoringVoidList;
	}

	public String getSp2dNumber() {
		return sp2dNumber;
	}

	public void setSp2dNumber(String sp2dNumber) {
		this.sp2dNumber = sp2dNumber;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	public Boolean getDoShowVoidDocumentNumber() {
		return doShowVoidDocumentNumber;
	}

	public void setDoShowVoidDocumentNumber(Boolean doShowVoidDocumentNumber) {
		this.doShowVoidDocumentNumber = doShowVoidDocumentNumber;
	}

	public SPANHostDataDetails getSpanHostDataDetail() {
		return spanHostDataDetail;
	}

	public void setSpanHostDataDetail(SPANHostDataDetails spanHostDataDetail) {
		this.spanHostDataDetail = spanHostDataDetail;
	}

	public Boolean getDoShowSearchMessage() {
		return doShowSearchMessage;
	}

	public void setDoShowSearchMessage(Boolean doShowSearchMessage) {
		this.doShowSearchMessage = doShowSearchMessage;
	}

	public String getSearchMessage() {
		return searchMessage;
	}

	public void setSearchMessage(String searchMessage) {
		this.searchMessage = searchMessage;
	}

	public LazyDataModel<SPANVoidDocumentNumber> getVoidDocumentNumberList() {
		return voidDocumentNumberList;
	}

	public void setVoidDocumentNumberList(LazyDataModel<SPANVoidDocumentNumber> voidDocumentNumberList) {
		this.voidDocumentNumberList = voidDocumentNumberList;
	}

	public String getFilenameDetail() {
		return filenameDetail;
	}

	public void setFilenameDetail(String filenameDetail) {
		this.filenameDetail = filenameDetail;
	}

	public String getSp2dDetail() {
		return sp2dDetail;
	}

	public void setSp2dDetail(String sp2dDetail) {
		this.sp2dDetail = sp2dDetail;
	}

	public LazyDataModel<SpanSp2dVoidLog> getMonitoringVoidSp2dList() {
		return monitoringVoidSp2dList;
	}

	public void setMonitoringVoidSp2dList(LazyDataModel<SpanSp2dVoidLog> monitoringVoidSp2dList) {
		this.monitoringVoidSp2dList = monitoringVoidSp2dList;
	}

	public void doShowDetail(SpanMonitoringVoidDepKeu monitoring, String status) {
		//System.out.println("Show detail summaries.");
		this.status = status;
		//System.out.println(getSPANDataValidationDao().toString());
		//System.out.println("filename : "+monitoring.getFileName());
		
		SPANDataValidation spdv = getSPANDataValidationDao()
				.getOneSPANDataValidation(monitoring.getFileName());
		spanDataValidation = spdv;
		//System.out.println(spdv);
		spanDataSummary.setFileName(monitoring.getFileName());
		detailFileName = spdv.getFileName();
		detailTotalRecord = spdv.getTotalRecord();
		detailTotalAmount = spdv.getTotalAmount();
		detailType = spdv.getSpanFnType();
		detailDebitAccount = spdv.getDebitAccount();
		detailDebitAccountType = spdv.getDebitAccountType();
		detailDocumentDate = spdv.getDocumentDate();
		loadDataDialog();
	}

	

	private void loadDataDialog() {
		setDetailList(new DialogDetailDataModel());
	}

	private class DialogDetailDataModel extends LazyDataModel<DetailsOutputList> {
		private static final long serialVersionUID = 1L;

		public DialogDetailDataModel() {

		}

		@Override
		public List<DetailsOutputList> load(int startingAt, int maxPerPage, String sortField, SortOrder sortOrder,
				Map<String, String> filters) {
			//System.out.println(spanDataValidation.getXmlFileName());
			List<DataArea> dataAreaList = SP2DXmltoObject.ConvertSP2DXMLToObject(spanDataValidation.getXmlFileName());
			List<DetailsOutputList> detailsOutputList = new ArrayList<DetailsOutputList>();
			try {
				for (Iterator iterator = dataAreaList.iterator(); iterator.hasNext();) {
					DataArea dataArea = (DataArea) iterator.next();
					DetailsOutputList d = new DetailsOutputList();
					d.setDocNumber(dataArea.getDocumentNumber());
					d.setCreditAcctNo(dataArea.getBeneficiaryAccount());
					d.setCreditAcctName(dataArea.getBeneficiaryName());
					d.setBenefBankName(dataArea.getBeneficiaryBank());
					detailsOutputList.add(d);
					
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			int jmlAll = detailsOutputList.size();

			List<DetailsOutputList> detailsFinalTemp = new ArrayList<DetailsOutputList>();
			int count = 0;
			for (int i = startingAt; i < detailsOutputList.size(); i++) {
				detailsFinalTemp.add(detailsOutputList.get(i));
				count++;
				if (count == maxPerPage) {
					break;
				}
			}
			setRowCount(jmlAll);
			setPageSize(maxPerPage);

			if (detailsFinalTemp.size() > 0) {
				return detailsFinalTemp;
			} else {
				return null;
			}
		}
	}
}
