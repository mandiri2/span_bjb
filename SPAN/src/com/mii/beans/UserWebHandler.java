package com.mii.beans;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import org.apache.commons.lang.SerializationUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.mii.constant.Constants;
import com.mii.constant.ConstantsUserLog;
import com.mii.dao.ApprovalDao;
import com.mii.dao.BranchDao;
import com.mii.dao.RoleDao;
import com.mii.dao.SystemParameterDao;
import com.mii.dao.UserDao;
import com.mii.helpers.EmailHelper;
import com.mii.helpers.FacesUtil;
import com.mii.helpers.HelperUtils;
import com.mii.helpers.LdapConfigurator;
import com.mii.helpers.LdapConnector;
import com.mii.helpers.PasswordGenerator;
import com.mii.models.Approval;
import com.mii.models.Branch;
import com.mii.models.Role;
import com.mii.models.SystemParameter;
import com.mii.models.User;
import com.mii.models.UserLog;
import com.mii.models.UserRole;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.SessionBean;

@ManagedBean
@ViewScoped
public class UserWebHandler implements Serializable {
	private static final long serialVersionUID = 1L;
	private User user;
	private User tempUser;
	private LazyDataModel < User > userList;
	private List < SelectItem > roleSelectItem = new ArrayList < SelectItem > ();
	private List<SelectItem> branchSelectItem = new ArrayList<SelectItem>();
	private String action;
	private boolean saveUser;
	private boolean addUserRole;

	private boolean saveUserPending;
	private boolean addUserRolePending;

	private String userId = "";
	private String userName = "";
	private String fullName = "";
	private String branch = "";
	private String role = "";
	private List < String > rolesName;
	private List < String > rolesNamePending;
	private List < String > status;
	private boolean pending;
	private boolean approved;
	private boolean rejected;
	
	private boolean teller = false;
	
	private boolean tellerTemp = false;
	
	private String currentUsername;
	
	private String duplicateRole;

	@ManagedProperty(value = "#{applicationBean}")
	private ApplicationBean applicationBean;
	@ManagedProperty(value = "#{sessionBean}")
	private SessionBean sessionBean;

	private boolean doShowSearch = false;
	
	private boolean doShowHistory = false;
	
	private String roleIdTeller;
	private String roleIdTellerSupervisor;

	@PostConstruct
	public void init() {

		FacesUtil.authMenu(sessionBean.getUser(), "userWebAdministration");
		userList = new UserDataModel();
		saveUser = true;
		this.addUserRole = true;
		this.roleSelectItem = new ArrayList < SelectItem > ();
		RoleDao roleDao = (RoleDao) applicationBean.getCorpGWContext().getBean(
			"roleDAO");
		List < Role > allRoles = roleDao.getAllValidRole();
		for (Role role: allRoles) {
			SelectItem selectItem = new SelectItem();
			selectItem.setValue(role.getRoleId());
			selectItem.setLabel(role.getRoleName());
			this.roleSelectItem.add(selectItem);
		}
		loadBranchList();
		pending = false;
		approved = false;
		rejected = false;

		doSearch();
		
		/*SystemParameter systemParameter	= getSystemParameterDAOBean().getDetailedParameterByParamName(Constants.ROLE_ID_TELLER);
		roleIdTeller	= systemParameter.getParamValue();*/
		roleIdTeller = "";
		
		/*systemParameter	= getSystemParameterDAOBean().getDetailedParameterByParamName(Constants.ROLE_ID_TELLER_SUPERVISOR);
		roleIdTellerSupervisor	= systemParameter.getParamValue();*/
		roleIdTellerSupervisor = "";
		
		doLoging(ConstantsUserLog.MENU_USER, ConstantsUserLog.ACTIVITY_INIT, "Menu Init.");
	}
	
	private void loadBranchList(){
		this.branchSelectItem = new ArrayList<SelectItem>();
		BranchDao branchDao = (BranchDao) applicationBean.getCorpGWContext().getBean("branchDAO");
		List<Branch> allBranch = branchDao.getAllValidBranch();
		for(Branch branch : allBranch){
			SelectItem selectItem = new SelectItem();
			selectItem.setValue(branch.getBranchCode());
			selectItem.setLabel(branch.getBranchName()+" - "+branch.getBranchCode());
			this.branchSelectItem.add(selectItem);
		}
	}

	private UserDao getUserDAOBean() {
		return (UserDao) applicationBean.getCorpGWContext().getBean("userDAO");
	}

	private ApprovalDao getApprovalDAOBean() {
		return (ApprovalDao) applicationBean.getCorpGWContext().getBean(
			"approvalDAO");
	}
	

	private SystemParameterDao getSystemParameterDAOBean(){
		return (SystemParameterDao) applicationBean.getCorpGWContext().getBean("systemParameterDAO");
	}

	private BranchDao getBranchDAOBean(){
		return (BranchDao) applicationBean.getCorpGWContext().getBean("branchDAO");
	}
	

	public boolean isDoShowSearch() {
		return doShowSearch;
	}

	public void setDoShowSearch(boolean doShowSearch) {
		this.doShowSearch = doShowSearch;
	}

	public void showSearch() {
		doShowSearch = true;
	}

	public void hideSearch() {
		doShowSearch = false;
	}
	
	
	public void showHistory(){
		doShowHistory = true;
	}
	
	public void hideHistory(){
		doShowHistory = false;
	}
	
	public boolean isDoShowHistory() {
		return doShowHistory;
	}

	public void setDoShowHistory(boolean doShowHistory) {
		this.doShowHistory = doShowHistory;
	}

	public List<SelectItem> getBranchSelectItem() {
		return branchSelectItem;
	}

	public void setBranchSelectItem(List<SelectItem> branchSelectItem) {
		this.branchSelectItem = branchSelectItem;
	}
	
	public List<String> getRolesName() {
		return rolesName;
	}

	public void setRolesName(List<String> rolesName) {
		this.rolesName = rolesName;
	}

	public List<String> getRolesNamePending() {
		return rolesNamePending;
	}

	public void setRolesNamePending(List<String> rolesNamePending) {
		this.rolesNamePending = rolesNamePending;
	}

	
	public boolean isTellerTemp() {
		return tellerTemp;
	}

	public void setTellerTemp(boolean tellerTemp) {
		this.tellerTemp = tellerTemp;
	}

	public boolean isSaveUserPending() {
		return saveUserPending;
	}

	public void setSaveUserPending(boolean saveUserPending) {
		this.saveUserPending = saveUserPending;
	}

	public boolean isAddUserRolePending() {
		return addUserRolePending;
	}

	public void setAddUserRolePending(boolean addUserRolePending) {
		this.addUserRolePending = addUserRolePending;
	}
	
	public void checkRole(){
		checkRoleTeller(this.user.getRoles());
	}
	public void checkRolePending(){
		checkRoleTellerTemp(this.tempUser.getRoles());
	}
	
	public void checkRoleTeller(List<UserRole> userRoles){
		
		int countTeller = 0;
		for (UserRole userRole : userRoles){
			if (userRole.getRole() != null){
				
				if ((roleIdTeller != null) && (roleIdTellerSupervisor != null)){
					if ((userRole.getRole().equals(roleIdTeller)) || (userRole.getRole().equals(roleIdTellerSupervisor))){
						countTeller = countTeller + 1;
					}
				}
			}
		}
		if (countTeller > 0 ){
			teller = true;
		}else{
			teller = false;
		}

	}
	
	public void checkRoleTellerTemp(List<UserRole> userRoles){
		
		int countTeller = 0;
		for (UserRole userRole : userRoles){
			if (userRole.getRole() != null){
				
				if ((roleIdTeller != null) && (roleIdTellerSupervisor != null)){
					if ((userRole.getRole().equals(roleIdTeller)) || (userRole.getRole().equals(roleIdTellerSupervisor))){
						countTeller = countTeller + 1;
					}
				}
			}
		}
		if (countTeller > 0 ){
			tellerTemp = true;
		}else{
			tellerTemp = false;
		}

	}
	
	public boolean isTeller() {
		return teller;
	}

	public void setTeller(boolean teller) {
		this.teller = teller;
	}

	public void viewDetailedUser(User user) {
		UserDao userDao = getUserDAOBean();
		this.user = userDao.getDetailedUser(user.getUserID());
		this.user.setBranchName(user.getBranchName());
		this.rolesName = new ArrayList<String>();
		
		for (UserRole userRole : this.user.getRoles()){
			RoleDao roleDao = (RoleDao) applicationBean.getCorpGWContext().getBean("roleDAO");
			Role role = new Role();
			role.setRoleId(userRole.getRole());
			role = roleDao.getDetailedRole(role);
			this.rolesName.add(role.getRoleName());
			
		}


		if (this.user.getStatus()
			.equals(Constants.APPROVAL_STATUS_PENDING_EDIT)) {
			List < Approval > approvals = getApprovalDAOBean()
				.getApprovalByParameterId(user.getUserID());
			if (approvals.size() > 0) {
				try {
					this.tempUser = (User) HelperUtils.deserialize(approvals.get(0).getNewBlobObject().getBinaryStream());
					Branch branch = getBranchDAOBean().getBranchByBranchCode(this.tempUser.getBranch());
					if (branch != null) this.tempUser.setBranchName(branch.getBranchName());
					this.rolesNamePending = new ArrayList<String>();
					
					for (UserRole userRole : this.tempUser.getRoles()){
						RoleDao roleDao = (RoleDao) applicationBean.getCorpGWContext().getBean("roleDAO");
						Role role = new Role();
						role.setRoleId(userRole.getRole());
						role = roleDao.getDetailedRole(role);
						this.rolesNamePending.add(role.getRoleName());
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				checkRoleTellerTemp(this.tempUser.getRoles());
				RequestContext.getCurrentInstance().execute(
					"viewUserEdit.show()");
			}
		} else {

			
			checkRoleTeller(this.user.getRoles());
			RequestContext.getCurrentInstance().execute("viewUser.show()");
		}
	}

	public void viewDetailedUserEdit(User user) {

		UserDao userDao = getUserDAOBean();
		this.user = userDao.getDetailedUser(user.getUserID());
		this.user.setBranchName(user.getBranchName());
		for (UserRole userRole: this.user.getRoles()) {
			userRole.setIdx(HelperUtils.generateUniqueString());
		}
		

		if (this.user.getDisableEdit() == true) {
			FacesUtil.setApprovalWarning("user", this.user.getUserName(), this.user.getStatus());
		} else {

			if (this.user.getStatus().equals(
			Constants.APPROVAL_STATUS_PENDING_EDIT)) {

				List < Approval > approvals = getApprovalDAOBean()
					.getApprovalByParameterId(user.getUserID());
				if (approvals.size() > 0) {
					try {
						this.tempUser = (User) HelperUtils.deserialize(approvals.get(0)
							.getNewBlobObject().getBinaryStream());
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}


					checkRoleTeller(this.user.getRoles());
					RequestContext.getCurrentInstance().execute("insertOrEditUserPending.show()");
				}
			} else {

				this.tempUser = (User) SerializationUtils.clone(this.user);


				checkRoleTellerTemp(this.tempUser.getRoles());
				
				RequestContext.getCurrentInstance().execute("insertOrEditUser.show()");
			}
			currentUsername = this.tempUser.getUserName();
			this.action = "edit";

		}
	}

	public void viewDetailedDelete(User user) {

		UserDao userDao = getUserDAOBean();
		this.user = userDao.getDetailedUser(user.getUserID());

		if (this.user.getDisableDelete() == true) {
			FacesUtil.setApprovalWarning("user", this.user.getUserName(), this.user.getStatus());
		} else {
			RequestContext.getCurrentInstance().execute("dlgConfirm.show()");
		}
	}

	public void checkDuplicateRole(UserRole userRole) {
		int duplicateRole = 0;
		for (UserRole userRole2: this.getUser().getRoles()) {
			if (userRole2.getRole() != null && userRole.getRole().equals(userRole2.getRole()) && !userRole.getIdx().equals(userRole2.getIdx())) {
				duplicateRole++;
			}
		}
		if (duplicateRole == 0) {
			this.saveUser = true;
			this.addUserRole = true;
		} else {
			this.saveUser = false;
			this.addUserRole = false;
			RoleDao roleDao = (RoleDao) applicationBean.getCorpGWContext().getBean("roleDAO");
			Role role = new Role();
			role.setRoleId(userRole.getRole());
			role = roleDao.getDetailedRole(role);
			FacesUtil.setErrorMessageDialog("Error",
				"Duplicate Role " + role.getRoleName());
		}
	}
	
	public boolean checkDuplicateRole(User user){
		
		for (UserRole ur1 : user.getRoles()){
			int count = 0;
			for (UserRole ur2 : user.getRoles()){
				if (ur1.getRole().equals(ur2.getRole())){
					count ++;
				}
			}
			if (count > 1){
				RoleDao roleDao = (RoleDao) applicationBean.getCorpGWContext().getBean("roleDAO");
				Role role = new Role();
				role.setRoleId(ur1.getRole());
				role = roleDao.getDetailedRole(role);
				
				this.duplicateRole = role.getRoleName();

				
				return true;
			}
		}
		return false;
	}
	

	public void newUser() {
		this.user = new User();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd-hhmm-sss");
		Date date = new Date();
		String id = sdf.format(date);
		Integer seq = getSystemParameterDAOBean().getParameterSequence().intValue();
		this.user.setUserID("USR-"+id+String.format("%04d", seq));
		this.action = "";
		teller = false;
		tellerTemp = false;
	}

	public void addRoleForm() {
		if (user.getRoles() != null) {
			UserRole userRole = new UserRole();
			userRole.setIdx(HelperUtils.generateUniqueString());
			user.getRoles().add(userRole);
		}
	}
	
	public void addRoleFormTemp() {
		if (tempUser.getRoles() != null) {
			UserRole userRole = new UserRole();
			userRole.setIdx(HelperUtils.generateUniqueString());
			tempUser.getRoles().add(userRole);
		}
	}

	public void removeRoleForm(UserRole userRole) {
		if (user.getRoles() != null) {
			user.getRoles().remove(userRole);
			this.saveUser = true;
			this.addUserRole = true;
		}
		checkRoleTeller(this.user.getRoles());
	}
	
	public void removeRoleFormTemp(UserRole userRole) {
		if (tempUser.getRoles() != null) {
			tempUser.getRoles().remove(userRole);
			this.saveUserPending = true;
			this.addUserRolePending = true;
		}
		checkRoleTellerTemp(this.tempUser.getRoles());
	}

	public void doSave() {
			user.setBranch("0001"); //Hardcode Branch for BJB
			if (this.action != null && this.action.equals("edit")) {
				if (this.user.getStatus().equals(
				Constants.APPROVAL_STATUS_PENDING_EDIT)) {
					if (checkDuplicateRole(this.tempUser)){
						FacesUtil.setErrorMessageDialog("Error",
								"Duplicate Role "+this.duplicateRole);
					}else{
						User userExist = getUserDAOBean().getByUserName(this.tempUser.getUserName());
						
						if ((userExist == null) || (userExist.getUserName().equals(currentUsername))){

//							if (LdapConnector.authenticate(sessionBean.getUser().getUserName(), sessionBean.getUser().getPassword(),this.tempUser.getUserName())){
								try {
									getApprovalDAOBean().updateApprovalNewObjectByParameterId(
									HelperUtils.serialize(this.tempUser),
									this.tempUser.getUserID());
									
		
									RequestContext.getCurrentInstance().execute("insertOrEditUserPending.hide()");
									RequestContext.getCurrentInstance().execute("insertOrEditUser.hide()");
									
				
									String headerMessage = "Edit User Successfully";
									String bodyMessage = "Supervisor must approve the editing of the <br/>user &apos;" + this.user.getUserName() + "&apos; to completely the process.";
									doLoging(ConstantsUserLog.MENU_USER, ConstantsUserLog.ACTIVITY_EDIT, "Edit User "+this.user.getUserName()+". Waiting for Approval.");
									FacesUtil.setInfoMessageDialog(headerMessage, bodyMessage);
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
//							}else{
//								String headerMessage = "Attention";
//								String bodyMessage = "User is not registered in LDAP server. <br/>Please contact the administrator.";
				
//								FacesUtil.setWarningMessageDialog(headerMessage, bodyMessage);
//							}
						}else{
							String headerMessage = "Attention";
							String bodyMessage = "User "+this.tempUser.getUserName()+" already exist in the application.";
							
							FacesUtil.setWarningMessageDialog(headerMessage, bodyMessage);
						}
					}
				} else {
					if (checkDuplicateRole(this.user)){
						FacesUtil.setErrorMessageDialog("Error",
								"Duplicate Role "+this.duplicateRole);
					}else{
						User userExist = getUserDAOBean().getByUserName(this.user.getUserName());
						
						if ((userExist == null) || (userExist.getUserName().equals(currentUsername))){


//							if (LdapConnector.authenticate(sessionBean.getUser().getUserName(), sessionBean.getUser().getPassword(),this.user.getUserName())){
								for (UserRole userRole: this.user.getRoles()) {
									userRole.setUserId(this.user.getUserID());
								}
								editUser(this.user);
//							}else{
//								String headerMessage = "Attention";
//								String bodyMessage = "User is not registered in LDAP server. <br/>Please contact the administrator.";
				
//								FacesUtil.setWarningMessageDialog(headerMessage, bodyMessage);
//							}
						}else{
							String headerMessage = "Attention";
							String bodyMessage = "User "+this.user.getUserName()+" already exist in the application.";
							
							FacesUtil.setWarningMessageDialog(headerMessage, bodyMessage);
						}
					}
				}
			} else {
				
				if (checkDuplicateRole(this.user)){
					FacesUtil.setErrorMessageDialog("Error",
							"Duplicate Role "+this.duplicateRole);
				}else{
				
					User userExist = getUserDAOBean().getByUserName(this.user.getUserName());
					
					if (userExist == null){
					
//						if (LdapConnector.authenticate(sessionBean.getUser().getUserName(), sessionBean.getUser().getPassword(),this.user.getUserName())){
							try {
								for (UserRole userRole: this.user.getRoles()) {
									userRole.setUserId(this.user.getUserID());
								}
								createUser(this.user);
							} catch (Exception e) {
			
								String headerMessage = "Error";
								String bodyMessage = "System error";
			
								FacesUtil.setErrorMessageDialog(headerMessage, bodyMessage);
								e.printStackTrace();
							}
//						}else{
//							String headerMessage = "Attention";
//							String bodyMessage = "User is not registered in LDAP server. <br/>Please contact the administrator.";
//			
//							FacesUtil.setWarningMessageDialog(headerMessage, bodyMessage);
//						}
					}else{
						String headerMessage = "Attention";
						String bodyMessage = "User "+this.user.getUserName()+" already exist in the application.";
		
						FacesUtil.setWarningMessageDialog(headerMessage, bodyMessage);
					}
				}
				
			}
	}

	public void editUser(User user) {
		UserDao userDao = getUserDAOBean();
		user.setChangeDate(HelperUtils.getCurrentDateTime());
		user.setChangeWho(sessionBean.getUser().getUserName());
		try {
			if (user.getStatus().equals(
			Constants.APPROVAL_STATUS_PENDING_CREATE)) {

				getApprovalDAOBean().updateApprovalNewObjectByParameterId(HelperUtils.serialize(this.user),
				this.user.getUserID());

			} else if (user.getStatus().equals(
				Constants.APPROVAL_STATUS_REJECTED_CREATE)) {

				Approval approval = new Approval();
				approval.setAction(Constants.APPROVAL_ACTION_CREATE);
				approval.setParameter(Constants.APPROVAL_PARAMETER_USER);
				approval.setParameterId(user.getUserID());
				approval.setParameterName(user.getUserName());
				approval.setOldByteObject(HelperUtils.serialize(this.tempUser));
				approval.setNewByteObject(HelperUtils.serialize(this.user));
				approval.setBranchId(sessionBean.getUser().getBranch());
				getApprovalDAOBean().saveApproval(approval);

				user = (User) SerializationUtils.clone(this.tempUser);
				user.setStatus(Constants.APPROVAL_STATUS_PENDING_CREATE);

			} else {

				Approval approval = new Approval();
				approval.setAction(Constants.APPROVAL_ACTION_EDIT);
				approval.setParameter(Constants.APPROVAL_PARAMETER_USER);
				approval.setParameterId(user.getUserID());
				approval.setParameterName(user.getUserName());
				approval.setOldByteObject(HelperUtils.serialize(this.tempUser));
				approval.setNewByteObject(HelperUtils.serialize(this.user));
				approval.setBranchId(sessionBean.getUser().getBranch());
				getApprovalDAOBean().saveApproval(approval);

				user = (User) SerializationUtils.clone(this.tempUser);
				user.setStatus(Constants.APPROVAL_STATUS_PENDING_EDIT);

			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		userDao.editUser(user);
		

		RequestContext.getCurrentInstance().execute("insertOrEditUserPending.hide()");
		RequestContext.getCurrentInstance().execute("insertOrEditUser.hide()");
		
		String headerMessage = "Edit User Successfully";
		String bodyMessage = "Supervisor must approve the editing of the <br/>user &apos;" + this.user.getUserName() + "&apos; to completely the process.";
		doLoging(ConstantsUserLog.MENU_USER, ConstantsUserLog.ACTIVITY_EDIT, "Edit User "+this.user.getUserName()+". Waiting for Approval.");
		FacesUtil.setInfoMessageDialog(headerMessage, bodyMessage);
		
	}

	public void createUser(User user) throws Exception {
		UserDao userDao = getUserDAOBean();
		user.setCreateDate(HelperUtils.getCurrentDateTime());
		user.setCreateWho(sessionBean.getUser().getUserName());
		user.setStatus(Constants.APPROVAL_STATUS_PENDING_CREATE);
		//Generate First Password and First Login Flag
		user.setPassword(PasswordGenerator.generatePassword());
		user.setFirstLogin(Constants.FISTLOGINYES);

		Approval approval = new Approval();
		approval.setAction(Constants.APPROVAL_ACTION_CREATE);
		approval.setParameter(Constants.APPROVAL_PARAMETER_USER);
		approval.setParameterId(user.getUserID());
		approval.setParameterName(user.getUserName());
		approval.setNewByteObject(HelperUtils.serialize(this.user));
		approval.setBranchId(sessionBean.getUser().getBranch());
		getApprovalDAOBean().saveApproval(approval);
		userDao.saveUserToDB(user);
		getApprovalDAOBean().updateApprovalNewObjectByParameterId(HelperUtils.serialize(this.user),
				this.user.getUserID());
		
		RequestContext.getCurrentInstance().execute("insertOrEditUser.hide()");
		RequestContext.getCurrentInstance().execute("insertOrEditUserPending.hide()");
		
		String headerMessage = "Create User Successfully";
		String bodyMessage = "Supervisor must approve the creating of the <br/>user &apos;" + this.user.getUserName() + "&apos; to completely the process.";
		doLoging(ConstantsUserLog.MENU_USER, ConstantsUserLog.ACTIVITY_SAVE, "Create User "+this.user.getUserName()+". Waiting for Approval.");
		//EMAIL TO USER
		try{
			EmailHelper.newUserMail(user.getUserName(), null, user.getEmail(), Constants.EMAILWAIT, getSystemParameterDAOBean());
		}catch(Exception e){
			e.printStackTrace();
		}
		FacesUtil.setInfoMessageDialog(headerMessage, bodyMessage);
	}

	public void doSearch() {

		status = new ArrayList < String > ();

		if (((pending == false) && (approved == false)) && (rejected == false)) {
			status.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
			status.add(Constants.APPROVAL_STATUS_PENDING_EDIT);
			status.add(Constants.APPROVAL_STATUS_PENDING_DELETE);
			status.add(Constants.APPROVAL_STATUS_APPROVED_CREATE);
			status.add(Constants.APPROVAL_STATUS_APPROVED_EDIT);
			status.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
			status.add(Constants.APPROVAL_STATUS_REJECTED_EDIT);
			status.add(Constants.APPROVAL_STATUS_REJECTED_DELETE);
		} else {
			if (pending == true) {
				status.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
				status.add(Constants.APPROVAL_STATUS_PENDING_EDIT);
				status.add(Constants.APPROVAL_STATUS_PENDING_DELETE);
			}
			if (approved == true) {
				status.add(Constants.APPROVAL_STATUS_APPROVED_CREATE);
				status.add(Constants.APPROVAL_STATUS_APPROVED_EDIT);
			}
			if (rejected == true) {
				status.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
				status.add(Constants.APPROVAL_STATUS_REJECTED_EDIT);
				status.add(Constants.APPROVAL_STATUS_REJECTED_DELETE);
			}
		}

		FacesUtil.resetPage("userForm:userTable");
		userList = new UserDataModel();
	}

	public void deleteUser() {
		
		if (user.getStatus().equals(Constants.APPROVAL_STATUS_REJECTED_CREATE)){
			
			List < UserRole > roles = user.getRoles();
			RoleDao roleDao = (RoleDao) applicationBean.getCorpGWContext().getBean("roleDAO");
			for (UserRole r: roles) {
				roleDao.delete(r);
			}
			
			getUserDAOBean().deleteUser(user.getUserID());
			
			String headerMessage = "Success";
			String bodyMessage = "Delete User " + this.user.getUserName() + " Successfully";
	
			FacesUtil.setInfoMessageDialog(headerMessage, bodyMessage);
			
		}else{
			
			Approval approval = new Approval();
			approval.setAction(Constants.APPROVAL_ACTION_DELETE);
			approval.setParameter(Constants.APPROVAL_PARAMETER_USER);
			approval.setParameterId(user.getUserID());
			approval.setParameterName(user.getUserName());
			approval.setBranchId(sessionBean.getUser().getBranch());
	
			try {
				approval.setOldByteObject(HelperUtils.serialize(this.user));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	
			getApprovalDAOBean().saveApproval(approval);
	
			this.user.setStatus(Constants.APPROVAL_STATUS_PENDING_DELETE);
			getUserDAOBean().save(this.user);
	
			String headerMessage = "Delete User Successfully";
			String bodyMessage = "Supervisor must approve the deleting of the <br/>user &apos;" + this.user.getUserName() + "&apos; to completely the process.";
			doLoging(ConstantsUserLog.MENU_USER, ConstantsUserLog.ACTIVITY_SAVE, "Delete User "+this.user.getUserName()+". Waiting for Approval.");
			FacesUtil.setInfoMessageDialog(headerMessage, bodyMessage);
		}

	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public User getTempUser() {
		return tempUser;
	}

	public void setTempUser(User tempUser) {
		this.tempUser = tempUser;
	}

	public ApplicationBean getApplicationBean() {
		return applicationBean;
	}

	public void setApplicationBean(ApplicationBean applicationBean) {
		this.applicationBean = applicationBean;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public List < String > getStatus() {
		return status;
	}

	public void setStatus(List < String > status) {
		this.status = status;
	}

	public boolean isPending() {
		return pending;
	}

	public void setPending(boolean pending) {
		this.pending = pending;
	}

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}

	public boolean isRejected() {
		return rejected;
	}

	public void setRejected(boolean rejected) {
		this.rejected = rejected;
	}

	private class UserDataModel extends LazyDataModel < User > {
		private static final long serialVersionUID = 1L;

		public UserDataModel() {

		}

		@Override
		public List < User > load(int startingAt, int maxPerPage,
		String sortField, SortOrder sortOrder,
		Map < String, String > filters) {

			UserDao userDao = (UserDao) applicationBean.getCorpGWContext()
				.getBean("userDAO");
			List < User > users;
			User user = userDao.getDetailedUser(sessionBean.getUser().getUserID());
			if (user != null) branch = user.getBranch();
			
			/*SystemParameter systemParameter = getSystemParameterDAOBean().getDetailedParameterByParamName(Constants.KODE_CABANG_KANTOR_PUSAT);
			String kodeCabangPusat = systemParameter.getParamValue();*/
			String kodeCabangPusat = "";
			if (branch.equals(kodeCabangPusat)) branch = "";
			
			users = userDao.getUserLazyLoadingByCategory(maxPerPage + startingAt, startingAt, userId, userName, fullName, branch, role, status);
			Integer jmlAll = userDao.countUserByCategory(userId, userName, fullName, branch, role,
			status);
			setRowCount(jmlAll);
			setPageSize(maxPerPage);

			return users;
		}
	}
	
	private void doLoging(String menu, String activity, String information) {
		UserLog userLog = new UserLog();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		userLog.setLogId("LOG-"+sdf.format(new Date())+"-"+sessionBean.getUser().getUserID());
		userLog.setActivity(activity);
		userLog.setInformation(information);
		userLog.setInsertTime(new Date());
		userLog.setMenu(menu);
		userLog.setUserId(sessionBean.getUser().getUserID());
		userLog.setUserName(sessionBean.getUser().getUserName());
		UserLogHandler.doLog(userLog, getUserDAOBean());
	}

	public LazyDataModel < User > getUserList() {
		return userList;
	}

	public void setUserList(LazyDataModel < User > userList) {
		this.userList = userList;
	}

	public List < SelectItem > getRoleSelectItem() {
		return roleSelectItem;
	}

	public void setRoleSelectItem(List < SelectItem > roleSelectItem) {
		this.roleSelectItem = roleSelectItem;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public boolean isSaveUser() {
		return saveUser;
	}

	public void setSaveUser(boolean saveUser) {
		this.saveUser = saveUser;
	}

	public boolean isAddUserRole() {
		return addUserRole;
	}

	public void setAddUserRole(boolean addUserRole) {
		this.addUserRole = addUserRole;
	}

}