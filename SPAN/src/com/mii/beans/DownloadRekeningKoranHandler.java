package com.mii.beans;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.apache.commons.lang.time.DateUtils;
import org.primefaces.model.LazyDataModel;

import com.mii.bjb.core.GSSBJBConnector;
import com.mii.constant.Constants;
import com.mii.dao.RekeningKoranDao;
import com.mii.dao.SystemParameterDao;
import com.mii.helpers.FacesUtil;
import com.mii.is.util.PubUtil;
import com.mii.json.TransactionHistory;
import com.mii.models.SpanRekeningKoran;
import com.mii.models.SystemParameter;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.SessionBean;

@ManagedBean
@ViewScoped
public class DownloadRekeningKoranHandler implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private Date startDocDate;
	private Date endDocDate;
	private String accountType;
	private String accountNumber;
	private String accountName;
	private long totalDebit;
	private long totalCredit;
	
	private LazyDataModel < SpanRekeningKoran > rekeningKoranList;
	private List<SpanRekeningKoran> rekKoranList;
	
	@ManagedProperty(value="#{applicationBean}")
	private ApplicationBean applicationBean;
	
	@ManagedProperty(value="#{sessionBean}")
	private SessionBean sessionBean;
	
	@PostConstruct
	public void init(){
		FacesUtil.authMenu(getSessionBean().getUser(), "downloadRekeningKoran");
		
		rekKoranList=new ArrayList<SpanRekeningKoran>();
//		forTest(); //Untuk Cobain Report 32.000 Rows silahkan di hapus
		
	}

	private void forTest() {
		//Untuk Cobain Report 32.000 Rows silahkan di hapus
		/*rekKoranList=new ArrayList<SpanRekeningKoran>();
		for(int i=0; i<=200; i++){
			SpanRekeningKoran rekening = new SpanRekeningKoran();
			rekening.setTransactionDate("2Agustus2016");
			rekening.setBeginningBalance("1");
			rekening.setEndingBalance("100");
			rekening.setBankReferenceNumber("122345");
			rekening.setBankTransactionCode("098098");
			rekening.setDebit("10000000");
			rekening.setCredit("2000000");
			rekening.setTotalAmount("5000000");
			this.rekKoranList.add(rekening);
			SpanRekeningKoran rekening2 = new SpanRekeningKoran();
			rekening2.setTransactionDate("3Agustus2016");
			rekening2.setBeginningBalance("2");
			rekening2.setEndingBalance("99");
			rekening2.setBankReferenceNumber("789065");
			rekening2.setBankTransactionCode("5677433");
			rekening2.setDebit("20000000");
			rekening2.setCredit("3000000");
			rekening2.setTotalAmount("6000000");
			this.rekKoranList.add(rekening2);
			SpanRekeningKoran rekening3 = new SpanRekeningKoran();
			rekening3.setTransactionDate("4Agustus2016");
			rekening3.setBeginningBalance("3");
			rekening3.setEndingBalance("98");
			rekening3.setBankReferenceNumber("54321");
			rekening3.setBankTransactionCode("6789087654");
			rekening3.setDebit("30000000");
			rekening3.setCredit("4000000");
			rekening3.setTotalAmount("7000000");
			this.rekKoranList.add(rekening3);
			SpanRekeningKoran rekening4 = new SpanRekeningKoran();
			rekening4.setTransactionDate("5Agustus2016");
			rekening4.setBeginningBalance("4");
			rekening4.setEndingBalance("97");
			rekening4.setBankReferenceNumber("45667432");
			rekening4.setBankTransactionCode("67432235");
			rekening4.setDebit("40000000");
			rekening4.setCredit("5000000");
			rekening4.setTotalAmount("8000000");
			this.rekKoranList.add(rekening4);
		}
		SpanRekeningKoran rekening5 = new SpanRekeningKoran();
		rekening5.setTransactionDate("5Agustus2016");
		rekening5.setBeginningBalance("5");
		rekening5.setEndingBalance("97");
		rekening5.setBankReferenceNumber("45667432");
		rekening5.setBankTransactionCode("67432235");
		rekening5.setDebit("40000000");
		rekening5.setCredit("5000000");
		rekening5.setTotalAmount("8000000");
		this.rekKoranList.add(rekening5);*/
		// Sampai sini
	}
	
	public RekeningKoranDao getRekeningKoranDao(){
		return (RekeningKoranDao) applicationBean.getCorpGWContext().getBean("rekeningKoranDao");
	}
	
	public void doInquiry(){
		SystemParameterDao parameterDao = (SystemParameterDao) applicationBean.getCorpGWContext().getBean("systemParameterDAO");
		SystemParameter parameter;
		SystemParameter parameterName;
		if (accountType.equals("0")){
			parameter = parameterDao.getDetailedParameterByParamName(Constants.PARAM_DEBITACCNO+Constants.GAJI);
			parameterName = parameterDao.getDetailedParameterByParamName(Constants.DEBIT_ACCOUNT_NO_GAJI_NAME);
		}
		else {
			parameter = parameterDao.getDetailedParameterByParamName(Constants.PARAM_DEBITACCNO+Constants.GAJIR);
			parameterName = parameterDao.getDetailedParameterByParamName(Constants.DEBIT_ACCOUNT_NO_GAJIR_NAME);
		}
		accountNumber =	parameter.getParamValue();
		accountName= parameterName.getParamValue();
		if(startDocDate!=null&endDocDate!=null&accountType!=null&!accountType.equals("")){
			if(!endDocDate.before(startDocDate)){
				System.out.println("start get transaction history ..." + accountNumber + accountType);
				System.out.println(startDocDate +" "+endDocDate);
//				rekKoranList = TransactionHistory.getTransactionHistory(parameterDao, accountNumber, startDocDate, endDocDate);
//				rekKoranList = GSSBJBConnector.sendRequestGSS(parameterDao, accountNumber, startDocDate, endDocDate);
				rekKoranList = getRekeningKoranDao().getAll(accountNumber,startDocDate,endDocDate);
			}else{
				String headerMessage = "Error";
				String bodyMessage = "End Date must be after Start Date. ";
				bodyMessage = bodyMessage.concat("Failed. ");
				FacesUtil.setErrorMessageDialog(headerMessage, bodyMessage);
			}
		}else{
			String headerMessage = "Error";
			String bodyMessage = "";
			if(startDocDate==null){bodyMessage = bodyMessage.concat("Start Date is empty. ");}
			if(endDocDate==null){bodyMessage = bodyMessage.concat("End Date is empty. ");}
			if(accountType==null||accountType.equals("")){bodyMessage = bodyMessage.concat("Account Type is empty. ");}
			bodyMessage = bodyMessage.concat("Failed. ");
			FacesUtil.setErrorMessageDialog(headerMessage, bodyMessage);
		}
	}

	public void downloadPdf(){
		if(rekKoranList.size()<=0){
			FacesUtil.setErrorMessageDialog("Empty", "No Transaction History Found");
		}else{
			SimpleDateFormat sdf= new SimpleDateFormat("yyyyMMdd");
			try{
				String fileName="";
				if(startDocDate!=null&endDocDate!=null){
					fileName=PubUtil.concatString("RekeningKoran",sdf.format(startDocDate),"-",sdf.format(endDocDate),".pdf");
				}else{
					fileName="RekeningKoran.pdf";
				}
				List listData= this.rekKoranList;
				String jasperPath = FacesUtil.getExternalContext().getRealPath("resources/report/RekeningKoran.jasper");
				getTotalDebitAndCredit();
				PubUtil.generateReport(fileName, jasperPath, Constants.PDF, getParameter(accountNumber,accountName, rekKoranList.get(0).getTotalAmount(), rekKoranList.get(rekKoranList.size()-1).getTotalAmount(),PubUtil.dateToString("yyyy-MM-dd",startDocDate),PubUtil.dateToString("yyyy-MM-dd", endDocDate),String.valueOf(totalCredit),String.valueOf(totalDebit)), Constants.CONTENT_TYPE_PDF, listData);
				
			}catch(Exception e){
				System.out.println(e.getMessage());
			}
		}

	}
	
	public void downloadXls(){
		if(rekKoranList.size()<=0){
			FacesUtil.setErrorMessageDialog("Empty", "No Transaction History Found");
		}else{
			SimpleDateFormat sdf= new SimpleDateFormat("yyyyMMdd");
			try {
				String fileName="";
				if(startDocDate!=null&endDocDate!=null){
					fileName=PubUtil.concatString("RekeningKoran",sdf.format(startDocDate),"-",sdf.format(endDocDate),".xls");
				}else{
					fileName="RekeningKoran.xls";
				}
				List listData=this.rekKoranList;
//				String jasperPath = FacesUtil.getExternalContext().getRealPath("resources/report/RekeningKoran.jasper");
				String jasperPath = FacesUtil.getExternalContext().getRealPath("resources/report/RekeningKoran_XLS.jasper");
				getTotalDebitAndCredit();
				PubUtil.generateReport(fileName, jasperPath, Constants.EXCEL, getParameter(accountNumber, accountName, rekKoranList.get(0).getTotalAmount(), rekKoranList.get(rekKoranList.size()-1).getTotalAmount(),PubUtil.dateToString("yyyy-MM-dd",startDocDate),PubUtil.dateToString("yyyy-MM-dd", endDocDate),String.valueOf(totalCredit),String.valueOf(totalDebit)), Constants.CONTENT_TYPE_XLS,listData);
	        } catch (Exception e) {
		        e.printStackTrace();
		        System.out.println(e.getMessage());
		    }
		}
	}

	
	public void getTotalDebitAndCredit(){
		totalCredit=0;
		totalDebit=0;
		for(SpanRekeningKoran srk:rekKoranList){
			try{
				totalDebit=totalDebit+Long.parseLong(srk.getDebit().replace(",", "").replace("-", ""));
			}catch(Exception e){
			}
			try{
				totalCredit=totalCredit+Long.parseLong(srk.getCredit().replace(",", "").replace("-", ""));
			}catch(Exception e){
			}
		}
		System.out.println(totalDebit);
		System.out.println(totalCredit);
	}
	
	private HashMap getParameter(String acctNo,String accName,String beginAmount,String lastAmount,String startDate,String endDate,String totalCredit,String totalDebit) {
		HashMap param = new HashMap();
		param.put("acctNo",acctNo);
		param.put("acctName", accName);
		param.put("beginingBalance", beginAmount);
		param.put("endingBalance",lastAmount);
		param.put("startDate", startDate);
		param.put("endDate", endDate);
		param.put("totalCredit", totalCredit);
		param.put("totalDebit",totalDebit);
		return param;
	}
	
	public void setApplicationBean(ApplicationBean applicationBean) {
		this.applicationBean = applicationBean;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public Date getStartDocDate() {
		return startDocDate;
	}

	public void setStartDocDate(Date startDocDate) {
		this.startDocDate = startDocDate;
	}

	public Date getEndDocDate() {
		return endDocDate;
	}

	public void setEndDocDate(Date endDocDate) {
		this.endDocDate = endDocDate;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public LazyDataModel < SpanRekeningKoran > getRekeningKoranList() {
		return rekeningKoranList;
	}

	public void setRekeningKoranList(LazyDataModel < SpanRekeningKoran > rekeningKoranList) {
		this.rekeningKoranList = rekeningKoranList;
	}

	public List<SpanRekeningKoran> getRekKoranList() {
		return rekKoranList;
	}

	public void setRekKoranList(List<SpanRekeningKoran> rekKoranList) {
		this.rekKoranList = rekKoranList;
	}

}
