package com.mii.beans;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.mii.constant.Constants;
import com.mii.dao.AccountHistoryDao;
import com.mii.dao.SystemParameterDao;
import com.mii.helpers.FacesUtil;
import com.mii.models.SpanAccountHistory;
import com.mii.models.SystemParameter;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.SessionBean;

@ManagedBean
@ViewScoped
public class AccountHistoryHandler implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private LazyDataModel < SpanAccountHistory > accountHistoryList;
	
	private Boolean doShowGAJI;
	private Boolean doShowGAJIR;
	
	private Date docDate;
	private String inquiryType;
	private String tabSelected;
	
	@ManagedProperty(value="#{applicationBean}")
	private ApplicationBean applicationBean;
	
	@ManagedProperty(value="#{sessionBean}")
	private SessionBean sessionBean;
	
	@PostConstruct
	public void init(){
		FacesUtil.authMenu(getSessionBean().getUser(), "accountHistory");
		doClickGaji();
	}
	
	public void doClickGaji(){
		doShowGAJI = true;
		doShowGAJIR = false;
		tabSelected = Constants.GAJI;
		accountHistoryList = null;
	}

	public void doClickGajiR(){
		doShowGAJI = false;
		doShowGAJIR = true;
		tabSelected = Constants.GAJIR;
		accountHistoryList = null;
	}
	
	public void doInquiry(){
		accountHistoryList = new DetailDataModel();
	}
	
	private class DetailDataModel extends LazyDataModel < SpanAccountHistory > {
		private static final long serialVersionUID = 1L;

		public DetailDataModel() {
		}

		@Override
		public List < SpanAccountHistory > load(int startingAt, int maxPerPage,
		String sortField, SortOrder sortOrder,
		Map < String, String > filters) {
			Integer jmlAll = 0;
			List<SpanAccountHistory> detailList = new ArrayList<SpanAccountHistory>();
			try{
				SystemParameter parameter = getSystemParameterDaoBean().getDetailedParameterByParamName(Constants.PARAM_DEBITACCNO+tabSelected);
				String accountNumber =	parameter.getParamValue();
				if(inquiryType.equals("0")){
					detailList = getSystemParameterDAOBean().showAccountHistoryGAJIMutasi(accountNumber, validateDate(docDate), startingAt+1, startingAt+maxPerPage);
					jmlAll = getSystemParameterDAOBean().countAccountHistoryGAJIMutasi(accountNumber, validateDate(docDate));
				}else{
					detailList = getSystemParameterDAOBean().showAccountHistoryGAJI(accountNumber, validateDate(docDate), inquiryType, startingAt+1, startingAt+maxPerPage);
					jmlAll = getSystemParameterDAOBean().countAccountHistoryGAJI(accountNumber, validateDate(docDate), inquiryType);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			setRowCount(jmlAll);
			setPageSize(maxPerPage);
			return detailList;
		}
	}
	
	public String validateDate(Date docDate){
		String iDate = "";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try{
			if(docDate!=null){
				iDate = sdf.format(docDate);
			}else{
				iDate = sdf.format(new Date());
			}
		}catch(Exception e){
			iDate = sdf.format(new Date());
		}
		return iDate;
	}
	
	private AccountHistoryDao getSystemParameterDAOBean(){
		return (AccountHistoryDao) applicationBean.getCorpGWContext().getBean("accountHistoryDao");
	}
	
	private SystemParameterDao getSystemParameterDaoBean(){
		return (SystemParameterDao) applicationBean.getCorpGWContext().getBean("systemParameterDAO");
	}

	public ApplicationBean getApplicationBean() {
		return applicationBean;
	}

	public void setApplicationBean(ApplicationBean applicationBean) {
		this.applicationBean = applicationBean;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public LazyDataModel < SpanAccountHistory > getAccountHistoryList() {
		return accountHistoryList;
	}

	public void setAccountHistoryList(LazyDataModel < SpanAccountHistory > accountHistoryList) {
		this.accountHistoryList = accountHistoryList;
	}

	public Date getDocDate() {
		return docDate;
	}

	public void setDocDate(Date docDate) {
		this.docDate = docDate;
	}

	public String getInquiryType() {
		return inquiryType;
	}

	public void setInquiryType(String inquiryType) {
		this.inquiryType = inquiryType;
	}

	public String getTabSelected() {
		return tabSelected;
	}

	public void setTabSelected(String tabSelected) {
		this.tabSelected = tabSelected;
	}

	public Boolean getDoShowGAJI() {
		return doShowGAJI;
	}

	public void setDoShowGAJI(Boolean doShowGAJI) {
		this.doShowGAJI = doShowGAJI;
	}

	public Boolean getDoShowGAJIR() {
		return doShowGAJIR;
	}

	public void setDoShowGAJIR(Boolean doShowGAJIR) {
		this.doShowGAJIR = doShowGAJIR;
	}
	
}
