package com.mii.beans;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import com.mii.dao.ReportLHPDao;
import com.mii.dao.ReportDetailLHPDao;
import com.mii.models.ReportLHPM;
import com.mii.models.ReportDetailLHPM;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.SessionBean;

@ManagedBean
@ViewScoped
public class ReportLHPHandler implements Serializable {

	private static final long serialVersionUID = 1L;
	@ManagedProperty(value="#{applicationBean}")
	private ApplicationBean applicationBean;
	@ManagedProperty(value="#{sessionBean}")
	private SessionBean sessionBean;
	private ReportLHPM transactionDetailM = new ReportLHPM();
	private List<ReportLHPM> listReport = new ArrayList<ReportLHPM>();
	private List<ReportLHPM> listAwal;
	private ReportDetailLHPM transactionDetail2M = new ReportDetailLHPM();
	private List<ReportDetailLHPM> listReportDetail = new ArrayList<ReportDetailLHPM>();
	private List<ReportDetailLHPM> listAwalDetail;
	private Date tglbuku;
	private Date tahunDate;
	private BigDecimal totalSetoran;
	private int totalTrx;
	private String noSakti;
	private String tglbukuS,tahunDateS,currencyS;
	private String currency;
	private String jmlSet,jmlTrx;
	private String nameFileLHPUSD,nameFileLHPIDR,letakFile,paramNamePF,kodeBank,paramNameKB;
	
	
	public ReportLHPHandler() {

		tglbukuS="TO_CHAR(SYSDATE, 'MMdd')";
		tahunDateS="TO_CHAR(SYSDATE, 'yyyy')";
		paramNamePF="";
		paramNameKB="";
		kodeBank="";

		Date date=new Date();
		tglbuku=date;
		tahunDate=date;
		nameFileLHPUSD="";
		nameFileLHPIDR="";
		currency="IDR";
	}


	public void getIsiReport(List<ReportLHPM> lisReportMs) {
		ReportLHPDao reportLHPDao = (ReportLHPDao) applicationBean.getCorpGWContext().getBean("reportLHPDao");
		List<ReportLHPM> lisDetailM = reportLHPDao.getReportLHP(tglbukuS, tahunDateS,currencyS);
		String string = "January 2, 2010"; 
		
		for (ReportLHPM transactionDetailM : lisDetailM) {
			Date date = null;
			try {
				if (transactionDetailM.getTanggalBuku() != null) {
					date = new SimpleDateFormat("MMddyyyy").parse(transactionDetailM.getTanggalBuku()+tahunDateS );
					transactionDetailM.setTglgabung(date);
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			lisReportMs.add(transactionDetailM);
		}
	}
	
	public void getIsiReportDetail(List<ReportDetailLHPM> lisReportDetailMs) {
		ReportDetailLHPDao reportDetailLHPDao = (ReportDetailLHPDao) applicationBean.getCorpGWContext().getBean("reportDetailLHPDao");
		List<ReportDetailLHPM> lisDetail2M = reportDetailLHPDao.getReportDetailLHP(tglbukuS, tahunDateS,currencyS);
		
		for (ReportDetailLHPM transactionDetail2M : lisDetail2M) {
			lisReportDetailMs.add(transactionDetail2M);
		}
	}

	public void generateFileLHP(){
		
		try{
			totalTrx = 0;
			paramNamePF="PATH_FILE_REPORT_RESULT";
			paramNameKB="BANK_CODE";
			listReport = new ArrayList<ReportLHPM>();
			listReportDetail = new ArrayList<ReportDetailLHPM>();
			listAwal = new ArrayList<ReportLHPM>(listReport);
			listAwalDetail = new ArrayList<ReportDetailLHPM>(listReportDetail);
			
			ReportLHPDao reportLHPDao = (ReportLHPDao) applicationBean.getCorpGWContext().getBean("reportLHPDao");
			letakFile = reportLHPDao.getPathFile(paramNamePF);
			kodeBank=reportLHPDao.getKodeBank(paramNameKB);
			nameFileLHPUSD=kodeBank+"LHPUSD";
			nameFileLHPIDR=kodeBank+"LHP";
			listReport = new ArrayList<ReportLHPM>();
			listReportDetail = new ArrayList<ReportDetailLHPM>();
			DateFormat sdFormat = new SimpleDateFormat("MMdd");
			DateFormat thnFormat = new SimpleDateFormat("yyyy");
			tglbukuS = sdFormat.format(tglbuku);
			tahunDateS = thnFormat.format(tglbuku);
			currencyS = currency;
			String tglBkNameFile =tglbukuS.substring(2)+tglbukuS.substring(0,2)+ tahunDateS.substring(2, 4);
			listAwal.clear();
			listAwalDetail.clear();
			getIsiReport(listAwal);
			getIsiReportDetail(listAwalDetail);
			FileWriter fw=null;
			if (currency.equals("USD")){
				//System.out.println("4");
//				 File file = new File("/data1/wmServer/SPAN/glassfish3/glassfish/domains/domainMPN/520008000990DNP"+tglbk+".txt");
				File file = new File(letakFile+nameFileLHPUSD+tglBkNameFile + ".txt");
				fw = new FileWriter(file);
			}else{
//				 File file = new File("/data1/wmServer/SPAN/glassfish3/glassfish/domains/domainMPN/520008000990DNP"+tglbk+".txt");
				File file = new File(letakFile+nameFileLHPIDR+ tglBkNameFile + ".txt");
				fw = new FileWriter(file);
			}
			for (int i = 0; i < listAwal.size(); i++) {
				if (listAwal.get(i).getTanggalBuku() != null) {
					if (listAwal.get(i).getTanggalBuku().equals(tglbukuS)) {
						listReport.add(listAwal.get(i));
						totalTrx++;
						fw.write("LAPORAN HARIAN PENERIMAAN"+"\r\n"+
								"Tanggal Buku"+"|"+tahunDateS+listAwal.get(i).getTanggalBuku()+"\r\n"+
//								"Kode Bank"+"|"+"520009000990"+"\n"+
								"Kode Bank"+"|"+listAwal.get(i).getKodeBank()+"\r\n"+
								"Nama Bank"+"|"+"Bank BTPN"+"\r\n"+
								"Nomor Rekening"+"|"+listAwal.get(i).getAccountNumber()+"\r\n"+
								"Mata Uang"+"|"+listAwal.get(i).getMataUang()+"\r\n"+
								"Jumlah Transaksi"+"|"+listAwal.get(i).getJumlahTrx()+"\r\n"+
								"Jumlah Penerimaan"+"|"+listAwal.get(i).getJumlahSetoran()+"\r\n"+
								"Pelimpahan Penerimaan"+"\r\n"+
								"Nomor Referensi Pelimpahan"+"|"+"Total Pelimpahan"+"\r\n");
					}
				}
			}
			
			for (int j = 0; j < listAwalDetail.size(); j++) {
				if (listAwalDetail.get(j).getNoSakti() != null) {
						listReportDetail.add(listAwalDetail.get(j));
						totalTrx++;
						fw.write(listAwalDetail.get(j).getNoSakti()+"|"+listAwalDetail.get(j).getJumlahSetoran()+"\r\n");
						
					}
				}
			
			fw.close();
			//System.out.println("DONE!");
		}catch (Exception e) {
			e.printStackTrace();
		}
		String nameOfFile="";
		String pathFile="";
		DateFormat sdFormat = new SimpleDateFormat("MMdd");
		DateFormat thnFormat = new SimpleDateFormat("yyyy");
		tglbukuS = sdFormat.format(tglbuku);
		tahunDateS = thnFormat.format(tahunDate);
		currencyS = currency;
		String tglBkNameFile2 =tglbukuS.substring(2)+tglbukuS.substring(0,2)+ tahunDateS.substring(2, 4);
		if (currency.equals("USD")){
			nameOfFile = nameFileLHPUSD + tglBkNameFile2 + ".txt";
	
//			String pathFile="/data1/wmServer/SPAN/glassfish3/glassfish/domains/domainMPN/";
			 pathFile = letakFile;
		}else{
			nameOfFile = nameFileLHPIDR + tglBkNameFile2 + ".txt";
//			String pathFile="/data1/wmServer/SPAN/glassfish3/glassfish/domains/domainMPN/";
			 pathFile = letakFile;
		}
		
		FacesContext fc = FacesContext.getCurrentInstance();
		HttpServletResponse response = (HttpServletResponse) fc
				.getExternalContext().getResponse();
		response.setContentType("application/octet-stream");
		response.setHeader("Content-Disposition", "attachment;filename="
				+ nameOfFile);
		try {
			FileInputStream in = new FileInputStream(new File(
					pathFile.concat(nameOfFile)));

			ServletOutputStream out = response.getOutputStream();
			byte[] buffer = new byte[2048];
			int length;
			while ((length = in.read(buffer)) > 0) {
				out.write(buffer, 0, length);
			}
			in.close();
			out.flush();
			out.close();
		//	//System.out.println("ukuran file" + in.read(buffer));
			fc.responseComplete();
		} catch (Exception e) {
			//System.out.println("masuk ke catch");
			e.printStackTrace();
		}
		
	}


	public List<ReportLHPM> getListReport() {
		return listReport;
	}

	public void setListReport(List<ReportLHPM> listReport) {
		this.listReport = listReport;
	}


	public ReportLHPM getTransactionDetailM() {
		return transactionDetailM;
	}

	public void setTransactionDetailM(ReportLHPM transactionDetailM) {
		this.transactionDetailM = transactionDetailM;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<ReportLHPM> getListAwal() {
		return listAwal;
	}

	public void setListAwal(List<ReportLHPM> listAwal) {
		this.listAwal = listAwal;
	}

	public BigDecimal getTotalSetoran() {
		return totalSetoran;
	}

	public void setTotalSetoran(BigDecimal totalSetoran) {
		this.totalSetoran = totalSetoran;
	}

	public int getTotalTrx() {
		return totalTrx;
	}

	public void setTotalTrx(int totalTrx) {
		this.totalTrx = totalTrx;
	}


	public String getNoSakti() {
		return noSakti;
	}

	public void setNoSakti(String noSakti) {
		this.noSakti = noSakti;
	}


	public String getJmlSet() {
		return jmlSet;
	}


	public void setJmlSet(String jmlSet) {
		this.jmlSet = jmlSet;
	}


	public String getJmlTrx() {
		return jmlTrx;
	}


	public void setJmlTrx(String jmlTrx) {
		this.jmlTrx = jmlTrx;
	}

	public String getTahunDateS() {
		return tahunDateS;
	}


	public void setTahunDateS(String tahunDateS) {
		this.tahunDateS = tahunDateS;
	}


	public Date getTglbuku() {
		return tglbuku;
	}


	public void setTglbuku(Date tglbuku) {
		this.tglbuku = tglbuku;
	}


	public Date getTahunDate() {
		return tahunDate;
	}


	public void setTahunDate(Date tahunDate) {
		this.tahunDate = tahunDate;
	}


	public String getTglbukuS() {
		return tglbukuS;
	}


	public void setTglbukuS(String tglbukuS) {
		this.tglbukuS = tglbukuS;
	}


	public String getCurrencyS() {
		return currencyS;
	}


	public void setCurrencyS(String currencyS) {
		this.currencyS = currencyS;
	}


	public String getCurrency() {
		return currency;
	}


	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public ApplicationBean getApplicationBean() {
		return applicationBean;
	}


	public void setApplicationBean(ApplicationBean applicationBean) {
		this.applicationBean = applicationBean;
	}


	public SessionBean getSessionBean() {
		return sessionBean;
	}


	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}


	public void handleDateSelect(ReportLHPHandler event){

	}
}
