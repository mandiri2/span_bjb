package com.mii.beans;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Rectangle;
import com.mii.constant.Constants;
import com.mii.dao.SpanDepkeuReportDao;
import com.mii.helpers.FacesUtil;
import com.mii.is.util.PubUtil;
import com.mii.models.SpanDepkeuReport;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.SessionBean;

@ManagedBean
@ViewScoped
public class DepkeuReportHandler implements Serializable {
	private static final long serialVersionUID = 1L;
	private LazyDataModel<SpanDepkeuReport> depkeuReportList;
	private List<SpanDepkeuReport> listDepkeuReport;
	private Date docDateStart;
	private Date docDateEnd;
	private String reportType;
	private int totalRow;
	private static final String docDateFormat = "yyyy-MM-dd";
	@ManagedProperty(value = "#{applicationBean}")
	private ApplicationBean applicationBean;
	@ManagedProperty(value = "#{sessionBean}")
	private SessionBean sessionBean;

	@PostConstruct
	public void init() {
		FacesUtil.authMenu(getSessionBean().getUser(), "depkeuReport");
		listDepkeuReport=new ArrayList<SpanDepkeuReport>();
	}

	public void doInquiryReport() {
		if(docDateStart!=null&docDateEnd!=null&reportType!=null&!reportType.equals("")){
			if(!docDateEnd.before(docDateStart)){
				depkeuReportList = new DetailDataModel();
			}else{
				String headerMessage = "Error";
				String bodyMessage = "End Date must be after Start Date. ";
				bodyMessage = bodyMessage.concat("Failed. ");
				FacesUtil.setErrorMessageDialog(headerMessage, bodyMessage);
			}
		}else{
			String headerMessage = "Error";
			String bodyMessage = "";
			if(docDateStart==null){bodyMessage = bodyMessage.concat("Start Date is empty. ");}
			if(docDateEnd==null){bodyMessage = bodyMessage.concat("End Date is empty. ");}
			if(reportType==null||reportType.equals("")){bodyMessage = bodyMessage.concat("Report Type is empty. ");}
			bodyMessage = bodyMessage.concat("Failed. ");
			FacesUtil.setErrorMessageDialog(headerMessage, bodyMessage);
		}
	}

	private class DetailDataModel extends LazyDataModel<SpanDepkeuReport> {
		private static final long serialVersionUID = 1L;

		public DetailDataModel() {
		}

		@Override
		public List<SpanDepkeuReport> load(int startingAt, int maxPerPage,
				String sortField, SortOrder sortOrder,
				Map<String, String> filters) {
			List<SpanDepkeuReport> detailList = new ArrayList<SpanDepkeuReport>();
//			totalRow = getSpanDepkeuReportDAOBean().countSp2dReject(PubUtil.getStringDate(docDate, docDateFormat));
			switch (reportType) {
			case "0":
				detailList = getSpanDepkeuReportDAOBean()
				.getSp2dReject(PubUtil.getStringDate(docDateStart, docDateFormat),PubUtil.getStringDate(docDateEnd, docDateFormat),startingAt, startingAt + maxPerPage);
				
				totalRow = getSpanDepkeuReportDAOBean().countSp2dReject(PubUtil.getStringDate(docDateStart, docDateFormat),PubUtil.getStringDate(docDateEnd, docDateFormat));
				break;
			case "1":
				detailList = getSpanDepkeuReportDAOBean()
				.getSp2dRejectApprovedByKemenkeu(
						PubUtil.getStringDate(docDateStart, docDateFormat),PubUtil.getStringDate(docDateEnd, docDateFormat),
						startingAt, startingAt + maxPerPage);
				
				totalRow = getSpanDepkeuReportDAOBean().countSp2dRejectApprovedByKemenkeu(PubUtil.getStringDate(docDateStart, docDateFormat),PubUtil.getStringDate(docDateEnd, docDateFormat));
				break;
			default:
				System.out.println("reportType Is Not Choosen");
				totalRow = 0;
				break;
			}
			setRowCount(totalRow);
			setPageSize(maxPerPage);
			return detailList;
		}
	}

	public void downloadPdf() {
		try {
			if(docDateStart==null){
				FacesUtil.setInfoMessage(null, "Please Insert Date Of Report", "Depkeu Report");
			}else{
				String fileName = "DepkeuReport.pdf";
				String jasperPath = FacesUtil.getExternalContext().getRealPath(
						"resources/report/DepkeuReport.jasper");
				switch (reportType) {
				case "0":
					listDepkeuReport = getSpanDepkeuReportDAOBean().getSp2dReject(
							PubUtil.getStringDate(docDateStart, docDateFormat),PubUtil.getStringDate(docDateEnd, docDateFormat), 0,
							totalRow);
					break;
				case "1":
					listDepkeuReport = getSpanDepkeuReportDAOBean().getSp2dRejectApprovedByKemenkeu(
							PubUtil.getStringDate(docDateStart, docDateFormat),PubUtil.getStringDate(docDateEnd, docDateFormat),0, totalRow);
					break;
				default:
					System.out.println("reportType Is Not Choosen");
					totalRow = 0;
					break;
				}
				PubUtil.generateReport(fileName, jasperPath,Constants.PDF, getParameter(), Constants.CONTENT_TYPE_PDF, listDepkeuReport);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void downloadXls() {
		try {
			if(docDateStart==null){
				FacesUtil.setInfoMessage(null, "Please Insert Date Of Report", "Depkeu Report");
			}else{
				String fileName = "DepkeuReport.xls";
				String jasperPath = FacesUtil.getExternalContext().getRealPath(
						"resources/report/DepkeuReport_XLS.jasper");
				switch (reportType) {
				case "0":
					listDepkeuReport = getSpanDepkeuReportDAOBean().getSp2dReject(
							PubUtil.getStringDate(docDateStart, docDateFormat),PubUtil.getStringDate(docDateEnd, docDateFormat), 0,
							totalRow);
					break;
				case "1":
					listDepkeuReport = getSpanDepkeuReportDAOBean().getSp2dRejectApprovedByKemenkeu(
							PubUtil.getStringDate(docDateStart, docDateFormat),PubUtil.getStringDate(docDateEnd, docDateFormat),0, totalRow);
					break;
				default:
					System.out.println("reportType Is Not Choosen");
					totalRow = 0;
					break;
				}
				PubUtil.generateReport(fileName, jasperPath, Constants.EXCEL, getParameter(), Constants.CONTENT_TYPE_XLS, listDepkeuReport);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public HashMap getParameter(){
		HashMap map=new HashMap();
		switch (reportType) {
		case "0":
		map.put("REPORT_HEADER", "Laporan SP2D Reject Sebelum di Approve");
		break;
		case "1":
		map.put("REPORT_HEADER", "Laporan SP2D Reject Setelah di Approve");
		break;
		default:
		map.put("REPORT_HEADER", "Laporan SP2D Reject");
		break;
		}
		return map;
	}

	private SpanDepkeuReportDao getSpanDepkeuReportDAOBean() {
		return (SpanDepkeuReportDao) applicationBean.getCorpGWContext()
				.getBean("spanDepkeuReportDao");
	}

	public ApplicationBean getApplicationBean() {
		return applicationBean;
	}

	public void setApplicationBean(ApplicationBean applicationBean) {
		this.applicationBean = applicationBean;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public LazyDataModel<SpanDepkeuReport> getDepkeuReportList() {
		return depkeuReportList;
	}

	public void setDepkeuReportList(
			LazyDataModel<SpanDepkeuReport> depkeuReportList) {
		this.depkeuReportList = depkeuReportList;
	}

	public String getReportType() {
		return reportType;
	}

	public void setReportType(String reportType) {
		this.reportType = reportType;
	}

	public List<SpanDepkeuReport> getListDepkeuReport() {
		return listDepkeuReport;
	}

	public void setListDepkeuReport(List<SpanDepkeuReport> listDepkeuReport) {
		this.listDepkeuReport = listDepkeuReport;
	}

	public Date getDocDateStart() {
		return docDateStart;
	}

	public void setDocDateStart(Date docDateStart) {
		this.docDateStart = docDateStart;
	}

	public Date getDocDateEnd() {
		return docDateEnd;
	}

	public void setDocDateEnd(Date docDateEnd) {
		this.docDateEnd = docDateEnd;
	}

}
