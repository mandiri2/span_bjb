package com.mii.beans;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.mii.dao.ReportTransaksiDao;
import com.mii.helpers.CurrencyUtils;
import com.mii.helpers.FacesUtil;
import com.mii.helpers.HelperUtils;
import com.mii.models.ReportTransaksiM;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.SessionBean;
import com.lowagie.text.BadElementException;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Rectangle;



@ManagedBean
@ViewScoped
public class ReportTransaksiHandler implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private List<ReportTransaksiM> filteredTransaction;
	private ReportTransaksiM transactionDetail2M = new ReportTransaksiM();
	private ReportTransaksiM transactionDetailM = new ReportTransaksiM();
	private List<ReportTransaksiM> listReport = new ArrayList<ReportTransaksiM>();
	private List<ReportTransaksiM> listAwal;
	private List<ReportTransaksiM> listReportBranch = new ArrayList<ReportTransaksiM>();
	private List<ReportTransaksiM> listAwalBranch;
	private List<String> listSelectCabang = new ArrayList<String>();
	
	private Date startDate;
	private Date endDate;
	private Date startDateBuku;
	private Date endDateBuku;
	private String typePembayaran, reversalStatus;
	private BigDecimal totalSetoran;
	private int totalTrx;
	private String tglbk;
	private String mataUang;
	private String startDateS,endDateS,startDateBukuS,endDateBukuS,ntpn,ntb,kodeBilling,status,uidOpr,kodeCabang,namaCabang;
	private String jmlSet,jmlTrx;
	private String userid,userIdS,branchS,kantorPusat,descKP;
	
	@ManagedProperty(value="#{sessionBean}")
	private SessionBean sessionBean;
	@ManagedProperty(value="#{applicationBean}")
	private ApplicationBean applicationBean;
	private boolean doCabang;
	
	public void handleDateSelect(ReportTransaksiHandler event){
	}
	
	 @PostConstruct
	    public void init() {
		FacesUtil.authMenu(sessionBean.getUser(), "reportTransaksi");

		 descKP="KANTOR_PUSAT";
		 userIdS =sessionBean.getUser().getUserID();
			ReportTransaksiDao reportTransaksiDao = (ReportTransaksiDao) applicationBean.getCorpGWContext().getBean("reportTransaksiDao");
		
			branchS = reportTransaksiDao.getBranchByUserId(userIdS);
			kantorPusat = reportTransaksiDao.getKantorPusat(descKP);
			doCabang = false;
			if (branchS.equals(kantorPusat)) {
				doCabang = true;
			} else {
				doCabang = false;
			}
			
			listReportBranch = new ArrayList<ReportTransaksiM>();
			listAwalBranch = new ArrayList<ReportTransaksiM>(listReportBranch);
			getSelectItem(listAwalBranch);
			for(int j=0;j<listAwalBranch.size();j++){
				listSelectCabang.add(listAwalBranch.get(j).getNamaCabang());
				namaCabang="";
			}
			ntb 		= "";
			kodeBilling = "";
			uidOpr 		= "";
			kodeCabang 	= "";
			namaCabang 	= "";
			status 		= "All";
			typePembayaran = "All";
			reversalStatus = "All";
			if (branchS.equals(kantorPusat)) {
				kodeCabang = "";
			} else {
				kodeCabang = branchS;
			}
			filterTransaction();
		}
	
	
	public ReportTransaksiHandler() {
		startDateS = "TO_CHAR(SYSDATE, 'yyyyMMdd')";
		endDateS ="TO_CHAR(SYSDATE, 'yyyyMMdd')";
		startDateBukuS = "TO_CHAR(SYSDATE, 'yyyyMMdd')";
		endDateBukuS ="TO_CHAR(SYSDATE, 'yyyyMMdd')";
		Date date=new Date();
		startDate=date;
		endDate=date;
		startDateBuku=date;
		endDateBuku=date;
	}
	
		
	public void getAllTransaksi(List<ReportTransaksiM> lisReportMs) {
		ReportTransaksiDao reportTransaksiDao = (ReportTransaksiDao) applicationBean.getCorpGWContext().getBean("reportTransaksiDao");
		List<ReportTransaksiM> lisDetailM = new ArrayList<ReportTransaksiM>();

		if (status.equals("All")){
			lisDetailM = reportTransaksiDao.getReportTransaksi(startDateS,endDateS,startDateBukuS,endDateBukuS, typePembayaran,reversalStatus,ntpn,ntb,kodeBilling,status,uidOpr,kodeCabang,namaCabang);
		}else if (status.equals("NTPN")){
			lisDetailM = reportTransaksiDao.getReportTransaksiNtpn(startDateS,endDateS,startDateBukuS,endDateBukuS, typePembayaran,reversalStatus, ntpn,ntb,kodeBilling,status,uidOpr,kodeCabang,namaCabang);
		}else if (status.equals("Tanpa NTPN")){
			lisDetailM = reportTransaksiDao.getReportTransaksiNoNtpn(startDateS,endDateS,startDateBukuS,endDateBukuS,typePembayaran,reversalStatus, ntb,kodeBilling,status,uidOpr,kodeCabang,namaCabang);
		}
		
		for (ReportTransaksiM transactionDetailM : lisDetailM) {
			Date date = null;
			try {
				if (transactionDetailM.getTglTerimaBayar() != null) {
					date = new SimpleDateFormat("yyyyMMdd").parse(transactionDetailM.getTglTerimaBayar());
					transactionDetailM.setTglgabung(date);
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			lisReportMs.add(transactionDetailM);
		}
	}

	
	public void getSelectItem(List<ReportTransaksiM> lisReportDetailMs) {
		ReportTransaksiDao reportTransaksiDao = (ReportTransaksiDao) applicationBean.getCorpGWContext().getBean("reportTransaksiDao");
		List<ReportTransaksiM> lisDetail2M = reportTransaksiDao.getSelectItem();
		
		for (ReportTransaksiM transactionDetail2M : lisDetail2M) {
			lisReportDetailMs.add(transactionDetail2M);
		}
	}
	
		
	public void filterTransaction() {		

		listReport = new ArrayList<ReportTransaksiM>();
		listReportBranch = new ArrayList<ReportTransaksiM>();	
		listAwal = new ArrayList<ReportTransaksiM>(listReport);
		listAwalBranch = new ArrayList<ReportTransaksiM>(listReportBranch);
		totalSetoran = new BigDecimal(0);
		
		listReport = new ArrayList<ReportTransaksiM>();
		DateFormat idFormat = new SimpleDateFormat("yyyyMMdd");
		
		startDateS = idFormat.format(startDate);
		endDateS = idFormat.format(endDate);
		startDateBukuS = idFormat.format(startDateBuku);
		endDateBukuS = idFormat.format(endDateBuku);
		listAwal.clear();
		getAllTransaksi(listReport);
		
		for(int i=0;i<listReport.size();i++){
			if (listReport.get(i).getJumlahBayar() != null){
				totalSetoran = totalSetoran.add(new BigDecimal(listReport.get(i).getJumlahBayar()));			
			}
			
		}
//		DecimalFormat myFormatter = new DecimalFormat("###,###");
////		String output = myFormatter.format(totalSetoran);
////		totalSetoran = myFormatter.format(totalSetoran);
//		totalSetoran = NumberFormat.getCurrencyInstance().format(totalSetoran);
	}
	
	public void preProcessPDF(Object document) throws IOException,  
    BadElementException, DocumentException{
        Document pdf = (Document) document;
        Rectangle r = new Rectangle(2000f, 1000f);
        pdf.setPageSize(r);
        pdf.setMargins(0f, 0f, 100f, 100f);
    }

		
	
	public List<ReportTransaksiM> getFilteredTransaction() {
		return filteredTransaction;
	}

	public void setFilteredTransaction(List<ReportTransaksiM> filteredTransaction) {
		this.filteredTransaction = filteredTransaction;
	}

	public ReportTransaksiM getTransactionDetailM() {
		return transactionDetailM;
	}

	public void setTransactionDetailM(ReportTransaksiM transactionDetailM) {
		this.transactionDetailM = transactionDetailM;
	}

	public List<ReportTransaksiM> getListReportRev() {
		return listReport;
	}

	public void setListReportRev(List<ReportTransaksiM> listReportRev) {
		this.listReport = listReportRev;
	}

	public List<ReportTransaksiM> getListAwal() {
		return listAwal;
	}

	public void setListAwal(List<ReportTransaksiM> listAwal) {
		this.listAwal = listAwal;
	}


	public BigDecimal getTotalSetoran() {
		return totalSetoran;
	}

	public void setTotalSetoran(BigDecimal totalSetoran) {
		this.totalSetoran = totalSetoran;
	}

	public int getTotalTrx() {
		return totalTrx;
	}

	public void setTotalTrx(int totalTrx) {
		this.totalTrx = totalTrx;
	}

	public String getJmlSet() {
		return jmlSet;
	}

	public void setJmlSet(String jmlSet) {
		this.jmlSet = jmlSet;
	}

	public String getJmlTrx() {
		return jmlTrx;
	}

	public void setJmlTrx(String jmlTrx) {
		this.jmlTrx = jmlTrx;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getStartDateS() {
		return startDateS;
	}

	public void setStartDateS(String startDateS) {
		this.startDateS = startDateS;
	}

	public String getEndDateS() {
		return endDateS;
	}

	public void setEndDateS(String endDateS) {
		this.endDateS = endDateS;
	}

	public Date getStartDateBuku() {
		return startDateBuku;
	}

	public void setStartDateBuku(Date startDateBuku) {
		this.startDateBuku = startDateBuku;
	}

	public Date getEndDateBuku() {
		return endDateBuku;
	}

	public void setEndDateBuku(Date endDateBuku) {
		this.endDateBuku = endDateBuku;
	}

	public String getStartDateBukuS() {
		return startDateBukuS;
	}

	public void setStartDateBukuS(String startDateBukuS) {
		this.startDateBukuS = startDateBukuS;
	}

	public String getEndDateBukuS() {
		return endDateBukuS;
	}

	public void setEndDateBukuS(String endDateBukuS) {
		this.endDateBukuS = endDateBukuS;
	}
	
	public String getTypePembayaran() {
		return typePembayaran;
	}

	public void setTypePembayaran(String typePembayaran) {
		this.typePembayaran = typePembayaran;
	}

	public String getReversalStatus() {
		return reversalStatus;
	}

	public void setReversalStatus(String reversalStatus) {
		this.reversalStatus = reversalStatus;
	}

	public String getMataUang() {
		return mataUang;
	}

	public void setMataUang(String mataUang) {
		this.mataUang = mataUang;
	}

	public List<ReportTransaksiM> getListReport() {
		return listReport;
	}

	public void setListReport(List<ReportTransaksiM> listReport) {
		this.listReport = listReport;
	}

	public String getTglbk() {
		return tglbk;
	}

	public void setTglbk(String tglbk) {
		this.tglbk = tglbk;
	}

	public String getNtpn() {
		return ntpn;
	}

	public void setNtpn(String ntpn) {
		this.ntpn = ntpn;
	}

	public String getNtb() {
		return ntb;
	}

	public void setNtb(String ntb) {
		this.ntb = ntb;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUidOpr() {
		return uidOpr;
	}

	public void setUidOpr(String uidOpr) {
		this.uidOpr = uidOpr;
	}

	public String getKodeCabang() {
		return kodeCabang;
	}

	public void setKodeCabang(String kodeCabang) {
		this.kodeCabang = kodeCabang;
	}

	public String getNamaCabang() {
		return namaCabang;
	}

	public void setNamaCabang(String namaCabang) {
		this.namaCabang = namaCabang;
	}

	public String getKodeBilling() {
		return kodeBilling;
	}

	public void setKodeBilling(String kodeBilling) {
		this.kodeBilling = kodeBilling;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public ApplicationBean getApplicationBean() {
		return applicationBean;
	}

	public void setApplicationBean(ApplicationBean applicationBean) {
		this.applicationBean = applicationBean;
	}

	public List<ReportTransaksiM> getListReportBranch() {
		return listReportBranch;
	}

	public void setListReportBranch(List<ReportTransaksiM> listReportBranch) {
		this.listReportBranch = listReportBranch;
	}

	public List<ReportTransaksiM> getListAwalBranch() {
		return listAwalBranch;
	}

	public void setListAwalBranch(List<ReportTransaksiM> listAwalBranch) {
		this.listAwalBranch = listAwalBranch;
	}

	public String getUserIdS() {
		return userIdS;
	}

	public void setUserIdS(String userIdS) {
		this.userIdS = userIdS;
	}

	public String getBranchS() {
		return branchS;
	}

	public void setBranchS(String branchS) {
		this.branchS = branchS;
	}

	public List<String> getListSelectCabang() {
		return listSelectCabang;
	}

	public void setListSelectCabang(List<String> listSelectCabang) {
		this.listSelectCabang = listSelectCabang;
	}

	public boolean isDoCabang() {
		return doCabang;
	}

	public void setDoCabang(boolean doCabang) {
		this.doCabang = doCabang;
	}

	public String getKantorPusat() {
		return kantorPusat;
	}

	public void setKantorPusat(String kantorPusat) {
		this.kantorPusat = kantorPusat;
	}

	public String getDescKP() {
		return descKP;
	}

	public void setDescKP(String descKP) {
		this.descKP = descKP;
	}
	
}
