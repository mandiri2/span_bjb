package com.mii.beans;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipOutputStream;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import com.mii.dao.ReportDNPDao;
import com.mii.dao.ReportRekKoranDao;
import com.mii.helpers.CreateZIPUtil;
import com.mii.helpers.FacesUtil;
import com.mii.models.ReportRekKoranM;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.SessionBean;

@ManagedBean
@ViewScoped
public class ReportRekKoranHandler implements Serializable {

	private static final long serialVersionUID = 1L;
	@ManagedProperty(value="#{applicationBean}")
	private ApplicationBean applicationBean;
	@ManagedProperty(value="#{sessionBean}")
	private SessionBean sessionBean;
	private ReportRekKoranM transactionDetailM = new ReportRekKoranM();
	private List<ReportRekKoranM> listReport = new ArrayList<ReportRekKoranM>();
	private List<ReportRekKoranM> listAwal;
	private List<String> listSelectBSname = new ArrayList<String>();

	private Date tglInput;
	private String currency,dateBS,paramNamePF;
	
	 @PostConstruct
	 public void init() {
		FacesUtil.authMenu(sessionBean.getUser(), "reportRekKoran");
	 }

	public void handleDateSelect(ReportDNPHandler event){

	}


	public ReportRekKoranHandler() {
		
		Date date=new Date();
		tglInput=date;
		paramNamePF="";
		
	}


	public void getFileNameBS(List<ReportRekKoranM> lisReportMs) {
		ReportRekKoranDao reportRekKoranDao = (ReportRekKoranDao) applicationBean.getCorpGWContext().getBean("reportRekKoranDao");
		List<ReportRekKoranM> lisDetailM = reportRekKoranDao.getFilenameBS(dateBS,currency);		
		
		for (ReportRekKoranM transactionDetailM : lisDetailM) {
			Date date = null;
			lisReportMs.add(transactionDetailM);
		}
	}
	

	public void downloadFile(){
		String nameOfFile="";
		String pathFile="";
		paramNamePF="JAR_BANK_STATEMENT_PATH";
		
		DateFormat dateBsFormat = new SimpleDateFormat("yyyyMMdd");
		dateBS = dateBsFormat.format(tglInput);
		listReport = new ArrayList<ReportRekKoranM>();
		listAwal = new ArrayList<ReportRekKoranM>(listReport);
		
		listAwal.clear();
		getFileNameBS(listAwal);
		ReportRekKoranDao reportRekKoranDao = (ReportRekKoranDao) applicationBean.getCorpGWContext().getBean("reportRekKoranDao");
		pathFile = reportRekKoranDao.getPathFile(paramNamePF);

		for(int j=0;j<listAwal.size();j++){
			listSelectBSname.add(pathFile+listAwal.get(j).getBsFileName());
		}
		
		String namaFileZip="BankStatement.zip";
		String fileNameList[] = new String[listSelectBSname.size()];
		
		int j = 0;
		for (String fileName : listSelectBSname){
			fileNameList[j] = fileName;
			j++;
		}
		
		String downloadFileName = pathFile+namaFileZip;
		File fileList[] = new File[listSelectBSname.size()] ;

		j = 0;

		for (String fileName : listSelectBSname){
			fileList[j] = new File(fileName);
		}

		String Status= "";
		String  ErrorMessage= "Error";
		
		if (listAwal.size()>1)
		{
			
			for (int i = 0; i < fileNameList.length; i++){
				File files = new File(fileNameList[i]);
				fileList[i] = files;
			}
			try{
			   File zipFileName = new File(downloadFileName);
					
			   ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFileName));
			   CreateZIPUtil zip = new CreateZIPUtil();
			   zip.zipFiles(fileList, out);
			   Status = "SUCCESS";
			   ErrorMessage = "Success to zip file";
			   out.close();
			}catch(Exception e){
			   Status = "FAILED";
			   ErrorMessage = e.getMessage();
			   e.printStackTrace();
			}
			//System.out.println(ErrorMessage);
			nameOfFile = namaFileZip;
		}else{
//			ReportRekKoranDao reportRekKoranDao = (ReportRekKoranDao) applicationBean.getCorpGWContext().getBean("reportRekKoranDao");		
			nameOfFile = reportRekKoranDao.getFilenameBSString( dateBS, currency);
			
		}
	
		
		FacesContext fc = FacesContext.getCurrentInstance();
		HttpServletResponse response = (HttpServletResponse) fc
				.getExternalContext().getResponse();
		response.setContentType("application/octet-stream");
		response.setHeader("Content-Disposition", "attachment;filename="
				+ nameOfFile);
		try {
			FileInputStream in = new FileInputStream(new File(
					pathFile.concat(nameOfFile)));

			ServletOutputStream out = response.getOutputStream();
			byte[] buffer = new byte[2048];
			int length;
			while ((length = in.read(buffer)) > 0) {
				out.write(buffer, 0, length);
			}
			in.close();
			out.flush();
			out.close();
			fc.responseComplete();
		} catch (Exception e) {
			//System.out.println("masuk ke catch");
			e.printStackTrace();
		}
		
	}
	


	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public ReportRekKoranM getTransactionDetailM() {
		return transactionDetailM;
	}


	public void setTransactionDetailM(ReportRekKoranM transactionDetailM) {
		this.transactionDetailM = transactionDetailM;
	}


	public List<ReportRekKoranM> getListReport() {
		return listReport;
	}


	public void setListReport(List<ReportRekKoranM> listReport) {
		this.listReport = listReport;
	}


	public List<ReportRekKoranM> getListAwal() {
		return listAwal;
	}


	public void setListAwal(List<ReportRekKoranM> listAwal) {
		this.listAwal = listAwal;
	}


	public String getCurrency() {
		return currency;
	}


	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public ApplicationBean getApplicationBean() {
		return applicationBean;
	}


	public void setApplicationBean(ApplicationBean applicationBean) {
		this.applicationBean = applicationBean;
	}


	public SessionBean getSessionBean() {
		return sessionBean;
	}


	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}


	public Date getTglInput() {
		return tglInput;
	}


	public void setTglInput(Date tglInput) {
		this.tglInput = tglInput;
	}


	public String getDateBS() {
		return dateBS;
	}


	public void setDateBS(String dateBS) {
		this.dateBS = dateBS;
	}


	public List<String> getListSelectBSname() {
		return listSelectBSname;
	}


	public void setListSelectBSname(List<String> listSelectBSname) {
		this.listSelectBSname = listSelectBSname;
	}

	
	
}
