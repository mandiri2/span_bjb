package com.mii.beans;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.xml.rpc.ServiceException;

import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.mii.constant.Constants;
import com.mii.dao.ReportDetailLHPDao;
import com.mii.dao.ReportTransaksiDao;
import com.mii.dao.RtgsDao;
import com.mii.dao.SystemParameterDao;
import com.mii.dao.UserDao;
import com.mii.helpers.FacesUtil;
import com.mii.helpers.HelperUtils;
import com.mii.models.Approval;
import com.mii.models.ReportDetailLHPM;
import com.mii.models.ReportTransaksiM;
import com.mii.models.Rtgs;
import com.mii.models.SystemParameter;
import com.mii.models.User;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.SessionBean;
import com.mii.ws.WebServiceConfigurator;
import com.mii.ws.WebServiceRtgsExecutor;

@ManagedBean
@ViewScoped
public class LogRtgsHandler implements Serializable {
	private static final long serialVersionUID = 1L;
	private Rtgs rtgs;
	private LazyDataModel<Rtgs> rtgsList;
	
	@ManagedProperty(value="#{applicationBean}")
	private ApplicationBean applicationBean;
	@ManagedProperty(value="#{sessionBean}")
	private SessionBean sessionBean;
	
	private String transactionId = "";
	private Date   transactionDate = null;
	private Date   rtgsTransactionDate = new Date();
	private String referenceNumber = "";
	private String state = "";
	private String bppxName = "";
	private String response = ""; 
	private String responseDesc = "";
	private String refNo;
	private boolean doShowSearch = false;
	
	private List<Rtgs> listReport = new ArrayList<Rtgs>();
//	private List<Rtgs> listAwal;
	
	@PostConstruct
	public void init(){
		FacesUtil.authMenu(sessionBean.getUser(), "logRtgs");

		rtgsList = new RtgsDataModel();	
	}
	
	private SystemParameterDao getSystemParameterDAOBean(){
		return (SystemParameterDao) applicationBean.getCorpGWContext().getBean("systemParameterDAO");
	}

	public Rtgs getRtgs() {
		return rtgs;
	}

	public void setRtgs(Rtgs rtgs) {
		this.rtgs = rtgs;
	}

	public LazyDataModel<Rtgs> getRtgsList() {
		return rtgsList;
	}

	public void setRtgsList(LazyDataModel<Rtgs> rtgsList) {
		this.rtgsList = rtgsList;
	}

	public ApplicationBean getApplicationBean() {
		return applicationBean;
	}

	public void setApplicationBean(ApplicationBean applicationBean) {
		this.applicationBean = applicationBean;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}
	
	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}
	
	public Date getRtgsTransactionDate() {
		return rtgsTransactionDate;
	}

	public void setRtgsTransactionDate(Date rtgsTransactionDate) {
		this.rtgsTransactionDate = rtgsTransactionDate;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}
	

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getBppxName() {
		return bppxName;
	}

	public void setBppxName(String bppxName) {
		this.bppxName = bppxName;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getResponseDesc() {
		return responseDesc;
	}

	public void setResponseDesc(String responseDesc) {
		this.responseDesc = responseDesc;
	}
	
	public boolean isDoShowSearch() {
		return doShowSearch;
	}

	public void setDoShowSearch(boolean doShowSearch) {
		this.doShowSearch = doShowSearch;
	}

	public void showSearch(){
		doShowSearch = true;
	}
	
	public void hideSearch(){
		doShowSearch = false;
	}
	
	public void doSearch(){

		FacesUtil.resetPage("logRtgsForm:logRtgsTable");
		rtgsList = new RtgsDataModel();
		
	}
	
	public void viewDetailedRtgs(Rtgs rtgs){
		RtgsDao rtgsDao = (RtgsDao) applicationBean.getCorpGWContext().getBean("rtgsDAO");
		
		this.rtgs = rtgsDao.getDetailedRtgs(rtgs.getTransactionId());
		
		RequestContext.getCurrentInstance().execute(
					"viewRtgs.show()");
		
	}
	
	public void getIsiList(List<Rtgs> lisReportDetailMs) {
		RtgsDao rtgsDao = (RtgsDao) applicationBean.getCorpGWContext().getBean("rtgsDAO");
		DateFormat df = new SimpleDateFormat("yyyyMMdd");
		String td ="";
		td = df.format(rtgsTransactionDate);

		List<Rtgs> lisDetailM = rtgsDao.getRefNo(td);
		
		for (Rtgs transactionDetailM : lisDetailM) {
			lisReportDetailMs.add(transactionDetailM);
		}
	}

	public void getRefNo(){
	
		
		listReport = new ArrayList<Rtgs>();
		listReport.clear();
		getIsiList(listReport);
		refNo = "";
		for (int i = 0; i < listReport.size(); i++) {

			if (listReport.get(i).getReferenceNumber()!= null || (!listReport.get(i).getReferenceNumber().equals(""))){
				refNo = listReport.get(i).getReferenceNumber();
			}
		}


	
		if ((refNo != null) && (!refNo.equals(""))){
			RequestContext.getCurrentInstance().execute("dlgConfirmAgain.show()");
		}else{
			executeRtgs();
		}
	}
	
	public void executeRtgs(){
		try {
			DateFormat df = new SimpleDateFormat("yyyyMMdd");
			String td = "";
			if ((rtgsTransactionDate != null) && (!rtgsTransactionDate.equals(""))){
				td = df.format(rtgsTransactionDate);
			 
				String status = WebServiceRtgsExecutor.executeRTGS(td);
				String headerMessage = "Execute Rtgs Finished";
				String bodyMessage = status;
	
				FacesUtil.setInfoMessageDialog(headerMessage, bodyMessage);
			}else{
				String headerMessage = "Rtgs Warning";
				String bodyMessage = "Transaction date cannot be empty.";
				
				//System.out.println("e.getMessage() --> " + e.getMessage());

				FacesUtil.setWarningMessageDialog(headerMessage, bodyMessage);
			}
	
		} catch (Exception e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();

			String headerMessage = "Execute Rtgs Failed";
			String bodyMessage = "Please contact the administrator.";
			
			//System.out.println("e.getMessage() --> " + e.getMessage());

			FacesUtil.setErrorMessageDialog(headerMessage, bodyMessage);
		} 
	}

	private class RtgsDataModel extends LazyDataModel<Rtgs>{
		private static final long serialVersionUID = 1L;
		public RtgsDataModel() {
			
		}
		@Override
		public List<Rtgs> load(int startingAt, 
				int maxPerPage, String sortField, 
				SortOrder sortOrder, Map<String, String> filters) {
			
			RtgsDao rtgsDao = (RtgsDao) applicationBean.getCorpGWContext().getBean("rtgsDAO");
			List<Rtgs> listRtgs;
			
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			String td = "";
			if ((transactionDate != null) && (!transactionDate.equals("")))
				td = df.format(transactionDate);
			
			listRtgs = rtgsDao.getRtgsLazyLoadingByCategory(maxPerPage+startingAt, startingAt, transactionId, td, bppxName, state, referenceNumber, response, responseDesc);
			Integer jmlAll = rtgsDao.countRtgsByCategory(transactionId, td, bppxName, state, referenceNumber, response, responseDesc);
			setRowCount(jmlAll);
			setPageSize(maxPerPage);

			return listRtgs;
		}
	}



	public List<Rtgs> getListReport() {
		return listReport;
	}

	public void setListReport(List<Rtgs> listReport) {
		this.listReport = listReport;
	}

	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}

}
