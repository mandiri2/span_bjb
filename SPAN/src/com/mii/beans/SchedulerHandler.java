package com.mii.beans;

import java.io.IOException;
import java.io.Serializable;
//import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
//import javax.xml.rpc.ServiceException;



import org.apache.commons.lang.SerializationUtils;
import org.apache.commons.lang.time.DateUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.mii.constant.Constants;
import com.mii.constant.ConstantsUserLog;
import com.mii.dao.ApprovalDao;
import com.mii.dao.SchedulerDao;
import com.mii.dao.SystemParameterDao;
import com.mii.dao.UserDao;
import com.mii.helpers.FacesUtil;
import com.mii.helpers.HelperUtils;
import com.mii.models.Approval;
//import com.mii.models.Holiday;
import com.mii.models.ScheduleTask;
import com.mii.models.UserLog;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.SessionBean;
import com.mii.ws.WebServiceSchedulerExecutor;

@ManagedBean
@ViewScoped
public class SchedulerHandler implements Serializable {
	private static final long serialVersionUID = 1L;

	private ScheduleTask task;
	private ScheduleTask tempTask;
	private String action;

	private LazyDataModel<ScheduleTask> taskList;
	
	private String checkScheduleType;
	
	private String checkScheduleTypePending;
	
	@ManagedProperty(value = "#{applicationBean}")
	private ApplicationBean applicationBean;
	@ManagedProperty(value = "#{sessionBean}")
	private SessionBean sessionBean;

	private List<Integer> hours;
	private List<Integer> minutes;

	private List<String> scheduleTypes;

	private boolean runOnce = false;
	private boolean repeating = false;
	private boolean complexRepeating = false;
	private boolean repeatingOrComplexRepeating = false;

	private boolean runOncePending = false;
	private boolean repeatingPending = false;
	private boolean complexRepeatingPending = false;
	private boolean repeatingOrComplexRepeatingPending = false;
	
	private boolean doShowHistory = false;
	
	private String currentJobName;
	
	@PostConstruct
	public void init() {
		FacesUtil.authMenu(sessionBean.getUser(), "scheduler");

		taskList = new TaskDataModel();
		initSelectItemData();
		
		doLoging(ConstantsUserLog.MENU_SCHEDULER,
				ConstantsUserLog.ACTIVITY_INIT, "Menu Init.");
		
	}

	private SchedulerDao getSchedulerDAOBean() {
		return (SchedulerDao) applicationBean.getCorpGWContext().getBean(
				"schedulerDAO");
	}
	
	private SystemParameterDao getSystemParameterDAOBean(){
		return (SystemParameterDao) applicationBean.getCorpGWContext().getBean("systemParameterDAO");
	}

	private ApprovalDao getApprovalDAOBean() {
		return (ApprovalDao) applicationBean.getCorpGWContext().getBean(
				"approvalDAO");
	}

	public ApplicationBean getApplicationBean() {
		return applicationBean;
	}

	public void setApplicationBean(ApplicationBean applicationBean) {
		this.applicationBean = applicationBean;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	
	
	public String getCheckScheduleType() {
		return checkScheduleType;
	}

	public void setCheckScheduleType(String checkScheduleType) {
		this.checkScheduleType = checkScheduleType;
	}
	
	

	public String getCheckScheduleTypePending() {
		return checkScheduleTypePending;
	}

	public void setCheckScheduleTypePending(String checkScheduleTypePending) {
		this.checkScheduleTypePending = checkScheduleTypePending;
	}

	public ScheduleTask getTask() {
		return task;
	}

	public void setTask(ScheduleTask task) {
		this.task = task;
	}

	public ScheduleTask getTempTask() {
		return tempTask;
	}

	public void setTempTask(ScheduleTask tempTask) {
		this.tempTask = tempTask;
	}
	
	

	public boolean isRunOncePending() {
		return runOncePending;
	}

	public void setRunOncePending(boolean runOncePending) {
		this.runOncePending = runOncePending;
	}

	public boolean isRepeatingPending() {
		return repeatingPending;
	}

	public void setRepeatingPending(boolean repeatingPending) {
		this.repeatingPending = repeatingPending;
	}

	public boolean isComplexRepeatingPending() {
		return complexRepeatingPending;
	}

	public void setComplexRepeatingPending(boolean complexRepeatingPending) {
		this.complexRepeatingPending = complexRepeatingPending;
	}

	public boolean isRepeatingOrComplexRepeatingPending() {
		return repeatingOrComplexRepeatingPending;
	}

	public void setRepeatingOrComplexRepeatingPending(
			boolean repeatingOrComplexRepeatingPending) {
		this.repeatingOrComplexRepeatingPending = repeatingOrComplexRepeatingPending;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public List<Integer> getHours() {
		return hours;
	}

	public void setHours(List<Integer> hours) {
		this.hours = hours;
	}

	public List<Integer> getMinutes() {
		return minutes;
	}

	public void setMinutes(List<Integer> minutes) {
		this.minutes = minutes;
	}

	public List<String> getScheduleTypes() {
		return scheduleTypes;
	}

	public void setScheduleTypes(List<String> scheduleTypes) {
		this.scheduleTypes = scheduleTypes;
	}

	public boolean isRunOnce() {
		return runOnce;
	}

	public void setRunOnce(boolean runOnce) {
		this.runOnce = runOnce;
	}

	public boolean isRepeating() {
		return repeating;
	}

	public void setRepeating(boolean repeating) {
		this.repeating = repeating;
	}

	public boolean isComplexRepeating() {
		return complexRepeating;
	}

	public void setComplexRepeating(boolean complexRepeating) {
		this.complexRepeating = complexRepeating;
	}

	public boolean isRepeatingOrComplexRepeating() {
		return repeatingOrComplexRepeating;
	}

	public void setRepeatingOrComplexRepeating(
			boolean repeatingOrComplexRepeating) {
		this.repeatingOrComplexRepeating = repeatingOrComplexRepeating;
	}

	public LazyDataModel<ScheduleTask> getTaskList() {
		return taskList;
	}

	public void setTaskList(LazyDataModel<ScheduleTask> taskList) {
		this.taskList = taskList;
	}
	
	
	public void showHistory(){
		doShowHistory = true;
	}
	
	public void hideHistory(){
		doShowHistory = false;
	}
	
	public boolean isDoShowHistory() {
		return doShowHistory;
	}

	public void setDoShowHistory(boolean doShowHistory) {
		this.doShowHistory = doShowHistory;
	}

	public void initSelectItemData() {

		scheduleTypes = new ArrayList<String>();
		scheduleTypes.add(Constants.SCHEDULER_RUN_ONCE);
		scheduleTypes.add(Constants.SCHEDULER_REPEATING);
		scheduleTypes.add(Constants.SCHEDULER_COMPLEX_REPEATING);

		hours = new ArrayList<Integer>();
		for (int i = 0; i < 25; i++) {
			hours.add(i);
		}

		minutes = new ArrayList<Integer>();
		for (int i = 0; i < 61; i++) {
			minutes.add(i);
		}
	}

	public void newTask() {
		this.task = new ScheduleTask();
		task.setSchedulerType(Constants.SCHEDULER_RUN_ONCE);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd-hhmm-sss");
		Date date = new Date();
		String id = sdf.format(date);
		Integer seq = getSystemParameterDAOBean().getParameterSequence().intValue();
		this.task.setSchedulerId("SCH-"+id+String.format("%04d", seq));
		this.action = "";
		checkScheduleType();
	}

	public void checkScheduleType() {
		if (task.getSchedulerType().equals(Constants.SCHEDULER_RUN_ONCE)) {
			runOnce = true;
			repeating = false;
			complexRepeating = false;
			repeatingOrComplexRepeating = false;
		} else if (task.getSchedulerType()
				.equals(Constants.SCHEDULER_REPEATING)) {
			runOnce = false;
			repeating = true;
			complexRepeating = false;
			repeatingOrComplexRepeating = true;
		} else if (task.getSchedulerType().equals(
				Constants.SCHEDULER_COMPLEX_REPEATING)) {
			runOnce = false;
			repeating = false;
			complexRepeating = true;
			repeatingOrComplexRepeating = true;
		} else {
			runOnce = false;
			repeating = false;
			complexRepeating = false;
			repeatingOrComplexRepeating = false;
		}
	}
	
	public void checkScheduleTypePending() {
		if (tempTask.getSchedulerType().equals(Constants.SCHEDULER_RUN_ONCE)) {
			runOncePending = true;
			repeatingPending = false;
			complexRepeatingPending = false;
			repeatingOrComplexRepeatingPending = false;
		} else if (tempTask.getSchedulerType()
				.equals(Constants.SCHEDULER_REPEATING)) {
			runOncePending = false;
			repeatingPending = true;
			complexRepeatingPending = false;
			repeatingOrComplexRepeatingPending = true;
		} else if (tempTask.getSchedulerType().equals(
				Constants.SCHEDULER_COMPLEX_REPEATING)) {
			runOncePending = false;
			repeatingPending = false;
			complexRepeatingPending = true;
			repeatingOrComplexRepeatingPending = true;
		} else {
			runOncePending = false;
			repeatingPending = false;
			complexRepeatingPending = false;
			repeatingOrComplexRepeatingPending = false;
		}
	}

	public void viewDetailedTask(ScheduleTask task) {
		SchedulerDao schedulerDao = getSchedulerDAOBean();
		this.task = schedulerDao.getDetailedTask(task.getSchedulerId());

		if (this.task.getApprovalStatus().equals(
				Constants.APPROVAL_STATUS_PENDING_EDIT)) {
			List<Approval> approvals = getApprovalDAOBean()
					.getApprovalByParameterId(task.getSchedulerId());
			if (approvals.size() > 0) {
				try {
					this.tempTask = (ScheduleTask) HelperUtils
							.deserialize(approvals.get(0).getNewBlobObject()
									.getBinaryStream());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				checkScheduleType();
				checkScheduleTypePending();
				RequestContext.getCurrentInstance().execute(
						"viewSchedulerEdit.show()");
			}
		} else {
			checkScheduleType();
			RequestContext.getCurrentInstance().execute("viewScheduler.show()");
		}
	}

	public void viewDetailedTaskEdit(ScheduleTask task) {

		SchedulerDao schedulerDao = getSchedulerDAOBean();
		this.task = schedulerDao.getDetailedTask(task.getSchedulerId());

		if (this.task.getDisableEdit() == true) {
			FacesUtil.setApprovalWarning("task", this.task.getJobName(),
					this.task.getApprovalStatus());
		} else {

			if (this.task.getApprovalStatus().equals(
					Constants.APPROVAL_STATUS_PENDING_EDIT)) {

				List<Approval> approvals = getApprovalDAOBean()
						.getApprovalByParameterId(task.getSchedulerId());
				if (approvals.size() > 0) {
					try {
						this.tempTask = (ScheduleTask) HelperUtils
								.deserialize(approvals.get(0)
										.getNewBlobObject().getBinaryStream());
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					checkScheduleType();
					checkScheduleTypePending();
					RequestContext.getCurrentInstance().execute(
							"insertOrEditTaskPending.show()");
				}
			} else {

				this.tempTask = (ScheduleTask) SerializationUtils
						.clone(this.task);

				checkScheduleType();
				RequestContext.getCurrentInstance().execute(
						"insertOrEditTask.show()");
			}

			currentJobName = this.tempTask.getJobName();
			this.action = "edit";
		}
	}

	public void viewDetailedDelete(ScheduleTask task) {

		SchedulerDao schedulerDao = getSchedulerDAOBean();
		this.task = schedulerDao.getDetailedTask(task.getSchedulerId());

		if (this.task.getDisableDelete() == true) {
			FacesUtil.setApprovalWarning("task", this.task.getJobName(),
					this.task.getApprovalStatus());
		} else {
			RequestContext.getCurrentInstance().execute("dlgConfirm.show()");
		}
	}

	public void viewDetailedStop(ScheduleTask task) {

		SchedulerDao schedulerDao = getSchedulerDAOBean();
		this.task = schedulerDao.getDetailedTask(task.getSchedulerId());

		if (this.task.getDisableStopStart() == true) {
			FacesUtil.setApprovalWarning("task", this.task.getJobName(),
					this.task.getApprovalStatus());
		} else {
			RequestContext.getCurrentInstance()
					.execute("dlgConfirmStop.show()");
		}
	}

	public void viewDetailedStart(ScheduleTask task) {

		SchedulerDao schedulerDao = getSchedulerDAOBean();
		this.task = schedulerDao.getDetailedTask(task.getSchedulerId());

		if (this.task.getDisableStopStart() == true) {
			FacesUtil.setApprovalWarning("task", this.task.getJobName(),
					this.task.getApprovalStatus());
		} else {

			RequestContext.getCurrentInstance().execute(
					"dlgConfirmStart.show()");
		}

	}
	
	public void viewDetailedPause(ScheduleTask task) {

		SchedulerDao schedulerDao = getSchedulerDAOBean();
		this.task = schedulerDao.getDetailedTask(task.getSchedulerId());

		if (this.task.getDisableStopStart() == true) {
			FacesUtil.setApprovalWarning("task", this.task.getJobName(),
					this.task.getApprovalStatus());
		} else {

			RequestContext.getCurrentInstance()
					.execute("dlgConfirmPause.show()");
		}
	}

	public void viewDetailedResume(ScheduleTask task) {

		SchedulerDao schedulerDao = getSchedulerDAOBean();
		this.task = schedulerDao.getDetailedTask(task.getSchedulerId());
		
		if (this.task.getDisableStopStart() == true) {
			FacesUtil.setApprovalWarning("task", this.task.getJobName(),
					this.task.getApprovalStatus());
		} else {

			RequestContext.getCurrentInstance().execute(
					"dlgConfirmResume.show()");
		}

	}


	public void startTask() {
		try {
			if (!WebServiceSchedulerExecutor.statusScheduler(task.getTaskId()).equals(
					Constants.SCHEDULER_STATUS_STARTED) && !this.task.getSchedulerStatus().equals(
							Constants.SCHEDULER_STATUS_STANDBY)) {
				WebServiceSchedulerExecutor.startScheduler(this.task
						.getTaskId());

				task.setSchedulerStatus(WebServiceSchedulerExecutor.statusScheduler(task.getTaskId()));	
				getSchedulerDAOBean().saveTask(task);
				FacesUtil.setInfoMessageDialog("Success",
						"Start scheduler "+task.getJobName()+" successfuly");
			} else {
				FacesUtil.setWarningMessageDialog("Warning",
						""+task.getJobName()+" scheduler already running / stand by");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			FacesUtil.setErrorMessageDialog("Error", "Start scheduler failed");
		}
	}
	
	public void pauseTask() {
		try {
			if (!WebServiceSchedulerExecutor.statusScheduler(task.getTaskId()).equals(
					Constants.SCHEDULER_STATUS_SHUTDOWN)) {
				if (!WebServiceSchedulerExecutor.statusScheduler(task.getTaskId()).equals(
						Constants.SCHEDULER_STATUS_STANDBY)) {
							WebServiceSchedulerExecutor.pauseScheduler(this.task.getTaskId());

				task.setSchedulerStatus(WebServiceSchedulerExecutor.statusScheduler(task.getTaskId()));	
				getSchedulerDAOBean().saveTask(task);
				FacesUtil.setInfoMessageDialog("Success",
						"Pause scheduler "+task.getJobName()+" successfuly.");
				doLoging(ConstantsUserLog.MENU_SCHEDULER,
						ConstantsUserLog.ACTIVITY_PAUSE_SCHEDULER, "Pause Scheduler that allready Paused: ".concat(task.getTaskId()));
				} else {
					FacesUtil.setWarningMessageDialog("Warning",
							""+task.getJobName()+" scheduler already paused.");
				}
			} else {
				FacesUtil.setWarningMessageDialog("Warning",
						""+task.getJobName()+" scheduler has been stopped.");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			FacesUtil.setErrorMessageDialog("Error", "Stop scheduler failed.");
		}
	}

	public void resumeTask() {
		try {
			if (!WebServiceSchedulerExecutor.statusScheduler(task.getTaskId()).equals(
					Constants.SCHEDULER_STATUS_STARTED)) {
				if (!WebServiceSchedulerExecutor.statusScheduler(task.getTaskId()).equals(
						Constants.SCHEDULER_STATUS_SHUTDOWN)) {
							WebServiceSchedulerExecutor.resumeScheduler(this.task.getTaskId());

				task.setSchedulerStatus(WebServiceSchedulerExecutor.statusScheduler(task.getTaskId()));	
				getSchedulerDAOBean().saveTask(task);
				FacesUtil.setInfoMessageDialog("Success",
						"Start scheduler "+task.getJobName()+" success");
				doLoging(ConstantsUserLog.MENU_SCHEDULER,
						ConstantsUserLog.ACTIVITY_RESUME_SCHEDULER, "Resume Scheduler : ".concat(task.getTaskId()));
				} else {
					FacesUtil.setWarningMessageDialog("Warning",
							""+task.getJobName()+" scheduler has been stopped.");
				}
			} else {
				FacesUtil.setWarningMessageDialog("Warning",
						""+task.getJobName()+" scheduler already running.");
				doLoging(ConstantsUserLog.MENU_SCHEDULER,
						ConstantsUserLog.ACTIVITY_RESUME_SCHEDULER, "Resume Scheduler that allready Running: ".concat(task.getTaskId()));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			FacesUtil.setErrorMessageDialog("Error", "Start scheduler failed");
		}
	}
	
	public void refresh(){
		System.out.println("Masuk Scheduler Refresh..");
		try {
			//update start date and start time
			for(ScheduleTask sch : getSchedulerDAOBean().getAllValidTask()){
				//for start date
				String startDate_ = "";
					DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
					Date date = new Date();
					startDate_ = dateFormat.format(date);
					getSchedulerDAOBean().getAllValidTask().remove(sch.getStartDate());
					sch.setStartDate(startDate_);
				
				//for start time	
				String nextTime = "";
					DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
					Date timeNow = new Date();
					Date time = DateUtils.addMinutes(timeNow, Constants.ADD_MINUTES_SCHEDULER);
					nextTime = timeFormat.format(time);
					getSchedulerDAOBean().getAllValidTask().remove(sch.getStartTime());
					sch.setStartTime(nextTime);
					
				//update to DB
				getSchedulerDAOBean().updateDateTime(sch.getJobName(), startDate_,nextTime);
			}
			
			for(ScheduleTask sch : getSchedulerDAOBean().getAllValidTask()){
				String taskId;

				try {
					//System.out.println("[Delete Scheduller]Task ID = "+sch.getTaskId());
					WebServiceSchedulerExecutor.deleteScheduler(sch.getTaskId());
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				taskId = WebServiceSchedulerExecutor.createScheduler(sch);
				
				getSchedulerDAOBean().getAllValidTask().remove(sch.getTaskId());
				//System.out.println("New Task ID = "+taskId);
				sch.setTaskId(taskId);
				//System.out.println("getDisableStopStart value = "+sch.getDisableStopStart());
				
				if (!sch.getDisableStopStart()){
					if (sch.getSchedulerStatus().equals(Constants.SCHEDULER_STATUS_STARTED)){
						//System.out.println("status is started with id "+sch.getTaskId());
						WebServiceSchedulerExecutor.startScheduler(sch.getTaskId());
					}else if (sch.getSchedulerStatus().equals(Constants.SCHEDULER_STATUS_STANDBY)){
						//System.out.println("status is standby with id "+sch.getTaskId());
						WebServiceSchedulerExecutor.startScheduler(sch.getTaskId());
						WebServiceSchedulerExecutor.pauseScheduler(sch.getTaskId());
					}else{
						//System.out.println("unknown status with id "+sch.getTaskId());
						WebServiceSchedulerExecutor.startScheduler(sch.getTaskId());
					}
				}
				
				System.out.println("[Refresh Service] status scheduler "+sch.getTaskId()+" is "+WebServiceSchedulerExecutor.statusScheduler(sch.getTaskId()));
				sch.setSchedulerStatus(WebServiceSchedulerExecutor.statusScheduler(sch.getTaskId()));
				
				getSchedulerDAOBean().saveTask(sch);
			}

			FacesUtil.setInfoMessageDialog("Success","Refresh all task successfully");
			doLoging(ConstantsUserLog.MENU_SCHEDULER,
					ConstantsUserLog.ACTIVITY_REFRESH_SCHEDULER, "Refresh Scheduler SUCCESS.");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			FacesUtil.setErrorMessageDialog("Failed",
					"Refresh all task failed");
			doLoging(ConstantsUserLog.MENU_SCHEDULER,
					ConstantsUserLog.ACTIVITY_REFRESH_SCHEDULER, "Refresh Scheduler FAILED.");
		}
	}

	public void doSave() {
		if (this.action != null && this.action.equals("edit")) {
			if (this.task.getApprovalStatus().equals(
					Constants.APPROVAL_STATUS_PENDING_EDIT)) {
				try {

					ScheduleTask taskExist = getSchedulerDAOBean().getDetailedTaskByName(this.tempTask.getJobName());
					
					if (taskExist == null || (taskExist.getJobName().equals(currentJobName))){
						getApprovalDAOBean().updateApprovalNewObjectByParameterId(
								HelperUtils.serialize(this.tempTask),
								this.tempTask.getSchedulerId());
	

						RequestContext.getCurrentInstance().execute("insertOrEditTask.hide()");
						RequestContext.getCurrentInstance().execute("insertOrEditTaskPending.hide()");
						
						String headerMessage = "Edit Task Successfully";
						String bodyMessage = "Supervisor must approve the editing of the <br/>task &apos;"
								+ tempTask.getJobName()
								+ "&apos; to completely the process.";
	
						FacesUtil.setInfoMessageDialog(headerMessage, bodyMessage);
					}else{
						String headerMessage = "Attention";
						String bodyMessage = "Task "+this.tempTask.getJobName() + " already exist in the application.";

						FacesUtil.setWarningMessageDialog(headerMessage, bodyMessage);
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {

				ScheduleTask taskExist = getSchedulerDAOBean().getDetailedTaskByName(this.task.getJobName());
				
				if (taskExist == null || (taskExist.getJobName().equals(currentJobName))){
					editTask(this.task);
				}else{
					String headerMessage = "Attention";
					String bodyMessage = "Task "+this.task.getJobName() + " already exist in the application.";

					FacesUtil.setWarningMessageDialog(headerMessage, bodyMessage);
				}
			}

		} else {
			try {

				ScheduleTask taskExist = getSchedulerDAOBean().getDetailedTaskByName(this.task.getJobName());
				
				if (taskExist == null || (taskExist.getJobName().equals(currentJobName))){
					createTask(this.task);
				}else{
					String headerMessage = "Attention";
					String bodyMessage = "Task "+this.task.getJobName() + " already exist in the application.";

					FacesUtil.setWarningMessageDialog(headerMessage, bodyMessage);
				}
			} catch (Exception e) {
				String headerMessage = "Error";
				String bodyMessage = "System error";

				FacesUtil.setErrorMessageDialog(headerMessage, bodyMessage);
				e.printStackTrace();
			}
		}
	}

	public void createTask(ScheduleTask task) throws Exception {
		SchedulerDao schedulerDao = getSchedulerDAOBean();
		task.setCreateDate(HelperUtils.getCurrentDateString());
		task.setCreateWho(sessionBean.getUser().getUserName());
		task.setApprovalStatus(Constants.APPROVAL_STATUS_PENDING_CREATE);
		
		task.setSchedulerStatus(Constants.SCHEDULER_STATUS_SHUTDOWN);

		Approval approval = new Approval();
		approval.setAction(Constants.APPROVAL_ACTION_CREATE);
		approval.setParameter(Constants.APPROVAL_PARAMETER_SCHEDULER);
		approval.setParameterId(task.getSchedulerId());
		approval.setParameterName(task.getExecutionService());
		approval.setNewByteObject(HelperUtils.serialize(this.task));
		approval.setBranchId(sessionBean.getUser().getBranch());
		getApprovalDAOBean().saveApproval(approval);
		schedulerDao.saveTask(task);


		RequestContext.getCurrentInstance().execute("insertOrEditTask.hide()");
		RequestContext.getCurrentInstance().execute("insertOrEditTaskPending.hide()");
		
		String headerMessage = "Create Task Successfully";
		String bodyMessage = "Supervisor must approve the creating of the <br/>task &apos;"
				+ task.getJobName() + "&apos; to completely the process.";

		FacesUtil.setInfoMessageDialog(headerMessage, bodyMessage);
	}

	public void editTask(ScheduleTask task) {
		SchedulerDao schedulerDao = getSchedulerDAOBean();
		task.setChangeDate(HelperUtils.getCurrentDateString());
		task.setChangeWho(sessionBean.getUser().getUserName());

		try {
			if (task.getApprovalStatus().equals(
					Constants.APPROVAL_STATUS_PENDING_CREATE)) {

				getApprovalDAOBean().updateApprovalNewObjectByParameterId(
						HelperUtils.serialize(this.task),
						this.task.getSchedulerId());

			} else if (task.getApprovalStatus().equals(
					Constants.APPROVAL_STATUS_REJECTED_CREATE)) {


				Approval approval = new Approval();
				approval.setAction(Constants.APPROVAL_ACTION_CREATE);
				approval.setParameter(Constants.APPROVAL_PARAMETER_SCHEDULER);
				approval.setParameterId(task.getSchedulerId());
				approval.setParameterName(task.getExecutionService());
				approval.setBranchId(sessionBean.getUser().getBranch());
				approval.setOldByteObject(HelperUtils.serialize(this.tempTask));
				approval.setNewByteObject(HelperUtils.serialize(this.task));
				getApprovalDAOBean().saveApproval(approval);

				task = (ScheduleTask) SerializationUtils.clone(this.tempTask);
				task.setApprovalStatus(Constants.APPROVAL_STATUS_PENDING_CREATE);

			}else {

				Approval approval = new Approval();
				approval.setAction(Constants.APPROVAL_ACTION_EDIT);
				approval.setParameter(Constants.APPROVAL_PARAMETER_SCHEDULER);
				approval.setParameterId(task.getSchedulerId());
				approval.setParameterName(task.getExecutionService());
				approval.setBranchId(sessionBean.getUser().getBranch());
				approval.setOldByteObject(HelperUtils.serialize(this.tempTask));
				approval.setNewByteObject(HelperUtils.serialize(this.task));
				getApprovalDAOBean().saveApproval(approval);

				task = (ScheduleTask) SerializationUtils.clone(this.tempTask);
				task.setApprovalStatus(Constants.APPROVAL_STATUS_PENDING_EDIT);

			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		schedulerDao.saveTask(task);


		RequestContext.getCurrentInstance().execute("insertOrEditTask.hide()");
		RequestContext.getCurrentInstance().execute("insertOrEditTaskPending.hide()");
		
		String headerMessage = "Edit Task Successfully";
		String bodyMessage = "Supervisor must approve the editing of the <br/>task &apos;"
				+ task.getJobName() + "&apos; to completely the process.";

		FacesUtil.setInfoMessageDialog(headerMessage, bodyMessage);
	}

	public void deleteTask() {
		
		if (task.getApprovalStatus().equals(Constants.APPROVAL_STATUS_REJECTED_CREATE)){

			SchedulerDao schedulerDao = getSchedulerDAOBean();
			schedulerDao.deleteTask(task.getSchedulerId());
			
			String headerMessage = "Success";
			String bodyMessage = "Delete Scheduler " + this.task.getExecutionService() + "Successfully";
	
			FacesUtil.setInfoMessageDialog(headerMessage, bodyMessage);
			
		}else{
		
			Approval approval = new Approval();
			approval.setAction(Constants.APPROVAL_ACTION_DELETE);
			approval.setParameter(Constants.APPROVAL_PARAMETER_SCHEDULER);
			approval.setParameterId(task.getSchedulerId());
			approval.setParameterName(task.getExecutionService());
			approval.setBranchId(sessionBean.getUser().getBranch());
	
			try {
				approval.setOldByteObject(HelperUtils.serialize(this.task));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	
			getApprovalDAOBean().saveApproval(approval);
	
			this.task.setApprovalStatus(Constants.APPROVAL_STATUS_PENDING_DELETE);
			getSchedulerDAOBean().saveTask(this.task);
	
			String headerMessage = "Delete Task Successfully";
			String bodyMessage = "Supervisor must approve the deleting of the <br/>task &apos;"
					+ task.getJobName() + "&apos; to completely the process.";
	
			FacesUtil.setInfoMessageDialog(headerMessage, bodyMessage);
		
		}

	}

	private class TaskDataModel extends LazyDataModel<ScheduleTask> {
		private static final long serialVersionUID = 1L;

		public TaskDataModel() {

		}

		@Override
		public List<ScheduleTask> load(int startingAt, int maxPerPage,
				String sortField, SortOrder sortOrder,
				Map<String, String> filters) {

			SchedulerDao schedulerDao = getSchedulerDAOBean();
			List<ScheduleTask> tasks = new ArrayList<ScheduleTask>();

			if (filters != null && !filters.isEmpty()) {
				// String userID = filters.get("userID");
				// users = userDao.filterUsers(userID,maxPerPage+startingAt,
				// startingAt);
				// setRowCount(userDao.countFilteredUser(userID));
				// setPageSize(maxPerPage);

			} else if (action == "search") {
				// users =
				// userDao.getUserLazyLoadingByCategory(maxPerPage+startingAt,
				// startingAt, userId, branch, role, status);
				// Integer jmlAll = userDao.countUserByCategory(userId, branch,
				// role, status);
				// setRowCount(jmlAll);
				// setPageSize(maxPerPage);
			} else {
				tasks = schedulerDao.getAllTaskLazyLoading(maxPerPage
						+ startingAt, startingAt);
				Integer jmlAll = schedulerDao.countAllTask();
				setRowCount(jmlAll);
				setPageSize(maxPerPage);
			}
			return tasks;
		}
	}
	
	private void doLoging(String menu, String activity, String information) {
		UserLog userLog = new UserLog();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		userLog.setLogId("LOG-" + sdf.format(new Date()) + "-" + sessionBean.getUser().getUserID());
		userLog.setActivity(activity);
		userLog.setInformation(information);
		userLog.setInsertTime(new Date());
		userLog.setMenu(menu);
		userLog.setUserId(sessionBean.getUser().getUserID());
		userLog.setUserName(sessionBean.getUser().getUserName());
		UserLogHandler.doLog(userLog, getUserDAOBean());
	}
	
	private UserDao getUserDAOBean() {
		return (UserDao) applicationBean.getCorpGWContext().getBean("userDAO");
	}

}
