package com.mii.beans;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.apache.commons.lang.SerializationUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.mii.constant.Constants;
import com.mii.constant.ConstantsUserLog;
import com.mii.dao.ApprovalDao;
import com.mii.dao.SystemParameterDao;
import com.mii.dao.UserDao;
import com.mii.helpers.FacesUtil;
import com.mii.helpers.HelperUtils;
import com.mii.models.Approval;
import com.mii.models.SystemParameter;
import com.mii.models.UserLog;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.SessionBean;

@ManagedBean
@ViewScoped
public class SystemParameterHandler implements Serializable {
	private static final long serialVersionUID = 1L;

	private SystemParameter systemParameter;
	private SystemParameter systemParameterTemp;

	private String action;

	private String paramId;
	private String paramName;
	private String paramValue;
	private List<String> status;

	private boolean pending;
	private boolean approved;
	private boolean rejected;

	private boolean saveParameter;

	private LazyDataModel<SystemParameter> parameterList;

	@ManagedProperty(value = "#{applicationBean}")
	private ApplicationBean applicationBean;

	@ManagedProperty(value = "#{sessionBean}")
	private SessionBean sessionBean;

	private boolean doShowSearch = false;

	private boolean doShowHistory = false;

	private String currentParamName;

	
	public void showSearch() {
		doShowSearch = true;
	}

	public void hideSearch() {
		doShowSearch = false;
	}
	
	@PostConstruct
	public void init() {
		FacesUtil
				.authMenu(sessionBean.getUser(), "systemParameter");

		parameterList = new ParameterDataModel();

		pending = false;
		approved = false;
		rejected = false;
		paramId = "";
		paramName = "";
		paramValue = "";

		doSearch();
		doLoging(ConstantsUserLog.MENU_SYSTEM_PARAMETER, ConstantsUserLog.ACTIVITY_INIT, "Menu Init.");
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getParamValue() {
		return paramValue;
	}

	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}

	public String getParamId() {
		return paramId;
	}

	public void setParamId(String paramId) {
		this.paramId = paramId;
	}

	public List<String> getStatus() {
		return status;
	}

	public void setStatus(List<String> status) {
		this.status = status;
	}

	public boolean isPending() {
		return pending;
	}

	public void setPending(boolean pending) {
		this.pending = pending;
	}

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}

	public boolean isRejected() {
		return rejected;
	}

	public void setRejected(boolean rejected) {
		this.rejected = rejected;
	}

	public void showHistory() {
		doShowHistory = true;
	}

	public void hideHistory() {
		doShowHistory = false;
	}

	public boolean isDoShowHistory() {
		return doShowHistory;
	}

	public void setDoShowHistory(boolean doShowHistory) {
		this.doShowHistory = doShowHistory;
	}

	public ApplicationBean getApplicationBean() {
		return applicationBean;
	}

	public void setApplicationBean(ApplicationBean applicationBean) {
		this.applicationBean = applicationBean;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public boolean isSaveParameter() {
		return saveParameter;
	}

	public void setSaveParameter(boolean saveParameter) {
		this.saveParameter = saveParameter;
	}

	public String getParamName() {
		return paramName;
	}

	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	public SystemParameter getSystemParameter() {
		return systemParameter;
	}

	public SystemParameter getSystemParameterTemp() {
		return systemParameterTemp;
	}

	public void setSystemParameterTemp(SystemParameter systemParameterTemp) {
		this.systemParameterTemp = systemParameterTemp;
	}

	public void setSystemParameter(SystemParameter systemParameter) {
		this.systemParameter = systemParameter;
	}

	public LazyDataModel<SystemParameter> getParameterList() {
		return parameterList;
	}

	public void setParameterList(LazyDataModel<SystemParameter> parameterList) {
		this.parameterList = parameterList;
	}

	private SystemParameterDao getSystemParameterDAOBean() {
		return (SystemParameterDao) applicationBean.getCorpGWContext().getBean(
				"systemParameterDAO");
	}

	private ApprovalDao getApprovalDAOBean() {
		return (ApprovalDao) applicationBean.getCorpGWContext().getBean(
				"approvalDAO");
	}

	public boolean isDoShowSearch() {
		return doShowSearch;
	}

	public void setDoShowSearch(boolean doShowSearch) {
		this.doShowSearch = doShowSearch;
	}


	public void viewDetailedParameter(SystemParameter systemParameter) {
		SystemParameterDao systemParameterDao = getSystemParameterDAOBean();
		this.systemParameter = systemParameterDao
				.getDetailedParameter(systemParameter.getParamId());

		if (this.systemParameter.getStatus().equals(
				Constants.APPROVAL_STATUS_PENDING_EDIT)) {
			List<Approval> approvals = getApprovalDAOBean()
					.getApprovalByParameterId(systemParameter.getParamId());
			if (approvals.size() > 0) {
				try {
					this.systemParameterTemp = (SystemParameter) HelperUtils
							.deserialize(approvals.get(0).getNewBlobObject()
									.getBinaryStream());
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				RequestContext.getCurrentInstance().execute(
						"viewParameterEdit.show()");
			}
		} else {
			RequestContext.getCurrentInstance().execute("viewParameter.show()");
		}
	}

	public void viewDetailedParameterEdit(SystemParameter systemParameter) {

		SystemParameterDao systemParameterDao = getSystemParameterDAOBean();
		this.systemParameter = systemParameterDao
				.getDetailedParameter(systemParameter.getParamId());

		if (this.systemParameter.getDisableEdit() == true) {
			FacesUtil.setApprovalWarning("parameter",
					this.systemParameter.getParamName(),
					this.systemParameter.getStatus());
		} else {

			if (this.systemParameter.getStatus().equals(
					Constants.APPROVAL_STATUS_PENDING_EDIT)) {

				List<Approval> approvals = getApprovalDAOBean()
						.getApprovalByParameterId(systemParameter.getParamId());
				if (approvals.size() > 0) {
					try {
						this.systemParameterTemp = (SystemParameter) HelperUtils
								.deserialize(approvals.get(0)
										.getNewBlobObject().getBinaryStream());
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					RequestContext.getCurrentInstance().execute(
							"insertOrEditParameterPending.show()");
				}
			} else {

				this.systemParameterTemp = (SystemParameter) SerializationUtils
						.clone(this.systemParameter);
				RequestContext.getCurrentInstance().execute(
						"insertOrEditParameter.show()");
			}
			currentParamName = this.systemParameterTemp.getParamName();
			this.action = "edit";
		}
	}

	public void newParameter() {
		this.systemParameter = new SystemParameter();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd-hhmm-sss");
		Date date = new Date();
		String id = sdf.format(date);
		Integer seq = getSystemParameterDAOBean().getParameterSequence()
				.intValue();
		this.systemParameter.setParamId("PRM-" + id
				+ String.format("%04d", seq));
		this.action = "";
	}

	public void createSystemParameter(SystemParameter systemParameter) {
		SystemParameterDao systemParameterDao = getSystemParameterDAOBean();
		String paramName = this.systemParameter.getParamId();
		systemParameter.setDescription(paramName);
		systemParameter.setInsertDate(HelperUtils.getCurrentDateTime());
		systemParameter.setUpdateDate(HelperUtils.getCurrentDateTime());
		systemParameter.setChangeWho(sessionBean.getUser().getUserName());
		systemParameter.setIsEnabled("Y");
		systemParameter.setStatus(Constants.APPROVAL_STATUS_PENDING_CREATE);

		Approval approval = new Approval();
		approval.setAction(Constants.APPROVAL_ACTION_CREATE);
		approval.setParameter(Constants.APPROVAL_PARAMETER_SPARAM);
		approval.setParameterId(systemParameter.getParamId());
		approval.setParameterName(systemParameter.getParamName());
		approval.setBranchId(sessionBean.getUser().getBranch());
		try {
			approval.setNewByteObject(HelperUtils
					.serialize(this.systemParameter));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		getApprovalDAOBean().saveApproval(approval);
		getSystemParameterDAOBean().saveSystemParameter(systemParameter);

		RequestContext.getCurrentInstance().execute(
				"insertOrEditParameter.hide()");
		RequestContext.getCurrentInstance().execute(
				"insertOrEditParameterPending.hide()");

		String headerMessage = "Create Parameter Successfully";
		String bodyMessage = "Supervisor must approve the creating of the <br/>parameter &apos;"
				+ systemParameter.getParamName()
				+ "&apos; to completely the process.";
		doLoging(ConstantsUserLog.MENU_SYSTEM_PARAMETER, ConstantsUserLog.ACTIVITY_SAVE, "Create Parameter "+systemParameter.getParamName()+". Waiting for Approval.");
		FacesUtil.setInfoMessageDialog(headerMessage, bodyMessage);
	}

	public void editParameter(SystemParameter systemParameter) {
		SystemParameterDao systemParameterDao = getSystemParameterDAOBean();
		systemParameter.setUpdateDate(HelperUtils.getCurrentDateTime());
		systemParameter.setChangeWho(sessionBean.getUser().getUserName());

		try {
			if (systemParameter.getStatus().equals(
					Constants.APPROVAL_STATUS_PENDING_CREATE)) {

				getApprovalDAOBean().updateApprovalNewObjectByParameterId(
						HelperUtils.serialize(this.systemParameter),
						this.systemParameter.getParamId());

			}else if (systemParameter.getStatus().equals(
					Constants.APPROVAL_STATUS_REJECTED_CREATE)) {


				Approval approval = new Approval();
				approval.setAction(Constants.APPROVAL_ACTION_CREATE);
				approval.setParameter(Constants.APPROVAL_PARAMETER_SPARAM);
				approval.setParameterId(systemParameter.getParamId());
				approval.setParameterName(systemParameter.getParamName());
				approval.setBranchId(sessionBean.getUser().getBranch());
				approval.setOldByteObject(HelperUtils
						.serialize(this.systemParameterTemp));
				approval.setNewByteObject(HelperUtils
						.serialize(this.systemParameter));
				getApprovalDAOBean().saveApproval(approval);

				systemParameter = (SystemParameter) SerializationUtils
						.clone(this.systemParameterTemp);
				systemParameter
						.setStatus(Constants.APPROVAL_STATUS_PENDING_CREATE);

			}  else {
				Approval approval = new Approval();
				approval.setAction(Constants.APPROVAL_ACTION_EDIT);
				approval.setParameter(Constants.APPROVAL_PARAMETER_SPARAM);
				approval.setParameterId(systemParameter.getParamId());
				approval.setParameterName(systemParameter.getParamName());
				approval.setBranchId(sessionBean.getUser().getBranch());
				approval.setOldByteObject(HelperUtils
						.serialize(this.systemParameterTemp));
				approval.setNewByteObject(HelperUtils
						.serialize(this.systemParameter));
				getApprovalDAOBean().saveApproval(approval);

				systemParameter = (SystemParameter) SerializationUtils
						.clone(this.systemParameterTemp);
				systemParameter
						.setStatus(Constants.APPROVAL_STATUS_PENDING_EDIT);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		systemParameterDao.editParameter(systemParameter);

		RequestContext.getCurrentInstance().execute(
				"insertOrEditParameter.hide()");
		RequestContext.getCurrentInstance().execute(
				"insertOrEditParameterPending.hide()");

		String headerMessage = "Edit Parameter Successfully";
		String bodyMessage = "Supervisor must approve the editing of the <br/>parameter &apos;"
				+ systemParameter.getParamName()
				+ "&apos; to completely the process.";
		doLoging(ConstantsUserLog.MENU_SYSTEM_PARAMETER, ConstantsUserLog.ACTIVITY_EDIT, "Edit Parameter "+systemParameter.getParamName()+". Waiting for Approval.");
		FacesUtil.setInfoMessageDialog(headerMessage, bodyMessage);
	}

	public void doSave() {
		if (this.action != null && this.action.equals("edit")) {
			if (this.systemParameter.getStatus().equals(
					Constants.APPROVAL_STATUS_PENDING_EDIT)) {
				try {

					SystemParameter systemParameterExist = getSystemParameterDAOBean()
							.getDetailedParameterByParamName(
									this.systemParameterTemp.getParamName());

					if (systemParameterExist == null
							|| (systemParameterExist.getParamName()
									.equals(currentParamName))) {

						getApprovalDAOBean()
								.updateApprovalNewObjectByParameterId(
										HelperUtils
												.serialize(this.systemParameterTemp),
										this.systemParameterTemp.getParamId());

						RequestContext.getCurrentInstance().execute(
								"insertOrEditParameter.hide()");
						RequestContext.getCurrentInstance().execute(
								"insertOrEditParameterPending.hide()");

						String headerMessage = "Edit Parameter Successfully";
						String bodyMessage = "Supervisor must approve the editing of the <br/>parameter &apos;"
								+ systemParameterTemp.getParamName()
								+ "&apos; to completely the process.";
						doLoging(ConstantsUserLog.MENU_SYSTEM_PARAMETER, ConstantsUserLog.ACTIVITY_EDIT, "Edit Parameter "+systemParameter.getParamName()+". Waiting for Approval.");
						FacesUtil.setInfoMessageDialog(headerMessage,
								bodyMessage);

					} else {
						String headerMessage = "Attention";
						String bodyMessage = "Parameter "
								+ this.systemParameterTemp.getParamName()
								+ " already exist in the application.";

						FacesUtil.setWarningMessageDialog(headerMessage,
								bodyMessage);
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				SystemParameter systemParameterExist = getSystemParameterDAOBean()
						.getDetailedParameterByParamName(
								this.systemParameter.getParamName());

				if (systemParameterExist == null
						|| (systemParameterExist.getParamName()
								.equals(currentParamName))) {

					editParameter(this.systemParameter);
				} else {
					String headerMessage = "Attention";
					String bodyMessage = "Parameter "
							+ this.systemParameter.getParamName()
							+ " already exist in the application.";

					FacesUtil.setWarningMessageDialog(headerMessage,
							bodyMessage);
				}
			}

		} else {
			try {

				SystemParameter systemParameterExist = getSystemParameterDAOBean()
						.getDetailedParameterByParamName(
								this.systemParameter.getParamName());

				if (systemParameterExist == null
						|| (systemParameterExist.getParamName()
								.equals(currentParamName))) {
					createSystemParameter(this.systemParameter);
				} else {
					String headerMessage = "Attention";
					String bodyMessage = "Parameter "
							+ this.systemParameter.getParamName()
							+ " already exist in the application.";

					FacesUtil.setWarningMessageDialog(headerMessage,
							bodyMessage);
				}

			} catch (Exception e) {
				String headerMessage = "Error";
				String bodyMessage = "System error";

				FacesUtil.setErrorMessageDialog(headerMessage, bodyMessage);
				e.printStackTrace();
			}
		}

	}

	public void doSearch() {

		status = new ArrayList<String>();

		if (((pending == false) && (approved == false)) && (rejected == false)) {
			status.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
			status.add(Constants.APPROVAL_STATUS_PENDING_EDIT);
			status.add(Constants.APPROVAL_STATUS_PENDING_DELETE);
			status.add(Constants.APPROVAL_STATUS_APPROVED_CREATE);
			status.add(Constants.APPROVAL_STATUS_APPROVED_EDIT);
			status.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
			status.add(Constants.APPROVAL_STATUS_REJECTED_EDIT);
			status.add(Constants.APPROVAL_STATUS_REJECTED_DELETE);
		} else {
			if (pending == true) {
				status.add(Constants.APPROVAL_STATUS_PENDING_CREATE);
				status.add(Constants.APPROVAL_STATUS_PENDING_EDIT);
				status.add(Constants.APPROVAL_STATUS_PENDING_DELETE);
			}
			if (approved == true) {
				status.add(Constants.APPROVAL_STATUS_APPROVED_CREATE);
				status.add(Constants.APPROVAL_STATUS_APPROVED_EDIT);
			}
			if (rejected == true) {
				status.add(Constants.APPROVAL_STATUS_REJECTED_CREATE);
				status.add(Constants.APPROVAL_STATUS_REJECTED_EDIT);
				status.add(Constants.APPROVAL_STATUS_REJECTED_DELETE);
			}
		}
		FacesUtil.resetPage("parameterForm:parameterTable");
		parameterList = new ParameterDataModel();

	}

	public void viewDetailedDelete(SystemParameter parameter) {

		SystemParameterDao systemParameterDao = getSystemParameterDAOBean();
		this.systemParameter = systemParameterDao
				.getDetailedParameter(parameter.getParamId());

		if (this.systemParameter.getDisableDelete() == true) {
			FacesUtil.setApprovalWarning("parameter",
					this.systemParameter.getParamName(),
					this.systemParameter.getStatus());
		} else {
			RequestContext.getCurrentInstance().execute("dlgConfirm.show()");
		}

	}

	public void deleteSystemParameter() {
		

		if (systemParameter.getStatus().equals(Constants.APPROVAL_STATUS_REJECTED_CREATE)){

			getSystemParameterDAOBean().deleteSystemParameter(systemParameter.getParamId());
			
			String headerMessage = "Success";
			String bodyMessage = "Delete System Parameter " + this.systemParameter.getParamName() + "Successfully";
			doLoging(ConstantsUserLog.MENU_SYSTEM_PARAMETER, ConstantsUserLog.ACTIVITY_DELETE, "Delete Rejected Parameter "+systemParameter.getParamName()+".");
			FacesUtil.setInfoMessageDialog(headerMessage, bodyMessage);
			
		}else{
		
			Approval approval = new Approval();
			approval.setAction(Constants.APPROVAL_ACTION_DELETE);
			approval.setParameter(Constants.APPROVAL_PARAMETER_SPARAM);
			approval.setParameterId(systemParameter.getParamId());
			approval.setParameterName(systemParameter.getParamName());
			approval.setBranchId(sessionBean.getUser().getBranch());
	
			try {
				approval.setOldByteObject(HelperUtils
						.serialize(this.systemParameter));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	
			getApprovalDAOBean().saveApproval(approval);
	
			this.systemParameter
					.setStatus(Constants.APPROVAL_STATUS_PENDING_DELETE);
			getSystemParameterDAOBean().saveSystemParameter(this.systemParameter);
	
			String headerMessage = "Delete Parameter Successfully";
			String bodyMessage = "Supervisor must approve the deleting of the <br/>parameter &apos;"
					+ systemParameter.getParamName()
					+ "&apos; to completely the process.";
			doLoging(ConstantsUserLog.MENU_SYSTEM_PARAMETER, ConstantsUserLog.ACTIVITY_DELETE, "Delete Parameter "+systemParameter.getParamName()+". Waiting for Approval.");
			FacesUtil.setInfoMessageDialog(headerMessage, bodyMessage);
		
		}

	}

	private class ParameterDataModel extends LazyDataModel<SystemParameter> {
		private static final long serialVersionUID = 1L;

		public ParameterDataModel() {

		}

		@Override
		public List<SystemParameter> load(int startingAt, int maxPerPage,
				String sortField, SortOrder sortOrder,
				Map<String, String> filters) {

			SystemParameterDao parameterDao = (SystemParameterDao) applicationBean
					.getCorpGWContext().getBean("systemParameterDAO");
			List<SystemParameter> systemParameter = null;

			systemParameter = parameterDao.getSearchParameterLazyLoading(
					paramId, paramName, paramValue, status, maxPerPage
							+ startingAt, startingAt);
			Integer jmlAll = parameterDao.countSearchParameter(paramId,
					paramName, paramValue, status);
			setRowCount(jmlAll);
			setPageSize(maxPerPage);

			return systemParameter;
		}

	}
	
	private void doLoging(String menu, String activity, String information) {
		UserLog userLog = new UserLog();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		userLog.setLogId("LOG-"+sdf.format(new Date())+"-"+sessionBean.getUser().getUserID());
		userLog.setActivity(activity);
		userLog.setInformation(information);
		userLog.setInsertTime(new Date());
		userLog.setMenu(menu);
		userLog.setUserId(sessionBean.getUser().getUserID());
		userLog.setUserName(sessionBean.getUser().getUserName());
		UserLogHandler.doLog(userLog, getUserDAOBean());
	}
	
	private UserDao getUserDAOBean() {
		return (UserDao) applicationBean.getCorpGWContext().getBean("userDAO");
	}

}
