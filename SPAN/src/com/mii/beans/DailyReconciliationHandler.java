package com.mii.beans;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.mii.constant.Constants;
import com.mii.helpers.FacesUtil;
import com.mii.invoker.DatabaseParameter;
import com.mii.invoker.SpanDataInvokeSOA;
import com.mii.invoker.WMInvoker;
import com.mii.models.SpanDataDailyReconcile;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.SessionBean;
import com.wm.data.IData;
import com.wm.data.IDataCursor;
import com.wm.data.IDataFactory;
import com.wm.data.IDataUtil;

@ManagedBean
@ViewScoped
public class DailyReconciliationHandler implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private Boolean doShowGAJI;
	private Boolean doShowGAJIR;
	private String tabSelected; //GAJI - GAJIR
	private Date docDate;
	
	private LazyDataModel < SpanDataDailyReconcile > reconcileList;
	
	@ManagedProperty(value="#{applicationBean}")
	private ApplicationBean applicationBean;
	
	@ManagedProperty(value="#{sessionBean}")
	private SessionBean sessionBean;
	
	@PostConstruct
	public void init(){
		FacesUtil.authMenu(getSessionBean().getUser(), "dailyReconciliation");
		doClickGaji();
		doReconciliation(); //Today Date//
	}
	
	public void doReconciliation(){
		setReconcileList(new DetailDataModel());
	}
	
	private class DetailDataModel extends LazyDataModel < SpanDataDailyReconcile > {
		private static final long serialVersionUID = 1L;

		public DetailDataModel() {
		}

		@Override
		public List < SpanDataDailyReconcile > load(int startingAt, int maxPerPage,
		String sortField, SortOrder sortOrder,
		Map < String, String > filters) {
			List < SpanDataDailyReconcile > spanDataDailyReconcileList = new ArrayList<SpanDataDailyReconcile>();
			Integer jmlAll = 0;
			IData output = IDataFactory.create();
			try{
				// Input to WebMethod
				IData input = IDataFactory.create();
				IDataCursor inputCursor = input.getCursor();
				IDataUtil.put( inputCursor, "tabType", tabSelected);
				IDataUtil.put( inputCursor, "todayDate", validateDate(docDate));
				inputCursor.destroy();
				SpanDataInvokeSOA sdi = DatabaseParameter.getInstance().getSpanDataInvokeSOA();
				// Output from WebMethod
				output = IDataFactory.create();
				output=(IData)WMInvoker.doInvoke(sdi.getIp(),sdi.getUsername(),sdi.getPassword(),"mandiri.span.service.report", "dailyReconcile", input);
			}catch( Exception e){
				e.printStackTrace();
			}
			if (output !=null) {
				 IDataCursor outputCursor = output.getCursor();
				 IData[]	reportData = IDataUtil.getIDataArray( outputCursor, "dailyReconcileList" );
				 if ( reportData != null){
					 	jmlAll = reportData.length;
						for ( int i = 0; i < reportData.length; i++ ){
							IDataCursor SpanDailyReconcileCursor = reportData[i].getCursor();
							SpanDataDailyReconcile spr=new SpanDataDailyReconcile();
							spr.setData(IDataUtil.getString( SpanDailyReconcileCursor, "Data"));
							spr.setDate(IDataUtil.getString( SpanDailyReconcileCursor, "Date"));
							spr.setValue(IDataUtil.getString( SpanDailyReconcileCursor, "Value"));
							SpanDailyReconcileCursor.destroy();
							spanDataDailyReconcileList.add(spr);
						}
					}
				 outputCursor.destroy();
			}
			setRowCount(jmlAll);
			setPageSize(maxPerPage);
			return spanDataDailyReconcileList;
		}
	}
	
	public String validateDate(Date docDate){
		String iDate = "";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try{
			if(docDate!=null){
				iDate = sdf.format(docDate);
			}else{
				iDate = sdf.format(new Date());
			}
		}catch(Exception e){
			iDate = sdf.format(new Date());
		}
		return iDate;
	}
	
	public void doClickGaji(){
		setDoShowGAJI(true);
		setDoShowGAJIR(false);
		setTabSelected(Constants.GAJI);
		docDate = null;
	}

	public void doClickGajiR(){
		setDoShowGAJI(false);
		setDoShowGAJIR(true);
		setTabSelected(Constants.GAJIR);
		docDate = null;
	}

	public ApplicationBean getApplicationBean() {
		return applicationBean;
	}

	public void setApplicationBean(ApplicationBean applicationBean) {
		this.applicationBean = applicationBean;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public Boolean getDoShowGAJI() {
		return doShowGAJI;
	}

	public void setDoShowGAJI(Boolean doShowGAJI) {
		this.doShowGAJI = doShowGAJI;
	}

	public Boolean getDoShowGAJIR() {
		return doShowGAJIR;
	}

	public void setDoShowGAJIR(Boolean doShowGAJIR) {
		this.doShowGAJIR = doShowGAJIR;
	}

	public String getTabSelected() {
		return tabSelected;
	}

	public void setTabSelected(String tabSelected) {
		this.tabSelected = tabSelected;
	}

	public Date getDocDate() {
		return docDate;
	}

	public void setDocDate(Date docDate) {
		this.docDate = docDate;
	}

	public LazyDataModel < SpanDataDailyReconcile > getReconcileList() {
		return reconcileList;
	}

	public void setReconcileList(LazyDataModel < SpanDataDailyReconcile > reconcileList) {
		this.reconcileList = reconcileList;
	}
}
