package com.mii.constant;


public final class Constants {
	
	//UIM
	public static final String UIM_HOST 						= "UIM_HOST";
	public static final String UIM_PORT 						= "UIM_PORT";
	public static final String UIM_ID_APLIKASI					= "UIM_ID_APLIKASI";
	
	//User
	public static final String USER_DEFAULT_BRANCH				= "0001";
	
	//Email
	public static final String EMAIL_FROM_ADDRESS				= "EMAIL_FROM_ADDRESS";
	public static final String EMAIL_FROM_PASSWORD				= "EMAIL_FROM_PASSWORD";
	public static final String EMAIL_SMTP_HOST					= "EMAIL_SMTP_HOST";
	public static final String EMAIL_SMTP_PORT					= "EMAIL_SMTP_PORT";
	
	//GAPURA
	public static final String SPAN_LOCAL_IP					= "SPAN_LOCAL_IP";
	public static final String GAPURA_CID						= "GAPURA_CID";
	public static final String GAPURA_IP						= "GAPURA_IP";
	public static final String GAPURA_PORT						= "GAPURA_PORT";
	public static final String GSS_IP							= "GSS_IP";
	public static final String GSS_PORT							= "GSS_PORT";
	
	//FAST
	public static final String FAST_FOUND						= "Valid/benar";
	public static final String FAST_SALAH_NAMA					= "Salah Nama Rekening";
	public static final String FAST_SALAH_NOMOR					= "Salah Nomor Rekening";
	public static final String FAST_NOT_FOUND					= "Tidak ditemukan";
	public static final String FAST_REK_DORMANT					= "Rekening Tutup/tidak aktif";
	
	public static final String GAPURA_AB_ZLSTS_TUTUP			= "Account Closing";
	public static final String GAPURA_AB_ZLSTS_BLOKIR			= "Blocked Account";
	
	public static final String APPROVAL_STATUS_PENDING_CREATE 	= "Pending - Create";
	public static final String APPROVAL_STATUS_PENDING_EDIT 	= "Pending - Edit";
	public static final String APPROVAL_STATUS_PENDING_DELETE 	= "Pending - Delete";
	public static final String APPROVAL_STATUS_APPROVED_CREATE	= "Approved - Create";
	public static final String APPROVAL_STATUS_APPROVED_EDIT	= "Approved - Edit";
	public static final String APPROVAL_STATUS_REJECTED_CREATE 	= "Rejected - Create";
	public static final String APPROVAL_STATUS_REJECTED_EDIT 	= "Rejected - Edit";
	public static final String APPROVAL_STATUS_REJECTED_DELETE 	= "Rejected - Delete";
	
	public static final String APPROVAL_ACTION_CREATE			= "Create";
	public static final String APPROVAL_ACTION_EDIT				= "Edit";
	public static final String APPROVAL_ACTION_DELETE			= "Delete";
	
	public static final String APPROVAL_PARAMETER_USER			= "User";
	public static final String APPROVAL_PARAMETER_ROLE			= "Role";
	public static final String APPROVAL_PARAMETER_SCHEDULER		= "Scheduler";
	public static final String APPROVAL_PARAMETER_SPARAM		= "System Parameter";
	public static final String APPROVAL_PARAMETER_BRANCH		= "Branch";
	public static final String APPROVAL_PARAMETER_HOLIDAY		= "Holiday";
	
	public final static String SCHEDULER_RUN_ONCE 				= "One Time Task";
	public final static String SCHEDULER_REPEATING				= "Repeat Interval";
	public final static String SCHEDULER_COMPLEX_REPEATING 		= "Repeat Time";

	public final static String SCHEDULER_STATUS_SHUTDOWN		= "Shutdown";
	public final static String SCHEDULER_STATUS_STARTED 		= "Started";
	public final static String SCHEDULER_STATUS_STANDBY 		= "Standby";
	
	public final static String APPROVAL_STATUS_CODE_SUCCESS	    = "0";
	public final static String APPROVAL_STATUS_CODE_FAILED	    = "1";
	
	public final static String DEFAULT_ROLE_ID_UPLOAD_USER		= "DEFAULT_ROLE_ID_UPLOAD_USER";

	/*public final static String WEB_SERVICE_IP				    = "WEB_SERVICE_IP";
	public final static String WEB_SERVICE_PORT				    = "WEB_SERVICE_PORT";
	public final static String WEB_SERVICE_USERNAME			    = "WEB_SERVICE_USERNAME";
	public final static String WEB_SERVICE_PASSWORD			    = "WEB_SERVICE_PASSWORD";*/
	
	public final static String WEB_SERVICE_INTERNAL_IP			= "WEB_SERVICE_INTERNAL_IP";
	public final static String WEB_SERVICE_INTERNAL_PORT		= "WEB_SERVICE_INTERNAL_PORT";
	
	/*public final static String LDAP_ADDRESS_URL				= "LDAP_ADDRESS_URL";
	public final static String LDAP_SEARCH_BASE					= "LDAP_SEARCH_BASE";
	public final static String LDAP_SEARCH_ATTRIBUTE			= "LDAP_SEARCH_ATTRIBUTE";
	public final static String LDAP_DOMAIN						= "LDAP_DOMAIN";*/
	
	/*public final static String KODE_CABANG_KANTOR_PUSAT		= "KANTOR_PUSAT";*/
	
	/*public final static String ROLE_ID_TELLER					= "ROLE_ID_TELLER";
	public final static String ROLE_ID_TELLER_SUPERVISOR		= "ROLE_ID_TELLER_SUPERVISOR";
	public final static String ROLE_ID_DEPKEU					= "ROLE_ID_DEPKEU";
	public final static String ROLE_ID_BO						= "ROLE_ID_BO";*/
	
	public final static int ADD_MINUTES_SCHEDULER				= 3;
	
	public final static String PARAM_DEBITACCNO					= "DEBIT_ACCOUNT_NO_";
	public final static String GAJI 							= "GAJI";
	public final static String CODE_GAJI 						= "SPN";
	public final static String CODE_GAJIR 						= "SPN01";
	public final static String GAJIR 							= "GAJIR";
	public final static String GAJI_R 							= "GAJI R";
	public final static String ACK								= "ACK";
	public final static String BS								= "BS";
	public final static String SORBOR							= "SORBOR";
	public final static String DEBIT_ACCOUNT_NO_GAJI_NAME 		= "DEBIT_ACCOUNT_NO_GAJI_NAME";
	public final static String DEBIT_ACCOUNT_NO_GAJIR_NAME 		= "DEBIT_ACCOUNT_NO_GAJIR_NAME";
	
	public final static String EMAILWAIT						= "EMAILWAIT";
	public final static String EMAILAPPROVED					= "EMAILAPPROVED";
	
	public final static String FISTLOGINYES						= "Y";
	public final static String FISTLOGINNO						= "N";
	public final static String FISTLOGINCHANGE					= "C";
	
	public final static String VOIDFLAG							= "0";
	public final static String VOIDDOCNO_INVOKE_TYPE_PARAM		= "VOIDDOCNO_INVOKE_TYPE_PARAM";
	
	public final static int MAXAPPROVELATEFILE					= 100;
	
	public static final String SORBOREXT = ".SBR";
	
	//GET_SPAN_SUMMARIES
	public final static String DATESTART_PART1					= "and b.create_date>=to_date('";
	public final static String DATESTART_PART2					= " 00:00:00', 'yyyyMMdd HH24:MI:SS')";
	public final static String DATEEND_PART1					= "and b.create_date<=to_date('";
	public final static String DATEEND_PART2					= " 23:59:59', 'yyyyMMdd HH24:MI:SS')";
	
	//SFTP
	public static final String SPAN_DEPKEU_MANUAL_PUT_LOCAL_DIR = "SPAN_DEPKEU_MANUAL_PUT_LOCAL_DIR";
	
	//FTP
	public static final String SPAN_CREATE_SORBOR_PATH = "FTP_SORBOR_FILE";
	public static final String SPAN_DEPKEU_SFTP_IPADDRESS = "SPAN_DEPKEU_SFTP_IPADDRESS";
	public static final String SPAN_DEPKEU_SFTP_USERNAME = "SPAN_DEPKEU_SFTP_USERNAME";
	public static final String SPAN_DEPKEU_SFTP_PASSWORD = "SPAN_DEPKEU_SFTP_PASSWORD";
	public static final String SPAN_DEPKEU_SFTP_PUT_SORBOR_REMOTE_DIR = "SPAN_DEPKEU_SFTP_PUT_SORBOR_REMOTE_DIR";
	public static final String JAR_BACKUP_SORBOR = "JAR_BACKUP_SORBOR";
	
	//BANK_ATTRIBUTE
	public static final String SPAN_BANK_NAME = "BANK_NAME";
	public static final String SPAN_BANK_CODE = "BANK_CODE";
	public static final String KODE_BANK_12_DIGIT = "KODE_BANK_12_DIGIT";
	
	//FILE CONTENT TYPE
	public static final String CONTENT_TYPE_PDF = "application/pdf";
	public static final String CONTENT_TYPE_XLS = "application/vnd.ms-excel";
	
	//T_PARAMETER
	public static final String DB_JASPER_HOST = "DB_JASPER_HOST";
	public static final String DB_JASPER_PORT = "DB_JASPER_PORT";
	public static final String DB_JASPER_UNAME = "DB_JASPER_UNAME";
	public static final String DB_JASPER_PASS = "DB_JASPER_PASS";
	public static final String DB_JASPER_DB_NAME = "DB_JASPER_DB_NAME";
	
	//OTHER
	public static final String PDF = "PDF";
	public static final String EXCEL = "EXCEL";
	
	
	//ACCT DATA VALIDATION
	public static final String ERROR="Error";
	
	//send ack void scheduller
	public static final String RC_VOID = "78";
	public static final String MSG_VOID = "VOID";
	
	//DOWNLOAD FILE CONSTANT
	public static final Object BS_DIR = "DL_BS_DIR";
	public static final Object SORBOR_DIR = "DL_SORBOR_DIR";
	public static final Object INBOUND_DIR = "DL_INBOUND_DIR";
	public static final Object BS_LIKE = "DL_BS_LIKE";
	public static final Object SORBOR_LIKE = "DL_SORBOR_LIKE";
	public static final Object INBOUND_LIKE = "DL_INBOUND_LIKE";
	public static final Object BS_EXT = "DL_BS_EXT";
	public static final Object SORBOR_EXT = "DL_SORBOR_EXT";
	public static final Object INBOUND_EXT = "DL_INBOUND_EXT";
	
	public static final String GAPURA_CONNECTION_TIMEOUT = "GAPURA_TIMEOUT. Timeout when connecting to GAPURA, Please Try Again.";
	public static final String GAPURA_RC_SUCCESS = "0000";
	public static final String GAPURA_RC_NOT_FOUND = "0004";
	
	//START END INDEX FOR NAME VALIDATION
	public static final int START_INDEX_NAME_VALIDATION 	= 0;
	public static final int END_INDEX_NAME_VALIDATION 		= 35;

}
