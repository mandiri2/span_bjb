package com.mii.constant;

public class ConstantsUserLog {
	//MENU//
	public static final String MENU_LOGIN 					= "LOGIN";
	public static final String MENU_USER 					= "USER";
	public static final String MENU_ROLE 					= "ROLE";
	public static final String MENU_SYSTEM_PARAMETER 		= "SYSTEM PARAMETER";
	public static final String MENU_APPROVAL		 		= "APPROVAL";
	public static final String MENU_RESULT_TO_BE_PROCESS	= "RESULT TO BE PROCESS";
	public static final String MENU_SUMMARY					= "SUMMARY";
	public static final String MENU_MONITORING_FILE_REJECT	= "MONITORING FILE REJECT";
	public static final String MENU_SKNRTGS					= "SKNRTGS";
	public static final String MENU_MONITORING_VOID_DEPKEU	= "MONITORING VOID DEPKEU";
	public static final String MENU_DATA_VALIDATION			= "DATA VALIDATION";
	public static final String MENU_SCHEDULER				= "SCHEDULER";
	
	//CORE ACTIVITY//
	public static final String ACTIVITY_LOGIN 				= "LOGIN";
	public static final String ACTIVITY_INIT 				= "INIT";
	public static final String ACTIVITY_APPROVE 			= "APPROVE";
	public static final String ACTIVITY_REJECT	 			= "REJECT";
	
	//RESULT TO BE PROCESS ACTIVITY//
	public static final String ACTIVITY_RTBP_GAJI 			= "GAJI";
	public static final String ACTIVITY_RTBP_GAJIR 			= "GAJIR";
	public static final String ACTIVITY_RTBP_RETRY 			= "RETRY";
	public static final String ACTIVITY_RTBP_RETUR 			= "RETUR";
	public static final String ACTIVITY_RTBP_SEND_ACK 		= "SEND ACK";
	public static final String ACTIVITY_RTBP_INQUIRY_SALDO	= "INQUIRY SALDO";
	public static final String ACTIVITY_RTBP_EXECUTE		= "EXECUTE";
	public static final String ACTIVITY_RTBP_FORCE_ACK		= "FORCE ACK";
	
	//CRUD ACTIVITY//
	public static final String ACTIVITY_SAVE 				= "SAVE";
	public static final String ACTIVITY_EDIT 				= "EDIT";
	public static final String ACTIVITY_DELETE 				= "DELETE";
	
	//MONITORING VOID ACTIVITY//
	public static final String ACTIVITY_VOID_CHECKED		= "VOID CHECKED";
	public static final String ACTIVITY_UNVOID_CHECKED		= "UNVOID CHECKED";
	public static final String ACTIVITY_SEARCH_DOC_NO		= "SEARCH DOC NO";
	public static final String ACTIVITY_VOID_DOC_NO			= "VOID DOC NO";
	public static final String ACTIVITY_UNVOID_DOC_NO		= "UNVOID DOC NO";
	
	//DATA VALIDATION ACTIVITY//
	public static final String ACTIVITY_SINGLE_DATA_VALIDATION		= "INQUIRY SINGLE";
	public static final String ACTIVITY_BULK_DATA_VALIDATION		= "INQUIRY BULK";
	
	//SCHEDULER ACTIVITY//
	public static final String ACTIVITY_PAUSE_SCHEDULER		= "PAUSE";
	public static final String ACTIVITY_RESUME_SCHEDULER	= "RESUME";
	public static final String ACTIVITY_REFRESH_SCHEDULER	= "REFRESH";
}
