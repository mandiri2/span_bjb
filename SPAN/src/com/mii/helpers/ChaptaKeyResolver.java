package com.mii.helpers;

import java.util.HashMap;
import java.util.Map;

public class ChaptaKeyResolver {

	private static ChaptaKeyResolver singleton = new ChaptaKeyResolver();
	
	public static Map<String,String> chaptchaTemp = new HashMap<String, String>();

	/*
	 * A private Constructor prevents any other class from instantiating.
	 */
	private ChaptaKeyResolver() {
	}

	/* Static 'instance' method */
	public static ChaptaKeyResolver getInstance() {
		return singleton;
	}

	/* Other methods protected by singleton-ness */
	protected static void demoMethod() {
		//System.out.println("demoMethod for singleton");
	}

	public static ChaptaKeyResolver getSingleton() {
		return singleton;
	}

	public static void setSingleton(ChaptaKeyResolver singleton) {
		ChaptaKeyResolver.singleton = singleton;
	}
	
}
