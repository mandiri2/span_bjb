package com.mii.helpers;

import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator("schedulerTimeValidator")
public class SchedulerTimeValidator implements Validator {

	private Pattern pattern;

	private static final String TIME_PATTERN = "([01]?[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]";

	public SchedulerTimeValidator() {
		pattern = Pattern.compile(TIME_PATTERN);
	}

	@Override
	public void validate(FacesContext arg0, UIComponent arg1, Object value)
			throws ValidatorException {
		// TODO Auto-generated method stub
		if ((value == null) || (value == "")) {
			return;
		}
		// hh:mm:ss
		if (!pattern.matcher(value.toString()).matches()) {
			throw new ValidatorException(new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "Validation Error",
					"Invalid time."));
		}
	}

}




