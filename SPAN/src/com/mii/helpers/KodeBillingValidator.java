package com.mii.helpers;

import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator("kodeBillingValidator")
public class KodeBillingValidator implements Validator {


	@Override
	public void validate(FacesContext arg0, UIComponent arg1, Object value)
			throws ValidatorException {
		// TODO Auto-generated method stub
		if ((value == null) || (value == "")) {
			return;
		}
		// dd/mm/yyyy
		if (value.toString().length() != 15) {
			throw new ValidatorException(new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "Validation Error",
					"Length "+value.toString().length()+", should be 15"));
		}
	}

}
