package com.mii.helpers;

import javax.faces.application.NavigationHandler;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.servlet.http.HttpSession;

import com.mii.constant.Constants;
import com.mii.models.User;
import com.mii.scoped.ApplicationBean;
import com.mii.scoped.SessionBean;

public class AuthorizationListener implements PhaseListener {
	private static final long serialVersionUID = 1L;

	@Override
	public void afterPhase(PhaseEvent event) {
//		System.out.println(">>> START AFTER PHASE AuthorizationListener");
		FacesContext facesContext = event.getFacesContext();
		String currentPage = facesContext.getViewRoot().getViewId();
		 
		boolean isLoginPage = (currentPage.lastIndexOf("loginSPAN.xhtml") > -1);
		
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
		if(session == null){
			NavigationHandler nh = facesContext.getApplication().getNavigationHandler();
			nh.handleNavigation(facesContext, null, "loginSPAN");
		}else{
			Object currentUser = session.getAttribute("user");
			if (!User.getLogins().containsKey(currentUser)) {
				currentUser = null;
				session.invalidate();
			}
			NavigationHandler nh = facesContext.getApplication().getNavigationHandler();
			if (!isLoginPage && (currentUser == null)) {
				nh.handleNavigation(facesContext, null, "loginSPAN");
				
			}else if(isLoginPage && currentUser != null){
				//NEW USER VALIDATION
				User cekUser = (User) currentUser;
				if(cekUser.getFirstLogin().equalsIgnoreCase(Constants.FISTLOGINNO)){
					nh.handleNavigation(facesContext, null, "welcome");
				}else{
					nh.handleNavigation(facesContext, null, "newPasswordSPAN");
				}
			}
		}
//		System.out.println(">>> END AFTER PHASE AuthorizationListener");
	}

	@Override
	public void beforePhase(PhaseEvent event) {
//		System.out.println(">>> BEFORE PHASE AuthorizationListener");
	}

	@Override
	public PhaseId getPhaseId() {
		return PhaseId.RESTORE_VIEW;
	}

}
