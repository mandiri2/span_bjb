package com.mii.helpers;

import com.mii.constant.Constants;
import com.mii.dao.SystemParameterDao;
import com.mii.models.SystemParameter;
import com.mii.scoped.ApplicationBean;

public class LdapConfigurator {
	
	private String addressUrl = "";
	
	private String searchBase = "";
	
	private String searchAttribute = "";
	
	public LdapConfigurator(){
		SystemParameter systemParameter;
		/*systemParameter = getSystemParameterDAOBean().getDetailedParameterByParamName(Constants.LDAP_ADDRESS_URL);
		addressUrl		= systemParameter.getParamValue();

		systemParameter = getSystemParameterDAOBean().getDetailedParameterByParamName(Constants.LDAP_SEARCH_BASE);
		searchBase		= systemParameter.getParamValue();

		systemParameter = getSystemParameterDAOBean().getDetailedParameterByParamName(Constants.LDAP_SEARCH_ATTRIBUTE);
		searchAttribute	= systemParameter.getParamValue();*/

	}
	
	private SystemParameterDao getSystemParameterDAOBean(){
		ApplicationBean applicationBean = (ApplicationBean) FacesUtil.getManagedBean("applicationBean", null);
		return (SystemParameterDao) applicationBean.getCorpGWContext().getBean("systemParameterDAO");
	}

	public String getAddressUrl() {
		return addressUrl;
	}

	public void setAddressUrl(String addressUrl) {
		this.addressUrl = addressUrl;
	}

	public String getSearchBase() {
		return searchBase;
	}

	public void setSearchBase(String searchBase) {
		this.searchBase = searchBase;
	}

	public String getSearchAttribute() {
		return searchAttribute;
	}

	public void setSearchAttribute(String searchAttribute) {
		this.searchAttribute = searchAttribute;
	}

	
}
