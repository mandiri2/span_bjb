package com.mii.helpers;

import com.zehon.exception.FileTransferException;
import com.zehon.ftp.FTP;
import com.zehon.sftp.SFTP;

public class FTPHelper {
	  public static String[] getFileNames(String host, String username, String password, String spanPath, String localPath, int totFileNames)
	  {
	    String[] fileNames = new String[totFileNames];
	    try
	    {
//	      SFTP.getFolder(spanPath, localPath, null, host, username, password);
//	      fileNames = SFTP.getFileNamesInFolder(spanPath, host, username, password);
	      System.out.println("Init FTP Connection");
	      FTP.getFolder(spanPath, localPath, null, host, username, password);
	      System.out.println("Get Folder.");
	      fileNames = FTP.getFileNamesInFolder(spanPath, host, username, password);
	      System.out.println("File found in server : " + fileNames.length);
	    }
	    catch (FileTransferException e)
	    {
	      e.printStackTrace();
	    }
	    return fileNames;
	  }
	  
	  public static boolean moveFileName(String spanPath, String fileName, String spanBackupFolder, String newFileName, String host, String username, String password)
	  {
	    boolean status_ = true;
	    try
	    {
	      int status = SFTP.moveFile(fileName, spanPath, newFileName, 
	        spanBackupFolder, host, username, password);
	      if (1 != status) {
	        if (status == 0) {
	          status_ = false;
	        }
	      }
	    }
	    catch (FileTransferException e)
	    {
	      e.printStackTrace();
	      status_ = false;
	    }
	    return status_;
	  }
	  
	  public static boolean uploadFileFTP(String host, String username, String password, String spanPath, String localPath, String fileName)
	  {
	    boolean status_ = true;
	    try
	    {
	      long start = System.currentTimeMillis();
	      long end = 0L;
	      
	      String filePath = localPath + "/" + fileName;
	      
	      start = System.currentTimeMillis();
	      int status = FTP.sendFile(filePath, spanPath, host, username, password);
//	      int status = SFTP.sendFile(filePath, spanPath, host, username, password);
	      end = System.currentTimeMillis() - start;
	      if (1 == status) {
	        System.out.println("The time for get file (" + fileName + ") : " + end);
	      } else if (status == 0) {
	        status_ = false;
	      }
	    }
	    catch (FileTransferException e)
	    {
	      e.printStackTrace();
	      status_ = false;
	    }
	    return status_;
	  }
	  
	  public static boolean getFileFTP(String remoteFileName, String serverName, String username, String password, String spanPath, String localPath)
	  {
	    boolean status_ = true;
	    
	    long start = System.currentTimeMillis();
	    long end = 0L;
	    try
	    {
	      start = System.currentTimeMillis();
	      int status = FTP.getFile(remoteFileName, spanPath, serverName, username, password, localPath);
//	      int status = SFTP.getFile(remoteFileName, spanPath, serverName, username, password, localPath);
	      end = System.currentTimeMillis() - start;
	      if (1 == status) {
	        System.out.println("The time for get file (" + remoteFileName + ") : " + end);
	      } else if (status == 0) {
	        status_ = false;
	      }
	    }
	    catch (FileTransferException e)
	    {
	      e.printStackTrace();
	      status_ = false;
	    }
	    return status_;
	  }
	  
	  public static boolean deleteFileFTP(String nameOfFileToDelete, String serverName, String username, String password, String spanPath)
	  {
	    boolean status_ = true;
	    try
	    {
	      int status = FTP.deleteFile(nameOfFileToDelete, spanPath, serverName, username, password);
//	      int status = SFTP.deleteFile(nameOfFileToDelete, spanPath, serverName, username, password);
	      if (1 != status) {
	        if (status == 0) {
	          status_ = false;
	        }
	      }
	    }
	    catch (FileTransferException e)
	    {
	      e.printStackTrace();
	      status_ = false;
	    }
	    return status_;
	  }
	  
	  public static boolean copyFileFTP(String sourceFilePath, String destFilePath, String host, String username, String password)
	  {
		boolean status_ = true;
	    status_ = true;
	    try
	    {
	      int status = FTP.copyFile(sourceFilePath, destFilePath, host, username, password);
//	      int status = SFTP.copyFile(sourceFilePath, destFilePath, host, username, password);
	      if (1 == status) {}
	      return status_;
	    }
	    catch (FileTransferException e)
	    {
	      e.printStackTrace();
	    }
		return status_;
	  }
	  
	  public static void main(String[] args)
	  {
	    int n = Integer.parseInt(args[5]);
	    getFileNames(args[0], args[1], args[2], args[3], args[4], n);
	  }
}

