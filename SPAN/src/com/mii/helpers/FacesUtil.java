package com.mii.helpers;

import java.io.Closeable;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.MethodExpression;
import javax.el.ValueExpression;
import javax.faces.FacesException;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.context.Flash;
import javax.faces.event.MethodExpressionActionListener;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.mii.constant.Constants;
import com.mii.dao.MenuDao;
import com.mii.dao.SystemParameterDao;
import com.mii.models.User;
import com.mii.scoped.ApplicationBean;

/**
 * This is a helper class
 * @author DanangDj
 */
public class FacesUtil {
	/**
	 * Set error message to be displayed by h:messages
	 * @author DanangDj
	 * @param clientId
	 * @param summary
	 * @param detail
	 */
	public static void setErrorMessage(String clientId, String summary,
			String detail) {
		FacesContext.getCurrentInstance().addMessage(clientId,
				new FacesMessage(FacesMessage.SEVERITY_ERROR, summary, detail));
		getFlash().setKeepMessages(true);
		
	}
	
	public static void setErrorMessageDialog(String summary,
			String detail) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, summary, detail);
        
        RequestContext.getCurrentInstance().showMessageInDialog(message);
	}
	
	public static void setFatalMessage(String clientId, String summary,
			String detail) {
		FacesContext.getCurrentInstance().addMessage(clientId,
				new FacesMessage(FacesMessage.SEVERITY_FATAL, summary, detail));
		getFlash().setKeepMessages(true);
	}
	
	public static void setFatalMessageDialog( String summary,
			String detail) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_FATAL, summary, detail);
        
        RequestContext.getCurrentInstance().showMessageInDialog(message);
	}
	
	public static void setWarningMessage(String clientId, String summary,
			String detail) {
		FacesContext.getCurrentInstance().addMessage(clientId,
				new FacesMessage(FacesMessage.SEVERITY_WARN, summary, detail));
		getFlash().setKeepMessages(true);
		
	}
	
	public static void setWarningMessageDialog(String summary,
			String detail) {
		
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_WARN, summary, detail);
        
        RequestContext.getCurrentInstance().showMessageInDialog(message);
	}
	
	public static void setInfoMessage(String clientId, String summary,
			String detail) {
		FacesContext.getCurrentInstance().addMessage(clientId,
				new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail));
		getFlash().setKeepMessages(true);
	}
	
	public static void setInfoMessageDialog(String summary,
			String detail) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail);
        
        RequestContext.getCurrentInstance().showMessageInDialog(message);
	}
	
	public static void setApprovalWarning(String parameter, String parameterId, String status){
		String headerMessage = "Cannot perform this action";
		String bodyMessage = "Please contact the supervisor.";
		
		if(status.equals(Constants.APPROVAL_STATUS_PENDING_CREATE)){
			bodyMessage = "Create "+parameter+" &apos;"+parameterId+"&apos; is waiting for approval.<br/>" + bodyMessage;
		}else if(status.equals(Constants.APPROVAL_STATUS_REJECTED_CREATE)){
			bodyMessage = "Create "+parameter+" &apos;"+parameterId+"&apos; is rejected.<br/>" + bodyMessage;
		}else if(status.equals(Constants.APPROVAL_STATUS_PENDING_EDIT)){
			bodyMessage = "Edit "+parameter+" &quot;"+parameterId+"&quot; is waiting for approval.<br/>" + bodyMessage;
		}else if(status.equals(Constants.APPROVAL_STATUS_PENDING_DELETE)){
			bodyMessage = "Delete "+parameter+" &quot;"+parameterId+"&quot; is waiting for approval.<br/>" + bodyMessage;
		}
		setWarningMessageDialog(headerMessage, bodyMessage);
	}
	
	public static ExternalContext getExternalContext(){
		return FacesContext.getCurrentInstance().getExternalContext();
	}
	
	/**
	 * @author Oleg Varaksin {@link http
	 *         ://www.javacodegeeks.com/2012/04/5-useful
	 *         -methods-jsf-developers-should.html}
	 * @param beanName
	 * @return managed bean by the given name. The bean must be registered
	 *         either per faces-config.xml or annotation.
	 */
	public static Object getManagedBean(final String beanName, FacesContext fc) {
		if(fc == null){
			fc = FacesContext.getCurrentInstance();
		}
		Object bean;

		try {
			ELContext elContext = fc.getELContext();
			bean = elContext.getELResolver()
					.getValue(elContext, null, beanName);
		} catch (RuntimeException e) {
			throw new FacesException(e.getMessage(), e);
		}

		if (bean == null) {
			throw new FacesException(
					"Managed bean with name '"
							+ beanName
							+ "' was not found. Check your faces-config.xml or @ManagedBean annotation.");
		}

		return bean;
	}

	/**
	 * evaluate the given value expression  and sets the result to the given
	 * value. {@link http
	 * ://www.javacodegeeks.com/2012/04/5-useful-methods-jsf-developers
	 * -should.html}
	 * 
	 * @author Oleg Varaksin
	 * @param value
	 * @param expression
	 */
	public static void setValue2ValueExpression(final Object value,
			final String expression) {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ELContext elContext = facesContext.getELContext();

		ValueExpression targetExpression = facesContext.getApplication()
				.getExpressionFactory()
				.createValueExpression(elContext, expression, Object.class);
		targetExpression.setValue(elContext, value);
	}

	/**
	 * maps a variable to the given value expression 
	 * {@link http
	 * ://www.javacodegeeks.com/2012/04/5-useful-methods-jsf-developers
	 * -should.html}
	 * 
	 * @author Oleg Varaksin
	 * @param variable
	 * @param expression
	 */
	public static void mapVariable2ValueExpression(final String variable,
			final String expression) {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ELContext elContext = facesContext.getELContext();

		ValueExpression targetExpression = facesContext.getApplication()
				.getExpressionFactory()
				.createValueExpression(elContext, expression, Object.class);
		elContext.getVariableMapper().setVariable(variable, targetExpression);
	}

	/**
	 * create MethodExpression programmatically. 
	 * It is handy if you use component binding via "binding" attribute or create some model classes in Java.
	 * {@link http
	 * ://www.javacodegeeks.com/2012/04/5-useful-methods-jsf-developers
	 * -should.html}
	 * 
	 * @author Oleg Varaksin
	 * @param valueExpression
	 * @param expectedReturnType
	 * @param expectedParamTypes
	 * @return MethodExpression
	 */
	public static MethodExpression createMethodExpression(
			String valueExpression, Class<?> expectedReturnType,
			Class<?>[] expectedParamTypes) {
		MethodExpression methodExpression = null;
		try {
			FacesContext fc = FacesContext.getCurrentInstance();
			ExpressionFactory factory = fc.getApplication()
					.getExpressionFactory();
			methodExpression = factory.createMethodExpression(
					fc.getELContext(), valueExpression, expectedReturnType,
					expectedParamTypes);
		} catch (Exception e) {
			throw new FacesException("Method expression '" + valueExpression
					+ "' could not be created.");
		}

		return methodExpression;
	}

	/**
	 * create MethodExpressionActionListener programmatically. 
	 * It is handy if you use component binding via "binding" attribute or create some model classes in Java.
	 * {@link http
	 * ://www.javacodegeeks.com/2012/04/5-useful-methods-jsf-developers
	 * -should.html}
	 * 
	 * @author Oleg Varaksin
	 * @param valueExpression
	 * @param expectedReturnType
	 * @param expectedParamTypes
	 * @return
	 */
	public static MethodExpressionActionListener createMethodActionListener(
			String valueExpression, Class<?> expectedReturnType,
			Class<?>[] expectedParamTypes) {
		MethodExpressionActionListener actionListener = null;
		try {
			actionListener = new MethodExpressionActionListener(
					createMethodExpression(valueExpression, expectedReturnType,
							expectedParamTypes));
		} catch (Exception e) {
			throw new FacesException("Method expression for ActionListener '"
					+ valueExpression + "' could not be created.");
		}

		return actionListener;
	}

	/**
	 * method for concatenating strings using StringBuilder for performance sake
	 * @author DanangDj
	 * @param strings
	 * @return
	 */
	public static String concatString(String... strings){
		if(strings==null || strings.length==0){
			return null;
		}
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < strings.length; i++) {
			sb.append(strings[i]);
		}
		return sb.toString();
	}
	
	/**
	 * Method for split concatenated phone string (e.g. 021-6522234) 
	 * and assign it using reflection method 
	 * @author DanangDj
	 * @param phone
	 * @param phoneSeparator
	 * @param setAreaMethodName
	 * @param setPhoneMethodName
	 * @param clazz
	 * @param targetObj
	 */
	public static void setPhone(String phone, String phoneSeparator, String setAreaMethodName, 
			String setPhoneMethodName, Class<?> clazz, Object targetObj){
		if(!StringUtils.isBlank(phone)){
			String[] strings = phone.split(phoneSeparator);
			try {
				Class<?>[] argTypes = new Class[] { String.class };
				Method setAreaMethod = clazz.getDeclaredMethod(setAreaMethodName,argTypes);
				Method setPhoneMethod = clazz.getDeclaredMethod(setPhoneMethodName,argTypes);
				
				setAreaMethod.invoke(targetObj, strings[0]);
				if(strings.length>1){
					setPhoneMethod.invoke(targetObj, strings[1]);
				}
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Truncates the date passed as parameter at the field passed by parameter. It sets all fields 
	 * that are part of the field passed as parameter to their respective minimum value.
	 * For instance, if you truncate a date at the field {@link Calendar#DATE}, the HOUR_OF_DAY,
	 * MINUTE, SECOND and MILLISECOND parts of that date will be set to zero.
	 * 
	 * Valid fields that can be passed as parameter are: {@link Calendar#YEAR}, {@link Calendar#MONTH}, 
	 * {@link Calendar#DATE}, {@link Calendar#HOUR_OF_DAY}, {@link Calendar#MINUTE}, 
	 * {@link Calendar#SECOND}, and {@link Calendar#MILLISECOND}.
	 *  
	 *  @author Henrique Ordine (http://hordine.com/2012/05/truncate-java-util-date-by-java-util-calendar-field/)
	 * @param date
	 * @param field
	 * @return
	 */
	public static Date truncate(Date date, int field) {
		int[] fields = new int[] { Calendar.YEAR, Calendar.MONTH,
				Calendar.DATE, Calendar.HOUR_OF_DAY, Calendar.MINUTE,
				Calendar.SECOND, Calendar.MILLISECOND };
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		boolean truncate = false;
		for (int f : fields) {
			if (truncate) {
				cal.set(f, cal.getMinimum(f));
			}
			if (f == field) {
				truncate = true;
			}
		}
		return cal.getTime();
	}
	
	/**
	 * method untuk mengembalikan obj.toString dg null checking terlebih dahulu
	 * @param obj
	 * @return
	 */
	public static String getStringValue(Object obj){
		return (obj==null)?null:obj.toString();
	}
	
	/**
	 * method untuk mengembalikan nilai integer dg null checking terlebih dahulu
	 * @param str
	 * @return
	 */
	public static Integer getIntegerValue(String str){
		return (str==null)?null:Integer.valueOf(str);
	}
	
	public static BigDecimal getBigDecimalValue(String str){
		return (str==null)?null:new BigDecimal(str);
	}
	
	public static String dateToString(Date dateIn, String pattern) {
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		try {
			return sdf.format(dateIn);
		} catch (Exception e) {
			return null;
		}
		
	}
	
	public static Date stringToDate(String stringIn, String pattern) {
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		try {
			return sdf.parse(stringIn);
		} catch (ParseException e) {
			return null;
		}
	}
	
	public static ServletContext getServletContext(){
		return  (ServletContext) FacesContext
			    .getCurrentInstance().getExternalContext().getContext();
	}
	
	public static HttpServletResponse getHttpServletResponse(){
		return (HttpServletResponse)getExternalContext().getResponse();
	}
	
	public static ApplicationContext getApplicationContext(){
		ServletContext servletContext = FacesUtil.getServletContext();
		return WebApplicationContextUtils.getRequiredWebApplicationContext(
				servletContext);
	}

	public static Object getSpringBean(String id){
		ServletContext servletContext = FacesUtil.getServletContext();
		return WebApplicationContextUtils.getRequiredWebApplicationContext(
				servletContext).getBean(id);
	}
	
	public static Flash getFlash(){
		return FacesContext.getCurrentInstance().getExternalContext().getFlash();
	}
	
	public static List<String> getConstantValuesAsList(Class<?> clazz) {
		List<String> list = new ArrayList<String>();
		for (Field field : clazz.getDeclaredFields()) {
			int mod = field.getModifiers();
			if (Modifier.isStatic(mod) && Modifier.isPublic(mod)
					&& Modifier.isFinal(mod) 
					&& field.getType().equals(String.class)) {
				try {
					list.add((String) field.get(null));
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}
	
	public static void attachContentToResponse(byte[] content, String filename, String contentType){
		HttpServletResponse res = (HttpServletResponse) FacesContext
				.getCurrentInstance().getExternalContext().getResponse();
		res.setHeader("Content-Disposition", "attachment; filename=\""
				+ filename + "\"");//
		res.setHeader("Content-Transfer-Encoding", "binary");
		res.setContentType(contentType);
		
		try {
			res.flushBuffer();	
			OutputStream out = res.getOutputStream();
			try{
				out.write(content);
			}finally{
				close(out);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void close(Closeable closeable){
		if (closeable != null) {
			try {
				closeable.close();
			} catch (IOException e) {
				Logger.getLogger(FacesUtil.class).error(e);
			}			
		}
	}
	
	/**
	 * method untuk substring tanpa takut terjadi index out of bound
	 * @author DanangDj
	 * @param string
	 * @param begin
	 * @param end
	 * @return substring dari input string.
	 */
	public static String safeSubstring(String string, int begin, int end){
		if(string==null){
			return null;
		}
		if(end > string.length()){
			return new String(string.substring(begin));
		}
		return new String(string.substring(begin, end));
	}

	/**
	 * method ini akan error apabila thread yg memanggil method ini bukan berasal dari web request
	 * @author DanangDj
	 * @return resource bundle dg base name i18n
	 */
	public static ResourceBundle getBundle(FacesContext fc){
		if(fc==null){
			fc = FacesContext.getCurrentInstance();
		}
		return ResourceBundle.getBundle("i18n", fc.getViewRoot().getLocale());
	}
	
	public static void main(String... strings){
		//System.out.println(dateToString(new Date(), "DD"));
	}
	
	public static int convertToJulian(Date date){
	    Calendar calendar = Calendar.getInstance();
	    calendar.setTime(date);
	    int year = calendar.get(Calendar.YEAR);
	    String syear = String.format("%04d",year).substring(3);
	    int julian = Integer.parseInt(String.format("%s%03d",syear,calendar.get(Calendar.DAY_OF_YEAR)));
	    int i = calendar.get(Calendar.DAY_OF_YEAR);
	    int j = calendar.get(Calendar.HOUR);
	    return julian;
	}
	/**
	 * @author DanangDj
	 * @return default locale untuk jspo yaitu Indonesia
	 */
	public static Locale getDefaultLocale() {
		return new Locale("id");
	}
	
	public static void setValueIfNull(Object value, Object... objects) {
		for(Object obj : objects){
			if(obj==null){
				obj = value;
			}
		}
		
	}
	
	/** 
	 * chops a list into non-view sublists of length listLength
	 * @param list
	 * @param listLength
	 * @return choppedLists
	 */
	public static <T> List<List<T>> divideList(List<T> list, final int listLength) {
	    List<List<T>> parts = new ArrayList<List<T>>();
	    final int oriListLength = list.size();
	    for (int i = 0; i < oriListLength; i += listLength) {
	        parts.add(new ArrayList<T>(
	            list.subList(i, Math.min(oriListLength, i + listLength)))
	        );
	    }
	    return parts;
	}
	
	public static boolean isEmpty(String string){
		return (string ==null || string.trim().isEmpty());
	}

	public static void appendOnNotEmpty(StringBuilder sb,
			String checkedString, String appendedString) {
		if(! FacesUtil.isEmpty(checkedString)){
			sb.append(appendedString);
		}
	}
	
	public static String padLeft(String s, int n, String padString) {
	    return String.format("%1$" + n + "s", s).replaceAll(" ", padString);  
	}
	
	private static final String KEEP_DIALOG_OPENED = "KEEP_DIALOG_OPENED";

	public static void closeDialog(){
		getRequestContext().addCallbackParam(KEEP_DIALOG_OPENED, false);
	}
	
	public static void keepDialogOpen(){
		getRequestContext().addCallbackParam(KEEP_DIALOG_OPENED, true);
	}
	
	public static RequestContext getRequestContext(){
		return RequestContext.getCurrentInstance();
	}

	public static HttpSession getSession(boolean b) {
		return (HttpSession) FacesContext.getCurrentInstance()
				.getExternalContext().getSession(b);
	}

	public static String getClientIpAddress(){
		HttpServletRequest request = (HttpServletRequest) getExternalContext().getRequest();
		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
		    ipAddress = request.getRemoteAddr();
		}
		return ipAddress;
	}
	
	public static String getClientUserAgent(){
		return getExternalContext().getRequestHeaderMap().get("User-Agent");
	}
	
	public static void executeJs(String js){
		RequestContext.getCurrentInstance().execute(js);
	}
	
	public static void authMenu(User user, String menuPath){
		ApplicationBean applicationBean = (ApplicationBean) FacesUtil.getManagedBean("applicationBean", null);
		MenuDao menuDao = (MenuDao) applicationBean.getCorpGWContext().getBean("menuDAO");
		int auth = menuDao.getMenuAuthority(user.getUserID(), menuPath);
		if (auth == 0){
			try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("loginSPAN"+".jsf");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else{
			//NEW USER VALIDATION
			if(user.getFirstLogin().equalsIgnoreCase(Constants.FISTLOGINYES)){
				try {
					FacesContext.getCurrentInstance().getExternalContext().redirect("newPasswordSPAN"+".jsf");
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	
	public static void resetPage(String dataTable) {
		final DataTable d = (DataTable) FacesContext.getCurrentInstance().getViewRoot()
		    .findComponent(dataTable);
		int first = 0;
		d.setFirst(first);
	}
	
	public static void generateDownloadReport(byte[] report, String filename, String contentType){
		FacesContext facesContext = FacesContext.getCurrentInstance();
		OutputStream outputStream = null;
		try {
			HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
			response.setHeader("Content-Disposition", "attachment; filename="+ filename);
			response.setContentType(contentType);
			outputStream = response.getOutputStream();
			outputStream.write(report);
			outputStream.flush();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(outputStream!=null)outputStream.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		facesContext.responseComplete();
	}
}
