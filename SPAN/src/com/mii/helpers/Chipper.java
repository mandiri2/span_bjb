package com.mii.helpers;

import java.security.Key;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

public class Chipper {
	public static void main(String[] args) {
		String key = "tgadapterbjb2012";
//		String initVector = "RandomInitVector";
		String value = "json Message";
		System.out.println(encrypt(key, value));
	}
	public static final String DEFAULT_ENCODING = "UTF-8";
	static BASE64Encoder enc = new BASE64Encoder();
	
	public static String encrypt(String key, String value){
		try {
//            IvParameterSpec iv = new IvParameterSpec(initVector.getBytes(DEFAULT_ENCODING));
			
			byte[] keyb = key.getBytes("UTF-8");
			MessageDigest sha = MessageDigest.getInstance("SHA-1");
	        keyb = sha.digest(keyb);
	        keyb = Arrays.copyOf(keyb, 16); // use only first 128 bit
	        SecretKeySpec secretKeySpec = new SecretKeySpec(keyb, "AES");
	        
//            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(DEFAULT_ENCODING), "AES");
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
            
//            cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
            byte[] encrypted = cipher.doFinal(value.getBytes());
            
            return enc.encode(encrypted);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
	}
	
	
}
