package com.mii.helpers;

import java.io.Serializable;

import org.hibernate.Session;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

@SuppressWarnings("rawtypes")
public class BasicDaoImpl extends HibernateDaoSupport implements BasicDao{
	
	public Session getCurrentSession(){
		Session session = null;
	     try
	     {
	       session = getHibernateTemplate().getSessionFactory().getCurrentSession();
	       
	     } catch (Exception e) {
	    	 e.printStackTrace();
	     }
	     return session;
	  }
	
	public void save(Object obj) {
		getHibernateTemplate().saveOrUpdate(obj);
	}

	@Override
	public Object get(Class clazz, Serializable id) {
		return getHibernateTemplate().get(clazz, id);
	}

	@Override
	public void delete(Class clazz, Serializable id) {
		Object object = get(clazz, id);
		getHibernateTemplate().delete(object);
	}

	@Override
	public void update(Object obj) {
		getHibernateTemplate().update(obj);
	}

	@Override
	public void delete(Object object) {
		// TODO Auto-generated method stub
		getHibernateTemplate().delete(object);
	}
	
}
