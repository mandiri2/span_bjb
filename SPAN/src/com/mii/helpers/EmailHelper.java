package com.mii.helpers;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;

import java.util.Date;

import javax.mail.Message;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.mii.constant.Constants;
import com.mii.dao.SystemParameterDao;
import com.mii.models.SystemParameter;

public class EmailHelper {
	
	private static String subject;
	private static String body;
	
	/**
	   Outgoing Mail (SMTP) Server
	   requires TLS or SSL: smtp.gmail.com (use authentication)
	   Use Authentication: Yes
	   Port for SSL: 465
	 */
	public static void newUserMail(String username, String password, String toEmail, String status, SystemParameterDao systemParameterDao) {

		final String fromEmail = getParamValue(systemParameterDao, Constants.EMAIL_FROM_ADDRESS); //requires valid bjb id
		final String passwordEmail = getParamValue(systemParameterDao, Constants.EMAIL_FROM_PASSWORD); // correct password for bjb id - Tanpa password
		
		/*final String fromEmail = "zukorenaldi@gmail.com"; //requires valid gmail id
		final String passwordEmail = "equalzt17"; // correct password for gmail id
		*/
		
		System.out.println("BJB Email Start");
		Properties props = new Properties();
		props.put("mail.smtp.host", getParamValue(systemParameterDao, Constants.EMAIL_SMTP_HOST)); //SMTP Host
		props.put("mail.smtp.port", getParamValue(systemParameterDao, Constants.EMAIL_SMTP_PORT)); //SMTP Port
		
		/*System.out.println("SSLEmail Start");
		props.put("mail.smtp.host", "smtp.gmail.com"); //SMTP Host
		props.put("mail.smtp.socketFactory.port", "465"); //SSL Port
		props.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory"); //SSL Factory Class
		props.put("mail.smtp.auth", "true"); //Enabling SMTP Authentication
		props.put("mail.smtp.port", "465"); //SMTP Port
		*/
		
		Authenticator auth = new Authenticator() {
			//override the getPasswordAuthentication method
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(fromEmail, passwordEmail);
			}
		};
		
		if(status.equalsIgnoreCase(Constants.EMAILWAIT)){
			constructWaitApproveMessage(username);
		}else if(status.equalsIgnoreCase(Constants.EMAILAPPROVED)){
			constructNewUserMessage(username, password);
		}
		
		Session session = Session.getDefaultInstance(props, auth);
		System.out.println("Session created");
        sendEmail(session, toEmail, subject, body);
	}
	
	private static String getParamValue(SystemParameterDao systemParameterDao,
			String paramName) {
		SystemParameter systemParameter = systemParameterDao.getDetailedParameterByParamName(paramName);
		return systemParameter.getParamValue();
	}

	private static void constructWaitApproveMessage(String username) {
		subject = "[BJB]SPAN - New User";
//			String body = "Welcome, Here is your new Password : changeit";
		body = "Dear "+username+", "
				+ "<br/>"
				+ "<br/>"
				+ " Your New User Request is waiting for an Approval."
				+ "<br/>"
				+ "<br/>"
				+ "Best Regards,"
				+ "<br/>"
				+ "<br/>"
				+ "Automail SPAN-BJB";
	}

	private static void constructNewUserMessage(String username, String password) {
		subject = "[BJB]SPAN - New User";
//			String body = "Welcome, Here is your new Password : changeit";
		body = "Dear "+username+", "
				+ "<br/>"
				+ "<br/>"
				+ " Here is your new Password : <b>"+password+"</b>"
				+ "<br/>"
				+ "<br/>"
				+ "Best Regards,"
				+ "<br/>"
				+ "<br/>"
				+ "Automail SPAN-BJB";
	}
	
	/**
	 * Utility method to send simple HTML email
	 * @param session
	 * @param toEmail
	 * @param subject
	 * @param body
	 */
	private static void sendEmail(Session session, String toEmail, String subject, String body){
		try
	    {
	      MimeMessage msg = new MimeMessage(session);
	      //set message headers
	      msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
	      msg.addHeader("format", "flowed");
	      msg.addHeader("Content-Transfer-Encoding", "8bit");

	      msg.setSubject(subject, "UTF-8");
	      
	      /*
	       * Plain Text
	       */
//	      msg.setText(body, "UTF-8");
	      
	      /*
	       * Using HTML
	       */
	      msg.setContent(body, "text/html; charset=utf-8");

	      msg.setSentDate(new Date());

	      msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail, false));
	      System.out.println("Message is ready");
    	  Transport.send(msg);  

	      System.out.println("EMail Sent Successfully!!");
	    }
	    catch (Exception e) {
	      e.printStackTrace();
	    }
	}
	}