package com.mii.helpers;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

public class EncryptUtils {
    public static final String DEFAULT_ENCODING = "UTF-8"; 
    static BASE64Encoder enc = new BASE64Encoder();
    static BASE64Decoder dec = new BASE64Decoder();
    
	private static	String key = "keyformaskingbjbpassword";    
    
	//Password Parameter List
	public final static String PASSWORD_PARAM_LIST	= "WEB_SERVICE_PASSWORD|WS_Password_GetNoSakti|SFTP_PASS|SFTP_PASS_BPSX|PRIVATE_KEY_PASS|KEYSTORE_PASS";

	public static boolean isPasswordParam(String value){
		String[] value_split = PASSWORD_PARAM_LIST.split("\\|");
		boolean exist = false;
		for (String a : value_split){
			System.out.println(a);
			if(a.equals(value)){
				exist = true;
				break;
			}
		}
		return exist;
	}
	
	public static void main(String[] args) {
		String password = "P@ssw0rd";
		System.out.println(EncryptEncode(password));
		System.out.println(DecodeDecrypt(EncryptEncode(password)));
	}
	
	public static String EncryptEncode(String input){
        String encrypted = xorMessage(input, key);
//        System.out.println(encrypted);
        
        String encoded = base64encode(encrypted);
//        System.out.println(encoded);
        return encoded;
    }
   
    public static String DecodeDecrypt(String encoded){
      String decoded = base64decode(encoded);
//      System.out.println(decoded);
      
      String decrypted = xorMessage(decoded, key);
//      System.out.println(decrypted);
      return decrypted;
    }
	
    public static String base64encode(String text) {
        try {
            return enc.encode(text.getBytes(DEFAULT_ENCODING));
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }//base64encode

    public static String base64decode(String text) {
        try {
            return new String(dec.decodeBuffer(text), DEFAULT_ENCODING);
        } catch (IOException e) {
            return null;
        }
    }//base64decode
    
	public static String xorMessage(String message, String key) {
        try {
            if (message == null || key == null) return null;

            char[] keys = key.toCharArray();
            char[] mesg = message.toCharArray();

            int ml = mesg.length;
            int kl = keys.length;
            char[] newmsg = new char[ml];

            for (int i = 0; i < ml; i++) {
                newmsg[i] = (char)(mesg[i] ^ keys[i % kl]);
            }//for i

            return new String(newmsg);
        } catch (Exception e) {
            return null;
        }
    }//xorMessage
}//class
