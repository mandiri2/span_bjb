package com.mii.helpers;

import java.io.*;
import java.util.zip.*;

public class CreateZIPUtil {
	public static void zipFiles(File[] files, ZipOutputStream zos){
		try{
			byte[] buffer = new byte[1024];
			for (int i = 0; i < files.length; i++){
				FileInputStream in = new FileInputStream(files[i]);
				zos.putNextEntry(new ZipEntry(files[i].getName()));
				int len;
				while ((len = in.read(buffer)) > 0){
					zos.write(buffer, 0, len);
				}
			in.close();
			}
		}
		catch (IllegalArgumentException iae){
			iae.printStackTrace();
		}
		catch (FileNotFoundException fnfe){
			fnfe.printStackTrace();
		}
		catch (IOException ioe){
			ioe.printStackTrace();
		}
	}
}
