package com.mii.helpers;

import java.io.Serializable;

@SuppressWarnings("rawtypes")
public interface BasicDao {
	public Object get(Class clazz, Serializable id);
	
	public void delete(Class clazz, Serializable id);
	
	public void update(Object obj);
	
	public void save(Object obj);
	
	public void delete(Object object);
}
