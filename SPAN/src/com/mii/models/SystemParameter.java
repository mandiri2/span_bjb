package com.mii.models;

import java.io.Serializable;
import java.util.Date;

import com.mii.constant.Constants;

public class SystemParameter implements Serializable {
    private static final long serialVersionUID = 1L;
    private String paramId;
    private String paramName;
    private String paramValue;
    private String description;
    private Date insertDate;
    private Date updateDate;
    private String changeWho;
    private String createWho;
    private String isEnabled;
    private String status;
	private Boolean disableEdit;
	private Boolean disableDelete;
	
	private Date approvalDate;
	
	private String approvalBy;
    
    public SystemParameter() {}
	
	public SystemParameter(String paramId, String paramName, String paramValue, String description, Date insertDate, Date updateDate,
							String changeWho, String isEnabled, String status){
		this.paramId = paramId;
		this.paramName = paramName;
		this.paramValue = paramValue;
		this.description = description;
		this.insertDate = insertDate;
		this.updateDate = updateDate;
		this.changeWho = changeWho;
		this.isEnabled = isEnabled;
		this.status = status;
		
	}
    
	public String getParamId() {
		return paramId;
	}
	public void setParamId(String paramId) {
		this.paramId = paramId;
	}
	public String getParamName() {
		return paramName;
	}
	public void setParamName(String paramName) {
		this.paramName = paramName;
	}
	public String getParamValue() {
		return paramValue;
	}
	
	public Date getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}

	public String getApprovalBy() {
		return approvalBy;
	}

	public void setApprovalBy(String approvalBy) {
		this.approvalBy = approvalBy;
	}

	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getInsertDate() {
		return insertDate;
	}
	public void setInsertDate(Date insertDate) {
		this.insertDate = insertDate;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public String getChangeWho() {
		return changeWho;
	}
	public void setChangeWho(String changeWho) {
		this.changeWho = changeWho;
	}
	public String getIsEnabled() {
		return isEnabled;
	}
	public void setIsEnabled(String isEnabled) {
		this.isEnabled = isEnabled;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getCreateWho() {
		return createWho;
	}

	public void setCreateWho(String createWho) {
		this.createWho = createWho;
	}

	public Boolean getDisableEdit() {
		if (this.status.equals(Constants.APPROVAL_STATUS_PENDING_DELETE))
			return true;
		else{
			return false;
		}
	}

	public void setDisableEdit(Boolean disableEdit) {
		this.disableEdit = disableEdit;
	}

	public Boolean getDisableDelete() {
		if ((this.status.equals(Constants.APPROVAL_STATUS_PENDING_CREATE) || (this.status.equals(Constants.APPROVAL_STATUS_PENDING_EDIT))) || this.status.equals(Constants.APPROVAL_STATUS_PENDING_DELETE))
			return true;
		else{
			return false;
		}
	}

	public void setDisableDelete(Boolean disableDelete) {
		this.disableDelete = disableDelete;
	}
    
    
 }