package com.mii.models;

import java.io.Serializable;
import java.util.Date;

import com.mii.constant.Constants;

public class Branch implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String 	branchId;
	private String 	branchCode;
	private String 	branchName;
	private String 	description;
	private String 	status;
	private Date 	createDate;
	private String 	createWho;
	private Date 	updateDate;
	private String 	changeWho;
	private Boolean disableEdit;
	private Boolean disableDelete;
	
	private Date approvalDate;
	
	private String approvalBy;
	
	public Branch(){
		
	}
	
	public Branch(String branchId, String branchCode, String branchName, String description, String status){
		this.branchId = branchId;
		this.branchCode = branchCode;
		this.branchName = branchName;
		this.description = description;
		this.status = status;
	}
	
	public String getBranchId() {
		return branchId;
	}
	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	

	public Date getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}

	public String getApprovalBy() {
		return approvalBy;
	}

	public void setApprovalBy(String approvalBy) {
		this.approvalBy = approvalBy;
	}

	public String getCreateWho() {
		return createWho;
	}

	public void setCreateWho(String createWho) {
		this.createWho = createWho;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getChangeWho() {
		return changeWho;
	}

	public void setChangeWho(String changeWho) {
		this.changeWho = changeWho;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	public Boolean getDisableEdit() {
		if (this.status.equals(Constants.APPROVAL_STATUS_PENDING_DELETE))
			return true;
		else{
			return false;
		}
	}

	public void setDisableEdit(Boolean disableEdit) {
		this.disableEdit = disableEdit;
	}

	public Boolean getDisableDelete() {
		if ((this.status.equals(Constants.APPROVAL_STATUS_PENDING_CREATE) || (this.status.equals(Constants.APPROVAL_STATUS_PENDING_EDIT))) || this.status.equals(Constants.APPROVAL_STATUS_PENDING_DELETE))
			return true;
		else{
			return false;
		}
	}

	public void setDisableDelete(Boolean disableDelete) {
		this.disableDelete = disableDelete;
	}
	
}
