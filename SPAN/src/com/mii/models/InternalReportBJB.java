package com.mii.models;

import java.io.Serializable;

public class InternalReportBJB implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String no;
	private String sp2dNo;
	private String docDate;
	private String accountName;
	private String accountNumber;
	private String amount;
	private String branchCode;
	private String transactionDate;
	private String status;
	private String totalRecord;
	private String totalAmount;
	private String rejectDate;
	private String fileName;
	
	// helper
	private String errorCode;
	private String errorMessage;
	
	// additional BJB
	private String postingTime;
	private String bankName;
	private String incommingTime;
	
	public InternalReportBJB(){
		
	}
	
	public InternalReportBJB(String no, String fileName, String docDate, String totalRecord,
			String totalAmount, String rejectDate, String incommingTime) {
		super();
		this.no = no;
		this.fileName = fileName;
		this.docDate = docDate;
		this.totalRecord = totalRecord;
		this.totalAmount = totalAmount;
		this.rejectDate = rejectDate;
		this.incommingTime = incommingTime;
	}

	public InternalReportBJB(String no, String sp2dNo, String docDate,
			String accountName, String accountNumber, String amount,
			String transactionDate, String errorCode, String errorMessage) {
		super();
		this.no = no;
		this.sp2dNo = sp2dNo;
		this.docDate = docDate;
		this.accountName = accountName;
		this.accountNumber = accountNumber;
		this.amount = amount;
		this.branchCode = branchCode;
		this.transactionDate = transactionDate;
		this.status = status;
		this.totalRecord = totalRecord;
		this.totalAmount = totalAmount;
		this.rejectDate = rejectDate;
		this.transactionDate = transactionDate;
		this.errorCode = errorCode;
		if (this.errorCode.equals("0"))
			this.errorMessage = "SUCCESS";
		else
			this.errorMessage = errorMessage;
	}

	public InternalReportBJB(String no, String sp2dNo, String docDate, String postingTime,
			String accountName, String accountNumber, String amount,
			String branchCode, String bankName, String errorMessage) {
		super();
		this.no = no;
		this.sp2dNo = sp2dNo;
		this.docDate = docDate;
		this.postingTime = postingTime;
		this.accountName = accountName;
		this.accountNumber = accountNumber;
		this.amount = amount;
		this.branchCode = branchCode;
		this.bankName = bankName;
		this.errorMessage = errorMessage;

	}
	
/*	public InternalReportBJB(String sp2dNo, String docDate,
			String accountName, String accountNumber, String amount,
			String errorMessage) {
		super();
		
		this.sp2dNo = sp2dNo;
		this.docDate = docDate;
		this.accountName = accountName;
		this.accountNumber = accountNumber;
		this.amount = amount;
		this.errorMessage = errorMessage;

	}*/
	
	public InternalReportBJB(String no, String sp2dNo, String docDate,
			String accountName, String accountNumber,
			String errorMessage) {
		super();
		this.no = no;
		this.sp2dNo = sp2dNo;
		this.docDate = docDate;
		this.accountName = accountName;
		this.accountNumber = accountNumber;
		this.status = errorMessage;

	}
	
	public InternalReportBJB(String sp2dNo, String docDate,
			String accountName, String accountNumber, String amount) {
		super();
		
		this.sp2dNo = sp2dNo;
		this.docDate = docDate;
		this.accountName = accountName;
		this.accountNumber = accountNumber;
		this.amount = amount;

	}

	public String getSp2dNo() {
		return sp2dNo;
	}

	public void setSp2dNo(String sp2dNo) {
		this.sp2dNo = sp2dNo;
	}

	public String getDocDate() {
		return docDate;
	}

	public void setDocDate(String docDate) {
		this.docDate = docDate;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTotalRecord() {
		return totalRecord;
	}

	public void setTotalRecord(String totalRecord) {
		this.totalRecord = totalRecord;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getRejectDate() {
		return rejectDate;
	}

	public void setRejectDate(String rejectDate) {
		this.rejectDate = rejectDate;
	}

	public String getNo() {
		return no;
	}

	public void setNo(String no) {
		this.no = no;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getPostingTime() {
		return postingTime;
	}

	public void setPostingTime(String postingTime) {
		this.postingTime = postingTime;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getIncommingTime() {
		return incommingTime;
	}

	public void setIncommingTime(String incommingTime) {
		this.incommingTime = incommingTime;
	}
}
