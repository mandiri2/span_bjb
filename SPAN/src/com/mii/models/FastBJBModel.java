package com.mii.models;

public class FastBJBModel {
	private String name;
	private String extNumber;
	private String statusActive;
	
	public FastBJBModel(){
	}
	
	public FastBJBModel(String name, String extNumber, String statusActive) {
		super();
		this.name = name;
		this.extNumber = extNumber;
		this.statusActive = statusActive;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getExtNumber() {
		return extNumber;
	}
	public void setExtNumber(String extNumber) {
		this.extNumber = extNumber;
	}
	public String getStatusActive() {
		return statusActive;
	}
	public void setStatusActive(String statusActive) {
		this.statusActive = statusActive;
	}
	
}
