package com.mii.models;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class Menu implements Serializable, Comparable<Menu>{
	private static final long serialVersionUID = 1L;
	private int menuId;
	private String menuName;
	private String menuPath;
	/**
	 * Note: <br/>
	 * must filled. <br/>
	 * if it's root(have no parent) parentId = 0
	 */
	private int menuParentId;
	private int ordinal;
	private boolean action;
	private Set<Role> roles = new HashSet<Role>();
	
	/**
	 * not database field
	 */
	private boolean selected;
	
	public Menu() {
	}
	public Menu(Menu menu){
		this.menuId = menu.getMenuId();
		this.menuName = menu.getMenuName();
		this.action = menu.isAction();
		this.menuParentId = menu.getMenuParentId();
		this.menuPath = menu.getMenuPath();
		this.ordinal = menu.getOrdinal();
		this.roles = menu.getRoles();
	}
	public int getMenuId() {
		return menuId;
	}
	public void setMenuId(int menuId) {
		this.menuId = menuId;
	}
	public String getMenuName() {
		return menuName;
	}
	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}	
	public int getMenuParentId() {
		return menuParentId;
	}
	public void setMenuParentId(int menuParentId) {
		this.menuParentId = menuParentId;
	}
	public int getOrdinal() {
		return ordinal;
	}
	public void setOrdinal(int ordinal) {
		this.ordinal = ordinal;
	}	

	public String getMenuPath() {
		return menuPath;
	}
	public void setMenuPath(String menuPath) {
		this.menuPath = menuPath;
	}
	public boolean isAction() {
		return action;
	}
	public void setAction(boolean action) {
		this.action = action;
	}
	public Set<Role> getRoles() {
		return roles;
	}
	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	
	@Override
	public int compareTo(Menu otherMenu) {
		int compareOrdinal = otherMenu.getOrdinal();
		//ascending order
		return this.ordinal - compareOrdinal;
		 
		//descending order
		//return compareQuantity - this.quantity;
	}
	
}
