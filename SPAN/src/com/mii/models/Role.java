package com.mii.models;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.mii.constant.Constants;

public class Role implements Serializable {
	private static final long serialVersionUID = 1L;
	private String roleId;
	private String roleName;
	private Date createDate;
	private String createWho;
	private Date changeDate;
	private String changeWho;
	private Set<Menu> menus = new HashSet<Menu>();
	private String status;
	private Boolean disableEdit;
	private Boolean disableDelete;
	
	private Date approvalDate;
	
	private String approvalBy;
	
	private String uimRoleId;
	
	
	public Role() {}
	
	public Role(String roleId, String roleName, String status, String uimRoleId){
		this.roleId = roleId;
		this.roleName = roleName;
		this.status = status;
		this.uimRoleId = uimRoleId;
	}
	
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getCreateWho() {
		return createWho;
	}
	public void setCreateWho(String createWho) {
		this.createWho = createWho;
	}
	public Date getChangeDate() {
		return changeDate;
	}
	public void setChangeDate(Date changeDate) {
		this.changeDate = changeDate;
	}
	public String getChangeWho() {
		return changeWho;
	}
	public void setChangeWho(String changeWho) {
		this.changeWho = changeWho;
	}
	public Date getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}

	public String getApprovalBy() {
		return approvalBy;
	}

	public void setApprovalBy(String approvalBy) {
		this.approvalBy = approvalBy;
	}

	public Set<Menu> getMenus() {
		return menus;
	}
	public void setMenus(Set<Menu> menus) {
		this.menus = menus;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public Boolean getDisableEdit() {
		if (this.status.equals(Constants.APPROVAL_STATUS_PENDING_DELETE))
			return true;
		else{
			return false;
		}
	}

	public void setDisableEdit(Boolean disableEdit) {
		this.disableEdit = disableEdit;
	}

	public Boolean getDisableDelete() {
		if ((this.status.equals(Constants.APPROVAL_STATUS_PENDING_CREATE) || (this.status.equals(Constants.APPROVAL_STATUS_PENDING_EDIT))) || this.status.equals(Constants.APPROVAL_STATUS_PENDING_DELETE))
			return true;
		else{
			return false;
		}
	}

	public void setDisableDelete(Boolean disableDelete) {
		this.disableDelete = disableDelete;
	}

	public String getUimRoleId() {
		return uimRoleId;
	}

	public void setUimRoleId(String uimRoleId) {
		this.uimRoleId = uimRoleId;
	}
	
}
