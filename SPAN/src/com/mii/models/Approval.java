package com.mii.models;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Blob;

public class Approval implements Serializable  {
	
	private static final long serialVersionUID = 1L;
	private BigInteger approvalId;
	private String parameter;
	private String parameterId;
	private String parameterName;
	private String action;
	private byte[] oldByteObject;
	private byte[] newByteObject;
	private Blob   oldBlobObject;
	private Blob   newBlobObject;
	private boolean selected;
	private String approverId;
	private String branchId;
	
	public Approval() {
	}
	
	public Approval(BigInteger approvalId, String parameter, String parameterId, String parameterName, String action, byte[] oldByteObject, byte[] newByteObject) {
		this.approvalId 	= approvalId;
		this.parameter 		= parameter;
		this.parameterId 	= parameterId;
		this.parameterName 	= parameterName;
		this.action 		= action;
		this.oldByteObject	= oldByteObject;
		this.newByteObject	= newByteObject;
		
	}
	
	public BigInteger getApprovalId() {
		return approvalId;
	}
	public void setApprovalId(BigInteger approvalId) {
		this.approvalId = approvalId;
	}
	public String getParameter() {
		return parameter;
	}
	public void setParameter(String parameter) {
		this.parameter = parameter;
	}
	public String getParameterId() {
		return parameterId;
	}
	public void setParameterId(String parameterId) {
		this.parameterId = parameterId;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}

	public byte[] getOldByteObject() {
		return oldByteObject;
	}

	public void setOldByteObject(byte[] oldByteObject) {
		this.oldByteObject = oldByteObject;
	}

	public byte[] getNewByteObject() {
		return newByteObject;
	}

	public void setNewByteObject(byte[] newByteObject) {
		this.newByteObject = newByteObject;
	}

	public Blob getOldBlobObject() {
		return oldBlobObject;
	}

	public void setOldBlobObject(Blob oldBlobObject) {
		this.oldBlobObject = oldBlobObject;
	}

	public Blob getNewBlobObject() {
		return newBlobObject;
	}

	public void setNewBlobObject(Blob newBlobObject) {
		this.newBlobObject = newBlobObject;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public String getApproverId() {
		return approverId;
	}

	public void setApproverId(String approverId) {
		this.approverId = approverId;
	}

	public String getBranchId() {
		return branchId;
	}

	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}

	public String getParameterName() {
		return parameterName;
	}

	public void setParameterName(String parameterName) {
		this.parameterName = parameterName;
	}
	
}