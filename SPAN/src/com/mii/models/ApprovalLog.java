package com.mii.models;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

public class ApprovalLog implements Serializable  {
	
	private static final long serialVersionUID = 1L;
	private BigInteger approvalLogId;
	private BigInteger approvalId;
	private String parameter;
	private String parameterId;
	private String action;
	private String approverId;
	private String branchId;
	private Date   approveDate;
	private String statusCode;
	private String statusMessage;
	
	public ApprovalLog() {
		// TODO Auto-generated constructor stub
	}

	public BigInteger getApprovalLogId() {
		return approvalLogId;
	}

	public void setApprovalLogId(BigInteger approvalLogId) {
		this.approvalLogId = approvalLogId;
	}

	public BigInteger getApprovalId() {
		return approvalId;
	}

	public void setApprovalId(BigInteger approvalId) {
		this.approvalId = approvalId;
	}

	public String getParameter() {
		return parameter;
	}

	public void setParameter(String parameter) {
		this.parameter = parameter;
	}

	public String getParameterId() {
		return parameterId;
	}

	public void setParameterId(String parameterId) {
		this.parameterId = parameterId;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getApproverId() {
		return approverId;
	}

	public void setApproverId(String approverId) {
		this.approverId = approverId;
	}

	public String getBranchId() {
		return branchId;
	}

	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}

	public Date getApproveDate() {
		return approveDate;
	}

	public void setApproveDate(Date approveDate) {
		this.approveDate = approveDate;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	
	
}