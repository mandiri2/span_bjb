package com.mii.models;

import java.io.Serializable;
import java.util.Date;

public class SpanHostResponse implements Serializable {
	private static final long serialVersionUID = 1L;

	private String batchId,
		trxHeaderId,
		trxDetailId,
		debitAccNo,
		debitAcccurrCode,
		debitAmount,
		chargeInstruction,
		creditAccNo,
		creditAccName,
		creditTrfCurr,
		creditTrfAmount,
		trxRemark1,
		trxRemark2,
		trxRemark3,
		trxRemark4,
		ftServices,
		beneficiaryBankCode,
		beneficiaryBankName,
		debitRefNo,
		creditRefNo,
		organizationDirName,
		swiftMethod,
		responseCode,
		errorCode,
		errorMessage,
		remittanceNo,
		postingTimeStamp,
		tellerId,
		journalSequenceNo,
		reserve1,
		reserve2,
		reserve3,
		reserve4,
		reserve5,
		instructionCode1,
		responseFlag,
		legStatus,
		sorborNum,
		sorborStatus,
		bsStatus,
		docNumber;
	
	private Date insertTdt;
	
	/**GETTER SETTER**/
	public String getBatchId() {
		return batchId;
	}

	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}

	public String getTrxHeaderId() {
		return trxHeaderId;
	}

	public void setTrxHeaderId(String trxHeaderId) {
		this.trxHeaderId = trxHeaderId;
	}

	public String getTrxDetailId() {
		return trxDetailId;
	}

	public void setTrxDetailId(String trxDetailId) {
		this.trxDetailId = trxDetailId;
	}

	public String getDebitAccNo() {
		return debitAccNo;
	}

	public void setDebitAccNo(String debitAccNo) {
		this.debitAccNo = debitAccNo;
	}

	public String getDebitAcccurrCode() {
		return debitAcccurrCode;
	}

	public void setDebitAcccurrCode(String debitAcccurrCode) {
		this.debitAcccurrCode = debitAcccurrCode;
	}

	public String getDebitAmount() {
		return debitAmount;
	}

	public void setDebitAmount(String debitAmount) {
		this.debitAmount = debitAmount;
	}

	public String getChargeInstruction() {
		return chargeInstruction;
	}

	public void setChargeInstruction(String chargeInstruction) {
		this.chargeInstruction = chargeInstruction;
	}

	public String getCreditAccNo() {
		return creditAccNo;
	}

	public void setCreditAccNo(String creditAccNo) {
		this.creditAccNo = creditAccNo;
	}

	public String getCreditAccName() {
		return creditAccName;
	}

	public void setCreditAccName(String creditAccName) {
		this.creditAccName = creditAccName;
	}

	public String getCreditTrfCurr() {
		return creditTrfCurr;
	}

	public void setCreditTrfCurr(String creditTrfCurr) {
		this.creditTrfCurr = creditTrfCurr;
	}

	public String getCreditTrfAmount() {
		return creditTrfAmount;
	}

	public void setCreditTrfAmount(String creditTrfAmount) {
		this.creditTrfAmount = creditTrfAmount;
	}

	public String getTrxRemark1() {
		return trxRemark1;
	}

	public void setTrxRemark1(String trxRemark1) {
		this.trxRemark1 = trxRemark1;
	}

	public String getTrxRemark2() {
		return trxRemark2;
	}

	public void setTrxRemark2(String trxRemark2) {
		this.trxRemark2 = trxRemark2;
	}

	public String getTrxRemark3() {
		return trxRemark3;
	}

	public void setTrxRemark3(String trxRemark3) {
		this.trxRemark3 = trxRemark3;
	}

	public String getTrxRemark4() {
		return trxRemark4;
	}

	public void setTrxRemark4(String trxRemark4) {
		this.trxRemark4 = trxRemark4;
	}

	public String getFtServices() {
		return ftServices;
	}

	public void setFtServices(String ftServices) {
		this.ftServices = ftServices;
	}

	public String getBeneficiaryBankCode() {
		return beneficiaryBankCode;
	}

	public void setBeneficiaryBankCode(String beneficiaryBankCode) {
		this.beneficiaryBankCode = beneficiaryBankCode;
	}

	public String getBeneficiaryBankName() {
		return beneficiaryBankName;
	}

	public void setBeneficiaryBankName(String beneficiaryBankName) {
		this.beneficiaryBankName = beneficiaryBankName;
	}

	public String getDebitRefNo() {
		return debitRefNo;
	}

	public void setDebitRefNo(String debitRefNo) {
		this.debitRefNo = debitRefNo;
	}

	public String getCreditRefNo() {
		return creditRefNo;
	}

	public void setCreditRefNo(String creditRefNo) {
		this.creditRefNo = creditRefNo;
	}

	public String getOrganizationDirName() {
		return organizationDirName;
	}

	public void setOrganizationDirName(String organizationDirName) {
		this.organizationDirName = organizationDirName;
	}

	public String getSwiftMethod() {
		return swiftMethod;
	}

	public void setSwiftMethod(String swiftMethod) {
		this.swiftMethod = swiftMethod;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getRemittanceNo() {
		return remittanceNo;
	}

	public void setRemittanceNo(String remittanceNo) {
		this.remittanceNo = remittanceNo;
	}

	public String getPostingTimeStamp() {
		return postingTimeStamp;
	}

	public void setPostingTimeStamp(String postingTimeStamp) {
		this.postingTimeStamp = postingTimeStamp;
	}

	public String getTellerId() {
		return tellerId;
	}

	public void setTellerId(String tellerId) {
		this.tellerId = tellerId;
	}

	public String getJournalSequenceNo() {
		return journalSequenceNo;
	}

	public void setJournalSequenceNo(String journalSequenceNo) {
		this.journalSequenceNo = journalSequenceNo;
	}

	public String getReserve1() {
		return reserve1;
	}

	public void setReserve1(String reserve1) {
		this.reserve1 = reserve1;
	}

	public String getReserve2() {
		return reserve2;
	}

	public void setReserve2(String reserve2) {
		this.reserve2 = reserve2;
	}

	public String getReserve3() {
		return reserve3;
	}

	public void setReserve3(String reserve3) {
		this.reserve3 = reserve3;
	}

	public String getReserve4() {
		return reserve4;
	}

	public void setReserve4(String reserve4) {
		this.reserve4 = reserve4;
	}

	public String getReserve5() {
		return reserve5;
	}

	public void setReserve5(String reserve5) {
		this.reserve5 = reserve5;
	}

	public String getInstructionCode1() {
		return instructionCode1;
	}

	public void setInstructionCode1(String instructionCode1) {
		this.instructionCode1 = instructionCode1;
	}

	public String getResponseFlag() {
		return responseFlag;
	}

	public void setResponseFlag(String responseFlag) {
		this.responseFlag = responseFlag;
	}

	public String getLegStatus() {
		return legStatus;
	}

	public void setLegStatus(String legStatus) {
		this.legStatus = legStatus;
	}

	public String getSorborNum() {
		return sorborNum;
	}

	public void setSorborNum(String sorborNum) {
		this.sorborNum = sorborNum;
	}

	public String getSorborStatus() {
		return sorborStatus;
	}

	public void setSorborStatus(String sorborStatus) {
		this.sorborStatus = sorborStatus;
	}

	public Date getInsertTdt() {
		return insertTdt;
	}

	public void setInsertTdt(Date insertTdt) {
		this.insertTdt = insertTdt;
	}

	public String getBsStatus() {
		return bsStatus;
	}

	public void setBsStatus(String bsStatus) {
		this.bsStatus = bsStatus;
	}

	public String getDocNumber() {
		return docNumber;
	}

	public void setDocNumber(String docNumber) {
		this.docNumber = docNumber;
	}

}
