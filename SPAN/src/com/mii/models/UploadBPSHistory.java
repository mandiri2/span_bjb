package com.mii.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class UploadBPSHistory implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private BigDecimal id;
	private String fileName;
	private BigDecimal fileSize;
	private Date uploadDate;
	private String uploadBy;
	private String status;
	
	public UploadBPSHistory() {
		// TODO Auto-generated constructor stub
	}
	
	public UploadBPSHistory(BigDecimal id, String fileName, BigDecimal fileSize, Date uploadDate, String uploadBy, String status){
		this.id = id;
		this.fileName = fileName;
		this.fileSize = fileSize;
		this.uploadDate = uploadDate;
		this.uploadBy = uploadBy;
		this.status = status;
	}
	
	public BigDecimal getId() {
		return id;
	}
	public void setId(BigDecimal id) {
		this.id = id;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public BigDecimal getFileSize() {
		return fileSize;
	}
	public void setFileSize(BigDecimal fileSize) {
		this.fileSize = fileSize;
	}
	public Date getUploadDate() {
		return uploadDate;
	}
	public void setUploadDate(Date uploadDate) {
		this.uploadDate = uploadDate;
	}
	public String getUploadBy() {
		return uploadBy;
	}
	public void setUploadBy(String uploadBy) {
		this.uploadBy = uploadBy;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}
