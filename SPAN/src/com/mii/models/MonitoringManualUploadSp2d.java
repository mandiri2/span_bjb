package com.mii.models;

import java.io.Serializable;

public class MonitoringManualUploadSp2d implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String no;
	private String tanggal;
	
	//Sum : SP2D yg diupload//
	private String uTotalFile;
	private String uTotalNilai;
	private String uTotalSP2D;
	private String uTotalTransaksi;
	
	//Executed : SP2D yg sudah dieksekusi//
	private String eTotalFile;
	private String eTotalNilai;
	private String eTotalSP2D;
	private String eTotalTransaksi;
	
	//Wait : SP2D yg belum dieksekusi//
	private String iTotalFile;
	private String iTotalNilai;
	private String iTotalSP2D;
	private String iTotalTransaksi;
	
	//Constructor//
	public MonitoringManualUploadSp2d(String no, String tanggal,
			String uTotalFile, String uTotalNilai, String uTotalSP2D,
			String uTotalTransaksi, String eTotalFile, String eTotalNilai,
			String eTotalSP2D, String eTotalTransaksi, String iTotalFile,
			String iTotalNilai, String iTotalSP2D, String iTotalTransaksi) {
		super();
		this.no = no;
		this.tanggal = tanggal;
		this.uTotalFile = uTotalFile;
		this.uTotalNilai = uTotalNilai;
		this.uTotalSP2D = uTotalSP2D;
		this.uTotalTransaksi = uTotalTransaksi;
		this.eTotalFile = eTotalFile;
		this.eTotalNilai = eTotalNilai;
		this.eTotalSP2D = eTotalSP2D;
		this.eTotalTransaksi = eTotalTransaksi;
		this.iTotalFile = iTotalFile;
		this.iTotalNilai = iTotalNilai;
		this.iTotalSP2D = iTotalSP2D;
		this.iTotalTransaksi = iTotalTransaksi;
	}
	
	public String getNo() {
		return no;
	}
	public void setNo(String no) {
		this.no = no;
	}
	public String getTanggal() {
		return tanggal;
	}
	public void setTanggal(String tanggal) {
		this.tanggal = tanggal;
	}
	public String getuTotalFile() {
		return uTotalFile;
	}
	public void setuTotalFile(String uTotalFile) {
		this.uTotalFile = uTotalFile;
	}
	public String getuTotalNilai() {
		return uTotalNilai;
	}
	public void setuTotalNilai(String uTotalNilai) {
		this.uTotalNilai = uTotalNilai;
	}
	public String getuTotalSP2D() {
		return uTotalSP2D;
	}
	public void setuTotalSP2D(String uTotalSP2D) {
		this.uTotalSP2D = uTotalSP2D;
	}
	public String getuTotalTransaksi() {
		return uTotalTransaksi;
	}
	public void setuTotalTransaksi(String uTotalTransaksi) {
		this.uTotalTransaksi = uTotalTransaksi;
	}
	public String geteTotalFile() {
		return eTotalFile;
	}
	public void seteTotalFile(String eTotalFile) {
		this.eTotalFile = eTotalFile;
	}
	public String geteTotalNilai() {
		return eTotalNilai;
	}
	public void seteTotalNilai(String eTotalNilai) {
		this.eTotalNilai = eTotalNilai;
	}
	public String geteTotalSP2D() {
		return eTotalSP2D;
	}
	public void seteTotalSP2D(String eTotalSP2D) {
		this.eTotalSP2D = eTotalSP2D;
	}
	public String geteTotalTransaksi() {
		return eTotalTransaksi;
	}
	public void seteTotalTransaksi(String eTotalTransaksi) {
		this.eTotalTransaksi = eTotalTransaksi;
	}
	public String getiTotalFile() {
		return iTotalFile;
	}
	public void setiTotalFile(String iTotalFile) {
		this.iTotalFile = iTotalFile;
	}
	public String getiTotalNilai() {
		return iTotalNilai;
	}
	public void setiTotalNilai(String iTotalNilai) {
		this.iTotalNilai = iTotalNilai;
	}
	public String getiTotalSP2D() {
		return iTotalSP2D;
	}
	public void setiTotalSP2D(String iTotalSP2D) {
		this.iTotalSP2D = iTotalSP2D;
	}
	public String getiTotalTransaksi() {
		return iTotalTransaksi;
	}
	public void setiTotalTransaksi(String iTotalTransaksi) {
		this.iTotalTransaksi = iTotalTransaksi;
	}
	
}
