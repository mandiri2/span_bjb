package com.mii.models;

import java.math.BigDecimal;

public class SpanAccountHistory {
	private BigDecimal accountHistoryId;
	private String trxDate;
	private String trxSeqNo;
	private String trxTicketNo;
	private String trxDebitAcct;
	private String trxDebitName;
	private String trxCreditAcct;
	private String trxCreditName;
	private String trxDebitAmount;
	private String trxCreditAmount;
	private String trxAvailableBal;
	private String trxLedgerBal;
	private String trxHoldBal;
	private String trxFloatBal;
	private String trxRefNo;
	private String trxCode;
	private String trxUserId;
	private String trxRemNo;
	private String trxDesc01;
	private String trxDesc02;
	private String trxDesc03;
	private String trxDesc04;
	private String createDate;
	private String trxDebitCreditCode;
	private String trxCurrency;
	
	
	
	public SpanAccountHistory(String trxDate, String trxDesc01, String trxTicketNo,
			String trxDebitAmount, String trxCreditAmount) {
		super();
		this.trxDate = trxDate;
		this.trxDebitAmount = trxDebitAmount;
		this.trxCreditAmount = trxCreditAmount;
		this.trxTicketNo = trxTicketNo;
		this.trxDesc01 = trxDesc01;
	}
	
	public String getTrxDate() {
		return trxDate;
	}
	public void setTrxDate(String trxDate) {
		this.trxDate = trxDate;
	}
	public String getTrxSeqNo() {
		return trxSeqNo;
	}
	public void setTrxSeqNo(String trxSeqNo) {
		this.trxSeqNo = trxSeqNo;
	}
	public String getTrxTicketNo() {
		return trxTicketNo;
	}
	public void setTrxTicketNo(String trxTicketNo) {
		this.trxTicketNo = trxTicketNo;
	}
	public String getTrxDebitAcct() {
		return trxDebitAcct;
	}
	public void setTrxDebitAcct(String trxDebitAcct) {
		this.trxDebitAcct = trxDebitAcct;
	}
	public String getTrxDebitName() {
		return trxDebitName;
	}
	public void setTrxDebitName(String trxDebitName) {
		this.trxDebitName = trxDebitName;
	}
	public String getTrxCreditAcct() {
		return trxCreditAcct;
	}
	public void setTrxCreditAcct(String trxCreditAcct) {
		this.trxCreditAcct = trxCreditAcct;
	}
	public String getTrxCreditName() {
		return trxCreditName;
	}
	public void setTrxCreditName(String trxCreditName) {
		this.trxCreditName = trxCreditName;
	}
	public String getTrxDebitAmount() {
		return trxDebitAmount;
	}
	public void setTrxDebitAmount(String trxDebitAmount) {
		this.trxDebitAmount = trxDebitAmount;
	}
	public String getTrxCreditAmount() {
		return trxCreditAmount;
	}
	public void setTrxCreditAmount(String trxCreditAmount) {
		this.trxCreditAmount = trxCreditAmount;
	}
	public String getTrxAvailableBal() {
		return trxAvailableBal;
	}
	public void setTrxAvailableBal(String trxAvailableBal) {
		this.trxAvailableBal = trxAvailableBal;
	}
	public String getTrxLedgerBal() {
		return trxLedgerBal;
	}
	public void setTrxLedgerBal(String trxLedgerBal) {
		this.trxLedgerBal = trxLedgerBal;
	}
	public String getTrxHoldBal() {
		return trxHoldBal;
	}
	public void setTrxHoldBal(String trxHoldBal) {
		this.trxHoldBal = trxHoldBal;
	}
	public String getTrxFloatBal() {
		return trxFloatBal;
	}
	public void setTrxFloatBal(String trxFloatBal) {
		this.trxFloatBal = trxFloatBal;
	}
	public String getTrxRefNo() {
		return trxRefNo;
	}
	public void setTrxRefNo(String trxRefNo) {
		this.trxRefNo = trxRefNo;
	}
	public String getTrxCode() {
		return trxCode;
	}
	public void setTrxCode(String trxCode) {
		this.trxCode = trxCode;
	}
	public String getTrxUserId() {
		return trxUserId;
	}
	public void setTrxUserId(String trxUserId) {
		this.trxUserId = trxUserId;
	}
	public String getTrxRemNo() {
		return trxRemNo;
	}
	public void setTrxRemNo(String trxRemNo) {
		this.trxRemNo = trxRemNo;
	}
	public String getTrxDesc01() {
		return trxDesc01;
	}
	public void setTrxDesc01(String trxDesc01) {
		this.trxDesc01 = trxDesc01;
	}
	public String getTrxDesc02() {
		return trxDesc02;
	}
	public void setTrxDesc02(String trxDesc02) {
		this.trxDesc02 = trxDesc02;
	}
	public String getTrxDesc03() {
		return trxDesc03;
	}
	public void setTrxDesc03(String trxDesc03) {
		this.trxDesc03 = trxDesc03;
	}
	public String getTrxDesc04() {
		return trxDesc04;
	}
	public void setTrxDesc04(String trxDesc04) {
		this.trxDesc04 = trxDesc04;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getTrxDebitCreditCode() {
		return trxDebitCreditCode;
	}
	public void setTrxDebitCreditCode(String trxDebitCreditCode) {
		this.trxDebitCreditCode = trxDebitCreditCode;
	}
	public String getTrxCurrency() {
		return trxCurrency;
	}
	public void setTrxCurrency(String trxCurrency) {
		this.trxCurrency = trxCurrency;
	}
	public BigDecimal getAccountHistoryId() {
		return accountHistoryId;
	}
	public void setAccountHistoryId(BigDecimal accountHistoryId) {
		this.accountHistoryId = accountHistoryId;
	}
	
}
