package com.mii.models;

import java.io.Serializable;
import java.util.Date;

import com.mii.constant.Constants;

public class Holiday implements Serializable  {
	
	private static final long serialVersionUID = 1L;
	
	private String holidayId;
	
	private String holidayName;
	
	private String description;
	
	private String holidayDate;
	
	private Date _holidayDate;
	
	private String status;
	
	private Date createDate;
	
	private String createWho;
	
	private Date updateDate;
	
	private String changeWho;
	
	private Date approvalDate;
	
	private String approvalBy;
	
	private Boolean disableEdit;
	private Boolean disableDelete;
	
	public Holiday(String holidayId, String holidayName, String description, String holidayDate, String status) {
		// TODO Auto-generated constructor stub
		this.holidayId = holidayId;
		this.holidayName = holidayName;
		this.description = description;
		this.holidayDate = holidayDate;
		this.status = status;
	}
	
	public Holiday() {
		// TODO Auto-generated constructor stub
	}

	public String getHolidayId() {
		return holidayId;
	}

	public void setHolidayId(String holidayId) {
		this.holidayId = holidayId;
	}

	public String getHolidayName() {
		return holidayName;
	}

	public void setHolidayName(String holidayName) {
		this.holidayName = holidayName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getHolidayDate() {
		return holidayDate;
	}

	public void setHolidayDate(String holidayDate) {
		this.holidayDate = holidayDate;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateWho() {
		return createWho;
	}

	public void setCreateWho(String createWho) {
		this.createWho = createWho;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getChangeWho() {
		return changeWho;
	}

	public void setChangeWho(String changeWho) {
		this.changeWho = changeWho;
	}

	public Date get_holidayDate() {
		return _holidayDate;
	}

	public void set_holidayDate(Date _holidayDate) {
		this._holidayDate = _holidayDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
	public Date getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}

	public String getApprovalBy() {
		return approvalBy;
	}

	public void setApprovalBy(String approvalBy) {
		this.approvalBy = approvalBy;
	}

	public Boolean getDisableEdit() {
		if (this.status.equals(Constants.APPROVAL_STATUS_PENDING_DELETE))
			return true;
		else{
			return false;
		}
	}

	public void setDisableEdit(Boolean disableEdit) {
		this.disableEdit = disableEdit;
	}

	public Boolean getDisableDelete() {
		if ((this.status.equals(Constants.APPROVAL_STATUS_PENDING_CREATE) || (this.status.equals(Constants.APPROVAL_STATUS_PENDING_EDIT))) || this.status.equals(Constants.APPROVAL_STATUS_PENDING_DELETE))
			return true;
		else{
			return false;
		}
	}

	public void setDisableDelete(Boolean disableDelete) {
		this.disableDelete = disableDelete;
	}
}
