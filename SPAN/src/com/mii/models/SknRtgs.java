package com.mii.models;

import java.io.Serializable;

public class SknRtgs implements Serializable {
	private static final long serialVersionUID = 1L;

	private String 
		docNo,
		creditAccName,
		creditAccNo,
		beneficiaryBankName,
		creditTrfAmount,
		remittanceNo,
		sorborNo,
		transactionType,
		returReason,
		returReasonMsg;
	private boolean doUpdatable;
	
	public SknRtgs(){
		
	}
	
	public SknRtgs(String docNo,String creditAccName,String creditAccNo,String beneficiaryBankName,
			String creditTrfAmount,String remittanceNo,String sorborNo,String transactionType, String returReason){
		this.docNo=docNo;
		this.creditAccName=creditAccName;
		this.creditAccNo = creditAccNo;
		this.beneficiaryBankName=beneficiaryBankName;
		this.creditTrfAmount=creditTrfAmount;
		this.remittanceNo=remittanceNo;
		this.sorborNo=sorborNo;
		setTransactionType(transactionType);
		setDoUpdatable(remittanceNo,sorborNo);
		this.returReason=returReason;
	}
	
	/**GETTER SETTER**/


	public String getCreditAccNo() {
		return creditAccNo;
	}

	

	public void setCreditAccNo(String creditAccNo) {
		this.creditAccNo = creditAccNo;
	}
	
	public String getBeneficiaryBankName() {
		return beneficiaryBankName;
	}

	public void setBeneficiaryBankName(String beneficiaryBankName) {
		this.beneficiaryBankName = beneficiaryBankName;
	}

	public String getCreditAccName() {
		return creditAccName;
	}

	public void setCreditAccName(String creditAccName) {
		this.creditAccName = creditAccName;
	}

	
	public String getCreditTrfAmount() {
		return creditTrfAmount;
	}

	public void setCreditTrfAmount(String creditTrfAmount) {
		this.creditTrfAmount = creditTrfAmount;
	}

	public String getRemittanceNo() {
		return remittanceNo;
	}

	public void setRemittanceNo(String remittanceNo) {
		this.remittanceNo = remittanceNo;
	}

	public String getsorborNo() {
		return sorborNo;
	}

	public void setsorborNo(String sorborNo) {
		this.sorborNo = sorborNo;
	}

	public String getDocNo() {
		return docNo;
	}

	public void setDocNo(String docNo) {
		this.docNo = docNo;
	}

	public String getTransactionType() {
		return transactionType;
	}
	
	public void setTransactionType(String transactionType) {
		switch (transactionType.toUpperCase()) {
		case "0":
			this.transactionType = "Overbooking";
			break;
		case "1":
			this.transactionType = "RTGS";
			break;
		case "2":
			this.transactionType = "SKN";
			break;
		case "OVERBOOKING":
			this.transactionType = "Overbooking";
			break;
		case "SKN":
			this.transactionType = "RTGS";
			break;
		case "RTGS":
			this.transactionType = "SKN";
			break;
		}
	}


	public boolean isDoUpdatable() {
		return doUpdatable;
	}

	public void setDoUpdatable(String remittanceNo ,String sorborNo) {
		if(remittanceNo.equals("")||sorborNo.equals("")){
			this.doUpdatable = false;
		}else{
			this.doUpdatable = true;
		}
	}

	public String getReturReason() {
		return returReason;
	}

	public void setReturReason(String returReason) {
		this.returReason = returReason;
	}

	public String getReturReasonMsg() {
		return returReasonMsg;
	}

	public void setReturReasonMsg(String returReasonMsg) {
		this.returReasonMsg = returReasonMsg;
	}

	
	
	
}
