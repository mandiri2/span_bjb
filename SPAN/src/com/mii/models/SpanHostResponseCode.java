package com.mii.models;

import java.io.Serializable;

public class SpanHostResponseCode implements Serializable {
	private static final long serialVersionUID = 1L;

	public String RESPONSE_CODE,DESCRIPTION,RC_ID,ERROR_MESSAGE;

	public String getRESPONSE_CODE() {
		return RESPONSE_CODE;
	}
	
	public void setRESPONSE_CODE(String rESPONSE_CODE) {
		RESPONSE_CODE = rESPONSE_CODE;
	}
	
	public String getDESCRIPTION() {
		return DESCRIPTION;
	}
	
	public void setDESCRIPTION(String dESCRIPTION) {
		DESCRIPTION = dESCRIPTION;
	}
	
	public String getRC_ID() {
		return RC_ID;
	}
	
	public void setRC_ID(String rC_ID) {
		RC_ID = rC_ID;
	}
	
	public String getERROR_MESSAGE() {
		return ERROR_MESSAGE;
	}
	
	public void setERROR_MESSAGE(String eRROR_MESSAGE) {
		ERROR_MESSAGE = eRROR_MESSAGE;
	}
 
}
