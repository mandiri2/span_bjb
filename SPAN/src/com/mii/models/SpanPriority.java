package com.mii.models;

import java.io.Serializable;

public class SpanPriority implements Serializable {

	private static final long serialVersionUID = -6977229172371451293L;
	
	private String sp2dno;
	
	private String reason;
	
	private Integer level;
	
	public String getSp2dno() {
		return sp2dno;
	}
	public void setSp2dno(String sp2dno) {
		this.sp2dno = sp2dno;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public Integer getLevel() {
		return level;
	}
	public void setLevel(Integer level) {
		this.level = level;
	}
	
}
