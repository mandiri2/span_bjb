package com.mii.models;

import java.io.Serializable;

public class SpanVoidDataTrx implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String fileName;
	private String documentNo;
	private String documentDate;
	private String beneficiaryName;
	private String beneficiaryBankCode;
	private String beneficiaryBank;
	private String beneficiaryAccount;
	private String amount;
	private String description;
	private String agentBankCode;
	private String agentBankAccountNo;
	private String agentBankAccountName;
	private String paymentMethod;
	private String sp2dCount;
	private String reasonToVoid;
	private String createDate;
	private String status;
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getDocumentNo() {
		return documentNo;
	}
	public void setDocumentNo(String documentNo) {
		this.documentNo = documentNo;
	}
	public String getDocumentDate() {
		return documentDate;
	}
	public void setDocumentDate(String documentDate) {
		this.documentDate = documentDate;
	}
	public String getBeneficiaryName() {
		return beneficiaryName;
	}
	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}
	public String getBeneficiaryBankCode() {
		return beneficiaryBankCode;
	}
	public void setBeneficiaryBankCode(String beneficiaryBankCode) {
		this.beneficiaryBankCode = beneficiaryBankCode;
	}
	public String getBeneficiaryBank() {
		return beneficiaryBank;
	}
	public void setBeneficiaryBank(String beneficiaryBank) {
		this.beneficiaryBank = beneficiaryBank;
	}
	public String getBeneficiaryAccount() {
		return beneficiaryAccount;
	}
	public void setBeneficiaryAccount(String beneficiaryAccount) {
		this.beneficiaryAccount = beneficiaryAccount;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getAgentBankCode() {
		return agentBankCode;
	}
	public void setAgentBankCode(String agentBankCode) {
		this.agentBankCode = agentBankCode;
	}
	public String getAgentBankAccountNo() {
		return agentBankAccountNo;
	}
	public void setAgentBankAccountNo(String agentBankAccountNo) {
		this.agentBankAccountNo = agentBankAccountNo;
	}
	public String getAgentBankAccountName() {
		return agentBankAccountName;
	}
	public void setAgentBankAccountName(String agentBankAccountName) {
		this.agentBankAccountName = agentBankAccountName;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public String getSp2dCount() {
		return sp2dCount;
	}
	public void setSp2dCount(String sp2dCount) {
		this.sp2dCount = sp2dCount;
	}
	public String getReasonToVoid() {
		return reasonToVoid;
	}
	public void setReasonToVoid(String reasonToVoid) {
		this.reasonToVoid = reasonToVoid;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
