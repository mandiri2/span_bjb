package com.mii.models;

public class SpanSummariesOutput {
	private String fileName;
	private String totalRecord;
	private String totalWaiting;
	private String totalInProgress;
	private String totalRetry;
	private String totalRetur;
	private String totalSuccess;
	private String totalAckRetur;
	private String totalAckSuccess;
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getTotalRecord() {
		return totalRecord;
	}
	public void setTotalRecord(String totalRecord) {
		this.totalRecord = totalRecord;
	}
	public String getTotalWaiting() {
		return totalWaiting;
	}
	public void setTotalWaiting(String totalWaiting) {
		this.totalWaiting = totalWaiting;
	}
	public String getTotalInProgress() {
		return totalInProgress;
	}
	public void setTotalInProgress(String totalInProgress) {
		this.totalInProgress = totalInProgress;
	}
	public String getTotalRetry() {
		return totalRetry;
	}
	public void setTotalRetry(String totalRetry) {
		this.totalRetry = totalRetry;
	}
	public String getTotalRetur() {
		return totalRetur;
	}
	public void setTotalRetur(String totalRetur) {
		this.totalRetur = totalRetur;
	}
	public String getTotalSuccess() {
		return totalSuccess;
	}
	public void setTotalSuccess(String totalSuccess) {
		this.totalSuccess = totalSuccess;
	}
	public String getTotalAckRetur() {
		return totalAckRetur;
	}
	public void setTotalAckRetur(String totalAckRetur) {
		this.totalAckRetur = totalAckRetur;
	}
	public String getTotalAckSuccess() {
		return totalAckSuccess;
	}
	public void setTotalAckSuccess(String totalAckSuccess) {
		this.totalAckSuccess = totalAckSuccess;
	}
	
}
