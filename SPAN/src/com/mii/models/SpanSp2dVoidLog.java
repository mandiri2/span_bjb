package com.mii.models;

import java.io.Serializable;

public class SpanSp2dVoidLog implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5359924615220842312L;

	private String spanSp2dVoidLog;
	private String filename;
	private String sp2dNo;
	private String datetimeVoid;
	public String getSpanSp2dVoidLog() {
		return spanSp2dVoidLog;
	}
	public void setSpanSp2dVoidLog(String spanSp2dVoidLog) {
		this.spanSp2dVoidLog = spanSp2dVoidLog;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getSp2dNo() {
		return sp2dNo;
	}
	public void setSp2dNo(String sp2dNo) {
		this.sp2dNo = sp2dNo;
	}
	public String getDatetimeVoid() {
		return datetimeVoid;
	}
	public void setDatetimeVoid(String datetimeVoid) {
		this.datetimeVoid = datetimeVoid;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
