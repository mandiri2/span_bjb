package com.mii.models;

import java.io.Serializable;

public class SpanDataReportMonitoring implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String ACK_fileName;
	private String ACK_ACKFilename;
	private String ACK_totalRecord;
	private String ACK_totalAmount;
	private String ACK_dateSent;
	private String ACK_status;
	private String BS_account;
	private String BS_fileName;
	private String BS_status;
	private String SORBOR_fileName;
	private String SORBOR_totalRecord;
	private String SORBOR_totalAmount;
	private String SORBOR_dateSent;
	private String SORBOR_status;
	
	private boolean selected;
	
	public String getACK_fileName() {
		return ACK_fileName;
	}
	public void setACK_fileName(String aCK_fileName) {
		ACK_fileName = aCK_fileName;
	}
	public String getACK_ACKFilename() {
		return ACK_ACKFilename;
	}
	public void setACK_ACKFilename(String aCK_ACKFilename) {
		ACK_ACKFilename = aCK_ACKFilename;
	}
	public String getACK_totalRecord() {
		return ACK_totalRecord;
	}
	public void setACK_totalRecord(String aCK_totalRecord) {
		ACK_totalRecord = aCK_totalRecord;
	}
	public String getACK_totalAmount() {
		return ACK_totalAmount;
	}
	public void setACK_totalAmount(String aCK_totalAmount) {
		ACK_totalAmount = aCK_totalAmount;
	}
	public String getACK_dateSent() {
		return ACK_dateSent;
	}
	public void setACK_dateSent(String aCK_dateSent) {
		ACK_dateSent = aCK_dateSent;
	}
	public String getACK_status() {
		return ACK_status;
	}
	public void setACK_status(String aCK_status) {
		ACK_status = aCK_status;
	}
	public String getBS_account() {
		return BS_account;
	}
	public void setBS_account(String bS_account) {
		BS_account = bS_account;
	}
	public String getBS_fileName() {
		return BS_fileName;
	}
	public void setBS_fileName(String bS_fileName) {
		BS_fileName = bS_fileName;
	}
	public String getSORBOR_fileName() {
		return SORBOR_fileName;
	}
	public void setSORBOR_fileName(String sORBOR_fileName) {
		SORBOR_fileName = sORBOR_fileName;
	}
	public String getSORBOR_totalRecord() {
		return SORBOR_totalRecord;
	}
	public void setSORBOR_totalRecord(String sORBOR_totalRecord) {
		SORBOR_totalRecord = sORBOR_totalRecord;
	}
	public String getSORBOR_totalAmount() {
		return SORBOR_totalAmount;
	}
	public void setSORBOR_totalAmount(String sORBOR_totalAmount) {
		SORBOR_totalAmount = sORBOR_totalAmount;
	}
	public String getSORBOR_dateSent() {
		return SORBOR_dateSent;
	}
	public void setSORBOR_dateSent(String sORBOR_dateSent) {
		SORBOR_dateSent = sORBOR_dateSent;
	}
	public String getSORBOR_status() {
		return SORBOR_status;
	}
	public void setSORBOR_status(String sORBOR_status) {
		SORBOR_status = sORBOR_status;
	}
	public String getBS_status() {
		return BS_status;
	}
	public void setBS_status(String bS_status) {
		BS_status = bS_status;
	}
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	
	
	
}
