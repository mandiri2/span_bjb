package com.mii.models;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

public class SpanMonitoringVoidDepKeu implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String fileName;
	private String sp2dNumber;
	private String debitAccountNo;
	private String debitAccountType;
	private String documentDate;
	private String totalAmount;
	private String totalRecord;
	private String procStatusFile;
	private String procStatusSp2dNo;
	private String description;
	private String descriptionOfUpdate;
	private Date createDate;
	private Date updateDate;
	private String voidFlag;
	private String procStatus;
	
	private boolean selected;
	
	private boolean voidStatus;
	
	public SpanMonitoringVoidDepKeu(String fileName,
			String sp2dNumber, String debitAccountNo, String debitAccountType,
			String documentDate, String totalAmount, String totalRecord,
			String procStatusFile, String procStatusSp2dNo, String description) {
		super();
		this.fileName = fileName;
		this.sp2dNumber = sp2dNumber;
		this.debitAccountNo = debitAccountNo;
		this.debitAccountType = debitAccountType;
		this.documentDate = documentDate;
		this.totalAmount = totalAmount;
		this.totalRecord = totalRecord;
		this.procStatusFile = procStatusFile;
		this.procStatusSp2dNo = procStatusSp2dNo;
		this.description = description;
	}
	public SpanMonitoringVoidDepKeu() {
	}
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getSp2dNumber() {
		return sp2dNumber;
	}
	public void setSp2dNumber(String sp2dNumber) {
		this.sp2dNumber = sp2dNumber;
	}
	public String getDebitAccountNo() {
		return debitAccountNo;
	}
	public void setDebitAccountNo(String debitAccountNo) {
		this.debitAccountNo = debitAccountNo;
	}
	public String getDebitAccountType() {
		return debitAccountType;
	}
	public void setDebitAccountType(String debitAccountType) {
		this.debitAccountType = debitAccountType;
	}
	public String getDocumentDate() {
		return documentDate;
	}
	public void setDocumentDate(String documentDate) {
		this.documentDate = documentDate;
	}
	public String getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}
	public String getTotalRecord() {
		return totalRecord;
	}
	public void setTotalRecord(String totalRecord) {
		this.totalRecord = totalRecord;
	}
	public String getProcStatusFile() {
		return procStatusFile;
	}
	public void setProcStatusFile(String procStatusFile) {
		this.procStatusFile = procStatusFile;
	}
	public String getProcStatusSp2dNo() {
		return procStatusSp2dNo;
	}
	public void setProcStatusSp2dNo(String procStatusSp2dNo) {
		this.procStatusSp2dNo = procStatusSp2dNo;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDescriptionOfUpdate() {
		return descriptionOfUpdate;
	}
	public void setDescriptionOfUpdate(String descriptionOfUpdate) {
		this.descriptionOfUpdate = descriptionOfUpdate;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public String getVoidFlag() {
		return voidFlag;
	}
	public void setVoidFlag(String voidFlag) {
		this.voidFlag = voidFlag;
	}
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	public boolean isVoidStatus() {
		return voidStatus;
	}
	public void setVoidStatus(boolean voidStatus) {
		this.voidStatus = voidStatus;
	}
	public String getProcStatus() {
		return procStatus;
	}
	public void setProcStatus(String procStatus) {
		this.procStatus = procStatus;
	}
	
	
	
}
