package com.mii.models;

import java.io.Serializable;
import java.util.Date;

public class ReportRekKoranM implements Serializable {
	private static final long serialVersionUID = 1L;

	
	private String Id,bsFileName,tanggalBS,currency;
	private Date tglgabung;
	
	
	public ReportRekKoranM(){
		
	}
	public ReportRekKoranM(String Id, String bsFileName,String tanggalBS){
		this.Id = Id;
		this.bsFileName = bsFileName;
		this.tanggalBS = tanggalBS;
	}


	public String getId() {
		return Id;
	}


	public void setId(String id) {
		Id = id;
	}


	public String getBsFileName() {
		return bsFileName;
	}


	public void setBsFileName(String bsFileName) {
		this.bsFileName = bsFileName;
	}
	


	public String getTanggalBS() {
		return tanggalBS;
	}


	public void setTanggalBS(String tanggalBS) {
		this.tanggalBS = tanggalBS;
	}


	public String getCurrency() {
		return currency;
	}


	public void setCurrency(String currency) {
		this.currency = currency;
	}


	public Date getTglgabung() {
		return tglgabung;
	}


	public void setTglgabung(Date tglgabung) {
		this.tglgabung = tglgabung;
	}
	
	
	}