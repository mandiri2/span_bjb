package com.mii.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class SPANHostDataDetails implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private BigDecimal detailId;
	private String batchId;
	private String trxHeaderId;
	private String trxDetailId;
	private Date createDate;
	private String messageId;
	private String messageType;
	private String documentDate;
	private String documentNumber;
	private String benName;
	private String benBankCode;
	private String benBank;
	private String benAccount;
	private String amount;
	private String currency;
	private String description;
	private String agentBankCode;
	private String agentBankAccNo;
	private String agentBankAccName;
	private String emailAddress;
	private String swiftCode;
	private String ibanCode;
	private String paymentMethods;
	private String spanCount;
	private String totalAccount;
	private String totalAmount;
	private String totalBatchAccount;
	private String transactionRef;
	private String providerRef;
	private String debitAccNo;
	private String debitAccExRate;
	private String debitAccCurrCode;
	private String chargeInstruction;
	private String remitterResFlag;
	private String remitterResCode;
	private String remitterCitizenFlag;
	private String remitterCitizenCode;
	private String creditAccNo;
	private String creditAccName;
	private String creditAccExRate;
	private String creditTrfCurr;
	private String creditTrfAmmount;
	private String trxRemark1;
	private String trxRemark2;
	private String trxRemark3;
	private String trxRemark4;
	private String ftServices;
	private String chargeCurrCode1;
	private String chargeAmount1;
	private String chargeCurrCode2;
	private String chargeAmount2;
	private String chargeCurrCode3;
	private String chargeAmount3;
	private String chargeCurrCode4;
	private String chargeAmount4;
	private String chargeCurrCode5;
	private String chargeAmount5;
	private String beneficiaryAddress1;
	private String beneficiaryAddress2;
	private String beneficiaryAddress3;
	private String beneficiaryBankCode;
	private String beneficiaryBankName;
	private String beneficiaryBankBranchName;
	private String beneficiaryBankAddress1;
	private String beneficiaryBankAddress2;
	private String beneficiaryBankAddress3;
	private String beneficiaryBankCityName;
	private String beneficiaryBankCountryName;
	private String beneficiaryResFlag;
	private String beneficiaryResCode;
	private String beneficiaryCitizenFlag;
	private String beneficiaryCitizenCode;
	private String debitRefNo;
	private String finalizePaymentFlag;
	private String creditRefNo;
	private String BENEFICIARYBANKCITYCODE;
	private String ORGANIZATIONDIRNAME;
	private String PURPOSEOFTRX;
	private String REMITTANCECODE1;
	private String REMITTANCEINFO1;
	private String REMITTANCECODE2;
	private String REMITTANCEINFO2;
	private String REMITTANCECODE3;
	private String REMITTANCEINFO3;
	private String REMITTANCECODE4;
	private String REMITTANCEINFO4;
	private String INSTRUCTIONCODE1;
	private String INSTRUCTIONREMARK1;
	private String INSTRUCTIONCODE2;
	private String INSTRUCTIONREMARK2;
	private String INSTRUCTIONCODE3;
	private String INSTRUCTIONREMARK3;
	private String SWIFTMETHOD;
	private String IBTBUYRATE;
	private String IBTSELLRATE;
	private String VALUEDATE;
	private String DEBITAMOUNT;
	private String RESERVE1;
	private String RESERVE2;
	private String RESERVE3;
	private String RESERVE4;
	private String RESERVE5;
	private Date insertDate;
	private Date processDate;
	private Integer respCountChecker;
	private String status;
	private String legStatus;
	private String mvaDesc;
	private String mvaStatus;
	private String execStatus;
	private String sorborMark;
	private String fileName;
	private String errorCode;
	private String errorMessage;
	private String remittanceNo;
	private String postingTimeStamp;
	
	private boolean selected;
	private String debitAccType;
	
	public SPANHostDataDetails(){
		
	}
	
	public SPANHostDataDetails( String documentNumber,String fileName, String debitAccNo,
			String debitAccType, String documentDate, String amount, String execStatus) {
		//For Search Document Number in Monitoring Void Depkeu
		super();
		this.documentDate = documentDate;
		this.documentNumber = documentNumber;
		this.amount = amount;
		this.debitAccType = debitAccType;
		this.debitAccNo = debitAccNo;
		this.fileName = fileName;
		this.execStatus = execStatus;
	}

	public SPANHostDataDetails(BigDecimal detailId, String batchId, String trxHeaderId, String trxDetailId, String documentNumber, String fileName, String amount, String creditAccNo, String creditAccName,String benBank, String errorCode, String errorMessage, String documentDate){
		this.detailId = detailId;
		this.batchId = batchId;
		this.trxHeaderId = trxHeaderId;
		this.trxDetailId = trxDetailId;
		this.documentNumber = documentNumber;
		this.fileName = fileName;
		this.amount = amount;
		this.creditAccNo = creditAccNo;
		this.creditAccName = creditAccName;
		this.benBank = benBank;
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.documentDate = documentDate;
	}
	
	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public BigDecimal getDetailId() {
		return detailId;
	}

	public void setDetailId(BigDecimal detailId) {
		this.detailId = detailId;
	}

	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	public String getBatchId() {
		return batchId;
	}

	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}

	public String getTrxHeaderId() {
		return trxHeaderId;
	}

	public void setTrxHeaderId(String trxHeaderId) {
		this.trxHeaderId = trxHeaderId;
	}

	public String getTrxDetailId() {
		return trxDetailId;
	}

	public void setTrxDetailId(String trxDetailId) {
		this.trxDetailId = trxDetailId;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getDocumentDate() {
		return documentDate;
	}

	public void setDocumentDate(String documentDate) {
		this.documentDate = documentDate;
	}

	public String getBenName() {
		return benName;
	}

	public void setBenName(String benName) {
		this.benName = benName;
	}

	public String getBenBankCode() {
		return benBankCode;
	}

	public void setBenBankCode(String benBankCode) {
		this.benBankCode = benBankCode;
	}

	public String getBenBank() {
		return benBank;
	}

	public void setBenBank(String benBank) {
		this.benBank = benBank;
	}

	public String getBenAccount() {
		return benAccount;
	}

	public void setBenAccount(String benAccount) {
		this.benAccount = benAccount;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAgentBankCode() {
		return agentBankCode;
	}

	public void setAgentBankCode(String agentBankCode) {
		this.agentBankCode = agentBankCode;
	}

	public String getAgentBankAccNo() {
		return agentBankAccNo;
	}

	public void setAgentBankAccNo(String agentBankAccNo) {
		this.agentBankAccNo = agentBankAccNo;
	}

	public String getAgentBankAccName() {
		return agentBankAccName;
	}

	public void setAgentBankAccName(String agentBankAccName) {
		this.agentBankAccName = agentBankAccName;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getSwiftCode() {
		return swiftCode;
	}

	public void setSwiftCode(String swiftCode) {
		this.swiftCode = swiftCode;
	}

	public String getIbanCode() {
		return ibanCode;
	}

	public void setIbanCode(String ibanCode) {
		this.ibanCode = ibanCode;
	}

	public String getPaymentMethods() {
		return paymentMethods;
	}

	public void setPaymentMethods(String paymentMethods) {
		this.paymentMethods = paymentMethods;
	}

	public String getSpanCount() {
		return spanCount;
	}

	public void setSpanCount(String spanCount) {
		this.spanCount = spanCount;
	}

	public String getTotalAccount() {
		return totalAccount;
	}

	public void setTotalAccount(String totalAccount) {
		this.totalAccount = totalAccount;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getTotalBatchAccount() {
		return totalBatchAccount;
	}

	public void setTotalBatchAccount(String totalBatchAccount) {
		this.totalBatchAccount = totalBatchAccount;
	}

	public String getTransactionRef() {
		return transactionRef;
	}

	public void setTransactionRef(String transactionRef) {
		this.transactionRef = transactionRef;
	}

	public String getProviderRef() {
		return providerRef;
	}

	public void setProviderRef(String providerRef) {
		this.providerRef = providerRef;
	}

	public String getDebitAccNo() {
		return debitAccNo;
	}

	public void setDebitAccNo(String debitAccNo) {
		this.debitAccNo = debitAccNo;
	}

	public String getDebitAccExRate() {
		return debitAccExRate;
	}

	public void setDebitAccExRate(String debitAccExRate) {
		this.debitAccExRate = debitAccExRate;
	}

	public String getDebitAccCurrCode() {
		return debitAccCurrCode;
	}

	public void setDebitAccCurrCode(String debitAccCurrCode) {
		this.debitAccCurrCode = debitAccCurrCode;
	}

	public String getChargeInstruction() {
		return chargeInstruction;
	}

	public void setChargeInstruction(String chargeInstruction) {
		this.chargeInstruction = chargeInstruction;
	}

	public String getRemitterResFlag() {
		return remitterResFlag;
	}

	public void setRemitterResFlag(String remitterResFlag) {
		this.remitterResFlag = remitterResFlag;
	}

	public String getRemitterResCode() {
		return remitterResCode;
	}

	public void setRemitterResCode(String remitterResCode) {
		this.remitterResCode = remitterResCode;
	}

	public String getRemitterCitizenFlag() {
		return remitterCitizenFlag;
	}

	public void setRemitterCitizenFlag(String remitterCitizenFlag) {
		this.remitterCitizenFlag = remitterCitizenFlag;
	}

	public String getRemitterCitizenCode() {
		return remitterCitizenCode;
	}

	public void setRemitterCitizenCode(String remitterCitizenCode) {
		this.remitterCitizenCode = remitterCitizenCode;
	}

	public String getCreditAccNo() {
		return creditAccNo;
	}

	public void setCreditAccNo(String creditAccNo) {
		this.creditAccNo = creditAccNo;
	}

	public String getCreditAccName() {
		return creditAccName;
	}

	public void setCreditAccName(String creditAccName) {
		this.creditAccName = creditAccName;
	}

	public String getCreditAccExRate() {
		return creditAccExRate;
	}

	public void setCreditAccExRate(String creditAccExRate) {
		this.creditAccExRate = creditAccExRate;
	}

	public String getCreditTrfCurr() {
		return creditTrfCurr;
	}

	public void setCreditTrfCurr(String creditTrfCurr) {
		this.creditTrfCurr = creditTrfCurr;
	}

	public String getCreditTrfAmmount() {
		return creditTrfAmmount;
	}

	public void setCreditTrfAmmount(String creditTrfAmmount) {
		this.creditTrfAmmount = creditTrfAmmount;
	}

	public String getTrxRemark1() {
		return trxRemark1;
	}

	public void setTrxRemark1(String trxRemark1) {
		this.trxRemark1 = trxRemark1;
	}

	public String getTrxRemark2() {
		return trxRemark2;
	}

	public void setTrxRemark2(String trxRemark2) {
		this.trxRemark2 = trxRemark2;
	}

	public String getTrxRemark3() {
		return trxRemark3;
	}

	public void setTrxRemark3(String trxRemark3) {
		this.trxRemark3 = trxRemark3;
	}

	public String getTrxRemark4() {
		return trxRemark4;
	}

	public void setTrxRemark4(String trxRemark4) {
		this.trxRemark4 = trxRemark4;
	}

	public String getFtServices() {
		return ftServices;
	}

	public void setFtServices(String ftServices) {
		this.ftServices = ftServices;
	}

	public String getChargeCurrCode1() {
		return chargeCurrCode1;
	}

	public void setChargeCurrCode1(String chargeCurrCode1) {
		this.chargeCurrCode1 = chargeCurrCode1;
	}

	public String getChargeAmount1() {
		return chargeAmount1;
	}

	public void setChargeAmount1(String chargeAmount1) {
		this.chargeAmount1 = chargeAmount1;
	}

	public String getChargeCurrCode2() {
		return chargeCurrCode2;
	}

	public void setChargeCurrCode2(String chargeCurrCode2) {
		this.chargeCurrCode2 = chargeCurrCode2;
	}

	public String getChargeAmount2() {
		return chargeAmount2;
	}

	public void setChargeAmount2(String chargeAmount2) {
		this.chargeAmount2 = chargeAmount2;
	}

	public String getChargeCurrCode3() {
		return chargeCurrCode3;
	}

	public void setChargeCurrCode3(String chargeCurrCode3) {
		this.chargeCurrCode3 = chargeCurrCode3;
	}

	public String getChargeAmount3() {
		return chargeAmount3;
	}

	public void setChargeAmount3(String chargeAmount3) {
		this.chargeAmount3 = chargeAmount3;
	}

	public String getChargeCurrCode4() {
		return chargeCurrCode4;
	}

	public void setChargeCurrCode4(String chargeCurrCode4) {
		this.chargeCurrCode4 = chargeCurrCode4;
	}

	public String getChargeAmount4() {
		return chargeAmount4;
	}

	public void setChargeAmount4(String chargeAmount4) {
		this.chargeAmount4 = chargeAmount4;
	}

	public String getChargeCurrCode5() {
		return chargeCurrCode5;
	}

	public void setChargeCurrCode5(String chargeCurrCode5) {
		this.chargeCurrCode5 = chargeCurrCode5;
	}

	public String getChargeAmount5() {
		return chargeAmount5;
	}

	public void setChargeAmount5(String chargeAmount5) {
		this.chargeAmount5 = chargeAmount5;
	}

	public String getBeneficiaryAddress1() {
		return beneficiaryAddress1;
	}

	public void setBeneficiaryAddress1(String beneficiaryAddress1) {
		this.beneficiaryAddress1 = beneficiaryAddress1;
	}

	public String getBeneficiaryAddress2() {
		return beneficiaryAddress2;
	}

	public void setBeneficiaryAddress2(String beneficiaryAddress2) {
		this.beneficiaryAddress2 = beneficiaryAddress2;
	}

	public String getBeneficiaryAddress3() {
		return beneficiaryAddress3;
	}

	public void setBeneficiaryAddress3(String beneficiaryAddress3) {
		this.beneficiaryAddress3 = beneficiaryAddress3;
	}

	public String getBeneficiaryBankCode() {
		return beneficiaryBankCode;
	}

	public void setBeneficiaryBankCode(String beneficiaryBankCode) {
		this.beneficiaryBankCode = beneficiaryBankCode;
	}

	public String getBeneficiaryBankName() {
		return beneficiaryBankName;
	}

	public void setBeneficiaryBankName(String beneficiaryBankName) {
		this.beneficiaryBankName = beneficiaryBankName;
	}

	public String getBeneficiaryBankBranchName() {
		return beneficiaryBankBranchName;
	}

	public void setBeneficiaryBankBranchName(String beneficiaryBankBranchName) {
		this.beneficiaryBankBranchName = beneficiaryBankBranchName;
	}

	public String getBeneficiaryBankAddress1() {
		return beneficiaryBankAddress1;
	}

	public void setBeneficiaryBankAddress1(String beneficiaryBankAddress1) {
		this.beneficiaryBankAddress1 = beneficiaryBankAddress1;
	}

	public String getBeneficiaryBankAddress2() {
		return beneficiaryBankAddress2;
	}

	public void setBeneficiaryBankAddress2(String beneficiaryBankAddress2) {
		this.beneficiaryBankAddress2 = beneficiaryBankAddress2;
	}

	public String getBeneficiaryBankAddress3() {
		return beneficiaryBankAddress3;
	}

	public void setBeneficiaryBankAddress3(String beneficiaryBankAddress3) {
		this.beneficiaryBankAddress3 = beneficiaryBankAddress3;
	}

	public String getBeneficiaryBankCityName() {
		return beneficiaryBankCityName;
	}

	public void setBeneficiaryBankCityName(String beneficiaryBankCityName) {
		this.beneficiaryBankCityName = beneficiaryBankCityName;
	}

	public String getBeneficiaryBankCountryName() {
		return beneficiaryBankCountryName;
	}

	public void setBeneficiaryBankCountryName(String beneficiaryBankCountryName) {
		this.beneficiaryBankCountryName = beneficiaryBankCountryName;
	}

	public String getBeneficiaryResFlag() {
		return beneficiaryResFlag;
	}

	public void setBeneficiaryResFlag(String beneficiaryResFlag) {
		this.beneficiaryResFlag = beneficiaryResFlag;
	}

	public String getBeneficiaryResCode() {
		return beneficiaryResCode;
	}

	public void setBeneficiaryResCode(String beneficiaryResCode) {
		this.beneficiaryResCode = beneficiaryResCode;
	}

	public String getBeneficiaryCitizenFlag() {
		return beneficiaryCitizenFlag;
	}

	public void setBeneficiaryCitizenFlag(String beneficiaryCitizenFlag) {
		this.beneficiaryCitizenFlag = beneficiaryCitizenFlag;
	}

	public String getBeneficiaryCitizenCode() {
		return beneficiaryCitizenCode;
	}

	public void setBeneficiaryCitizenCode(String beneficiaryCitizenCode) {
		this.beneficiaryCitizenCode = beneficiaryCitizenCode;
	}

	public String getDebitRefNo() {
		return debitRefNo;
	}

	public void setDebitRefNo(String debitRefNo) {
		this.debitRefNo = debitRefNo;
	}

	public String getFinalizePaymentFlag() {
		return finalizePaymentFlag;
	}

	public void setFinalizePaymentFlag(String finalizePaymentFlag) {
		this.finalizePaymentFlag = finalizePaymentFlag;
	}

	public String getCreditRefNo() {
		return creditRefNo;
	}

	public void setCreditRefNo(String creditRefNo) {
		this.creditRefNo = creditRefNo;
	}

	public String getBENEFICIARYBANKCITYCODE() {
		return BENEFICIARYBANKCITYCODE;
	}

	public void setBENEFICIARYBANKCITYCODE(String bENEFICIARYBANKCITYCODE) {
		BENEFICIARYBANKCITYCODE = bENEFICIARYBANKCITYCODE;
	}

	public String getORGANIZATIONDIRNAME() {
		return ORGANIZATIONDIRNAME;
	}

	public void setORGANIZATIONDIRNAME(String oRGANIZATIONDIRNAME) {
		ORGANIZATIONDIRNAME = oRGANIZATIONDIRNAME;
	}

	public String getPURPOSEOFTRX() {
		return PURPOSEOFTRX;
	}

	public void setPURPOSEOFTRX(String pURPOSEOFTRX) {
		PURPOSEOFTRX = pURPOSEOFTRX;
	}

	public String getREMITTANCECODE1() {
		return REMITTANCECODE1;
	}

	public void setREMITTANCECODE1(String rEMITTANCECODE1) {
		REMITTANCECODE1 = rEMITTANCECODE1;
	}

	public String getREMITTANCEINFO1() {
		return REMITTANCEINFO1;
	}

	public void setREMITTANCEINFO1(String rEMITTANCEINFO1) {
		REMITTANCEINFO1 = rEMITTANCEINFO1;
	}

	public String getREMITTANCECODE2() {
		return REMITTANCECODE2;
	}

	public void setREMITTANCECODE2(String rEMITTANCECODE2) {
		REMITTANCECODE2 = rEMITTANCECODE2;
	}

	public String getREMITTANCEINFO2() {
		return REMITTANCEINFO2;
	}

	public void setREMITTANCEINFO2(String rEMITTANCEINFO2) {
		REMITTANCEINFO2 = rEMITTANCEINFO2;
	}

	public String getREMITTANCECODE3() {
		return REMITTANCECODE3;
	}

	public void setREMITTANCECODE3(String rEMITTANCECODE3) {
		REMITTANCECODE3 = rEMITTANCECODE3;
	}

	public String getREMITTANCEINFO3() {
		return REMITTANCEINFO3;
	}

	public void setREMITTANCEINFO3(String rEMITTANCEINFO3) {
		REMITTANCEINFO3 = rEMITTANCEINFO3;
	}

	public String getREMITTANCECODE4() {
		return REMITTANCECODE4;
	}

	public void setREMITTANCECODE4(String rEMITTANCECODE4) {
		REMITTANCECODE4 = rEMITTANCECODE4;
	}

	public String getREMITTANCEINFO4() {
		return REMITTANCEINFO4;
	}

	public void setREMITTANCEINFO4(String rEMITTANCEINFO4) {
		REMITTANCEINFO4 = rEMITTANCEINFO4;
	}

	public String getINSTRUCTIONCODE1() {
		return INSTRUCTIONCODE1;
	}

	public void setINSTRUCTIONCODE1(String iNSTRUCTIONCODE1) {
		INSTRUCTIONCODE1 = iNSTRUCTIONCODE1;
	}

	public String getINSTRUCTIONREMARK1() {
		return INSTRUCTIONREMARK1;
	}

	public void setINSTRUCTIONREMARK1(String iNSTRUCTIONREMARK1) {
		INSTRUCTIONREMARK1 = iNSTRUCTIONREMARK1;
	}

	public String getINSTRUCTIONCODE2() {
		return INSTRUCTIONCODE2;
	}

	public void setINSTRUCTIONCODE2(String iNSTRUCTIONCODE2) {
		INSTRUCTIONCODE2 = iNSTRUCTIONCODE2;
	}

	public String getINSTRUCTIONREMARK2() {
		return INSTRUCTIONREMARK2;
	}

	public void setINSTRUCTIONREMARK2(String iNSTRUCTIONREMARK2) {
		INSTRUCTIONREMARK2 = iNSTRUCTIONREMARK2;
	}

	public String getINSTRUCTIONCODE3() {
		return INSTRUCTIONCODE3;
	}

	public void setINSTRUCTIONCODE3(String iNSTRUCTIONCODE3) {
		INSTRUCTIONCODE3 = iNSTRUCTIONCODE3;
	}

	public String getINSTRUCTIONREMARK3() {
		return INSTRUCTIONREMARK3;
	}

	public void setINSTRUCTIONREMARK3(String iNSTRUCTIONREMARK3) {
		INSTRUCTIONREMARK3 = iNSTRUCTIONREMARK3;
	}

	public String getSWIFTMETHOD() {
		return SWIFTMETHOD;
	}

	public void setSWIFTMETHOD(String sWIFTMETHOD) {
		SWIFTMETHOD = sWIFTMETHOD;
	}

	public String getIBTBUYRATE() {
		return IBTBUYRATE;
	}

	public void setIBTBUYRATE(String iBTBUYRATE) {
		IBTBUYRATE = iBTBUYRATE;
	}

	public String getIBTSELLRATE() {
		return IBTSELLRATE;
	}

	public void setIBTSELLRATE(String iBTSELLRATE) {
		IBTSELLRATE = iBTSELLRATE;
	}

	public String getVALUEDATE() {
		return VALUEDATE;
	}

	public void setVALUEDATE(String vALUEDATE) {
		VALUEDATE = vALUEDATE;
	}

	public String getDEBITAMOUNT() {
		return DEBITAMOUNT;
	}

	public void setDEBITAMOUNT(String dEBITAMOUNT) {
		DEBITAMOUNT = dEBITAMOUNT;
	}

	public String getRESERVE1() {
		return RESERVE1;
	}

	public void setRESERVE1(String rESERVE1) {
		RESERVE1 = rESERVE1;
	}

	public String getRESERVE2() {
		return RESERVE2;
	}

	public void setRESERVE2(String rESERVE2) {
		RESERVE2 = rESERVE2;
	}

	public String getRESERVE3() {
		return RESERVE3;
	}

	public void setRESERVE3(String rESERVE3) {
		RESERVE3 = rESERVE3;
	}

	public String getRESERVE4() {
		return RESERVE4;
	}

	public void setRESERVE4(String rESERVE4) {
		RESERVE4 = rESERVE4;
	}

	public String getRESERVE5() {
		return RESERVE5;
	}

	public void setRESERVE5(String rESERVE5) {
		RESERVE5 = rESERVE5;
	}

	public Date getInsertDate() {
		return insertDate;
	}

	public void setInsertDate(Date insertDate) {
		this.insertDate = insertDate;
	}

	public Date getProcessDate() {
		return processDate;
	}

	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}

	public Integer getRespCountChecker() {
		return respCountChecker;
	}

	public void setRespCountChecker(Integer respCountChecker) {
		this.respCountChecker = respCountChecker;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getLegStatus() {
		return legStatus;
	}

	public void setLegStatus(String legStatus) {
		this.legStatus = legStatus;
	}

	public String getMvaDesc() {
		return mvaDesc;
	}

	public void setMvaDesc(String mvaDesc) {
		this.mvaDesc = mvaDesc;
	}

	public String getMvaStatus() {
		return mvaStatus;
	}

	public void setMvaStatus(String mvaStatus) {
		this.mvaStatus = mvaStatus;
	}

	public String getExecStatus() {
		return execStatus;
	}

	public void setExecStatus(String execStatus) {
		this.execStatus = execStatus;
	}

	public String getSorborMark() {
		return sorborMark;
	}

	public void setSorborMark(String sorborMark) {
		this.sorborMark = sorborMark;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getRemittanceNo() {
		return remittanceNo;
	}

	public void setRemittanceNo(String remittanceNo) {
		this.remittanceNo = remittanceNo;
	}

	public String getPostingTimeStamp() {
		return postingTimeStamp;
	}

	public void setPostingTimeStamp(String postingTimeStamp) {
		this.postingTimeStamp = postingTimeStamp;
	}

	public String getDebitAccType() {
		return debitAccType;
	}

	public void setDebitAccType(String debitAccType) {
		this.debitAccType = debitAccType;
	}
	
}
