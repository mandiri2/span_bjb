package com.mii.models;

import java.io.Serializable;
import java.util.Date;

public class ReportTransaksiM implements Serializable {
	private static final long serialVersionUID = 1L;

	private Date tglgabung;
	private String Id,startDate,endDate,NTPN,NTB,kodeBilling,status,uidOpr,kodeCabang,namaCabang,
	tanggalBuku,tglTerimaBayar,NPWP,namaWP,biller,jumlahTagih,jumlahBayar,uidSpv,userId,kantorPusat, reversal,sourceAcct;
	

	public ReportTransaksiM(){
		
	}
	public ReportTransaksiM(String kodeCabang, String namaCabang){
		this.kodeCabang = kodeCabang;
		this.namaCabang = namaCabang;
		
	}
	
	public String getTglTerimaBayar() {
		return tglTerimaBayar;
	}

	public void setTglTerimaBayar(String tglTerimaBayar) {
		this.tglTerimaBayar = tglTerimaBayar;
	}

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}


	public String getNTPN() {
		return NTPN;
	}

	public void setNTPN(String nTPN) {
		NTPN = nTPN;
	}

	public String getNTB() {
		return NTB;
	}

	public void setNTB(String nTB) {
		NTB = nTB;
	}

	public String getKodeBilling() {
		return kodeBilling;
	}

	public void setKodeBilling(String kodeBilling) {
		this.kodeBilling = kodeBilling;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUidOpr() {
		return uidOpr;
	}

	public void setUidOpr(String uidOpr) {
		this.uidOpr = uidOpr;
	}

	public String getKodeCabang() {
		return kodeCabang;
	}

	public void setKodeCabang(String kodeCabang) {
		this.kodeCabang = kodeCabang;
	}

	public String getNamaCabang() {
		return namaCabang;
	}

	public void setNamaCabang(String namaCabang) {
		this.namaCabang = namaCabang;
	}

	public String getTanggalBuku() {
		return tanggalBuku;
	}

	public void setTanggalBuku(String tanggalBuku) {
		this.tanggalBuku = tanggalBuku;
	}



	public String getNPWP() {
		return NPWP;
	}

	public void setNPWP(String nPWP) {
		NPWP = nPWP;
	}

	public String getNamaWP() {
		return namaWP;
	}

	public void setNamaWP(String namaWP) {
		this.namaWP = namaWP;
	}

	public String getBiller() {
		return biller;
	}

	public void setBiller(String biller) {
		this.biller = biller;
	}


	public String getJumlahTagih() {
		return jumlahTagih;
	}
	public void setJumlahTagih(String jumlahTagih) {
		this.jumlahTagih = jumlahTagih;
	}
	public String getJumlahBayar() {
		return jumlahBayar;
	}
	public void setJumlahBayar(String jumlahBayar) {
		this.jumlahBayar = jumlahBayar;
	}
	public String getUidSpv() {
		return uidSpv;
	}

	public void setUidSpv(String uidSpv) {
		this.uidSpv = uidSpv;
	}

	public Date getTglgabung() {
		return tglgabung;
	}

	public void setTglgabung(Date tglgabung) {
		this.tglgabung = tglgabung;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getKantorPusat() {
		return kantorPusat;
	}
	public void setKantorPusat(String kantorPusat) {
		this.kantorPusat = kantorPusat;
	}
	public String getReversal() {
		return reversal;
	}
	public void setReversal(String reversal) {
		this.reversal = reversal;
	}
	public String getSourceAcct() {
		return sourceAcct;
	}
	public void setSourceAcct(String sourceAcct) {
		this.sourceAcct = sourceAcct;
	}	
		
	
	
	}