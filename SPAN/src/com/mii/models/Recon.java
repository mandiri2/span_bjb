package com.mii.models;

import java.io.Serializable;

public class Recon implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private String caId;
    private String caName;
    private String settlementDate;
    private String reconTime;
    private String currency;
    private String billingCode;
    private String ntb;
    private String ntpn;
    private String amount;
    private String noSakti;
    
	public Recon(String caId, String caName, String settlementDate, String reconTime,
			String currency, String billingCode, String ntb, String ntpn,
			String amount, String noSakti) {
		super();
		this.caId = caId;
		this.caName = caName;
		this.settlementDate = settlementDate;
		this.reconTime = reconTime;
		this.currency = currency;
		this.billingCode = billingCode;
		this.ntb = ntb;
		this.ntpn = ntpn;
		this.amount = amount;
		this.noSakti = noSakti;
	}
	public String getCaId() {
		return caId;
	}
	public void setCaId(String caId) {
		this.caId = caId;
	}
	public String getCaName() {
		return caName;
	}
	public void setCaName(String caName) {
		this.caName = caName;
	}
	public String getSettlementDate() {
		return settlementDate;
	}
	public void setSettlementDate(String settlementDate) {
		this.settlementDate = settlementDate;
	}
	public String getReconTime() {
		return reconTime;
	}
	public void setReconTime(String reconTime) {
		this.reconTime = reconTime;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getBillingCode() {
		return billingCode;
	}
	public void setBillingCode(String billingCode) {
		this.billingCode = billingCode;
	}
	public String getNtb() {
		return ntb;
	}
	public void setNtb(String ntb) {
		this.ntb = ntb;
	}
	public String getNtpn() {
		return ntpn;
	}
	public void setNtpn(String ntpn) {
		this.ntpn = ntpn;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getNoSakti() {
		return noSakti;
	}
	public void setNoSakti(String noSakti) {
		this.noSakti = noSakti;
	}
    
 }