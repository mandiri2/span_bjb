package com.mii.models;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import com.mii.constant.Constants;
import com.mii.helpers.FacesUtil;

public class User implements Serializable, HttpSessionBindingListener  {
	private static final long serialVersionUID = 1L;
	
	private String userID;
	
	private String email;
	private String phone;
	private String userName;
	private String fullName;
	private String password;
	private String division;
	private String department;
	private String contactNumber;
	private Date lastLogin;
	private String createWho;
	private String changeWho;
	private Date createDate;
	private Date changeDate;
	private String mobileNumber;
	private String branch;
	private String branchName;
	private String tellerId;
	private BigInteger debitLimitTransaction;
	private int failedLogin;
	private Date failedLoginDate;	
	private List<UserRole> roles = new ArrayList<UserRole>();
	private String roleNameTemp;
	private String status;	
	private Boolean disableEdit;
	private Boolean disableDelete;
	
	private Date approvalDate;
	
	private String approvalBy;
	
	private String currentTerminal;
	private String userAgent;	
	private static Map<User, HttpSession> logins = new HashMap<User, HttpSession>();
	private String firstLogin;
	
	public User() {}
	
	public User(String userID, String name, String fullName, String email, Date lastLogin, String branch, String branchName, String status){
		this.userID = userID;
		this.email = email;
		this.userName = name;
		this.fullName = fullName;
		this.lastLogin = lastLogin;
		this.branch = branch;
		this.branchName = branchName;
		this.status = status;
	}
	
	public User(String userID, String email, String phone, String mobileNumber, String contactNumber, String name, String fullName, String division, String department, String branch, String branchName, String createwho, String tellerId, String  debitLimitTransaction ){
		this.userID = userID;
		this.email = email;
		this.phone = phone;
		this.mobileNumber = mobileNumber;
		this.contactNumber = contactNumber;
		this.userName = name;
		this.fullName = fullName;
		this.division = division;
		this.department = department;
		this.branch = branch;
		this.branchName = branchName;
		this.createWho = createwho;
		this.tellerId = tellerId;
		this.debitLimitTransaction = new BigInteger(debitLimitTransaction);
	}
	
	public List<UserRole> getRoles() {
		return roles;
	}

	public void setRoles(List<UserRole> roles) {
		this.roles = roles;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	
	
	public String getRoleNameTemp() {
		return roleNameTemp;
	}

	public void setRoleNameTemp(String roleNameTemp) {
		this.roleNameTemp = roleNameTemp;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public Date getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}

	public String getCreateWho() {
		return createWho;
	}

	public void setCreateWho(String createWho) {
		this.createWho = createWho;
	}

	public String getChangeWho() {
		return changeWho;
	}

	public void setChangeWho(String changeWho) {
		this.changeWho = changeWho;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getChangeDate() {
		return changeDate;
	}

	public void setChangeDate(Date changeDate) {
		this.changeDate = changeDate;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}
	
	public BigInteger getDebitLimitTransaction() {
		return debitLimitTransaction;
	}

	public String getTellerId() {
		return tellerId;
	}

	public void setTellerId(String tellerId) {
		this.tellerId = tellerId;
	}

	public void setDebitLimitTransaction(BigInteger debitLimitTransaction) {
		this.debitLimitTransaction = debitLimitTransaction;
	}

	public int getFailedLogin() {
		return failedLogin;
	}

	public void setFailedLogin(int failedLogin) {
		this.failedLogin = failedLogin;
	}

	public Date getFailedLoginDate() {
		return failedLoginDate;
	}

	public void setFailedLoginDate(Date failedLoginDate) {
		this.failedLoginDate = failedLoginDate;
	}
	
	public Boolean getDisableEdit() {
		if (this.status.equals(Constants.APPROVAL_STATUS_PENDING_DELETE))
			return true;
		else{
			return false;
		}
	}

	public void setDisableEdit(Boolean disableEdit) {
		this.disableEdit = disableEdit;
	}

	public Boolean getDisableDelete() {
		if ((this.status.equals(Constants.APPROVAL_STATUS_PENDING_CREATE) || (this.status.equals(Constants.APPROVAL_STATUS_PENDING_EDIT))) || this.status.equals(Constants.APPROVAL_STATUS_PENDING_DELETE))
			return true;
		else{
			return false;
		}
	}

	public void setDisableDelete(Boolean disableDelete) {
		this.disableDelete = disableDelete;
	}

	@Override
	public void valueBound(HttpSessionBindingEvent event) {			
		HttpSession session = logins.remove(this);
        if (session != null) {
            session.invalidate();
        }
        this.currentTerminal = FacesUtil.getClientIpAddress();
        this.userAgent = FacesUtil.getClientUserAgent();
        logins.put(this, event.getSession());
	}

	@Override
	public void valueUnbound(HttpSessionBindingEvent event) {
		logins.remove(this);
	}
	
	@Override
    public boolean equals(Object other) {
        return (other instanceof User) && (userID != null) ? userID.equals(((User) other).userID) : (other == this);
    }
	
	@Override
    public int hashCode() {
        return (userID != null) ? (this.getClass().hashCode() + userID.hashCode()) : super.hashCode();
    }

	public static Map<User, HttpSession> getLogins() {
		return logins;
	}

	public static void setLogins(Map<User, HttpSession> logins) {
		User.logins = logins;
	}

	public String getCurrentTerminal() {
		return currentTerminal;
	}

	public void setCurrentTerminal(String currentTerminal) {
		this.currentTerminal = currentTerminal;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}

	public String getApprovalBy() {
		return approvalBy;
	}

	public void setApprovalBy(String approvalBy) {
		this.approvalBy = approvalBy;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getFirstLogin() {
		return firstLogin;
	}

	public void setFirstLogin(String firstLogin) {
		this.firstLogin = firstLogin;
	}
	
	

}
