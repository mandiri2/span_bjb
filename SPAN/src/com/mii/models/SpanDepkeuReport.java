package com.mii.models;

public class SpanDepkeuReport {
	private String docDate;
	private String docNumber;
	private String benName;
	private String benAccount;
	private String amount;
	private String status;
	
	public SpanDepkeuReport(){
		
	}
	
	public SpanDepkeuReport(String docDate,String docNumber,String benName,String benAccount,String amount,String status){
		this.docDate=docDate;
		this.docNumber=docNumber;
		this.benName=benName;
		this.benAccount=benAccount;
		this.amount=amount;
		this.status=status;
		
	}
	
	
	public String getDocDate() {
		return docDate;
	}
	public void setDocDate(String docDate) {
		this.docDate = docDate;
	}
	public String getDocNumber() {
		return docNumber;
	}
	public void setDocNumber(String docNumber) {
		this.docNumber = docNumber;
	}
	public String getBenName() {
		return benName;
	}
	public void setBenName(String benName) {
		this.benName = benName;
	}
	public String getBenAccount() {
		return benAccount;
	}
	public void setBenAccount(String benAccount) {
		this.benAccount = benAccount;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
