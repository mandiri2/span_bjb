package com.mii.models;

import java.io.Serializable;
import java.util.Date;

public class SpanUploadSp2d implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String file_name;
	private String insert_date;
	private Date upload_time;
	private String upload_by;
	
	public String getFile_name() {
		return file_name;
	}
	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}
	public String getInsert_date() {
		return insert_date;
	}
	public void setInsert_date(String insert_date) {
		this.insert_date = insert_date;
	}
	public Date getUpload_time() {
		return upload_time;
	}
	public void setUpload_time(Date upload_time) {
		this.upload_time = upload_time;
	}
	public String getUpload_by() {
		return upload_by;
	}
	public void setUpload_by(String upload_by) {
		this.upload_by = upload_by;
	}
	
	
}
