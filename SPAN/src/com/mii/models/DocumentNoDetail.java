package com.mii.models;

public class DocumentNoDetail {
	
	private String fileName;
	private String documentNo;
	private String documentDate;
	private String creditAccountName;
	private String creditAccountNo;
	private String debitAccountNo;
	private String amount;
	private String remittanceNo;
	
	private String transactionDate;
	private String createDate;
	private String errorCode;
	private String errorMessage;
	
	private String statusTrx;
	private String sorBorNum;
	private String executor;
	
	
	
	public String getStatusTrx() {
		return statusTrx;
	}
	public void setStatusTrx(String statusTrx) {
		this.statusTrx = statusTrx;
	}
	public String getSorBorNum() {
		return sorBorNum;
	}
	public void setSorBorNum(String sorBorNum) {
		this.sorBorNum = sorBorNum;
	}
	public String getExecutor() {
		return executor;
	}
	public void setExecutor(String executor) {
		this.executor = executor;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getDocumentNo() {
		return documentNo;
	}
	public void setDocumentNo(String documentNo) {
		this.documentNo = documentNo;
	}
	public String getDocumentDate() {
		return documentDate;
	}
	public void setDocumentDate(String documentDate) {
		this.documentDate = documentDate;
	}
	public String getCreditAccountName() {
		return creditAccountName;
	}
	public void setCreditAccountName(String creditAccountName) {
		this.creditAccountName = creditAccountName;
	}
	public String getCreditAccountNo() {
		return creditAccountNo;
	}
	public void setCreditAccountNo(String creditAccountNo) {
		this.creditAccountNo = creditAccountNo;
	}
	public String getDebitAccountNo() {
		return debitAccountNo;
	}
	public void setDebitAccountNo(String debitAccountNo) {
		this.debitAccountNo = debitAccountNo;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getRemittanceNo() {
		return remittanceNo;
	}
	public void setRemittanceNo(String remittanceNo) {
		this.remittanceNo = remittanceNo;
	}
	public String getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
}
