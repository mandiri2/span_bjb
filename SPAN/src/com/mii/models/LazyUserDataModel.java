package com.mii.models;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

public class LazyUserDataModel extends LazyDataModel<User> {
	private static final long serialVersionUID = 1L;
	private List<User> users;
	
	public LazyUserDataModel(List<User> users) {
		this.users = users;
	}
	@Override
    public User getRowData(String rowKey) {
        for(User user : users) {
            if(user.getUserID().equals(rowKey))
                return user;
        }
        return null;
    }
	
	@Override
    public Object getRowKey(User User) {
        return User.getUserID();
    }
	
	@Override
	public List<User> load(int first, int pageSize, String sortField, SortOrder sortOrder, 
    		Map<String, String> filters) {
		
        List<User> data = new ArrayList<User>();
        
        //filter
        for(User user : users) {
            boolean match = true;
 
            for(Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                try {
                    String filterProperty = it.next();
                    String filterValue = (String) filters.get(filterProperty);
                    Object fieldValue = String.valueOf(user.getClass().getField(filterProperty).get(user));
 
                    if(filterValue == null || ((String) fieldValue).startsWith(filterValue.toString())) {
                        match = true;
                    }
                    else {
                        match = false;
                        break;
                    }
                } catch(Exception e) {
                    match = false;
                } 
            }
 
            if(match) {
                data.add(user);
            }
        }

        //rowCount
        int dataSize = data.size();
        this.setRowCount(dataSize);
 
        //paginate
        if(dataSize > pageSize) {
            try {
                return data.subList(first, first + pageSize);
            }
            catch(IndexOutOfBoundsException e) {
                return data.subList(first, first + (dataSize % pageSize));
            }
        }
        else {
            return data;
        }
    }
	
	public List<User> getUsers() {
		return users;
	}
	
	public void setUsers(List<User> users) {
		this.users = users;
	}
	
}
