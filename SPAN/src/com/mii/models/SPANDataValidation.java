package com.mii.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Blob;
import java.util.Date;

public class SPANDataValidation implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String fileName;
	private String docNumber;
	private String benName;
	private String benAccount;
	private String amount;
	private String status;
	private String spanFnType;
	private String debitAccount;
	private String debitAccountType;
	private String transactionId;
	private String documentDate;
	private String totalAmount;
	private String totalRecord;
	private String procStatus;
	private String procDescription;
	private String xmlFileName;
	private String responseCode;
	private String ackFileName;
	private String ackStatus;
	private String ackDescription;
	private String trxtype;
	private String scFileName;
	private Date createDate;
	private String transFlag;
	
	private int fileNameCount;
	private Long totalRecords;
	
	//Update Add exec status
	private String execFileFlag;
	//Update Add batchId
	private String batchId;
	private boolean selected;
	
	//constructor
	public SPANDataValidation(String documentDate,  String fileName, String docNumber,
			String benName, String benAccount, String amount, String transFlag) {
		super();
		this.fileName = fileName;
		this.docNumber = docNumber;
		this.benName = benName;
		this.benAccount = benAccount;
		this.amount = amount;
		this.documentDate = documentDate;
		this.transFlag = transFlag;
	}
	
	public SPANDataValidation(String fileName,  String totalRecord, String totalAmount,
			String spanFnType, String debitAccount, String debitAccountType, String documentDate, String dummy) {
		super();
		this.fileName = fileName;
		this.totalRecord = totalRecord;
		this.totalAmount = totalAmount;
		this.spanFnType = spanFnType;
		this.debitAccount = debitAccount;
		this.debitAccountType = debitAccountType;
		this.documentDate = documentDate;
	}
	
	
	// setter and getter
	
	public SPANDataValidation(String fileName, String totalAmount,
			String totalRecord, String procStatus, String xmlFileName, String responseCode) {
		super();
		this.fileName = fileName;
		this.totalAmount = totalAmount;
		this.totalRecord = totalRecord;
		this.procStatus = procStatus;
		this.setXmlFileName(xmlFileName);
		this.responseCode = responseCode;
	}



	public SPANDataValidation() {
	}

	public void setFileName(String fileName){
		this.fileName=fileName;
	}

	public String getTransFlag() {
		return transFlag;
	}

	public void setTransFlag(String transFlag) {
		this.transFlag = transFlag;
	}

	public String getDocNumber() {
		return docNumber;
	}

	public void setDocNumber(String docNumber) {
		this.docNumber = docNumber;
	}

	public String getBenName() {
		return benName;
	}

	public void setBenName(String benName) {
		this.benName = benName;
	}

	public String getBenAccount() {
		return benAccount;
	}

	public void setBenAccount(String benAccount) {
		this.benAccount = benAccount;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFileName(){
		return fileName;
	}
	
	public void setSpanFnType(String spanFnType){
		this.spanFnType=spanFnType;
	}
	
	public String getSpanFnType(){
		return spanFnType;
	}
	

	public void setDebitAccount(String debitAccount){
		this.debitAccount=debitAccount;
	}
	
	public String getDebitAccount(){
		return debitAccount;
	}
	
	public void setDebitAccountType(String debitAccountType){
		this.debitAccountType=debitAccountType;
	}
	
	public String getDebitAccountType(){
		return debitAccountType;
	}
	
	public void setTransactionId(String transactionId){
		this.transactionId=transactionId;
	}
	
	public String getTransactionId(){
		return transactionId;
	}
	
	public void setDocumentDate(String documentDate){
		this.documentDate=documentDate;
	}
	
	public String getDocumentDate(){
		return documentDate;
	}
	
	public void setTotalAmount(String totalAmount){
		this.totalAmount=totalAmount;
		/*this.totalAmount=amountConverter(totalAmount);*/
		
	}
	
	public String getTotalAmount(){
		return totalAmount;
	}

	public void setTotalRecord(String totalRecord){
		this.totalRecord=totalRecord;
	}
	
	public String getTotalRecord(){
		return totalRecord;
	}

	public void setProcStatus(String procStatus){
		this.procStatus=procStatus;
	}
	
	public String getProcStatus(){
		return procStatus;
	}
	
	public void setProcDescription(String procDescription){
		this.procDescription=procDescription;
	}
	
	public String getProcDescription(){
		return procDescription;
	}
	
	public void setResponseCode(String responseCode){
		this.responseCode=responseCode;
	}
	
	public String getResponseCode(){
		return responseCode;
	}
	
	public void setAckFileName(String ackFileName){
		this.ackFileName=ackFileName;
	}
	
	public String getAckFileName(){
		return ackFileName;
	}
	
	public void setAckStatus(String ackStatus){
		this.ackStatus=ackStatus;
	}
	
	public String getAckStatus(){
		return ackStatus;
	}
	
	public void setAckDescription(String ackDescription){
		this.ackDescription=ackDescription;
	}
	
	public String getAckDescription(){
		return ackDescription;
	}
	
	public void setTrxtype(String trxtype){
		this.trxtype=trxtype;
	}
	
	public String getTrxtype(){
		return trxtype;
	}
	
	
	public void setCreateDate(Date createDate){
		this.createDate=createDate;
	}
	
	public Date getCreateDate(){
		return createDate;
	}
	
	public void setScFileName(String scFileName){
		this.scFileName=scFileName;
	}
	
	public String getScFileName(){
		return scFileName;
	}

	public int getFileNameCount() {
		return fileNameCount;
	}

	public void setFileNameCount(int fileNameCount) {
		this.fileNameCount = fileNameCount;
	}

	public Long getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(Long totalRecords) {
		this.totalRecords = totalRecords;
	}
	
	public String getExecFileFlag() {
		return execFileFlag;
	}

	public void setExecFileFlag(String execFileFlag) {
		this.execFileFlag = execFileFlag;
	}

	/*public String amountConverter(String totalAmount){
		if (StringUtils.isEmpty(totalAmount)) {
			System.out.println("Amount is null");
			return "0";
		} else {
			DecimalFormat formatter = (DecimalFormat) NumberFormat.getCurrencyInstance(new Locale("in", "ID"));
			DecimalFormatSymbols currencySymbol = formatter.getDecimalFormatSymbols();
			currencySymbol.setCurrencySymbol("");
			formatter.setDecimalFormatSymbols(currencySymbol);		
			return formatter.format(new BigDecimal(String.valueOf(totalAmount)));
		}
		
	}*/

	public String getBatchId() {
		return batchId;
	}

	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}



	public String getXmlFileName() {
		return xmlFileName;
	}



	public void setXmlFileName(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}
	
	/*private BigDecimal validationId;
	private String fileName;
	private String batchId;
	private String trxHeaderId;
	private String debitAccount;
	private String debitAccountType;
	private String documentDate;
	private String totalAmount;
	private String totalRecord;
	private String procStatus;
	private String procDescription;
	private String totalRetry;
	private String totalWaiting;
	private String totalProcess;
	private String totalSuccess;
	private String totalACKSuccess;
	private String totalRetur;
	private String totalACKRetur;*/
	
	
	/*public SPANDataValidation(BigDecimal validationId, String fileName, String batchId,
			String trxHeaderId, String debitAccount, String documentDate,
			String totalRecord, String procStatus, String totalRetry, String totalWaiting,
			String totalProcess, String totalSuccess, String totalACKSuccess, String totalRetur, String totalACKRetur) {
		super();
		this.validationId = validationId;
		this.fileName = fileName;
		this.batchId = batchId;
		this.trxHeaderId = trxHeaderId;
		this.debitAccount = debitAccount;
		this.documentDate = documentDate;
		this.totalRecord = totalRecord;
		this.procStatus = procStatus;
		this.totalRetry = totalRetry;
		this.totalWaiting = totalWaiting;
		this.totalProcess = totalProcess;
		this.totalSuccess = totalSuccess;
		this.totalACKSuccess = totalACKSuccess;
		this.totalRetur = totalRetur;
		this.totalACKRetur = totalACKRetur;
	}*/

	
	/*public BigDecimal getValidationId() {
		return validationId;
	}
	public void setValidationId(BigDecimal validationId) {
		this.validationId = validationId;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getBatchId() {
		return batchId;
	}
	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}
	public String getTrxHeaderId() {
		return trxHeaderId;
	}
	public void setTrxHeaderId(String trxHeaderId) {
		this.trxHeaderId = trxHeaderId;
	}
	public String getDebitAccount() {
		return debitAccount;
	}
	public void setDebitAccount(String debitAccount) {
		this.debitAccount = debitAccount;
	}
	public String getDebitAccountType() {
		return debitAccountType;
	}
	public void setDebitAccountType(String debitAccountType) {
		this.debitAccountType = debitAccountType;
	}
	public String getDocumentDate() {
		return documentDate;
	}
	public void setDocumentDate(String documentDate) {
		this.documentDate = documentDate;
	}
	public String getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}
	public String getTotalRecord() {
		return totalRecord;
	}
	public void setTotalRecord(String totalRecord) {
		this.totalRecord = totalRecord;
	}
	public String getProcStatus() {
		return procStatus;
	}
	public void setProcStatus(String procStatus) {
		this.procStatus = procStatus;
	}
	public String getProcDescription() {
		return procDescription;
	}
	public void setProcDescription(String procDescription) {
		this.procDescription = procDescription;
	}
	public String getTotalRetry() {
		return totalRetry;
	}
	public void setTotalRetry(String totalRetry) {
		this.totalRetry = totalRetry;
	}

	public String getTotalWaiting() {
		return totalWaiting;
	}

	public void setTotalWaiting(String totalWaiting) {
		this.totalWaiting = totalWaiting;
	}

	public String getTotalProcess() {
		return totalProcess;
	}

	public void setTotalProcess(String totalProcess) {
		this.totalProcess = totalProcess;
	}

	public String getTotalSuccess() {
		return totalSuccess;
	}

	public void setTotalSuccess(String totalSuccess) {
		this.totalSuccess = totalSuccess;
	}

	public String getTotalACKSuccess() {
		return totalACKSuccess;
	}

	public void setTotalACKSuccess(String totalACKSuccess) {
		this.totalACKSuccess = totalACKSuccess;
	}

	public String getTotalRetur() {
		return totalRetur;
	}

	public void setTotalRetur(String totalRetur) {
		this.totalRetur = totalRetur;
	}

	public String getTotalACKRetur() {
		return totalACKRetur;
	}

	public void setTotalACKRetur(String totalACKRetur) {
		this.totalACKRetur = totalACKRetur;
	}*/
	
	
}
