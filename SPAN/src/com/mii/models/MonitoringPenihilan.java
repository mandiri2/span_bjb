package com.mii.models;

import java.io.Serializable;

public class MonitoringPenihilan implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1493028093656195673L;
	private String no;
	private String tanggalPenihilan;
	private String tanggalSp2d;
	private String referenceNo;
	private String amount;
	private String creditAcc;
	private String sequencePenihilan;
	private String narasi;
	
	public MonitoringPenihilan(String tanggalPenihilan,
			String tanggalSp2d, String referenceNo, String amount,
			String creditAcc, String sequencePenihilan, String narasi) {
		super();
		this.tanggalPenihilan = tanggalPenihilan;
		this.tanggalSp2d = tanggalSp2d;
		this.referenceNo = referenceNo;
		this.amount = amount;
		this.creditAcc = creditAcc;
		this.sequencePenihilan = sequencePenihilan;
		this.setNarasi(narasi);
	}
	public String getTanggalPenihilan() {
		return tanggalPenihilan;
	}
	public void setTanggalPenihilan(String tanggalPenihilan) {
		this.tanggalPenihilan = tanggalPenihilan;
	}
	public String getTanggalSp2d() {
		return tanggalSp2d;
	}
	public void setTanggalSp2d(String tanggalSp2d) {
		this.tanggalSp2d = tanggalSp2d;
	}
	public String getReferenceNo() {
		return referenceNo;
	}
	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getSequencePenihilan() {
		return sequencePenihilan;
	}
	public void setSequencePenihilan(String sequencePenihilan) {
		this.sequencePenihilan = sequencePenihilan;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getCreditAcc() {
		return creditAcc;
	}
	public void setCreditAcc(String creditAcc) {
		this.creditAcc = creditAcc;
	}
	public String getNo() {
		return no;
	}
	public void setNo(String no) {
		this.no = no;
	}
	public String getNarasi() {
		return narasi;
	}
	public void setNarasi(String narasi) {
		this.narasi = narasi;
	}
}
