package com.mii.models;

import java.io.Serializable;
import java.util.Date;

import com.mii.constant.Constants;

public class ScheduleTask implements Serializable{

	private static final long serialVersionUID = 1L;

	private String taskId;;
	private String schedulerId;
	private String createDate;
	private String changeDate;
	private String createWho;
	private String changeWho;
	private String description;
	private String jobName; 
	private String executionService; 
	private String schedulerType; 
	private String date; 
	private String time; 
	private String interval; 
	private String startDate; 
	private String endDate; 
	private String startTime; 
	private String endTime; 
	private String maskYear; 
	private String maskMonth; 
	private String maskDay; 
	private String maskWeeklyDay; 
	private String maskHour; 
	private String maskHourEnd; 
	private String maskMinute; 
	private String schedulerStatus;
	private String repeating;
	private String approvalStatus;
	private Boolean disableEdit;
	private Boolean disableDelete;
	private Boolean disableStopStart;
	
	private Date approvalDate;
	
	private String approvalBy;
	
	
	public ScheduleTask() {
		// TODO Auto-generated constructor stub
	}
	
	public ScheduleTask(String schedulerId, String jobName, String executionService, String description, String schedulerType, String schedulerStatus, String approvalStatus) {
		// TODO Auto-generated constructor stub
		this.schedulerId = schedulerId;
		this.jobName = jobName;
		this.executionService = executionService;
		this.description = description;
		this.schedulerType = schedulerType;
		this.schedulerStatus = schedulerStatus;
		this.approvalStatus = approvalStatus;
	}
	
	public String getSchedulerId() {
		return schedulerId;
	}
	public void setSchedulerId(String schedulerId) {
		this.schedulerId = schedulerId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public String getExecutionService() {
		return executionService;
	}
	public void setExecutionService(String executionService) {
		this.executionService = executionService;
	}
	public String getSchedulerType() {
		return schedulerType;
	}
	public void setSchedulerType(String schedulerType) {
		this.schedulerType = schedulerType;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getInterval() {
		return interval;
	}
	public void setInterval(String interval) {
		this.interval = interval;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getMaskYear() {
		return maskYear;
	}
	public void setMaskYear(String maskYear) {
		this.maskYear = maskYear;
	}
	public String getMaskMonth() {
		return maskMonth;
	}
	public void setMaskMonth(String maskMonth) {
		this.maskMonth = maskMonth;
	}
	public String getMaskDay() {
		return maskDay;
	}
	public void setMaskDay(String maskDay) {
		this.maskDay = maskDay;
	}
	public String getMaskWeeklyDay() {
		return maskWeeklyDay;
	}
	public void setMaskWeeklyDay(String maskWeeklyDay) {
		this.maskWeeklyDay = maskWeeklyDay;
	}
	public String getMaskHour() {
		return maskHour;
	}
	public void setMaskHour(String maskHour) {
		this.maskHour = maskHour;
	}
	
	public String getMaskHourEnd() {
		return maskHourEnd;
	}

	public void setMaskHourEnd(String maskHourEnd) {
		this.maskHourEnd = maskHourEnd;
	}

	public String getMaskMinute() {
		return maskMinute;
	}
	public void setMaskMinute(String maskMinute) {
		this.maskMinute = maskMinute;
	}
	public String getSchedulerStatus() {
		return schedulerStatus;
	}
	public void setSchedulerStatus(String schedulerStatus) {
		this.schedulerStatus = schedulerStatus;
	}
	public String getRepeating() {
		return repeating;
	}
	public void setRepeating(String repeating) {
		this.repeating = repeating;
	}
	public String getApprovalStatus() {
		return approvalStatus;
	}
	public void setApprovalStatus(String approvalStatus) {
		this.approvalStatus = approvalStatus;
	}
	
	public Date getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}

	public String getApprovalBy() {
		return approvalBy;
	}

	public void setApprovalBy(String approvalBy) {
		this.approvalBy = approvalBy;
	}

	public String getCreateWho() {
		return createWho;
	}
	public void setCreateWho(String createWho) {
		this.createWho = createWho;
	}

	public String getChangeDate() {
		return changeDate;
	}

	public void setChangeDate(String changeDate) {
		this.changeDate = changeDate;
	}

	public String getChangeWho() {
		return changeWho;
	}

	public void setChangeWho(String changeWho) {
		this.changeWho = changeWho;
	}
	
	public Boolean getDisableEdit() {
		if (this.approvalStatus.equals(Constants.APPROVAL_STATUS_PENDING_DELETE))
			return true;
		else{
			return false;
		}
	}

	public void setDisableEdit(Boolean disableEdit) {
		this.disableEdit = disableEdit;
	}

	public Boolean getDisableDelete() {
		if ((this.approvalStatus.equals(Constants.APPROVAL_STATUS_PENDING_CREATE) || (this.approvalStatus.equals(Constants.APPROVAL_STATUS_PENDING_EDIT))) || this.approvalStatus.equals(Constants.APPROVAL_STATUS_PENDING_DELETE))
			return true;
		else{
			return false;
		}
	}

	public void setDisableDelete(Boolean disableDelete) {
		this.disableDelete = disableDelete;
	}

	public Boolean getDisableStopStart() {
		if (this.approvalStatus.equals(Constants.APPROVAL_STATUS_PENDING_CREATE))
			return true;
		else{
			return false;
		}
	}

	public void setDisableStopStart(Boolean disableStopStart) {
		this.disableStopStart = disableStopStart;
	}
	
	
	
}
