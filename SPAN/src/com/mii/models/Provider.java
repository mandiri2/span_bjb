package com.mii.models;

import java.io.Serializable;
import java.util.Date;

public class Provider implements Serializable{
	
private static final long serialVersionUID = 1L;
	
	private long providerId;
	private String providerCode, 
		providerName,
		providerAccount,
		providerCurrency,
		providerCodeAlias,
		createBy,
		updateBy;
	private Date createDtm, 
		updateDtm;
	
	
	
	
	/**GETTER SETTER**/
	
	public long getProviderId() {
		return providerId;
	}
	public void setProviderId(long providerId) {
		this.providerId = providerId;
	}
	public String getProviderCode() {
		return providerCode;
	}
	public void setProviderCode(String providerCode) {
		this.providerCode = providerCode;
	}
	public String getProviderName() {
		return providerName;
	}
	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}
	public String getProviderAccount() {
		return providerAccount;
	}
	public void setProviderAccount(String providerAccount) {
		this.providerAccount = providerAccount;
	}
	public String getProviderCurrency() {
		return providerCurrency;
	}
	public void setProviderCurrency(String providerCurrency) {
		this.providerCurrency = providerCurrency;
	}
	public String getProviderCodeAlias() {
		return providerCodeAlias;
	}
	public void setProviderCodeAlias(String providerCodeAlias) {
		this.providerCodeAlias = providerCodeAlias;
	}
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public Date getCreateDtm() {
		return createDtm;
	}
	public void setCreateDtm(Date createDtm) {
		this.createDtm = createDtm;
	}
	public Date getUpdateDtm() {
		return updateDtm;
	}
	public void setUpdateDtm(Date updateDtm) {
		this.updateDtm = updateDtm;
	}
	
}
