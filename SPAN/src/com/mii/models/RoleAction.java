package com.mii.models;

import java.io.Serializable;

public class RoleAction implements Serializable{
	private static final long serialVersionUID = 1L;
	private String roleId;
	private String action;
	
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	
}
