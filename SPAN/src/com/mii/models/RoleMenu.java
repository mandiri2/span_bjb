package com.mii.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RoleMenu implements Serializable {
	private static final long serialVersionUID = 1L;
	private String roleId;
	private String menuId;
	private List<Menu> menus = new ArrayList<Menu>();
	
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public String getMenuId() {
		return menuId;
	}
	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}
	public List<Menu> getMenus() {
		return menus;
	}
	public void setMenus(List<Menu> menus) {
		this.menus = menus;
	}
	
}
