package com.mii.models;

import java.io.Serializable;
import java.util.Date;

public class Rtgs implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String transactionId;
	
	private String transactionDate;
	
	private String updateDate;
	
	private String transactionFlag;
	
	private String referenceNumber;
	
	private String accountCurrency;
	
	private String sourceAccountNumber;
	
	private String destinationAccountNumber;
	
	private String destinationAccountName;
	
	private String transactionAmount;
	
	private String paymentType;
	
	private String bankCode;
	
	private String description1;
	
	private String description2;
	
	private String description3;
	
	private String description4;
	
	private String userId;
	
	private String response;
	
	private String responseDesc;
	
	private String reserved1;
	
	private String reserved2;
	
	private String reserved3;
	
	private String reserved4;
	
	private String state;
	
	private String bpsxName;
	
	private String bpoxName;
	
	private String bppxName;
	
	private String noSakti;
	
	public Rtgs() {
		// TODO Auto-generated constructor stub
	}
	
	public Rtgs(String transactionId, String transactionDate, String bppxName, String state, String referenceNumber, String response, String responseDesc) {
		// TODO Auto-generated constructor stub
		this.transactionId = transactionId;
		this.transactionDate = transactionDate;
		this.referenceNumber = referenceNumber;
		this.bppxName = bppxName;
		this.state = state;
		this.response = response;
		this.responseDesc = responseDesc;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getTransactionFlag() {
		return transactionFlag;
	}

	public void setTransactionFlag(String transactionFlag) {
		this.transactionFlag = transactionFlag;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getAccountCurrency() {
		return accountCurrency;
	}

	public void setAccountCurrency(String accountCurrency) {
		this.accountCurrency = accountCurrency;
	}

	public String getSourceAccountNumber() {
		return sourceAccountNumber;
	}

	public void setSourceAccountNumber(String sourceAccountNumber) {
		this.sourceAccountNumber = sourceAccountNumber;
	}

	public String getDestinationAccountNumber() {
		return destinationAccountNumber;
	}

	public void setDestinationAccountNumber(String destinationAccountNumber) {
		this.destinationAccountNumber = destinationAccountNumber;
	}

	public String getDestinationAccountName() {
		return destinationAccountName;
	}

	public void setDestinationAccountName(String destinationAccountName) {
		this.destinationAccountName = destinationAccountName;
	}

	public String getTransactionAmount() {
		return transactionAmount.replaceFirst("^0+(?!$)", "");
	}

	public void setTransactionAmount(String transactionAmount) {
		this.transactionAmount = transactionAmount.replaceFirst("^0+(?!$)", "");
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public String getDescription1() {
		return description1;
	}

	public void setDescription1(String description1) {
		this.description1 = description1;
	}

	public String getDescription2() {
		return description2;
	}

	public void setDescription2(String description2) {
		this.description2 = description2;
	}

	public String getDescription3() {
		return description3;
	}

	public void setDescription3(String description3) {
		this.description3 = description3;
	}

	public String getDescription4() {
		return description4;
	}

	public void setDescription4(String description4) {
		this.description4 = description4;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getResponseDesc() {
		return responseDesc;
	}

	public void setResponseDesc(String responseDesc) {
		this.responseDesc = responseDesc;
	}

	public String getReserved1() {
		return reserved1;
	}

	public void setReserved1(String reserved1) {
		this.reserved1 = reserved1;
	}

	public String getReserved2() {
		return reserved2;
	}

	public void setReserved2(String reserved2) {
		this.reserved2 = reserved2;
	}

	public String getReserved3() {
		return reserved3;
	}

	public void setReserved3(String reserved3) {
		this.reserved3 = reserved3;
	}

	public String getReserved4() {
		return reserved4;
	}

	public void setReserved4(String reserved4) {
		this.reserved4 = reserved4;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}


	public String getBpsxName() {
		return bpsxName;
	}

	public void setBpsxName(String bpsxName) {
		this.bpsxName = bpsxName;
	}

	public String getBpoxName() {
		return bpoxName;
	}

	public void setBpoxName(String bpoxName) {
		this.bpoxName = bpoxName;
	}

	public String getBppxName() {
		return bppxName;
	}

	public void setBppxName(String bppxName) {
		this.bppxName = bppxName;
	}

	public String getNoSakti() {
		return noSakti;
	}

	public void setNoSakti(String noSakti) {
		this.noSakti = noSakti;
	}

	
	
}
