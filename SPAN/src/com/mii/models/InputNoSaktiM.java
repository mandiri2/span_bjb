package com.mii.models;

import java.io.Serializable;
import java.util.Date;

public class InputNoSaktiM implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String Id,NTPN, NTB, kodeBank, kodeCabang, tglTerimaBayar, jamTerimaBayar, tanggalBuku, 
	jumlahSetoran, tglTransaksi, noSakti, jumlahTrx,kodeBilling,currency,referenceNo;
	
	private Date tglgabung;

	public InputNoSaktiM(){
		
	}

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}


	public String getKodeBilling() {
		return kodeBilling;
	}


	public void setKodeBilling(String kodeBilling) {
		this.kodeBilling = kodeBilling;
	}


	public String getJumlahTrx() {
		return jumlahTrx;
	}


	public void setJumlahTrx(String jumlahTrx) {
		this.jumlahTrx = jumlahTrx;
	}


	public String getNoSakti() {
		return noSakti;
	}


	public void setNoSakti(String noSakti) {
		this.noSakti = noSakti;
	}


	public Date getTglgabung() {
		return tglgabung;
	}


	public void setTglgabung(Date tglgabung) {
		this.tglgabung = tglgabung;
	}


	public String getNTPN() {
		return NTPN;
	}


	public void setNTPN(String nTPN) {
		NTPN = nTPN;
	}


	public String getNTB() {
		return NTB;
	}


	public void setNTB(String nTB) {
		NTB = nTB;
	}


	public String getKodeBank() {
		return kodeBank;
	}


	public void setKodeBank(String kodeBank) {
		this.kodeBank = kodeBank;
	}


	public String getKodeCabang() {
		return kodeCabang;
	}


	public void setKodeCabang(String kodeCabang) {
		this.kodeCabang = kodeCabang;
	}


	public String getTglTerimaBayar() {
		return tglTerimaBayar;
	}


	public void setTglTerimaBayar(String tglTerimaBayar) {
		this.tglTerimaBayar = tglTerimaBayar;
	}


	public String getJamTerimaBayar() {
		return jamTerimaBayar;
	}


	public void setJamTerimaBayar(String jamTerimaBayar) {
		this.jamTerimaBayar = jamTerimaBayar;
	}


	public String getTanggalBuku() {
		return tanggalBuku;
	}


	public void setTanggalBuku(String tanggalBuku) {
		this.tanggalBuku = tanggalBuku;
	}



	public String getJumlahSetoran() {
		return jumlahSetoran;
	}


	public void setJumlahSetoran(String jumlahSetoran) {
		this.jumlahSetoran = jumlahSetoran;
	}



	public String getTglTransaksi() {
		return tglTransaksi;
	}


	public void setTglTransaksi(String tglTransaksi) {
		this.tglTransaksi = tglTransaksi;
	}



	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}
	
	
	}