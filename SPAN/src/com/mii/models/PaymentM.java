package com.mii.models;

import java.io.Serializable;

public class PaymentM implements Serializable{
	private String Id, settlementdate,createdate,branchcode,ntb,ntpn,paymentcode, amount, billinginfo1, billinginfo2, 
	billinginfo3, billinginfo4, billinginfo5, billinginfo6, billinginfo7, billinginfo8, reserved2;

	public String getSettlementdate() {
		return settlementdate;
	}

	public void setSettlementdate(String settlementdate) {
		this.settlementdate = settlementdate;
	}

	public String getCreatedate() {
		return createdate;
	}

	public void setCreatedate(String createdate) {
		this.createdate = createdate;
	}

	public String getBranchcode() {
		return branchcode;
	}

	public void setBranchcode(String branchcode) {
		this.branchcode = branchcode;
	}

	public String getNtb() {
		return ntb;
	}

	public void setNtb(String ntb) {
		this.ntb = ntb;
	}

	public String getNtpn() {
		return ntpn;
	}

	public void setNtpn(String ntpn) {
		this.ntpn = ntpn;
	}

	public String getPaymentcode() {
		return paymentcode;
	}

	public void setPaymentcode(String paymentcode) {
		this.paymentcode = paymentcode;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getBillinginfo1() {
		return billinginfo1;
	}

	public void setBillinginfo1(String billinginfo1) {
		this.billinginfo1 = billinginfo1;
	}

	public String getBillinginfo2() {
		return billinginfo2;
	}

	public void setBillinginfo2(String billinginfo2) {
		this.billinginfo2 = billinginfo2;
	}

	public String getBillinginfo3() {
		return billinginfo3;
	}

	public void setBillinginfo3(String billinginfo3) {
		this.billinginfo3 = billinginfo3;
	}

	public String getBillinginfo4() {
		return billinginfo4;
	}

	public void setBillinginfo4(String billinginfo4) {
		this.billinginfo4 = billinginfo4;
	}

	public String getBillinginfo5() {
		return billinginfo5;
	}

	public void setBillinginfo5(String billinginfo5) {
		this.billinginfo5 = billinginfo5;
	}

	public String getBillinginfo6() {
		return billinginfo6;
	}

	public void setBillinginfo6(String billinginfo6) {
		this.billinginfo6 = billinginfo6;
	}

	public String getBillinginfo7() {
		return billinginfo7;
	}

	public void setBillinginfo7(String billinginfo7) {
		this.billinginfo7 = billinginfo7;
	}

	public String getBillinginfo8() {
		return billinginfo8;
	}

	public void setBillinginfo8(String billinginfo8) {
		this.billinginfo8 = billinginfo8;
	}

	public String getReserved2() {
		return reserved2;
	}

	public void setReserved2(String reserved2) {
		this.reserved2 = reserved2;
	}

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}
	
	

}
